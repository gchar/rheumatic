﻿<?php
session_start();

include 'library/config.php';
include 'library/openDB.php';
include 'library/functions.php';

$username=trim($_REQUEST['username']);
$password=trim($_REQUEST['password']);
$application=trim($_REQUEST['application']);

	if (($username=="") || ($password==""))
	{
	?>
<script>
alert("Insert username and/or password!");
</script>
<?php
		echo("<script>window.location.href = 'index.php?login=error';</script>");
	}
	else if($application=="1")
	{
		$sql = get_username($username,'');
		$result = pg_fetch_array($sql);
		$numrows = pg_num_rows($sql);
		$user_id=$result['user_id'];
		$login_attempt=$result['login_attempt'];
		
		if($numrows > 0)
		{
			if($login_attempt>=10) //Υπέρβαση μέγιστου αριθμού προσπαθιών εισόδου στο σύστημα
			{
				?>
				<script>
				alert("Your account has been blocked due to a large number of unsuccessful login attempts.Contact with administrators!");
				</script>
				<?php
				echo("<script>window.location.href = 'index.php?login=error';</script>");
			}
			else
			{
				//Τσεκάρουμε το password
				$sql3 = check_password($username,$password);
				$result3 = pg_fetch_array($sql3);
				$numrows3 = pg_num_rows($sql3);
				if($numrows3 > 0)
				{
					//Τσεκάρουμε αν έχει πρόσβαση στην συγκεκριμένη εφαρμογή
						
						$sql2 = check_application($result3['user_id'],$application);
						$result2 = pg_fetch_array($sql2);
						$numrows2 = pg_num_rows($sql2);
						if($numrows2 > 0)
						{
							//Έχει δώσει σωστά στοιχεία και έχει δικαίωμα πρόσβασης στην εφαρμογή
						
							$user_id=$result3['user_id'];
							$role_id=$result3['role_id'];
							$name=$result3['name'];
							$surname=$result3['surname'];
							$photo=$result3['photo'];
							$update_value=reset_login_attempts($user_id);
							
							$logging_id=start_logging($user_id,1);
							
							$_SESSION['user_id']=$user_id;
							$_SESSION['user']=$surname.' '.$name;
							$_SESSION['application_id']=1;
							$_SESSION['logging_id']=$logging_id;
							echo("<script>window.location.href = 'admin/index.php';</script>");
						}
						else // Δεν έχει πρόσβαση στην συγκεκριμένη εφαρμογή
						{
							$update_value=reset_login_attempts($user_id);
							
							 ?>
								<script>
								//alert("Δεν έχετε δικαίωμα πρόσβασης στο συγκεκριμένο υποσύστημα! Επιλέξτε κάποιο άλλο στο οποίο έχετε πρόσβαση.");
								alert("You do not have administrator rights! Choose another role.");
								</script>
							<?php
								echo("<script>window.location.href = 'index.php?login=error';</script>");
						}
					
				}
				else //Λάθος password
				{
						
					$new_login_attempt=$login_attempt+1;
					$remaining_attempts= 10 - $new_login_attempt;
					$update_value=update_login_attempts($new_login_attempt,$result['user_id']);
					
					if($remaining_attempts<=0)
					{
					?>
					<script>
					alert("You have entered wrong password! Your account has been blocked due to a large number of failed login attempts. Contact with administrators!");
					</script>
					<?php
					}
					else
					{
					?>
					<script>
					alert("You have entered wrong password. You still have <?php echo $remaining_attempts; ?> attempts before your account is locked!");
					</script>
					<?php
					}
					echo("<script>window.location.href = 'index.php?login=error';</script>");
				}
			}
		}
		else //Λάθος username ή ο χρήστης είναι ανενεργός
		{
					?>
					<script>
					alert("You have entered wrong password or your account is inactive!");
					</script>
					<?php
					echo("<script>window.location.href = 'index.php?login=error';</script>");
		}
		
	}
	else  if($application=="2")
	{	

		$sql = get_username($username,'');
		$result = pg_fetch_array($sql);
		$numrows = pg_num_rows($sql);
		$user_id=$result['user_id'];
			
		if($numrows > 0)
		{
			$login_attempt=$result['login_attempt'];
			if($login_attempt>=10) //Υπέρβαση μέγιστου αριθμού προσπαθιών εισόδου στο σύστημα
			{
				?>
				<script>
				alert("Your account has been blocked due to a large number of unsuccessful login attempts.Contact with administrators!");
				</script>
				<?php
				echo("<script>window.location.href = 'index.php?login=error';</script>");
			}
			else
			{
				//Τσεκάρουμε το password
				$sql3 = check_password($username,$password);
				$result3 = pg_fetch_array($sql3);
				$numrows3 = pg_num_rows($sql3);
				if($numrows3 > 0)
				{
					//Τσεκάρουμε αν έχει πρόσβαση σε έστω μια από τις δυο εφαρμογές
					
					$sql2 = check_application($result3['user_id'],"2");
					$result2 = pg_fetch_array($sql2);
					$numrows2 = pg_num_rows($sql2);
					if($numrows2 > 0)
						$rights="1";
					else
						$rights="0";
					
					if($rights == "1")
					{
						//Έχει δώσει σωστά στοιχεία και έχει δικαίωμα πρόσβασης στην εφαρμογή
					
						$user_id=$result3['user_id'];
						$name=$result3['name'];
						$surname=$result3['surname'];
						$centre=$result3['centre'];
						$all_rpts=$result3['all_rpts'];
						$centre_val=$result3['centre_val'];
						
						$update_value=reset_login_attempts($user_id);
						$logging_id=start_logging($user_id,2);
						
						$_SESSION['user_id']=$user_id;
						$_SESSION['application_id']=2;
						$_SESSION['user']=$surname.' '.$name;;
						$_SESSION['logging_id']=$logging_id;
						$_SESSION['user_centre_id']=$centre;
						$_SESSION['user_centre_val']=$centre_val;
						$_SESSION['user_all_rpts']=$all_rpts;
						echo("<script>window.location.href = 'user/index.php';</script>");
											}
					else // Δεν έχει πρόσβαση στην συγκεκριμένη εφαρμογή
					{
						$update_value=reset_login_attempts($user_id);
						
						 ?>
							<script>
							alert("You do not have user rights! Choose another role.");
							</script>
						<?php
							echo("<script>window.location.href = 'index.php?login=error';</script>");
					}
				}
				else //Λάθος password
				{
				
					$new_login_attempt=$login_attempt+1;
					$remaining_attempts= 10 - $new_login_attempt;
					$update_value=update_login_attempts($new_login_attempt,$result['user_id']);
					
					if($remaining_attempts<=0)
					{
					?>
					<script>
					alert("Your account has been blocked due to a large number of unsuccessful login attempts.Contact with administrators!");
					</script>
					<?php
					}
					else
					{
					?>
					<script>
					alert("You have entered wrong password. You still have <?php echo $remaining_attempts; ?> attempts before your account is locked!");
					</script>
					<?php
					}
						echo("<script>window.location.href = 'index.php?login=error';</script>");
				}
			}
		}
		else //Λάθος username ή ο χρήστης είναι ανενεργός
		{
					?>
					<script>
					alert("You have entered wrong password or your account is inactive!");
					</script>
					<?php
					echo("<script>window.location.href = 'index.php?login=error';</script>");
		}
	}
	include 'library/closeDB.php';
	?>