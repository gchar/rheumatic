<?php
	session_start();
	
	include 'library/config.php';
	include 'library/openDB.php';
	include 'library/functions.php';
	$logout=$_REQUEST['logout'];
	$logging_id=$_SESSION['logging_id'];
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>UCRCR</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="plugins/select2/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">  
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" href="css/epoch_styles.css">
  <link rel="stylesheet" href="css/loading.css">
</head>
<body class="hold-transition login-page">
<?php
	if($logout==1)
		$msg=stop_logging($logging_id);
	
	$_SESSION['user_id']="";
	$_SESSION['user']="";
	$_SESSION['application_id']="";
	$_SESSION['logging_id']="";
?>
<div class="login-box">
  <div class="login-logo">
	<img src="images/icsa.png">
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <h4><strong><p class="login-box-msg">University of Crete</br>Rheumatology Clinic Registry</p></strong></h4>
	<p class="login-box-msg">Sign in to start your session</p>
    <form action="logincheck.php" method="post" id="loginform" name="loginform">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" id="username" name="username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" id="password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
	  <div class="form-group has-feedback">
			<b>Role:</b>
			<select name="application" id="application" class="form-control" >
				<option value="0">--</option>
				<option value="2" selected>User</option>
				<option value="1">Administrator</option>
			</select>
		</div>
      <div class="row">
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
	</div>
	</br>
		
        <!-- /.col -->
    </form>
    <!-- /.social-auth-links -->
  </div>
  <!-- /.login-box-body -->
  </br>
  <div class="login-logo">
	<img src="images/icsab.png">
  </div>
  <?php
	$sql = get_version();
	$result = pg_fetch_array($sql);
	$version=$result['version'];
?>
  <div class="row">
			<div class="col-12" align="center">Copyright @ 2020 ICS - FORTH - CeHA - Version:<?php echo $version?></div>
		</div>
</div>
<!-- /.login-box -->
<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="plugins/input-mask/jquery.inputmask.js"></script>
<script src="plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="bower_components/moment/min/moment.min.js"></script>
<script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- dataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<!--  jQuery UI - v1.11.4 - 2015-03-11 -->
<script src="plugins/jQueryUI/jquery-ui.js"></script>
<script src="js/epoch_classes.js"></script>
<script src="js/utils.js"></script>
<script src="js/loading.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
	   
    $('#loginform').submit(function(){
		
		var msg="";
		 
		 if($('#username').val()=='')
			msg +='-Enter username!\n'; 
		
		if($('#password').val()=='')
			msg +='-Enter password!\n'; 
		 
		if ($('#application').val()=="0" )
			msg +='-Select role!\n';
		 
			 
			
		if(msg != '')
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
  });	

  document.getElementById('username').focus();</script>
</body>
</html>
