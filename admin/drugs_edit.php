﻿<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	$save=$_REQUEST['save'];
	$drugs_id=$_REQUEST['drugs_id'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>
</head>
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<?php
	if($save==1)
	{
		
		if (isset($_REQUEST['previous']))
			$previous=1;
		else
			$previous=0;
			
		if (isset($_REQUEST['ongoing']))
			$ongoing=1;
		else
			$ongoing=0;
		
		if (isset($_REQUEST['prim']))
			$prim=1;
		else
			$prim=0;
			
			
		$parameters[0]=str_replace("'", "''",$_REQUEST['substance']);
		$parameters[1]=str_replace("'", "''",$_REQUEST['code']);
		$parameters[2]=str_replace("'", "''",$_REQUEST['min']);
		$parameters[3]=str_replace("'", "''",$_REQUEST['max']);
		$parameters[4]=$_REQUEST['route_of_administration'];
		$parameters[5]=0;
		$parameters[6]=$previous;
		$parameters[7]=$ongoing;
		$parameters[8]=$prim;
		$table_names[0]='substance';
		$table_names[1]='code';
		$table_names[2]='min';
		$table_names[3]='max';
		$table_names[4]='route_of_administration';
		$table_names[5]='deleted';
		$table_names[6]='previous';
		$table_names[7]='ongoing';
		$table_names[8]='prim';
		$edit_name[0]='drugs_id';
		$edit_id[0]=$drugs_id;
		$sumbol[0]='=';
		
		pg_query("BEGIN") or die("Could not start transaction\n");
		try
		{
			if($drugs_id=='')
			{
				$drugs_id=insert('drugs',$table_names,$parameters,'drugs_id');
				if($drugs_id!='')
					$msg="ok";
			}
			else
			{
				$msg=update('drugs',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
				 
			}
				
			
			if($msg=="ok")
				pg_query("COMMIT") or die("Transaction commit failed\n");
			else
				pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		catch(exception $e) {
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
		
	}
?>
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
	  <?php
		$exec = get_drug($drugs_id);
		$result = pg_fetch_array($exec);
		$code=$result['code'];
		$substance=$result['substance'];
		$min=$result['min'];
		$max=$result['max'];
		$route_of_administration=$result['route_of_administration'];
		$previous=$result['previous'];
		$ongoing=$result['ongoing'];
		$prim=$result['prim'];
	?>
      <section class="content-header">
	  <h1>
          <small>
			<?php if($drugs_id=='') { echo 'Add'; } else { echo 'Edit'; } ?> Drug <?php if($drugs_id!='') { echo ":".$substance; } ?>
			</small>
        </h1>
	  </section>
			<br>
			 <!-- Main content -->
			<section class="content">
			 <div class="alert alert_suc alert-success" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Successful save!</strong>
			  </div>
			  <div class="alert alert_wr alert-danger" role="alert" style="DISPLAY:none;">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>Unsuccessful save!</strong>
			  </div>
			<form id="form" action="drugs_edit.php?drugs_id=<?php echo $drugs_id; ?>" method="POST">
			<br>
			<input type="hidden" id="save" name="save" value="1">
			<input type="hidden" id="drugs_id" name="drugs_id" value="<?php echo $drugs_id;?>">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<!--<div class="box-header with-border">
							<h3 class="box-title"></h3>
						</div>-->
						<div class="box-body">
							<div class="row">
								<div class="form-group col-md-6" id="divname" name="divname">
								  <label>*Code</label>
								  <input type="text" class="form-control" placeholder="code ..." value="<?php echo $code; ?>" id="code" name="code">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6" id="divname" name="divname">
								  <label>*Substance/Substance group</label>
								  <input type="text" class="form-control" placeholder="value ..." value="<?php echo $substance; ?>" id="substance" name="substance">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6" id="divname" name="divname">
								  <label>*Route of administration</label>
								  <select class="form-control" name="route_of_administration" id="route_of_administration">
									<option value="0" <?php if($route_of_administration==0) { echo "selected"; } ?>>--</option>
									<?php
										$sql = get_lookup_tbl_values('route_of_administration');
										$numrows = pg_num_rows($sql);
										while($result2 = pg_fetch_array($sql))
										{
											$route=$result2['id'];
											$value=$result2['value'];
									?>
											<option value="<?php echo $route; ?>" <?php if($route_of_administration==$route) { echo "selected"; } ?>><?php echo $value; ?></option>
									<?php
										}
									?>
									</select>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6" id="divmin" name="divmin">
								  <label>*Minimum dose</label>
								  <input type="text" class="form-control" placeholder="Minimum dose ..." value="<?php echo $min; ?>" id="min" name="min">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6" id="divmax" name="divmax">
								  <label>*Maximum dose</label>
								  <input type="text" class="form-control" placeholder="Maximum dose ..." value="<?php echo $max; ?>" id="max" name="max">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6" id="divaccess" name="divaccess">
								  <label>Use only</label>
									  <div>
											<label><input type="checkbox" id="previous" name="previous" <?php if ($previous==1) { echo "checked"; }?> >Previous Rheumatic drugs</label>
										</div>
										<div>
											<label><input type="checkbox" id="ongoing" name="ongoing" <?php if ($ongoing==1) { echo "checked"; }?> >Ongoing Rheumatic drugs</label>
										</div>
										<div>
											<label><input type="checkbox" id="prim" name="prim" <?php if ($prim==1) { echo "checked"; }?> >Primary cohort assignment</label>
										</div>
								  <label class="control-label" for="inputError" id="Erroraccess" name="Erroraccess" style="display:none;"><i class="fa fa-times-circle-o"></i> Please select at least one option!</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6">
								  <input type="submit" class="btn btn-primary" value="Save">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</form>
		</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>
  $(function () {
	  
	   $('#form').submit(function(){
		
		var msg="";
		 
		if($('#substance').val()=='')
			msg +='-Fill field substance!\n'; 
		
		if($('#code').val()=='')
			msg +='-Fill field code!\n';

		if($('#route_of_administration').val()=='0')
			msg +='-Select route of administration!\n';
		 	 
		if($('#min').val()=='')
			msg +='-Fill field minimum dose!\n'; 
		
		if($('#max').val()=='')
			msg +='-Fill field maximum dose!\n'; 
		
		if(msg != '')
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
		
		
	  });	
</script>
<?php
	
if($save==1)
{
	if($msg=="ok")
	{
?>
<script>
$(".alert_suc").show();
window.setTimeout(function() {
    $(".alert_suc").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
window.location = "drugs_edit.php";
</script>
<?php
	}
	else
	{
?>
<script>
$(".alert_wr").show();
window.setTimeout(function() {
    $(".alert_wr").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
}
?>
</body>
</html>
