﻿<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>

</head>
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <small>Logging list</small>
        </h1>
      </section>
			 <!-- Main content -->
			<section class="content">
			<div class="box">
            <!-- /.box-header -->
            <div class="box-body">
			<?php
				$exec = get_logging();
				$num_rows = pg_num_rows($exec);
				if ($num_rows=='0')
				{
				?>
				<tr align=center><td colspan=3>
				<?php
					echo "No records!!!";
				?>
				</td></tr>
				</table>
				<br>
				<?php
				}
				else
				{
				?>
			<div class="row">
			  <div class="col-md-8">
				<table class="table table-bordered table-striped" role="grid" id="table_jq"  width="100%"  rules="all">
					<thead>
						<tr class="gradeC">
							<th>User</th>
							<th>Role</th>
							<th>Login date</th>
							<th>Logout date</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=0;
						while($result = pg_fetch_array($exec))
						{
							
							$login_date=$result['login_date_str'];
							if ($login_date=='12-12-1900')
								$login_date_str="";
							$logout_date=$result['logout_date_str'];
							if ($logout_date=='12-12-1900')
								$login_date="";
							$surname=$result['surname'];
							$name=$result['name'];
							$title=$result['title'];
							
							
								$i++;
					?>
						<tr class="gradeA" align="center" >
							<td><?php echo $surname.' '.$name; ?></td>
							<td><?php echo $title; ?></td>
							<td><?php echo $login_date; ?></td>
							<td><?php echo $logout_date; ?></td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table>
			</div>
			</div>
				<?php
				}
				?>
			</div>
			</div>
			 </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#table_jq').dataTable( {} );
	});
</script>
</body>
</html>