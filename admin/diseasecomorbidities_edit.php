﻿<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	$save=$_REQUEST['save'];
	$diseases_comorbidities_id=$_REQUEST['diseases_comorbidities_id'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>
</head>
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<?php
	if($save==1)
	{
		$parameters[0]=str_replace("'", "''",$_REQUEST['value']);
		$parameters[1]=str_replace("'", "''",$_REQUEST['code']);
		$parameters[2]=str_replace("'", "''",$_REQUEST['icd10']);
		$parameters[3]=0;
		$parameters[4]=$_REQUEST['diseases_organs_id'];
		$table_names[0]='value';
		$table_names[1]='code';
		$table_names[2]='icd10';
		$table_names[3]='deleted';
		$table_names[4]='diseases_organs_id';
		$edit_name[0]='diseases_comorbidities_id';
		$edit_id[0]=$diseases_comorbidities_id;
		$sumbol[0]='=';
		
		pg_query("BEGIN") or die("Could not start transaction\n");
		try
		{
			if($diseases_comorbidities_id=='')
			{
				$diseases_comorbidities_id=insert('diseases_comorbidities',$table_names,$parameters,'diseases_comorbidities_id');
				if($diseases_comorbidities_id!='')
					$msg="ok";
			}
			else
			{
				$msg=update('diseases_comorbidities',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
				 
			}
				
			
			if($msg=="ok")
				pg_query("COMMIT") or die("Transaction commit failed\n");
			else
				pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		catch(exception $e) {
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
		
	}
?>
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
	  <?php
		$exec = get_disease_comorbidities($diseases_comorbidities_id);
		$result = pg_fetch_array($exec);
		$diseases_comorbidities_id=$result['diseases_comorbidities_id'];
		$diseases_comorbidities_value=$result['value'];
		$code=$result['code'];
		$icd10=$result['icd10'];
		$diseases_organs_id=$result['diseases_organs_id'];
		
	?>
      <section class="content-header">
	  <h1>
          <small>
			<?php if($diseases_comorbidities_id=='') { echo 'Add'; } else { echo 'Edit'; } ?> Disease Comorbidities: <?php echo $value; ?>
			</small>
        </h1>
	  </section>
			<br>
			 <!-- Main content -->
			<section class="content">
			<form id="form" action="diseasecomorbidities_edit.php?diseases_comorbidities_id=<?php echo $diseases_comorbidities_id; ?>" method="POST">
			<br>
			<input type="hidden" id="save" name="save" value="1">
			<input type="hidden" id="diseases_comorbidities_id" name="diseases_comorbidities_id" value="<?php echo $diseases_comorbidities_id;?>">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<!--<div class="box-header with-border">
							<h3 class="box-title"></h3>
						</div>-->
						<div class="box-body">
							<div class="row">
								<div class="form-group col-md-6">
								  <label>Organ/System</label>
									<select class="form-control"  id="diseases_organs_id" name="diseases_organs_id">
										<option value="0" <?php if($diseases_organs_id=="") { echo "selected"; } ?>>--</option>
											<?php
												$query2="select diseases_organs_id,value from diseases_organs  where deleted=0 order by value asc ";
												
												$exec2 = pg_query($query2);
												
												while($result2 = pg_fetch_array($exec2))
												{
													$id=$result2['diseases_organs_id'];
													$value =$result2['value'];
											?>
													<option value="<?php echo $id; ?>" <?php if($diseases_organs_id==$id) { echo "selected"; } ?>><?php echo $value; ?></option>
											<?php
												}
											?>
									</select>
								  <label class="control-label" for="inputError" id="Errorsurname" name="Errorsurname" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6" id="divname" name="divname">
								  <label>*value</label>
								  <input type="text" class="form-control" placeholder="value ..." value="<?php echo $diseases_comorbidities_value; ?>" id="value" name="value">
								  <label class="control-label" for="inputError" id="Errorname" name="Errorname" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6" id="divname" name="divname">
								  <label>*code</label>
								  <input type="text" class="form-control" placeholder="code ..." value="<?php echo $code; ?>" id="code" name="code">
								  <label class="control-label" for="inputError" id="Errorname" name="Errorname" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6" id="divname" name="divname">
								  <label>ICD-10 code</label>
								  <input type="text" class="form-control" placeholder="ICD-10 code ..." value="<?php echo $icd10; ?>" id="icd10" name="icd10">
								  <label class="control-label" for="inputError" id="Errorname" name="Errorname" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6">
								  <input type="submit" class="btn btn-primary" value="Save">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</form>
		</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>
  $(function () {
	  
	   $('#form').submit(function(){
		
		var msg="";
		 
		if($('#value').val()=='')
			msg +='-Fill field Value!\n'; 
		if($('#code').val()=='')
			msg +='-Fill field code!\n'; 
		 	 
			
		if(msg != '')
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
		
		
	  });	
</script>
<?php

if($save==1)
{
	if($msg=="ok")
	{
?>
<script>
alert("Successful save!");
</script>
<?php
	}
	else
	{
?>
<script>
alert("Unsuccessful save!");
</script>
<?php
	}
}
?>
</body>
</html>