﻿<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	
	$list_code=$_REQUEST['list_code'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <?php
				$exec = get_lookup_table_val2($list_code);
				$result = pg_fetch_array($exec);
				$description=$result['description'];
				$has_parent=$result['parent_code'];
			?>
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <small>Dynamic list: <?php echo $description; ?></small>
        </h1>
      </section>
			 <!-- Main content -->
			<section class="content">
			<div class="box">
            <!-- /.box-header -->
            <div class="box-body">
			<a class="btn btn-primary" href="list_edit.php?list_code=<?php echo $list_code; ?>" target="_self"><i class="fa fa-plus"></i> New value</a>
			<br><br>
			<?php
				$exec = get_lookup_tbl_values($list_code);
				$num_rows = pg_num_rows($exec);
				if ($num_rows=='0')
				{
				?>
				<tr align=center><td colspan=3>
				<?php
					echo "No records!!!";
				?>
				</td></tr>
				</table>
				<br>
				<?php
				}
				else
				{
				?>
			<div class="row">
			  <div class="col-md-8">
				<table id="table_jq" class="table table-bordered table-striped">
					<thead>
						<tr class="gradeC">
							<?php if ($has_parent<>"0" and $has_parent<>"" ) { ?>
							<th>Parent value</th>
							<?php } ?>
							<th>Value</th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i=0;
							while($result = pg_fetch_array($exec))
							{
								
								$id=$result['id'];
								$value=$result['value'];
								$parent_value_id=$result['parent_value_id'];
								if ($has_parent<>"0" and $has_parent<>"" ) {
									
								$exec2 = get_lookup_tbl_value($parent_value_id);
								$result2 = pg_fetch_array($exec2);
								$parent_value=$result2['value'];
								}
								$i++;
					?>
						<tr class="gradeA" align="center" >
							<?php if ($has_parent<>"0" and $has_parent<>"" ) { ?>
							<td>&nbsp;<?php echo $parent_value; ?></td>
							<?php } ?>
							<td>&nbsp;<?php echo $value; ?></td>
							
							<td>
								<a title="edit" href="list_edit.php?id=<?php echo $id; ?>&list_code=<?php echo $list_code; ?>" target="_self"><i class="fa fa-edit"></i><span></a>
							</td>
							<td>
								<a onclick="setdeleteid('lookup_tbl_val','id',<?php echo $id; ?>)" title="delete" href="#" ><i class="fa fa-trash"></i><span></a>
							</td>
						</tr>
						<?php
							}
						?>
					</tbody>
				</table> <?php
				}
				?>
            </div>
			</div>
       <!-- /.box-body -->
       </div>
       <!-- /.box -->
		</div>
		
	
			 </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#table_jq').dataTable({});
	});
</script>
</body>
</html>