﻿<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	$save=$_REQUEST['save'];
	$analgesics_id=$_REQUEST['analgesics_id'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>
</head>
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<?php
	if($save==1)
	{
		$parameters[0]=str_replace("'", "''",$_REQUEST['substance']);
		$parameters[1]=str_replace("'", "''",$_REQUEST['code']);
		$parameters[2]=str_replace("'", "''",$_REQUEST['dose']);
		$parameters[3]=str_replace("'", "''",$_REQUEST['max_dose']);
		$parameters[4]=0;
		$table_names[0]='substance';
		$table_names[1]='code';
		$table_names[2]='dose';
		$table_names[3]='max_dose';
		$table_names[4]='deleted';
		$edit_name[0]='analgesics_id';
		$edit_id[0]=$analgesics_id;
		$sumbol[0]='=';
		
		pg_query("BEGIN") or die("Could not start transaction\n");
		try
		{
			if($analgesics_id=='')
			{
				$analgesics_id=insert('analgesics',$table_names,$parameters,'analgesics_id');
				if($analgesics_id!='')
					$msg="ok";
			}
			else
			{
				$msg=update('analgesics_id',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
				 
			}
				
			
			if($msg=="ok")
				pg_query("COMMIT") or die("Transaction commit failed\n");
			else
				pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		catch(exception $e) {
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
		
	}
?>
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
	  <?php
		$exec = get_analgesic($analgesics_id);
		$result = pg_fetch_array($exec);
		$code=$result['code'];
		$substance=$result['substance'];
		$dose=$result['dose'];
		$max_dose=$result['max_dose'];
		
	?>
      <section class="content-header">
	  <h1>
          <small>
			<?php if($analgesics_id=='') { echo 'Add'; } else { echo 'Edit'; } ?> NSAIDs Analgesics <?php if($analgesics_id!='') { echo ":".$substance; } ?>
			</small>
        </h1>
	  </section>
			<br>
			 <!-- Main content -->
			<section class="content">
			<form id="form" action="analgesics_edit.php?analgesics_id=<?php echo $analgesics_id; ?>" method="POST">
			<br>
			<input type="hidden" id="save" name="save" value="1">
			<input type="hidden" id="analgesics_id" name="analgesics_id" value="<?php echo $analgesics_id;?>">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<!--<div class="box-header with-border">
							<h3 class="box-title"></h3>
						</div>-->
						<div class="box-body">
							<div class="row">
								<div class="form-group col-md-6" id="divname" name="divname">
								  <label>*Code</label>
								  <input type="text" class="form-control" placeholder="code ..." value="<?php echo $code; ?>" id="code" name="code">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6" id="divname" name="divname">
								  <label>*Substance</label>
								  <input type="text" class="form-control" placeholder="value ..." value="<?php echo $substance; ?>" id="substance" name="substance">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6" id="divmin" name="divmin">
								  <label>*dose</label>
								  <input type="text" class="form-control" placeholder="dose ..." value="<?php echo $dose; ?>" id="dose" name="dose">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6" id="divmax" name="divmax">
								  <label>*Maximum dose</label>
								  <input type="text" class="form-control" placeholder="Maximum dose ..." value="<?php echo $max_dose; ?>" id="max_dose" name="max_dose">
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6">
								  <input type="submit" class="btn btn-primary" value="Save">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</form>
		</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>
  $(function () {
	  
	   $('#form').submit(function(){
		
		var msg="";
		 
		if($('#substance').val()=='')
			msg +='-Fill field substance!\n'; 
		
		if($('#code').val()=='')
			msg +='-Fill field code!\n'; 
		 	 
		if($('#dose').val()=='')
			msg +='-Fill field dose!\n'; 
		
		if($('#max_dose').val()=='')
			msg +='-Fill field maximum dose!\n'; 
		
		if(msg != '')
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
		
		
	  });	
</script>
<?php

if($save==1)
{
	if($msg=="ok")
	{
?>
<script>
alert("Successful save!");
</script>
<?php
	}
	else
	{
?>
<script>
alert("Unsuccessful save!");
</script>
<?php
	}
}
?>
</body>
</html>