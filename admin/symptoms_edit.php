﻿<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	$save=$_REQUEST['save'];
	$symptoms_id=$_REQUEST['symptoms_id'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>
</head>
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<?php
	if($save==1)
	{
		$parameters[0]=str_replace("'", "''",$_REQUEST['value']);
		$parameters[1]=0;
		$parameters[2]=$_REQUEST['symptoms_organs_id'];
		$table_names[0]='value';
		$table_names[1]='deleted';
		$table_names[2]='symptoms_organs_id';
		$edit_name[0]='symptoms_id';
		$edit_id[0]=$symptoms_id;
		$sumbol[0]='=';
		
		pg_query("BEGIN") or die("Could not start transaction\n");
		try
		{
			if($symptoms_id=='')
			{
				$symptoms_id=insert('symptoms',$table_names,$parameters,'symptoms_id');
				if($symptoms_id!='')
					$msg="ok";
			}
			else
			{
				$msg=update('symptoms',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
				 
			}
				
			
			if($msg=="ok")
				pg_query("COMMIT") or die("Transaction commit failed\n");
			else
				pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		catch(exception $e) {
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
		
	}
?>
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
	  <?php
		$exec = get_symptom($symptoms_id);
		$result = pg_fetch_array($exec);
		$symptoms_organs_id=$result['symptoms_organs_id'];
		$symptom_value=$result['value'];
		
	?>
      <section class="content-header">
	  <h1>
          <small>
			<?php if($symptoms_id=='') { echo 'Add'; } else { echo 'Edit'; } ?> Symptom: <?php echo $value; ?>
			</small>
        </h1>
	  </section>
			<br>
			 <!-- Main content -->
			<section class="content">
			<form id="form" action="symptoms_edit.php?symptoms_id=<?php echo $symptoms_id; ?>" method="POST">
			<br>
			<input type="hidden" id="save" name="save" value="1">
			<input type="hidden" id="symptoms_id" name="symptoms_id" value="<?php echo $symptoms_id;?>">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<!--<div class="box-header with-border">
							<h3 class="box-title"></h3>
						</div>-->
						<div class="box-body">
							<div class="row">
								<div class="form-group col-md-6">
								  <label>Organ/System</label>
									<select class="form-control"  id="symptoms_organs_id" name="symptoms_organs_id">
										<option value="0" <?php if($symptoms_organs_id=="") { echo "selected"; } ?>>--</option>
											<?php
												$query2="select symptoms_organs_id,value from symptoms_organs where deleted=0 order by value asc ";
												
												$exec2 = pg_query($query2);
												
												while($result2 = pg_fetch_array($exec2))
												{
													$id=$result2['symptoms_organs_id'];
													$value =$result2['value'];
											?>
													<option value="<?php echo $id; ?>" <?php if($symptoms_organs_id==$id) { echo "selected"; } ?>><?php echo $value; ?></option>
											<?php
												}
											?>
									</select>
								  <label class="control-label" for="inputError" id="Errorsurname" name="Errorsurname" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6" id="divname" name="divname">
								  <label>*value</label>
								  <input type="text" class="form-control" placeholder="value ..." value="<?php echo $symptom_value; ?>" id="value" name="value">
								  <label class="control-label" for="inputError" id="Errorname" name="Errorname" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6">
								  <input type="submit" class="btn btn-primary" value="Save">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</form>
		</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>
  $(function () {
	  
	   $('#form').submit(function(){
		
		var msg="";
		 
		if($('#value').val()=='')
			msg +='-Fill field Value!\n'; 
		 	 
			
		if(msg != '')
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
		
		
	  });	
</script>
<?php

if($save==1)
{
	if($msg=="ok")
	{
?>
<script>
alert("Successful save!");
</script>
<?php
	}
	else
	{
?>
<script>
alert("Unsuccessful save!");
</script>
<?php
	}
}
?>
</body>
</html>