﻿<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	
	$user_id2=$_REQUEST['user_id2'];
	$save=$_REQUEST['save'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>
</head>
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<?php
	if($save==1)
	{
		$sql = get_username(str_replace("'", "''",$_REQUEST['username']),$user_id2);
		$result = pg_fetch_array($sql);
		$numrows = pg_num_rows($sql);
			
		$sql2 = get_username2(str_replace("'", "''",$_REQUEST['mail']),$user_id2);
		$result2 = pg_fetch_array($sql2);
		$numrows2 = pg_num_rows($sql2);
		
		if($numrows > 0)
		{
			if($numrows2 > 0)
			{
				?>
				<script>
					alert("There is already a user with username : <?php echo str_replace("'", "''",$_REQUEST['username']); ?> and email: <?php echo str_replace("'", "''",$_REQUEST['mail']); ?> ! Please select another one!");
				</script>
				<?php
			}
			else
			{
				?>
				<script>
					alert("There is already a user with username : <?php echo str_replace("'", "''",$_REQUEST['username']); ?>! Please select another one!");
				</script>
				<?php
			}
			
		}
		else if($numrows2 > 0)
		{
			?>
			<script>
				alert("There is already a user with mail: <?php echo str_replace("'", "''",$_REQUEST['mail']); ?> ! Please select another one!");
			</script>
			<?php
		}
		else
		{
			$parameters[0]=str_replace("'", "''",$_REQUEST['name']);
			$parameters[1]=str_replace("'", "''",$_REQUEST['surname']);
			$parameters[2]=str_replace("'", "''",$_REQUEST['username']);
			$parameters[3]=str_replace("'", "''",$_REQUEST['mail']);
			$parameters[4]=str_replace("'", "''",$_REQUEST['telephone']);
			if ($_REQUEST['login_attempt']=="")
				$parameters[5]=0;
			else
				$parameters[5]=$_REQUEST['login_attempt'];
			
			$parameters[6]=$_REQUEST['deleted'];
			$parameters[7]=$_REQUEST['centre'];
			$parameters[8]=$_REQUEST['all_rpts'];
			
			
			
			if($_REQUEST['password']<>"")
				$ps=str_replace("'", "''",$_REQUEST['password']);
			
			$edit_name[0]='user_id';
			$edit_id[0]=$user_id2;
			$sumbol[0]='=';
			
			
			if (isset($_REQUEST['ch_ksd']))
				$ksd=0;
			else
				$ksd=1;
			
			if (isset($_REQUEST['ch_user']))
				$user=0;
			else
				$user=1;
		
			pg_query("BEGIN") or die("Could not start transaction\n");
			try
			{
				if($user_id2=='')
				{
					$query = "insert into users (name,surname,username,mail,telephone,login_attempt,deleted,password,centre,all_rpts) ";
					$query .= " values ('$parameters[0]','$parameters[1]','$parameters[2]','$parameters[3]','$parameters[4]',$parameters[5],$parameters[6],md5('".$ps."'),$parameters[7],$parameters[8])";
					
					$result = pg_query($query);
					if($result)
					{
						$query2="select user_id from users order by user_id desc limit 1";
						
						$order2 = pg_query($query2);
						$result2 = pg_fetch_array($order2);
						$user_id2=$result2["user_id"];
						if ($user_id2<>"")
						{
							$parameters1[0]=$user_id2;
							$parameters1[1]=$ksd;
							$parameters1[2]=1;
							$table_names1[0]='user_id';
							$table_names1[1]='deleted';
							$table_names1[2]='application_id';
							$set_ksd=insert('apps_per_user',$table_names1,$parameters1,'apps_per_user_id');
							
							$parameters2[0]=$user_id2;
							$parameters2[1]=$user;
							$parameters2[2]=2;
							$table_names2[0]='user_id';
							$table_names2[1]='deleted';
							$table_names2[2]='application_id';
							$set_tech=insert('apps_per_user',$table_names2,$parameters2,'apps_per_user_id');
							
							$msg="ok";
						}
					}
				}
				else
				{
					$query = "update users set name='$parameters[0]',surname='$parameters[1]',username='$parameters[2]',mail='$parameters[3]',telephone='$parameters[4]',login_attempt='$parameters[5]',deleted='$parameters[6]',centre=$parameters[7] ";
					$query .= ",all_rpts=$parameters[8]";
					if($_REQUEST['password']<>"")
						$query .= ",password=md5('".$ps."')";
					$query .= " where user_id=$user_id2";
					
					$result = pg_query($query);
					if($result)
					{
						$parameters1[0]=$ksd;
						$table_names1[0]='deleted';
						$edit_name1[0]='user_id';
						$edit_id1[0]=$user_id2;
						$sumbol1[0]='=';
						$edit_name1[1]='application_id';
						$edit_id1[1]=1;
						$sumbol1[1]='=';
						$set_ksd=update('apps_per_user',$table_names1,$parameters1,$edit_name1,$edit_id1,$sumbol1);
						
						$parameters2[0]=$user;
						$table_names2[0]='deleted';
						$edit_name2[0]='user_id';
						$edit_id2[0]=$user_id2;
						$sumbol2[0]='=';
						$edit_name2[1]='application_id';
						$edit_id2[1]=2;
						$sumbol2[1]='=';
						$set_tech=update('apps_per_user',$table_names2,$parameters2,$edit_name2,$edit_id2,$sumbol2);
						
						$msg="ok";
					}
				}
				
				pg_query("COMMIT") or die("Transaction commit failed\n");
			}
			catch(exception $e) {
				pg_query("ROLLBACK") or die("Transaction rollback failed\n");
			}
		}
	}
?>
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
			<h4><b><?php if($user_id2=='') { echo 'New'; } else { echo 'Edit'; } ?> user</b></h4>
	  </section>
			 <!-- Main content -->
			<section class="content">
			<form id="form" action="user_edit.php" method="POST"  enctype="multipart/form-data">
			<?php
				if($user_id2!="")
				{
					$exec = get_user($user_id2);
					$result = pg_fetch_array($exec);
					
					$user_id2=$result['user_id'];
					$username=$result['username'];
					$name=$result['name'];
					$surname=$result['surname'];
					$telephone=$result['telephone'];
					$mail=$result['mail'];
					$login_attempt=$result['login_attempt'];
					$deleted=$result['deleted'];
					$centre=$result['centre'];
					$all_rpts=$result['all_rpts'];
					
					$exec = get_app($user_id2,1);
					$ch_ksd = pg_numrows($exec);
					
					$exec = get_app($user_id2,2);
					$ch_user = pg_numrows($exec);
				}
				?>
			<input type="hidden" id="save" name="save" value="1">
			<input type="hidden" id="user_id2" name="user_id2" value="<?php echo $user_id2;?>">
			<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<!--<div class="box-header with-border">
						<h3 class="box-title"></h3>
					</div>-->
					<div class="box-body">
						<div class="row">
							<!--add has-error in class-->
							<div class="form-group col-md-6" id="divsurname" name="divsurname">
							  <label>*Surname</label>
							  <input type="text" class="form-control" placeholder="Surname ..." value="<?php echo $surname; ?>" id="surname" name="surname">
							</div>
							<div class="form-group col-md-6" id="divname" name="divname">
							  <label>*Name</label>
							  <input type="text" class="form-control" placeholder="Name ..." value="<?php echo $name; ?>" id="name" name="name">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-6" id="divusername" name="divusername">
							  <label>*Username</label>
							  <input type="text" class="form-control" placeholder="Username ..." value="<?php echo $username; ?>" id="username" name="username">
							</div>
							<div class="form-group col-md-6" id="divmail" name="divmail">
							  <label>*Email</label>
							  <input type="text" class="form-control" placeholder="Email ..." value="<?php echo $mail; ?>" id="mail" name="mail">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-6" id="divcentre" name="divcentre">
							  <label>*Centre</label>
							  <select class="form-control" name="centre" id="centre">
									<option value="0">--</option>
									<?php
										$sql = get_lookup_tbl_values("centre");
										$numrows = pg_num_rows($sql);
										while($result2 = pg_fetch_array($sql))
										{
											$centre_id=$result2['id'];
											$value =$result2['value'];
									?>
									<option value="<?php echo $centre_id; ?>" <?php if($centre_id==$centre) { echo "selected"; } ?>><?php echo $value; ?></option>
									<?php
										}
									?>
								  </select>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-6" id="divtelephone" name="divtelephone">
							  <label>Contact number</label>
							  <input type="text" class="form-control" placeholder="Contact number ..." value="<?php echo $telephone; ?>" id="telephone" name="telephone">
							  <label class="control-label" for="inputError" id="Errortelephone" name="Errortelephone" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
							</div>
							<div class="form-group col-md-6" id="divloginattempt" name="divloginattempt">
							  <label>Login attempts</label>
							  <input type="text" class="form-control" placeholder="Login attempts ..." value="<?php echo $login_attempt; ?>" id="login_attempt" name="login_attempt">
							  <label class="control-label" for="inputError" id="Errorloginattempt" name="Errorloginattempt" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-6" id="divpassword" name="divpassword">
							  <label>**Password</label>
							  <input type="password" class="form-control" placeholder="Password ..." value="<?php echo $password; ?>" id="password" name="password">
							  <label class="control-label" for="inputError" id="Errorpassword" name="Errorpassword" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
							</div>
							<div class="form-group col-md-6" id="divpassword2" name="divpassword2">
							  <label>**Confirm password</label>
							  <input type="password" class="form-control" placeholder="Password ..." value="<?php echo $password2; ?>" id="password2" name="password2" onKeyUp="checkPass(); return false;"><span id="confirmMessage" class="confirmMessage"></span>
							  <label class="control-label" for="inputError" id="Errorpassword2" name="Errorpassword2" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-6">
							  <label>Export statistics from all Centres?</label>
								<select class="form-control"  id="all_rpts" name="all_rpts">
									<option value="0" <?php if($all_rpts==0 or $all_rpts=="") { ?> selected <?php } ?>>No</option>
									<option value="1"<?php  if($all_rpts==1) { ?> selected <?php } ?>>Yes</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-6">
							  <label>Active</label>
								<select class="form-control"  id="deleted" name="deleted">
									<option value="0" <?php if($deleted==0 or $deleted=="") { ?> selected <?php } ?>>Yes</option>
									<option value="1"<?php  if($deleted==1) { ?> selected <?php } ?>>No</option>
								</select>
							</div>
							<div class="form-group col-md-6" id="divaccess" name="divaccess">
							  <label>Access to</label>
								  <div class="radio">
										<label><input type="checkbox" value="" id="ch_ksd" name="ch_ksd" <?php if ($ch_ksd==1) { echo "checked"; }?> >Administrator</label>
									</div>
									<div class="radio">
										<label><input type="checkbox" value="" id="ch_user" name="ch_user" <?php if ($ch_user==1) { echo "checked"; }?> >User</label>
									</div>
							  <label class="control-label" for="inputError" id="Erroraccess" name="Erroraccess" style="display:none;"><i class="fa fa-times-circle-o"></i> Please select at least one option!</label>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-8">
								*Mandatory fields
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-8">
								**If you do not want to change the password, leave the blank or else add the password you want
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-6">
							  <input type="submit" class="btn btn-primary" value="Save">
							</div>
						</div>
				  </div>
				</div>
			</div>	
				</div>
				</form>
			 </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>
function checkPass()
{
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('password');
    var pass2 = document.getElementById('password2');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
	if(pass1.value=="")
	{
		message.innerHTML="";
		 pass2.style.backgroundColor="#ffffff";
	}
	else
	{
		if(pass1.value == pass2.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Passwords match!!"
		}else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords does not match!!"
		}
	}
    
}  

  $(function () {
	  
	   $('#form').submit(function(){
		
		var msg="";
		 
		 if($('#surname').val()=='')
			msg +='-Fill field Surname!\n'; 
		
		if($('#name').val()=='')
			msg +='-Fill field Name!\n'; 
		
		 if($('#username').val()=='')
			msg +='-Fill field Username!\n'; 
		
		if($('#password').val()=='' && $('#user_id2').val()=='')
			msg +='-Fill field Password!\n'; 
		 
		if($('#mail').val()=='')
			msg +='-Fill field Email!\n';
		
		if($('#centre').val()=='0')
			msg +='-Select Centre!\n';
		 	 
			
		if(msg != '')
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
		
	  });	
</script>
<?php

if($save==1)
{
	if($msg=="ok")
	{
?>
<script>
alert("Successful save!");
</script>
<?php
	}
	else
	{
?>
<script>
alert("Unsuccessful save!");
</script>
<?php
	}
}
?>
</body>
</html>