﻿<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	$save=$_REQUEST['save'];
	$diagnosis_id=$_REQUEST['diagnosis_id'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>
</head>
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<?php
	if($save==1)
	{
		if (isset($_REQUEST['invisible']))
			$invisible=1;
		else
			$invisible=0;
		
		$parameters[0]=str_replace("'", "''",$_REQUEST['value']);
		$parameters[1]=str_replace("'", "''",$_REQUEST['code']);
		$parameters[2]=str_replace("'", "''",$_REQUEST['icd10']);
		//$parameters[3]=$_REQUEST['criteria'];
		$parameters[3]=0;
		$parameters[4]=$invisible;
		$table_names[0]='value';
		$table_names[1]='code';
		$table_names[2]='icd10';
		//$table_names[3]='criteria';
		$table_names[3]='deleted';
		$table_names[4]='invisible';
		
		$edit_name[0]='diagnosis_id';
		$edit_id[0]=$diagnosis_id;
		$sumbol[0]='=';
		
		pg_query("BEGIN") or die("Could not start transaction\n");
		try
		{
			if($diagnosis_id=='')
			{
				$diagnosis_id=insert('diagnosis',$table_names,$parameters,'diagnosis_id');
				if($diagnosis_id!='')
					$msg_start="ok";
			}
			else
			{
				$msg_start=update('diagnosis',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
				 
			}
			
			
			
			// criteria - start
			
			if (isset($_REQUEST['pa']))
				$pa="1";
			else
				$pa="0";
			
			if (isset($_REQUEST['peripheral']))
				$peripheral="1";
			else
				$peripheral="0";
			
			if (isset($_REQUEST['axial']))
				$axial="1";
			else
				$axial="0";
			
			if (isset($_REQUEST['caspar']))
				$caspar="1";
			else
				$caspar="0";
			
			if (isset($_REQUEST['sle']))
				$sle="1";
			else
				$sle="0";
			
			if (isset($_REQUEST['aps']))
				$aps="1";
			else
				$aps="0";
			
			$parameters_criteria_deleted[0]=1;
			$table_names_criteria_deleted[0]='deleted';
			$edit_name_criteria_deleted[0]='diagnosis_id';
			$edit_id_criteria_deleted[0]=$diagnosis_id;
			$sumbol_criteria_deleted[0]='=';
		
			$msg_deleted=update('criteriagroup_per_diagnosis',$table_names_criteria_deleted,$parameters_criteria_deleted,$edit_name_criteria_deleted,$edit_id_criteria_deleted,$sumbol_criteria_deleted);
			
			$parameters_criteria_pa[0]=$diagnosis_id;
			$parameters_criteria_pa[1]=1;
			$parameters_criteria_pa[2]=$pa;
			$parameters_criteria_pa[3]=$user_id;
			$parameters_criteria_pa[4]=0;
			$table_names_criteria_pa[0]='diagnosis_id';
			$table_names_criteria_pa[1]='criteria_id';
			$table_names_criteria_pa[2]='active';
			$table_names_criteria_pa[3]='editor_id';
			$table_names_criteria_pa[4]='deleted';
		
			$criteriagroup_per_diagnosis_id=insert('criteriagroup_per_diagnosis',$table_names_criteria_pa,$parameters_criteria_pa,'criteriagroup_per_diagnosis_id');
			if($criteriagroup_per_diagnosis_id!='')
				$msg_pa="ok";
			
			$parameters_criteria_peripheral[0]=$diagnosis_id;
			$parameters_criteria_peripheral[1]=2;
			$parameters_criteria_peripheral[2]=$peripheral;
			$parameters_criteria_peripheral[3]=$user_id;
			$parameters_criteria_peripheral[4]=0;
			$table_names_criteria_peripheral[0]='diagnosis_id';
			$table_names_criteria_peripheral[1]='criteria_id';
			$table_names_criteria_peripheral[2]='active';
			$table_names_criteria_peripheral[3]='editor_id';
			$table_names_criteria_peripheral[4]='deleted';
		
			$criteriagroup_per_diagnosis_id=insert('criteriagroup_per_diagnosis',$table_names_criteria_peripheral,$parameters_criteria_peripheral,'criteriagroup_per_diagnosis_id');
			if($criteriagroup_per_diagnosis_id!='')
				$msg_peripheral="ok";
			
			$parameters_criteria_axial[0]=$diagnosis_id;
			$parameters_criteria_axial[1]=3;
			$parameters_criteria_axial[2]=$axial;
			$parameters_criteria_axial[3]=$user_id;
			$parameters_criteria_axial[4]=0;
			$table_names_criteria_axial[0]='diagnosis_id';
			$table_names_criteria_axial[1]='criteria_id';
			$table_names_criteria_axial[2]='active';
			$table_names_criteria_axial[3]='editor_id';
			$table_names_criteria_axial[4]='deleted';
		
			$criteriagroup_per_diagnosis_id=insert('criteriagroup_per_diagnosis',$table_names_criteria_axial,$parameters_criteria_axial,'criteriagroup_per_diagnosis_id');
			if($criteriagroup_per_diagnosis_id!='')
				$msg_axial="ok";
			
			$parameters_criteria_caspar[0]=$diagnosis_id;
			$parameters_criteria_caspar[1]=4;
			$parameters_criteria_caspar[2]=$caspar;
			$parameters_criteria_caspar[3]=$user_id;
			$parameters_criteria_caspar[4]=0;
			$table_names_criteria_caspar[0]='diagnosis_id';
			$table_names_criteria_caspar[1]='criteria_id';
			$table_names_criteria_caspar[2]='active';
			$table_names_criteria_caspar[3]='editor_id';
			$table_names_criteria_caspar[4]='deleted';
		
			$criteriagroup_per_diagnosis_id=insert('criteriagroup_per_diagnosis',$table_names_criteria_caspar,$parameters_criteria_caspar,'criteriagroup_per_diagnosis_id');
			if($criteriagroup_per_diagnosis_id!='')
				$msg_caspar="ok";
			
			$parameters_criteria_sle[0]=$diagnosis_id;
			$parameters_criteria_sle[1]=5;
			$parameters_criteria_sle[2]=$sle;
			$parameters_criteria_sle[3]=$user_id;
			$parameters_criteria_sle[4]=0;
			$table_names_criteria_sle[0]='diagnosis_id';
			$table_names_criteria_sle[1]='criteria_id';
			$table_names_criteria_sle[2]='active';
			$table_names_criteria_sle[3]='editor_id';
			$table_names_criteria_sle[4]='deleted';
		
			$criteriagroup_per_diagnosis_id=insert('criteriagroup_per_diagnosis',$table_names_criteria_sle,$parameters_criteria_sle,'criteriagroup_per_diagnosis_id');
			if($criteriagroup_per_diagnosis_id!='')
				$msg_sle="ok";
			
			$parameters_criteria_aps[0]=$diagnosis_id;
			$parameters_criteria_aps[1]=6;
			$parameters_criteria_aps[2]=$aps;
			$parameters_criteria_aps[3]=$user_id;
			$parameters_criteria_aps[4]=0;
			$table_names_criteria_aps[0]='diagnosis_id';
			$table_names_criteria_aps[1]='criteria_id';
			$table_names_criteria_aps[2]='active';
			$table_names_criteria_aps[3]='editor_id';
			$table_names_criteria_aps[4]='deleted';
		
			$criteriagroup_per_diagnosis_id=insert('criteriagroup_per_diagnosis',$table_names_criteria_aps,$parameters_criteria_aps,'criteriagroup_per_diagnosis_id');
			if($criteriagroup_per_diagnosis_id!='')
				$msg_aps="ok";
			
			// criteria - end
			
			//other characteristics - start
			if (isset($_REQUEST['autoantibodies']))
				$autoantibodies="1";
			else
				$autoantibodies="0";
			
			if (isset($_REQUEST['ra']))
				$ra="1";
			else
				$ra="0";
			
			if (isset($_REQUEST['spa']))
				$spa="1";
			else
				$spa="0";
			
			if (isset($_REQUEST['neuropsychiatric']))
				$neuropsychiatric="1";
			else
				$neuropsychiatric="0";
			
			if (isset($_REQUEST['nephritis']))
				$nephritis="1";
			else
				$nephritis="0";
			
			if (isset($_REQUEST['cutaneous']))
				$cutaneous="1";
			else
				$cutaneous="0";
			
			if (isset($_REQUEST['pylmonary']))
				$pylmonary="1";
			else
				$pylmonary="0";
			
			$parameters_othercriteria_deleted[0]=1;
			$table_names_othercriteria_deleted[0]='deleted';
			$edit_name_othercriteria_deleted[0]='diagnosis_id';
			$edit_id_othercriteria_deleted[0]=$diagnosis_id;
			$sumbol_othercriteria_deleted[0]='=';
		
			$msg_deleted2=update('othercriteria_per_diagnosis',$table_names_othercriteria_deleted,$parameters_othercriteria_deleted,$edit_name_othercriteria_deleted,$edit_id_othercriteria_deleted,$sumbol_othercriteria_deleted);
			
			$parameters_criteria_autoantibodies[0]=$diagnosis_id;
			$parameters_criteria_autoantibodies[1]=1;
			$parameters_criteria_autoantibodies[2]=$autoantibodies;
			$parameters_criteria_autoantibodies[3]=$user_id;
			$parameters_criteria_autoantibodies[4]=0;
			$table_names_criteria_autoantibodies[0]='diagnosis_id';
			$table_names_criteria_autoantibodies[1]='othercriteria_id';
			$table_names_criteria_autoantibodies[2]='active';
			$table_names_criteria_autoantibodies[3]='editor_id';
			$table_names_criteria_autoantibodies[4]='deleted';
		
			$othercriteria_per_diagnosis_id	=insert('othercriteria_per_diagnosis',$table_names_criteria_autoantibodies,$parameters_criteria_autoantibodies,'othercriteria_per_diagnosis_id');
			if($othercriteria_per_diagnosis_id!='')
				$msg_autoantibodies="ok";
			
			$parameters_criteria_ra[0]=$diagnosis_id;
			$parameters_criteria_ra[1]=2;
			$parameters_criteria_ra[2]=$ra;
			$parameters_criteria_ra[3]=$user_id;
			$parameters_criteria_ra[4]=0;
			$table_names_criteria_ra[0]='diagnosis_id';
			$table_names_criteria_ra[1]='othercriteria_id';
			$table_names_criteria_ra[2]='active';
			$table_names_criteria_ra[3]='editor_id';
			$table_names_criteria_ra[4]='deleted';
		
			$othercriteria_per_diagnosis_id	=insert('othercriteria_per_diagnosis',$table_names_criteria_ra,$parameters_criteria_ra,'othercriteria_per_diagnosis_id');
			if($othercriteria_per_diagnosis_id!='')
				$msg_ra="ok";
			
			$parameters_criteria_spa[0]=$diagnosis_id;
			$parameters_criteria_spa[1]=3;
			$parameters_criteria_spa[2]=$spa;
			$parameters_criteria_spa[3]=$user_id;
			$parameters_criteria_spa[4]=0;
			$table_names_criteria_spa[0]='diagnosis_id';
			$table_names_criteria_spa[1]='othercriteria_id';
			$table_names_criteria_spa[2]='active';
			$table_names_criteria_spa[3]='editor_id';
			$table_names_criteria_spa[4]='deleted';
		
			$othercriteria_per_diagnosis_id	=insert('othercriteria_per_diagnosis',$table_names_criteria_spa,$parameters_criteria_spa,'othercriteria_per_diagnosis_id');
			if($othercriteria_per_diagnosis_id!='')
				$msg_spa="ok";
			
			$parameters_criteria_neuropsychiatric[0]=$diagnosis_id;
			$parameters_criteria_neuropsychiatric[1]=4;
			$parameters_criteria_neuropsychiatric[2]=$neuropsychiatric;
			$parameters_criteria_neuropsychiatric[3]=$user_id;
			$parameters_criteria_neuropsychiatric[4]=0;
			$table_names_criteria_neuropsychiatric[0]='diagnosis_id';
			$table_names_criteria_neuropsychiatric[1]='othercriteria_id';
			$table_names_criteria_neuropsychiatric[2]='active';
			$table_names_criteria_neuropsychiatric[3]='editor_id';
			$table_names_criteria_neuropsychiatric[4]='deleted';
		
			$othercriteria_per_diagnosis_id	=insert('othercriteria_per_diagnosis',$table_names_criteria_neuropsychiatric,$parameters_criteria_neuropsychiatric,'othercriteria_per_diagnosis_id');
			if($othercriteria_per_diagnosis_id!='')
				$msg_neuropsychiatric="ok";
			
			$parameters_criteria_nephritis[0]=$diagnosis_id;
			$parameters_criteria_nephritis[1]=5;
			$parameters_criteria_nephritis[2]=$nephritis;
			$parameters_criteria_nephritis[3]=$user_id;
			$parameters_criteria_nephritis[4]=0;
			$table_names_criteria_nephritis[0]='diagnosis_id';
			$table_names_criteria_nephritis[1]='othercriteria_id';
			$table_names_criteria_nephritis[2]='active';
			$table_names_criteria_nephritis[3]='editor_id';
			$table_names_criteria_nephritis[4]='deleted';
		
			$othercriteria_per_diagnosis_id	=insert('othercriteria_per_diagnosis',$table_names_criteria_nephritis,$parameters_criteria_nephritis,'othercriteria_per_diagnosis_id');
			if($othercriteria_per_diagnosis_id!='')
				$msg_nephritis="ok";
			
			$parameters_criteria_cutaneous[0]=$diagnosis_id;
			$parameters_criteria_cutaneous[1]=6;
			$parameters_criteria_cutaneous[2]=$cutaneous;
			$parameters_criteria_cutaneous[3]=$user_id;
			$parameters_criteria_cutaneous[4]=0;
			$table_names_criteria_cutaneous[0]='diagnosis_id';
			$table_names_criteria_cutaneous[1]='othercriteria_id';
			$table_names_criteria_cutaneous[2]='active';
			$table_names_criteria_cutaneous[3]='editor_id';
			$table_names_criteria_cutaneous[4]='deleted';
		
			$othercriteria_per_diagnosis_id	=insert('othercriteria_per_diagnosis',$table_names_criteria_cutaneous,$parameters_criteria_cutaneous,'othercriteria_per_diagnosis_id');
			if($othercriteria_per_diagnosis_id!='')
				$msg_cutaneous="ok";
			
			$parameters_criteria_pylmonary[0]=$diagnosis_id;
			$parameters_criteria_pylmonary[1]=7;
			$parameters_criteria_pylmonary[2]=$pylmonary;
			$parameters_criteria_pylmonary[3]=$user_id;
			$parameters_criteria_pylmonary[4]=0;
			$table_names_criteria_pylmonary[0]='diagnosis_id';
			$table_names_criteria_pylmonary[1]='othercriteria_id';
			$table_names_criteria_pylmonary[2]='active';
			$table_names_criteria_pylmonary[3]='editor_id';
			$table_names_criteria_pylmonary[4]='deleted';
		
			$othercriteria_per_diagnosis_id	=insert('othercriteria_per_diagnosis',$table_names_criteria_pylmonary,$parameters_criteria_pylmonary,'othercriteria_per_diagnosis_id');
			if($othercriteria_per_diagnosis_id!='')
				$msg_pylmonary="ok";
		
			//other characteristics - end
		
			if($msg_start=="ok" and $msg_deleted=="ok" and $msg_deleted2=="ok" and $msg_pa=="ok" and $msg_peripheral=="ok" and $msg_axial=="ok" and $msg_caspar=="ok" and $msg_sle=="ok" and $msg_aps=="ok"   and $msg_autoantibodies=="ok" and $msg_ra=="ok" and $msg_spa=="ok" and $msg_neuropsychiatric=="ok" and $msg_nephritis=="ok" and $msg_cutaneous=="ok" and $msg_pylmonary=="ok")
			{	
				$msg="ok";
				pg_query("COMMIT") or die("Transaction commit failed\n");
			}
			else
			{
				$msg="nok";
				pg_query("ROLLBACK") or die("Transaction rollback failed\n");
			}
		}
		catch(exception $e) {
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
		
	}
?>
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
	  <?php
		$exec = get_diagnosi($diagnosis_id);
		$result = pg_fetch_array($exec);
		$diagnosis_id=$result['diagnosis_id'];
		$value=$result['value'];
		$code=$result['code'];
		$icd10=$result['icd10'];
		$criteria=$result['criteria'];
		$invisible=$result['invisible'];
		
	?>
      <section class="content-header">
	  <h1>
          <small>
			<?php if($diagnosis_id=='') { echo 'Add'; } else { echo 'Edit'; } ?> Diagnosis: <?php echo $value; ?>
			</small>
        </h1>
	  </section>
			<br>
			 <!-- Main content -->
			<section class="content">
			<form id="form" action="diagnosis_edit.php?diagnosis_id=<?php echo $diagnosis_id; ?>" method="POST">
			<br>
			<input type="hidden" id="save" name="save" value="1">
			<input type="hidden" id="diagnosis_id" name="diagnosis_id" value="<?php echo $diagnosis_id;?>">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<!--<div class="box-header with-border">
							<h3 class="box-title"></h3>
						</div>-->
						<div class="box-body">
							<div class="row">
								<div class="form-group col-md-6" id="divname" name="divname">
								  <label>*value</label>
								  <input type="text" class="form-control" placeholder="value ..." value="<?php echo $value; ?>" id="value" name="value">
								  <label class="control-label" for="inputError" id="Errorname" name="Errorname" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6" id="divname" name="divname">
								  <label>*code</label>
								  <input type="text" class="form-control" placeholder="code ..." value="<?php echo $code; ?>" id="code" name="code">
								  <label class="control-label" for="inputError" id="Errorname" name="Errorname" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6" id="divname" name="divname">
								  <label>ICD-10 code</label>
								  <input type="text" class="form-control" placeholder="ICD-10 code ..." value="<?php echo $icd10; ?>" id="icd10" name="icd10">
								  <label class="control-label" for="inputError" id="Errorname" name="Errorname" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6" id="divname" name="divname">
								  <label>Invisible</label>
								 <input type="checkbox" id="invisible" name="invisible" <?php if($invisible==1) { echo "checked"; } ?>>
								</div>
							</div>
							<?php
							$exec = get_criteria($diagnosis_id,1);
							$result = pg_fetch_array($exec);
							$pa=$result['active'];
							
							$exec = get_criteria($diagnosis_id,2);
							$result = pg_fetch_array($exec);
							$peripheral=$result['active'];
							
							$exec = get_criteria($diagnosis_id,3);
							$result = pg_fetch_array($exec);
							$axial=$result['active'];
							
							$exec = get_criteria($diagnosis_id,4);
							$result = pg_fetch_array($exec);
							$caspar=$result['active'];
							
							$exec = get_criteria($diagnosis_id,5);
							$result = pg_fetch_array($exec);
							$sle=$result['active'];
							
							$exec = get_criteria($diagnosis_id,6);
							$result = pg_fetch_array($exec);
							$aps=$result['active'];
							
							?>
							<div class="row">
								<div class="form-group col-md-4">
								  <label>Criteria</label>
									<div class="checkbox">
									<label>
									  <input type="checkbox" id="pa" name="pa" <?php if($pa==1) { echo "checked"; } ?>>
										RΑ (ACR/EULAR 2010 RA Classification Criteria)
									</label>
								  </div>
								</div>
								<div class="form-group col-md-4">
								  <label>&nbsp;</label>
									<div class="checkbox">
									<label>
									  <input type="checkbox" id="peripheral" name="peripheral" <?php if($peripheral==1) { echo "checked"; } ?>>
										Questionnaire for Peripheral SpA Criteria
									</label>
								  </div>
								</div>
								<div class="form-group col-md-4">
								  <label>&nbsp;</label>
									<div class="checkbox">
									<label>
									  <input type="checkbox" id="axial" name="axial" <?php if($axial==1) { echo "checked"; } ?>>
										Questionnaire for Axial SpA Criteria
									</label>
								  </div>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-4">
								  <label>&nbsp;</label>
									<div class="checkbox">
									<label>
									  <input type="checkbox" id="caspar" name="caspar" <?php if($caspar==1) { echo "checked"; } ?>>
										CASPAR Criteria for Psoriatic Arthritis
									</label>
								  </div>
								</div>
								<div class="form-group col-md-4">
								  <label>&nbsp;</label>
									<div class="checkbox">
									<label>
									  <input type="checkbox" id="sle" name="sle" <?php if($sle==1) { echo "checked"; } ?>>
										SLE criteria (1997 ACR/SLICC/EULAR-ACR)
									</label>
								  </div>
								</div>
								<div class="form-group col-md-4">
								  <label>&nbsp;</label>
									<div class="checkbox">
									<label>
									  <input type="checkbox" id="aps" name="aps" <?php if($aps==1) { echo "checked"; } ?>>
										APS criteria
									</label>
								  </div>
								</div>
							</div>
							<?php
							$exec = get_othercriteria($diagnosis_id,1);
							$result = pg_fetch_array($exec);
							$autoantibodies=$result['active'];
							
							$exec = get_othercriteria($diagnosis_id,2);
							$result = pg_fetch_array($exec);
							$ra=$result['active'];
							
							$exec = get_othercriteria($diagnosis_id,3);
							$result = pg_fetch_array($exec);
							$spa=$result['active'];
							
							$exec = get_othercriteria($diagnosis_id,4);
							$result = pg_fetch_array($exec);
							$neuropsychiatric=$result['active'];
							
							$exec = get_othercriteria($diagnosis_id,5);
							$result = pg_fetch_array($exec);
							$nephritis=$result['active'];
							
							$exec = get_othercriteria($diagnosis_id,6);
							$result = pg_fetch_array($exec);
							$cutaneous=$result['active'];
							
							$exec = get_othercriteria($diagnosis_id,7);
							$result = pg_fetch_array($exec);
							$pylmonary=$result['active'];
							?>
							<div class="row">
								<div class="form-group col-md-3">
								  <label>Other disease characteristics</label>
									<div class="checkbox">
									<label>
									  <input type="checkbox" id="autoantibodies" name="autoantibodies" <?php if($autoantibodies==1) { echo "checked"; } ?>>
										Autoantibodies
									</label>
								  </div>
								</div>
								<div class="form-group col-md-3">
								  <label>&nbsp;</label>
									<div class="checkbox">
									<label>
									  <input type="checkbox" id="ra" name="ra" <?php if($ra==1) { echo "checked"; } ?>>
										RA extraarticular manifestations
									</label>
								  </div>
								</div>
								<div class="form-group col-md-3">
								  <label>&nbsp;</label>
									<div class="checkbox">
									<label>
									  <input type="checkbox" id="spa" name="spa" <?php if($spa==1) { echo "checked"; } ?>>
										SpA extraarticular manifestations
									</label>
								  </div>
								</div>
								<div class="form-group col-md-3">
								  <label>&nbsp;</label>
									<div class="checkbox">
									<label>
									  <input type="checkbox" id="neuropsychiatric" name="neuropsychiatric" <?php if($neuropsychiatric==1) { echo "checked"; } ?>>
										Neuropsychiatric SLE
									</label>
								  </div>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-3">
								  <label>&nbsp;</label>
									<div class="checkbox">
									<label>
									  <input type="checkbox" id="nephritis" name="nephritis" <?php if($nephritis==1) { echo "checked"; } ?>>
										Lupus nephritis
									</label>
								  </div>
								</div>
								<div class="form-group col-md-3">
								  <label>&nbsp;</label>
									<div class="checkbox">
									<label>
									  <input type="checkbox" id="cutaneous" name="cutaneous" <?php if($cutaneous==1) { echo "checked"; } ?>>
										Cutaneous lupus
									</label>
								  </div>
								</div>
								<div class="form-group col-md-3">
								  <label>&nbsp;</label>
									<div class="checkbox">
									<label>
									  <input type="checkbox" id="pylmonary" name="pylmonary" <?php if($pylmonary==1) { echo "checked"; } ?>>
										Pylmonary involvement
									</label>
								  </div>
								</div>
								<div class="form-group col-md-3">
								  <label>&nbsp;</label>
									<div class="checkbox">
									<label>
										&nbsp;
									</label>
								  </div>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6">
								  <input type="submit" class="btn btn-primary" value="Save">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</form>
		</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>
  $(function () {
	  
	   $('#form').submit(function(){
		
		var msg="";
		 
		if($('#value').val()=='')
			msg +='-Fill field Value!\n'; 
		if($('#code').val()=='')
			msg +='-Fill field code!\n'; 
		 	 
			
		if(msg != '')
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
		
		
	  });	
</script>
<?php

if($save==1)
{
	if($msg=="ok")
	{
?>
<script>
alert("Successful save!");
</script>
<?php
	}
	else
	{
?>
<script>
alert("Unsuccessful save!");
</script>
<?php
	}
}
?>
</body>
</html>