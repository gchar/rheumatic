﻿<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <small>Dynamic lists</small>
        </h1>
      </section>
			 <!-- Main content -->
			<section class="content">
			<div class="box">
            <!-- /.box-header -->
            <div class="box-body">
			<a class="btn btn-primary" href="lists_edit.php" target="_self"><i class="fa fa-plus"></i> Add list</a>
			<br><br>
			<?php
				$exec = get_lookup_tables();
				$num_rows = pg_num_rows($exec);
				if ($num_rows=='0')
				{
				?>
				<tr align=center><td colspan=3>
				<?php
					echo "No records!!!";
				?>
				</td></tr>
				</table>
				<br>
				<?php
				}
				else
				{
				?>
			<div class="row">
			  <div class="col-md-8">
				<table id="table_jq" class="table table-bordered table-striped">
					<thead>
						<tr class="gradeC">
							<th>Code</th>
							<th>Descriptions</th>
							<th>Values</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=0;
						while($result = pg_fetch_array($exec))
						{
							
							$id=$result['id'];
							$code=$result['code'];
							$description=$result['description'];
								$i++;
					?>
						<tr class="gradeA" align="center" >
							<td>&nbsp;<?php echo $code; ?></td>
							<td>&nbsp;<?php echo $description; ?></td>
							<td>
								<a title="Values" href="list.php?list_code=<?php echo $code; ?>" target="_self"><i class="fa fa-plus-square-o"></i><span></a>
							</td>
							<td>
								<a title="Edit" href="lists_edit.php?list_id=<?php echo $id; ?>" target="_self"><i class="fa fa-edit"></i><span></a>
							</td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table> <?php
				}
				?>
            </div>
			</div>
       <!-- /.box-body -->
       </div>
       <!-- /.box -->
		</div>
	 </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#table_jq').dataTable({});
	});
</script>
</body>
</html>