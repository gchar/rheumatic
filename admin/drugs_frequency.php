﻿<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <small>Drugs Frequencies</small>
        </h1>
      </section>
			 <!-- Main content -->
			<section class="content">
			<div class="box">
            <!-- /.box-header -->
            <div class="box-body">
			<a class="btn btn-primary" href="drugs_frequency_edit.php" target="_self"><i class="fa fa-plus"></i> New drug frequency</a>
			<br><br>
			<?php
				$exec = get_drugs_frequencies();
				$num_rows = pg_num_rows($exec);
				if ($num_rows=='0')
				{
				?>
				<tr align=center><td colspan=3>
				<?php
					echo "No records!!!";
				?>
				</td></tr>
				</table>
				<br>
				<?php
				}
				else
				{
				?>
			<div class="row">
			  <div class="col-md-8">
				<table id="table_jq" class="table table-bordered table-striped">
					<thead>
						<tr class="gradeC">
							<th>Frequence</th>
							<th>mg Per week</th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$i=0;
							while($result = pg_fetch_array($exec))
							{
								
								$drugs_frequency_id=$result['drugs_frequency_id'];
								$value=$result['value'];
								$explain=$result['explain'];
								
								$i++;
					?>
						<tr class="gradeA" align="center" >
							<td>&nbsp;<?php echo $value; ?></td>
							<td>&nbsp;<?php echo $explain; ?></td>
							<td>
								<a title="edit" href="drugs_frequency_edit.php?drugs_frequency_id=<?php echo $drugs_frequency_id; ?>" target="_self"><i class="fa fa-edit"></i><span></a>
							</td>
							<td>
								<a onclick="setdeleteid('drugs_frequency','drugs_frequency_id',<?php echo $drugs_frequency_id; ?>)" title="delete" href="#" ><i class="fa fa-trash"></i><span></a>
							</td>
						</tr>
						<?php
							}
						?>
					</tbody>
				</table> <?php
				}
				?>
            </div>
			</div>
       <!-- /.box-body -->
       </div>
       <!-- /.box -->
		</div>
		
	
			 </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script type="text/javascript" charset="utf-8">
	$(document).ready(function() {
		$('#table_jq').dataTable({
			 "aaSorting": [[0,'asc']]
		});
	});
</script>
</body>
</html>