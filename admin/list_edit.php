﻿<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	
	$id=$_REQUEST['id'];
	$parent_code=$_REQUEST['parent_code'];
	$list_code=$_REQUEST['list_code'];
	$save=$_REQUEST['save'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>
</head>
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<?php
	if($save==1)
	{
		$parameters[0]=str_replace("'", "''",$_REQUEST['value']);
		$parameters[1]=0;
		$parameters[2]=$list_code;
		if($parent_code<>"")
			$parameters[3]=$_REQUEST['parent_value_id'];
		$table_names[0]='value';
		$table_names[1]='deleted';
		$table_names[2]='code';
		if($parent_code<>"")
			$table_names[3]='parent_value_id';
		$edit_name[0]='id';
		$edit_id[0]=$id;
		$sumbol[0]='=';
		
		pg_query("BEGIN") or die("Could not start transaction\n");
		try
		{
			if($id=='')
			{
				$id=insert('lookup_tbl_val',$table_names,$parameters,'id');
				if($id!='')
					$msg="ok";
			}
			else
			{
				$msg=update('lookup_tbl_val',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
			}
			
			if ($list_code=="autoantibodies"){
			
				$exec2 = get_explanation($id);
				$result2 = pg_fetch_array($exec2);
				$autoantibodies_explanation_id=$result2['autoantibodies_explanation_id'];
				
				$parameters2[0]=str_replace("'", "''",$_REQUEST['explanation']);
				$parameters2[1]=0;
				$parameters2[2]=$id;
				$table_names2[0]='explanation';
				$table_names2[1]='deleted';
				$table_names2[2]='lookup_tbl_val_id';
				$edit_name2[0]='autoantibodies_explanation_id';
				$edit_id2[0]=$autoantibodies_explanation_id;
				$sumbol2[0]='=';
		
				if($autoantibodies_explanation_id==''){
					$autoantibodies_explanation_id=insert('autoantibodies_explanation',$table_names2,$parameters2,'autoantibodies_explanation_id');
					if($autoantibodies_explanation_id!='')
						$msg2="ok";
				}
				else {
					$msg2=update('autoantibodies_explanation',$table_names2,$parameters2,$edit_name2,$edit_id2,$sumbol2);
				}
			}
				
			pg_query("COMMIT") or die("Transaction commit failed\n");
		}
		catch(exception $e) {
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
		
	}
?>
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
	  <?php
		$exec = get_list($list_code);
		$result = pg_fetch_array($exec);
		$parent_code=$result['parent_code'];
		$description=$result['description'];
		
	?>
      <section class="content-header">
	  <h1>
          <small>
			<?php if($id=='') { echo 'Add'; } else { echo 'Edit'; } ?> value of list: <?php echo $description; ?>
			</small>
        </h1>
	  </section>
			<br>
			 <!-- Main content -->
			<section class="content">
			 <div class="alert alert_suc alert-success" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Successful save!</strong>
			  </div>
			  <div class="alert alert_wr alert-danger" role="alert" style="DISPLAY:none;">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>Unsuccessful save!</strong>
			  </div>
			<form id="form" action="list_edit.php?list_code=<?php echo $list_code; ?>" method="POST">
			<br>
			<?php
				if($id!="")
				{
					$exec = get_lookup_tbl_value($id);
					
					$result = pg_fetch_array($exec);
					$parent_value_id=$result['parent_value_id'];
					$value=$result['value'];
				}
				?>
			<input type="hidden" id="save" name="save" value="1">
			<input type="hidden" id="id" name="id" value="<?php echo $id;?>">
			<input type="hidden" id="parent_code" name="parent_code" value="<?php echo $parent_code;?>">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<!--<div class="box-header with-border">
							<h3 class="box-title"></h3>
						</div>-->
						<div class="box-body">
							<?php
							if($parent_code<>"")
							{
							?>
							<div class="row">
								<!--add has-error in class-->
								<div class="form-group col-md-6" id="divsurname" name="divsurname">
								  <label>Parent value</label>
									<select class="form-control"  id="parent_value_id" name="parent_value_id">
										<option value="0" <?php if($parent_value_id=="") { echo "selected"; } ?>>--</option>
											<?php
												$query2="select id,value from lookup_tbl_val  where deleted=0 and code='$parent_code' order by value asc ";
												
												$exec2 = pg_query($query2);
												
												while($result2 = pg_fetch_array($exec2))
												{
													$id=$result2['id'];
													$parent_value =$result2['value'];
											?>
													<option value="<?php echo $id; ?>" <?php if($parent_value_id==$id) { echo "selected"; } ?>><?php echo $parent_value; ?></option>
											<?php
												}
											?>
									</select>
								  <label class="control-label" for="inputError" id="Errorsurname" name="Errorsurname" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
								</div>
							</div>
							<?php
							}
							?>
							<div class="row">
								<div class="form-group col-md-6" id="divname" name="divname">
								  <label>*value</label>
								  <input type="text" class="form-control" placeholder="value ..." value="<?php echo $value; ?>" id="value" name="value">
								  <label class="control-label" for="inputError" id="Errorname" name="Errorname" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
								</div>
							</div>
							<?php
							if ($list_code=="autoantibodies"){
								
								$exec2 = get_explanation($id);
								$result2 = pg_fetch_array($exec2);
								$explanation=$result2['explanation'];
							?>
							<div class="row">
								<div class="form-group col-md-6" id="divexplanation" name="divexplanation">
								  <label>Options explanation</label>
								  <textarea class="form-control" id="explanation" name="explanation"><?php echo $explanation; ?></textarea>
								</div>
							</div>
							<?php } ?>
							<div class="row">
								<div class="form-group col-md-6">
								  <input type="submit" class="btn btn-primary" value="Save">
								  <a href="list.php?list_code=<?php echo $list_code; ?>"  class="btn btn-primary">Back</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</form>
		</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>
  $(function () {
	  
	   $('#form').submit(function(){
		
		var msg="";
		 
		if($('#description').val()=='')
			msg +='-Fill field Value!\n'; 
		 	 
			
		if(msg != '')
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
		
		
	  });	
</script>
<?php
	
if($save==1)
{
	if($msg=="ok")
	{
?>
<script>
$(".alert_suc").show();
window.setTimeout(function() {
    $(".alert_suc").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
window.location = "list_edit.php?list_code=<?php echo $list_code; ?>";
</script>
<?php
	}
	else
	{
?>
<script>
$(".alert_wr").show();
window.setTimeout(function() {
    $(".alert_wr").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
}
?>
</body>
</html>
