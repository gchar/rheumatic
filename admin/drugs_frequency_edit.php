﻿<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	$save=$_REQUEST['save'];
	$drugs_frequency_id=$_REQUEST['drugs_frequency_id'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>
</head>
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<?php
	if($save==1)
	{
		$parameters[0]=str_replace("'", "''",$_REQUEST['value']);
		$parameters[1]=str_replace("'", "''",$_REQUEST['explain']);
		$parameters[2]=0;
		$parameters[3]=str_replace("'", "''",$_REQUEST['ordering']);
		$table_names[0]='value';
		$table_names[1]='explain';
		$table_names[2]='deleted';
		$table_names[3]='ordering';
		$edit_name[0]='drugs_frequency_id';
		$edit_id[0]=$drugs_frequency_id;
		$sumbol[0]='=';
		
		pg_query("BEGIN") or die("Could not start transaction\n");
		try
		{
			if($drugs_frequency_id=='')
			{
				$drugs_frequency_id=insert('drugs_frequency',$table_names,$parameters,'drugs_frequency_id');
				if($drugs_frequency_id!='')
					$msg="ok";
			}
			else
			{
				$msg=update('drugs_frequency',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
				 
			}
				
			
			if($msg=="ok")
				pg_query("COMMIT") or die("Transaction commit failed\n");
			else
				pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		catch(exception $e) {
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
		
	}
?>
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
	  <?php
		$exec = get_drugs_frequency($drugs_frequency_id);
		$result = pg_fetch_array($exec);
		$value=$result['value'];
		$explain=$result['explain'];
		$ordering=$result['ordering'];
		
	?>
      <section class="content-header">
	  <h1>
          <small>
			<?php if($drugs_frequency_id=='') { echo 'Add'; } else { echo 'Edit'; } ?> Drug frequency
			</small>
        </h1>
	  </section>
			<br>
			 <!-- Main content -->
			<section class="content">
			<form id="form" action="drugs_frequency_edit.php" method="POST">
			<br>
			<input type="hidden" id="save" name="save" value="1">
			<input type="hidden" id="drugs_frequency_id" name="drugs_frequency_id" value="<?php echo $drugs_frequency_id;?>">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<!--<div class="box-header with-border">
							<h3 class="box-title"></h3>
						</div>-->
						<div class="box-body">
							<div class="row">
								<div class="form-group col-md-6" id="divname" name="divname">
								  <label>*value</label>
								  <input type="text" class="form-control" placeholder="value ..." value="<?php echo $value; ?>" id="value" name="value">
								  <label class="control-label" for="inputError" id="Errorname" name="Errorname" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6" id="divname" name="divname">
								  <label>*Ordering</label>
								  <input type="text" class="form-control" placeholder="Ordering (only integer) ..." value="<?php echo $ordering; ?>" id="ordering" name="ordering">
								  <label class="control-label" for="inputError" id="Errorname" name="Errorname" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6" id="divname" name="divname">
								  <label>*Transform to mg per week</label>
								  <input type="text" class="form-control" placeholder="Transform mg/per week ..." value="<?php echo $explain; ?>" id="explain" name="explain">
								  <label class="control-label" for="inputError" id="Errorname" name="Errorname" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-6">
								  <input type="submit" class="btn btn-primary" value="Save">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			</form>
		</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>
  $(function () {
	  
	   $('#form').submit(function(){
		
		var msg="";
		 
		if($('#value').val()=='')
			msg +='-Fill field value!\n'; 
		if($('#ordering').val()=='')
			msg +='-Fill field ordering!\n'; 
		
		if($('#explain').val()=='')
			msg +='-Fill field Transform mg/per week!\n'; 
		 	 
			
		if(msg != '')
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
		
		
	  });	
</script>
<?php

if($save==1)
{
	if($msg=="ok")
	{
?>
<script>
alert("Successful save!");
</script>
<?php
	}
	else
	{
?>
<script>
alert("Unsuccessful save!");
</script>
<?php
	}
}
?>
</body>
</html>