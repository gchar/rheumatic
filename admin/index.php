<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <small>Users</small>
        </h1>
      </section>

      <!-- Main content -->
      <section class="content">
		
      <!-- Your Page Content Here -->
		<div class="box">
            <!-- /.box-header -->
            <div class="box-body">
			<a class="btn btn-primary" href="user_edit.php" target="_self"><i class="fa fa-plus"></i> Add user</a>
			<br><br>
			<?php
				$exec = get_users();
				$num_rows = pg_num_rows($exec);
				if ($num_rows=='0')
				{
				?>
				<tr align=center><td colspan=3>
				<?php
					echo "No records!!!";
				?>
				</td></tr>
				</table>
				<br>
				<?php
				}
				else
				{
				?>
              <table id="users" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Name</th>
				  <th>Centre</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>Telephone</th>
				  <th>Active</th>
                  <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php
					$i=0;
					while($result = pg_fetch_array($exec))
					{
						$user_id2=$result['user_id'];
						$name=$result['name'];
						$surname=$result['surname'];
						$username=$result['username'];
						$mail=$result['mail'];
						$telephone=$result['telephone'];
						$deleted=$result['deleted'];
						$centre_val=$result['centre_val'];
						$i++;
				?>
					<tr class="gradeA" align="center" >
						<td>&nbsp;<?php echo $surname.' '.$name; ?></td>
						<td>&nbsp;<?php echo $centre_val;?></td>
						<td>&nbsp;<?php echo $username;?></td>
						<td>&nbsp;<?php echo $mail;?></td>
						<td>&nbsp;<?php echo $telephone;?></td>
						<td>&nbsp;<?php if ($deleted==0) { echo 'Yes'; } else { echo 'No'; } ?></td>
						<td>
							<a title="edit" href="user_edit.php?user_id2=<?php echo $user_id2; ?>" target="_self"><i class="fa fa-edit"></i><span></a>
						</td>
					</tr>
				<?php
					}
				?>
				</tbody>
              </table>
			  <?php
				}
				?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>
  $(function () {
    $('#users').DataTable()
  })
</script>
</body>
</html>
