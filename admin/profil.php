﻿<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	
	$save=$_REQUEST['save'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php
	if($save==1)
	{
		$sql = get_username(str_replace("'", "''",$_REQUEST['username']),$user_id);
		$result = pg_fetch_array($sql);
		$numrows = pg_num_rows($sql);
			
		$sql2 = get_username2(str_replace("'", "''",$_REQUEST['mail']),$user_id);
		$result2 = pg_fetch_array($sql2);
		$numrows2 = pg_num_rows($sql2);
		
		if($numrows > 0)
		{
			if($numrows2 > 0)
			{
				?>
				<script>
					alert("Υπάρχει ήδη χρήστης με username : <?php echo str_replace("'", "''",$_REQUEST['username']); ?> και mail: <?php echo str_replace("'", "''",$_REQUEST['mail']); ?> ! Παρακαλούμε επιλέξτε άλλο!");
				</script>
				<?php
			}
			else
			{
				?>
				<script>
					alert("Υπάρχει ήδη χρήστης με username : <?php echo str_replace("'", "''",$_REQUEST['username']); ?>! Παρακαλούμε επιλέξτε άλλο!");
				</script>
				<?php
			}
			
		}
		else if($numrows2 > 0)
		{
			?>
			<script>
				alert("Υπάρχει ήδη χρήστης με mail: <?php echo str_replace("'", "''",$_REQUEST['mail']); ?> ! Παρακαλούμε επιλέξτε άλλο!");
			</script>
			<?php
		}
		else
		{
			$parameters[0]=str_replace("'", "''",$_REQUEST['mail']);
			$parameters[1]=str_replace("'", "''",$_REQUEST['username']);
			if($_REQUEST['password']<>"")
				$ps=str_replace("'", "''",$_REQUEST['password']);
			
			$query = "update users set mail='$parameters[0]',username='$parameters[1]' ";
			if($_REQUEST['password']<>"")
				$query .= ",password=md5('".$ps."')";
			$query .= " where user_id=$user_id";
			
				$result = pg_query($query);
				if($result)
					$msg="ok";
		}
			
		
	}
?>
<div class="wrapper">
  <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php
  include "../portion/sidebar.php";
  ?>

	 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <!-- Content Header (Page header) -->
    <section class="content-header">
      <h4><b>Επεξεργασία προφίλ</b></h4>
	  </section>
			<br>
			 <!-- Main content -->
			<section class="content">
			<form  id="form" action="profil.php" method="POST">
			<br>
			<?php
				if($user_id!="")
				{
					$exec = get_user($user_id);
					$result = pg_fetch_array($exec);
					
					$mail=$result['mail'];
					$username=$result['username'];
				}
				?>
			<input type="hidden" id="save" name="save" value="1">
			<input type="hidden" id="user_id" name="user_id" value="<?php echo $user_id;?>">
			<div class="row">
			  <div class="col-md-6">
				<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-striped dataTable" role="grid" id="table_jq"  width="40%"  rules="all">
					<tbody>
						<tr class="gradeA odd" align="left" >
							<td>Username:</td>
							<td><input class="form-control" type="text" id="username" name="username" value="<?php echo $username;?>"></td>
						</tr>
						<tr class="gradeA odd" align="left" >
							<td>Email:</td>
							<td><input class="form-control"  type="text" id="mail" name="mail" value="<?php echo $mail;?>"></td>
						</tr>
						<tr class="gradeA odd" align="left" >
							<td>Password:</td>
							<td><input class="form-control" type="password" id="password" name="password" value="<?php echo $password;?>"></td>
						</tr>
						<tr class="gradeA odd" align="left" >
							<td>Confirm password:</td>
							<td><input class="form-control" type="password" id="password2" name="password2" value="<?php echo $password;?>" onKeyUp="checkPass(); return false;"><span id="confirmMessage" class="confirmMessage"></spa</td>
						</tr>
						<tr class="gradeA odd" align="center" <?php if($user_id=='') { ?> style="DISPLAY:none;" <?php } ?>>
						<td colspan=2>*Αν δεν θέλετε να αλλάξετε το password αφήστε το κενό αλλιώς προσθέστε το password που επιθυμείτε</td>
						</tr>
						<tr class="gradeA even" align="center" >
							<td colspan=2>
								<input type="submit" class="btn btn-primary" value="Αποθήκευση">
							</td>
						</tr>
						
					</tbody>
				</table>
				</div>
				</div>
				</form>
			 </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>
function checkPass()
{
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('password');
    var pass2 = document.getElementById('password2');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field 
    //and the confirmation field
	if(pass1.value=="")
	{
		message.innerHTML="";
		 pass2.style.backgroundColor="#ffffff";
	}
	else
	{
		if(pass1.value == pass2.value){
        //The passwords match. 
        //Set the color to the good color and inform
        //the user that they have entered the correct password 
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColor;
        message.innerHTML = "Τα password ταιριάζουν!"
		}else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Τα password δεν ταιριάζουν!"
		}
	}
    
}  

  $(function () {
	  
	  $('#form').submit(function(){
		
		var msg="";
		 
		 if($('#username').val()=='')
			msg +='-Πρέπει να εισάγετε όνομα χρήστη!\n'; 
		
		if($('#password').val()=='' && $('#user_id').val()=='')
			msg +='-Πρέπει να εισάγετε κωδικό!\n'; 
		 
			
		if(msg != '')
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
		
	  });	
</script>
<?php

if($save==1)
{
	if($msg=="ok")
	{
?>
<script>
alert("Η απόθηκευση πραγματοποιήθηκε με επιτυχία!");
</script>
<?php
	}
	else
	{
?>
<script>
alert("Η απόθηκευση ΔΕΝ πραγματοποιήθηκε με επιτυχία!");
</script>
<?php
	}
}
?>
</body>
</html>