﻿<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	
	$list_id=$_REQUEST['list_id'];
	$save=$_REQUEST['save'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>
</head>
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<?php
	if($save==1)
	{
		$parameters[0]=str_replace("'", "''",$_REQUEST['code']);
		$parameters[1]=str_replace("'", "''",$_REQUEST['description']);
		$parameters[2]=0;
		$parameters[3]=$_REQUEST['parent_code'];
		$table_names[0]='code';
		$table_names[1]='description';
		$table_names[2]='deleted';
		$table_names[3]='parent_code';
		$edit_name[0]='id';
		$edit_id[0]=$list_id;
		$sumbol[0]='=';
		
					
		if($list_id=='')
		{
			$list_id=insert('lookup_tbl',$table_names,$parameters,'id');
			if($list_id<>'')
			{
				$msg="ok";
				
			}
		}
		else
		{
			$msg=update('lookup_tbl',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
			 
		}
		
		
	}
?>
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
		<h1>
          <small>
			<?php if($list_id=='') { echo 'New'; } else { echo 'Edit'; } ?> dynamic list
			</small>
        </h1>
	  </section>
			 <!-- Main content -->
			<section class="content">
			<form id="form" action="lists_edit.php" method="POST">
			<?php
				if($list_id!="")
				{
					$query="select * from lookup_tbl where id=$list_id";
					
					$exec = get_lookup_table($list_id);
					$result = pg_fetch_array($exec);
					
					$code=$result['code'];
					$description=$result['description'];
					$parent_code=$result['parent_code'];
				}
				?>
			<input type="hidden" id="save" name="save" value="1">
			<input type="hidden" id="list_id" name="list_id" value="<?php echo $list_id;?>">
			<div class="row">
				<div class="col-md-12">
					<div class="box box-primary">
						<!--<div class="box-header with-border">
							<h3 class="box-title"></h3>
						</div>-->
						<div class="box-body">
							<div class="row">
								<!--add has-error in class-->
								<div class="form-group col-md-6" id="divsurname" name="divsurname">
								  <label>Parent list (if available)</label>
								  <select class="form-control"  id="parent_code" name="parent_code">
									<option value="" <?php if($parent_code=="") { echo "selected"; } ?>>--</option>
										<?php
											$query2="select code,description from lookup_tbl  where deleted=0 order by description asc ";
											
											$exec2 = pg_query($query2);
											
											while($result2 = pg_fetch_array($exec2))
											{
												$code2=$result2['code'];
												$description2 =$result2['description'];
										?>
												<option value="<?php echo $code2; ?>" <?php if($parent_code==$code2) { echo "selected"; } ?>><?php echo $description2; ?></option>
										<?php
											}
										?>
								</select>
								  <label class="control-label" for="inputError" id="Errorsurname" name="Errorsurname" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
								</div>
								<div class="form-group col-md-6" id="divname" name="divname">
								  <label>*Unique code (Latin characters only)</label>
								  <input type="text" class="form-control" placeholder="code ..." value="<?php echo $code; ?>" id="code" name="code">
								  <label class="control-label" for="inputError" id="Errorname" name="Errorname" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
								</div>
								</div>
								<div class="row">
								<div class="form-group col-md-6" id="divname" name="divname">
								  <label>*description</label>
								  <input type="text" class="form-control" placeholder="description ..." value="<?php echo $description; ?>" id="description" name="description">
								  <label class="control-label" for="inputError" id="Errorname" name="Errorname" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field!</label>
								</div>
								</div>
								<div class="row">
									<div class="form-group col-md-6">
									  <input type="submit" class="btn btn-primary" value="Save">
									</div>
								</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>
  $(function () {
	  
	   $('#form').submit(function(){
		
		var msg="";
		 
		 if($('#code').val()=='')
			msg +='-Fill field code!\n'; 
		
		if($('#description').val()=='')
			msg +='-Fill field Description!\n'; 
		 	 
			
		if(msg != '')
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
		
		
	  });	
</script>
<?php

if($save==1)
{
	if($msg=="ok")
	{
?>
<script>
alert("Successful save!");
</script>
<?php
	}
	else
	{
?>
<script>
alert("Unsuccessful save!");
</script>
<?php
	}
}
?>
</body>
</html>