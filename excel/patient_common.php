<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php 
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

include '../library/config.php';
include '../library/openDB.php';

require_once 'excel_reader.php';


 
$data = new Spreadsheet_Excel_Reader("patient_biobanking.xls");
 
echo "Total Sheets in this xls file: ".count($data->sheets)."<br /><br />";
 
//$html="<table border='1'>";

$tr=0;
for($i=0;$i<count($data->sheets);$i++) // Loop to get all sheets in a file.
{	
	if(count($data->sheets[$i][cells])>0) // checking sheet not empty
	{
		echo "Sheet $i:<br /><br />Total rows in sheet $i  ".count($data->sheets[$i][cells])."<br />";
		
		for($j=1;$j<=count($data->sheets[$i][cells]);$j++) // loop used to get each row of the sheet
		{ 
		
			//$html .="<tr>";
			$pat_id="";
			$deleted="";
			$editor_id="";
			$biobanking_sample_id="";
			$biobanking_date="";
			$availability="";
			$protocol_descr="";
			$codenbr="";
			
			for($k=1;$k<=count($data->sheets[$i][cells][$j]);$k++) // This loop is created to get data in a table format.
			{
				if ($k==1)
					$pat_id=$data->sheets[$i][cells][$j][$k];
				elseif ($k==2)
					$deleted=$data->sheets[$i][cells][$j][$k];
				elseif ($k==3)
					$editor_id=$data->sheets[$i][cells][$j][$k];
				elseif ($k==4)
					$biobanking_sample_id=$data->sheets[$i][cells][$j][$k];
				elseif ($k==5)
					$biobanking_date=$data->sheets[$i][cells][$j][$k];
				elseif ($k==6)
					$availability=$data->sheets[$i][cells][$j][$k];
				elseif ($k==7)
					$protocol_descr=$data->sheets[$i][cells][$j][$k];
				elseif ($k==8)
					$codenbr=$data->sheets[$i][cells][$j][$k];
			}
			
				$tr++;
				$order = "INSERT INTO patient_biobanking (pat_id,deleted,editor_id,edit_date,biobanking_sample_id,biobanking_date,box,position,availability,protocol_descr,codenbr)";
				$order .= " VALUES ('$pat_id',0,1,now(),";
				$order .= "'$biobanking_sample_id','$biobanking_date','','','$availability','$protocol_descr','$codenbr')";
				$result = pg_query($order);
				
				echo $tr.":".$order."</br>";
				$result = pg_query($order);
			
			//$html .="</tr>";
		}
	}
 
}

//$html .="</table>";

//echo $html;
include '../library/closeDB.php';
?>
</body>
</html>