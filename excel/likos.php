<?php
include '../library/config.php';
include '../library/openDB.php';

include 'excel_reader.php';     // include the class

// creates an object instance of the class, and read the excel file data
$excel = new PhpExcelReader;
$excel->read('likos.xls');

// Excel file data is stored in $sheets property, an Array of worksheets
/*
The data is stored in 'cells' and the meta-data is stored in an array called 'cellsInfo'

Example (firt_sheet - index 0, second_sheet - index 1, ...):

$sheets[0]  -->  'cells'  -->  row --> column --> Interpreted value
         -->  'cellsInfo' --> row --> column --> 'type' (Can be 'date', 'number', or 'unknown')
                                            --> 'raw' (The raw data that Excel stores for that data cell)
*/

// this function creates and returns a HTML table with excel rows and columns data
// Parameter - array with excel worksheet data
function sheetData($sheet) {
  $re = '<table>';     // starts html table

  $x = 2;
  while($x <= $sheet['numRows']) {
    $re .= "<tr>\n";
    $y = 0;
	
			$patient_id="";
			$criteriasle19971="0";
			$criteriadatesle19971="1900-12-12";
			$criteriasle19972="0";
			$criteriadatesle19972="1900-12-12";
			$criteriasle19973="0";
			$criteriadatesle19973="1900-12-12";
			$criteriasle19974="0";
			$criteriadatesle19974="1900-12-12";
			$criteriasle19975="0";
			$criteriadatesle19975="1900-12-12";
			$criteriasle19976="0";
			$criteriadatesle19976="1900-12-12";
			$criteriasle19977="0";
			$criteriadatesle19977="1900-12-12";
			$criteriasle19978="0";
			$criteriadatesle19978="1900-12-12";
			$criteriasle19979="0";
			$criteriadatesle19979="1900-12-12";
			$criteriasle19971="0";
			$criteriadatesle19971="1900-12-12";
			$criteriasle199711="0";
			$criteriadatesle199711="1900-12-12";
			$sle1997totalscore="0";
			$sle1997="";
			$sle1997date="1900-12-12";
			$criteriasle20121="0";
			$criteriadatesle20121="1900-12-12";
			$criteriasle20122="0";
			$criteriadatesle20122="1900-12-12";
			$criteriasle20123="0";
			$criteriadatesle20123="1900-12-12";
			$criteriasle20124="0";
			$criteriadatesle20124="1900-12-12";
			$criteriasle20125="0";
			$criteriadatesle20125="1900-12-12";
			$criteriasle20126="0";
			$criteriadatesle20126="1900-12-12";
			$criteriasle20127="0";
			$criteriadatesle20127="1900-12-12";
			$criteriasle20128="0";
			$criteriadatesle20128="1900-12-12";
			$criteriasle20129="0";
			$criteriadatesle20129="1900-12-12";
			$criteriasle201210="0";
			$criteriadatesle201210="1900-12-12";
			$criteriasle201211="0";
			$criteriadatesle201211="1900-12-12";
			$criteriasle2012b1="0";
			$criteriadatesle2012b1="1900-12-12";
			$criteriasle2012b2="0";
			$criteriadatesle2012b2="1900-12-12";
			$criteriasle2012b3="0";
			$criteriadatesle2012b3="1900-12-12";
			$criteriasle2012b4="0";
			$criteriadatesle2012b4="1900-12-12";
			$criteriasle2012b5="0";
			$criteriadatesle2012b5="1900-12-12";
			$criteriasle2012b6="0";
			$criteriadatesle2012b6="1900-12-12";
			$criteriasle2012c1="0";
			$criteriadatesle2012c1="1900-12-12";
			$sle2012totalscore="0";
			$sle2012="";
			$sle2012date="1900-12-12";
			$criteriasle2017a1="0";
			$criteriadatesle2017a1="1900-12-12";
			$criteriasle2017b1="0";
			$criteriadatesle2017b1="1900-12-12";
			$criteriasle2017b21="0";
			$criteriasle2017b22="0";
			$criteriasle2017b23="0";
			$criteriadatesle2017b21="1900-12-12";
			$criteriadatesle2017b22="1900-12-12";
			$criteriadatesle2017b23="1900-12-12";
			$criteriasle2017b3="0";
			$criteriadatesle2017b3="1900-12-12";
			$criteriasle2017b41="0";
			$criteriasle2017b42="0";
			$criteriasle2017b43="0";
			$criteriadatesle2017b41="1900-12-12";
			$criteriadatesle2017b42="1900-12-12";
			$criteriadatesle2017b43="1900-12-12";
			$criteriasle2017b51="0";
			$criteriasle2017b52="0";
			$criteriadatesle2017b51="1900-12-12";;
			$criteriadatesle2017b52="1900-12-12";;
			$criteriasle2017b61="0";
			$criteriasle2017b62="0";
			$criteriasle2017b63="0";
			$criteriadatesle2017b61="1900-12-12";;
			$criteriadatesle2017b62="1900-12-12";;
			$criteriadatesle2017b63="1900-12-12";;
			$criteriasle2017b71="0";
			$criteriasle2017b72="0";
			$criteriasle2017b73="0";
			$criteriadatesle2017b71="1900-12-12";;
			$criteriadatesle2017b72="1900-12-12";;
			$criteriadatesle2017b73="1900-12-12";;
			$criteriasle2017b8="0";
			$criteriadatesle2017b8="1900-12-12";;
			$criteriasle2017b91="0";
			$criteriasle2017b92="0";
			$criteriadatesle2017b91="1900-12-12";;
			$criteriadatesle2017b92="1900-12-12";;
			$criteriasle2017b10="0";
			$criteriadatesle2017b10="1900-12-12";;
			$sle2017totalscore="";
			$sle2017="";
			$sle2017date="1900-12-12";;
			
			
    while($y <= $sheet['numCols']) {
      $cell = isset($sheet['cells'][$x][$y]) ? $sheet['cells'][$x][$y] : '';
      $re .= " <td>$cell</td>\n";  
      $y++;
	  
				if ($y==1)
					$patient_id=$sheet['cells'][$x][$y];
				elseif ($y==2)
					$criteriasle19971=$sheet['cells'][$x][$y];
				elseif ($y==3 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle19971=$sheet['cells'][$x][$y];
				elseif ($y==4)
					$criteriasle19972=$sheet['cells'][$x][$y];
				elseif ($y==5 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle19972=$sheet['cells'][$x][$y];
				elseif ($y==6)
					$criteriasle19973=$sheet['cells'][$x][$y];
				elseif ($y==7 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle19973=$sheet['cells'][$x][$y];
				elseif ($y==8)
					$criteriasle19974=$sheet['cells'][$x][$y];
				elseif ($y==9 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle19974=$sheet['cells'][$x][$y];
				elseif ($y==10)
					$criteriasle19975=$sheet['cells'][$x][$y];
				elseif ($y==11 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle19975=$sheet['cells'][$x][$y];
				elseif ($y==12)
					$criteriasle19976=$sheet['cells'][$x][$y];
				elseif ($y==13 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle19976=$sheet['cells'][$x][$y];
				elseif ($y==14)
					$criteriasle19977=$sheet['cells'][$x][$y];
				elseif ($y==15 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle19977=$sheet['cells'][$x][$y];
				elseif ($y==16)
					$criteriasle19978=$sheet['cells'][$x][$y];
				elseif ($y==17 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle19978=$sheet['cells'][$x][$y];
				elseif ($y==18)
					$criteriasle19979=$sheet['cells'][$x][$y];
				elseif ($y==19 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle19979=$sheet['cells'][$x][$y];
				elseif ($y==20)
					$criteriasle199710=$sheet['cells'][$x][$y];
				elseif ($y==21 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle199710=$sheet['cells'][$x][$y];
				elseif ($y==22)
					$criteriasle199711=$sheet['cells'][$x][$y];
				elseif ($y==23 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle199711=$sheet['cells'][$x][$y];
				elseif ($y==24)
					$sle1997totalscore=$sheet['cells'][$x][$y];
				elseif ($y==25)
					$sle1997=$sheet['cells'][$x][$y];
				elseif ($y==26 and $sheet['cells'][$x][$y]<>"")
					$sle1997date=$sheet['cells'][$x][$y];
				elseif ($y==27)
					$criteriasle20121=$sheet['cells'][$x][$y];
				elseif ($y==28 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle20121=$sheet['cells'][$x][$y];	
				elseif ($y==29)
					$criteriasle20122=$sheet['cells'][$x][$y];
				elseif ($y==30 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle20122=$sheet['cells'][$x][$y];	
				elseif ($y==31)
					$criteriasle20123=$sheet['cells'][$x][$y];
				elseif ($y==32 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle20123=$sheet['cells'][$x][$y];	
				elseif ($y==33)
					$criteriasle20124=$sheet['cells'][$x][$y];
				elseif ($y==34 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle20124=$sheet['cells'][$x][$y];	
				elseif ($y==35)
					$criteriasle20125=$sheet['cells'][$x][$y];
				elseif ($y==36 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle20125=$sheet['cells'][$x][$y];	
				elseif ($y==37)
					$criteriasle20126=$sheet['cells'][$x][$y];
				elseif ($y==38 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle20126=$sheet['cells'][$x][$y];	
				elseif ($y==39)
					$criteriasle20127=$sheet['cells'][$x][$y];
				elseif ($y==40 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle20127=$sheet['cells'][$x][$y];	
				elseif ($y==41)
					$criteriasle20128=$sheet['cells'][$x][$y];
				elseif ($y==42 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle20128=$sheet['cells'][$x][$y];	
				elseif ($y==43)
					$criteriasle20129=$sheet['cells'][$x][$y];
				elseif ($y==44 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle20129=$sheet['cells'][$x][$y];	
				elseif ($y==45)
					$criteriasle201210=$sheet['cells'][$x][$y];
				elseif ($y==46 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle201210=$sheet['cells'][$x][$y];	
				elseif ($y==47)
					$criteriasle201211=$sheet['cells'][$x][$y];
				elseif ($y==48 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle201211=$sheet['cells'][$x][$y];	
				elseif ($y==49)
					$criteriasle2012b1=$sheet['cells'][$x][$y];
				elseif ($y==50 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2012b1=$sheet['cells'][$x][$y];
				elseif ($y==51)
					$criteriasle2012b2=$sheet['cells'][$x][$y];
				elseif ($y==52 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2012b2=$sheet['cells'][$x][$y];
				elseif ($y==53)
					$criteriasle2012b3=$sheet['cells'][$x][$y];
				elseif ($y==54 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2012b3=$sheet['cells'][$x][$y];
				elseif ($y==55)
					$criteriasle2012b4=$sheet['cells'][$x][$y];
				elseif ($y==56 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2012b4=$sheet['cells'][$x][$y];
				elseif ($y==57)
					$criteriasle2012b5=$sheet['cells'][$x][$y];
				elseif ($y==58 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2012b5=$sheet['cells'][$x][$y];
				elseif ($y==59)
					$criteriasle2012b6=$sheet['cells'][$x][$y];
				elseif ($y==60 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2012b6=$sheet['cells'][$x][$y];
				elseif ($y==61)
					$criteriasle2012c1=$sheet['cells'][$x][$y];
				elseif ($y==62 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2012c1=$sheet['cells'][$x][$y];
				elseif ($y==63)
					$sle2012totalscore=$sheet['cells'][$x][$y];
				elseif ($y==64)
					$sle2012=$sheet['cells'][$x][$y];
				elseif ($y==65 and $sheet['cells'][$x][$y]<>"")
					$sle2012date=$sheet['cells'][$x][$y];
				elseif ($y==66)
					$criteriasle2017a1=$sheet['cells'][$x][$y];
				elseif ($y==67 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017a1=$sheet['cells'][$x][$y];
				elseif ($y==68)
					$criteriasle2017b1=$sheet['cells'][$x][$y];
				elseif ($y==69 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b1=$sheet['cells'][$x][$y];	
				elseif ($y==70)
					$criteriasle2017b21=$sheet['cells'][$x][$y];
				elseif ($y==71)
					$criteriasle2017b22=$sheet['cells'][$x][$y];
				elseif ($y==72)
					$criteriasle2017b23=$sheet['cells'][$x][$y];
				elseif ($y==73 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b21=$sheet['cells'][$x][$y];
				elseif ($y==74 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b22=$sheet['cells'][$x][$y];
				elseif ($y==75 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b23=$sheet['cells'][$x][$y];
				elseif ($y==76)
					$criteriasle2017b3=$sheet['cells'][$x][$y];
				elseif ($y==77 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b3=$sheet['cells'][$x][$y];
				elseif ($y==78)
					$criteriasle2017b41=$sheet['cells'][$x][$y];
				elseif ($y==79)
					$criteriasle2017b42=$sheet['cells'][$x][$y];
				elseif ($y==80)
					$criteriasle2017b43=$sheet['cells'][$x][$y];
				elseif ($y==81 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b41=$sheet['cells'][$x][$y];
				elseif ($y==82 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b42=$sheet['cells'][$x][$y];
				elseif ($y==83 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b43=$sheet['cells'][$x][$y];
				elseif ($y==84)
					$criteriasle2017b51=$sheet['cells'][$x][$y];
				elseif ($y==85)
					$criteriasle2017b52=$sheet['cells'][$x][$y];
				elseif ($y==86 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b51=$sheet['cells'][$x][$y];
				elseif ($y==87 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b52=$sheet['cells'][$x][$y];
				elseif ($y==88)
					$criteriasle2017b61=$sheet['cells'][$x][$y];
				elseif ($y==89)
					$criteriasle2017b62=$sheet['cells'][$x][$y];
				elseif ($y==90)
					$criteriasle2017b63=$sheet['cells'][$x][$y];
				elseif ($y==91 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b61=$sheet['cells'][$x][$y];	
				elseif ($y==92 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b62=$sheet['cells'][$x][$y];	
				elseif ($y==93 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b63=$sheet['cells'][$x][$y];	
				elseif ($y==94)
					$criteriasle2017b71=$sheet['cells'][$x][$y];
				elseif ($y==95)
					$criteriasle2017b72=$sheet['cells'][$x][$y];
				elseif ($y==96)
					$criteriasle2017b73=$sheet['cells'][$x][$y];
				elseif ($y==97 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b71=$sheet['cells'][$x][$y];
				elseif ($y==98 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b72=$sheet['cells'][$x][$y];
				elseif ($y==99 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b73=$sheet['cells'][$x][$y];
				elseif ($y==100)
					$criteriasle2017b8=$sheet['cells'][$x][$y];
				elseif ($y==101 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b8=$sheet['cells'][$x][$y];
				elseif ($y==102)
					$criteriasle2017b91=$sheet['cells'][$x][$y];
				elseif ($y==103)
					$criteriasle2017b92=$sheet['cells'][$x][$y];
				elseif ($y==104 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b91=$sheet['cells'][$x][$y];
				elseif ($y==105 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b92=$sheet['cells'][$x][$y];
				elseif ($y==106)
					$criteriasle2017b10=$sheet['cells'][$x][$y];
				elseif ($y==107 and $sheet['cells'][$x][$y]<>"")
					$criteriadatesle2017b10=$sheet['cells'][$x][$y];
				elseif ($y==108)
					$sle2017totalscore=$sheet['cells'][$x][$y];
				elseif ($y==109)
					$sle2017=$sheet['cells'][$x][$y];
				elseif ($y==110 and $sheet['cells'][$x][$y]<>"")
					$sle2017date=$sheet['cells'][$x][$y];
					
			}
			if($patient_id<>"")	
			{
				$tr++;
				$order = "INSERT INTO patient_criteriasle (";
				$order .= "pat_id,deleted,editor_id,edit_date,";
				$order .= "criteriasle19971,criteriadatesle19971,criteriasle19972,criteriadatesle19972,criteriasle19973,criteriadatesle19973,criteriasle19974,criteriadatesle19974,criteriasle19975,criteriadatesle19975,";
				$order .= "criteriasle19976,criteriadatesle19976,criteriasle19977,criteriadatesle19977,criteriasle19978,criteriadatesle19978,criteriasle19979,criteriadatesle19979,criteriasle199710,criteriadatesle199710";
				$order .= ",criteriasle199711,criteriadatesle199711,sle1997totalscore,sle1997,sle1997date,";
				$order .= "criteriasle20121,criteriadatesle20121,criteriasle20122,criteriadatesle20122,criteriasle20123,criteriadatesle20123,criteriasle20124,criteriadatesle20124,criteriasle20125,criteriadatesle20125,";
				$order .= "criteriasle20126,criteriadatesle20126,criteriasle20127,criteriadatesle20127,criteriasle20128,criteriadatesle20128,criteriasle20129,criteriadatesle20129,criteriasle201210,criteriadatesle201210,";
				$order .= "criteriasle201211,criteriadatesle201211,criteriasle2012b1,criteriadatesle2012b1,criteriasle2012b2,criteriadatesle2012b2,criteriasle2012b3,criteriadatesle2012b3,criteriasle2012b4,criteriadatesle2012b4,";
				$order .= "criteriasle2012b5,criteriadatesle2012b5,criteriasle2012b6,criteriadatesle2012b6,criteriasle2012c1,criteriadatesle2012c1,sle2012totalscore,sle2012,sle2012date,";
				$order .= "criteriasle2017a1,criteriadatesle2017a1,criteriasle2017b1,criteriadatesle2017b1,criteriasle2017b21,criteriasle2017b22,criteriasle2017b23,criteriadatesle2017b21,criteriadatesle2017b22,criteriadatesle2017b23,";
				$order .= "criteriasle2017b3,criteriadatesle2017b3,";
				$order .= "criteriasle2017b41,criteriasle2017b42,criteriasle2017b43,criteriadatesle2017b41,criteriadatesle2017b42,criteriadatesle2017b43,criteriasle2017b51,criteriasle2017b52,criteriadatesle2017b51,criteriadatesle2017b52,";
				$order .= "criteriasle2017b61,criteriasle2017b62,criteriasle2017b63,criteriadatesle2017b61,criteriadatesle2017b62,criteriadatesle2017b63,";
				$order .= "criteriasle2017b71,criteriasle2017b72,criteriasle2017b73,criteriadatesle2017b71,criteriadatesle2017b72,criteriadatesle2017b73,criteriasle2017b8,criteriadatesle2017b8,";
				$order .= "criteriasle2017b91,criteriasle2017b92,criteriadatesle2017b91,criteriadatesle2017b92,criteriasle2017b10,criteriadatesle2017b10,";
				$order .= "sle2017totalscore,sle2017,sle2017date";
				$order .= ")";
				$order .= " VALUES (";
				$order .= "'$patient_id',0,1,now(),";
				$order .= "'$criteriasle19971','$criteriadatesle19971','$criteriasle19972','$criteriadatesle19972','$criteriasle19973','$criteriadatesle19973','$criteriasle19974','$criteriadatesle19974','$criteriasle19975',";
				$order .= "'$criteriadatesle19975','$criteriasle19976','$criteriadatesle19976','$criteriasle19977','$criteriadatesle19977','$criteriasle19978','$criteriadatesle19978','$criteriasle19979','$criteriadatesle19979','$criteriasle199710','$criteriadatesle199710'";
				$order .= ",'$criteriasle199711','$criteriadatesle199711','$sle1997totalscore','$sle1997','$sle1997date',";
				$order .= "'$criteriasle20121','$criteriadatesle20121','$criteriasle20122','$criteriadatesle20122','$criteriasle20123','$criteriadatesle20123','$criteriasle20124','$criteriadatesle20124','$criteriasle20125','$criteriadatesle20125',";
				$order .= "'$criteriasle20126','$criteriadatesle20126','$criteriasle20127','$criteriadatesle20127','$criteriasle20128','$criteriadatesle20128','$criteriasle20129','$criteriadatesle20129','$criteriasle201210','$criteriadatesle201210',";
				$order .= "'$criteriasle201211','$criteriadatesle201211','$criteriasle2012b1','$criteriadatesle2012b1','$criteriasle2012b2','$criteriadatesle2012b2','$criteriasle2012b3','$criteriadatesle2012b3','$criteriasle2012b4','$criteriadatesle2012b4',";
				$order .= "'$criteriasle2012b5','$criteriadatesle2012b5','$criteriasle2012b6','$criteriadatesle2012b6','$criteriasle2012c1','$criteriadatesle2012c1','$sle2012totalscore','$sle2012','$sle2012date',";
				$order .= "'$criteriasle2017a1','$criteriadatesle2017a1','$criteriasle2017b1','$criteriadatesle2017b1','$criteriasle2017b21','$criteriasle2017b22','$criteriasle2017b23','$criteriadatesle2017b21','$criteriadatesle2017b22','$criteriadatesle2017b23',";
				$order .= "'$criteriasle2017b3','$criteriadatesle2017b3',";
				$order .= "'$criteriasle2017b41','$criteriasle2017b42','$criteriasle2017b43','$criteriadatesle2017b41','$criteriadatesle2017b42','$criteriadatesle2017b43',";
				$order .= "'$criteriasle2017b51','$criteriasle2017b52','$criteriadatesle2017b51','$criteriadatesle2017b52','$criteriasle2017b61','$criteriasle2017b62','$criteriasle2017b63','$criteriadatesle2017b61','$criteriadatesle2017b62','$criteriadatesle2017b63'";
				$order .= ",'$criteriasle2017b71','$criteriasle2017b72','$criteriasle2017b73','$criteriadatesle2017b71','$criteriadatesle2017b72','$criteriadatesle2017b73','$criteriasle2017b8','$criteriadatesle2017b8',";
				$order .= "'$criteriasle2017b91','$criteriasle2017b92','$criteriadatesle2017b91','$criteriadatesle2017b92','$criteriasle2017b10','$criteriadatesle2017b10'";
				$order .= ",'$sle2017totalscore','$sle2017','$sle2017date'";
				$order .= ")";
				echo $tr.":".$order."</br>";
				$result = pg_query($order);
    }  
    $re .= "</tr>\n";
    $x++;
  }

  return $re .'</table>';     // ends and returns the html table
}

$nr_sheets = count($excel->sheets);       // gets the number of sheets
$excel_data = '';              // to store the the html tables with data of each sheet

// traverses the number of sheets and sets html table with each sheet data in $excel_data
//for($i=0; $i<$nr_sheets; $i++) {
for($i=0; $i<1; $i++) {
  $excel_data .= '<h4>Sheet '. ($i + 1) .' (<em>'. $excel->boundsheets[$i]['name'] .'</em>)</h4>'. sheetData($excel->sheets[$i]) .'<br/>';  
}
?>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Example PHP Excel Reader</title>
<style type="text/css">
table {
 border-collapse: collapse;
}        
td {
 border: 1px solid black;
 padding: 0 0.5em;
}        
</style>
</head>
<body>

<?php
// displays tables with excel file data
echo $excel_data;
include '../library/closeDB.php';
?>    

</body>
</html>
