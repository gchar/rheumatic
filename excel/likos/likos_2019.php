<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php 
//error_reporting(E_ALL);
//ini_set('display_errors', 1);

include '../library/config.php';
include '../library/openDB.php';

require_once 'excel_reader.php';


 
$data = new Spreadsheet_Excel_Reader("book1.xls");
 
echo "Total Sheets in this xls file: ".count($data->sheets)."<br /><br />";
 
//$html="<table border='1'>";

$tr=0;
for($i=0;$i<count($data->sheets);$i++) // Loop to get all sheets in a file.
{	
	if(count($data->sheets[$i][cells])>0) // checking sheet not empty
	{
		echo "Sheet $i:<br /><br />Total rows in sheet $i  ".count($data->sheets[$i][cells])."<br />";
		
		for($j=2;$j<=count($data->sheets[$i][cells]);$j++) // loop used to get each row of the sheet
		{ 
			$patient_id="";
			$criteriasle19971="";
			$criteriadatesle19971="";
			$criteriasle19972="";
			$criteriadatesle19972="";
			$criteriasle19973="";
			$criteriadatesle19973="";
			$criteriasle19974="";
			$criteriadatesle19974="";
			$criteriasle19975="";
			$criteriadatesle19975="";
			$criteriasle19976="";
			$criteriadatesle19976="";
			$criteriasle19977="";
			$criteriadatesle19977="";
			$criteriasle19978="";
			$criteriadatesle19978="";
			$criteriasle19979="";
			$criteriadatesle19979="";
			$criteriasle19971="";
			$criteriadatesle19971="";
			$criteriasle199711="";
			$criteriadatesle199711="";
			$sle1997totalscore="";
			$sle1997="";
			$sle1997date="";
			$criteriasle20121="";
			$criteriadatesle20121="";
			$criteriasle20122="";
			$criteriadatesle20122="";
			$criteriasle20123="";
			$criteriadatesle20123="";
			$criteriasle20124="";
			$criteriadatesle20124="";
			$criteriasle20125="";
			$criteriadatesle20125="";
			$criteriasle20126="";
			$criteriadatesle20126="";
			$criteriasle20127="";
			$criteriadatesle20127="";
			$criteriasle20128="";
			$criteriadatesle20128="";
			$criteriasle20129="";
			$criteriadatesle20129="";
			$criteriasle201210="";
			$criteriadatesle201210="";
			$criteriasle201211="";
			$criteriadatesle201211="";
			$criteriasle2012b1="";
			$criteriadatesle2012b1="";
			$criteriasle2012b2="";
			$criteriadatesle2012b2="";
			$criteriasle2012b3="";
			$criteriadatesle2012b3="";
			$criteriasle2012b4="";
			$criteriadatesle2012b4="";
			$criteriasle2012b5="";
			$criteriadatesle2012b5="";
			$criteriasle2012b6="";
			$criteriadatesle2012b6="";
			$criteriasle2012c1="";
			$criteriadatesle2012c1="";
			$sle2012totalscore="";
			$sle2012="";
			$sle2012date="";
			$criteriasle2017a1="";
			$criteriadatesle2017a1="";
			$criteriasle2017b1="";
			$criteriadatesle2017b1="";
			$criteriasle2017b21="";
			$criteriasle2017b22="";
			$criteriasle2017b23="";
			$criteriadatesle2017b2="";
			$criteriasle2017b3="";
			$criteriadatesle2017b3="";
			$criteriasle2017b41="";
			$criteriasle2017b42="";
			$criteriasle2017b43="";
			$criteriadatesle2017b4="";
			$criteriasle2017b51="";
			$criteriasle2017b52="";
			$criteriadatesle2017b5="";
			$criteriasle2017b61="";
			$criteriasle2017b62="";
			$criteriasle2017b63="";
			$criteriadatesle2017b6="";
			$criteriasle2017b71="";
			$criteriasle2017b72="";
			$criteriasle2017b73="";
			$criteriadatesle2017b7="";
			$criteriasle2017b8="";
			$criteriadatesle2017b8="";
			$criteriasle2017b91="";
			$criteriasle2017b92="";
			$criteriadatesle2017b9="";
			$criteriasle2017b10="";
			$criteriadatesle2017b10="";
			$sle2017totalscore="";
			$sle2017="";
			$sle2017date="";
		
			for($k=1;$k<=count($data->sheets[$i][cells][$j]);$k++) // This loop is created to get data in a table format.
			{
				if ($k==1)
					$patient_id=$data->sheets[$i][cells][$j][$k];
				elseif ($k==2)
					$criteriasle19971=$data->sheets[$i][cells][$j][$k];
				elseif ($k==3)
					$criteriadatesle19971=$data->sheets[$i][cells][$j][$k];
				elseif ($k==4)
					$criteriasle19972=$data->sheets[$i][cells][$j][$k];
				elseif ($k==5)
					$criteriadatesle19972=$data->sheets[$i][cells][$j][$k];
				elseif ($k==6)
					$criteriasle19973=$data->sheets[$i][cells][$j][$k];
				elseif ($k==7)
					$criteriadatesle19973=$data->sheets[$i][cells][$j][$k];
				elseif ($k==8)
					$criteriasle19974=$data->sheets[$i][cells][$j][$k];
				elseif ($k==9)
					$criteriadatesle19974=$data->sheets[$i][cells][$j][$k];
				elseif ($k==10)
					$criteriasle19975=$data->sheets[$i][cells][$j][$k];
				elseif ($k==11)
					$criteriadatesle19975=$data->sheets[$i][cells][$j][$k];
				elseif ($k==12)
					$criteriasle19976=$data->sheets[$i][cells][$j][$k];
				elseif ($k==13)
					$criteriadatesle19976=$data->sheets[$i][cells][$j][$k];
				elseif ($k==14)
					$criteriasle19977=$data->sheets[$i][cells][$j][$k];
				elseif ($k==15)
					$criteriadatesle19977=$data->sheets[$i][cells][$j][$k];
				elseif ($k==16)
					$criteriasle19978=$data->sheets[$i][cells][$j][$k];
				elseif ($k==17)
					$criteriadatesle19978=$data->sheets[$i][cells][$j][$k];
				elseif ($k==18)
					$criteriasle19979=$data->sheets[$i][cells][$j][$k];
				elseif ($k==19)
					$criteriadatesle19979=$data->sheets[$i][cells][$j][$k];
				elseif ($k==20)
					$criteriasle199710=$data->sheets[$i][cells][$j][$k];
				elseif ($k==21)
					$criteriadatesle199710=$data->sheets[$i][cells][$j][$k];
				elseif ($k==22)
					$criteriasle199711=$data->sheets[$i][cells][$j][$k];
				elseif ($k==23)
					$criteriadatesle199711=$data->sheets[$i][cells][$j][$k];
				elseif ($k==24)
					$sle1997totalscore=$data->sheets[$i][cells][$j][$k];
				elseif ($k==25)
					$sle1997=$data->sheets[$i][cells][$j][$k];
				elseif ($k==26 and $data->sheets[$i][cells][$j][$k]<>"")
					$sle1997date=$data->sheets[$i][cells][$j][$k];
				elseif ($k==27)
					$criteriasle20121=$data->sheets[$i][cells][$j][$k];
				elseif ($k==28)
					$criteriadatesle20121=$data->sheets[$i][cells][$j][$k];	
				elseif ($k==29)
					$criteriasle20122=$data->sheets[$i][cells][$j][$k];
				elseif ($k==30)
					$criteriadatesle20122=$data->sheets[$i][cells][$j][$k];	
				elseif ($k==31)
					$criteriasle20123=$data->sheets[$i][cells][$j][$k];
				elseif ($k==32)
					$criteriadatesle20123=$data->sheets[$i][cells][$j][$k];	
				elseif ($k==33)
					$criteriasle20124=$data->sheets[$i][cells][$j][$k];
				elseif ($k==34)
					$criteriadatesle20124=$data->sheets[$i][cells][$j][$k];	
				elseif ($k==35)
					$criteriasle20125=$data->sheets[$i][cells][$j][$k];
				elseif ($k==36)
					$criteriadatesle20125=$data->sheets[$i][cells][$j][$k];	
				elseif ($k==37)
					$criteriasle20126=$data->sheets[$i][cells][$j][$k];
				elseif ($k==38)
					$criteriadatesle20126=$data->sheets[$i][cells][$j][$k];	
				elseif ($k==39)
					$criteriasle20127=$data->sheets[$i][cells][$j][$k];
				elseif ($k==40)
					$criteriadatesle20127=$data->sheets[$i][cells][$j][$k];	
				elseif ($k==41)
					$criteriasle20128=$data->sheets[$i][cells][$j][$k];
				elseif ($k==42)
					$criteriadatesle20128=$data->sheets[$i][cells][$j][$k];	
				elseif ($k==43)
					$criteriasle20129=$data->sheets[$i][cells][$j][$k];
				elseif ($k==44)
					$criteriadatesle20129=$data->sheets[$i][cells][$j][$k];	
				elseif ($k==45)
					$criteriasle201210=$data->sheets[$i][cells][$j][$k];
				elseif ($k==46)
					$criteriadatesle201210=$data->sheets[$i][cells][$j][$k];	
				elseif ($k==47)
					$criteriasle201211=$data->sheets[$i][cells][$j][$k];
				elseif ($k==48)
					$criteriadatesle201211=$data->sheets[$i][cells][$j][$k];	
				elseif ($k==49)
					$criteriasle2012b1=$data->sheets[$i][cells][$j][$k];
				elseif ($k==50)
					$criteriadatesle2012b1=$data->sheets[$i][cells][$j][$k];
				elseif ($k==51)
					$criteriasle2012b2=$data->sheets[$i][cells][$j][$k];
				elseif ($k==52)
					$criteriadatesle2012b2=$data->sheets[$i][cells][$j][$k];
				elseif ($k==53)
					$criteriasle2012b3=$data->sheets[$i][cells][$j][$k];
				elseif ($k==54)
					$criteriadatesle2012b3=$data->sheets[$i][cells][$j][$k];
				
			}
			
			if($patient_id<>"")	
			{
				$tr++;
				$order = "INSERT INTO patient_common (";
				$order .= "patient_id,deleted,editor_id,edit_date,";
				$order .= "criteriasle19971,criteriadatesle19971,criteriasle19972,criteriadatesle19972,divcriteriasle19973,criteriadatesle19973,criteriasle19974,criteriadatesle19974,criteriasle19975,criteriadatesle19975,criteriasle19976,";
				$order .= "criteriadatesle19976,criteriasle19977,criteriadatesle19977,criteriasle19978,criteriadatesle19978,criteriasle19979,criteriadatesle19979,criteriasle199710,criteriadatesle199710,criteriasle199711,criteriadatesle199711,";
				$order .= "sle1997totalscore,sle1997,sle1997date,";
				$order .= "criteriasle20121,criteriadatesle20121,criteriasle20122,criteriadatesle20122,criteriasle20123,criteriadatesle20123,criteriasle20124,criteriadatesle20124,criteriasle20125,criteriadatesle20125,criteriasle20126,criteriadatesle20126,";
				$order .= "criteriasle20127,criteriadatesle20127,criteriasle20128,criteriadatesle20128,criteriasle20129,criteriadatesle20129,criteriasle201210,criteriadatesle201210,criteriasle201211,criteriadatesle201211,";
				$order .= "criteriasle2012b1,criteriadatesle2012b1,criteriasle2012b2,criteriadatesle2012b2,criteriasle2012b3,criteriadatesle2012b3";
				//$order .= ",criteriasle2012b4,criteriadatesle2012b4,criteriasle2012b5,criteriadatesle2012b5,";
				//$order .= "criteriasle2012b6,criteriadatesle2012b6,criteriasle2012c1,criteriadatesle2012c1,sle2012totalscore,sle2012,sle2012date,criteriasle2017a1,criteriadatesle2017a1,criteriasle2017b1,criteriadatesle2017b1,criteriasle2017b21,criteriasle2017b22,criteriasle2017b23,criteriadatesle2017b2,criteriasle2017b3,criteriadatesle2017b3,criteriasle2017b41,criteriasle2017b42,criteriasle2017b43,criteriadatesle2017b4,criteriasle2017b51,criteriasle2017b52,criteriadatesle2017b5,criteriasle2017b61,criteriasle2017b62,criteriasle2017b63,criteriadatesle2017b6,criteriasle2017b71,criteriasle2017b72,criteriasle2017b73,criteriadatesle2017b7,criteriasle2017b8,criteriadatesle2017b8,criteriasle2017b91,criteriasle2017b92,criteriadatesle2017b9,criteriasle2017b10,criteriadatesle2017b10,sle2017totalscore,sle2017,sle2017date";
				$order .= ")";
				$order .= " VALUES (";
				$order .= "'$patient_id',0,1,now(),";
				$order .= "'$criteriasle19971','$criteriadatesle19971','$criteriasle19972','$criteriadatesle19972','$divcriteriasle19973','$criteriadatesle19973','$criteriasle19974','$criteriadatesle19974','$criteriasle19975','$criteriadatesle19975','$criteriasle19976',";
				$order .= "'$criteriadatesle19976','$criteriasle19977','$criteriadatesle19977','$criteriasle19978','$criteriadatesle19978','$criteriasle19979','$criteriadatesle19979','$criteriasle199710','$criteriadatesle199710','$criteriasle199711','$criteriadatesle199711',";
				$order .= "'$sle1997totalscore','$sle1997','$sle1997date',";
				$order .= "'$criteriasle20121','$criteriadatesle20121','$criteriasle20122','$criteriadatesle20122','$criteriasle20123','$criteriadatesle20123','$criteriasle20124','$criteriadatesle20124','$criteriasle20125','$criteriadatesle20125','$criteriasle20126','$criteriadatesle20126',";
				$order .= "'$criteriasle20127','$criteriadatesle20127','$criteriasle20128','$criteriadatesle20128','$criteriasle20129','$criteriadatesle20129','$criteriasle201210','$criteriadatesle201210','$criteriasle201211','$criteriadatesle201211',";
				$order .= "'$criteriasle2012b1','$criteriadatesle2012b1','$criteriasle2012b2','$criteriadatesle2012b2','$criteriasle2012b3','$criteriadatesle2012b3'";
				//$order .= ",'$criteriasle2012b4','$criteriadatesle2012b4','$criteriasle2012b5','$criteriadatesle2012b5',";
				//$order .= "'$criteriasle2012b6','$criteriadatesle2012b6','$criteriasle2012c1','$criteriadatesle2012c1','$sle2012totalscore','$sle2012','$sle2012date','$criteriasle2017a1','$criteriadatesle2017a1','$criteriasle2017b1','$criteriadatesle2017b1','$criteriasle2017b21','$criteriasle2017b22','$criteriasle2017b23','$criteriadatesle2017b2','$criteriasle2017b3','$criteriadatesle2017b3','$criteriasle2017b41','$criteriasle2017b42','$criteriasle2017b43','$criteriadatesle2017b4','$criteriasle2017b51','$criteriasle2017b52','$criteriadatesle2017b5','$criteriasle2017b61','$criteriasle2017b62','$criteriasle2017b63','$criteriadatesle2017b6','$criteriasle2017b71','$criteriasle2017b72','$criteriasle2017b73','$criteriadatesle2017b7','$criteriasle2017b8','$criteriadatesle2017b8','$criteriasle2017b91','$criteriasle2017b92','$criteriadatesle2017b9','$criteriasle2017b10','$criteriadatesle2017b10','$sle2017totalscore','$sle2017','$sle2017date'";
				$order .= ")";
				echo $order."</br></br>";
				$result = pg_query($order);
			}
		}
	}
 
}

//$html .="</table>";

//echo $html;
include '../library/closeDB.php';
?>
</body>
</html>