<?php
include '../library/config.php';
include '../library/openDB.php';

include 'excel_reader.php';     // include the class

// creates an object instance of the class, and read the excel file data
$excel = new PhpExcelReader;
$excel->read('patientfu.xls');

// Excel file data is stored in $sheets property, an Array of worksheets
/*
The data is stored in 'cells' and the meta-data is stored in an array called 'cellsInfo'

Example (firt_sheet - index 0, second_sheet - index 1, ...):

$sheets[0]  -->  'cells'  -->  row --> column --> Interpreted value
         -->  'cellsInfo' --> row --> column --> 'type' (Can be 'date', 'number', or 'unknown')
                                            --> 'raw' (The raw data that Excel stores for that data cell)
*/

// this function creates and returns a HTML table with excel rows and columns data
// Parameter - array with excel worksheet data
function sheetData($sheet) {
  $re = '<table>';     // starts html table

  $x = 1;
  while($x <= $sheet['numRows']) {
    $re .= "<tr>\n";
    //$y = 0;
			
			
    //while($y <= $sheet['numCols']) {
		$patient_id="";
		
      $cell = isset($sheet['cells'][$x][1]) ? $sheet['cells'][$x][1] : '';
      $re .= " <td>$cell</td>\n";  
      //$y++;
	  
				//if ($y==1)
					$patient_id=$sheet['cells'][$x][1];
					
					if($patient_id<>"")
					{
						//for ($fui=1;$fui<=2;$fui++)
						//{
							/*if($fui==1)
								$fu=0;
							else if($fui==2)
								$fu=3;
							else if($fui==3)
								$fu=6;
							else if($fui==4)
								$fu=9;
							else if($fui==5)
								$fu=12;
							else if($fui==6)
								$fu=15;
							else if($fui==7)
								$fu=18;
							else if($fui==8)
								$fu=21;
							else if($fui==9)
								$fu=24;
							else if($fui==10)
								$fu=30;
							else if($fui==11)
								$fu=36;
							else if($fui==12)
								$fu=42;
							else if($fui==13)
								$fu=48;
							else if($fui==14)
								$fu=54;
							else if($fui==15)
								$fu=60;
							else if($fui==16)
								$fu=72;
							else if($fui==17)
								$fu=84;
							*/
							$fu=60;
							//$re .= " <td>$fu</td>\n";
							
							$q_coh = "select patient_cohort_id from patient_cohort where deleted=0 and pat_id=$patient_id";
							$q_coh_exec = pg_query($q_coh);
							$num_rows = pg_num_rows($q_coh_exec);
							while($q_coh_reason = pg_fetch_assoc($q_coh_exec))
							{
								$patient_cohort_id=$q_coh_reason['patient_cohort_id'];
								$q1 = "select count(pat_id) as exist from patient_lookup_drugs where patient_cohort_id=$patient_cohort_id and deleted=0 and fumonth_cohort=$fu and pat_id=$patient_id and drug_flag=1";
								$q_exec = pg_query($q1);
								$q_reason=pg_fetch_array($q_exec); 
								$exist=$q_reason['exist'];
								if($exist==0)
								{
									$query_update = "update patient_lookup_drugs set deleted=0 where patient_cohort_id=$patient_cohort_id and deleted=1 and fumonth_cohort=$fu and pat_id=$patient_id and drug_flag=1";
									$result = pg_query($query_update);
									if($result)
										$re .= " <td>$patient_cohort_id</td>\n";
								}
								else
									$re .= " <td>not upd:$patient_cohort_id</td>\n";
							}
						
							
							
						//}
						
					}
			//}
			
    $re .= "</tr>\n";
    $x++;
  }

  return $re .'</table>';     // ends and returns the html table
}

$nr_sheets = count($excel->sheets);       // gets the number of sheets
$excel_data = '';              // to store the the html tables with data of each sheet

// traverses the number of sheets and sets html table with each sheet data in $excel_data
//for($i=0; $i<$nr_sheets; $i++) {
for($i=0; $i<1; $i++) {
  $excel_data .= '<h4>Sheet '. ($i + 1) .' (<em>'. $excel->boundsheets[$i]['name'] .'</em>)</h4>'. sheetData($excel->sheets[$i]) .'<br/>';  
}
?>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>Example PHP Excel Reader</title>
<style type="text/css">
table {
 border-collapse: collapse;
}        
td {
 border: 1px solid black;
 padding: 0 0.5em;
}        
</style>
</head>
<body>

<?php
// displays tables with excel file data
echo $excel_data;
include '../library/closeDB.php';
?>    

</body>
</html>
