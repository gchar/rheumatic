<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php 
//error_reporting(E_ALL);
///ini_set('display_errors', 1);

include '../../library/config.php';
include '../../library/openDB.php';

require_once 'excel_reader.php';


 
$data = new Spreadsheet_Excel_Reader("patient_spa1.xls");
 
echo "Total Sheets in this xls file: ".count($data->sheets)."<br /><br />";
 
//$html="<table border='1'>";
for($i=0;$i<count($data->sheets);$i++) // Loop to get all sheets in a file.
{	
	if(count($data->sheets[$i][cells])>0) // checking sheet not empty
	{
		echo "Sheet $i:<br /><br />Total rows in sheet $i  ".count($data->sheets[$i][cells])."<br />";
		for($j=1;$j<=count($data->sheets[$i][cells]);$j++) // loop used to get each row of the sheet
		{ 
		
			//$html .="<tr>";
			$patient_spa_id="";
			$patient_followup_id="";
			$pat_id="";
			$variable="";
			$value="";
			
			for($k=1;$k<=count($data->sheets[$i][cells][$j]);$k++) // This loop is created to get data in a table format.
			{
				if ($k==1)
				{
					$pat_id=$data->sheets[$i][cells][$j][$k];
					//$html .="<td>";
					//$html .=$data->sheets[$i][cells][$j][$k];
					//$html .="</td>";
				}
				elseif ($k==2)
				{
					$patient_followup_id=$data->sheets[$i][cells][$j][$k];
					//$html .="<td>";
					//$html .=$data->sheets[$i][cells][$j][$k];
					//$html .="</td>";
				}
				elseif ($k==3)
				{
					$variable=$data->sheets[$i][cells][$j][$k];
					//$html .="<td>";
					//$html .=$data->sheets[$i][cells][$j][$k];
					//$html .="</td>";
				}
				elseif ($k==4)
				{
					$value=$data->sheets[$i][cells][$j][$k];
					//$html .="<td>";
					//$html .=$data->sheets[$i][cells][$j][$k];
					//$html .="</td>";
				}
				
				
				
			}
			
			if($variable<>"BASFI" and $variable<>"BASDAI")
				$value=number_format($value, 1, '.', '');
			else
				$value=number_format($value, 2, '.', '');
			
			$query="select patient_spa_id from patient_spa where deleted=0 and patient_followup_id=".$patient_followup_id." and pat_id=".$pat_id;
			$exec = pg_query($query);
			
			$result = pg_fetch_array($exec);
			$patient_spa_id=$result['patient_spa_id'];
			

			if($patient_spa_id=="")	
			{
				$order = "INSERT INTO patient_spa (patient_followup_id,pat_id,deleted,";
				$order .= $variable;
				$order .= ")";
				$order .= " VALUES ($patient_followup_id,$pat_id,0,'$value')";
				$result = pg_query($order);
				//echo $order."</br>";
				
				$order = "update patient_followup set spa=2 where patient_followup_id=$patient_followup_id";
				
				$result = pg_query($order);
			}
			else
			{
				$order = "update patient_spa set ";
				$order .= $variable;
				$order .= "=";
				$order .= "'$value' where patient_spa_id=$patient_spa_id";
				$result = pg_query($order);
				//echo $order."</br>";
			}
			
			//$html .="</tr>";
		}
	}
 
}
//$html .="</table>";
//echo $html;
include '../../library/closeDB.php';
?>
</body>
</html>