<?php
$id = $_REQUEST['id'];
	
include '../library/config.php';
include '../library/openDB.php';
include '../library/functions.php';
include '../library/JSON.php';

$populate_arr = array();
if($id<>"" and $id<>"0")
{	
	$query="select * from diseases_therapy_comorbidities where deleted=0 and diseases_therapy_organs_id=$id order by code asc";
	
	$exec = pg_query($query);
	//$result = pg_fetch_array($exec);

	while($result = pg_fetch_assoc($exec))
	{
		$id = $result['diseases_therapy_comorbidities_id'];
		$code = $result['code'];
		$value = $result['value'];
		$atc_ddd = $result['atc_ddd'];
		$populate_arr[] = array("id" => $id, "code" => $code, "value" => $value, "atc_ddd" => $atc_ddd);
		
	}
	
}

$json = new Services_JSON();
//$decoded = $json->decode($jsondata);
echo $json->encode($populate_arr);
include '../library/closeDB.php';
?>
