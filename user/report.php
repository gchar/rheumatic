﻿<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	
	$save=$_REQUEST['save'];
	$type=$_REQUEST['type'];
	$all_centre=$_REQUEST['all_centre'];
	/*$patient_cohort_enddate=$_REQUEST['patient_cohort_enddate'];
	$patient_cohort_startdate=$_REQUEST['patient_cohort_startdate'];
	$patient_event_enddate=$_REQUEST['patient_event_enddate'];
	$patient_event_startdate=$_REQUEST['patient_event_startdate'];
	$selectmonth=$_REQUEST['selectmonth'];
	$selectdrugs=$_REQUEST['selectdrugs'];
	$selectcommon=$_REQUEST['selectcommon'];
	$selectsle=$_REQUEST['selectsle'];
	*/
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>
</head>
<body class="hold-transition skin-blue layout-top-nav">
<?php
	
	
			
	
				
	if($save==1)
	{
		pg_query("BEGIN") or die("Could not start transaction\n");
		try
		{

			
			if($type==1)
			{
				
				$selectdrugs="";
				$selectmonths="";
				$selectcommons="";
				$selectsles="";
				$selectcomors="";
				$selectrest="";
				$numselectdrugs = count($_REQUEST['selectdrugs']);
				$numselectmonths = count($_REQUEST['selectmonth']);
				$numselectcommons = count($_REQUEST['selectcommon']);
				$numselectsles = count($_REQUEST['selectsle']);
				$numselectcomor = count($_REQUEST['selectcomor']);
				$numselectrest = count($_REQUEST['selectrest']);
				
				$i=0;
				$j=0;
				$k=0;
				$l=0;
				$m=0;
				$n=0;
				foreach ($_REQUEST['selectdrugs'] as $selectdrugsOption)
				{
					$selectdrugs .= $selectdrugsOption;
					if (++$i != $numselectdrugs)
						$selectdrugs .= ",";
				}
					
				//echo "selectdrugs:$selectdrugs</br>";
						
				foreach ($_REQUEST['selectmonth'] as $selectmonthOption)
				{
					$selectmonths .= $selectmonthOption;
					if (++$j != $numselectmonths)
						$selectmonths .= ",";
				}
				//echo "selectmonth:$selectmonths</br>";
				
				foreach ($_REQUEST['selectcommon'] as $selectcommonOption)
				{
					$selectcommons .= '"'.$selectcommonOption.'"';
					if (++$k != $numselectcommons)
						$selectcommons .= ',';
				}
				
				foreach ($_REQUEST['selectsle'] as $selectsleOption)
				{
					$selectsles .= '"'.$selectsleOption.'"';
					if (++$l != $numselectsles)
						$selectsles .= ",";
				}
				
				//echo "numselectcomor:$numselectcomor</br>";
				foreach ($_REQUEST['selectcomor'] as $selectcomorOption)
				{
					$selectcomors .= '"'.$selectcomorOption.'"';
					if (++$m != $numselectcomor)
						$selectcomors .= ",";
				}
				
				//echo "numselectrest:$numselectrest</br>";
				foreach ($_REQUEST['selectrest'] as $selectrestOption)
				{
					$selectrest .= '"'.$selectrestOption.'"';
					if (++$n != $numselectrest)
						$selectrest .= ",";
				}
				//echo "selectsle:$selectsles</br>";
				
				/*$query = "(select statistics_lk1.*,statistics_lk2.*,statistics_lk3.* from statistics_lk1 join statistics_lk2 on statistics_lk1.id=statistics_lk2.id join statistics_lk3 on statistics_lk2.id=statistics_lk3.id order by statistics_lk1.id asc limit 1 )";
				$query .= " union (select statistics_lk1.*,statistics_lk2.*,statistics_lk3.* from statistics_lk1 join statistics_lk2 on statistics_lk1.id=statistics_lk2.id join statistics_lk3 on statistics_lk2.id=statistics_lk3.id ";
				
				$query .= " where statistics_lk1.id<>1 ";
				if($user_all_rpts==1)
				{
					if($all_centre==0)
						$query .= " and statistics_lk1.a5='$user_centre_id' ";
				}
				else
					$query .= " and statistics_lk1.a5='$user_centre_id' ";
				
				if($patient_cohort_enddate<>"" or $patient_cohort_startdate<>"")
				$query .= " and ";
			
				if($patient_cohort_startdate<>"")
					$query .= " statistics_lk1.a8 >= '$patient_cohort_startdate' ";

				if($patient_cohort_enddate<>"")
				{
					if($patient_cohort_startdate<>"")
						$query .= " and statistics_lk1.a8 <= '$patient_cohort_enddate' ";
					else
						$query .= " statistics_lk1.a8 <= '$patient_cohort_enddate' ";
				}

				$query .= " order by statistics_lk1.id asc )";
				*/
				
				$query= "SELECT select_detail_cohort('{".$selectdrugs."}','{".$selectmonths."}','{".$selectcommons."}','{".$selectsles."}','{".$selectcomors."}','{".$selectrest."}',".$user_all_rpts.",".$all_centre.",".$user_centre_id.",";
				$patient_cohort_startdate=date_for_postgres($_REQUEST['patient_cohort_startdate']);
				$patient_cohort_enddate=date_for_postgres($_REQUEST['patient_cohort_enddate']);
				
				if ($patient_cohort_startdate=="")
						$query .= "NULL,";
				else
						$query .= "'".$patient_cohort_startdate."',";
				
				if ($patient_cohort_enddate=="")
					$query .= "NULL)";
				else
					$query .= "'".$patient_cohort_enddate."')";
				
				//echo "query:$query";
				
				$sql = pg_query($query);
				$result = pg_fetch_array($sql);
				$select=$result[0];
				//echo "select:$select";
				$dt=date("YmdHis");
				$final_query="COPY (".$select.") TO 'C:/Bitnami/apache2/htdocs/rheumatic/user/statistics/cohort_".$dt.".csv' DELIMITER ',';";
				//echo "final_query:$final_query";
				$exec = pg_query($final_query);
				if ($exec)
					$msg1="ok";
				
				$parameters[0]=0;
				$parameters[1]=$user_id;
				$parameters[2]='now()';
				$parameters[3]=date_for_postgres($_REQUEST['patient_cohort_startdate']);
				$parameters[4]=date_for_postgres($_REQUEST['patient_cohort_enddate']);
				$parameters[5]='now()';
				$parameters[6]="cohort_".$dt;
				$parameters[7]=1;
				$parameters[8]=$all_centre;
				if($user_all_rpts==0)
					$parameters[9]=$user_centre_id;
				else
				{
					if($all_centre==0)
						$parameters[9]=$user_centre_id;
					else
						$parameters[9]=0;
				}
					
				
				$table_names[0]='deleted';
				$table_names[1]='editor_id';
				$table_names[2]='edit_date';
				$table_names[3]='start_date';
				$table_names[4]='end_date';
				$table_names[5]='create_date';
				$table_names[6]='filename';
				$table_names[7]='type';
				$table_names[8]='all_centre';
				$table_names[9]='centre_id';
				
				$statistics_id=insert('statistics',$table_names,$parameters,'statistics_id');
				if($statistics_id!='')
					$msg2="ok";
			}
			else if($type==2)
			{
				$m=0;
				
				$patient_event_enddate=$_REQUEST['patient_event_enddate'];
				$patient_event_startdate=$_REQUEST['patient_event_startdate'];
				
				
				$query = "(select * from statistics_lk4 order by id asc limit 1 )";
				$query .= " union (select * from statistics_lk4  ";
				
				$query .= " where id<>1 ";
				if($user_all_rpts==1)
				{
					if($all_centre==0)
						$query .= " and a5='$user_centre_id' ";
				}
				else
					$query .= " and a5='$user_centre_id' ";
				
				if($patient_event_enddate<>"" or $patient_event_startdate<>"")
				$query .= " and ";
				
				if($patient_event_startdate<>"")
					$query .= " a31 >= '$patient_event_startdate' ";

				if($patient_event_enddate<>"")
				{
					if($patient_event_startdate<>"")
						$query .= " and a31 <= '$patient_event_enddate' ";
					else
						$query .= " a31 <= '$patient_event_enddate' ";
				}

				$query .= " order by id asc )  order by id asc";
				
				
				
				//echo "query:$query</br>";
				$dt=date("YmdHis");
				$final_query="COPY (".$query.") TO 'C:/Bitnami/apache2/htdocs/rheumatic/user/statistics/event_".$dt.".csv' DELIMITER ',';";
				//$final_query="COPY (".$query.") TO 'C:/statistics/event_".$dt.".csv' DELIMITER ',';";
				//echo $final_query;
				$exec = pg_query($final_query);
				if ($exec)
					$msg1="ok";
				
				$parameters[0]=0;
				$parameters[1]=$user_id;
				$parameters[2]='now()';
				$parameters[3]=date_for_postgres($_REQUEST['patient_event_startdate']);
				$parameters[4]=date_for_postgres($_REQUEST['patient_event_enddate']);
				$parameters[5]='now()';
				$parameters[6]="event_".$dt;
				$parameters[7]=2;
				$parameters[8]=$all_centre;
				if($user_all_rpts==0)
					$parameters[9]=$user_centre_id;
				else
				{
					if($all_centre==0)
						$parameters[9]=$user_centre_id;
					else
						$parameters[9]=0;
				}
				
				$table_names[0]='deleted';
				$table_names[1]='editor_id';
				$table_names[2]='edit_date';
				$table_names[3]='start_date';
				$table_names[4]='end_date';
				$table_names[5]='create_date';
				$table_names[6]='filename';
				$table_names[7]='type';
				$table_names[8]='all_centre';
				$table_names[9]='centre_id';
				
				$statistics_id=insert('statistics',$table_names,$parameters,'statistics_id');
				if($statistics_id!='')
					$msg2="ok";
			}
			else if($type==3)
			{
				$patient_bio_startdate=$_REQUEST['patient_bio_startdate'];
				$patient_bio_enddate=$_REQUEST['patient_bio_enddate'];
				
				$query = "(select * from statistics_lk5 order by id asc limit 1 )";
				$query .= " union (select * from statistics_lk5  ";
				
				$query .= " where id<>1 ";
				if($user_all_rpts==1)
				{
					if($all_centre==0)
						$query .= " and a4='$user_centre_id' ";
				}
				else
					$query .= " and a4='$user_centre_id' ";
				
				if($patient_bio_enddate<>"" or $patient_bio_startdate<>"")
				$query .= " and ";
				
				if($patient_bio_startdate<>"")
					$query .= " a3 >= '$patient_bio_startdate' ";

				if($patient_bio_enddate<>"")
				{
					if($patient_bio_startdate<>"")
						$query .= " and a3 <= '$patient_bio_enddate' ";
					else
						$query .= " a3 <= '$patient_bio_enddate' ";
				}

				$query .= " order by id asc )  order by id asc";
				$dt=date("YmdHis");
				$final_query="COPY (".$query.") TO 'C:/Bitnami/apache2/htdocs/rheumatic/user/statistics/biobanking_".$dt.".csv' DELIMITER ',';";
				//echo $final_query;
				$exec = pg_query($final_query);
				if ($exec)
					$msg1="ok";
				
				$parameters[0]=0;
				$parameters[1]=$user_id;
				$parameters[2]='now()';
				$parameters[3]=date_for_postgres($_REQUEST['patient_bio_startdate']);
				$parameters[4]=date_for_postgres($_REQUEST['patient_bio_enddate']);
				$parameters[5]='now()';
				$parameters[6]="biobanking_".$dt;
				$parameters[7]=3;
				$parameters[8]=$all_centre;
				if($user_all_rpts==0)
					$parameters[9]=$user_centre_id;
				else
				{
					if($all_centre==0)
						$parameters[9]=$user_centre_id;
					else
						$parameters[9]=0;
				}
				
				$table_names[0]='deleted';
				$table_names[1]='editor_id';
				$table_names[2]='edit_date';
				$table_names[3]='start_date';
				$table_names[4]='end_date';
				$table_names[5]='create_date';
				$table_names[6]='filename';
				$table_names[7]='type';
				$table_names[8]='all_centre';
				$table_names[9]='centre_id';
				
				$statistics_id=insert('statistics',$table_names,$parameters,'statistics_id');
				if($statistics_id!='')
					$msg2="ok";
			}
			else if($type==4)
			{
				
				$selectdrugs="";
				$selectmonths="";
				$selectcommons="";
				$selectsles="";
				$selectcomors="";
				$selectrest="";
				$numselectdrugs = count($_REQUEST['selectdrugs']);
				$numselectmonths = count($_REQUEST['selectmonth_inc']);
				$numselectcommons = count($_REQUEST['selectcommon']);
				$numselectsles = count($_REQUEST['selectsle']);
				$numselectcomor = count($_REQUEST['selectcomor_inc']);
				$numselectrest = count($_REQUEST['selectrest_inc']);
				
				$i=0;
				$j=0;
				$k=0;
				$l=0;
				$m=0;
				$n=0;
				foreach ($_REQUEST['selectdrugs'] as $selectdrugsOption)
				{
					$selectdrugs .= $selectdrugsOption;
					if (++$i != $numselectdrugs)
						$selectdrugs .= ",";
				}
					
				//echo "selectdrugs:$selectdrugs</br>";
						
				foreach ($_REQUEST['selectmonth_inc'] as $selectmonthOption)
				{
					$selectmonths .= $selectmonthOption;
					if (++$j != $numselectmonths)
						$selectmonths .= ",";
				}
				//echo "selectmonth:$selectmonths</br>";
				
				foreach ($_REQUEST['selectcommon'] as $selectcommonOption)
				{
					$selectcommons .= '"'.$selectcommonOption.'"';
					if (++$k != $numselectcommons)
						$selectcommons .= ',';
				}
				
				foreach ($_REQUEST['selectsle'] as $selectsleOption)
				{
					$selectsles .= '"'.$selectsleOption.'"';
					if (++$l != $numselectsles)
						$selectsles .= ",";
				}
				
				//echo "numselectcomor:$numselectcomor</br>";
				foreach ($_REQUEST['selectcomor_inc'] as $selectcomorOption)
				{
					$selectcomors .= '"'.$selectcomorOption.'"';
					if (++$m != $numselectcomor)
						$selectcomors .= ",";
				}
				
				//echo "numselectrest:$numselectrest</br>";
				foreach ($_REQUEST['selectrest_inc'] as $selectrestOption)
				{
					$selectrest .= '"'.$selectrestOption.'"';
					if (++$n != $numselectrest)
						$selectrest .= ",";
				}
				//echo "selectsle:$selectsles</br>";
				
				/*$query = "(select statistics_lk1.*,statistics_lk2.*,statistics_lk3.* from statistics_lk1 join statistics_lk2 on statistics_lk1.id=statistics_lk2.id join statistics_lk3 on statistics_lk2.id=statistics_lk3.id order by statistics_lk1.id asc limit 1 )";
				$query .= " union (select statistics_lk1.*,statistics_lk2.*,statistics_lk3.* from statistics_lk1 join statistics_lk2 on statistics_lk1.id=statistics_lk2.id join statistics_lk3 on statistics_lk2.id=statistics_lk3.id ";
				
				$query .= " where statistics_lk1.id<>1 ";
				if($user_all_rpts==1)
				{
					if($all_centre==0)
						$query .= " and statistics_lk1.a5='$user_centre_id' ";
				}
				else
					$query .= " and statistics_lk1.a5='$user_centre_id' ";
				
				if($patient_cohort_enddate<>"" or $patient_cohort_startdate<>"")
				$query .= " and ";
			
				if($patient_cohort_startdate<>"")
					$query .= " statistics_lk1.a8 >= '$patient_cohort_startdate' ";

				if($patient_cohort_enddate<>"")
				{
					if($patient_cohort_startdate<>"")
						$query .= " and statistics_lk1.a8 <= '$patient_cohort_enddate' ";
					else
						$query .= " statistics_lk1.a8 <= '$patient_cohort_enddate' ";
				}

				$query .= " order by statistics_lk1.id asc )";
				*/
				
				$query= "SELECT select_detail_inclusion('{".$selectdrugs."}','{".$selectmonths."}','{".$selectcommons."}','{".$selectsles."}','{".$selectcomors."}','{".$selectrest."}',".$user_all_rpts.",".$all_centre.",".$user_centre_id.",";
				
				$patient_inclusion_startdate=date_for_postgres($_REQUEST['patient_inclusion_startdate']);
				$patient_inclusion_enddate=date_for_postgres($_REQUEST['patient_inclusion_enddate']);
				
				if ($patient_inclusion_startdate=="")
						$query .= "NULL,";
				else
						$query .= "'".$patient_inclusion_startdate."',";
				
				if ($patient_inclusion_enddate=="")
					$query .= "NULL)";
				else
					$query .= "'".$patient_inclusion_enddate."')";
				
				echo "query:$query";
				
				$sql = pg_query($query);
				$result = pg_fetch_array($sql);
				$select=$result[0];
				echo "select:$select";
				$dt=date("YmdHis");
				$final_query="COPY (".$select.") TO 'C:/Bitnami/apache2/htdocs/rheumatic/user/statistics/cohort_".$dt.".csv' DELIMITER ',';";
				//echo "final_query:$final_query";
				$exec = pg_query($final_query);
				if ($exec)
					$msg1="ok";
				
				$parameters[0]=0;
				$parameters[1]=$user_id;
				$parameters[2]='now()';
				$parameters[3]=date_for_postgres($_REQUEST['patient_inclusion_startdate']);
				$parameters[4]=date_for_postgres($_REQUEST['patient_inclusion_enddate']);
				$parameters[5]='now()';
				$parameters[6]="cohort_".$dt;
				$parameters[7]=4;
				$parameters[8]=$all_centre;
				if($user_all_rpts==0)
					$parameters[9]=$user_centre_id;
				else
				{
					if($all_centre==0)
						$parameters[9]=$user_centre_id;
					else
						$parameters[9]=0;
				}
					
				
				$table_names[0]='deleted';
				$table_names[1]='editor_id';
				$table_names[2]='edit_date';
				$table_names[3]='start_date';
				$table_names[4]='end_date';
				$table_names[5]='create_date';
				$table_names[6]='filename';
				$table_names[7]='type';
				$table_names[8]='all_centre';
				$table_names[9]='centre_id';
				
				$statistics_id=insert('statistics',$table_names,$parameters,'statistics_id');
				if($statistics_id!='')
					$msg2="ok";
			}
			
			if ($msg1=="ok" and $msg2=="ok")
			{
				$save_chk="ok";
				pg_query("COMMIT") or die("Transaction commit failed\n");
			}
			else
			{
				$save_chk="nok";
				pg_query("ROLLBACK") or die("Transaction rollback failed\n");
			}
			
		}
		catch(exception $e) {
			echo "e:".$e."</br>";
			echo "ROLLBACK";
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
	}
?>
<div class="wrapper">
  <!-- Main Header -->
  <?php
 // include "../portion/header.php";
  ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php
  //include "../portion/sidebar.php";
  ?>

	 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <!-- Content Header (Page header) -->
    <section class="content-header">
       <h4><b>Create new report</b></h4>
	  </section>
			<br>
			 <!-- Main content -->
			<section class="content">
			<div class="alert alert_suc alert-success" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Successful report creation!Please wait for the window to close by itself</strong>
			  </div>
			  <div class="alert alert_wr alert-danger" role="alert" style="DISPLAY:none;">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>Unsuccessful report creation!</strong>
			  </div>
			<form  id="form" action="report.php" method="POST"  enctype="multipart/form-data">
			<br>
				<input type="hidden" id="save" name="save" value="1">
			<div class="row">
				<div class="form-group col-md-4">
				  <label>Select report type</label>
					  <select class="form-control" name="type" id="type">
						<option value="0" <?php if ($type==0) { echo "selected"; } ?>>--</option>
						<option value="1" <?php if ($type==1) { echo "selected"; } ?>>Cohort</option>
						<option value="2" <?php if ($type==2) { echo "selected"; } ?>>Event</option>
						<option value="3" <?php if ($type==3) { echo "selected"; } ?>>Biobanking</option>
						<option value="4" <?php if ($type==4) { echo "selected"; } ?>>Inclusion</option>
					 </select>
				</div>
				<div class="form-group col-md-2">
					&nbsp;
				</div>
				<div class="form-group col-md-4" id="divcentre" name="divcentre" <?php if($user_all_rpts==1)  { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
					<label>From which centres?</label>
					<select class="form-control" name="all_centre" id="all_centre">
						<option value="0" <?php if ($all_centre==0) { echo "selected"; } ?>>Yours</option>
						<option value="1" <?php if ($all_centre==1) { echo "selected"; } ?>>All</option>
					</select>
				</div>
				<div class="form-group col-md-2">
					&nbsp;
				</div>
			</div>
			<div class="row" id="cohort1" <?php if ($type==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
					<div class="form-group col-md-4">
						<label>Patient cohort start date</label>
						<input type="text" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" id="patient_cohort_startdate" name="patient_cohort_startdate" value="<?php echo $patient_cohort_startdate; ?>">
					</div>
					<div class="form-group col-md-2">
					&nbsp;
				</div>
					<div class="form-group col-md-4">
						<label>Patient cohort end date</label>
						<input type="text" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" id="patient_cohort_enddate" name="patient_cohort_enddate" value="<?php echo $patient_cohort_enddate; ?>">
					</div>
					<div class="form-group col-md-2">
					&nbsp;
				</div>
			</div>
			<div class="row" id="inclusion1" <?php if ($type==4) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
					<div class="form-group col-md-4">
						<label>Patient Inclusion start date</label>
						<input type="text" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" id="patient_inclusion_startdate" name="patient_inclusion_startdate" value="<?php echo $patient_inclusion_startdate; ?>">
					</div>
					<div class="form-group col-md-2">
					&nbsp;
				</div>
					<div class="form-group col-md-4">
						<label>Patient Inclusion end date</label>
						<input type="text" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" id="patient_inclusion_enddate" name="patient_inclusion_enddate" value="<?php echo $patient_nclusion_enddate; ?>">
					</div>
					<div class="form-group col-md-2">
					&nbsp;
				</div>
			</div>
			<div class="row" id="cohort2" <?php if ($type==1 or $type==4) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
						<div class="form-group col-md-6">
							<label>Drugs</label>
							<select id="selectdrugs" name="selectdrugs[]" style="width:100%"  multiple="multiple">
							<?php
								$sql = export_drugs();
								$numrows = pg_num_rows($sql);
								while($result2 = pg_fetch_array($sql))
								{
									$drugs_id=$result2['drugs_id'];
									$value = $result2['code'];
									$value .= "-";
									$value .= $result2['substance'];
									$value .= " (";
									$value .= $result2['route_of_administration_val'];
									$value .= ")";	
							?>
								<option value="<?php echo $drugs_id; ?>"><?php echo $value; ?></option>
							<?php
								}
							?>
							</select>
							<input type="checkbox" id="checkboxdrugs" >Select All
						</div>
						<div id="month_cohort" <?php if ($type==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?> class="form-group col-md-6">
							<label>Months</label>
							<select id="selectmonth" name="selectmonth[]" style="width:100%" multiple="multiple">
							  <option>0</option>
							  <option>3</option>
							  <option>6</option>
							  <option>9</option>
							  <option>12</option>
							  <option>15</option>
							  <option>18</option>
							  <option>21</option>
							  <option>24</option>
							  <option>27</option>
							  <option>30</option>
							  <option>33</option>
							  <option>36</option>
							  <option>42</option>
							  <option>48</option>
							  <option>54</option>
							  <option>60</option>
							  <option>66</option>
							  <option>72</option>
							  <option>78</option>
							  <option>84</option>
							  <option>90</option>
							  <option>96</option>
							  <option>102</option>
							  <option>108</option>
							  <option>114</option>
							  <option>120</option>
							</select>
							<input type="checkbox" id="checkboxmonth" >Select All
						</div>
						<div id="month_inclusion" <?php if ($type==4) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?> class="form-group col-md-6">
							<label>Months</label>
							<select id="selectmonth_inc" name="selectmonth_inc[]" style="width:100%" multiple="multiple">
							  <option>0</option>
							  <option>3</option>
							  <option>6</option>
							  <option>9</option>
							  <option>12</option>
							  <option>15</option>
							  <option>18</option>
							  <option>21</option>
							  <option>24</option>
							  <option>27</option>
							  <option>30</option>
							  <option>33</option>
							  <option>36</option>
							  <option>39</option>
							  <option>42</option>
							  <option>45</option>
							  <option>48</option>
							  <option>51</option>
							  <option>54</option>
							  <option>57</option>
							  <option>60</option>
							  <option>63</option>
							  <option>66</option>
							  <option>69</option>
							  <option>72</option>
							  <option>75</option>
							  <option>78</option>
							  <option>81</option>
							  <option>84</option>
							</select>
							<input type="checkbox" id="checkboxmonth_inc" >Select All
						</div>
					</div>
					<div class="row" id="cohort3" <?php if ($type==1 or $type==4) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
						<div class="form-group col-md-6">
								<label>Common Parameters</label>
								<select id="selectcommon" name="selectcommon[]" style="width:100%" multiple="multiple">
									<option value="BSD">BASDAI score</option>
									<option value="BSD1">BASDAI1</option>
									<option value="BSD2">BASDAI2</option>
									<option value="BSD3">BASDAI3</option>
									<option value="BSD4">BASDAI4</option>
									<option value="BSD5">BASDAI5</option>
									<option value="BSD6">BASDAI6</option>
									<option value="BSF">BASFI score</option>
									<option value="CRP">CRP(mg/dl)</option>
									<option value="Dactyl">Dactylitis</option>
									<option value="DAS28CRP">DAS28-CRP</option>
									<option value="DAS28ESR">DAS28-ESR</option>
									<option value="Enthesitis">Enthesitis</option>
									<option value="ESR">ESR(mm/hr)</option>
									<option value="EQ5D">EQ 5D score</option>
									<option value="Euroqol">Euroqol total</option>
									<option value="Hght">Body Height</option>
									<option value="J">J</option>
									<option value="J1">J1</option>
									<option value="J2">J2</option>
									<option value="mHAQ">mHAQ</option>
									<option value="psorarea">Psoriasis area (%)</option>
									<option value="SJC28">28Swollen JC</option>
									<option value="SJC44">44Swollen JC</option>
									<option value="TJC28">28Tender JC</option>
									<option value="SJC44">44Tender JC</option>
									<option value="VASglb">VAS global</option>
									<option value="VASph">VAS physician</option>
									<option value="VASpain">VAS pain</option>
									<option value="Wght">Body Weight</option>
									<option value="w1">WPAI-1</option>
									<option value="w2">WPAI-2</option>
									<option value="w3">WPAI-3</option>
									<option value="w4">WPAI-4</option>
									<option value="w5">WPAI-5</option>
									<option value="w6">WPAI-6</option>
								</select>
							<input type="checkbox" id="checkboxcommon" >Select All
						</div>
						<div class="form-group col-md-6">
							<label>SLE Parameters</label>
							<select id="selectsle" name="selectsle[]" style="width:100%" multiple="multiple">
							  <option value="spga">Physician Global Assessment</option>
							  <option value="s1">SLEDAI1 -Seizure</option>
							  <option value="s2">SLEDAI2 -Psychosis</option>
							  <option value="s3">SLEDAI3 -Confusion/altered mental status</option>
							  <option value="s4">SLEDAI4 -Optic neuritis / retinal exudate</option>
							  <option value="s5">SLEDAI5 -Cranial neuropathy</option>
							  <option value="s6">SLEDAI6 -Headache</option>
							  <option value="s7">SLEDAI7 -Cerebrovascular disease</option>
							  <option value="s8">SLEDAI8 -Vasculitis</option>
							  <option value="s9">SLEDAI9 -Arthritis</option>
							  <option value="s10">SLEDAI10 -Myositis</option>
							  <option value="s11">SLEDAI11 -Urine casts</option>
							  <option value="s12">SLEDAI12 -Hematuria</option>
							  <option value="s13">SLEDAI13 -Proteinuria</option>
							  <option value="s14">SLEDAI14 -Pyuria</option>
							  <option value="s15">SLEDAI15 -Inflammatory skin rash</option>
							  <option value="s16">SLEDAI16 -Hair loss</option>
							  <option value="s17">SLEDAI17 -Mucosal ulcers</option>
							  <option value="s18">SLEDAI18 -Pleural effusion</option>
							  <option value="s19">SLEDAI19 -Pericarditis</option>
							  <option value="s20">SLEDAI20 -Low serum C3/C4</option>
							  <option value="s21">SLEDAI21 -High serum anti-dsDNA</option>
							  <option value="s22">SLEDAI22 -Fever</option>
							  <option value="s23">SLEDAI23 -Thrombocytopenia</option>
							  <option value="s24">SLEDAI24 -Leucopenia</option>
							  <option value="sl2k">SLEDAI-2K</option>
							  <option value="sel">SELENA-SLEDAI Flare Index</option>
							  <option value="cns">CNS activity</option>
							  <option value="prot">Proteinuria (g/24h)</option>
							  <option value="SLdmg">SLICC / ACR Damage Index</option>
							  <option value="slesevind">SLE Severity Index</option>
							  <option value="Neuro">Neuropsychiatric Lupus Outcome</option>
							  <option value="ChrKid">Chronic kidney disease (stage III or higher)</option>
							  <option value="Endstagekid">End-stage kidney disease</option>
							  <option value="KidTransp">Kidney transplantation</option>
							</select>
							<input type="checkbox" id="checkboxsle" >Select All
						</div>
					</div>
					<div class="row" id="cohort4" <?php if ($type==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
						<div class="form-group col-md-6">
							<label>Comorbidities Parameters</label>
							<select id="selectcomor" name="selectcomor[]" style="width:100%" multiple="multiple">
							  <option value="40">Aortic Aneurysm</option>
							  <option value="42">Cardiac arrhythmia</option>
							  <option value="44">Congestive Heart Failure</option>
							  <option value="46">Coronary Heart Disease</option>
							  <option value="48">Acute myocardial infarction</option>
							  <option value="50">Angina pectoris</option>
							  <option value="52">Atrial fibrillation</option>
							  <option value="54">Pericardial disease Acute Pericarditis</option>
							  <option value="56">Hypertension</option>
							  <option value="58">Cardiomyopathies</option>
							  <option value="60">Peripheral vascular disease</option>
							  <option value="62">Valvular Heart Disease</option>
							  <option value="64">Ischemic stroke/TIA</option>
							  <option value="66">Sequelae of ischemic stroke</option>
							  <option value="68">Hemorrhagic stroke</option>
							  <option value="70">Sequelae of hemorrhagic stroke</option>
							  <option value="72">Anxiety disorders</option>
							  <option value="74">Bipolar disorder</option>
							  <option value="76">Dementia (unspecified)</option>
							  <option value="78">Major depression without psychotic symptoms</option>
							  <option value="80">Other depressive episodes</option>
							  <option value="82">Multiple Sclerosis - Demyelinating disease</option>
							  <option value="84">Parkinson's disease</option>
							  <option value="86">Peripheral Neuropathy</option>
							  <option value="88">Psychosis</option>
							  <option value="90">Schizophrenia</option>
							  <option value="92">Asthma</option>
							  <option value="94">Bronchiectasis</option>
							  <option value="96">Chronic obstructive pulmonary disease</option>
							  <option value="98">Chronic sinusitis</option>
							  <option value="100">Interstitial Lung Disease</option>
							  <option value="102">Pulmonary Hypertension</option>
							  <option value="104">Autoimmune hepatitis</option>
							  <option value="106">Alcoholic liver disease</option>
							  <option value="108">Alcoholic liver cirrhosis</option>
							  <option value="110">Nonalcoholic fatty liver disease [NAFLD]</option>
							  <option value="112">Crohns disease</option>
							  <option value="114">Diverticulosis</option>
							  <option value="116">Esophagitis/GERD</option>
							  <option value="118">Peptic ulcer disease (biopsy-proven)</option>
							  <option value="120">Diabetes mellitus</option>
							  <option value="122">Dyslipidaemia</option>
							  <option value="124">Hypothyroidism</option>
							  <option value="126">Hyperthyroidism</option>
							  <option value="128">Obesity</option>
							  <option value="130">Chronic kidney disease stage I/II (GFR>60)</option>
							  <option value="132">Chronic kidney disease stage III/IV (GFR 15-59)</option>
							  <option value="134">End-stage renal disease (GFR <15)</option>
							  <option value="136">Hodgkin Lymphoma</option>
							  <option value="138">Non-Hodgkin Lymphoma (unspecified)</option>
							  <option value="140">Leukemia</option>
							  <option value="142">Solid tumor</option>
							  <option value="144">Solid tumor without metastasis</option>
							  <option value="146">Metastatic solid tumor</option>
							  <option value="148">Metastatic cancer without known primary tumor site</option>
							  <option value="150">Ischemic Optic Neuropathy</option>
							  <option value="152">Optic Neuritis</option>
							  <option value="154">Chronic osteomyelitis</option>
							  <option value="156">Latent TB infection</option>
							  <option value="158">Viral hepatitis</option>
							  <option value="160">Chronic viral infection (other)</option>
							  <option value="162">Past hospitalization for serious infection</option>
							  <option value="164">Recurrent past hospitalizations for serious infection</option>
							  <option value="166">History of opportunistic infection</option>
							  <option value="168">Prosthetic left hip joint</option>
							  <option value="170">Prosthetic right hip joint</option>
							  <option value="172">Prosthetic left knee joint</option>
							  <option value="174">Prosthetic right knee joint</option>
							  <option value="176">Prosthetic joint -other</option>
							</select>
							<input type="checkbox" id="checkboxcomor" >Select All
						</div>
						<div class="form-group col-md-6">
							<label>Rest Parameters</label>
							<select id="selectrest" name="selectrest[]" style="width:100%" multiple="multiple">
							  <option value="sdai">SDAI</option>
							  <option value="cdai">CDAI</option>
							  <option value="crp">ASDAS-CRP(SpA)</option>
							  <option value="esr">ASDAS-ESR SDAI (SpA)</option>
							  <option value="asashi">ASAS-HI</option>
							  <option value="psaid">PSAID</option>
							</select>
							<input type="checkbox" id="checkboxrest" >Select All
						</div>
					</div>
					<div class="row" id="inclusion4" <?php if ($type==4) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
						<div class="form-group col-md-6">
							<label>Comorbidities Parameters</label>
							<select id="selectcomor_inc" name="selectcomor_inc[]" style="width:100%" multiple="multiple">
							  <option value="32">Aortic Aneurysm</option>
							  <option value="34">Cardiac arrhythmia</option>
							  <option value="36">Congestive Heart Failure</option>
							  <option value="38">Coronary Heart Disease</option>
							  <option value="40">Acute myocardial infarction</option>
							  <option value="42">Angina pectoris</option>
							  <option value="44">Atrial fibrillation</option>
							  <option value="46">Pericardial disease Acute Pericarditis</option>
							  <option value="48">Hypertension</option>
							  <option value="50">Cardiomyopathies</option>
							  <option value="52">Peripheral vascular disease</option>
							  <option value="54">Valvular Heart Disease</option>
							  <option value="56">Ischemic stroke/TIA</option>
							  <option value="58">Sequelae of ischemic stroke</option>
							  <option value="60">Hemorrhagic stroke</option>
							  <option value="62">Sequelae of hemorrhagic stroke</option>
							  <option value="64">Anxiety disorders</option>
							  <option value="66">Bipolar disorder</option>
							  <option value="68">Dementia (unspecified)</option>
							  <option value="70">Major depression without psychotic symptoms</option>
							  <option value="72">Other depressive episodes</option>
							  <option value="74">Multiple Sclerosis - Demyelinating disease</option>
							  <option value="76">Parkinson's disease</option>
							  <option value="78">Peripheral Neuropathy</option>
							  <option value="80">Psychosis</option>
							  <option value="82">Schizophrenia</option>
							  <option value="84">Asthma</option>
							  <option value="86">Bronchiectasis</option>
							  <option value="88">Chronic obstructive pulmonary disease</option>
							  <option value="90">Chronic sinusitis</option>
							  <option value="92">Interstitial Lung Disease</option>
							  <option value="94">Pulmonary Hypertension</option>
							  <option value="96">Autoimmune hepatitis</option>
							  <option value="98">Alcoholic liver disease</option>
							  <option value="100">Alcoholic liver cirrhosis</option>
							  <option value="102">Nonalcoholic fatty liver disease [NAFLD]</option>
							  <option value="104">Crohns disease</option>
							  <option value="106">Diverticulosis</option>
							  <option value="108">Esophagitis/GERD</option>
							  <option value="110">Peptic ulcer disease (biopsy-proven)</option>
							  <option value="112">Diabetes mellitus</option>
							  <option value="114">Dyslipidaemia</option>
							  <option value="116">Hypothyroidism</option>
							  <option value="118">Hyperthyroidism</option>
							  <option value="120">Obesity</option>
							  <option value="122">Chronic kidney disease stage I/II (GFR>60)</option>
							  <option value="124">Chronic kidney disease stage III/IV (GFR 15-59)</option>
							  <option value="126">End-stage renal disease (GFR <15)</option>
							  <option value="128">Hodgkin Lymphoma</option>
							  <option value="130">Non-Hodgkin Lymphoma (unspecified)</option>
							  <option value="132">Leukemia</option>
							  <option value="134">Solid tumor</option>
							  <option value="136">Solid tumor without metastasis</option>
							  <option value="138">Metastatic solid tumor</option>
							  <option value="140">Metastatic cancer without known primary tumor site</option>
							  <option value="142">Ischemic Optic Neuropathy</option>
							  <option value="144">Optic Neuritis</option>
							  <option value="146">Chronic osteomyelitis</option>
							  <option value="148">Latent TB infection</option>
							  <option value="150">Viral hepatitis</option>
							  <option value="152">Chronic viral infection (other)</option>
							  <option value="154">Past hospitalization for serious infection</option>
							  <option value="156">Recurrent past hospitalizations for serious infection</option>
							  <option value="158">History of opportunistic infection</option>
							  <option value="160">Prosthetic left hip joint</option>
							  <option value="162">Prosthetic right hip joint</option>
							  <option value="164">Prosthetic left knee joint</option>
							  <option value="166">Prosthetic right knee joint</option>
							  <option value="168">Prosthetic joint -other</option>
							</select>
							<input type="checkbox" id="checkboxcomor_inc" >Select All
						</div>
						<div class="form-group col-md-6">
							<label>Rest Parameters</label>
							<select id="selectrest_inc" name="selectrest_inc[]" style="width:100%" multiple="multiple">
							  <option value="sdai">SDAI</option>
							  <option value="cdai">CDAI</option>
							  <option value="crp">ASDAS-CRP(SpA)</option>
							  <option value="esr">ASDAS-ESR SDAI (SpA)</option>
							  <option value="asashi">ASAS-HI</option>
							  <option value="psaid">PSAID</option>
							</select>
							<input type="checkbox" id="checkboxrest_inc" >Select All
						</div>
					</div>
				<div id="eventdate" <?php if ($type==2) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
					<div class="row">
						<div class="form-group col-md-4">
							<label>Patient event start date</label>
							<input type="text" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" id="patient_event_startdate" name="patient_event_startdate" value="<?php echo $patient_event_startdate; ?>">
						</div>
						<div class="form-group col-md-2">
						&nbsp;
						</div>
						<div class="form-group col-md-4">
							<label>Patient event end date</label>
							<input type="text" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" id="patient_event_enddate" name="patient_event_enddate" value="<?php echo $patient_event_enddate; ?>">
						</div>
					</div>
				</div>
				
				<div class="row" id="biodate" <?php if ($type==3) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
					<div class="form-group col-md-4">
						<label>Patient Biobanking start date</label>
						<input type="text" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" id="patient_bio_startdate" name="patient_bio_startdate" value="<?php echo $patient_bio_startdate; ?>">
					</div>
					<div class="form-group col-md-2">
					&nbsp;
				</div>
					<div class="form-group col-md-4">
						<label>Patient Biobanking end date</label>
						<input type="text" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" id="patient_bio_enddate" name="patient_bio_enddate" value="<?php echo $patient_bio_enddate; ?>">
					</div>
				</div>
				<div class="form-group col-md-2">
					&nbsp;
				</div>
			<div class="row">
				<div class="form-group col-md-12">
					<input class="btn btn-l btn-primary" type="submit" value="OK">
					<input class="btn btn-l btn-primary" type="button" value="Cancel" name="close" onclick="javascript:return closeWin();">
				</div>
			</div>
				</form>
			 </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
//include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>


$(document).ready(function() {
	
	
	$("#selectcommon").select2({
		closeOnSelect : false,
			placeholder : "",
			allowHtml: true,
			allowClear: true,
			tags: true
		});
		
		$("#selectsle").select2({
		closeOnSelect : false,
			placeholder : "",
			allowHtml: true,
			allowClear: true,
			tags: true
		});
		
	$("#selectmonth").select2({
		closeOnSelect : false,
			placeholder : "",
			allowHtml: true,
			allowClear: true,
			tags: true
		});
		
	$("#selectmonth_inc").select2({
		closeOnSelect : false,
			placeholder : "",
			allowHtml: true,
			allowClear: true,
			tags: true
		});
		
	$("#selectdrugs").select2({
		closeOnSelect : false,
			placeholder : "",
			allowHtml: true,
			allowClear: true,
			tags: true
		});
		
		
		$("#selectcomor_inc").select2({
		closeOnSelect : false,
			placeholder : "",
			allowHtml: true,
			allowClear: true,
			tags: true
		});
		
		$("#selectcomor").select2({
		closeOnSelect : false,
			placeholder : "",
			allowHtml: true,
			allowClear: true,
			tags: true
		});
		
		$("#selectrest").select2({
		closeOnSelect : false,
			placeholder : "",
			allowHtml: true,
			allowClear: true,
			tags: true
		});
		
		$("#selectrest_inc").select2({
		closeOnSelect : false,
			placeholder : "",
			allowHtml: true,
			allowClear: true,
			tags: true
		});
		
		$("#checkboxrest_inc").click(function(){
		if($("#checkboxrest_inc").is(':checked') ){
			$("#selectrest_inc > option").prop("selected","selected");
			$("#selectrest_inc").trigger("change");
		}else{
			$("#selectrest_inc > option").prop("selected",false);
			 $("#selectrest_inc").trigger("change");
		 }
	});
	
		$("#checkboxrest").click(function(){
		if($("#checkboxrest").is(':checked') ){
			$("#selectrest > option").prop("selected","selected");
			$("#selectrest").trigger("change");
		}else{
			$("#selectrest > option").prop("selected",false);
			 $("#selectrest").trigger("change");
		 }
	});
	
		$("#checkboxcomor_inc").click(function(){
		if($("#checkboxcomor_inc").is(':checked') ){
			$("#selectcomor_inc > option").prop("selected","selected");
			$("#selectcomor_inc").trigger("change");
		}else{
			$("#selectcomor_inc > option").prop("selected",false);
			 $("#selectcomor_inc").trigger("change");
		 }
	});
	
		$("#checkboxcomor").click(function(){
		if($("#checkboxcomor").is(':checked') ){
			$("#selectcomor > option").prop("selected","selected");
			$("#selectcomor").trigger("change");
		}else{
			$("#selectcomor > option").prop("selected",false);
			 $("#selectcomor").trigger("change");
		 }
	});
	
		$("#checkboxsle").click(function(){
		if($("#checkboxsle").is(':checked') ){
			$("#selectsle > option").prop("selected","selected");
			$("#selectsle").trigger("change");
		}else{
			$("#selectsle > option").prop("selected",false);
			 $("#selectsle").trigger("change");
		 }
	});
	
		$("#checkboxmonth").click(function(){
		if($("#checkboxmonth").is(':checked') ){
			$("#selectmonth > option").prop("selected","selected");
			$("#selectmonth").trigger("change");
		}else{
			$("#selectmonth > option").prop("selected",false);
			 $("#selectmonth").trigger("change");
		 }
	});
	
	$("#checkboxmonth_inc").click(function(){
		if($("#checkboxmonth_inc").is(':checked') ){
			$("#selectmonth_inc > option").prop("selected","selected");
			$("#selectmonth_inc").trigger("change");
		}else{
			$("#selectmonth_inc > option").prop("selected",false);
			 $("#selectmonth_inc").trigger("change");
		 }
	});
	
	$("#checkboxdrugs").click(function(){
		if($("#checkboxdrugs").is(':checked') ){
			$("#selectdrugs > option").prop("selected","selected");
			$("#selectdrugs").trigger("change");
		}else{
			$("#selectdrugs > option").prop("selected",false);
			 $("#selectdrugs").trigger("change");
		 }
	});
	
	$("#checkboxcommon").click(function(){
		if($("#checkboxcommon").is(':checked') ){
			$("#selectcommon > option").prop("selected","selected");
			$("#selectcommon").trigger("change");
		}else{
			$("#selectcommon > option").prop("selected",false);
			 $("#selectcommon").trigger("change");
		 }
	});
});

$("#type").change(function() {
  
	if($("#type").val()==0)
	{
		$("#cohort1").hide();
		$("#inclusion1").hide();
		$("#cohort2").hide();
		$("#cohort3").hide();
		$("#cohort4").hide();
		$("#month_cohort").hide();
		$("#month_inclusion").hide();
		$("#inclusion4").hide();
		$("#eventdate").hide();
		$("#biodate").hide();
		$("#submit").hide();
		
	}
	else if($("#type").val()==1)
	{
		$("#cohort1").show();
		$("#inclusion1").hide();
		$("#cohort2").show();
		$("#cohort3").show();
		$("#cohort4").show();
		$("#month_cohort").show();
		$("#month_inclusion").hide();
		$("#inclusion4").hide();
		$("#eventdate").hide();
		$("#biodate").hide();
		$("#submit").show();
	}
	else if($("#type").val()==2)
	{
		$("#cohort1").hide();
		$("#inclusion1").hide();
		$("#cohort2").hide();
		$("#cohort3").hide();
		$("#cohort4").hide();
		$("#month_cohort").hide();
		$("#month_inclusion").hide();
		$("#inclusion4").hide();
		$("#eventdate").show();
		$("#biodate").hide();
		$("#submit").show();
	}
	else if($("#type").val()==3)
	{
		$("#cohort1").hide();
		$("#cohort2").hide();
		$("#cohort3").hide();
		$("#cohort4").hide();
		$("#month_cohort").hide();
		$("#month_inclusion").hide();
		$("#inclusion4").hide();
		$("#eventdate").hide();
		$("#biodate").show();
		$("#submit").show();
	}
	else if($("#type").val()==4)
	{
		$("#cohort1").hide();
		$("#inclusion1").show();
		$("#cohort2").show();
		$("#cohort3").show();
		$("#cohort4").hide();
		$("#month_cohort").hide();
		$("#month_inclusion").show();
		$("#inclusion4").show();
		$("#eventdate").hide();
		$("#biodate").hide();
		$("#submit").show();
	}
});

	$('#patient_inclusion_startdate').datepicker({
	dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	});
	$('#patient_inclusion_startdate').inputmask();
	
	$('#patient_inclusion_enddate').datepicker({
	dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	});
	$('#patient_inclusion_enddate').inputmask();

	$('#patient_bio_startdate').datepicker({
	dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	});
	$('#patient_bio_startdate').inputmask();
	
	$('#patient_bio_enddate').datepicker({
	dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	});
	$('#patient_bio_enddate').inputmask();
	
	$('#patient_event_startdate').datepicker({
	dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	});
	$('#patient_event_startdate').inputmask();
	
	$('#patient_event_enddate').datepicker({
	dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	});
	$('#patient_event_enddate').inputmask();
	
	$('#patient_cohort_startdate').datepicker({
	dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	});
	$('#patient_cohort_startdate').inputmask();
	
	$('#patient_cohort_enddate').datepicker({
	dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	});
	$('#patient_cohort_enddate').inputmask();
function reload()
{
	
	window.opener.location.reload();
	setInterval(afterreload, 3500);
}

function afterreload()
{
	window.opener.document.getElementById('type').value=<?php if (isset($type)) { echo $type; } else { echo 1; };?>;
	window.opener.trg();
	setTimeout(window.close(), 5000);
}


function closeWin()
{
	return window.close();
}	


  $(function () {
	  
	  $('#form').submit(function(){
		
		var counter_cohort=0;
		var counter_inclusion=0;
		var msg="";
		
		 if ($('#type').val()=='1' &&  (($('#selectdrugs').val()!=''  || $('#selectsle').val()!=''  || $('#selectcommon').val()!=''  || $('#selectrest').val()!='') && $('#selectmonth').val()==''))
			msg +='-Must select at least one month slot!\n'; 
		
		counter_cohort= 217 + 2*$( "#selectdrugs option:selected" ).length*$( "#selectmonth option:selected" ).length + $( "#selectsle option:selected" ).length*$( "#selectmonth option:selected" ).length + $( "#selectcommon option:selected" ).length*$( "#selectmonth option:selected" ).length + $( "#selectrest option:selected" ).length*$( "#selectmonth option:selected" ).length;
		
		if (counter_cohort > 1664)
			msg += 'The number of selected columns (' + counter + ') is greater than the maximum allowed (1664)!\n'; 
		
		if ($('#type').val()=='4' &&  (($('#selectdrugs').val()!=''  || $('#selectsle').val()!=''  || $('#selectcommon').val()!=''  || $('#selectrest_inc').val()!='') && $('#selectmonth_inc').val()==''))
			msg +='-Must select at least one month slot!\n'; 
		
		counter_cohort= /*217*/ 280 + 2*$( "#selectdrugs option:selected" ).length*$( "#selectmonth_inc option:selected" ).length + $( "#selectsle option:selected" ).length*$( "#selectmonth_inc option:selected" ).length + $( "#selectcommon option:selected" ).length*$( "#selectmonth option:selected" ).length + $( "#selectrest_inc option:selected" ).length*$( "#selectmonth option:selected" ).length;
		
		if (counter_cohort > 1664)
			msg += 'The number of selected columns (' + counter + ') is greater than the maximum allowed (1664)!\n'; 
		
		
		if(msg!="")
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
		
	  });
</script>
<?php
if($save==1)
{
	if($save_chk=="ok")
	{
?>
<script>
$(".alert_suc").show();
window.setTimeout(function() {
    $(".alert_suc").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);

reload();
//window.opener.location.reload();
//setTimeout(window.close(), 5000);

</script>
<?php
	}
	else
	{
?>
<script>
$(".alert_wr").show();
window.setTimeout(function() {
    $(".alert_wr").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
//window.close();
</script>
<?php
	}
}
?>
</body>
</html>