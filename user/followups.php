		
<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	$pat_id=$_REQUEST['pat_id'];
	if($pat_id<>"")
		$_SESSION['pat_id']=$pat_id;
	
	$pat_id=$_SESSION['pat_id'];
	
	$patient_cohort_id=$_REQUEST['patient_cohort_id'];
	if($patient_cohort_id<>"")
		$_SESSION['patient_cohort_id']=$patient_cohort_id;
	
	$patient_cohort_id=$_SESSION['patient_cohort_id'];
	
	$_SESSION['patient_followup_id']="";
	$_SESSION['patient_event_id']="";
	
	$save=$_REQUEST['save'];
	$patient_followup_id=$_REQUEST['patient_followup_id'];
	
	if($save==1 and $patient_followup_id<>"")
	{
		pg_query("BEGIN") or die("Could not start transaction\n");
		try
		{
			$table_names[0]='deleted';
			$parameters[0]=1;
			$edit_name[0]='patient_followup_id';
			$edit_id[0]=$patient_followup_id;
			$sumbol[0]='=';
			$msg_patient_corticosteroids=update('patient_corticosteroids',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
			$msg_patient_lookup_drugs=update('patient_lookup_drugs',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
			$msg_patient_event=update('patient_event',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
			$msg_patient_followup=update('patient_followup',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
			$msg_patient_followup_drugs=update('patient_followup_drugs',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
			$msg_patient_followup_drugs_period=update('patient_followup_drugs_period',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
			
			
			if ($msg_patient_corticosteroids=="ok" and $msg_patient_lookup_drugs=="ok" and $msg_patient_event=="ok" and $msg_patient_followup=="ok" and $msg_patient_followup_drugs=="ok" and $msg_patient_followup_drugs_period=="ok")
			{
				$save_chk="ok";
				pg_query("COMMIT") or die("Transaction commit failed\n");
			}
			else
			{
				$save_chk="nok";
				pg_query("ROLLBACK") or die("Transaction rollback failed\n");
			}
			
		}
		catch(exception $e) {
			echo "e:".$e."</br>";
			echo "ROLLBACK";
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
	}
	
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
		<h1>
			Follow ups</br>
         </h1>
		 <h1>
          <?php
				$exec = get_patient_demographics($pat_id);
				$result = pg_fetch_array($exec);
				$dateofinclusion=$result['dateofinclusion_str'];
				if($dateofinclusion=="12-12-1900")
					$dateofinclusion="";
				$dateofbirth=$result['dateofbirth_str'];
				if($dateofbirth=="12-12-1900")
					$dateofbirth="";
				$gender=$result['gender'];
				if($gender==1)
					$gender_str="Female";
				else if($gender==2)
					$gender_str="Male";
				$patient_id=$result['patient_id'];
				$order = get_cohort($patient_cohort_id);
				$result = pg_fetch_assoc($order);
				$cohort_name_str=$result['cohort_name_str'];
				$start_date=$result['start_date_str'];
				if($start_date=="12-12-1900")
					$start_date="";
				$stop_date=$result['stop_date_str'];
				if($stop_date=="12-12-1900")
					$stop_date="-";
				
				echo "<small>Patient:".$patient_id.",".$gender_str." (".$dateofbirth.") </small></br>";
				echo "<small>Cohort:".$cohort_name_str.", start date:".$start_date." end date:".$stop_date."</small>"; ?>
        </h1>
      </section>
      <!-- Main content -->
      <section class="content">
	  <form id="form" action="followups.php" method="POST">
	  <input type="hidden" id="save" name="save" value="1">
		<input type="hidden" id="pat_id" name="pat_id" value="<?php echo $pat_id;?>">
		<input type="hidden" id="patient_cohort_id" name="patient_cohort_id" value="<?php echo $patient_cohort_id;?>">
		<input type="hidden" id="patient_followup_id" name="patient_followup_id" value="<?php echo $patient_followup_id;?>">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Follow ups</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-12" id="divdiagnosis" name="divdiagnosis">
								<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="patient_biobanking_tbl" name="patient_biobanking_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-3"><b>Cohort Treatment</b></th>
											<th class="col-md-3"><b>Followup date</b></th>
											<th class="col-md-2"><b>FU cohort month</b></th>
											<th class="col-md-2"><b>Physician assigning</b></th>
											<th class="col-md-1">&nbsp;</th>
											<th class="col-md-1">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
											$order = get_followups($patient_cohort_id);
											$num_rows = pg_num_rows($order);
											
											while($result = pg_fetch_assoc($order))
											{
												$patient_followup_id=$result['patient_followup_id'];
												$start_date=$result['start_date_str'];
												if($start_date=="12-12-1900")
													$start_date="";
												$cohort_name_str=$result['cohort_name_str'];
												$physician_assigning=$result['physician_assigning'];
												$fumonthcohort=$result['fumonthcohort'];
												
												$sql = get_lookup_tbl_value($physician_assigning);
												$result2 = pg_fetch_array($sql);
												$value=$result2['value'];
												
											
									?>
										  <tr id="patient_biobanking_tr_<?php echo $i;?>"> 
												<td align="center">
													<label><?php echo $cohort_name_str; ?></label>
												</td>
												<td align="center">
													<label><?php echo $start_date; ?></label>
												</td>
												<td align="center">
													<label><?php echo $fumonthcohort; ?></label>
												</td>
												<td align="center">
													<label><?php echo $value; ?></label>
												</td>
												<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="deletefollowup(<?php echo $patient_followup_id; ?>)"><i class="fa fa-trash"></i><span></a>
												  </td>
													<td align="center">
													<a title="Edit" href="followup_edit.php?patient_followup_id=<?php echo $patient_followup_id; ?>"><i class="fa fa-edit"></i><span></a>
												  </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										
									?>
									</tbody>
								</table>
								<br>
								 <a class="btn btn-primary" href="followup_edit.php"><i class="fa fa-plus"></i> New follow up</a>
							 </div>
						</div>
					</div>
				</div><!-- diagnoses status div -->
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				&nbsp;
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				&nbsp;
			</div>
		</div>
		</form>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
   <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<?php

if($save==1)
{
	if($save_chk=="ok")
	{
?>
<script>
alert("Record deleted!");
</script>
<?php
	}
	else
	{
?>
<script>
alert("unsuccessful record deletion!");
</script>
<?php
	}
}
?>
<script>

function deletefollowup(patient_followup_id)
{
	if (confirm("Are you sure about this action? All data available for this record will be deleted."))
	{
		$("#patient_followup_id").val(patient_followup_id);
		document.getElementById('form').submit();
	}
	else
	{
		return false;
	}
}
</script>
</body>
</html>
