﻿<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	
	$save=$_REQUEST['save'];
	$pat_id=$_REQUEST['pat_id'];
	$row=$_REQUEST['row'];
	$patient_lupus_nefritis_id=$_REQUEST['patient_lupus_nefritis_id'];
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>
</head>
<body class="hold-transition skin-blue layout-top-nav">
<?php
	if($save==1)
	{
		$patient_lupus_parameters[0]=date_for_postgres($_REQUEST['patient_lupus_nefritis_date']);
		$patient_lupus_parameters[1]=$_REQUEST['patient_lupus_nefritis_biopsy_diagnosis'];
		if ($_REQUEST['patient_lupus_nefritis_biopsy_diagnosis']==1)
		{
			$patient_lupus_parameters[2]=$_REQUEST['patient_lupus_nefritis_histological_class'];
			$patient_lupus_parameters[3]=$_REQUEST['patient_lupus_nefritis_fibrinoid_necrosis'];
			$patient_lupus_parameters[4]=$_REQUEST['patient_lupus_nefritis_cellular_crescents'];
			$patient_lupus_parameters[5]=$_REQUEST['patient_lupus_nefritis_hypercellularity'];
			$patient_lupus_parameters[6]=$_REQUEST['patient_lupus_nefritis_neutrophils'];
			$patient_lupus_parameters[7]=$_REQUEST['patient_lupus_nefritis_inflammation'];
			$patient_lupus_parameters[8]=$_REQUEST['patient_lupus_nefritis_fibrosis'];
			$patient_lupus_parameters[9]=$_REQUEST['patient_lupus_nefritis_tubular'];
			$patient_lupus_parameters[10]=$_REQUEST['patient_lupus_nefritis_sclerosis'];
			$patient_lupus_parameters[11]=$_REQUEST['patient_lupus_nefritis_fibrocellular'];
			$patient_lupus_parameters[12]=$_REQUEST['patient_lupus_nefritis_nih_activity_index'];
			$patient_lupus_parameters[13]=$_REQUEST['patient_lupus_nefritis_nih_chronity_index'];
			$patient_lupus_parameters[14]=$_REQUEST['patient_lupus_nefritis_throbotic'];
			$patient_lupus_parameters[15]=$_REQUEST['patient_lupus_nefritis_intimal'];
			$patient_lupus_parameters[16]=$_REQUEST['patient_lupus_nefritis_organizing_thrombi'];
			$patient_lupus_parameters[17]=$_REQUEST['patient_lupus_nefritis_focal_cortical'];
			$patient_lupus_parameters[18]=$_REQUEST['patient_lupus_nefritis_fibrous'];
		}
		else
		{
			$patient_lupus_parameters[2]=0;
			$patient_lupus_parameters[3]=0;
			$patient_lupus_parameters[4]=0;
			$patient_lupus_parameters[5]=0;
			$patient_lupus_parameters[6]=0;
			$patient_lupus_parameters[7]=0;
			$patient_lupus_parameters[8]=0;
			$patient_lupus_parameters[9]=0;
			$patient_lupus_parameters[10]=0;
			$patient_lupus_parameters[11]=0;
			$patient_lupus_parameters[12]=0;
			$patient_lupus_parameters[13]=0;
			$patient_lupus_parameters[14]=0;
			$patient_lupus_parameters[15]=0;
			$patient_lupus_parameters[16]=0;
			$patient_lupus_parameters[17]=0;
			$patient_lupus_parameters[18]=0;
		}
		
		$patient_lupus_parameters[19]=$user_id;
		$patient_lupus_parameters[20]='now()';
				
		$patient_lupus_table_names[0]='patient_lupus_nefritis_date';
		$patient_lupus_table_names[1]='patient_lupus_nefritis_biopsy_diagnosis';
		$patient_lupus_table_names[2]='patient_lupus_nefritis_histological_class';
		$patient_lupus_table_names[3]='patient_lupus_nefritis_fibrinoid_necrosis';
		$patient_lupus_table_names[4]='patient_lupus_nefritis_cellular_crescents';
		$patient_lupus_table_names[5]='patient_lupus_nefritis_hypercellularity';
		$patient_lupus_table_names[6]='patient_lupus_nefritis_neutrophils';
		$patient_lupus_table_names[7]='patient_lupus_nefritis_inflammation';
		$patient_lupus_table_names[8]='patient_lupus_nefritis_fibrosis';
		$patient_lupus_table_names[9]='patient_lupus_nefritis_tubular';
		$patient_lupus_table_names[10]='patient_lupus_nefritis_sclerosis';
		$patient_lupus_table_names[11]='patient_lupus_nefritis_fibrocellular';
		$patient_lupus_table_names[12]='patient_lupus_nefritis_nih_activity_index';
		$patient_lupus_table_names[13]='patient_lupus_nefritis_nih_chronity_index';
		$patient_lupus_table_names[14]='patient_lupus_nefritis_throbotic';
		$patient_lupus_table_names[15]='patient_lupus_nefritis_intimal';
		$patient_lupus_table_names[16]='patient_lupus_nefritis_organizing_thrombi';
		$patient_lupus_table_names[17]='patient_lupus_nefritis_focal_cortical';
		$patient_lupus_table_names[18]='patient_lupus_nefritis_fibrous';
		$patient_lupus_table_names[19]='editor_id';
		$patient_lupus_table_names[20]='edit_date';
				
		$patient_lupus_edit_name[0]='patient_lupus_nefritis_id';
		$patient_lupus_code_edit_id[0]=$patient_lupus_nefritis_id;
		$patient_lupus_code_sumbol[0]='=';
		if($patient_lupus_nefritis_id=='')
		{
			$patient_lupus_parameters[21]=$pat_id;
			$patient_lupus_parameters[22]=0;
			$patient_lupus_table_names[21]='pat_id';
			$patient_lupus_table_names[22]='deleted';
				
			$patient_lupus_nefritis_id=insert('patient_lupus_nefritis',$patient_lupus_table_names,$patient_lupus_parameters,'patient_lupus_nefritis_id');
			if($patient_lupus_nefritis_id!='')
				$save_chk="ok";
		}
		else
			$save_chk=update('patient_lupus_nefritis',$patient_lupus_table_names,$patient_lupus_parameters,$patient_lupus_edit_name,$patient_lupus_code_edit_id,$patient_lupus_code_sumbol);
		
	}
?>
<div class="wrapper">
  <!-- Main Header -->
  <?php
 // include "../portion/header.php";
  ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php
  //include "../portion/sidebar.php";
  ?>

	 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <!-- Content Header (Page header) -->
    <section class="content-header">
       <h4><b><?php if($patient_lupus_nefritis_id=='') { echo 'Add'; } else { echo 'Edit'; } ?> lupus nephritis</b></h4>
	  </section>
			<br>
			 <!-- Main content -->
			<section class="content">
			<div class="alert alert_suc alert-success" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Successful save!</strong>
			  </div>
			  <div class="alert alert_wr alert-danger" role="alert" style="DISPLAY:none;">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>Unsuccessful save!</strong>
			  </div>
			<form  id="form" action="lupus.php" method="POST"  enctype="multipart/form-data">
			<br>
			<?php
				if($patient_lupus_nefritis_id!="")
				{
					$exec = get_patient_lupus_nefritis_spec($patient_lupus_nefritis_id);
					$result = pg_fetch_array($exec);
					
					$patient_lupus_nefritis_date=$result['patient_lupus_nefritis_date_str'];
					if($patient_lupus_nefritis_date=='12-12-1900')
						$patient_lupus_nefritis_date='';
					$patient_lupus_nefritis_biopsy_diagnosis=$result['patient_lupus_nefritis_biopsy_diagnosis'];
					$patient_lupus_nefritis_histological_class=$result['patient_lupus_nefritis_histological_class'];
					$patient_lupus_nefritis_fibrinoid_necrosis=$result['patient_lupus_nefritis_fibrinoid_necrosis'];
					$patient_lupus_nefritis_cellular_crescents=$result['patient_lupus_nefritis_cellular_crescents'];
					$patient_lupus_nefritis_hypercellularity=$result['patient_lupus_nefritis_hypercellularity'];
					$patient_lupus_nefritis_neutrophils=$result['patient_lupus_nefritis_neutrophils'];
					$patient_lupus_nefritis_inflammation=$result['patient_lupus_nefritis_inflammation'];
					$patient_lupus_nefritis_fibrosis=$result['patient_lupus_nefritis_fibrosis'];
					$patient_lupus_nefritis_tubular=$result['patient_lupus_nefritis_tubular'];
					$patient_lupus_nefritis_sclerosis=$result['patient_lupus_nefritis_sclerosis'];
					$patient_lupus_nefritis_fibrocellular=$result['patient_lupus_nefritis_fibrocellular'];
					$patient_lupus_nefritis_nih_activity_index=$result['patient_lupus_nefritis_nih_activity_index'];
					$patient_lupus_nefritis_nih_chronity_index=$result['patient_lupus_nefritis_nih_chronity_index'];
					$patient_lupus_nefritis_throbotic=$result['patient_lupus_nefritis_throbotic'];
					$patient_lupus_nefritis_intimal=$result['patient_lupus_nefritis_intimal'];
					$patient_lupus_nefritis_organizing_thrombi=$result['patient_lupus_nefritis_organizing_thrombi'];
					$patient_lupus_nefritis_focal_cortical=$result['patient_lupus_nefritis_focal_cortical'];
					$patient_lupus_nefritis_fibrous=$result['patient_lupus_nefritis_fibrous'];
				}
				?>
			<input type="hidden" id="save" name="save" value="1">
			<input type="hidden" id="patient_lupus_nefritis_id" name="patient_lupus_nefritis_id" value="<?php echo $patient_lupus_nefritis_id;?>">
			<input type="hidden" id="row" name="row" value="<?php echo $row;?>">
			<input type="hidden" id="pat_id" name="pat_id" value="<?php echo $pat_id;?>">
			<div class="row">
				<div class="form-group col-md-6">
				  <label>*Kidney biopsy date</label>
					<input type="text" readonly="true" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="patient_lupus_nefritis_date" name="patient_lupus_nefritis_date" value="<?php echo $patient_lupus_nefritis_date; ?>">
				</div>
				<div class="form-group col-md-6">
				  <label>*Biopsy diagnosis</label>
					<select class="form-control" name="patient_lupus_nefritis_biopsy_diagnosis" id="patient_lupus_nefritis_biopsy_diagnosis">
						<option value="0">--</option>
						<option value="1" <?php if($patient_lupus_nefritis_biopsy_diagnosis==1) { echo "selected"; } ?>>Lupus Nephritis</option>
						<option value="2" <?php if($patient_lupus_nefritis_biopsy_diagnosis==2) { echo "selected"; } ?>>IgA nephropathy</option>
						<option value="3" <?php if($patient_lupus_nefritis_biopsy_diagnosis==3) { echo "selected"; } ?>>FSGS</option>
						<option value="4" <?php if($patient_lupus_nefritis_biopsy_diagnosis==4) { echo "selected"; } ?>>Membranoproliferative nephritis</option>
						<option value="5" <?php if($patient_lupus_nefritis_biopsy_diagnosis==5) { echo "selected"; } ?>>Other kidney disease</option>
					</select>
				</div>
			</div>
			<div id="lupus_nefritis" name="lupus_nefritis" <?php if($patient_lupus_nefritis_biopsy_diagnosis==1) { echo 'style="DISPLAY:block;" '; } else { echo 'style="DISPLAY:none;" '; } ?>>
				<div class="row">
					<div class="form-group col-md-6">
					  <label>Lupus Nephritis Historical Class</label>
						<select class="form-control" name="patient_lupus_nefritis_histological_class" id="patient_lupus_nefritis_histological_class">
							<option value="0">--</option>
							<option value="1" <?php if($patient_lupus_nefritis_histological_class==1) { echo "selected"; } ?>>Class I</option>
							<option value="2" <?php if($patient_lupus_nefritis_histological_class==2) { echo "selected"; } ?>>Class II</option>
							<option value="3" <?php if($patient_lupus_nefritis_histological_class==3) { echo "selected"; } ?>>Class III (subtypes: A, C, A/C)</option>
							<option value="4" <?php if($patient_lupus_nefritis_histological_class==4) { echo "selected"; } ?>>Class IV (subtypes: A, C, A/C)</option>
							<option value="5" <?php if($patient_lupus_nefritis_histological_class==5) { echo "selected"; } ?>>Class V</option>
							<option value="6" <?php if($patient_lupus_nefritis_histological_class==6) { echo "selected"; } ?>>Class V + III (subtypes: A, C, A/C)</option>
							<option value="7" <?php if($patient_lupus_nefritis_histological_class==7) { echo "selected"; } ?>>Class V + IV (subtypes: A, C, A/C)</option>
						</select>
					</div>
					<div class="form-group col-md-6">
						&nbsp;
					</div>
				</div>
				<fieldset>
				<legend>Kidney lesions and severity</legend>
				<div class="row">
					<div class="form-group col-md-3">
						<label>Fibrinoid necrosis</label>
						<select class="form-control" name="patient_lupus_nefritis_fibrinoid_necrosis" id="patient_lupus_nefritis_fibrinoid_necrosis">
							<option value="0">--</option>
							<option value="1" <?php if($patient_lupus_nefritis_fibrinoid_necrosis==1) { echo "selected"; } ?>>Yes</option>
							<option value="2" <?php if($patient_lupus_nefritis_fibrinoid_necrosis==2) { echo "selected"; } ?>>No</option>
						</select>
					</div>
					<div class="form-group col-md-3">
						<label>Fibrinoid necrosis</label>
						<select class="form-control" name="patient_lupus_nefritis_cellular_crescents" id="patient_lupus_nefritis_cellular_crescents">
							<option value="0">--</option>
							<option value="1" <?php if($patient_lupus_nefritis_cellular_crescents==1) { echo "selected"; } ?>>Yes</option>
							<option value="2" <?php if($patient_lupus_nefritis_cellular_crescents==2) { echo "selected"; } ?>>No</option>
						</select>
					</div>
					<div class="form-group col-md-3">
						<label>Hypercellularity(endoc.)</label>
						<select class="form-control" name="patient_lupus_nefritis_hypercellularity" id="patient_lupus_nefritis_hypercellularity">
							<option value="0">--</option>
							<option value="1" <?php if($patient_lupus_nefritis_hypercellularity==1) { echo "selected"; } ?>>Yes</option>
							<option value="2" <?php if($patient_lupus_nefritis_hypercellularity==2) { echo "selected"; } ?>>No</option>
						</select>
					</div>
					<div class="form-group col-md-3">
						<label>Glomerular neutrophils</label>
						<select class="form-control" name="patient_lupus_nefritis_neutrophils" id="patient_lupus_nefritis_neutrophils">
							<option value="0">--</option>
							<option value="1" <?php if($patient_lupus_nefritis_neutrophils==1) { echo "selected"; } ?>>Yes</option>
							<option value="2" <?php if($patient_lupus_nefritis_neutrophils==2) { echo "selected"; } ?>>No</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-3">
						<label>Interstitial inflammation</label>
						<select class="form-control" name="patient_lupus_nefritis_inflammation" id="patient_lupus_nefritis_inflammation">
							<option value="0">--</option>
							<option value="1" <?php if($patient_lupus_nefritis_inflammation==1) { echo "selected"; } ?>>Yes</option>
							<option value="2" <?php if($patient_lupus_nefritis_inflammation==2) { echo "selected"; } ?>>No</option>
						</select>
					</div>
					<div class="form-group col-md-3">
						<label>Interstitial fibrosis</label>
						<select class="form-control" name="patient_lupus_nefritis_fibrosis" id="patient_lupus_nefritis_fibrosis">
							<option value="0">--</option>
							<option value="1" <?php if($patient_lupus_nefritis_fibrosis==1) { echo "selected"; } ?>>Yes</option>
							<option value="2" <?php if($patient_lupus_nefritis_fibrosis==2) { echo "selected"; } ?>>No</option>
						</select>
					</div>
					<div class="form-group col-md-3">
						<label>Tubular atrophy</label>
						<select class="form-control" name="patient_lupus_nefritis_tubular" id="patient_lupus_nefritis_tubular">
							<option value="0">--</option>
							<option value="1" <?php if($patient_lupus_nefritis_tubular==1) { echo "selected"; } ?>>Yes</option>
							<option value="2" <?php if($patient_lupus_nefritis_tubular==2) { echo "selected"; } ?>>No</option>
						</select>
					</div>
					<div class="form-group col-md-3">
						<label>Glomerular sclerosis</label>
						<select class="form-control" name="patient_lupus_nefritis_sclerosis" id="patient_lupus_nefritis_sclerosis">
							<option value="0">--</option>
							<option value="1" <?php if($patient_lupus_nefritis_sclerosis==1) { echo "selected"; } ?>>Yes</option>
							<option value="2" <?php if($patient_lupus_nefritis_sclerosis==2) { echo "selected"; } ?>>No</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-3">
						<label>Fibrocellular crescents</label>
						<select class="form-control" name="patient_lupus_nefritis_fibrocellular" id="patient_lupus_nefritis_fibrocellular">
							<option value="0">--</option>
							<option value="1" <?php if($patient_lupus_nefritis_fibrocellular==1) { echo "selected"; } ?>>Yes</option>
							<option value="2" <?php if($patient_lupus_nefritis_fibrocellular==2) { echo "selected"; } ?>>No</option>
						</select>
					</div>
					<div class="form-group col-md-3">
						<label>NIH activity index</label>
						<select class="form-control" name="patient_lupus_nefritis_nih_activity_index" id="patient_lupus_nefritis_nih_activity_index">
							<option value="0">--</option>
							<?php
								for($i=1;$i<=24;$i++)
								{
							?>
									<option value="<?php echo $i; ?>" <?php if($patient_lupus_nefritis_nih_activity_index==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
							<?php
								}
							?>
						</select>
					</div>
					<div class="form-group col-md-3">
						<label>NIH chronicity index</label>
						<select class="form-control" name="patient_lupus_nefritis_nih_chronity_index" id="patient_lupus_nefritis_nih_chronity_index">
							<option value="0">--</option>
							<?php
								for($i=1;$i<=12;$i++)
								{
							?>
									<option value="<?php echo $i; ?>" <?php if($patient_lupus_nefritis_nih_chronity_index==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
							<?php
								}
							?>
						</select>
					</div>
					<div class="form-group col-md-3">
						&nbsp;
					</div>
				</div>
				</fieldset>
				<fieldset>
				<legend>APS-nephropathy lesions</legend>
					<div class="row">
					<div class="form-group col-md-3">
						<label>Thrombotic microangiopathy</label>
						<select class="form-control" name="patient_lupus_nefritis_throbotic" id="patient_lupus_nefritis_throbotic">
							<option value="0">--</option>
							<option value="1" <?php if($patient_lupus_nefritis_throbotic==1) { echo "selected"; } ?>>Yes</option>
							<option value="2" <?php if($patient_lupus_nefritis_throbotic==2) { echo "selected"; } ?>>No</option>
						</select>
					</div>
					<div class="form-group col-md-3">
						<label>Fibrous Intimal Hyperplasia</label>
						<select class="form-control" name="patient_lupus_nefritis_intimal" id="patient_lupus_nefritis_intimal">
							<option value="0">--</option>
							<option value="1" <?php if($patient_lupus_nefritis_intimal==1) { echo "selected"; } ?>>Yes</option>
							<option value="2" <?php if($patient_lupus_nefritis_intimal==2) { echo "selected"; } ?>>No</option>
						</select>
					</div>
					<div class="form-group col-md-6">
						<label>Organizing thrombi with recanalization</label>
						<select class="form-control" name="patient_lupus_nefritis_organizing_thrombi" id="patient_lupus_nefritis_organizing_thrombi" style="width:50%;">
							<option value="0">--</option>
							<option value="1" <?php if($patient_lupus_nefritis_organizing_thrombi==1) { echo "selected"; } ?>>Yes</option>
							<option value="2" <?php if($patient_lupus_nefritis_organizing_thrombi==2) { echo "selected"; } ?>>No</option>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-3">
						<label>Focal cortical atrophy</label>
						<select class="form-control" name="patient_lupus_nefritis_focal_cortical" id="patient_lupus_nefritis_focal_cortical">
							<option value="0">--</option>
							<option value="1" <?php if($patient_lupus_nefritis_focal_cortical==1) { echo "selected"; } ?>>Yes</option>
							<option value="2" <?php if($patient_lupus_nefritis_focal_cortical==2) { echo "selected"; } ?>>No</option>
						</select>
					</div>
					<div class="form-group col-md-6">
						<label>Fibrous occlusions of arteries/arterioles</label>
						<select class="form-control" name="patient_lupus_nefritis_fibrous" id="patient_lupus_nefritis_fibrous" style="width:50%;">
							<option value="0">--</option>
							<option value="1" <?php if($patient_lupus_nefritis_fibrous==1) { echo "selected"; } ?>>Yes</option>
							<option value="2" <?php if($patient_lupus_nefritis_fibrous==2) { echo "selected"; } ?>>No</option>
						</select>
					</div>
					<div class="form-group col-md-3">
						&nbsp;
					</div>
				</DIV>
				</fieldset>
			</div>
			
			<div class="row">
				<div class="form-group col-md-12">
					<input class="btn btn-l btn-primary" type="submit" value="OK">
					<input class="btn btn-l btn-primary" type="button" value="Cancel" name="close" onclick="javascript:return closeWin();">
				</div>
			</div>
				</form>
			 </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
//include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>
function reload()
{
	//if (confirm('Έχετε αποθηκεύσει τα δεδομένα σας;Το παράθυρο θα κλείσει!'))
	//{
		var row=$("#row").val();
		if($("#row").val()!="")
		{
			var newrow=row;
			window.opener.document.getElementById('patient_lupus_nefritis_biopsy_diagnosis_'+newrow).value=$("#patient_lupus_nefritis_biopsy_diagnosis option:selected").text();
			window.opener.document.getElementById('patient_lupus_nefritis_date_'+newrow).value=$("#patient_lupus_nefritis_date").val();
		}
		else
		{
			window.opener.patient_lupus_nefritis_addrow($("#pat_id").val(),$("#patient_lupus_nefritis_id").val());
			$("#row").val(window.opener.document.getElementById('patient_lupus_nefritis_numofrows').value);
			var newrow=$("#row").val();
			window.opener.document.getElementById('patient_lupus_nefritis_biopsy_diagnosis_'+newrow).value=$("#patient_lupus_nefritis_biopsy_diagnosis option:selected").text();
			window.opener.document.getElementById('patient_lupus_nefritis_date_'+newrow).value=$("#patient_lupus_nefritis_date").val();
			window.opener.document.getElementById('patient_lupus_nefritis_id_'+newrow).value=$("#patient_lupus_nefritis_id").val();
			
		}
		
	//}
	//else
	//	return false;
}


$( document ).ready(function() {
	
	$('#patient_lupus_nefritis_date').datepicker({
	  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		onClose2:function(dateText) {}
	})
	$('#patient_lupus_nefritis_date').inputmask();
	
	
	$('#patient_lupus_nefritis_biopsy_diagnosis').change(function(){ 
		
		if($('#patient_lupus_nefritis_biopsy_diagnosis').val()==1)
			$('#lupus_nefritis').show();
		else
			$('#lupus_nefritis').hide();
			
	});
});

function closeWin()
{
	return window.close();
}	


  $(function () {
	  
	  $('#form').submit(function(){
		
		var msg="";
		 
		 if($('#patient_lupus_nefritis_date').val()=='')
			msg +='-Fill Kidney biopsy date!\n'; 
		
		if($('#patient_lupus_nefritis_biopsy_diagnosis').val()=='0')
			msg +='-Select Biopsy diagnosis!\n'; 
		
		if(msg != '')
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
		
	  });	
</script>
<?php
if($save==1)
{
	if($save_chk=="ok")
	{
?>
<script>
$(".alert_suc").show();
window.setTimeout(function() {
    $(".alert_suc").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);

reload();
window.close();
</script>
<?php
	}
	else
	{
?>
<script>
$(".alert_wr").show();
window.setTimeout(function() {
    $(".alert_wr").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
window.close();
</script>
<?php
	}
}
?>
</body>
</html>