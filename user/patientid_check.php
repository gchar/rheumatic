<?php
$pat_id = $_REQUEST['pat_id'];
$patient_id = $_REQUEST['patient_id'];
	
include '../library/config.php';
include '../library/openDB.php';
include '../library/functions.php';
include '../library/JSON.php';


$user_centre_id=$_SESSION['user_centre_id'];
	$populate_arr = array();
	
	if($pat_id<>"")
		$query="select gender,TO_CHAR(dateofbirth, 'DD-MM-YYYY') AS dateofbirth_str from patient where deleted=0 and pat_id<>'$pat_id' and patient_id=$patient_id and centre=$user_centre_id";
	else
		$query="select gender,TO_CHAR(dateofbirth, 'DD-MM-YYYY') AS dateofbirth_str from patient where deleted=0 and patient_id=$patient_id and centre=$user_centre_id";
	
	
	$exec = pg_query($query);
	//$result = pg_fetch_array($exec);

	while($result = pg_fetch_assoc($exec))
	{
		$gender=$result['gender'];
		if($gender==2)
			$gen='Male';
		else if($gender==1)
			$gen='Female';
		else
			$gen='';
		
		$dateofbirth = $result['dateofbirth_str'];
		if($dateofbirth=="12-12-1900")
			$dateofbirth="";
		$populate_arr[] = array("gender" => $gen,"dateofbirth" => $dateofbirth);
		
	}

$json = new Services_JSON();
//$decoded = $json->decode($jsondata);
echo $json->encode($populate_arr);
include '../library/closeDB.php';
?>
