<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	$save=$_REQUEST['save'];
	$new=$_REQUEST['new'];
	$pat_id=$_REQUEST['pat_id'];
	
	if($new==1){
		
		$_SESSION['patient_cohort_id']="";
		$_SESSION['patient_followup_id']="";
		$_SESSION['patient_event_id']="";
	}
	
	if($pat_id<>"")
		$_SESSION['pat_id']=$pat_id;
	
	$pat_id=$_SESSION['pat_id'];
	if($_SESSION['patient_cohort_id']=="")
	{
		$patient_cohort_id=$_REQUEST['patient_cohort_id'];
		$_SESSION['patient_cohort_id']=$patient_cohort_id;
	}
	else
		$patient_cohort_id=$_SESSION['patient_cohort_id'];
	
	
	
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<?php
					/*
					$date_start=date_create('1950-06-06');
					$date_start = date_sub($date_start,date_interval_create_from_date_string("90 days"));
					
					$date_start=date_format($date_start,"Y-m-d");
					$date_start_time=strtotime($date_start);
					echo "date_start:$date_start</br>";
					echo "date_start_time:$date_start_time</br>";
					
					
					$date_end=date_create('1950-06-06');
					$date_end = date_format($date_end,"Y-m-d");
					$date_end_time=strtotime($date_end);
					echo "date_end:$date_end</br>";
					echo "date_end_time:$date_end_time</br>";
					
					$date_period_start=date_create('1950-04-08');
					$date_period_start=date_format($date_period_start,"Y-m-d");
					$date_period_start_time=strtotime($date_period_start);
					echo "date_period_start:".$date_period_start."</br>";
					echo "date_period_start_time:".$date_period_start_time."</br>";
					
					$date_period_end=date_create('1950-07-06');
					$date_period_end=date_format($date_period_end,"Y-m-d");
					$date_period_end_time=strtotime($date_period_end);
					echo "date_period_end:".$date_period_end."</br>";
					echo "date_period_end_time:".$date_period_end_time."</br>";
					
					if ($date_start_time > $date_period_start_time)
						$compare_start_date = date_create($date_start);
					else
						$compare_start_date = date_create($date_period_start);
					
					if ($date_end_time < $date_period_end_time)
						$compare_end_date = date_create($date_end);
					else
						$compare_end_date = date_create($date_period_end);
					
					echo  "compare_start_date:".date_format($compare_start_date,"Y-m-d")." </br>";
					echo  "compare_end_date:".date_format($compare_end_date,"Y-m-d")." </br>";
					$compare_start_date=date_format($compare_start_date,"Y-m-d");
					$compare_end_date=date_format($compare_end_date,"Y-m-d");
					
					echo "compare_start_date_time:".strtotime($compare_start_date)."</br>";
					echo "compare_end_date_time:".strtotime($compare_end_date)."</br>";
					if(strtotime($compare_start_date)<strtotime($compare_end_date))
						$days_diff=(strtotime($compare_end_date)-strtotime($compare_start_date))/(60*60*24*7);
					else
						$days_diff=0;
					echo $days_diff;
					*/
if($save==1)
{
	pg_query("BEGIN") or die("Could not start transaction\n");
	try
	{
		
		$exec = get_patient_demographics($pat_id);
		$result = pg_fetch_array($exec);
		$gender=$result['gender'];
		
		
		if (isset($_REQUEST['irregularfu']))
			$irregularfu=1;
		else
			$irregularfu=0;
		
		$parameters[0]=$_REQUEST['cohort_name'];
		$parameters[1]=str_replace("'", "''",$_REQUEST['treatment_nbr']);
		$parameters[2]=date_for_postgres($_REQUEST['start_date']);
		$parameters[3]=date_for_postgres($_REQUEST['stop_date']);
		$parameters[4]=$_REQUEST['stop_reason'];
		$parameters[5]=str_replace("'", "''",$_REQUEST['stop_ae_reason']);
		$parameters[6]=$_REQUEST['physician_assigning'];
		$parameters[7]=$_REQUEST['previous_dmards_nbr'];
		$parameters[8]=$_REQUEST['ongoing_dmards_nbr'];
		$parameters[9]=str_replace("'", "''",$_REQUEST['age_at_cohort']);	
		
		$parameters[10]=$_REQUEST['residence'];
		
		if($gender==2)
		{
			$parameters[11]=0;
			$parameters[12]=0;
			$parameters[13]=0;
			$parameters[14]=0;
			$parameters[15]=0;
			$parameters[16]=0;
			$parameters[17]=0;
			$parameters[18]=0;
			$parameters[19]=0;
		}
		else
		{
			$parameters[11]=$_REQUEST['contraceptive_methods'];
			$parameters[12]=$_REQUEST['pregnancies'];
			$parameters[13]=$_REQUEST['childbirths'];
			$parameters[14]=$_REQUEST['pregnancy_loses'];
			$parameters[15]=$_REQUEST['premature_births'];
			$parameters[16]=$_REQUEST['eclampsia'];
			$parameters[17]=$_REQUEST['premature_rom'];
			$parameters[18]=$_REQUEST['placental_insufficiency'];
			$parameters[19]=$_REQUEST['premature_births_reason'];
		}
		
		
		$parameters[20]=0;//$_REQUEST['employment_changed'];
		$parameters[21]=$_REQUEST['employment_at_cohort'];
		$parameters[22]=$_REQUEST['marital_status'];
		$parameters[23]=$_REQUEST['marital_status_year'];
		$parameters[24]=str_replace("'", "''",$_REQUEST['alcohol_consumption']);
		$parameters[25]=$_REQUEST['physical_status'];
		$parameters[26]=str_replace("'", "''",$_REQUEST['mediterranean_diet']);
		$parameters[27]=date_for_postgres($_REQUEST['date_phys_str']);
		
		$table_names[0]='cohort_name';
		$table_names[1]='treatment_nbr';
		$table_names[2]='start_date';
		$table_names[3]='stop_date';
		$table_names[4]='stop_reason';
		$table_names[5]='stop_ae_reason';
		$table_names[6]='physician_assigning';
		$table_names[7]='previous_dmards_nbr';
		$table_names[8]='ongoing_dmards_nbr';
		$table_names[9]='age_at_cohort';
		$table_names[10]='residence';
		$table_names[11]='contraceptive_methods';
		
		$table_names[12]='pregnancies';
		$table_names[13]='childbirths';
		$table_names[14]='pregnancy_loses';
		$table_names[15]='premature_births';
		$table_names[16]='eclampsia';
		$table_names[17]='premature_rom';
		$table_names[18]='placental_insufficiency';
		$table_names[19]='premature_births_reason';
		
		$table_names[20]='employment_changed';
		$table_names[21]='employment_at_cohort';
		$table_names[22]='marital_status';
		$table_names[23]='marital_status_year';
		$table_names[24]='alcohol_consumption';
		$table_names[25]='physical_status';
		$table_names[26]='mediterranean_diet';
		$table_names[27]='date_phys';
		
		$parameters[28]=$pat_id;
		$table_names[28]='pat_id';
		$parameters[29]=$user_id;
		$table_names[29]='editor_id';
		$parameters[30]='now()';
		$table_names[30]='edit_date';
		$parameters[31]=0;
		$table_names[31]='deleted';
		
		
		$parameters[32]=$_REQUEST['rheumatoid_arthritis'];
		if($_REQUEST['rheumatoid_arthritis']<>1)
		{
			$parameters[33]=0;
			$parameters[34]=0;
			$parameters[35]=0;
			$parameters[36]=0;
			$parameters[42]="1900-12-12";
		}
		else
		{
			$parameters[33]=$_REQUEST['erosions'];
			$parameters[34]=$_REQUEST['scharp'];
			$parameters[35]=$_REQUEST['sens'];
			$parameters[36]=$_REQUEST['psa'];
			$parameters[42]=date_for_postgres($_REQUEST['rheumatoid_arthritis_date']);
		}
		$parameters[37]=$_REQUEST['axial'];
		
		if($_REQUEST['axial']<>1)
		{
			$parameters[38]=0;
			$parameters[39]=0;
			$parameters[41]=0;
			$parameters[41]=0;
			$parameters[43]="1900-12-12";
		}
		else
		{
			$parameters[38]=$_REQUEST['xrays_sacroileal'];
			$parameters[39]=$_REQUEST['mri_sacroileal'];
			$parameters[40]=$_REQUEST['xrays_vertebral'];
			$parameters[41]=$_REQUEST['mri_vertebral'];
			$parameters[43]=date_for_postgres($_REQUEST['axial_date']);
		}
		$parameters[44]=$irregularfu;
		
		
		$table_names[32]='rheumatoid_arthritis';
		$table_names[33]='erosions';
		$table_names[34]='scharp';
		$table_names[35]='sens';
		$table_names[36]='psa';
		$table_names[37]='axial';
		$table_names[38]='xrays_sacroileal';
		$table_names[39]='mri_sacroileal';
		$table_names[40]='xrays_vertebral';
		$table_names[41]='mri_vertebral';
		$table_names[42]='rheumatoid_arthritis_date';
		$table_names[43]='axial_date';
		$table_names[44]='irregularfu';
		
		$edit_name[0]='patient_cohort_id';
		$edit_id[0]=$patient_cohort_id;
		$sumbol[0]='=';
		
		if($patient_cohort_id=='')
		{
			$patient_cohort_id=insert('patient_cohort',$table_names,$parameters,'patient_cohort_id');
			if($patient_cohort_id!='')
				//$msg="ok";
			
			$_SESSION['patient_cohort_id']=$patient_cohort_id;
			
			$exec_prv = get_prv_followup($pat_id);
			$result_prv = pg_fetch_array($exec_prv);
			
			$sle_severity=$result_prv['sle_severity'];				
			$common=$result_prv['common'];
			$tobacco=$result_prv['tobacco'];
			$smokingstarting=$result_prv['smokingstarting'];
			$smokingstopped=$result_prv['smokingstopped'];
			$smokingyear=$result_prv['smokingyear'];
			$decadefirst=$result_prv['decadefirst'];
			$decadesecond=$result_prv['decadesecond'];
			$decadethird=$result_prv['decadethird'];
			$decadeforth=$result_prv['decadeforth'];
			$decadefifth=$result_prv['decadefifth'];
			$decadesixth=$result_prv['decadesixth'];
			$decadeseventh=$result_prv['decadeseventh'];
			$decadeeight=$result_prv['decadeeight'];
			$averagepackperday=$result_prv['averagepackperday'];
			$averagepackperyear=$result_prv['averagepackperyear'];
			$parameters_followup[0]=date_for_postgres($_REQUEST['start_date']);
			$parameters_followup[1]=$_REQUEST['physician_assigning'];
			$parameters_followup[2]=$common;
			$parameters_followup[3]=$tobacco;
			$parameters_followup[4]=$smokingstarting;
			$parameters_followup[5]=$smokingstopped;
			$parameters_followup[6]=$smokingyear;
			$parameters_followup[7]=$decadefirst;	
			$parameters_followup[8]=$decadesecond;
			$parameters_followup[9]=$decadethird;
			$parameters_followup[10]=$decadeforth;
			$parameters_followup[11]=$decadefifth;
			$parameters_followup[12]=$decadesixth;
			$parameters_followup[13]=$decadeseventh;
			$parameters_followup[14]=$decadeeight;
			$parameters_followup[15]=$averagepackperday;
			$parameters_followup[16]=$averagepackperyear;
			/*$parameters_followup[16]=$_REQUEST['premature_births_reason'];
			$parameters_followup[17]=$_REQUEST['employment_changed'];
			$parameters_followup[18]=$_REQUEST['employment_at_cohort'];
			$parameters_followup[19]=$_REQUEST['marital_status'];
			$parameters_followup[20]=$_REQUEST['marital_status_year'];
			$parameters_followup[21]=str_replace("'", "''",$_REQUEST['alcohol_consumption']);
			$parameters_followup[22]=str_replace("'", "''",$_REQUEST['physical_activity']);
			$parameters_followup[23]=str_replace("'", "''",$_REQUEST['mediterranean_diet']);*/
			
			$table_names_followup[0]='start_date';
			$table_names_followup[1]='physician_assigning';
			$table_names_followup[2]='common';
			$table_names_followup[3]='tobacco';
			$table_names_followup[4]='smokingstarting';			
			$table_names_followup[5]='smokingstopped';
			$table_names_followup[6]='smokingyear';
			$table_names_followup[7]='decadefirst';
			$table_names_followup[8]='decadesecond';
			$table_names_followup[9]='decadethird';
			$table_names_followup[10]='decadeforth';
			$table_names_followup[11]='decadefifth';
			$table_names_followup[12]='decadesixth';
			$table_names_followup[13]='decadeseventh';
			$table_names_followup[14]='decadeeight';
			$table_names_followup[15]='averagepackperday';
			$table_names_followup[16]='averagepackperyear';
			/*$table_names_followup[16]='premature_births_reason';
			$table_names_followup[17]='employment_changed';
			$table_names_followup[18]='employment_at_cohort';
			$table_names_followup[19]='marital_status';
			$table_names_followup[20]='marital_status_year';
			$table_names_followup[21]='alcohol_consumption';
			$table_names_followup[22]='physical_activity';
			$table_names_followup[23]='mediterranean_diet';*/
			
			$parameters_followup[17]=$pat_id;
			$table_names_followup[17]='pat_id';
			$parameters_followup[18]=$user_id;
			$table_names_followup[18]='editor_id';
			$parameters_followup[19]='now()';
			$table_names_followup[19]='edit_date';
			$parameters_followup[20]=0;
			$table_names_followup[20]='deleted';
			$parameters_followup[21]=$patient_cohort_id;
			$table_names_followup[21]='patient_cohort_id';
			$parameters_followup[22]=$sle_severity;
			$table_names_followup[22]='sle_severity';
			
			
			$patient_followup_id=insert('patient_followup',$table_names_followup,$parameters_followup,'patient_followup_id');
			if($patient_followup_id!='')
			{
				if($common==2){
					$order_prv = get_prv_common($pat_id);
					$result_prv = pg_fetch_array($order_prv);
					$weight=$result_prv['weight'];
					$height=$result_prv['height'];
					$bmi=$result_prv['bmi'];
					
					$parameters_common[0]=$weight;
					$table_names_common[0]='weight';
					$parameters_common[1]=$height;
					$table_names_common[1]='height';
					$parameters_common[2]=$bmi;
					$table_names_common[2]='bmi';
					$parameters_common[3]=$patient_followup_id;
					$table_names_common[3]='patient_followup_id';
					$parameters_common[4]=$pat_id;
					$table_names_common[4]='pat_id';
					$parameters_common[5]=0;
					$table_names_common[5]='deleted';
					
					$patient_common_id=insert('patient_common',$table_names_common,$parameters_common,'patient_common_id');
				}
				else
					$patient_common_id=1;
				
				if($patient_common_id!='')
				{
					if($sle_severity==2){
						$order_prv = get_prv_sle_severity($pat_id);
						$result_prv = pg_fetch_array($order_prv);
						
						$parameters_sle_severity[0]=$result_prv['cns4'];
						$table_names_sle_severity[0]='cns4';
						$parameters_sle_severity[1]=$patient_followup_id;
						$table_names_sle_severity[1]='patient_followup_id';
						$parameters_sle_severity[2]=$pat_id;
						$table_names_sle_severity[2]='pat_id';
						$parameters_sle_severity[3]=0;
						$table_names_sle_severity[3]='deleted';
						$parameters_sle_severity[4]=$result_prv['slicc1'];
						$table_names_sle_severity[4]='slicc1';
						$parameters_sle_severity[5]=$result_prv['slicc2'];
						$table_names_sle_severity[5]='slicc2';
						$parameters_sle_severity[6]=$result_prv['slicc3'];
						$table_names_sle_severity[6]='slicc3';
						$parameters_sle_severity[7]=$result_prv['slicc4'];
						$table_names_sle_severity[7]='slicc4';
						$parameters_sle_severity[8]=$result_prv['slicc5'];
						$table_names_sle_severity[8]='slicc5';
						$parameters_sle_severity[9]=$result_prv['slicc6'];
						$table_names_sle_severity[9]='slicc6';
						$parameters_sle_severity[10]=$result_prv['slicc7'];
						$table_names_sle_severity[10]='slicc7';
						$parameters_sle_severity[11]=$result_prv['slicc8'];
						$table_names_sle_severity[11]='slicc8';
						$parameters_sle_severity[12]=$result_prv['slicc9'];
						$table_names_sle_severity[12]='slicc9';
						$parameters_sle_severity[13]=$result_prv['slicc10'];
						$table_names_sle_severity[13]='slicc10';
						$parameters_sle_severity[14]=$result_prv['slicc11'];
						$table_names_sle_severity[14]='slicc11';
						$parameters_sle_severity[15]=$result_prv['slicc12'];
						$table_names_sle_severity[15]='slicc12';
						$parameters_sle_severity[16]=$result_prv['slicc13'];
						$table_names_sle_severity[16]='slicc13';
						$parameters_sle_severity[17]=$result_prv['slicc14'];
						$table_names_sle_severity[17]='slicc14';
						$parameters_sle_severity[18]=$result_prv['slicc15'];
						$table_names_sle_severity[18]='slicc15';
						$parameters_sle_severity[19]=$result_prv['slicc16'];
						$table_names_sle_severity[19]='slicc16';
						$parameters_sle_severity[20]=$result_prv['slicc17'];
						$table_names_sle_severity[20]='slicc17';
						$parameters_sle_severity[21]=$result_prv['slicc18'];
						$table_names_sle_severity[21]='slicc18';
						$parameters_sle_severity[22]=$result_prv['slicc19'];
						$table_names_sle_severity[22]='slicc19';
						$parameters_sle_severity[23]=$result_prv['slicc20'];
						$table_names_sle_severity[23]='slicc20';
						$parameters_sle_severity[24]=$result_prv['slicc21'];
						$table_names_sle_severity[24]='slicc21';
						$parameters_sle_severity[25]=$result_prv['slicc22'];
						$table_names_sle_severity[25]='slicc22';
						$parameters_sle_severity[26]=$result_prv['slicc23'];
						$table_names_sle_severity[26]='slicc23';
						$parameters_sle_severity[27]=$result_prv['slicc24'];
						$table_names_sle_severity[27]='slicc24';
						$parameters_sle_severity[28]=$result_prv['slicc25'];
						$table_names_sle_severity[28]='slicc25';
						$parameters_sle_severity[29]=$result_prv['slicc26'];
						$table_names_sle_severity[29]='slicc26';
						$parameters_sle_severity[30]=$result_prv['slicc27'];
						$table_names_sle_severity[30]='slicc27';
						$parameters_sle_severity[31]=$result_prv['slicc28'];
						$table_names_sle_severity[31]='slicc28';
						$parameters_sle_severity[32]=$result_prv['slicc29'];
						$table_names_sle_severity[32]='slicc29';
						$parameters_sle_severity[33]=$result_prv['slicc30'];
						$table_names_sle_severity[33]='slicc30';
						$parameters_sle_severity[34]=$result_prv['slicc31'];
						$table_names_sle_severity[34]='slicc31';
						$parameters_sle_severity[35]=$result_prv['slicc32'];
						$table_names_sle_severity[35]='slicc32';
						$parameters_sle_severity[36]=$result_prv['slicc33'];
						$table_names_sle_severity[36]='slicc33';
						$parameters_sle_severity[37]=$result_prv['slicc34'];
						$table_names_sle_severity[37]='slicc34';
						$parameters_sle_severity[38]=$result_prv['slicc35'];
						$table_names_sle_severity[38]='slicc35';
						$parameters_sle_severity[39]=$result_prv['slicc36'];
						$table_names_sle_severity[39]='slicc36';
						$parameters_sle_severity[40]=$result_prv['slicc37'];
						$table_names_sle_severity[40]='slicc37';
						$parameters_sle_severity[41]=$result_prv['slicc38'];
						$table_names_sle_severity[41]='slicc38';
						$parameters_sle_severity[42]=$result_prv['slicc39'];
						$table_names_sle_severity[42]='slicc39';
						$parameters_sle_severity[43]=$result_prv['slicc40'];
						$table_names_sle_severity[43]='slicc40';
						$parameters_sle_severity[44]=$result_prv['slicc41'];
						$table_names_sle_severity[44]='slicc41';
						$parameters_sle_severity[45]=$result_prv['index1'];
						$table_names_sle_severity[45]='index1';
						$parameters_sle_severity[46]=$result_prv['index2'];
						$table_names_sle_severity[46]='index2';
						$parameters_sle_severity[47]=$result_prv['index3'];
						$table_names_sle_severity[47]='index3';
						$parameters_sle_severity[48]=$result_prv['index4'];
						$table_names_sle_severity[48]='index4';
						$parameters_sle_severity[49]=$result_prv['index5'];
						$table_names_sle_severity[49]='index5';
						$parameters_sle_severity[50]=$result_prv['index6'];
						$table_names_sle_severity[50]='index6';
						$parameters_sle_severity[51]=$result_prv['index7'];
						$table_names_sle_severity[51]='index7';
						$parameters_sle_severity[52]=$result_prv['index8'];
						$table_names_sle_severity[52]='index8';
						$parameters_sle_severity[53]=$result_prv['index9'];
						$table_names_sle_severity[53]='index9';
						$parameters_sle_severity[54]=$result_prv['index10'];
						$table_names_sle_severity[54]='index10';
						$parameters_sle_severity[55]=$result_prv['index11'];
						$table_names_sle_severity[55]='index11';
						$parameters_sle_severity[56]=$result_prv['index12'];
						$table_names_sle_severity[56]='index12';
						$parameters_sle_severity[57]=$result_prv['index13'];
						$table_names_sle_severity[57]='index13';
						
						
						$parameters_sle_severity[58]=$result_prv['cns3'];
						$table_names_sle_severity[58]='cns3';
						$parameters_sle_severity[59]=$result_prv['slicc1_date'];
						$table_names_sle_severity[59]='slicc1_date';
						$parameters_sle_severity[60]=$result_prv['slicc2_date'];
						$table_names_sle_severity[60]='slicc2_date';
						$parameters_sle_severity[61]=$result_prv['slicc3_date'];
						$table_names_sle_severity[61]='slicc3_date';
						$parameters_sle_severity[62]=$result_prv['slicc4_date'];
						$table_names_sle_severity[62]='slicc4_date';
						$parameters_sle_severity[63]=$result_prv['slicc5_date'];
						$table_names_sle_severity[63]='slicc5_date';
						$parameters_sle_severity[64]=$result_prv['slicc6_date'];
						$table_names_sle_severity[64]='slicc6_date';
						$parameters_sle_severity[65]=$result_prv['slicc7_date'];
						$table_names_sle_severity[65]='slicc7_date';
						$parameters_sle_severity[66]=$result_prv['slicc8_date'];
						$table_names_sle_severity[66]='slicc8_date';
						$parameters_sle_severity[67]=$result_prv['slicc9_date'];
						$table_names_sle_severity[67]='slicc9_date';
						$parameters_sle_severity[68]=$result_prv['slicc10_date'];
						$table_names_sle_severity[68]='slicc10_date';
						$parameters_sle_severity[69]=$result_prv['slicc11_date'];
						$table_names_sle_severity[69]='slicc11_date';
						$parameters_sle_severity[70]=$result_prv['slicc12_date'];
						$table_names_sle_severity[70]='slicc12_date';
						$parameters_sle_severity[71]=$result_prv['slicc13_date'];
						$table_names_sle_severity[71]='slicc13_date';
						$parameters_sle_severity[72]=$result_prv['slicc14_date'];
						$table_names_sle_severity[72]='slicc14_date';
						$parameters_sle_severity[73]=$result_prv['slicc15_date'];
						$table_names_sle_severity[73]='slicc15_date';
						$parameters_sle_severity[74]=$result_prv['slicc16_date'];
						$table_names_sle_severity[74]='slicc16_date';
						$parameters_sle_severity[75]=$result_prv['slicc17_date'];
						$table_names_sle_severity[75]='slicc17_date';
						$parameters_sle_severity[76]=$result_prv['slicc18_date'];
						$table_names_sle_severity[76]='slicc18_date';
						$parameters_sle_severity[77]=$result_prv['slicc19_date'];
						$table_names_sle_severity[77]='slicc19_date';
						$parameters_sle_severity[78]=$result_prv['slicc20_date'];
						$table_names_sle_severity[78]='slicc20_date';
						$parameters_sle_severity[79]=$result_prv['slicc21_date'];
						$table_names_sle_severity[79]='slicc21_date';
						$parameters_sle_severity[80]=$result_prv['slicc22_date'];
						$table_names_sle_severity[80]='slicc22_date';
						$parameters_sle_severity[81]=$result_prv['slicc23_date'];
						$table_names_sle_severity[81]='slicc23_date';
						$parameters_sle_severity[82]=$result_prv['slicc24_date'];
						$table_names_sle_severity[82]='slicc24_date';
						$parameters_sle_severity[83]=$result_prv['slicc25_date'];
						$table_names_sle_severity[83]='slicc25_date';
						$parameters_sle_severity[84]=$result_prv['slicc26_date'];
						$table_names_sle_severity[84]='slicc26_date';
						$parameters_sle_severity[85]=$result_prv['slicc27_date'];
						$table_names_sle_severity[85]='slicc27_date';
						$parameters_sle_severity[86]=$result_prv['slicc28_date'];
						$table_names_sle_severity[86]='slicc28_date';
						$parameters_sle_severity[87]=$result_prv['slicc29_date'];
						$table_names_sle_severity[87]='slicc29_date';
						$parameters_sle_severity[88]=$result_prv['slicc30_date'];
						$table_names_sle_severity[88]='slicc30_date';
						$parameters_sle_severity[89]=$result_prv['slicc31_date'];
						$table_names_sle_severity[89]='slicc31_date';
						$parameters_sle_severity[90]=$result_prv['slicc32_date'];
						$table_names_sle_severity[90]='slicc32_date';
						$parameters_sle_severity[91]=$result_prv['slicc33_date'];
						$table_names_sle_severity[91]='slicc33_date';
						$parameters_sle_severity[92]=$result_prv['slicc34_date'];
						$table_names_sle_severity[92]='slicc34_date';
						$parameters_sle_severity[93]=$result_prv['slicc35_date'];
						$table_names_sle_severity[93]='slicc35_date';
						$parameters_sle_severity[94]=$result_prv['slicc36_date'];
						$table_names_sle_severity[94]='slicc36_date';
						$parameters_sle_severity[95]=$result_prv['slicc37_date'];
						$table_names_sle_severity[95]='slicc37_date';
						$parameters_sle_severity[96]=$result_prv['slicc38_date'];
						$table_names_sle_severity[96]='slicc38_date';
						$parameters_sle_severity[97]=$result_prv['slicc39_date'];
						$table_names_sle_severity[97]='slicc39_date';
						$parameters_sle_severity[98]=$result_prv['slicc40_date'];
						$table_names_sle_severity[98]='slicc40_date';
						$parameters_sle_severity[99]=$result_prv['index1_date'];
						$table_names_sle_severity[99]='index1_date';
						$parameters_sle_severity[100]=$result_prv['index2_date'];
						$table_names_sle_severity[100]='index2_date';
						$parameters_sle_severity[101]=$result_prv['index3_date'];
						$table_names_sle_severity[101]='index3_date';
						$parameters_sle_severity[102]=$result_prv['index4_date'];
						$table_names_sle_severity[102]='index4_date';
						$parameters_sle_severity[103]=$result_prv['index5_date'];
						$table_names_sle_severity[103]='index5_date';
						$parameters_sle_severity[104]=$result_prv['index6_date'];
						$table_names_sle_severity[104]='index6_date';
						$parameters_sle_severity[105]=$result_prv['index7_date'];
						$table_names_sle_severity[105]='index7_date';
						$parameters_sle_severity[106]=$result_prv['index8_date'];
						$table_names_sle_severity[106]='index8_date';
						$parameters_sle_severity[107]=$result_prv['index9_date'];
						$table_names_sle_severity[107]='index9_date';
						$parameters_sle_severity[108]=$result_prv['index10_date'];
						$table_names_sle_severity[108]='index10_date';
						$parameters_sle_severity[109]=$result_prv['index11_date'];
						$table_names_sle_severity[109]='index11_date';
						$parameters_sle_severity[110]=$result_prv['index12_date'];
						$table_names_sle_severity[110]='index12_date';
						$parameters_sle_severity[111]=$result_prv['cns1'];
						$table_names_sle_severity[111]='cns1';
						$parameters_sle_severity[112]=$result_prv['cns2'];
						$table_names_sle_severity[112]='cns2';
						$parameters_sle_severity[113]=$result_prv['slicc42'];
						$table_names_sle_severity[113]='slicc42';
						$parameters_sle_severity[114]=$result_prv['slicc41_date'];
						$table_names_sle_severity[114]='slicc41_date';
						$patient_sle_severity_id=insert('patient_sle_severity',$table_names_sle_severity,$parameters_sle_severity,'patient_sle_severity_id');
					}
					else
						$patient_sle_severity_id=1;
					
					if($patient_sle_severity_id!='')
						$msg="ok";
				}
			}
			
			//PREVIOUS ANTI-RHEUMATIC DRUG TREATMENT  table-start
		
			$j=1;
			$patient_previous_drugs_numofrows=$_REQUEST['patient_previous_drugs_numofrows'];
			for ($i=1;$i<=$patient_previous_drugs_numofrows;$i++)
			{
				$patient_previous_drugs_hidden=$_REQUEST['patient_previous_drugs_hidden_'.$i];
				$patient_cohort_previous_drugs_id=$_REQUEST['patient_cohort_previous_drugs_id_'.$i];
				if($patient_previous_drugs_hidden=='-1')
					$j++;
				else
				{	
					$patient_previous_drugs_parameters[0]=0;
					$patient_previous_drugs_parameters[1]=$_REQUEST['patient_previous_drugs_id_'.$i];
					$patient_previous_drugs_parameters[2]=0;
					$patient_previous_drugs_parameters[3]=0;
					$patient_previous_drugs_parameters[4]=0;
					$patient_previous_drugs_parameters[5]=$user_id;
					$patient_previous_drugs_parameters[6]='now()';
					//$patient_previous_drugs_parameters[7]=$_REQUEST['route_of_administration_'.$i];;
					$patient_previous_drugs_table_names[0]='reason_of_discontinuation_id';
					$patient_previous_drugs_table_names[1]='drugs_id';
					$patient_previous_drugs_table_names[2]='year_of_therapy_start';
					$patient_previous_drugs_table_names[3]='treatment_stop';
					$patient_previous_drugs_table_names[4]='treatment_duration';
					$patient_previous_drugs_table_names[5]='editor_id';
					$patient_previous_drugs_table_names[6]='edit_date';
					//$patient_previous_drugs_table_names[7]='route_of_administration';
					$patient_previous_drugs_edit_name[0]='patient_previous_drugs_id';
					$patient_previous_drugs_edit_id[0]=$patient_previous_drugs_id;
					$patient_previous_drugs_sumbol[0]='=';
						
					$patient_previous_drugs_parameters[7]=$pat_id;
					$patient_previous_drugs_parameters[8]=0;
					$patient_previous_drugs_parameters[9]=$patient_cohort_id;
							
					$patient_previous_drugs_table_names[7]='pat_id';
					$patient_previous_drugs_table_names[8]='deleted';
					$patient_previous_drugs_table_names[9]='patient_cohort_id';
							
					$id=insert('patient_cohort_previous_drugs',$patient_previous_drugs_table_names,$patient_previous_drugs_parameters,'patient_cohort_previous_drugs_id');
					if($id!='')
						$j++;
				}
			}
			
			if ($j==$i)
				$antirheumatictreat_msg="ok";
			else
				$antirheumatictreat_msg="nok";
			//PREVIOUS ANTI-RHEUMATIC DRUG TREATMENT  table-end
			
		}
		else
		{
			
			$msg=update('patient_cohort',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
			
			//PREVIOUS ANTI-RHEUMATIC DRUG TREATMENT  table-start
		
			$j=1;
			$patient_previous_drugs_numofrows=$_REQUEST['patient_previous_drugs_numofrows'];
			for ($i=1;$i<=$patient_previous_drugs_numofrows;$i++)
			{
				$patient_previous_drugs_hidden=$_REQUEST['patient_previous_drugs_hidden_'.$i];
				$patient_cohort_previous_drugs_id=$_REQUEST['patient_cohort_previous_drugs_id_'.$i];
				if($patient_previous_drugs_hidden=='-1')
				{
					if($patient_cohort_previous_drugs_id<>"")
					{
						$patient_previous_drugs_hdn_parameters[0]=1;
						$patient_previous_drugs_hdn_names[0]='deleted';
						$patient_previous_drugs_hdn_edit_name[0]='patient_cohort_previous_drugs_id';
						$patient_previous_drugs_hdn_edit_id[0]=$patient_cohort_previous_drugs_id;
						$patient_previous_drugs_hdn_sumbol[0]='=';
						$patient_previous_drugs_msg=update('patient_cohort_previous_drugs',$patient_previous_drugs_hdn_names,$patient_previous_drugs_hdn_parameters,$patient_previous_drugs_hdn_edit_name,$patient_previous_drugs_hdn_edit_id,$patient_previous_drugs_hdn_sumbol);
						if ($patient_previous_drugs_msg=="ok")
							$j++;
					}
					else
						$j++;
				}
				else
				{	
					$patient_previous_drugs_parameters[0]=0;
					$patient_previous_drugs_parameters[1]=$_REQUEST['patient_previous_drugs_id_'.$i];
					$patient_previous_drugs_parameters[2]=0;
					$patient_previous_drugs_parameters[3]=0;
					$patient_previous_drugs_parameters[4]=0;
					$patient_previous_drugs_parameters[5]=$user_id;
					$patient_previous_drugs_parameters[6]='now()';
					//$patient_previous_drugs_parameters[7]=$_REQUEST['route_of_administration_'.$i];;
					$patient_previous_drugs_table_names[0]='reason_of_discontinuation_id';
					$patient_previous_drugs_table_names[1]='drugs_id';
					$patient_previous_drugs_table_names[2]='year_of_therapy_start';
					$patient_previous_drugs_table_names[3]='treatment_stop';
					$patient_previous_drugs_table_names[4]='treatment_duration';
					$patient_previous_drugs_table_names[5]='editor_id';
					$patient_previous_drugs_table_names[6]='edit_date';
					//$patient_previous_drugs_table_names[7]='route_of_administration';
					$patient_previous_drugs_edit_name[0]='patient_previous_drugs_id';
					$patient_previous_drugs_edit_id[0]=$patient_previous_drugs_id;
					$patient_previous_drugs_sumbol[0]='=';
						
					$patient_previous_drugs_parameters[7]=$pat_id;
					$patient_previous_drugs_parameters[8]=0;
					$patient_previous_drugs_parameters[9]=$patient_cohort_id;
							
					$patient_previous_drugs_table_names[7]='pat_id';
					$patient_previous_drugs_table_names[8]='deleted';
					$patient_previous_drugs_table_names[9]='patient_cohort_id';
					
					if ($patient_cohort_previous_drugs_id=="")
					{
						$id=insert('patient_cohort_previous_drugs',$patient_previous_drugs_table_names,$patient_previous_drugs_parameters,'patient_cohort_previous_drugs_id');
						if($id!='')
							$j++;
					}
					else
					{
						$patient_previous_drugs_edit_name[0]='patient_cohort_previous_drugs_id';
						$patient_previous_drugs_edit_id[0]=$patient_cohort_previous_drugs_id;
						$patient_previous_drugs_sumbol[0]='=';
						$patient_cohort_drugs_msg=update('patient_cohort_previous_drugs',$patient_previous_drugs_table_names,$patient_previous_drugs_parameters,$patient_previous_drugs_edit_name,$patient_previous_drugs_edit_id,$patient_previous_drugs_sumbol);
						if ($patient_cohort_drugs_msg=="ok")
							$j++;
					}
				}
			}
			
			if ($j==$i)
				$antirheumatictreat_msg="ok";
			else
				$antirheumatictreat_msg="nok";
			//PREVIOUS ANTI-RHEUMATIC DRUG TREATMENT  table-end
		}
		
		//DRUG TREATMENT  table-start
		if($patient_followup_id=="")
		{
			$sql = get_first_followup($patient_cohort_id);
			$result= pg_fetch_array($sql);
			$patient_followup_id=$result['patient_followup_id'];
		}
		
		$j=1;
		$patient_cohort_drugs_numofrows=$_REQUEST['patient_cohort_drugs_numofrows'];
		
		
		
		//patient_lookup_drugs parameters reset
		
		//patient_lookup_drugs delete
		$patient_cohort_drugs_hdn_parameters2[0]=1;
		$patient_cohort_drugs_hdn_names2[0]='deleted';
		$patient_cohort_drugs_hdn_edit_name2[0]='patient_followup_id';
		$patient_cohort_drugs_hdn_edit_id2[0]=$patient_followup_id;
		$patient_cohort_drugs_hdn_sumbol2[0]='=';
		$patient_cohort_drugs_hdn_edit_name2[1]='drug_flag';
		$patient_cohort_drugs_hdn_edit_id2[1]=1;
		$patient_cohort_drugs_hdn_sumbol2[1]='=';
		$patient_cohort_drugs_hdn_edit_name2[2]='fumonth_cohort';
		$patient_cohort_drugs_hdn_edit_id2[2]=0;
		$patient_cohort_drugs_hdn_sumbol2[2]='=';
		
		$patient_cohort_drugs_msg2=update('patient_lookup_drugs',$patient_cohort_drugs_hdn_names2,$patient_cohort_drugs_hdn_parameters2,$patient_cohort_drugs_hdn_edit_name2,$patient_cohort_drugs_hdn_edit_id2,$patient_cohort_drugs_hdn_sumbol2);
					
		
		for ($i=1;$i<=$patient_cohort_drugs_numofrows;$i++)
		{
					
			$patient_cohort_drugs_hidden=$_REQUEST['patient_cohort_drugs_hidden_'.$i];
			if($patient_cohort_drugs_hidden=='-1')
			{
				if ($_REQUEST['patient_cohort_drugs_type_'.$i]=='cohort')
				{
					$patient_cohort_drugs_id=$_REQUEST['patient_cohort_drugs_id_'.$i];
					if($patient_cohort_drugs_id<>"")
					{
						//vazei tin dosi sto proigoumeno drug
						if ($drugs_id<>"")
						{
							$patient_lookup_drugs_parameters[0]=$pat_id;
							$patient_lookup_drugs_parameters[1]=0;
							$patient_lookup_drugs_parameters[2]=$user_id;
							$patient_lookup_drugs_parameters[3]='now()';
							$patient_lookup_drugs_parameters[4]=$patient_cohort_id;
							$patient_lookup_drugs_parameters[5]=$patient_followup_id;
							$patient_lookup_drugs_parameters[6]='0';
							//$patient_lookup_drugs_parameters[7]='';
							$patient_lookup_drugs_parameters[7]=$drugs_id;
							$patient_lookup_drugs_parameters[8]=$route;
							$patient_lookup_drugs_parameters[9]=$maintherapy;
							$patient_lookup_drugs_parameters[10]=round($weekdosage, 2);
							$patient_lookup_drugs_parameters[11]=1;
							$patient_lookup_drugs_parameters[12]=round($currentdosage, 2);
							
							
							$patient_lookup_drugs_table_names[0]='pat_id';
							$patient_lookup_drugs_table_names[1]='deleted';
							$patient_lookup_drugs_table_names[2]='editor_id';
							$patient_lookup_drugs_table_names[3]='edit_date';
							$patient_lookup_drugs_table_names[4]='patient_cohort_id';
							$patient_lookup_drugs_table_names[5]='patient_followup_id';
							$patient_lookup_drugs_table_names[6]='fumonth_cohort';
							//$patient_lookup_drugs_table_names[7]='fumonth_inclusion';
							$patient_lookup_drugs_table_names[7]='drugs_id';
							$patient_lookup_drugs_table_names[8]='route';
							$patient_lookup_drugs_table_names[9]='maintherapy';
							$patient_lookup_drugs_table_names[10]='weekdosage';
							$patient_lookup_drugs_table_names[11]='drug_flag';
							$patient_lookup_drugs_table_names[12]='currentdosage';
							
							$patient_lookup_drugs_edit_name[0]='patient_followup_id';
							$patient_lookup_drugs_edit_id[0]=$patient_followup_id;
							$patient_lookup_drugs_sumbol[0]='=';
							$patient_lookup_drugs_edit_name[1]='drugs_id';
							$patient_lookup_drugs_edit_id[1]=$drugs_id;
							$patient_lookup_drugs_sumbol[1]='=';
							$patient_lookup_drugs_edit_name[2]='drug_flag';
							$patient_lookup_drugs_edit_id[2]=1;
							$patient_lookup_drugs_sumbol[2]='=';
							
							$q1 = "select patient_lookup_drugs_id from patient_lookup_drugs where deleted=0 and patient_followup_id=$patient_followup_id and drugs_id=$drugs_id and drug_flag=1";
							$q_exec = pg_query($q1);
							$q_reason=pg_fetch_array($q_exec); 
							$patient_lookup_drugs_id=$q_reason['patient_lookup_drugs_id'];
						
							if($patient_lookup_drugs_id=='')
							{
								$patient_lookup_drugs_id=insert('patient_lookup_drugs',$patient_lookup_drugs_table_names,$patient_lookup_drugs_parameters,'patient_lookup_drugs_id');
								if($patient_lookup_drugs_id!='')
									$patient_lookup_drugs_msg="ok";
							}
							else
								$patient_lookup_drugs_msg=update('patient_lookup_drugs',$patient_lookup_drugs_table_names,$patient_lookup_drugs_parameters,$patient_lookup_drugs_edit_name,$patient_lookup_drugs_edit_id,$patient_lookup_drugs_sumbol);
						}
						else
							$patient_lookup_drugs_msg="ok";
					
						//patient_lookup_drugs initialazation
						$patient_lookup_drugs_id="";
						$drugs_id="";
						$route="";
						$maintherapy="";
						$weekdosage="";
						$currentdosage="0";
						$trans="";
					
						$patient_cohort_drugs_hdn_parameters[0]=1;
						$patient_cohort_drugs_hdn_names[0]='deleted';
						$patient_cohort_drugs_hdn_edit_name[0]='patient_cohort_drugs_id';
						$patient_cohort_drugs_hdn_edit_id[0]=$patient_cohort_drugs_id;
						$patient_cohort_drugs_hdn_sumbol[0]='=';
						$patient_cohort_drugs_msg=update('patient_cohort_drugs',$patient_cohort_drugs_hdn_names,$patient_cohort_drugs_hdn_parameters,$patient_cohort_drugs_hdn_edit_name,$patient_cohort_drugs_hdn_edit_id,$patient_cohort_drugs_hdn_sumbol);
						
						//patient_lookup_drugs delete
						/*$patient_cohort_drugs_hdn_parameters2[0]=1;
						$patient_cohort_drugs_hdn_names2[0]='deleted';
						$patient_cohort_drugs_hdn_edit_name2[0]='drugs_id';
						$patient_cohort_drugs_hdn_edit_id2[0]=$_REQUEST['drugs_id_'.$i];
						$patient_cohort_drugs_hdn_sumbol2[0]='=';
						$patient_cohort_drugs_hdn_edit_name2[1]='patient_followup_id';
						$patient_cohort_drugs_hdn_edit_id2[1]=$patient_followup_id;
						$patient_cohort_drugs_hdn_sumbol2[1]='=';
						$patient_cohort_drugs_hdn_edit_name2[2]='drug_flag';
						$patient_cohort_drugs_hdn_edit_id2[2]=1;
						$patient_cohort_drugs_hdn_sumbol2[2]='=';
						
						$patient_cohort_drugs_msg2=update('patient_lookup_drugs',$patient_cohort_drugs_hdn_names2,$patient_cohort_drugs_hdn_parameters2,$patient_cohort_drugs_hdn_edit_name2,$patient_cohort_drugs_hdn_edit_id2,$patient_cohort_drugs_hdn_sumbol2);*/
						
						/* Αφαιρέθηκε στις 2021-11-30 γιατι στην αρχή γίνονται όλα τα patient_lookup_drugs deleted=1 οπότε δεν χρειάζεται να υπάρχει
						$current_drugs_id=$_REQUEST['drugs_id_'.$i];
						$q11 = "select patient_lookup_drugs_id from patient_lookup_drugs where deleted=0 and patient_followup_id=$patient_followup_id and drugs_id=$current_drugs_id and drug_flag=1";
						$q_exec11 = pg_query($q11);
						$q_reason11=pg_fetch_array($q_exec11); 
						$patient_lookup_drugs_id=$q_reason11['patient_lookup_drugs_id'];
						
						$patient_cohort_drugs_hdn_parameters22[0]=1;
						$patient_cohort_drugs_hdn_names22[0]='deleted';
						$patient_cohort_drugs_hdn_edit_name22[0]='patient_lookup_drugs_id';
						$patient_cohort_drugs_hdn_edit_id22[0]=$patient_lookup_drugs_id;
						$patient_cohort_drugs_hdn_sumbol22[0]='=';
						
						$patient_cohort_drugs_msg2=update('patient_lookup_drugs',$patient_cohort_drugs_hdn_names22,$patient_cohort_drugs_hdn_parameters22,$patient_cohort_drugs_hdn_edit_name22,$patient_cohort_drugs_hdn_edit_id22,$patient_cohort_drugs_hdn_sumbol22);
						if ($patient_cohort_drugs_msg=="ok" and $patient_cohort_drugs_msg2=="ok")
						*/
						if ($patient_cohort_drugs_msg=="ok")
							$j++;
					}
					else
						$j++;
					
					$drugs_id="";
				}
				else if ($_REQUEST['patient_cohort_drugs_type_'.$i]=='period')
				{
					$patient_cohort_drugs_period_id=$_REQUEST['patient_cohort_drugs_period_id_'.$i];
					if($patient_cohort_drugs_period_id<>"")
					{
						$patient_cohort_drugs_hdn_parameters[0]=1;
						$patient_cohort_drugs_hdn_names[0]='deleted';
						$patient_cohort_drugs_hdn_edit_name[0]='patient_cohort_drugs_period_id';
						$patient_cohort_drugs_hdn_edit_id[0]=$patient_cohort_drugs_period_id;
						$patient_cohort_drugs_hdn_sumbol[0]='=';
						$patient_cohort_drugs_msg=update('patient_cohort_drugs_period',$patient_cohort_drugs_hdn_names,$patient_cohort_drugs_hdn_parameters,$patient_cohort_drugs_hdn_edit_name,$patient_cohort_drugs_hdn_edit_id,$patient_cohort_drugs_hdn_sumbol);
						if ($patient_cohort_drugs_msg=="ok")
							$j++;
					}
					else
						$j++;
				}
				else if ($_REQUEST['patient_cohort_drugs_type_'.$i]=='period_label')
				{
					$j++;
				}
				
			}
			else
			{
				if ($_REQUEST['patient_cohort_drugs_type_'.$i]=='cohort')
				{
					
					//patient_lookup_drugs check if is the first else make insert or update
					//vazei tin dosi sto proigoumeno drug
					if ($drugs_id<>"")
					{
						$patient_lookup_drugs_parameters[0]=$pat_id;
						$patient_lookup_drugs_parameters[1]=0;
						$patient_lookup_drugs_parameters[2]=$user_id;
						$patient_lookup_drugs_parameters[3]='now()';
						$patient_lookup_drugs_parameters[4]=$patient_cohort_id;
						$patient_lookup_drugs_parameters[5]=$patient_followup_id;
						$patient_lookup_drugs_parameters[6]='0';
						//$patient_lookup_drugs_parameters[7]='';
						$patient_lookup_drugs_parameters[7]=$drugs_id;
						$patient_lookup_drugs_parameters[8]=$route;
						$patient_lookup_drugs_parameters[9]=$maintherapy;
						$patient_lookup_drugs_parameters[10]=round($weekdosage, 2);
						$patient_lookup_drugs_parameters[11]=1;
						$patient_lookup_drugs_parameters[12]=round($currentdosage, 2);
						
						
						$patient_lookup_drugs_table_names[0]='pat_id';
						$patient_lookup_drugs_table_names[1]='deleted';
						$patient_lookup_drugs_table_names[2]='editor_id';
						$patient_lookup_drugs_table_names[3]='edit_date';
						$patient_lookup_drugs_table_names[4]='patient_cohort_id';
						$patient_lookup_drugs_table_names[5]='patient_followup_id';
						$patient_lookup_drugs_table_names[6]='fumonth_cohort';
						//$patient_lookup_drugs_table_names[7]='fumonth_inclusion';
						$patient_lookup_drugs_table_names[7]='drugs_id';
						$patient_lookup_drugs_table_names[8]='route';
						$patient_lookup_drugs_table_names[9]='maintherapy';
						$patient_lookup_drugs_table_names[10]='weekdosage';
						$patient_lookup_drugs_table_names[11]='drug_flag';
						$patient_lookup_drugs_table_names[12]='currentdosage';
						
						$patient_lookup_drugs_edit_name[0]='patient_followup_id';
						$patient_lookup_drugs_edit_id[0]=$patient_followup_id;
						$patient_lookup_drugs_sumbol[0]='=';
						$patient_lookup_drugs_edit_name[1]='drugs_id';
						$patient_lookup_drugs_edit_id[1]=$drugs_id;
						$patient_lookup_drugs_sumbol[1]='=';
						$patient_lookup_drugs_edit_name[2]='drug_flag';
						$patient_lookup_drugs_edit_id[2]=1;
						$patient_lookup_drugs_sumbol[2]='=';
						
						$q1 = "select patient_lookup_drugs_id from patient_lookup_drugs where deleted=0 and patient_followup_id=$patient_followup_id and drugs_id=$drugs_id and drug_flag=1";
						$q_exec = pg_query($q1);
						$q_reason=pg_fetch_array($q_exec); 
						$patient_lookup_drugs_id=$q_reason['patient_lookup_drugs_id'];
					
						if($patient_lookup_drugs_id=='')
						{
							$patient_lookup_drugs_id=insert('patient_lookup_drugs',$patient_lookup_drugs_table_names,$patient_lookup_drugs_parameters,'patient_lookup_drugs_id');
							if($patient_lookup_drugs_id!='')
								$patient_lookup_drugs_msg="ok";
						}
						else
							$patient_lookup_drugs_msg=update('patient_lookup_drugs',$patient_lookup_drugs_table_names,$patient_lookup_drugs_parameters,$patient_lookup_drugs_edit_name,$patient_lookup_drugs_edit_id,$patient_lookup_drugs_sumbol);
					}
					else
						$patient_lookup_drugs_msg="ok";
					
					
					//patient_lookup_drugs initialazation
					$patient_lookup_drugs_id="";
					$drugs_id="";
					$route="";
					$maintherapy="";
					$weekdosage="";
					$currentdosage="0";
					$trans="";
					
					
					$patient_cohort_drugs_id=$_REQUEST['patient_cohort_drugs_id_'.$i];
					
					if (isset($_REQUEST['main_'.$i]))
						$main="1";
					else
						$main="0";
		
					$patient_cohort_drugs_parameters[0]=$user_id;
					$patient_cohort_drugs_parameters[1]='now()';
					$patient_cohort_drugs_parameters[2]=$_REQUEST['drugs_id_'.$i];
					$patient_cohort_drugs_parameters[3]='';
					//$patient_cohort_drugs_parameters[4]=$_REQUEST['route_of_administration_'.$i];
					$patient_cohort_drugs_parameters[4]=$patient_cohort_id;
					$patient_cohort_drugs_parameters[5]=0;
					$patient_cohort_drugs_parameters[6]=date_for_postgres($_REQUEST['startdate_'.$i]);
					$patient_cohort_drugs_parameters[7]=date_for_postgres($_REQUEST['stopdate_'.$i]);
					$patient_cohort_drugs_parameters[8]=$main;
										
					$patient_cohort_drugs_table_names[0]='editor_id';
					$patient_cohort_drugs_table_names[1]='edit_date';
					$patient_cohort_drugs_table_names[2]='drugs_id';
					$patient_cohort_drugs_table_names[3]='dose';
					//$patient_cohort_drugs_table_names[4]='route_of_administration';
					$patient_cohort_drugs_table_names[4]='patient_cohort_id';
					$patient_cohort_drugs_table_names[5]='frequency';
					$patient_cohort_drugs_table_names[6]='startdate';
					$patient_cohort_drugs_table_names[7]='stopdate';
					$patient_cohort_drugs_table_names[8]='main';
					
										
					
					$patient_cohort_drugs_edit_name[0]='patient_cohort_drugs_id';
					$patient_cohort_drugs_edit_id[0]=$patient_cohort_drugs_id;
					$patient_cohort_drugs_sumbol[0]='=';
					
					//patient_lookup_drugs set parameters
					$drugs_id=$_REQUEST['drugs_id_'.$i];
					$maintherapy=$main;
					$sql = get_drug($drugs_id);
					$result2 = pg_fetch_array($sql);
					$route=$result2['route_of_administration'];
					
					if($patient_cohort_drugs_id=='')
					{
						
						$patient_cohort_drugs_parameters[9]=$pat_id;
						$patient_cohort_drugs_parameters[10]=0;
						
						$patient_cohort_drugs_table_names[9]='pat_id';
						$patient_cohort_drugs_table_names[10]='deleted';
						$patient_cohort_drugs_parameters[11]=$patient_followup_id;
						$patient_cohort_drugs_table_names[11]='patient_followup_id';
					
						$patient_cohort_drugs_id=insert('patient_cohort_drugs',$patient_cohort_drugs_table_names,$patient_cohort_drugs_parameters,'patient_cohort_drugs_id');
						if($patient_cohort_drugs_id!='' and $patient_lookup_drugs_msg=="ok")
							$j++;
					}
					else
					{
						$patient_cohort_drugs_msg=update('patient_cohort_drugs',$patient_cohort_drugs_table_names,$patient_cohort_drugs_parameters,$patient_cohort_drugs_edit_name,$patient_cohort_drugs_edit_id,$patient_cohort_drugs_sumbol);
						if ($patient_cohort_drugs_msg=="ok" and $patient_lookup_drugs_msg=="ok")
							$j++;
					}
				}
				else if ($_REQUEST['patient_cohort_drugs_type_'.$i]=='period')
				{
					$patient_cohort_drugs_period_id=$_REQUEST['patient_cohort_drugs_period_id_'.$i];
					
					$patient_cohort_drugs_period_parameters[0]=$user_id;
					$patient_cohort_drugs_period_parameters[1]='now()';
					$patient_cohort_drugs_period_parameters[2]=$patient_cohort_drugs_id;
					$patient_cohort_drugs_period_parameters[3]=str_replace("'", "''",$_REQUEST['dose_'.$i]);
					$patient_cohort_drugs_period_parameters[4]=$patient_cohort_id;
					$patient_cohort_drugs_period_parameters[5]=$_REQUEST['frequency_'.$i];
					$patient_cohort_drugs_period_parameters[6]=date_for_postgres($_REQUEST['startdate_'.$i]);
					$patient_cohort_drugs_period_parameters[7]=date_for_postgres($_REQUEST['stopdate_'.$i]);
					$patient_cohort_drugs_period_parameters[8]=$_REQUEST['reason_'.$i];
					
					$patient_cohort_drugs_period_table_names[0]='editor_id';
					$patient_cohort_drugs_period_table_names[1]='edit_date';
					$patient_cohort_drugs_period_table_names[2]='patient_cohort_drugs_id';
					$patient_cohort_drugs_period_table_names[3]='dose';
					$patient_cohort_drugs_period_table_names[4]='patient_cohort_id';
					$patient_cohort_drugs_period_table_names[5]='frequency';
					$patient_cohort_drugs_period_table_names[6]='startdate';
					$patient_cohort_drugs_period_table_names[7]='stopdate';
					$patient_cohort_drugs_period_table_names[8]='reason';					
					
					$patient_cohort_drugs_period_edit_name[0]='patient_cohort_drugs_period_id';
					$patient_cohort_drugs_period_edit_id[0]=$patient_cohort_drugs_period_id;
					$patient_cohort_drugs_period_sumbol[0]='=';
					
					if($patient_cohort_drugs_period_id=='')
					{
						
						$patient_cohort_drugs_period_parameters[9]=0;
						$patient_cohort_drugs_period_table_names[9]='deleted';
						$patient_cohort_drugs_period_table_names[10]='patient_followup_id';
						$patient_cohort_drugs_period_parameters[10]=$patient_followup_id;
					
						$patient_cohort_drugs_period_id=insert('patient_cohort_drugs_period',$patient_cohort_drugs_period_table_names,$patient_cohort_drugs_period_parameters,'patient_cohort_drugs_period_id');
						if($patient_cohort_drugs_period_id!='')
							$j++;
					}
					else
					{
						$patient_cohort_drugs_period_msg=update('patient_cohort_drugs_period',$patient_cohort_drugs_period_table_names,$patient_cohort_drugs_period_parameters,$patient_cohort_drugs_period_edit_name,$patient_cohort_drugs_period_edit_id,$patient_cohort_drugs_period_sumbol);
						if ($patient_cohort_drugs_period_msg=="ok")
							$j++;
					}
					
					//patient_lookup_drugs set weekdosage and currentdosage
					if($_REQUEST['start_date']<>"" and $_REQUEST['startdate_'.$i]<>"")
					{
						$date_start=date_create($_REQUEST['start_date']);
						$date_start_curr=date_create($_REQUEST['start_date']);
						$date_start = date_sub($date_start,date_interval_create_from_date_string("90 days"));
						
						$date_start=date_format($date_start,"Y-m-d");
						$date_start_curr=date_format($date_start_curr,"Y-m-d");
						$date_start_time=strtotime($date_start);
						$date_start_curr_time=strtotime($date_start_curr);
												
						$date_end=date_create($_REQUEST['start_date']);
						$date_end = date_format($date_end,"Y-m-d");
						$date_end_time=strtotime($date_end);
						
						$date_period_start=date_create($_REQUEST['startdate_'.$i]);
						$date_period_start=date_format($date_period_start,"Y-m-d");
						$date_period_start_time=strtotime($date_period_start);
						
						if($_REQUEST['stopdate_'.$i]<>"")
							$date_period_end=date_create($_REQUEST['stopdate_'.$i]);
						else
							$date_period_end=date_create($_REQUEST['start_date']);
						
						$date_period_end=date_format($date_period_end,"Y-m-d");
						$date_period_end_time=strtotime($date_period_end);
						
						if ($date_start_time > $date_period_start_time)
							$compare_start_date = date_create($date_start);
						else
							$compare_start_date = date_create($date_period_start);
						
						if ($date_end_time < $date_period_end_time)
							$compare_end_date = date_create($date_end);
						else
							$compare_end_date = date_create($date_period_end);
						
						$compare_start_date=date_format($compare_start_date,"Y-m-d");
						$compare_end_date=date_format($compare_end_date,"Y-m-d");
						
						if(strtotime($compare_start_date)<strtotime($compare_end_date))
							//$days_diff=(strtotime($compare_end_date)-strtotime($compare_start_date))/(60*60*24*7);
							$days_diff=(strtotime($compare_end_date)-strtotime($compare_start_date))/(60*60*24);
						else
							$days_diff=0;

						
						$q1 = "select explain from drugs_frequency where drugs_frequency_id=".$_REQUEST['frequency_'.$i];
						$q_exec = pg_query($q1);
						$q_reason=pg_fetch_array($q_exec); 
						$trans=$q_reason['explain'];
						$weekdosage = $weekdosage + ($days_diff*$_REQUEST['dose_'.$i]*$trans)/90;
						
						if($compare_start_date<=$date_start_curr and $date_start_curr<=$compare_end_date)
							$currentdosage = $_REQUEST['dose_'.$i]*$trans;
					}
				}
				else if ($_REQUEST['patient_cohort_drugs_type_'.$i]=='period_label')
				{
					$j++;
				}
				
			}
			
			//patient_lookup_drugs set last record
				if($i==$patient_cohort_drugs_numofrows)
				{
					//patient_lookup_drugs check if is the first else make insert or update
					if ($drugs_id<>"")
					{
						$patient_lookup_drugs_parameters[0]=$pat_id;
						$patient_lookup_drugs_parameters[1]=0;
						$patient_lookup_drugs_parameters[2]=$user_id;
						$patient_lookup_drugs_parameters[3]='now()';
						$patient_lookup_drugs_parameters[4]=$patient_cohort_id;
						$patient_lookup_drugs_parameters[5]=$patient_followup_id;
						$patient_lookup_drugs_parameters[6]='0';
						$patient_lookup_drugs_parameters[7]='0';
						$patient_lookup_drugs_parameters[8]=$drugs_id;
						$patient_lookup_drugs_parameters[9]=$route;
						$patient_lookup_drugs_parameters[10]=$maintherapy;
						$patient_lookup_drugs_parameters[11]=round($weekdosage, 2);
						$patient_lookup_drugs_parameters[12]=1;
						$patient_lookup_drugs_parameters[13]=round($currentdosage, 2);
						
						
						$patient_lookup_drugs_table_names[0]='pat_id';
						$patient_lookup_drugs_table_names[1]='deleted';
						$patient_lookup_drugs_table_names[2]='editor_id';
						$patient_lookup_drugs_table_names[3]='edit_date';
						$patient_lookup_drugs_table_names[4]='patient_cohort_id';
						$patient_lookup_drugs_table_names[5]='patient_followup_id';
						$patient_lookup_drugs_table_names[6]='fumonth_cohort';
						$patient_lookup_drugs_table_names[7]='fumonth_inclusion';
						$patient_lookup_drugs_table_names[8]='drugs_id';
						$patient_lookup_drugs_table_names[9]='route';
						$patient_lookup_drugs_table_names[10]='maintherapy';
						$patient_lookup_drugs_table_names[11]='weekdosage';
						$patient_lookup_drugs_table_names[12]='drug_flag';
						$patient_lookup_drugs_table_names[13]='currentdosage';
						
						$patient_lookup_drugs_edit_name[0]='patient_followup_id';
						$patient_lookup_drugs_edit_id[0]=$patient_followup_id;
						$patient_lookup_drugs_sumbol[0]='=';
						$patient_lookup_drugs_edit_name[1]='drugs_id';
						$patient_lookup_drugs_edit_id[1]=$drugs_id;
						$patient_lookup_drugs_sumbol[1]='=';
						$patient_lookup_drugs_edit_name[2]='drug_flag';
						$patient_lookup_drugs_edit_id[2]=1;
						$patient_lookup_drugs_sumbol[2]='=';
						
						$q1 = "select patient_lookup_drugs_id from patient_lookup_drugs where deleted=0 and patient_followup_id=$patient_followup_id and drugs_id=$drugs_id and drug_flag=1";
						$q_exec = pg_query($q1);
						$q_reason=pg_fetch_array($q_exec); 
						$patient_lookup_drugs_id=$q_reason['patient_lookup_drugs_id'];
						
						if($patient_lookup_drugs_id=='')
						{
							$patient_lookup_drugs_id=insert('patient_lookup_drugs',$patient_lookup_drugs_table_names,$patient_lookup_drugs_parameters,'patient_lookup_drugs_id');
							if($patient_lookup_drugs_id!='')
								$patient_lookup_drugs_msg="ok";
						}
						else
							$patient_lookup_drugs_msg=update('patient_lookup_drugs',$patient_lookup_drugs_table_names,$patient_lookup_drugs_parameters,$patient_lookup_drugs_edit_name,$patient_lookup_drugs_edit_id,$patient_lookup_drugs_sumbol);
					}
					else
						$patient_lookup_drugs_msg="ok";
					
				}
		}
		
		if ($j==$i)
			$cohort_drugs_msg="ok";
		else
			$cohort_drugs_msg="nok";
		//DRUG TREATMENT  table-end
		
		
		
		
		if ($msg=="ok" and $cohort_drugs_msg=="ok" and $antirheumatictreat_msg=="ok")
		{
			$save_chk="ok";
			pg_query("COMMIT") or die("Transaction commit failed\n");
		}
		else
		{
			$save_chk="nok";
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
	}
	catch(exception $e) {
		echo "e:".$e."</br>";
		echo "ROLLBACK";
		pg_query("ROLLBACK") or die("Transaction rollback failed\n");
	}
	
}

$exec = get_patient_demographics($pat_id);
$result = pg_fetch_array($exec);
$dateofinclusion=$result['dateofinclusion_str'];
if($dateofinclusion=="12-12-1900")
	$dateofinclusion="";
$dateofbirth=$result['dateofbirth_str'];
if($dateofbirth=="12-12-1900")
	$dateofbirth="";
$gender=$result['gender'];
if($gender==1)
	$gender_str="Female";
else if($gender==2)
	$gender_str="Male";
$patient_id=$result['patient_id'];
?>
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
	  <h1>
          <?php if($patient_cohort_id=="") { echo "New Cohort "; } else { echo "Edit Cohort "; } ?>
        </h1>
		<h1>
		  <?php
			if($pat_id<>"")
			{
				echo "<small>Patient:".$patient_id.",".$gender_str." (".$dateofbirth.") </small>";
			}
			?>
        </h1>
      </section>

      <!-- Main content -->
      <section class="content">
	  <div class="alert alert_suc alert-success" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Successful save!</strong>
	  </div>
	  <div class="alert alert_wr alert-danger" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Unsuccessful save!</strong>
	  </div>
	  <form id="form" name="form" action="cohort_edit.php" method="POST">
	  <input type="hidden" id="save" name="save" value="1">
	  <?php
		$sql = get_patient_age($pat_id);
		$result= pg_fetch_array($sql);
		$dateofbirth_str=$result['dateofbirth_str'];
	  ?>
		<input type="hidden" id="dateofinclusion" name="dateofinclusion" value="<?php echo $dateofinclusion;?>">
		<input type="hidden" id="dateofbirth_str" name="dateofbirth_str" value="<?php echo $dateofbirth_str;?>">
		<input type="hidden" id="patient_cohort_id" name="patient_cohort_id" value="<?php echo $patient_cohort_id;?>">
		<?php
		$order = get_cohorts_exc($pat_id,$patient_cohort_id);
		$num_rows=0;
		while($result = pg_fetch_assoc($order))
		{
			$num_rows++;
			$start_dates=$result['start_date_str'];
			
			$startdates .=$start_dates;
			$startdates .="!@#$";
			$stop_dates=$result['stop_date_str'];
			if($stop_dates=="")
				$stop_dates="12-12-1900";
			
			$stopdates .=$stop_dates;
			$stopdates .="!@#$";
		}
		?>
		<input type="hidden" id="cohortsnum" name="cohortsnum" value="<?php echo $num_rows;?>">
		<input type="hidden" id="startdates" name="startdates" value="<?php echo $startdates;?>">
		<input type="hidden" id="stopdates" name="stopdates" value="<?php echo $stopdates;?>">
       		<?php
				if($patient_cohort_id!="")
				{
					$exec = get_patient_demographics($pat_id);
					$result2 = pg_fetch_array($exec);
					$gender=$result2['gender'];
					
					$exec = get_cohort($patient_cohort_id);
					$result = pg_fetch_array($exec);
					
					$date_phys_str=$result['date_phys_str'];
					if($date_phys_str=="12-12-1900")
						$date_phys_str="";
					
					$cohort_name=$result['cohort_name'];
					$cohort_name_str=$result['cohort_name_str'];
					$treatment_nbr=$result['treatment_nbr'];
					$start_date=$result['start_date_str'];
					if($start_date=="12-12-1900")
						$start_date="";
					$stop_date=$result['stop_date_str'];
					if($stop_date=="12-12-1900")
						$stop_date="";
					$stop_reason=$result['stop_reason'];
					$stop_ae_reason=$result['stop_ae_reason'];
					$physician_assigning=$result['physician_assigning'];
					$previous_dmards_nbr=$result['previous_dmards_nbr'];
					$ongoing_dmards_nbr=$result['ongoing_dmards_nbr'];
					$age_at_cohort=$result['age_at_cohort'];
					$residence=$result['residence'];
					$contraceptive_methods=$result['contraceptive_methods'];
					$pregnancies=$result['pregnancies'];
					$childbirths=$result['childbirths'];
					$pregnancy_loses=$result['pregnancy_loses'];
					$premature_births=$result['premature_births'];
					$eclampsia=$result['eclampsia'];
					$premature_rom=$result['premature_rom'];
					$placental_insufficiency=$result['placental_insufficiency'];
					$premature_births_reason=$result['premature_births_reason'];
					//$employment_changed=$result['employment_changed'];
					$employment_at_cohort=$result['employment_at_cohort'];
					$marital_status=$result['marital_status'];
					$marital_status_year=$result['marital_status_year'];
					$alcohol_consumption=$result['alcohol_consumption'];
					$physical_status=$result['physical_status'];
					$mediterranean_diet=$result['mediterranean_diet'];
					$irregularfu=$result['irregularfu'];
					
					//copy apo to teleytaio cohort an einai kainourgio to cohort
					
					$rheumatoid_arthritis=$result['rheumatoid_arthritis'];
					$erosions=$result['erosions'];
					$scharp=$result['scharp'];
					$sens=$result['sens'];
					$psa=$result['psa'];
					$axial=$result['axial'];
					$xrays_sacroileal=$result['xrays_sacroileal'];
					$mri_sacroileal=$result['mri_sacroileal'];
					$xrays_vertebral=$result['xrays_vertebral'];
					$mri_vertebral=$result['mri_vertebral'];
					$axial_date=$result['axial_date_str'];
					if($axial_date=="12-12-1900")
						$axial_date="";
					$rheumatoid_arthritis_date=$result['rheumatoid_arthritis_date_str'];
					if($rheumatoid_arthritis_date=="12-12-1900")
						$rheumatoid_arthritis_date="";
					
				}
				else
				{
					$exec_prv = get_prv_cohort($pat_id);
					$result_prv = pg_fetch_array($exec_prv);
					$rheumatoid_arthritis=$result_prv['rheumatoid_arthritis'];
					$erosions=$result_prv['erosions'];
					$scharp=$result_prv['scharp'];
					$sens=$result_prv['sens'];
					$psa=$result_prv['psa'];
					$axial=$result_prv['axial'];
					$xrays_sacroileal=$result_prv['xrays_sacroileal'];
					$mri_sacroileal=$result_prv['mri_sacroileal'];
					$xrays_vertebral=$result_prv['xrays_vertebral'];
					$mri_vertebral=$result_prv['mri_vertebral'];
					$axial_date=$result_prv['axial_date_str'];
					if($axial_date=="12-12-1900")
						$axial_date="";
					$rheumatoid_arthritis_date=$result_prv['rheumatoid_arthritis_date_str'];
					if($rheumatoid_arthritis_date=="12-12-1900")
						$rheumatoid_arthritis_date="";
				}
				
				?>
		<div class="row">
			<div class="form-group col-md-12">
				<input type="submit" class="btn btn-primary" value="Save">&nbsp;&nbsp;&nbsp;
				<a href="followups.php"><input type="button" class="btn btn-primary" value="Follow-ups"></a>&nbsp;&nbsp;&nbsp;
				<!--<a href="followups_fix.php"><input type="button" class="btn btn-primary" value="SLICC/ACR Damage Index fix"></a>&nbsp;&nbsp;&nbsp;-->
				<a href="adverses_all.php"><input type="button" class="btn btn-primary" value="All Adverse events"></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Cohort</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-4" id="divtreatment" name="divtreatment">
							  <label>*Cohort Treatment</label>
							  <?php
								if($cohort_name=="")
								{
								?>
							  <select class="form-control" name="cohort_name" id="cohort_name">
								
								<option value="0" <?php if ($cohort_name==0) { echo "selected"; } ?>>--</option>
								<?php
									$sql = get_drugs_primary();
									$numrows = pg_num_rows($sql);
									while($result2 = pg_fetch_array($sql))
									{
										$drugs_id=$result2['drugs_id'];
										$value = $result2['code'];
										$value .= " - ";
										$value .= $result2['substance'];
										$value .= " (";
										$value .= $result2['route_of_administration_val'];
										$value .= ")";
																
									?>
								<option value="<?php echo $drugs_id; ?>" <?php if($drugs_id==$cohort_name) { echo "selected"; } ?>><?php echo $value; ?></option>
								<?php
									}
								
								?>
							  </select>
							  <?php
								}
							  else
								{
									$sql = get_drug($cohort_name);
									$numrows = pg_num_rows($sql);
									$result2 = pg_fetch_array($sql);
									$route_of_administration_val=$result2['route_of_administration_val'];
								?>
								<input type="text" class="form-control" readonly value="<?php echo $cohort_name_str." (".$route_of_administration_val.")"; ?>" id="cohort_name_str" name="cohort_name_str">
								<input type="hidden" class="form-control" value="<?php echo $cohort_name; ?>" id="cohort_name" name="cohort_name">
								<?php
								}
								?>
							</div>
							<div class="form-group col-md-4" id="divtreatment_nbr" name="divtreatment_nbr">
							  <label>*Treatment Number</label>
							  <select class="form-control" name="treatment_nbr" id="treatment_nbr">
								<option value="0" <?php if ($treatment_nbr==0) { echo "selected"; } ?>>--</option>
								<option value="-15" <?php if ($treatment_nbr=='-15') { echo "selected"; } ?>>-15</option>
								<option value="-14" <?php if ($treatment_nbr=='-14') { echo "selected"; } ?>>-14</option>
								<option value="-13" <?php if ($treatment_nbr=='-13') { echo "selected"; } ?>>-13</option>
								<option value="-12" <?php if ($treatment_nbr=='-12') { echo "selected"; } ?>>-12</option>
								<option value="-11" <?php if ($treatment_nbr=='-11') { echo "selected"; } ?>>-11</option>
								<option value="-10" <?php if ($treatment_nbr=='-10') { echo "selected"; } ?>>-10</option>
								<option value="-9" <?php if ($treatment_nbr=='-9') { echo "selected"; } ?>>-9</option>
								<option value="-8" <?php if ($treatment_nbr=='-8') { echo "selected"; } ?>>-8</option>
								<option value="-7" <?php if ($treatment_nbr=='-7') { echo "selected"; } ?>>-7</option>
								<option value="-6" <?php if ($treatment_nbr=='-6') { echo "selected"; } ?>>-6</option>
								<option value="-5" <?php if ($treatment_nbr=='-5') { echo "selected"; } ?>>-5</option>
								<option value="-4" <?php if ($treatment_nbr=='-4') { echo "selected"; } ?>>-4</option>
								<option value="-3" <?php if ($treatment_nbr=='-3') { echo "selected"; } ?>>-3</option>
								<option value="-2" <?php if ($treatment_nbr=='-2') { echo "selected"; } ?>>-2</option>
								<option value="-1" <?php if ($treatment_nbr=='-1') { echo "selected"; } ?>>-1</option>
								<option value="0.1" <?php if ($treatment_nbr=='0.1') { echo "selected"; } ?>>0.1</option>
								<option value="0.2" <?php if ($treatment_nbr=='0.2') { echo "selected"; } ?>>0.2</option>
								<option value="0.3" <?php if ($treatment_nbr=='0.3') { echo "selected"; } ?>>0.3</option>
								<option value="0.4" <?php if ($treatment_nbr=='0.4') { echo "selected"; } ?>>0.4</option>
								<option value="0.5" <?php if ($treatment_nbr=='0.5') { echo "selected"; } ?>>0.5</option>
								<option value="0.6" <?php if ($treatment_nbr=='0.6') { echo "selected"; } ?>>0.6</option>
								<option value="0.7" <?php if ($treatment_nbr=='0.7') { echo "selected"; } ?>>0.7</option>
								<option value="0.8" <?php if ($treatment_nbr=='0.8') { echo "selected"; } ?>>0.8</option>
								<option value="0.9" <?php if ($treatment_nbr=='0.9') { echo "selected"; } ?>>0.9</option>
								<option value="1" <?php if ($treatment_nbr=='1') { echo "selected"; } ?>>1</option>
								<option value="2" <?php if ($treatment_nbr=='2') { echo "selected"; } ?>>2</option>
								<option value="3" <?php if ($treatment_nbr=='3') { echo "selected"; } ?>>3</option>
								<option value="4" <?php if ($treatment_nbr=='4') { echo "selected"; } ?>>4</option>
								<option value="5" <?php if ($treatment_nbr=='5') { echo "selected"; } ?>>5</option>
								<option value="6" <?php if ($treatment_nbr=='6') { echo "selected"; } ?>>6</option>
								<option value="7" <?php if ($treatment_nbr=='7') { echo "selected"; } ?>>7</option>
								<option value="8" <?php if ($treatment_nbr=='8') { echo "selected"; } ?>>8</option>
								<option value="9" <?php if ($treatment_nbr=='9') { echo "selected"; } ?>>9</option>
								<option value="10" <?php if ($treatment_nbr=='10') { echo "selected"; } ?>>10</option>
								<option value="11" <?php if ($treatment_nbr=='11') { echo "selected"; } ?>>11</option>
								<option value="12" <?php if ($treatment_nbr=='12') { echo "selected"; } ?>>12</option>
								<option value="13" <?php if ($treatment_nbr=='13') { echo "selected"; } ?>>13</option>
								<option value="14" <?php if ($treatment_nbr=='14') { echo "selected"; } ?>>14</option>
								<option value="15" <?php if ($treatment_nbr=='15') { echo "selected"; } ?>>15</option>
							  </select>
							</div>
							  <div  class="form-group col-md-4" id="divstartdate" name="divstartdate">
								<label>*Cohort start date</label>
								<div class="input-group date">
								  <div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								  </div>
								  <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo $start_date; ?>" readonly>
								</div>
								<!-- /.input group -->
							  </div>
						</div>
						<div class="row">
							<div class="form-group col-md-4" id="divphysician" name="divphysician">
								<label>Physician assigning cohort</label>
								  <select class="form-control" name="physician_assigning" id="physician_assigning">
									<option value="0">--</option>
									<?php
										$sql = get_lookup_tbl_values_specific_parent("referringphysician",$_SESSION['user_centre_id']);
										$numrows = pg_num_rows($sql);
										while($result2 = pg_fetch_array($sql))
										{
											$physician_assigning_id=$result2['id'];
											$value =$result2['value'];
									?>
									<option value="<?php echo $physician_assigning_id; ?>" <?php if($physician_assigning_id==$physician_assigning) { echo "selected"; } ?>><?php echo $value; ?></option>
									<?php
										}
									?>
								  </select>
							</div>
							<div class="form-group col-md-4" id="divpreviousdmardsnbr" name="divpreviousdmardsnbr">
								<label>Number of previous DMARDs</label>
								  <select class="form-control" name="previous_dmards_nbr" id="previous_dmards_nbr">
									<option value="-1" <?php if($previous_dmards_nbr==-1) { echo "selected"; } ?>>--</option>
									<?php														
										for($i=0;$i<=30;$i++)
										{
									?>
									<option value="<?php echo $i; ?>" <?php if($previous_dmards_nbr==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
									<?php
									}
									?>
								  </select>
							</div>
							<div class="form-group col-md-4" id="divpreviousdmardsnbr" name="divpreviousdmardsnbr">
								<label>Number of co-administered DMARDs</label>
								  <select class="form-control" name="ongoing_dmards_nbr" id="ongoing_dmards_nbr">
								  <option value="-1" <?php if($ongoing_dmards_nbr==-1) { echo "selected"; } ?>>--</option>
									<?php														
										for($i=0;$i<=5;$i++)
										{
									?>
									<option value="<?php echo $i; ?>" <?php if($ongoing_dmards_nbr==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
									<?php
									}
									?>
								  </select>
							</div>
						</div>
						<div class="row">
							<div  class="form-group col-md-4" id="divstartdate" name="divstartdate">
								<label>Cohort stop date</label>
								<div class="input-group date">
								  <div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								  </div>
								  <input type="text" class="form-control" id="stop_date" name="stop_date" value="<?php echo $stop_date; ?>" data-inputmask="'alias': 'dd-mm-yyyy'">
								</div>
								<!-- /.input group -->
							  </div>
							<div class="form-group col-md-4" id="divstopreason" name="divstopreason">
								<label>Cohort stop reason</label>
								  <select class="form-control" name="stop_reason" id="stop_reason">
									<option value="0">--</option>
									<?php														
									$sql = get_discontinuation_reason();
									$numrows = pg_num_rows($sql);
									while($result2 = pg_fetch_array($sql))
									{
										$discontinuation_reason_id=$result2['discontinuation_reason_id'];
										$value=$result2['value'];
													
									?>
									<option value="<?php echo $discontinuation_reason_id; ?>" <?php if($discontinuation_reason_id==$stop_reason) { echo "selected"; } ?>><?php echo $value; ?></option>
									<?php
									}
									?>
								  </select>
							</div>
							<div class="form-group col-md-4" id="divstopaereason" name="divstopaereason">
								<label>Cohort stop AE reason</label>
								  <textarea class="form-control" name="stop_ae_reason" id="stop_ae_reason"><?php echo $stop_ae_reason; ?></textarea>
							</div>
						</div>
						<div class="row">
							<div  class="form-group col-md-4" id="divstartdate" name="divstartdate">
								<label>Irregular  followup</label>
								<div class="input-group date">
								  <input type="checkbox" id="irregularfu" name="irregularfu" <?php if ($irregularfu==1) { echo "checked"; } ?>>
								</div>
								<!-- /.input group -->
							  </div>
						</div>
					</div>
				</div>
				<!-- cohort div end -->
				<!-- demographic div start -->
				<?php
				
				if($patient_cohort_id=="")
				{
					$exec = get_patient_demographics($pat_id);
					$result = pg_fetch_array($exec);
					$contraceptive_methods=$result['contraceptivemethods'];
					$pregnancies=$result['pregnancies'];
					$childbirths=$result['childbirths'];
					$pregnancy_loses=$result['pregnancylosesbeforeno'];
					$premature_births=$result['prematurebirthsno'];
					$eclampsia=$result['eclampsiano'];
					$premature_rom=$result['prematureno'];
					$placental_insufficiency=$result['placentalno'];
					$premature_births_reason=$result['prematureotherno'];
					//$employment_at_cohort=$result['followupemploymentstatus'];
					$employment_at_cohort=$result['employmentstatus'];
					$order = get_resindence($pat_id);
					$num_rows = pg_num_rows($order);
											
					$result = pg_fetch_assoc($order);
					$residence=$result['resindence'];
					
					$order = get_marital($pat_id);
					$result = pg_fetch_assoc($order);
					$marital_status=$result['marital_status'];
					$marital_status_year=$result['marital_year'];
					
					$order = get_physical($pat_id);
					$result = pg_fetch_assoc($order);
					$physical_status=$result['status'];
					$date_phys_str=$result['date_str'];
					
					
					$order = get_mediterranean($pat_id);
					$mediterranean_diet = pg_num_rows($order);
				}
				else
				{
				}
				?>
				<a class="btn btn-primary" data-toggle="collapse" href="#collapse" role="button" aria-expanded="false" aria-controls="collapse">
					Demographic at cohort assignment
				  </a>
				  <br>
				<div class="box box-primary <?php if ($patient_cohort_id<>"") { echo 'collapse';  } ?>"  id="collapse">
					<!--<div class="box-header with-border">
						<h3 class="box-title"></h3>
					</div>-->
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-4">
							  <label>Age</label>
							  <input type="text" class="form-control" readonly value="<?php echo $age_at_cohort; ?>" id="age_at_cohort" name="age_at_cohort">
							</div>
							<div class="form-group col-md-4" id="divresidence" name="divresidence">
								<label>Residence</label>
								  <select class="form-control" name="residence" id="residence">
										<option value="0" <?php if ($residence==0) { echo "selected"; } ?>>--</option>
										<option value="1" <?php if ($residence==1) { echo "selected"; } ?>>Urban (>15.000 dwellers)</option>
										<option value="2" <?php if ($residence==2) { echo "selected"; } ?>>Suburban (10-15.000 dwellers)</option>
										<option value="3" <?php if ($residence==3) { echo "selected"; } ?>>Rular (<10.000 dwellers)</option>
										<option value="4" <?php if ($residence==4) { echo "selected"; } ?>>Don't know</option>
									</select>
							</div>
						</div>
					</div>
					<!-- Obstetric history start div -->
					<div class="box-header with-border" <?php if($gender==2) { ?> style="DISPLAY:none;" <?php  } ?>>
						<h3 class="box-title">Obstetric history</h3>
					</div>
					<div class="box-body" <?php if($gender==2) { ?> style="DISPLAY:none;" <?php  } ?>>
							<div class="row">
								<div class="form-group col-md-3" id="divcontraceptivemethods" name="divcontraceptivemethods">
								  <label>Use of contraceptive methods</label>
								  <select class="form-control" name="contraceptive_methods" id="contraceptive_methods">
									<option value="0" <?php if ($contraceptive_methods==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if ($contraceptive_methods==1) { echo "selected"; } ?>>No</option>
									<option value="2" <?php if ($contraceptive_methods==2) { echo "selected"; } ?>>Yes</option>
								  </select>
								</div>
								<div class="form-group col-md-3" id="divpregnancies" name="divpregnancies">
								  <label>No.Pregnancies</label>
								  <select class="form-control" name="pregnancies" id="pregnancies">
									<option value="-1" <?php if ($pregnancies==-1 or $pregnancies=="") { echo "selected"; } ?>>--</option>
									<?php 
										for($i=0;$i<=15;$i++)
										{
									?>
									<option value="<?php echo $i; ?>" <?php if ($pregnancies==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
									<?php 
										}
									?>
								  </select>
								</div>
								<div class="form-group col-md-2" id="divchildbirths" name="divchildbirths">
								  <label>No.Childbirths</label>
								  <select class="form-control" name="childbirths" id="childbirths">
									<option value="-1" <?php if ($childbirths==-1 or $childbirths=="") { echo "selected"; } ?>>--</option>
									<?php 
										for($i=0;$i<=15;$i++)
										{
									?>
									<option value="<?php echo $i; ?>" <?php if ($childbirths==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
									<?php 
										}
									?>
									</select>
								</div>
								<div class="form-group col-md-2">
									<label>No.Pregnancy loses</label>
									<div class="input-group">
										<select class="form-control" name="pregnancy_loses" id="pregnancy_loses">
											<option value="0" <?php if ($pregnancy_loses=="0") { echo "selected"; } ?>>--</option>
											<?php 
												for($i=1;$i<=15;$i++)
												{
											?>
												<option value="<?php echo $i; ?>" <?php if ($pregnancy_loses==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
											<?php 
												}
											?>
										  </select>
									</div>
									<!-- /.input group -->
								</div>
								<div class="form-group col-md-2" id="divprematurebirths" name="divprematurebirths">
									<div  id="divprematurebirthsno" name="divprematurebirthsno">
											<label>Premature births</label>
											<div class="input-group">
												<select class="form-control" name="premature_births" id="premature_births">
													<option value="0" <?php if ($premature_births=="0") { echo "selected"; } ?>>--</option>
													<?php 
														for($i=1;$i<=15;$i++)
														{
													?>
													<option value="<?php echo $i; ?>" <?php if ($premature_births==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
													<?php 
														}
													?>
												  </select>
											</div>
											<!-- /.input group -->
									</div>
								</div>
							</div>
						<div class="row">
							<div class="form-group col-md-3" id="diveclampsia" name="diveclampsia">
							 <label>Reason for premature births</label>
								<div class="form-group">
									<div  id="diveclampsiano" name="diveclampsiano">
											<label>(pre-)eclampsia</label>
											<div class="input-group">
												<select class="form-control" name="eclampsia" id="eclampsia">
													<option value="0" <?php if ($eclampsia=="0") { echo "selected"; } ?>>--</option>
													<?php 
														for($i=1;$i<=15;$i++)
														{
													?>
													<option value="<?php echo $i; ?>" <?php if ($eclampsia==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
													<?php 
														}
													?>
												  </select>
											</div>
											<!-- /.input group -->
										  </div>
								</div>
							</div>
							<div class="form-group col-md-3" id="divpremature" name="divpremature">
							 	<label>&nbsp;</label>
								<div class="form-group">
									<div  id="divprematureno" name="divprematureno">
											<label>premature ROM</label>
											<div class="input-group">
												<select class="form-control" name="premature_rom" id="premature_rom">
													<option value="0" <?php if ($premature_rom=="0") { echo "selected"; } ?>>--</option>
													<?php 
														for($i=1;$i<=15;$i++)
														{
													?>
													<option value="<?php echo $i; ?>" <?php if ($premature_rom==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
													<?php 
														}
													?>
												  </select>
											</div>
											<!-- /.input group -->
										  </div>
								</div>
							</div>
							<div class="form-group col-md-3" id="divplacental" name="divplacental">
							 <label>&nbsp;</label>
								<div class="form-group">
									<div  id="divplacentalno" name="divplacentalno">
											<label>placental insufficiency</label>
											<div class="input-group">
												<select class="form-control" name="placental_insufficiency" id="placental_insufficiency">
													<option value="0" <?php if ($placental_insufficiency=="0") { echo "selected"; } ?>>--</option>
													<?php 
														for($i=1;$i<=15;$i++)
														{
													?>
													<option value="<?php echo $i; ?>" <?php if ($placental_insufficiency==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
													<?php 
														}
													?>
												  </select>
											</div>
											<!-- /.input group -->
										  </div>
								</div>
							</div>
							<div class="form-group col-md-3" id="divprematureother" name="divprematureother">
							 <label>&nbsp;</label>
								<div class="form-group">
									<div  id="divprematureotherno" name="divprematureotherno">
											<label>Other</label>
											<div class="input-group">
												<select class="form-control" name="premature_births_reason" id="premature_births_reason">
													<option value="0" <?php if ($premature_births_reason=="0") { echo "selected"; } ?>>--</option>
													<?php 
														for($i=1;$i<=15;$i++)
														{
													?>
													<option value="<?php echo $i; ?>" <?php if ($premature_births_reason==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
													<?php 
														}
													?>
												  </select>
											</div>
											<!-- /.input group -->
										  </div>
								</div>
							</div>
						</div>
				  </div>
				  <!-- Obstetric history end div -->
				  <!-- Employment start div -->
				  <div class="box-header with-border">
						<h3 class="box-title">Employment</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<!--<div class="form-group col-md-5" id="divemployment" name="divemployment">
							  <label>Has the employment status at inclusion changed during new cohort?</label>
							  <select class="form-control" name="employment_changed" id="employment_changed">
								<option value="0" <?php //if ($employment_changed==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php //if ($employment_changed==1) { echo "selected"; } ?>>Yes</option>
								<option value="2" <?php //if ($employment_changed==2) { echo "selected"; } ?>>No</option>
							  </select>
							</div>-->
							<div class="form-group col-md-3" id="divemploymentstatus" name="divemploymentstatus">
							  <label>Employment status at cohort</label>
							  <select class="form-control" name="employment_at_cohort" id="employment_at_cohort">
								<option value="0" <?php if ($employment_at_cohort==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($employment_at_cohort==1) { echo "selected"; } ?>>Unemployed</option>
								<option value="2" <?php if ($employment_at_cohort==2) { echo "selected"; } ?>>Student</option>
								<option value="3" <?php if ($employment_at_cohort==3) { echo "selected"; } ?>>Employee</option>
								<option value="4" <?php if ($employment_at_cohort==4) { echo "selected"; } ?>>Worker</option>
								<option value="5" <?php if ($employment_at_cohort==5) { echo "selected"; } ?>>Self-employed</option>
								<option value="6" <?php if ($employment_at_cohort==6) { echo "selected"; } ?>>Retired</option>
								<option value="7" <?php if ($employment_at_cohort==7) { echo "selected"; } ?>>Disability pension</option>
								<option value="9" <?php if ($employment_at_cohort==9) { echo "selected"; } ?>>Housewife</option>
								<option value="8" <?php if ($employment_at_cohort==8) { echo "selected"; } ?>>Unknown</option>
								
							  </select>
							</div>
						</div>
					</div><!-- Employment end div -->
					 <!-- Marital status start div -->
					<div class="box-header with-border">
						<h3 class="box-title">Marital status</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-5" id="divemployment" name="divemployment">
							  <label>Status</label>
								 <select class="form-control" name="marital_status" id="marital_status">
									<option value="1" <?php if ($marital_status==1) { echo "selected"; } ?>>Not married</option>
									<option value="2" <?php if ($marital_status==2) { echo "selected"; } ?>>Married</option>
									<option value="3" <?php if ($marital_status==3) { echo "selected"; } ?>>Divorced</option>
									<option value="4" <?php if ($marital_status==4) { echo "selected"; } ?>>Widow</option>
									<option value="5" <?php if ($marital_status==5) { echo "selected"; } ?>>Unknown</option>
								</select>
							</div>
							<div class="form-group col-md-3" id="divemploymentstatus" name="divemploymentstatus">
							  <label>Year</label>
								<input type="text" class="form-control pull-right" data-inputmask="'mask': '9999'" value="<?php echo $marital_status_year; ?>" id="marital_status_year" name="marital_status_year">
							</div>
						</div>
					</div><!-- Marital status end div -->
					<!-- physical status start div -->
					<div class="box-header with-border">
						<h3 class="box-title">Physical activity</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-5" id="divemployment" name="divemployment">
							  <label>Status</label>
								 <select class="form-control" name="physical_status" id="physical_status">
									<option value="0" <?php if ($physical_status==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if ($physical_status==1) { echo "selected"; } ?>>Moving only for necesary chores</option>
									<option value="2" <?php if ($physical_status==2) { echo "selected"; } ?>>Walking or other outdoor activities 1-2 times per week</option>
									<option value="3" <?php if ($physical_status==3) { echo "selected"; } ?>>Walking or other outdoor activities several times per week</option>
									<option value="4" <?php if ($physical_status==4) { echo "selected"; } ?>>Exercing 1-2 times per week to the point of perspiring and heavy breathing</option>
									<option value="5" <?php if ($physical_status==5) { echo "selected"; } ?>>Exercing several times per week to the point of perspiring and heavy breathing</option>
									<option value="6" <?php if ($physical_status==6) { echo "selected"; } ?>>Keep-fit heavy exercise ot competitive sport several times per week</option>
								</select>
							</div>
							<div class="form-group col-md-3" id="divemploymentstatus" name="divemploymentstatus">
							  <label>date</label>
								<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" value="<?php echo $date_phys_str; ?>" id="date_phys_str" name="date_phys_str">
							</div>
						</div>
					</div><!-- physical status end div -->
					<!-- rest start div -->
					<?php
						$order = get_alcohol($pat_id);
						$sum=0;
						while($result2 = pg_fetch_array($order))
						{	
								$type=$result2['type'];
								$dose=$result2['dose'];
								if($type==1)
									$type_gr='12.8';
								if($type==2)
									$type_gr='11';
								if($type==3)
									$type_gr='14';
								
								if($dose==1)
									$dose_gr='0';
								if($dose==2)
									$dose_gr='12';
								if($dose==3)
									$dose_gr='75';
								if($dose==4)
									$dose_gr='2';
								if($dose==5)
									$dose_gr='22';
								if($dose==6)
									$dose_gr='135';
								if($dose==7)
									$dose_gr='4';
								if($dose==8)
									$dose_gr='30';
								if($dose==9)
									$dose_gr='180';
								
								$sum += $type_gr*$dose_gr;
								
						}
					?>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-4">
							  <label>Alcohol consumption (gr/month)</label>
								 <input type="text" class="form-control" value="<?php if($patient_cohort_id<>"") { echo $alcohol_consumption; } else { echo $sum; } ?>" id="alcohol_consumption" name="alcohol_consumption">
							</div>
							<div class="form-group col-md-4">
							  <label>Mediterranean diet score</label>
								<input type="text" class="form-control" value="<?php echo $mediterranean_diet; ?>" id="mediterranean_diet" name="mediterranean_diet">
							</div>
						</div>
					</div><!-- rest end div -->
				</div><!-- demographic div end -->
				</br>
				<!-- Anti-Rheumatic drug treatment start div -->
				<a class="btn btn-primary" data-toggle="collapse" href="#collapse2" role="button" aria-expanded="false" aria-controls="collapse2">
					Previous Anti-Rheumatic drug treatment
				  </a>
				  <br>
				<div class="row <?php if ($patient_cohort_id<>"") { echo 'collapse';  } ?>"   id="collapse2">
					<?php
						$patient_previous_drugs_counter=0;
						$order = get_patient_previous_drugs($pat_id);
						while($result = pg_fetch_assoc($order))
						{
							$drugs_id=$result['drugs_id'];
							
							$sql = get_drugs_anti_spec($drugs_id);
							$result2 = pg_fetch_array($sql);
							$value = $result2['code'];
							$value .= "-";
							$value .= $result2['substance'];
							$value .= " (";
							$value .= $result2['route_of_administration_val'];
							$value .= ")";
							
							$patient_previous_drugs_values .=  $value;
							$patient_previous_drugs_values .= "!@#$%^" ;
							$patient_previous_drugs_ids .=$result['drugs_id'];
							$patient_previous_drugs_ids .= "!@#$%^";
							$patient_previous_drugs_counter++;
						}
					?>
					<input type="hidden" value="<?php echo $patient_previous_drugs_counter; ?>" id="patient_previous_drugs_counter" name="patient_previous_drugs_counter"/>
					<input type="hidden" value="<?php echo $patient_previous_drugs_values; ?>" id="patient_previous_drugs_values" name="patient_previous_drugs_values"/>
					<input type="hidden" value="<?php echo $patient_previous_drugs_ids; ?>" id="patient_previous_drugs_ids" name="patient_previous_drugs_ids"/>
											
					<div class="col-md-12">
						<div class="box box-primary">
							<!--<div class="box-header with-border">
								<h3 class="box-title">Anti-Rheumatic drug treatment</small></h3>
							</div>-->
							<div class="box-body">
								<div class="row">
							<div class="form-group col-md-8" id="divantirheumatic" name="divantirheumatic">
								<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="patient_cohort_previous_drugs_tbl" name="patient_cohort_previous_drugs_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-6"><b>Drug</b></th>
											<!--<th class="col-md-2"><b>Route of</br>administration</b></th>
											<th class="col-md-2"><b>Treatment duration</br>(months)</b></th>
											<th class="col-md-1"><b>Year of</br>therapy start</b></th>
											<th class="col-md-1"><b>Treatment stop</b></th>
											<th class="col-md-2"><b>Reason of</br>discontinuation</b></th>-->
											<th class="col-md-2">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
											if($patient_cohort_id=="")
												$order = get_patient_previous_drugs($pat_id);
											else
												$order = get_patient_cohort_previous_drugs($patient_cohort_id);
											
											$num_rows = pg_num_rows($order);
											
											while($result = pg_fetch_assoc($order))
											{
												//$route_of_administration=$result['route_of_administration'];
												$patient_cohort_previous_drugs_id=$result['patient_cohort_previous_drugs_id'];
												$drugs_id=$result['drugs_id'];
												//$treatment_duration=$result['treatment_duration'];
												//$year_of_therapy_start=$result['year_of_therapy_start'];
												//$discontinuation_reason_id=$result['reason_of_discontinuation_id'];
												//$treatment_stop=$result['treatment_stop'];
									?>
										  <tr id="patient_previous_drugs_tr_<?php echo $i;?>">
												<td align="center">
													<input type="hidden" id="patient_previous_drugs_hidden_<?php echo $i;?>" name="patient_previous_drugs_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="patient_cohort_previous_drugs_id_<?php echo $i; ?>" id="patient_cohort_previous_drugs_id_<?php echo $i; ?>" value="<?php echo $patient_cohort_previous_drugs_id;?>">
													<!--<input type="hidden" name="patient_previous_drugs_id_<?php echo $i; ?>" id="patient_previous_drugs_id_<?php echo $i; ?>" value="<?php echo $drugs_id;?>">
													<input type="hidden" name="treatment_duration_<?php echo $i; ?>" id="treatment_duration_<?php echo $i; ?>" value="<?php echo $treatment_duration;?>">
													<input type="hidden" name="year_of_therapy_start_<?php echo $i; ?>" id="year_of_therapy_start_<?php echo $i; ?>" value="<?php echo $year_of_therapy_start;?>">
													<input type="hidden" name="treatment_stop_<?php echo $i; ?>" id="treatment_stop_<?php echo $i; ?>" value="<?php echo $treatment_stop;?>">
													<input type="hidden" name="reason_of_discontinuation_id_<?php echo $i; ?>" id="reason_of_discontinuation_id_<?php echo $i; ?>" value="<?php echo $discontinuation_reason_id;?>">-->
													<select  class="form-control" name="patient_previous_drugs_id_<?php echo $i; ?>" id="patient_previous_drugs_id_<?php echo $i; ?>">
																<option value="0">--</option>
																<?php
																	$sql3 = get_patient_previous_drugs($pat_id);
																	$numrows3 = pg_num_rows($sql3);
																	while($result3 = pg_fetch_array($sql3))
																	{
																		$drugs_id2=$result3['drugs_id'];
																		$sql = get_drugs_anti_spec($drugs_id2);
																		$result2 = pg_fetch_array($sql);
																		
																		$value = $result2['code'];
																		$value .= "-";
																		$value .= $result2['substance'];
																		$value .= " (";
																		$value .= $result2['route_of_administration_val'];
																		$value .= ")";
																		
																		
																		
																?>
																<option value="<?php echo $drugs_id2; ?>" <?php if($drugs_id==$drugs_id2) { echo "selected"; } ?>><?php echo $value; ?></option>
																<?php
																	}
																?>
													</select>
												</td>
												<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="patient_previous_drugs_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
												 </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										
									?>
									</tbody>
								</table>
								<input type="hidden" name="patient_previous_drugs_numofrows" id="patient_previous_drugs_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								<a class="btn btn-primary" href="javascript:void(0);" onclick="patient_cohort_previous_drugs_addrow();"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
							</div>
						</div>
					</div>
				</div><!-- Anti-Rheumatic drug treatment end div -->
				</br>
				
				<!-- treatments start div -->
				<div class="row">
					<div class="col-md-12">
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Treatments</small></h3>
							</div>
							<div class="box-body">
								<div class="row">
									<div class="form-group col-md-12" id="divdiseases" name="divdiseases">
									<?php
												$sql = get_drugs_ongoing();
												$drugs_counter=0;
												
												while($result2 = pg_fetch_array($sql))
												{	
													$drugs_values .= $result2['code'];
													$drugs_values .= "-";
													$drugs_values .= $result2['substance'];
													$drugs_values .= "( min: ";
													$drugs_values .= $result2['min'];
													$drugs_values .= " - max:";
													$drugs_values .= $result2['max'];
													$drugs_values .= " / route: ";
													$drugs_values .= $result2['route_of_administration_val'];
													$drugs_values .= " )";
													
													$drugs_values .= "!@#$%^" ;
													
													$drugs_ids .=$result2['drugs_id'];
													$drugs_ids .= "!@#$%^";
													
													$drugs_maxs .=$result2['max'];
													$drugs_maxs .= "!@#$%^";
													
													$drugs_mins .=$result2['min'];
													$drugs_mins .= "!@#$%^";
																	
													$drugs_counter++;
												}
											?>
												<input type="hidden" value="<?php echo $drugs_counter; ?>" id="drugs_counter" name="drugs_counter"/>
												<input type="hidden" value="<?php echo $drugs_values; ?>" id="drugs_values" name="drugs_values"/>
												<input type="hidden" value="<?php echo $drugs_ids; ?>" id="drugs_ids" name="drugs_ids"/>
												<input type="hidden" value="<?php echo $drugs_maxs; ?>" id="drugs_maxs" name="drugs_maxs"/>
												<input type="hidden" value="<?php echo $drugs_mins; ?>" id="drugs_mins" name="drugs_mins"/>
											
											<?php
												/*$sql = get_lookup_tbl_values('route_of_administration');
												$route_counter=0;				
												while($result2 = pg_fetch_array($sql))
												{	
													$route_values .= $result2['value'];
													$route_values .= "!@#$%^" ;
													$route_ids .=$result2['id'];
													$route_ids .= "!@#$%^";
																	
													$route_counter++;
												}*/
											?>
												<!--<input type="hidden" value="<?php echo $route_counter; ?>" id="route_counter" name="route_counter"/>
												<input type="hidden" value="<?php echo $route_values; ?>" id="route_values" name="route_values"/>
												<input type="hidden" value="<?php echo $route_ids; ?>" id="route_ids" name="route_ids"/>-->
												
												<?php
												$sql = get_drugs_frequencies();
												$drugs_frequency_counter=0;
												
												while($result2 = pg_fetch_array($sql))
												{	
													$drugs_frequency_values .= $result2['value'];
													$drugs_frequency_values .= "!@#$%^" ;
													
													$drugs_frequency_ids .=$result2['drugs_frequency_id'];
													$drugs_frequency_ids .= "!@#$%^" ;
																	
													$drugs_frequency_counter++;
												}
											?>
												<input type="hidden" value="<?php echo $drugs_frequency_counter; ?>" id="drugs_frequency_counter" name="drugs_frequency_counter"/>
												<input type="hidden" value="<?php echo $drugs_frequency_values; ?>" id="drugs_frequency_values" name="drugs_frequency_values"/>
												<input type="hidden" value="<?php echo $drugs_frequency_ids; ?>" id="drugs_frequency_ids" name="drugs_frequency_ids"/>
												<?php
												
												$drugs_reason_values .= 'Change of main diagnosis';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="20";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Insufficient response (primary)';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="1";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Secondary failure';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="2";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Failure & adverse event';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="3";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Lost to follow-up';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="4";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Drug intolerance';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="5";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Drug reaction/Allergy';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="6";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Infection';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="7";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Remission';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="8";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Solid tumor/Lymphoma';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="9";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Other adverse event';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="10";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'patient decision';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="11";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Planned therapy';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="12";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Pregnancy';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="13";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Financial';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="14";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Change to BIOsimilar';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="15";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Change practical reasons';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="16";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Change to bio-originator';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="17";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Other';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="18";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Unknown';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="19";
												$drugs_reason_ids .= "!@#$%^" ;
												
												
											?>
												<input type="hidden" value="19" id="drugs_reason_counter" name="drugs_reason_counter"/>
												<input type="hidden" value="<?php echo $drugs_reason_values; ?>" id="drugs_reason_values" name="drugs_reason_values"/>
												<input type="hidden" value="<?php echo $drugs_reason_ids; ?>" id="drugs_reason_ids" name="drugs_reason_ids"/>
										  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="patient_drugs_tbl" name="patient_drugs_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th colspan="3" class="col-md-6"><b>Drug</b></th>
													<th class="col-md-1"><b>Main</br>treatment</b></th>
													<!--<th class="col-md-2"><b>Route of</br>administration</b></th>-->
													<th class="col-md-2"><b>Start date</b></th>
													<th class="col-md-2"><b>Stop date</b></th>
													<th class="col-md-1">&nbsp;</th>
												</tr>
											 </thead>
											 <tbody>
											<?php
												$i=0;
												$j=0;
													$order = get_patient_cohort_drugs($patient_cohort_id);
													$num_rows = pg_num_rows($order);
													$drugs_row=0;
													while($result = pg_fetch_assoc($order))
													{
														$i++;
														$j++;
														$patient_cohort_drugs_id=$result['patient_cohort_drugs_id'];
														$drugs_id=$result['drugs_id'];
														$patient_cohort_drugs_min=$result['min'];
														$patient_cohort_drugs_max=$result['max'];
														$main=$result['main'];
														$route_of_administration=$result['route_of_administration'];
														$dose=$result['dose'];
														$frequency=$result['frequency'];
														$startdate_str=$result['startdate_str'];
														if ($startdate_str=='12-12-1900')
															$startdate_str='';
														$stopdate_str=$result['stopdate_str'];
														if ($stopdate_str=='12-12-1900')
															$stopdate_str='';
														
														$drugs_row=$i;
											?>
												  <tr id="patient_cohort_drugs_tr_<?php echo $i;?>" style="background-color:#f0f8ff;">
														<td colspan="3" align="center">
															<input type="hidden" id="patient_cohort_drugs_type_<?php echo $i;?>" name="patient_cohort_drugs_type_<?php echo $i;?>" value="cohort"/>
															<input type="hidden" id="patient_cohort_drugs_hidden_<?php echo $i;?>" name="patient_cohort_drugs_hidden_<?php echo $i;?>" value="1"/>
															<input type="hidden" name="patient_cohort_drugs_id_<?php echo $i; ?>" id="patient_cohort_drugs_id_<?php echo $i; ?>" value="<?php echo $patient_cohort_drugs_id;?>">
															<input type="hidden" name="patient_cohort_drugs_row_<?php echo $i; ?>" id="patient_cohort_drugs_row_<?php echo $i; ?>" value="<?php echo $drugs_row;?>">
															
															<input type="hidden" name="patient_cohort_drugs_min_<?php echo $i; ?>" id="patient_cohort_drugs_min_<?php echo $i; ?>" value="<?php echo $patient_cohort_drugs_min;?>">
															<input type="hidden" name="patient_cohort_drugs_max_<?php echo $i; ?>" id="patient_cohort_drugs_max_<?php echo $i; ?>" value="<?php echo $patient_cohort_drugs_max;?>">
															
															<select  class="form-control" name="drugs_id_<?php echo $i; ?>" id="drugs_id_<?php echo $i; ?>" onchange="setminmax(<?php echo $i; ?>)">
																<option value="0">--</option>
																<?php
																	$sql = get_drugs_ongoing();
																	$numrows = pg_num_rows($sql);
																	while($result2 = pg_fetch_array($sql))
																	{
																		$drugs_id2=$result2['drugs_id'];
																		$value = $result2['code'];
																		$value .= "-";
																		$value .= $result2['substance'];
																		$value .= "( min: ";
																		$value .= $result2['min'];
																		$value .= " - max:";
																		$value .= $result2['max'];
																		$value .= " / route: ";
																		$value .= $result2['route_of_administration_val'];
																		$value .= " )";
																		
																		
																		
																?>
																<option value="<?php echo $drugs_id2; ?>" <?php if($drugs_id==$drugs_id2) { echo "selected"; } ?>><?php echo $value; ?></option>
																<?php
																	}
																?>
															</select>
														</td>
														<td align="center">
															<input type="checkbox" id="main_<?php echo $i; ?>" name="main_<?php echo $i; ?>" onclick="calc(<?php echo $i; ?>)" <?php if ($main==1) { echo 'checked'; }; ?>>
														</td>
														<!--<td align="center">
															<select class="form-control" name="route_of_administration_<?php /* echo $i; ?>" id="route_of_administration_<?php echo $i; ?>">
																<option value="0" <?php if($route_of_administration==0) { echo "selected"; } ?>>--</option>
																<?php
																	$sql = get_lookup_tbl_values('route_of_administration');
																	$numrows = pg_num_rows($sql);
																	while($result2 = pg_fetch_array($sql))
																	{
																		$route=$result2['id'];
																		$value=$result2['value'];
																?>
																	<option value="<?php echo $route; ?>" <?php if($route_of_administration==$route) { echo "selected"; } ?>><?php echo $value; ?></option>
																<?php
																	}*/
																		
																?>
															</select>
														</td>-->
														 <td align="center">
															<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="startdate_<?php echo $i; ?>"  name="startdate_<?php echo $i; ?>" value="<?php echo $startdate_str; ?>">
														</td>
														<td align="center">
															<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="stopdate_<?php echo $i; ?>"  name="stopdate_<?php echo $i; ?>" value="<?php echo $stopdate_str; ?>">
														</td>	
													 	<td align="center">
															<a <?php if($main==1) { ?> style="DISPLAY:none;" <?php }?> id="a_<?php echo $i; ?>" name="a_<?php echo $i; ?>" title="Delete" href="javascript:void(0);" onclick="javascript:if (confirm('Are you sure;All periods will be deleted!')) { patient_cohort_drugs_removerow(<?php echo $i; ?>)};"><i class="fa fa-trash"></i><span></a>
														</td>
														 </tr>
											<?php
														$i++;
														$j++;
											?>
												<tr id="patient_cohort_drugs_tr_<?php echo $i;?>">
													 <td align="center">
															Periods
														</td>
														 <td align="center">
															<input type="hidden" id="patient_cohort_drugs_hidden_<?php echo $i;?>" name="patient_cohort_drugs_hidden_<?php echo $i;?>" value="1"/>
															<input type="hidden" id="patient_cohort_drugs_type_<?php echo $i;?>" name="patient_cohort_drugs_type_<?php echo $i;?>" value="period_label"/>
															<input type="hidden" name="patient_cohort_drugs_row_<?php echo $i; ?>" id="patient_cohort_drugs_row_<?php echo $i; ?>" value="<?php echo $drugs_row;?>">
															<input type="hidden" name="patient_cohort_drugs_id_<?php echo $i; ?>" id="patient_cohort_drugs_id_<?php echo $i; ?>" value="<?php echo $patient_cohort_drugs_id;?>">
															Reason for </br> treatment change
														</td>
														<td align="center">
															frequency
														</td>
														<td align="center">
															dose
														</td>
														<td align="center">
															start date
														</td>
														<td align="center">
															stop date
														</td>
														<td align="center">
															<a id="a2_add_<?php echo $i; ?>" name="a2_add_<?php echo $i; ?>" title="Add new period" href="javascript:void(0);" onclick="patient_cohort_drugs_newperiod(<?php echo $i; ?>,<?php echo $patient_cohort_drugs_id; ?>);"><i class="fa fa-calendar-plus-o"></i><span></a>
														</td>
												</tr>
											<?php
														
														$order2 = get_patient_cohort_drugs_period($patient_cohort_drugs_id);
														$num_rows2 = pg_num_rows($order2);
														
														while($result2 = pg_fetch_assoc($order2))
														{
															$i++;
															$j++;
															$patient_cohort_drugs_period_id=$result2['patient_cohort_drugs_period_id'];
															$patient_cohort_drugs_id=$result2['patient_cohort_drugs_id'];
															$reason=$result2['reason'];
															$dose=$result2['dose'];
															$frequency=$result2['frequency'];
															$startdate_str=$result2['startdate_str'];
															if ($startdate_str=='12-12-1900')
																$startdate_str='';
															$stopdate_str=$result2['stopdate_str'];
															if ($stopdate_str=='12-12-1900')
																$stopdate_str='';
											?>
												  <tr id="patient_cohort_drugs_tr_<?php echo $i;?>">
														<td align="center">
															<input type="hidden" id="patient_cohort_drugs_type_<?php echo $i;?>" name="patient_cohort_drugs_type_<?php echo $i;?>" value="period"/>
															<input type="hidden" id="patient_cohort_drugs_hidden_<?php echo $i;?>" name="patient_cohort_drugs_hidden_<?php echo $i;?>" value="1"/>
															<input type="hidden" name="patient_cohort_drugs_id_<?php echo $i; ?>" id="patient_cohort_drugs_id_<?php echo $i; ?>" value="<?php echo $patient_cohort_drugs_id;?>">
															<input type="hidden" name="patient_cohort_drugs_period_id_<?php echo $i; ?>" id="patient_cohort_drugs_period_id_<?php echo $i; ?>" value="<?php echo $patient_cohort_drugs_period_id;?>">
															<input type="hidden" name="patient_cohort_drugs_row_<?php echo $i; ?>" id="patient_cohort_drugs_row_<?php echo $i; ?>" value="<?php echo $drugs_row;?>">
															&nbsp;
														</td>
														<td align="center">
															<select class="form-control" name="reason_<?php echo $i; ?>" id="reason_<?php echo $i; ?>">
																<option value="0" <?php if($reason==0) { echo "selected"; } ?>>--</option>
																<option value="20" <?php if($reason==20) { echo "selected"; } ?>>Change of main diagnosis</option>
																<option value="1" <?php if($reason==1) { echo "selected"; } ?>>Insufficient response (primary)</option>
																<option value="2" <?php if($reason==2) { echo "selected"; } ?>>Secondary failure</option>
																<option value="3" <?php if($reason==3) { echo "selected"; } ?>>Failure & adverse event</option>
																<option value="4" <?php if($reason==4) { echo "selected"; } ?>>Lost to follow-up</option>
																<option value="5" <?php if($reason==5) { echo "selected"; } ?>>Drug intolerance</option>
																<option value="6" <?php if($reason==6) { echo "selected"; } ?>>Drug reaction/Allergy</option>
																<option value="7" <?php if($reason==7) { echo "selected"; } ?>>Infection</option>
																<option value="8" <?php if($reason==8) { echo "selected"; } ?>>Remission</option>
																<option value="9" <?php if($reason==9) { echo "selected"; } ?>>Solid tumor/Lymphoma</option>
																<option value="10" <?php if($reason==10) { echo "selected"; } ?>>Other adverse event</option>
																<option value="11" <?php if($reason==11) { echo "selected"; } ?>>Patient decision</option>
																<option value="12" <?php if($reason==12) { echo "selected"; } ?>>Planned therapy</option>
																<option value="13" <?php if($reason==13) { echo "selected"; } ?>>Pregnancy</option>
																<option value="14" <?php if($reason==14) { echo "selected"; } ?>>Financial</option>
																<option value="15" <?php if($reason==15) { echo "selected"; } ?>>Change to BIOsimilar</option>
																<option value="16" <?php if($reason==16) { echo "selected"; } ?>>Change practical reasons</option>
																<option value="17" <?php if($reason==17) { echo "selected"; } ?>>Change to bio-originator</option>
																<option value="18" <?php if($reason==18) { echo "selected"; } ?>>Other</option>
																<option value="19" <?php if($reason==19) { echo "selected"; } ?>>Unknown</option>
															</select>
														</td>
														<td align="center">
															<select class="form-control" name="frequency_<?php echo $i; ?>" id="frequency_<?php echo $i; ?>">
																<option value="0" <?php if($frequency==0) { echo "selected"; } ?>>--</option>
																<?php
																	$sql = get_drugs_frequencies();
																	$numrows = pg_num_rows($sql);
																	while($result3 = pg_fetch_array($sql))
																	{
																		$frequency_id=$result3['drugs_frequency_id'];
																		$value = $result3['value'];
																		
																		
																		
																?>
																<option value="<?php echo $frequency_id; ?>" <?php if($frequency==$frequency_id) { echo "selected"; } ?>><?php echo $value; ?></option>
																<?php
																	}
																?>
															</select>
														</td>
														<td align="center">
															<input type="text" class="form-control" id="dose_<?php echo $i; ?>" name="dose_<?php echo $i; ?>" value="<?php echo $dose	; ?>">
														</td>
														<td align="center">
															<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="startdate_<?php echo $i; ?>"  name="startdate_<?php echo $i; ?>" value="<?php echo $startdate_str; ?>">
														</td>
														<td align="center">
															<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="stopdate_<?php echo $i; ?>"  name="stopdate_<?php echo $i; ?>" value="<?php echo $stopdate_str; ?>">
														</td>
														<td align="center">
															<a id="a2_<?php echo $i; ?>" name="a2_<?php echo $i; ?>" title="Delete" href="javascript:void(0);" onclick="patient_cohort_drugs_period_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
														</td>
														 </tr>
											<?php
														}
													}
												
											?>
											</tbody>
										</table>
										<input type="hidden" name="patient_cohort_drugs_numofrows" id="patient_cohort_drugs_numofrows" value="<?php echo $i; ?>">
										<br>
										 <a class="btn btn-primary" href="javascript:void(0);" onclick="patient_cohort_drugs_addrow();"><i class="fa fa-plus"></i> New record</a>
									 </div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- treatments end div -->
				<!-- Corticosteroids start div -->
				<div class="row">
					<div class="col-md-12">
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Corticosteroids</small></h3>
							</div>
							<div class="box-body">
								<div class="row">
									<div class="form-group col-md-12" id="divcorticosteroids" name="divcorticosteroids">
									 <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="patient_corticosteroids_tbl" name="patient_corticosteroids_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>Followup date</b></th>
													<th class="col-md-6"><b>Corticosteroids</b></th>
													<th class="col-md-4"><b>Date</b></th>
												</tr>
											 </thead>
											 <tbody>
											<?php
												$i=0;
												$j=0;
													$order = get_all_patient_corticosteroids($patient_cohort_id);
													$num_rows = pg_num_rows($order);
													while($result = pg_fetch_assoc($order))
													{
														$i++;
														$j++;
														$patient_corticosteroids_id=$result['patient_corticosteroids_id'];
														$corticosteroids_id=$result['corticosteroids_id'];
														$date_str=$result['date_str'];
														if ($date_str=='12-12-1900')
															$date_str='';
														$patient_followup_date_str=$result['patient_followup_date_str'];
														if ($patient_followup_date_str=='12-12-1900')
															$patient_followup_date_str='';
											?>
												  <tr id="patient_corticosteroids_tr_<?php echo $i;?>">
														<td align="center">
															<?php echo "<label>$patient_followup_date_str</label>"; ?>
														</td>
														<td align="center">
															<?php
																	$sql = get_corticosteroids();
																	$numrows = pg_num_rows($sql);
																	while($result2 = pg_fetch_array($sql))
																	{
																		$corticosteroids_id2=$result2['corticosteroids_id'];
																		$value = $result2['code'];
																		$value .= "-";
																		$value .= $result2['substance'];
																		
																		if($corticosteroids_id==$corticosteroids_id2)
																			echo "<label>$value</label>"; 
																	}
																?>
														</td>
														 <td align="center">
															<?php echo "<label>$date_str</label>"; ?>
														</td>
													</tr>
											<?php
													}
											?>
											</tbody>
										</table>
									 </div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- Corticosteroids end div -->
				
				<!-- NSAIDs_Analgesics start div -->
				<div class="row">
					<div class="col-md-12">
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">NSAIDs/Analgesics</small></h3>
							</div>
							<div class="box-body">
								<div class="row">
									<div class="form-group col-md-12" id="divAnalgesics" name="divAnalgesics">
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="patient_analgesics_tbl" name="patient_analgesics_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>Followup date</b></th>
													<th class="col-md-3"><b>Analgesics</b></th>
													<th class="col-md-3"><b>Days with intake</b></th>
													<th class="col-md-2"><b>Starting Date</b></th>
													<th class="col-md-2"><b>Ending Date</b></th>
												</tr>
											 </thead>
											 <tbody>
											<?php
												$i=0;
												$j=0;
													$order = get_all_patient_analgesics($patient_cohort_id);
													$num_rows = pg_num_rows($order);
													while($result = pg_fetch_assoc($order))
													{
														$i++;
														$j++;
														$patient_analgesics_id=$result['patient_analgesics_id'];
														$analgesics_id=$result['analgesics_id'];
														$intake=$result['intake'];
														$startdate_str=$result['startdate_str'];
														if ($startdate_str=='12-12-1900')
															$startdate_str='';
														$stopdate_str=$result['stopdate_str'];
														if ($stopdate_str=='12-12-1900')
															$stopdate_str='';
														$patient_followup_date_str=$result['patient_followup_date_str'];
														if ($patient_followup_date_str=='12-12-1900')
															$patient_followup_date_str='';
														
											?>
												  <tr id="patient_analgesics_tr_<?php echo $i;?>">
														 <td align="center">
															<?php echo "<label>$patient_followup_date_str</label>"; ?>
														</td>
														<td align="center">
																<?php
																	$sql = get_analgesics();
																	$numrows = pg_num_rows($sql);
																	while($result2 = pg_fetch_array($sql))
																	{
																		$analgesics_id2=$result2['analgesics_id'];
																		$value = $result2['code'];
																		$value .= "-";
																		$value .= $result2['substance'];
																		if($analgesics_id==$analgesics_id2)
																			echo $value;
																	}
																?>
															</select>
														</td>
														<td align="center">
																<?php if($intake==1) { echo "<label>None (N=0)"; } ?>
																<?php if($intake==2) { echo "<label>< 1 day/week (N=0.5/7)</label>"; } ?>
																<?php if($intake==3) { echo "<label>1-3 days/week (N=2/7)</label>"; } ?>
																<?php if($intake==4) { echo "<label>3-5 days/week (N=4/7)</label>"; } ?>
																<?php if($intake==5) { echo "<label>≥ 5 days/week  (N=6/7)</label>"; } ?>
																<?php if($intake==6) { echo "<label>Everyday (N= 7/7)</label>"; } ?>
														</td>
														 <td align="center">
															<?php echo "<label>$startdate_str</label>"; ?>
														</td>
														<td align="center">
															<?php echo "<label>$stopdate_str</label>"; ?>
														</td>
													</tr>
											<?php
													}
											?>
											</tbody>
										</table>
									 </div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- NSAIDs_Analgesics end div -->
				<!-- severity div start -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Imaging</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-4" id="divrheumatoid_arthritis" name="divrheumatoid_arthritis">
								<label>Rheumatoid arthritis & Psoriatic arthritis</label>
									<select class="form-control" name="rheumatoid_arthritis" id="rheumatoid_arthritis" >
										<option value="0" <?php if($rheumatoid_arthritis==0) { echo "selected"; } ?>>Unknown</option>
										<option value="1" <?php if($rheumatoid_arthritis==1) { echo "selected"; } ?>>Yes</option>
										<option value="2" <?php if($rheumatoid_arthritis==2) { echo "selected"; } ?>>No</option>
									  </select>
							</div>
							<div  class="form-group col-md-4" id="divrheumatoid_arthritis_date" name="divrheumatoid_arthritis_date" <?php if ($rheumatoid_arthritis==1) { ?>style="DISPLAY: block;"<?php } else { ?>style="DISPLAY: none;"<?php } ?>>
								<label>date</label>
								<div class="input-group date">
								  <div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								  </div>
								  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="rheumatoid_arthritis_date"  name="rheumatoid_arthritis_date" value="<?php echo $rheumatoid_arthritis_date; ?>">
								</div>
							  </div>
						</div>
						<div class="row" id="divrheumatoid_arthritis_details" name="divrheumatoid_arthritis_details" <?php if($rheumatoid_arthritis=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							<div class="form-group col-md-12">
								<div class="row">
									<div class="form-group col-md-3">
										<label>X-Rays (hand/feet)</label>
									</div>
								</div>
								<div class="row">
									<div class="form-group col-md-3">
										<label>Erosions/narrowings</label>
										  <select class="form-control" name="erosions" id="erosions">
											<option value="0" <?php if($erosions==0) { echo "selected"; } ?>>Unknown</option>
											<option value="1" <?php if($erosions==1) { echo "selected"; } ?>>Yes</option>
											<option value="2" <?php if($erosions==2) { echo "selected"; } ?>>No</option>
										  </select>
									</div>
									<div class="form-group col-md-3">
										<label>Scharp van der Heijde index</label>
										  <select class="form-control" name="scharp" id="scharp">
											<?php
											for($i=0;$i<=448;$i++)
											{
											?>
											<option value="<?php echo $i; ?>" <?php if($scharp==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
											<?php
											}
											?>
										  </select>
									</div>
									<div class="form-group col-md-3">
										<label>SΕΝS index</label>
										  <select class="form-control" name="sens" id="sens">
											<?php
											for($i=0;$i<=86;$i++)
											{
											?>
											<option value="<?php echo $i; ?>" <?php if($sens==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
											<?php
											}
											?>
										  </select>
									</div>
									<div class="form-group col-md-3">
										<label>Juxtaarticular new bone formation (PsA)</label>
										  <select class="form-control" name="psa" id="psa">
											<option value="0" <?php if($psa==0) { echo "selected"; } ?>>Unknown</option>
											<option value="1" <?php if($psa==1) { echo "selected"; } ?>>Yes</option>
											<option value="2" <?php if($psa==2) { echo "selected"; } ?>>No</option>
										  </select>
									</div>
								</div>
							</div>	
						</div>
						<div class="row">
							<div class="form-group col-md-4" id="divaxial" name="divaxial">
								<label>Axial spondyloarthritis</label>
									<select class="form-control" name="axial" id="axial">
									<option value="0" <?php if($axial==0) { echo "selected"; } ?>>Unknown</option>
									<option value="1" <?php if($axial==1) { echo "selected"; } ?>>Yes</option>
									<option value="2" <?php if($axial==2) { echo "selected"; } ?>>No</option>
								  </select>
							</div>
							<!--<div  class="form-group col-md-4" id="divaxial_date" name="divaxial_date" <?php if ($axial==1) { ?>style="DISPLAY: block;"<?php } else { ?>style="DISPLAY: none;"<?php } ?>>
								<label>date</label>
								<div class="input-group date">
								  <div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								  </div>
								  <input type="text" class="form-control" id="axial_date" name="axial_date" value="<?php echo $axial_date; ?>" readonly onclick="javascript:newCalendar(this);">
								</div>
							  </div>-->
						</div>
						<div class="row" id="divaxial_details" name="divaxial_details" <?php if($axial=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							<div class="form-group col-md-12">
								<div class="row">
									<div class="form-group col-md-3">
										<label>X-Rays of sacroileal joints with pathologic findings</label>
										  <select class="form-control" name="xrays_sacroileal" id="xrays_sacroileal">
											<option value="0" <?php if($xrays_sacroileal==0) { echo "selected"; } ?>>Unknown</option>
											<option value="1" <?php if($xrays_sacroileal==1) { echo "selected"; } ?>>Yes</option>
											<option value="2" <?php if($xrays_sacroileal==2) { echo "selected"; } ?>>No</option>
										  </select>
									</div>
									<div class="form-group col-md-3">
										<label>MRI of sacroileal joints with pathologic findings</label>
										  <select class="form-control" name="mri_sacroileal" id="mri_sacroileal">
											<option value="0" <?php if($mri_sacroileal==0) { echo "selected"; } ?>>Unknown</option>
											<option value="1" <?php if($mri_sacroileal==1) { echo "selected"; } ?>>Yes</option>
											<option value="2" <?php if($mri_sacroileal==2) { echo "selected"; } ?>>No</option>
										  </select>
									</div>
									<div class="form-group col-md-3">
										<label>X-Rays of vertebral column with pathologic findings</label>
										  <select class="form-control" name="xrays_vertebral" id="xrays_vertebral">
											<option value="0" <?php if($xrays_vertebral==0) { echo "selected"; } ?>>Unknown</option>
											<option value="1" <?php if($xrays_vertebral==1) { echo "selected"; } ?>>Yes</option>
											<option value="2" <?php if($xrays_vertebral==2) { echo "selected"; } ?>>No</option>
										  </select>
									</div>
									<div class="form-group col-md-3">
										<label>MRI of vertebral column with pathologic findings</label>
										  <select class="form-control" name="mri_vertebral" id="mri_vertebral">
											<option value="0" <?php if($mri_vertebral==0) { echo "selected"; } ?>>Unknown</option>
											<option value="1" <?php if($mri_vertebral==1) { echo "selected"; } ?>>Yes</option>
											<option value="2" <?php if($mri_vertebral==2) { echo "selected"; } ?>>No</option>
										  </select>
									</div>
								</div>
							</div>	
						</div>
					</div>
				</div>
				<!-- severity div end -->
				<div class="row">
					<div class="form-group col-md-12">
						<input type="submit" class="btn btn-primary" value="Save">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						&nbsp;
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						&nbsp;
					</div>
				</div>
			</div>
		</div>
		</form>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
   <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>
function patient_cohort_previous_drugs_addrow()
{
	 var tbl = document.getElementById('patient_cohort_previous_drugs_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
  row.id= 'patient_previous_drugs_tr_'+iteration;
  document.getElementById('patient_previous_drugs_numofrows').value = iteration;
	
	var patient_previous_drugs_ids=document.getElementById('patient_previous_drugs_ids').value;
	var patient_previous_drugs_values=document.getElementById('patient_previous_drugs_values').value;
	var patient_previous_drugs_counter=document.getElementById('patient_previous_drugs_counter').value;
	var patient_previous_drugs_value=patient_previous_drugs_values.split('!@#$%^');
	var patient_previous_drugs_id=patient_previous_drugs_ids.split('!@#$%^');
	
	
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('select');
  el1.name = 'patient_previous_drugs_id_' + iteration;
  el1.id = 'patient_previous_drugs_id_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
  
 var opt=new Option("--",0);
 el1.add(opt,undefined);
 for(i = 0; i < patient_previous_drugs_counter; i++)
 {
 	var opt=new Option(patient_previous_drugs_value[i],patient_previous_drugs_id[i]);
 	el1.add(opt,undefined);
 }
  
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'patient_previous_drugs_hidden_' + iteration;
  el2.id = 'patient_previous_drugs_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'patient_cohort_previous_drugs_id_' + iteration;
  el3.id = 'patient_cohort_previous_drugs_id_' + iteration;
  el3.value='';
  
  
 
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
	var r2 = row.insertCell(1);
	r2.align='center';

	var el4 = document.createElement('a');
	el4.name = 'a_' + iteration;
	el4.id = 'a_' + iteration;
	el4.title = 'Delete';
	el4.href="javascript:avoid(0)";
	var icon = document.createElement('i');
	icon.className='fa fa-trash-o';
	el4.appendChild(icon);
	el4.onclick =function(){patient_previous_drugs_removerow(iteration);};
	
	r2.appendChild(el4);
	
}

function patient_previous_drugs_removerow(row)
{
	document.getElementById('patient_previous_drugs_hidden_'+row).value='-1';
	document.getElementById('patient_previous_drugs_tr_'+row).style.display='none';
}

$('[id^=dose_]').focusout(function() 
{
	/*if (($(this).val()).charAt(0)=="0")
		$(this).val(($(this).val()).substr(1));
	
	if(!isNaN($(this).val()))
	{
		if ($(this).val()=="")
			$(this).val("0");
	}
	else
	{
		$(this).val(($(this).val()).slice(0,-1));
		if ($(this).val()=="")
			$(this).val("0");
	}*/
	
	if($(this).val()=="")
		$(this).val("0");
	else
		$(this).val(parseFloat($(this).val()));
});

jQuery(document).ready(function() {

for(i=1;i<=document.getElementById('patient_cohort_drugs_numofrows').value;i++)
{
	if(document.getElementById('patient_cohort_drugs_type_'+i).value=="cohort" || document.getElementById('patient_cohort_drugs_type_'+i).value=="period")
	{
		$('#startdate_'+i).datepicker({
		   dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
		})
		$('#startdate_'+i).inputmask();
		
		$('#stopdate_'+i).datepicker({
		  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
		})
		$('#stopdate_'+i).inputmask();
	}
}

$("#marital_status_year").inputmask();
$('#start_date').datepicker({
		  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) { $("#age_at_cohort").val(""); },
		onSelect: function(dateText) {
				var start_val = $("#dateofbirth_str").val();
				var end_val = $("#start_date").val();
				if(end_val!="" && start_val!="")
				{
					var startyear=(start_val.split('-'))[2];
					var endyear=(end_val.split('-'))[2];
					$("#age_at_cohort").val(endyear - startyear);
				}
				else
					$("#age_at_cohort").val("");
		  }
  })

  $('#stop_date').datepicker({
		 dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  
  $('#rheumatoid_arthritis_date').datepicker({
		 dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  
  
 $('#stop_date').inputmask();
 
 });

 $('#date_phys_str').datepicker({
		 dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
 $('#date_phys_str').inputmask();
 
 
 
$("#axial").change(function() 
	{
		var axial_val = $(this).val();
		if(axial_val==1)
		{
			$("#divaxial_details").show();
			//$("#divaxial_date").show();
		}
		else
		{
			 $("#divaxial_details").hide();
			 //$("#divaxial_date").hide();
		}
	});
	
$("#rheumatoid_arthritis").change(function() 
	{
		var rheumatoid_arthritis_val = $(this).val();
		if(rheumatoid_arthritis_val==1)
		{
			$("#divrheumatoid_arthritis_details").show();
			$("#divrheumatoid_arthritis_date").show();
		}
		else
		{
			 $("#divrheumatoid_arthritis_details").hide();
			 $("#divrheumatoid_arthritis_date").hide();
		}
		
	});
	
function setminmax(row)
{
	var drugs_ids=document.getElementById('drugs_ids').value;
	var drugs_id=drugs_ids.split('!@#$%^');
	var drugs_maxs=document.getElementById('drugs_maxs').value;
	var drugs_max=drugs_maxs.split('!@#$%^');
	var drugs_mins=document.getElementById('drugs_mins').value;
	var drugs_min=drugs_mins.split('!@#$%^');
	var drugs_counter=document.getElementById('drugs_counter').value;
	
	var drugs_id_selected=$("#drugs_id_"+row).val();
	
	for(i = 0; i < drugs_counter; i++)
	{
		if(drugs_id_selected==drugs_id[i])
		{
			$("#patient_cohort_drugs_min_"+row).val(drugs_min[i]);
			$("#patient_cohort_drugs_max_"+row).val(drugs_max[i]);
		}
	}
}

function calc(row)
{
	if (document.getElementById('main_'+row).checked===true) 
	{
		$("input[name^='main_']").prop('checked', false);
		$("a[name^='a_']").show();
		$("#main_"+row).prop('checked', true);
		$("#a_"+row).hide();
	  
	}
	else
	{
		$("input[name^='main_']").prop('checked', false);
		$("a[name^='a_']").show();
		$("#main_"+row).prop('checked', false);
		$("#a_"+row).show();
	}  
}

$(function () {
	  
	  $('#form').submit(function(){
		
		var msg="";
		var cohort_name=$('#cohort_name').val();
		var treatment_nbr=$('#treatment_nbr').val();
		var start_date2=$('#start_date').val();
		
		
		var start_date_unformat=$('#start_date').val();
		var start_date_split = start_date_unformat.split("-");
		var start_date= new Date(start_date_split[2]+"-"+start_date_split[1]+"-"+start_date_split[0]);
		
		var stop_date_unformat=$('#stop_date').val();
		var stop_date_split = stop_date_unformat.split("-");
		var stop_date= new Date(stop_date_split[2]+"-"+stop_date_split[1]+"-"+stop_date_split[0]);
		
		var dateofinclusion_unformat=document.getElementById('dateofinclusion').value;
		var dateofinclusion_split = dateofinclusion_unformat.split("-");
		var dateofinclusion= new Date(dateofinclusion_split[2]+"-"+dateofinclusion_split[1]+"-"+dateofinclusion_split[0]);
		
		var patient_cohort_id=$('#patient_cohort_id').val();
		
		
		
		var stop_reason=$('#stop_reason').val();
		
		var tbl = document.getElementById('patient_drugs_tbl');
		var lastRow = tbl.rows.length;
		
		var k=0;
		var dose_min=0;
		var dose_max=0;
		var min=0;
		var max=0;
		var checkstopdate=0;
		
		var sum_main=0;
		var sum_main_drugs=0;
		
		for (i=1;i<=lastRow-1;i++)
		{
			if ($('#patient_cohort_drugs_type_'+i).val()=='cohort')
			{
				sum_main_drugs++;
				if($('#main_'+i).prop('checked'))
					sum_main++;
			}
			
			if ($('#patient_cohort_drugs_type_'+i).val()=='cohort')
			{
				var cohort_startdate_unformat=document.getElementById('startdate_'+i).value;
				var cohort_startdate_split = cohort_startdate_unformat.split("-");
				var cohort_startdate= new Date(cohort_startdate_split[2]+"-"+cohort_startdate_split[1]+"-"+cohort_startdate_split[0]);
				
				var cohort_stopdate_unformat=document.getElementById('stopdate_'+i).value;
				var cohort_stopdate_split = cohort_stopdate_unformat.split("-");
				var cohort_stopdate= new Date(cohort_stopdate_split[2]+"-"+cohort_stopdate_split[1]+"-"+cohort_stopdate_split[0]);
				
				min=$("#patient_cohort_drugs_min_"+i).val();
				max=$("#patient_cohort_drugs_max_"+i).val();
				
				if (cohort_stopdate_unformat=="")
				{
					document.getElementById('stopdate_'+i).style.backgroundColor = "red";
					document.getElementById('stopdate_'+i).style.color  = "white";
					checkstopdate++;
				}
				else
				{
					document.getElementById('stopdate_'+i).style.backgroundColor = "white";
					document.getElementById('stopdate_'+i).style.color  = "black";
				}
				
				/*if(cohort_startdate<start_date)
				{
					document.getElementById('startdate_'+i).style.backgroundColor = "red";
					document.getElementById('startdate_'+i).style.color  = "white";
					k++;
				}
				else
				{
					document.getElementById('startdate_'+i).style.backgroundColor = "white";
					document.getElementById('startdate_'+i).style.color  = "black";
				}*/
			
			}
			
			if ($('#patient_cohort_drugs_type_'+i).val()=='period')
			{
				if(parseInt($('#dose_'+i).val())>max)
				{
					
					dose_max++;
					document.getElementById('dose_'+i).style.backgroundColor = "red";
					document.getElementById('dose_'+i).style.color  = "white";
				}
				else if(parseInt($('#dose_'+i).val())<min)
				{
					dose_min++;
					document.getElementById('dose_'+i).style.backgroundColor = "red";
					document.getElementById('dose_'+i).style.color  = "white";
				}
				else
				{
					document.getElementById('dose_'+i).style.backgroundColor = "white";
					document.getElementById('dose_'+i).style.color  = "black";
				}
			}
			
		}
		
		if(patient_cohort_id=="")
		{
			if(cohort_name=="0")
			{
				document.getElementById('cohort_name').style.backgroundColor = "red";
				document.getElementById('cohort_name').style.color  = "white";
				msg +="-Fill cohort Treatment!\n";
			}
			else
			{
				document.getElementById('cohort_name').style.backgroundColor = "white";
				document.getElementById('cohort_name').style.color  = "black";
			}
		}
		
		if(treatment_nbr=="0")
		{
			document.getElementById('treatment_nbr').style.backgroundColor = "red";
			document.getElementById('treatment_nbr').style.color  = "white";
			msg +="-Fill cohort Treatment Number!\n";
		}
		else
		{
			document.getElementById('treatment_nbr').style.backgroundColor = "white";
			document.getElementById('treatment_nbr').style.color  = "black";
		}
		
		
		if(start_date2=="")
		{
			document.getElementById('start_date').style.backgroundColor = "red";
			document.getElementById('start_date').style.color  = "white";
			msg +="-Fill cohort start date!\n";
		}
		else if(dateofinclusion>start_date)
		{
			document.getElementById('start_date').style.backgroundColor = "red";
			document.getElementById('start_date').style.color  = "white";
			msg +="-Cohort start date must be greater than inclusion date (" + dateofinclusion_unformat + ")!\n";
		}
		else if(Date.now()<start_date)
		{
			document.getElementById('start_date').style.backgroundColor = "red";
			document.getElementById('start_date').style.color  = "white";
			msg +="-Cohort start date should not be future!\n";
		}
		else
		{
			document.getElementById('start_date').style.backgroundColor = "white";
			document.getElementById('start_date').style.color  = "black";
		}
		
		
		if(dose_max>0)
			msg +="-There is a dose greater than maximum dose!\n";
		if(dose_min>0)
			msg +="-There is a dose smaller than minimum dose!\n";
		
		if (stop_date_unformat!="" && stop_reason==0)
		{
			document.getElementById('stop_reason').style.backgroundColor = "red";
			document.getElementById('stop_reason').style.color  = "white";
			msg +="-Cohort can't stop because stop reason is unfilled!\n";
		}
		else
		{
			document.getElementById('stop_reason').style.backgroundColor = "white";
			document.getElementById('stop_reason').style.color  = "black";
		}
		
		if(stop_date_unformat!="" && checkstopdate>0)
			msg +="-Cohort can't stop because there is/are "+checkstopdate+" open treatment/s!\n"; 
		
		
		
		
		if(sum_main==0 && sum_main_drugs>0)
			msg +='-You must have at least one main Treatment drug!\n';
		
		var start_num=0;
		var stop_num=0;
		
		var cohortsnum=document.getElementById('cohortsnum').value;
		if(cohortsnum > 0)
		{
			var startdates=document.getElementById('startdates').value;
			var stopdates=document.getElementById('stopdates').value;
			var stopdate=stopdates.split('!@#$');
			var startdate=startdates.split('!@#$');
			
			for (i=0;i<cohortsnum;i++)
			{
				var start_date_prv_unformat=startdate[i];
				var start_date_prv_split = start_date_prv_unformat.split("-");
				var start_date_prv= new Date(start_date_prv_split[2]+"-"+start_date_prv_split[1]+"-"+start_date_prv_split[0]);
				
				var stop_date_prv_unformat=stopdate[i];
				var stop_date_prv_split = stop_date_prv_unformat.split("-");
				var stop_prv_date= new Date(stop_date_prv_split[2]+"-"+stop_date_prv_split[1]+"-"+stop_date_prv_split[0]);
				
				if(start_date_prv<=start_date &&  start_date<=stop_prv_date && startdate[i] != '12-12-1900')
					start_num++;	
				
				if( start_date_prv<=stop_date && stop_date<=stop_prv_date && stopdate[i] != '12-12-1900' && stop_date_unformat != '' && stop_date_unformat != '12-12-1900')
					stop_num++;
				
			}
		}
		
		if(start_date>=stop_date)
		{
			document.getElementById('start_date').style.backgroundColor = "red";
			document.getElementById('start_date').style.color  = "white";
			document.getElementById('stop_date').style.backgroundColor = "red";
			document.getElementById('stop_date').style.color  = "white";
			msg +='-Cohort start date must be earlier than stop date cohort dates!\n';
		}
		
		if(start_num>0)
		{
			document.getElementById('start_date').style.backgroundColor = "red";
			document.getElementById('start_date').style.color  = "white";
			msg +='-Cohort start date overlap with other cohort dates!\n';
		}
		else if (start_date<stop_date && start_date2!="")
		{
			document.getElementById('start_date').style.backgroundColor = "white";
			document.getElementById('start_date').style.color  = "black";
		}
			
		
		if(stop_num>0)
		{
			document.getElementById('stop_date').style.backgroundColor = "red";
			document.getElementById('stop_date').style.color  = "white";
			msg +='-Cohort stop date overlap with other cohort dates!\n';
		}
		else if (start_date<stop_date)
		{
			document.getElementById('stop_date').style.backgroundColor = "white";
			document.getElementById('stop_date').style.color  = "black";
		}
			
		
		if(msg != '')
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
		
	  });	

function patient_cohort_drugs_addrow()
{
  var tbl = document.getElementById('patient_drugs_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'patient_cohort_drugs_tr_'+iteration;
   $('#patient_cohort_drugs_tr_'+iteration).css('background', '#f0f8ff');
  document.getElementById('patient_cohort_drugs_numofrows').value = iteration;
	
	var drugs_ids=document.getElementById('drugs_ids').value;
	var drugs_values=document.getElementById('drugs_values').value;
	var drugs_counter=document.getElementById('drugs_counter').value;
	var drugs_value=drugs_values.split('!@#$%^');
	var drugs_id=drugs_ids.split('!@#$%^');
	
	/*var route_ids=document.getElementById('route_ids').value;
	var route_values=document.getElementById('route_values').value;
	var route_counter=document.getElementById('route_counter').value;
	var route_value=route_values.split('!@#$%^');
	var route_id=route_ids.split('!@#$%^');*/
	
  var r1 = row.insertCell(0);
  r1.align='center';
  r1.colSpan="3";
  
  var el1 = document.createElement('select');
  el1.name = 'drugs_id_' + iteration;
  el1.id = 'drugs_id_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
  el1.onchange =function(){setminmax(iteration)};
  
	var opt=new Option("--",0);
	el1.add(opt,undefined);
	for(i = 0; i < drugs_counter; i++)
	{
		var opt=new Option(drugs_value[i],drugs_id[i]);
		el1.add(opt,undefined);
	}
  
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'patient_cohort_drugs_hidden_' + iteration;
  el2.id = 'patient_cohort_drugs_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'patient_cohort_drugs_id_' + iteration;
  el3.id = 'patient_cohort_drugs_id_' + iteration;
  el3.value='';
  
  var el33 = document.createElement('input');
  el33.type = 'hidden';
  el33.name = 'patient_cohort_drugs_type_' + iteration;
  el33.id = 'patient_cohort_drugs_type_' + iteration;
  el33.value='cohort';
  
  var el34 = document.createElement('input');
  el34.type = 'hidden';
  el34.name = 'patient_cohort_drugs_row_' + iteration;
  el34.id = 'patient_cohort_drugs_row_' + iteration;
  el34.value=iteration;
  
  var el35 = document.createElement('input');
  el35.type = 'hidden';
  el35.name = 'patient_cohort_drugs_min_' + iteration;
  el35.id = 'patient_cohort_drugs_min_' + iteration;
  el35.value=0;
  
  var el36 = document.createElement('input');
  el36.type = 'hidden';
  el36.name = 'patient_cohort_drugs_max_' + iteration;
  el36.id = 'patient_cohort_drugs_max_' + iteration;
  el36.value=0;
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  r1.appendChild(el33);
  r1.appendChild(el34);
  r1.appendChild(el35);
  r1.appendChild(el36);
  
  var r11 = row.insertCell(1);
  r11.align='center';
  
  var el333 = document.createElement('input');
	el333.type = 'checkbox';
	el333.name = 'main_' + iteration;
	el333.id = 'main_' + iteration;
	el333.onclick =function(){calc(iteration)};
	r11.appendChild(el333);

	/*var r12 = row.insertCell(2);
	r12.align='center';
  
  var el11 = document.createElement('select');
  el11.name = 'route_of_administration_' + iteration;
  el11.id = 'route_of_administration_' + iteration;
  el11.style.width='100%';
  el11.className='form-control';
  
	var opt=new Option("--",0);
	el11.add(opt,undefined);
	for(i = 0; i < route_counter; i++)
	{
		var opt=new Option(route_value[i],route_id[i]);
		el11.add(opt,undefined);
	}
	
	
	  
	r12.appendChild(el11);
  */
	var r4 = row.insertCell(2);
	r4.align='center';
  
		var el6 = document.createElement('input');
	  el6.type = 'text';
	  el6.name = 'startdate_' + iteration;
	  el6.id = 'startdate_' + iteration;
	  el6.style.width='100%';
	  el6.className='form-control';
	
	r4.appendChild(el6);
  
		var r5 = row.insertCell(3);
		r5.align='center';
  
		var el7 = document.createElement('input');
	  el7.type = 'text';
	  el7.name = 'stopdate_' + iteration;
	  el7.id = 'stopdate_' + iteration;
	  el7.style.width='100%';
	  el7.className='form-control';
  
	r5.appendChild(el7);
  
		$('#startdate_'+iteration).datepicker({
		  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  $('#startdate_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
		
		$('#stopdate_'+iteration).datepicker({
		  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  $('#stopdate_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
		
	var r6 = row.insertCell(4);
	r6.align='center';
	
	/*var el9 = document.createElement('a');
	el9.name = 'a_add_' + iteration;
	el9.id = 'a_add_' + iteration;
	el9.title = 'add period';
	el9.href="javascript:avoid(0)";
	var icon = document.createElement('i');
	icon.className='fa fa-calendar-plus-o';
	el9.appendChild(icon);
	el9.onclick =function(){patient_cohort_drugs_newperiod(iteration,'')};*/
	
	var el8 = document.createElement('a');
	el8.name = 'a_' + iteration;
	el8.id = 'a_' + iteration;
	el8.title = 'Delete';
	el8.href="javascript:avoid(0)";
	var icon = document.createElement('i');
	icon.className='fa fa-trash-o';
	el8.appendChild(icon);
	el8.onclick =function(){if (confirm("Are you sure;All periods will be deleted!")) { patient_cohort_drugs_removerow(iteration); }};
	
	/*var span = document.createElement("span");
	span.innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;";*/
	
	/*r6.appendChild(el9);
	r6.appendChild(span);*/
	r6.appendChild(el8);
	
	var iteration2 = lastRow+1;
	var row = tbl.insertRow(lastRow+1);
	row.id= 'patient_cohort_drugs_tr_'+iteration2;
	document.getElementById('patient_cohort_drugs_numofrows').value = iteration2;
	
	var r111 = row.insertCell(0);
	r111.align='center';
	var span1 = document.createElement("span");
	span1.innerHTML = "Periods";
	r111.appendChild(span1);
	
	var r11 = row.insertCell(1);
	r11.align='center';
	
	
	var el11 = document.createElement('input');
	el11.type = 'hidden';
	el11.name = 'patient_cohort_drugs_hidden_' + iteration2;
	el11.id = 'patient_cohort_drugs_hidden_' + iteration2;
	el11.value='1';
	
	var el12 = document.createElement('input');
	el12.type = 'hidden';
	el12.name = 'patient_cohort_drugs_type_' + iteration2;
	el12.id = 'patient_cohort_drugs_type_' + iteration2;
	el12.value='period_label';
  
	var el13 = document.createElement('input');
	el13.type = 'hidden';
	el13.name = 'patient_cohort_drugs_row_' + iteration2;
	el13.id = 'patient_cohort_drugs_row_' + iteration2;
	el13.value=iteration;
	
	var el333 = document.createElement('input');
	el333.type = 'hidden';
	el333.name = 'patient_cohort_drugs_id_' + iteration2;
	el333.id = 'patient_cohort_drugs_id_' + iteration2;
	el333.value='';
  
	var span = document.createElement("span");
	span.innerHTML = "Reason for </br> treatment change";
	
	r11.appendChild(el11);
	r11.appendChild(el12);
	r11.appendChild(el13);
	r11.appendChild(el333);
	r11.appendChild(span);
	
	var r12 = row.insertCell(2);
	r12.align='center';
	
	var span2 = document.createElement("span");
	span2.innerHTML = "frequency";
	
	r12.appendChild(span2);
	
	var r13 = row.insertCell(3);
	r13.align='center';
	
	var span3 = document.createElement("span");
	span3.innerHTML = "dose";
	
	r13.appendChild(span3);
	
	var r14 = row.insertCell(4);
	r14.align='center';
	
	var span4 = document.createElement("span");
	span4.innerHTML = "start date";
	
	r14.appendChild(span4);
	
	var r15 = row.insertCell(5);
	r15.align='center';
	
	var span5 = document.createElement("span");
	span5.innerHTML = "stop date";
	
	r15.appendChild(span5);
	
	var r16 = row.insertCell(6);
	r16.align='center';
	
	var el9 = document.createElement('a');
	el9.name = 'a2_add_' + iteration2;
	el9.id = 'a2_add_' + iteration2;
	el9.title = 'add period';
	el9.href="javascript:avoid(0)";
	var icon = document.createElement('i');
	icon.className='fa fa-calendar-plus-o';
	el9.appendChild(icon);
	el9.onclick =function(){patient_cohort_drugs_newperiod(iteration2,'')};
	
	r16.appendChild(el9);
}

function patient_cohort_drugs_newperiod(w_row,patient_cohort_drugs_id)
{
  var tbl = document.getElementById('patient_drugs_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var newrow2=parseInt(w_row)+1;
  document.getElementById('patient_cohort_drugs_numofrows').value = iteration;
  /*alert("iteration:"+iteration);
  alert("lastRow:"+lastRow);
  alert("newrow2:"+newrow2);
  alert("w_row:"+w_row);*/
   var cohort_new_row=0;
  
  for (i=lastRow-1;i>=newrow2;i--)
  {
	  var newrow3=parseInt(i)+1;
	 
		if (document.getElementById('patient_cohort_drugs_type_'+i).value=='cohort')
		{
			cohort_new_row=newrow3;
			var el51=document.getElementById('patient_cohort_drugs_tr_'+i);
			el51.id= 'patient_cohort_drugs_tr_'+newrow3;
			el51.name= 'patient_cohort_drugs_tr_'+newrow3;
			
			var el52=document.getElementById('drugs_id_'+i);
			el52.id = 'drugs_id_' + newrow3;
			el52.name = 'drugs_id_' + newrow3;
			el52.onchange =function(){setminmax(this.id.substr(9))};
			
			var el53=document.getElementById('patient_cohort_drugs_hidden_'+i);
			el53.id = 'patient_cohort_drugs_hidden_' + newrow3;
			el53.name = 'patient_cohort_drugs_hidden_' + newrow3;
			
			var el54=document.getElementById('patient_cohort_drugs_id_'+i);
			el54.id = 'patient_cohort_drugs_id_' + newrow3;
			el54.name = 'patient_cohort_drugs_id_' + newrow3;
			
			
			var el55=document.getElementById('patient_cohort_drugs_type_'+i);
			el55.id = 'patient_cohort_drugs_type_' + newrow3;
			el55.name = 'patient_cohort_drugs_type_' + newrow3;
			
			/*var el56=document.getElementById('route_of_administration_'+i);
			el56.id = 'route_of_administration_' + newrow3;
			el56.name = 'route_of_administration_' + newrow3;*/
			
			var el510=document.getElementById('patient_cohort_drugs_row_'+i);
			el510.id = 'patient_cohort_drugs_row_' + newrow3;
			el510.name = 'patient_cohort_drugs_row_' + newrow3;
			
			var el511=document.getElementById('patient_cohort_drugs_min_'+i);
			el511.id = 'patient_cohort_drugs_min_' + newrow3;
			el511.name = 'patient_cohort_drugs_min_' + newrow3;
			
			var el512=document.getElementById('patient_cohort_drugs_max_'+i);
			el512.id = 'patient_cohort_drugs_max_' + newrow3;
			el512.name = 'patient_cohort_drugs_max_' + newrow3;
			
			var el57=document.getElementById('main_'+i);
			el57.name = 'main_' + newrow3;
			el57.id = 'main_' + newrow3;
			el57.onclick =function(){calc(this.id.substr(5))};
			
			var el58=document.getElementById('startdate_'+i);
			el58.id = 'startdate_' + newrow3;
			el58.name = 'startdate_' + newrow3;
			
			var el59=document.getElementById('stopdate_'+i);
			el59.id = 'stopdate_' + newrow3;
			el59.name = 'stopdate_' + newrow3;
			
			$('#startdate_'+newrow3).datepicker('destroy'); //detach
			$('#startdate_'+newrow3).datepicker({
			  dateFormat: 'dd-mm-yy',
			  showButtonPanel: true,
			  yearRange: "-100:+100",
			default: "0:0",
			changeMonth: true,
			changeYear: true,
			  onClose2:function(dateText) {}
			})
			$('#startdate_'+newrow3).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
			
			$('#stopdate_'+newrow3).datepicker('destroy'); //detach
			$('#stopdate_'+newrow3).datepicker({
			  dateFormat: 'dd-mm-yy',
			  showButtonPanel: true,
			  yearRange: "-100:+100",
				default: "0:0",
				changeMonth: true,
				changeYear: true,
			  onClose2:function(dateText) {}
			})
			$('#stopdate_'+newrow3).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
		
			/*var el510=document.getElementById('a_add_'+i);
			el510.id = 'a_add_' + newrow3;
			el510.name = 'a_add_' + newrow3;
			el510.onclick =function() {patient_cohort_drugs_newperiod(this.id.substr(6),el54.id);};*/

			if(el57.value!=1)
			{	
				var el511=document.getElementById('a_'+i);
				el511.id = 'a_' + newrow3;
				el511.name = 'a_' + newrow3;
				el511.onclick =function() {if (confirm("Are you sure;All periods will be deleted!")) { patient_cohort_drugs_removerow(this.id.substr(2));}};
			}
		}
		else if (document.getElementById('patient_cohort_drugs_type_'+i).value=='period')
		{
			var el512=document.getElementById('patient_cohort_drugs_tr_'+i);
			el512.id= 'patient_cohort_drugs_tr_'+newrow3;
			el512.name= 'patient_cohort_drugs_tr_'+newrow3;
			
			var el513=document.getElementById('patient_cohort_drugs_period_id_'+i);
			el513.id = 'patient_cohort_drugs_period_id_' + newrow3;
			el513.name = 'patient_cohort_drugs_period_id_' + newrow3;
			
			var el514=document.getElementById('patient_cohort_drugs_hidden_'+i);
			el514.id = 'patient_cohort_drugs_hidden_' + newrow3;
			el514.name = 'patient_cohort_drugs_hidden_' + newrow3;
			
			var el515=document.getElementById('patient_cohort_drugs_id_'+i);
			el515.id = 'patient_cohort_drugs_id_' + newrow3;
			el515.name = 'patient_cohort_drugs_id_' + newrow3;
			
			var el516=document.getElementById('patient_cohort_drugs_type_'+i);
			el516.id = 'patient_cohort_drugs_type_' + newrow3;
			el516.name = 'patient_cohort_drugs_type_' + newrow3;
			
			var el517=document.getElementById('patient_cohort_drugs_row_'+i);
			el517.id = 'patient_cohort_drugs_row_' + newrow3;
			el517.name = 'patient_cohort_drugs_row_' + newrow3;
			//el517.value=cohort_new_row;
			
			var el523=document.getElementById('reason_'+i);
			el523.name = 'reason_' + newrow3;
			el523.id = 'reason_' + newrow3;
	
			var el518=document.getElementById('dose_'+i);
			el518.id = 'dose_' + newrow3;
			el518.name = 'dose_' + newrow3;
			
			var el519=document.getElementById('frequency_'+i);
			el519.id = 'frequency_' + newrow3;
			el519.name = 'frequency_' + newrow3;
			
			var el520=document.getElementById('startdate_'+i);
			el520.id = 'startdate_' + newrow3;
			el520.name = 'startdate_' + newrow3;
			
			var el521=document.getElementById('stopdate_'+i);
			el521.id = 'stopdate_' + newrow3;
			el521.name = 'stopdate_' + newrow3;
			
			var el522=document.getElementById('a2_'+i);
			el522.id = 'a2_' + newrow3;
			el522.name = 'a2_' + newrow3;
			el522.onclick =function() {patient_cohort_drugs_removerow(this.id.substr(3));};
			
			$('#startdate_'+newrow3).datepicker('destroy'); //detach
			$('#startdate_'+newrow3).datepicker({
			 dateFormat: 'dd-mm-yy',
			  showButtonPanel: true,
			  yearRange: "-100:+100",
				default: "0:0",
				changeMonth: true,
				changeYear: true,
			  onClose2:function(dateText) {}
			})
			$('#startdate_'+newrow3).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
			
			$('#stopdate_'+newrow3).datepicker('destroy'); //detach
			$('#stopdate_'+newrow3).datepicker({
			  dateFormat: 'dd-mm-yy',
			  showButtonPanel: true,
			  yearRange: "-100:+100",
				default: "0:0",
				changeMonth: true,
				changeYear: true,
			  onClose2:function(dateText) {}
			})
			$('#stopdate_'+newrow3).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
		}
		else if (document.getElementById('patient_cohort_drugs_type_'+i).value=='period_label')
		{
			var el512=document.getElementById('patient_cohort_drugs_tr_'+i);
			el512.id= 'patient_cohort_drugs_tr_'+newrow3;
			el512.name= 'patient_cohort_drugs_tr_'+newrow3;
			
			
			var el514=document.getElementById('patient_cohort_drugs_hidden_'+i);
			el514.id = 'patient_cohort_drugs_hidden_' + newrow3;
			el514.name = 'patient_cohort_drugs_hidden_' + newrow3;
			
			var el516=document.getElementById('patient_cohort_drugs_type_'+i);
			el516.id = 'patient_cohort_drugs_type_' + newrow3;
			el516.name = 'patient_cohort_drugs_type_' + newrow3;
			
			var el517=document.getElementById('patient_cohort_drugs_row_'+i);
			el517.id = 'patient_cohort_drugs_row_' + newrow3;
			el517.name = 'patient_cohort_drugs_row_' + newrow3;
			//el517.value=cohort_new_row;
			
			var el515=document.getElementById('patient_cohort_drugs_id_'+i);
			el515.id = 'patient_cohort_drugs_id_' + newrow3;
			el515.name = 'patient_cohort_drugs_id_' + newrow3;
			
			var el510=document.getElementById('a2_add_'+i);
			el510.id = 'a2_add_' + newrow3;
			el510.name = 'a2_add_' + newrow3;
			el510.onclick =function() {patient_cohort_drugs_newperiod(this.id.substr(7),el515.value);};
		}
		
  }
  
  var row = tbl.insertRow(newrow2);
  row.id= 'patient_cohort_drugs_tr_'+newrow2;
  
	
	var drugs_frequency_ids=document.getElementById('drugs_frequency_ids').value;
	var drugs_frequency_values=document.getElementById('drugs_frequency_values').value;
	var drugs_frequency_counter=document.getElementById('drugs_frequency_counter').value;
	var drugs_frequency_value=drugs_frequency_values.split('!@#$%^');
	var drugs_frequency_id=drugs_frequency_ids.split('!@#$%^');
	
	var drugs_reason_ids=document.getElementById('drugs_reason_ids').value;
	var drugs_reason_values=document.getElementById('drugs_reason_values').value;
	var drugs_reason_counter=document.getElementById('drugs_reason_counter').value;
	var drugs_reason_value=drugs_reason_values.split('!@#$%^');
	var drugs_reason_id=drugs_reason_ids.split('!@#$%^');
	
	var r111 = row.insertCell(0);
	r111.align='center';
	var span1 = document.createElement("span");
	span1.innerHTML = "&nbsp;";
	r111.appendChild(span1);
	
  var r1 = row.insertCell(1);
  r1.align='center';
  
  var el1 = document.createElement('input');
  el1.type = 'hidden';
  el1.name = 'patient_cohort_drugs_period_id_' + newrow2;
  el1.id = 'patient_cohort_drugs_period_id_' + newrow2;
  el1.value='';
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'patient_cohort_drugs_hidden_' + newrow2;
  el2.id = 'patient_cohort_drugs_hidden_' + newrow2;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'patient_cohort_drugs_id_' + newrow2;
  el3.id = 'patient_cohort_drugs_id_' + newrow2;
  el3.value=patient_cohort_drugs_id;
  
  var el33 = document.createElement('input');
  el33.type = 'hidden';
  el33.name = 'patient_cohort_drugs_type_' + newrow2;
  el33.id = 'patient_cohort_drugs_type_' + newrow2;
  el33.value='period';
  
  var el34 = document.createElement('input');
  el34.type = 'hidden';
  el34.name = 'patient_cohort_drugs_row_' + newrow2;
  el34.id = 'patient_cohort_drugs_row_' + newrow2;
  el34.value = $('#patient_cohort_drugs_row_' + w_row).val();
  
  var el35 = document.createElement('select');
	el35.name = 'reason_' + newrow2;
	el35.id = 'reason_' + newrow2;
	el35.className="form-control";
	el35.style.width='100%';
	
	var opt=new Option("--",0);
	el35.add(opt,undefined);
	for(i = 0; i < drugs_reason_counter; i++)
	{
		var opt=new Option(drugs_reason_value[i],drugs_reason_id[i]);
		el35.add(opt,undefined);
	}
	
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  r1.appendChild(el33);
  r1.appendChild(el34);
  r1.appendChild(el35);
  
	var r44 = row.insertCell(2);
	r44.align='center';
	
	
	var el5 = document.createElement('select');
	el5.name = 'frequency_' + newrow2;
	el5.id = 'frequency_' + newrow2;
	el5.className="form-control";
	el5.style.width='100%';
	
	var opt=new Option("--",0);
	el5.add(opt,undefined);
	for(i = 0; i < drugs_frequency_counter; i++)
	{
		var opt=new Option(drugs_frequency_value[i],drugs_frequency_id[i]);
		el5.add(opt,undefined);
	}	
	
	
	
	r44.appendChild(el5);
	
	var r11 = row.insertCell(3);
	r11.align='center';
  var el4 = document.createElement('input');
	el4.name = 'dose_' + newrow2;
	el4.id = 'dose_' + newrow2;
	el4.style.width='100%';
	el4.className='form-control';	
	
	
	  
	r11.appendChild(el4);
	
	$('#dose_'+newrow2).focusout(function() 
	{
		/*if (($(this).val()).charAt(0)=="0")
			$(this).val(($(this).val()).substr(1));
		
		if(!isNaN($(this).val()))
		{
			if ($(this).val()=="")
				$(this).val("0");
		}
		else
		{
			$(this).val(($(this).val()).slice(0,-1));
			if ($(this).val()=="")
				$(this).val("0");
		}*/
		if($(this).val()=="")
			$(this).val("0");
		else
			$(this).val(parseFloat($(this).val()));
	});
	
	var r4 = row.insertCell(4);
	r4.align='center';
  
	var el6 = document.createElement('input');
	  el6.type = 'text';
	  el6.name = 'startdate_' + newrow2;
	  el6.id = 'startdate_' + newrow2;
	  el6.style.width='100%';
	  el6.className='form-control';
	
	r4.appendChild(el6);
	
  var r5 = row.insertCell(5);
	r5.align='center';
	
	var el7 = document.createElement('input');
	  el7.type = 'text';
	  el7.name = 'stopdate_' + newrow2;
	  el7.id = 'stopdate_' + newrow2;
	  el7.style.width='100%';
	  el7.className='form-control';
  
	r5.appendChild(el7);
	 
	var r2 = row.insertCell(6);
	r2.align='center';
	
	var el8 = document.createElement('a');
	el8.name = 'a2_' + newrow2;
	el8.id = 'a2_' + newrow2;
	el8.title = 'Delete';
	el8.href="javascript:avoid(0)";
	var icon = document.createElement('i');
	icon.className='fa fa-trash-o';
	el8.appendChild(icon);
	el8.onclick =function(){patient_cohort_drugs_period_removerow(newrow2)};
  
	r2.appendChild(el8);
	
	$('#startdate_'+newrow2).datepicker({
		  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  $('#startdate_'+newrow2).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
		
		$('#stopdate_'+newrow2).datepicker({
		 dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  $('#stopdate_'+newrow2).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
}

function patient_cohort_drugs_removerow(row)
{
	document.getElementById('patient_cohort_drugs_hidden_'+row).value='-1';
	document.getElementById('patient_cohort_drugs_tr_'+row).style.display='none';
	 var tbl = document.getElementById('patient_drugs_tbl');
	 var lastRow = tbl.rows.length;
	
	for (i=row;i<=lastRow-1;i++)
	{
		if (document.getElementById('patient_cohort_drugs_type_'+i).value=='period' || document.getElementById('patient_cohort_drugs_type_'+i).value=='period_label')
		{
			if (document.getElementById('patient_cohort_drugs_row_'+i).value==document.getElementById('patient_cohort_drugs_row_'+row).value)
			{
				document.getElementById('patient_cohort_drugs_hidden_'+i).value='-1';
				document.getElementById('patient_cohort_drugs_tr_'+i).style.display='none';
			}
		}
	}
	
}

function patient_cohort_drugs_period_removerow(row)
{
	document.getElementById('patient_cohort_drugs_hidden_'+row).value='-1';
	document.getElementById('patient_cohort_drugs_tr_'+row).style.display='none';
	
}

</script>
<?php
	
if($save==1)
{
	if($save_chk=="ok")
	{
?>
<script>
$(".alert_suc").show();
window.setTimeout(function() {
    $(".alert_suc").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
	else
	{
?>
<script>
$(".alert_wr").show();
window.setTimeout(function() {
    $(".alert_wr").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
}
?>
</body>
</html>
