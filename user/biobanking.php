		
<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	$save=$_REQUEST['save'];
	$pat_id=$_REQUEST['pat_id'];
	if($pat_id<>"")
		$_SESSION['pat_id']=$pat_id;
	
	$pat_id=$_SESSION['pat_id'];
	
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<?php
if($save==1)
{
	pg_query("BEGIN") or die("Could not start transaction\n");
	try
	{
		//patient_biobanking table-start
		
		$j=1;
		$patient_biobanking_numofrows=$_REQUEST['patient_biobanking_numofrows'];
		for ($i=1;$i<=$patient_biobanking_numofrows;$i++)
		{
			$patient_biobanking_hidden=$_REQUEST['patient_biobanking_hidden_'.$i];
			$patient_biobanking_id=$_REQUEST['patient_biobanking_id_'.$i];
			if($patient_biobanking_hidden=='-1')
			{
				if($patient_biobanking_id<>"")
				{
					$patient_biobanking_hdn_parameters[0]=1;
					$patient_biobanking_hdn_names[0]='deleted';
					$patient_biobanking_hdn_edit_name[0]='patient_biobanking_id';
					$patient_biobanking_hdn_edit_id[0]=$patient_biobanking_id;
					$patient_biobanking_hdn_sumbol[0]='=';
					$patient_biobanking_msg=update('patient_biobanking',$patient_biobanking_hdn_names,$patient_biobanking_hdn_parameters,$patient_biobanking_hdn_edit_name,$patient_biobanking_hdn_edit_id,$patient_biobanking_hdn_sumbol);
					if ($patient_biobanking_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
			{
				
				$patient_biobanking_parameters[0]=$_REQUEST['biobanking_samples_id_'.$i];
				$patient_biobanking_parameters[1]=date_for_postgres($_REQUEST['biobanking_date_'.$i]);
				$patient_biobanking_parameters[2]=$_REQUEST['protocol_descr_'.$i];
				$patient_biobanking_parameters[3]=str_replace("'", "''",$_REQUEST['codenbr_'.$i]);
				$patient_biobanking_parameters[4]=str_replace("'", "''",$_REQUEST['box_'.$i]);
				$patient_biobanking_parameters[5]="";//str_replace("'", "''",$_REQUEST['position_'.$i]);
				$patient_biobanking_parameters[6]=$_REQUEST['availability_'.$i];
				$patient_biobanking_parameters[7]=$user_id;
				$patient_biobanking_parameters[8]='now()';
				$patient_biobanking_parameters[9]=$_REQUEST['patient_cohort_id_'.$i];
				$patient_biobanking_parameters[10]=$_REQUEST['patient_followup_id_'.$i];
				$patient_biobanking_parameters[11]=$_REQUEST['pbmcs_'.$i];
				$patient_biobanking_parameters[12]=$_REQUEST['pmns_'.$i];
				
				$patient_biobanking_table_names[0]='biobanking_sample_id';
				$patient_biobanking_table_names[1]='biobanking_date';
				$patient_biobanking_table_names[2]='protocol_descr';
				$patient_biobanking_table_names[3]='codenbr';
				$patient_biobanking_table_names[4]='box';
				$patient_biobanking_table_names[5]='position';
				$patient_biobanking_table_names[6]='availability';
				$patient_biobanking_table_names[7]='editor_id';
				$patient_biobanking_table_names[8]='edit_date';
				$patient_biobanking_table_names[9]='patient_cohort_id';
				$patient_biobanking_table_names[10]='patient_followup_id';
				$patient_biobanking_table_names[11]='pbmcs';
				$patient_biobanking_table_names[12]='pmns';
				$patient_biobanking_edit_name[0]='patient_biobanking_id';
				$patient_biobanking_edit_id[0]=$patient_biobanking_id;
				$patient_biobanking_sumbol[0]='=';
				
				if($patient_biobanking_id=='')
				{
					
					$patient_biobanking_parameters[13]=$pat_id;
					$patient_biobanking_parameters[14]=0;
					
					$patient_biobanking_table_names[13]='pat_id';
					$patient_biobanking_table_names[14]='deleted';
					
					$id=insert('patient_biobanking',$patient_biobanking_table_names,$patient_biobanking_parameters,'patient_biobanking_id');
					if($id!='')
						$j++;
				}
				else
				{
					$patient_biobanking_msg=update('patient_biobanking',$patient_biobanking_table_names,$patient_biobanking_parameters,$patient_biobanking_edit_name,$patient_biobanking_edit_id,$patient_biobanking_sumbol);
					if ($patient_biobanking_msg=="ok")
						$j++;
				}
			}
		}
		
		if ($j==$i)
			$biobanking_msg="ok";
		else
			$biobanking_msg="nok";
		//patient_biobanking table-end
		
		
		if ($biobanking_msg=="ok")
		{
			$save_chk="ok";
			pg_query("COMMIT") or die("Transaction commit failed\n");
		}
		else
		{
			$save_chk="nok";
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
	}
	catch(exception $e) {
		echo "e:".$e."</br>";
		echo "ROLLBACK";
		pg_query("ROLLBACK") or die("Transaction rollback failed\n");
	}
	
}

$exec = get_patient_demographics($pat_id);
$result = pg_fetch_array($exec);
$dateofinclusion=$result['dateofinclusion_str'];
if($dateofinclusion=="12-12-1900")
	$dateofinclusion="";
$dateofbirth=$result['dateofbirth_str'];
if($dateofbirth=="12-12-1900")
	$dateofbirth="";
$gender=$result['gender'];
if($gender==1)
	$gender_str="Female";
else if($gender==2)
	$gender_str="Male";
$patient_id=$result['patient_id'];
?>
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
	   <h1>
          Biobanking
		</h1>
		<h1>
		<?php echo "<small>Patient:".$patient_id.",".$gender_str." (".$dateofbirth.") </small>"; ?>
      </h1>
      </section>

      <!-- Main content -->
      <section class="content">
	  <div class="alert alert_suc alert-success" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Successful save!</strong>
	  </div>
	  <div class="alert alert_wr alert-danger" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Unsuccessful save!</strong>
	  </div>
	  <form id="form" action="biobanking.php" method="POST">
	  <input type="hidden" id="save" name="save" value="1">
		<input type="hidden" id="pat_id" name="pat_id" value="<?php echo $pat_id;?>">
		<div class="row">
			<div class="form-group col-md-12">
				<input type="submit" class="btn btn-primary" value="Save">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Biobanking samples</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-12" id="divdiagnosis" name="divdiagnosis">
							<?php
										$sql = get_lookup_tbl_values('biobanking_samples');
										$biobanking_sample_counter=0;				
										while($result2 = pg_fetch_array($sql))
										{	
											$biobanking_sample_values .= $result2['value'];
											$biobanking_sample_values .= "!@#$%^" ;
											$biobanking_sample_ids .=$result2['id'];
											$biobanking_sample_ids .= "!@#$%^" ;
															
											$biobanking_sample_counter++;
										}
									?>
										<input type="hidden" value="<?php echo $biobanking_sample_counter; ?>" id="biobanking_sample_counter" name="biobanking_sample_counter"/>
										<input type="hidden" value="<?php echo $biobanking_sample_values; ?>" id="biobanking_sample_values" name="biobanking_sample_values"/>
										<input type="hidden" value="<?php echo $biobanking_sample_ids; ?>" id="biobanking_sample_ids" name="biobanking_sample_ids"/>
										
							<?php
										$sql = get_lookup_tbl_values('protocol_description');
										$protocol_descr_counter=0;				
										while($result2 = pg_fetch_array($sql))
										{	
											$protocol_descr_values .= $result2['value'];
											$protocol_descr_values .= "!@#$%^" ;
											$protocol_descr_ids .=$result2['id'];
											$protocol_descr_ids .= "!@#$%^" ;
															
											$protocol_descr_counter++;
										}
									?>
										<input type="hidden" value="<?php echo $protocol_descr_counter; ?>" id="protocol_descr_counter" name="protocol_descr_counter"/>
										<input type="hidden" value="<?php echo $protocol_descr_values; ?>" id="protocol_descr_values" name="protocol_descr_values"/>
										<input type="hidden" value="<?php echo $protocol_descr_ids; ?>" id="protocol_descr_ids" name="protocol_descr_ids"/>
										<?php
										$sql = get_cohort_names($pat_id);
										$cohort_counter=0;				
										while($result2 = pg_fetch_array($sql))
										{	
											$cohort_values .= $result2['patient_cohort_date_str'];
											$cohort_values .= "!@#$%^" ;
											$cohort_ids .=$result2['patient_cohort_id'];
											$cohort_ids .= "!@#$%^" ;
															
											$cohort_counter++;
										}
									?>
										<input type="hidden" value="<?php echo $cohort_counter; ?>" id="cohort_counter" name="cohort_counter"/>
										<input type="hidden" value="<?php echo $cohort_values; ?>" id="cohort_values" name="cohort_values"/>
										<input type="hidden" value="<?php echo $cohort_ids; ?>" id="cohort_ids" name="cohort_ids"/>
								  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="patient_biobanking_tbl" name="patient_biobanking_tbl"  width="1200px"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-1"><b>Date</b></th>
											<th class="col-md-1"><b>Cohort</b></th>
											<th class="col-md-2"><b>FU Date</b></th>
											<th class="col-md-1"><b>Protocol descr</b></th>
											<th class="col-md-1"><b>Sample</b></th>
											<th class="col-md-1"><b>Code Nbr</b></th>
											<th class="col-md-1"><b>Box</b></th>
											<th class="col-md-1"><b>PBMCs</b></th>
											<th class="col-md-1"><b>PMNs</b></th>
											<th class="col-md-1"><b>Avail/ty</b></th>
											<th class="col-md-1">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
											$order = get_biobankings($pat_id);
											$num_rows = pg_num_rows($order);
											
											while($result = pg_fetch_assoc($order))
											{
												$patient_biobanking_id=$result['patient_biobanking_id'];
												$biobanking_date=$result['biobanking_date_str'];
												if($biobanking_date=="12-12-1900")
													$biobanking_date="";
												$protocol_descr=$result['protocol_descr'];
												$biobanking_sample_id=$result['biobanking_sample_id'];
												$codenbr=$result['codenbr'];
												$box=$result['box'];
												$pbmcs=$result['pbmcs'];
												$pmns=$result['pmns'];
												$availability=$result['availability'];
												$patient_cohort_id=$result['patient_cohort_id'];
												$patient_followup_id=$result['patient_followup_id'];
												
									?>
										  <tr id="patient_biobanking_tr_<?php echo $i;?>"> 
											   <td align="center">
													<input type="hidden" id="patient_biobanking_hidden_<?php echo $i;?>" name="patient_biobanking_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="patient_biobanking_id_<?php echo $i; ?>" id="patient_biobanking_id_<?php echo $i; ?>" value="<?php echo $patient_biobanking_id;?>">
													<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="biobanking_date_<?php echo $i; ?>" name="biobanking_date_<?php echo $i; ?>" value="<?php echo $biobanking_date; ?>">
												</td>
												<td align="center">
													<select  onchange="setfollowupdates(<?php echo $i; ?>)" class="form-control" name="patient_cohort_id_<?php echo $i; ?>" id="patient_cohort_id_<?php echo $i; ?>">
														<option value="0">--</option>
														<?php
															$sql = get_cohort_names($pat_id);
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$patient_cohort_id2=$result2['patient_cohort_id'];
																$cohort_name=$result2['patient_cohort_date_str'];
																
																if($patient_cohort_id==$patient_cohort_id2)
																	$patient_cohort_id_sel=$patient_cohort_id2;
																
														?>
														<option value="<?php echo $patient_cohort_id2; ?>" <?php if($patient_cohort_id==$patient_cohort_id2) { echo "selected"; } ?>><?php echo $cohort_name; ?></option>
														<?php
															}
														?>
													</select>
												</td>
												<td align="center">
													<select class="form-control" name="patient_followup_id_<?php echo $i; ?>" id="patient_followup_id_<?php echo $i; ?>">
														<?php
														//if ($patient_cohort_id_sel==0)
														//{
														?>
															<option value="0">--</option>
														<?php
														//}
														?>
														<?php
															$sql = get_followup_dates($patient_cohort_id_sel);
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$patient_followup_id2=$result2['patient_followup_id'];
																$patient_followup_date=$result2['patient_followup_date_str'];
																if($patient_followup_date=="12-12-1900")
																	$patient_followup_date="";
																
																//if($patient_followup_id2==$patient_followup_id) 
																//{
														?>
														<option value="<?php echo $patient_followup_id2; ?>" <?php if($patient_followup_id==$patient_followup_id2) { echo "selected"; } ?>><?php echo $patient_followup_date; ?></option>
														<?php
																//}
															}
														?>
													</select>
												</td>
												 <td align="center">
													<select class="form-control" name="protocol_descr_<?php echo $i; ?>" id="protocol_descr_<?php echo $i; ?>">
														<option value="0">--</option>
														<?php
															$sql = get_lookup_tbl_values('protocol_description');
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$protocol_descr_id2=$result2['id'];
																$value=$result2['value'];

														?>
															<option value="<?php echo $protocol_descr_id2; ?>" <?php if($protocol_descr==$protocol_descr_id2) { echo "selected"; } ?>><?php echo $value; ?></option>
														<?php
															}
																
														?>
													</select>
												</td>
												<td align="center">
													<select class="form-control" name="biobanking_samples_id_<?php echo $i; ?>" id="biobanking_samples_id_<?php echo $i; ?>">
														<option value="0">--</option>
														<?php
															$sql = get_lookup_tbl_values('biobanking_samples');
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$biobanking_sample_id2=$result2['id'];
																$value=$result2['value'];
														?>
															<option value="<?php echo $biobanking_sample_id2; ?>" <?php if($biobanking_sample_id==$biobanking_sample_id2) { echo "selected"; } ?>><?php echo $value; ?></option>
														<?php
															}
																
														?>
													</select>
												</td>
												<td align="center">
													<input type="text" class="form-control" id="codenbr_<?php echo $i; ?>" name="codenbr_<?php echo $i; ?>" value="<?php echo $codenbr; ?>">
												</td>
												<td align="center">
													<input type="text" class="form-control" id="box_<?php echo $i; ?>" name="box_<?php echo $i; ?>" value="<?php echo $box; ?>">
												</td>
												<td align="center">
													<select class="form-control" name="pbmcs_<?php echo $i; ?>" id="pbmcs_<?php echo $i; ?>">
														<option value="0" <?php if($pbmcs==0) { echo "selected"; } ?>>--</option>
														<option value="1" <?php if($pbmcs==1) { echo "selected"; } ?>>Yes</option>
														<option value="2" <?php if($pbmcs==2) { echo "selected"; } ?>>No</option>
													</select>
												</td>
												<td align="center">
													<select class="form-control" name="pmns_<?php echo $i; ?>" id="pmns_<?php echo $i; ?>">
														<option value="0" <?php if($pmns==0) { echo "selected"; } ?>>--</option>
														<option value="1" <?php if($pmns==1) { echo "selected"; } ?>>Yes</option>
														<option value="2" <?php if($pmns==2) { echo "selected"; } ?>>No</option>
													</select>
												</td>
												<td align="center">
													<select class="form-control" name="availability_<?php echo $i; ?>" id="availability_<?php echo $i; ?>">
														<option value="0" <?php if($availability==0) { echo "selected"; } ?>>--</option>
														<option value="1" <?php if($availability==1) { echo "selected"; } ?>>Avalaible</option>
														<option value="2" <?php if($availability==2) { echo "selected"; } ?>>Used</option>
													</select>
												</td>
													<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="patient_biobanking_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
												  </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										
									?>
									</tbody>
								</table>
								<input type="hidden" name="patient_biobanking_numofrows" id="patient_biobanking_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								 <a class="btn btn-primary" href="javascript:void(0);" onclick="patient_biobanking_addrow();"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
					</div>
				</div><!-- diagnoses status div -->
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<input type="submit" class="btn btn-primary" value="Save">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				&nbsp;
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				&nbsp;
			</div>
		</div>
		</form>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
   <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>

function setfollowupdates(row)
{
		
        var patient_cohort_id = $("#patient_cohort_id_"+row).val();
		
        $.ajax({
            url: 'populate_fu_content.php',
            type: 'post',
            data: {
				id:patient_cohort_id
				},
            dataType: 'json',
            success:function(response){
			
                var len = response.length;
				
                $("#patient_followup_id_"+row).empty();
				$("#patient_followup_id_"+row).append("<option value='0'>--</option>");
				
                for( var i = 0; i<len; i++){
                    var id2 = response[i]['id'];
					var value = response[i]['value'];
					
                    $("#patient_followup_id_"+row).append("<option value='"+id2+"'>"+value+"</option>");
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }


jQuery(document).ready(function() {
	
	$('#patient_biobanking_tbl').dataTable( {
  "scrollX": true,
  "scrollY": "50vh",
  "scrollX": "50vh",
  "paging": false
} );

if ($(".dataTables_empty")[0]){
    $('.odd').remove();
	$('#patient_biobanking_numofrows').val();
}
$('#patient_biobanking_tbl_filter').remove();
$('#patient_biobanking_tbl_info').remove();



for(i=1;i<=document.getElementById('patient_biobanking_numofrows').value;i++)
{
	$('#biobanking_date_'+i).datepicker({
	  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	})
	$('#biobanking_date_'+i).inputmask();
}

$(document).ajaxStart(function(){
		$('body').loading();
		
    });
    $(document).ajaxComplete(function(){
       $('body').loading('stop');
    });
})

function patient_biobanking_addrow()
{
  var tbl = document.getElementById('patient_biobanking_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'patient_biobanking_tr_'+iteration;
  document.getElementById('patient_biobanking_numofrows').value = iteration;
  
	var biobanking_sample_ids=document.getElementById('biobanking_sample_ids').value;
	var biobanking_sample_values=document.getElementById('biobanking_sample_values').value;
	var biobanking_sample_counter=document.getElementById('biobanking_sample_counter').value;
	var biobanking_sample_value=biobanking_sample_values.split('!@#$%^');
	var biobanking_sample_id=biobanking_sample_ids.split('!@#$%^');
	
	var protocol_descr_ids=document.getElementById('protocol_descr_ids').value;
	var protocol_descr_values=document.getElementById('protocol_descr_values').value;
	var protocol_descr_counter=document.getElementById('protocol_descr_counter').value;
	var protocol_descr_value=protocol_descr_values.split('!@#$%^');
	var protocol_descr_id=protocol_descr_ids.split('!@#$%^');
	
	var cohort_ids=document.getElementById('cohort_ids').value;
	var cohort_values=document.getElementById('cohort_values').value;
	var cohort_counter=document.getElementById('cohort_counter').value;
	var cohort_value=cohort_values.split('!@#$%^');
	var cohort_id=cohort_ids.split('!@#$%^');
	
  var r1 = row.insertCell(0);
  r1.align='center';
  
   var el1 = document.createElement('input');
  el1.type = 'text';
  el1.name = 'biobanking_date_' + iteration;
  el1.id = 'biobanking_date_' + iteration;
  el1.className="form-control";
  el1.style.width='100%';
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'patient_biobanking_hidden_' + iteration;
  el2.id = 'patient_biobanking_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'patient_biobanking_id_' + iteration;
  el3.id = 'patient_biobanking_id_' + iteration;
  el3.value='';
  
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
	var r11 = row.insertCell(1);
	r11.align='center';
  
	var el11 = document.createElement('select');
	el11.name = 'patient_cohort_id_' + iteration;
	el11.id = 'patient_cohort_id_' + iteration;
	el11.style.width='100%';
	el11.className='form-control';
	el11.onchange =function(){setfollowupdates(iteration)};
	
	var opt=new Option("--",0);
	el11.add(opt,undefined);
	for(i = 0; i < cohort_counter; i++)
	{
		var opt=new Option(cohort_value[i],cohort_id[i]);
		el11.add(opt,undefined);
	}
	
	r11.appendChild(el11);
  
	var r12 = row.insertCell(2);
	r12.align='center';
  
	var el12 = document.createElement('select');
	el12.name = 'patient_followup_id_' + iteration;
	el12.id = 'patient_followup_id_' + iteration;
	el12.style.width='100%';
	el12.className='form-control';
													
	var opt=new Option("--",0);
	el12.add(opt,undefined);
  
  r12.appendChild(el12);
  
  var r2 = row.insertCell(3);
  r2.align='center';
 
  var el4 = document.createElement('select');
  el4.name = 'protocol_descr_' + iteration;
  el4.id = 'protocol_descr_' + iteration;
  el4.style.width='100%';
  el4.className='form-control';
													
	var opt=new Option("--",0);
	el4.add(opt,undefined);
	for(i = 0; i < protocol_descr_counter; i++)
	{
		var opt=new Option(protocol_descr_value[i],protocol_descr_id[i]);
		el4.add(opt,undefined);
	}
	
  r2.appendChild(el4);
  
  var r3 = row.insertCell(4);
  r3.align='center';
  
  var el5 = document.createElement('select');
  el5.name = 'biobanking_samples_id_' + iteration;
  el5.id = 'biobanking_samples_id_' + iteration;
  el5.style.width='100%';
  el5.className='form-control';
  
	var opt=new Option("--",0);
	el5.add(opt,undefined);
	for(i = 0; i < biobanking_sample_counter; i++)
	{
		var opt=new Option(biobanking_sample_value[i],biobanking_sample_id[i]);
		el5.add(opt,undefined);
	}
  
  r3.appendChild(el5);
  
  var r4 = row.insertCell(5);
  r4.align='center';
  
  var el6 = document.createElement('input');
  el6.type = 'text';
  el6.name = 'codenbr_' + iteration;
  el6.id = 'codenbr_' + iteration;
  el6.className="form-control";
  el6.style.width='100%';
  
  r4.appendChild(el6);
  
  var r5 = row.insertCell(6);
  r5.align='center';
  
  var el7 = document.createElement('input');
  el7.type = 'text';
  el7.name = 'box_' + iteration;
  el7.id = 'box_' + iteration;
  el7.className="form-control";
  el7.style.width='100%';
  
  r5.appendChild(el7);
  
  var r6 = row.insertCell(7);
  r6.align='center';
  
	var el8 = document.createElement('select');
	el8.name = 'pbmcs_' + iteration;
	el8.id = 'pbmcs_' + iteration;
	el8.style.width='100%';
	el8.className='form-control';
													
	var opt=new Option("--",0);
	el8.add(opt,undefined);
	var opt=new Option("Yes",1);
	el8.add(opt,undefined);
	var opt=new Option("No",2);
	el8.add(opt,undefined);
  
  r6.appendChild(el8);
  
  var r66 = row.insertCell(8);
  r66.align='center';
  
	var el88 = document.createElement('select');
	el88.name = 'pmns_' + iteration;
	el88.id = 'pmns_' + iteration;
	el88.style.width='100%';
	el88.className='form-control';
													
	var opt=new Option("--",0);
	el88.add(opt,undefined);
	var opt=new Option("Yes",1);
	el88.add(opt,undefined);
	var opt=new Option("No",2);
	el88.add(opt,undefined);
  
  r66.appendChild(el88);
  
  
  var r7 = row.insertCell(9);
  r7.align='center';
  
  var el9 = document.createElement('select');
  el9.name = 'availability_' + iteration;
  el9.id = 'availability_' + iteration;
  el9.style.width='100%';
  el9.className='form-control';
													
	var opt=new Option("--",0);
	el9.add(opt,undefined);
	var opt=new Option("Available",1);
	el9.add(opt,undefined);
	var opt=new Option("Used",2);
	el9.add(opt,undefined);
	
	
  r7.appendChild(el9);
  
  var r8 = row.insertCell(10);
  r8.align='center';
  
  var el10 = document.createElement('a');
  el10.name = 'a_' + iteration;
  el10.id = 'a_' + iteration;
  el10.title = 'Delete';
  el10.href="javascript:avoid(0)";
  var icon = document.createElement('i');
  icon.className='fa fa-trash-o';
  el10.appendChild(icon);
  el10.onclick =function(){patient_biobanking_removerow(iteration)};
		 
  r8.appendChild(el10);
  
  $('#biobanking_date_'+iteration).datepicker({
	   dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	})
	$('#biobanking_date_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
}

function patient_biobanking_removerow(row)
{
	document.getElementById('patient_biobanking_hidden_'+row).value='-1';
	document.getElementById('patient_biobanking_tr_'+row).style.display='none';
}

</script>
<?php
	
if($save==1)
{
	if($save_chk=="ok")
	{
?>
<script>
$(".alert_suc").show();
window.setTimeout(function() {
    $(".alert_suc").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
	else
	{
?>
<script>
$(".alert_wr").show();
window.setTimeout(function() {
    $(".alert_wr").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
}
?>
</body>
</html>