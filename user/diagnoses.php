		
<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	$save=$_REQUEST['save'];
	$pat_id=$_REQUEST['pat_id'];
	if($pat_id<>"")
		$_SESSION['pat_id']=$pat_id;
	
	$pat_id=$_SESSION['pat_id'];
	
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<?php
$exec = get_patient_demographics($pat_id);
$result = pg_fetch_array($exec);
$dateofinclusion=$result['dateofinclusion_str'];
if($dateofinclusion=="12-12-1900" or $dateofinclusion=="")
	$dateofinclusion="";
$dateofbirth=$result['dateofbirth_str'];
if($dateofbirth=="12-12-1900")
	$dateofbirth="";
$gender=$result['gender'];
if($gender==1)
	$gender_str="Female";
else if($gender==2)
	$gender_str="Male";
$patient_id=$result['patient_id'];
if($save==1)
{
	pg_query("BEGIN") or die("Could not start transaction\n");
	try
	{
		//diagnosis table-start
		
		
		if ($dateofinclusion=="")
			$date1="";
		else
		{
			$inclusion_date = explode("-",$dateofinclusion);
			$date1 = new DateTime($inclusion_date[2].'-'.$inclusion_date[1].'-'.$inclusion_date[0]);
		}
		
		$j=1;
		$diagnosis_numofrows=$_REQUEST['diagnosis_numofrows'];
		for ($i=1;$i<=$diagnosis_numofrows;$i++)
		{
			$diagnosis_hidden=$_REQUEST['diagnosis_hidden_'.$i];
			$diagnoses_id=$_REQUEST['diagnoses_id_'.$i];
			if($diagnosis_hidden=='-1')
			{
				if($diagnoses_id<>"")
				{
					$diagnosis_hdn_parameters[0]=1;
					$diagnosis_hdn_names[0]='deleted';
					$diagnosis_hdn_edit_name[0]='diagnoses_id';
					$diagnosis_hdn_edit_id[0]=$diagnoses_id;
					$diagnosis_hdn_sumbol[0]='=';
					$diagnosis_msg=update('patient_diagnosis',$diagnosis_hdn_names,$diagnosis_hdn_parameters,$diagnosis_hdn_edit_name,$diagnosis_hdn_edit_id,$diagnosis_hdn_sumbol);
					if ($diagnosis_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
			{
				if (isset($_REQUEST['main_'.$i]))
					$main="1";
				else
					$main="0";
				
				if (isset($_REQUEST['inactive_'.$i]))
					$inactive="1";
				else
					$inactive="0";
				
				if($_REQUEST['symptom_date_'.$i]=="" or $date1=="")
				{
					$disdurincl=0;
				}
				else
				{
					$symptom_date = explode("-",$_REQUEST['symptom_date_'.$i]);
					$date2 = $date1->diff(new DateTime($symptom_date[2].'-'.$symptom_date[1].'-'.$symptom_date[0]));
					$disdurincl = $date2->y * 12 + $date2->m;
				}
				
				$diagnosis_parameters[0]=$_REQUEST['diagnosis_id_'.$i];
				$diagnosis_parameters[1]=$main;
				$diagnosis_parameters[2]=date_for_postgres($_REQUEST['symptom_date_'.$i]);
				$diagnosis_parameters[3]=date_for_postgres($_REQUEST['disease_date_'.$i]);
				$diagnosis_parameters[4]=$user_id;
				$diagnosis_parameters[5]='now()';
				$diagnosis_parameters[6]=$disdurincl;
				$diagnosis_parameters[7]=$inactive;
				$diagnosis_parameters[8]=date_for_postgres($_REQUEST['inactive_date_'.$i]);
				$diagnosis_table_names[0]='diagnosis_id';
				$diagnosis_table_names[1]='main';
				$diagnosis_table_names[2]='symptom_date';
				$diagnosis_table_names[3]='disease_date';
				$diagnosis_table_names[4]='editor_id';
				$diagnosis_table_names[5]='edit_date';
				$diagnosis_table_names[6]='disdurincl';
				$diagnosis_table_names[7]='inactive';
				$diagnosis_table_names[8]='inactive_date';
				$diagnosis_edit_name[0]='diagnoses_id';
				$diagnosis_edit_id[0]=$diagnoses_id;
				$diagnosis_sumbol[0]='=';
				
				if($diagnoses_id=='')
				{
					
					$diagnosis_parameters[9]=$pat_id;
					$diagnosis_parameters[10]=0;
					
					$diagnosis_table_names[9]='pat_id';
					$diagnosis_table_names[10]='deleted';
					
					$id=insert('patient_diagnosis',$diagnosis_table_names,$diagnosis_parameters,'diagnoses_id');
					if($id!='')
						$j++;
				}
				else
				{
					$diagnosis_msg=update('patient_diagnosis',$diagnosis_table_names,$diagnosis_parameters,$diagnosis_edit_name,$diagnosis_edit_id,$diagnosis_sumbol);
					if ($diagnosis_msg=="ok")
						$j++;
				}
			}
		}
		
		if ($j==$i)
			$diagnosis_msg="ok";
		else
			$diagnosis_msg="nok";
		//diagnosis table-end
		
		//criteriapa table-start
		$criteriapa=$_REQUEST['criteriapa'];
		
		$j=0;
		$i=0;
		if($criteriapa==1)
		{
			$i++;
			$criteriapa_parameters[0]=$pat_id;
			$criteriapa_parameters[1]=0;
			$criteriapa_parameters[2]=$user_id;
			$criteriapa_parameters[3]='now()';
			$criteriapa_parameters[4]=$_REQUEST['criteria1'];
			$criteriapa_parameters[5]=date_for_postgres($_REQUEST['date1']);
			$criteriapa_parameters[6]=$_REQUEST['criteria2'];
			$criteriapa_parameters[7]=date_for_postgres($_REQUEST['date2']);
			$criteriapa_parameters[8]=$_REQUEST['criteria3'];
			$criteriapa_parameters[9]=date_for_postgres($_REQUEST['date3']);
			if($_REQUEST['criteria3']==1)
			{
				$criteriapa_parameters[10]=$_REQUEST['criteriab1'];
				$criteriapa_parameters[11]=date_for_postgres($_REQUEST['dateb1']);
				$criteriapa_parameters[12]=$_REQUEST['criteriab2'];
				$criteriapa_parameters[13]=date_for_postgres($_REQUEST['dateb2']);
				$criteriapa_parameters[14]=$_REQUEST['criteriab3'];
				$criteriapa_parameters[15]=date_for_postgres($_REQUEST['dateb3']);
				$criteriapa_parameters[16]=$_REQUEST['criteriab4'];
				$criteriapa_parameters[17]=date_for_postgres($_REQUEST['dateb4']);
			}
			else
			{
				$criteriapa_parameters[10]=0;
				$criteriapa_parameters[11]=date_for_postgres("");
				$criteriapa_parameters[12]=0;
				$criteriapa_parameters[13]=date_for_postgres("");
				$criteriapa_parameters[14]=0;
				$criteriapa_parameters[15]=date_for_postgres("");
				$criteriapa_parameters[16]=0;
				$criteriapa_parameters[17]=date_for_postgres("");
			}
			$criteriapa_parameters[18]=$_REQUEST['rheumatoid'];
			$criteriapa_parameters[19]=date_for_postgres($_REQUEST['rheumatoiddate']);
			if($_REQUEST['totalscore']=="")
				$criteriapa_parameters[20]=0;
			else
				$criteriapa_parameters[20]=$_REQUEST['totalscore'];
			
			$criteriapa_table_names[0]='pat_id';
			$criteriapa_table_names[1]='deleted';
			$criteriapa_table_names[2]='editor_id';
			$criteriapa_table_names[3]='edit_date';
			$criteriapa_table_names[4]='criteria1';
			$criteriapa_table_names[5]='date1';
			$criteriapa_table_names[6]='criteria2';
			$criteriapa_table_names[7]='date2';
			$criteriapa_table_names[8]='criteria3';
			$criteriapa_table_names[9]='date3';
			$criteriapa_table_names[10]='criteriab1';
			$criteriapa_table_names[11]='dateb1';
			$criteriapa_table_names[12]='criteriab2';
			$criteriapa_table_names[13]='dateb2';
			$criteriapa_table_names[14]='criteriab3';
			$criteriapa_table_names[15]='dateb3';
			$criteriapa_table_names[16]='criteriab4';
			$criteriapa_table_names[17]='dateb4';
			$criteriapa_table_names[18]='rheumatoid';
			$criteriapa_table_names[19]='rheumatoiddate';
			$criteriapa_table_names[20]='totalscore';
			
			$racriteria_id=$_REQUEST['racriteria_id'];
			$criteriapa_edit_name[0]='racriteria_id';
			$criteriapa_edit_id[0]=$racriteria_id;
			$criteriapa_sumbol[0]='=';
				
			
			if($racriteria_id=='')
			{
				$id=insert('patient_racriteria',$criteriapa_table_names,$criteriapa_parameters,'racriteria_id');
				if($id!='')
					$j++;
			}
			else
			{
				$criteriapa_update_msg=update('patient_racriteria',$criteriapa_table_names,$criteriapa_parameters,$criteriapa_edit_name,$criteriapa_edit_id,$criteriapa_sumbol);
				if ($criteriapa_update_msg=="ok")
					$j++;
			}
			
		}
		else
		{
			$i++;
			$delete_criteriapa_parameters[0]=1;
			$delete_criteriapa_names[0]='deleted';
			$delete_criteriapa_edit_name[0]='pat_id';
			$delete_criteriapa_edit_id[0]=$pat_id;
			$delete_criteriapa_sumbol[0]='=';
			$delete_criteriapa_msg=update('patient_racriteria',$delete_criteriapa_names,$delete_criteriapa_parameters,$delete_criteriapa_edit_name,$delete_criteriapa_edit_id,$delete_criteriapa_sumbol);
			if ($delete_criteriapa_msg=="ok")
				$j++;
		}
		
		if ($j==$i)
			$criteriapa_msg="ok";
		else
			$criteriapa_msg="nok";
		//criteriapa table-end
		
		//criteriaaps table-start
		$criteriaaps=$_REQUEST['criteriaaps'];
		
		$j=0;
		$i=0;
		if($criteriaaps==1)
		{
			$i++;
			$criteriaaps_parameters[0]=$pat_id;
			$criteriaaps_parameters[1]=0;
			$criteriaaps_parameters[2]=$user_id;
			$criteriaaps_parameters[3]='now()';
			$criteriaaps_parameters[4]=$_REQUEST['criteriaaps1'];
			$criteriaaps_parameters[5]=date_for_postgres($_REQUEST['criteriadateaps1']);
			$criteriaaps_parameters[6]=$_REQUEST['criteriaaps2'];
			$criteriaaps_parameters[7]=date_for_postgres($_REQUEST['criteriadateaps2']);
			$criteriaaps_parameters[8]=$_REQUEST['criteriaaps3'];
			$criteriaaps_parameters[9]=date_for_postgres($_REQUEST['criteriadateaps3']);
			$criteriaaps_parameters[10]=$_REQUEST['criteriaaps4'];
			$criteriaaps_parameters[11]=date_for_postgres($_REQUEST['criteriadateaps4']);
			$criteriaaps_parameters[12]=$_REQUEST['criteriaaps5'];
			$criteriaaps_parameters[13]=date_for_postgres($_REQUEST['criteriadateaps5']);
			$criteriaaps_parameters[14]=$_REQUEST['criteriaaps6'];
			$criteriaaps_parameters[15]=date_for_postgres($_REQUEST['criteriadateaps6']);
			$criteriaaps_parameters[16]=$_REQUEST['criteriaaps7'];
			$criteriaaps_parameters[17]=date_for_postgres($_REQUEST['criteriadateaps7']);
			$criteriaaps_parameters[18]=$_REQUEST['criteriaaps8'];
			$criteriaaps_parameters[19]=date_for_postgres($_REQUEST['criteriadateaps8']);
			
			$criteriaaps_parameters[20]=$_REQUEST['aps'];
			$criteriaaps_parameters[21]=date_for_postgres($_REQUEST['apsdate']);
			if($_REQUEST['apstotalscore']=="")
				$criteriaaps_parameters[22]=0;
			else
				$criteriaaps_parameters[22]=$_REQUEST['apstotalscore'];
			
			$criteriaaps_table_names[0]='pat_id';
			$criteriaaps_table_names[1]='deleted';
			$criteriaaps_table_names[2]='editor_id';
			$criteriaaps_table_names[3]='edit_date';
			$criteriaaps_table_names[4]='criteria1';
			$criteriaaps_table_names[5]='date1';
			$criteriaaps_table_names[6]='criteria2';
			$criteriaaps_table_names[7]='date2';
			$criteriaaps_table_names[8]='criteria3';
			$criteriaaps_table_names[9]='date3';
			$criteriaaps_table_names[10]='criteriab1';
			$criteriaaps_table_names[11]='dateb1';
			$criteriaaps_table_names[12]='criteriab2';
			$criteriaaps_table_names[13]='dateb2';
			$criteriaaps_table_names[14]='criteriab3';
			$criteriaaps_table_names[15]='dateb3';
			$criteriaaps_table_names[16]='criteriab4';
			$criteriaaps_table_names[17]='dateb4';
			$criteriaaps_table_names[18]='criteriab5';
			$criteriaaps_table_names[19]='dateb5';
			$criteriaaps_table_names[20]='aps';
			$criteriaaps_table_names[21]='apsdate';
			$criteriaaps_table_names[22]='totalscore';
			
			$patient_apscriteria_id=$_REQUEST['patient_apscriteria_id'];
			$criteriaaps_edit_name[0]='patient_apscriteria_id';
			$criteriaaps_edit_id[0]=$patient_apscriteria_id;
			$criteriaaps_sumbol[0]='=';
				
			
			if($patient_apscriteria_id=='')
			{
				$id=insert('patient_apscriteria',$criteriaaps_table_names,$criteriaaps_parameters,'patient_apscriteria_id');
				if($id!='')
					$j++;
			}
			else
			{
				$criteriaaps_update_msg=update('patient_apscriteria',$criteriaaps_table_names,$criteriaaps_parameters,$criteriaaps_edit_name,$criteriaaps_edit_id,$criteriaaps_sumbol);
				if ($criteriaaps_update_msg=="ok")
					$j++;
			}
			
		}
		else
		{
			$i++;
			$delete_criteriaaps_parameters[0]=1;
			$delete_criteriaaps_names[0]='deleted';
			$delete_criteriaaps_edit_name[0]='pat_id';
			$delete_criteriaaps_edit_id[0]=$pat_id;
			$delete_criteriaaps_sumbol[0]='=';
			$delete_criteriaaps_msg=update('patient_apscriteria',$delete_criteriaaps_names,$delete_criteriaaps_parameters,$delete_criteriaaps_edit_name,$delete_criteriaaps_edit_id,$delete_criteriaaps_sumbol);
			if ($delete_criteriaaps_msg=="ok")
				$j++;
		}
		
		if ($j==$i)
			$criteriaaps_msg="ok";
		else
			$criteriaaps_msg="nok";
		//criteriaaps table-end
		
		//criteriacaspar table-start
		$criteriacaspar=$_REQUEST['criteriacaspar'];
		
		$j=0;
		$i=0;
		if($criteriacaspar==1)
		{
			$i++;
			$criteriacaspar_parameters[0]=$pat_id;
			$criteriacaspar_parameters[1]=0;
			$criteriacaspar_parameters[2]=$user_id;
			$criteriacaspar_parameters[3]='now()';
			$criteriacaspar_parameters[4]=$_REQUEST['criteriacaspar1'];
			$criteriacaspar_parameters[5]=date_for_postgres($_REQUEST['criteriadatecaspar1']);
			$criteriacaspar_parameters[6]=$_REQUEST['criteriacaspar2'];
			$criteriacaspar_parameters[7]=date_for_postgres($_REQUEST['criteriadatecaspar2']);
			$criteriacaspar_parameters[8]=$_REQUEST['criteriacaspar3'];
			$criteriacaspar_parameters[9]=date_for_postgres($_REQUEST['criteriadatecaspar3']);
			$criteriacaspar_parameters[10]=$_REQUEST['criteriacaspar4'];
			$criteriacaspar_parameters[11]=date_for_postgres($_REQUEST['criteriadatecaspar4']);
			$criteriacaspar_parameters[12]=$_REQUEST['criteriacaspar5'];			
			$criteriacaspar_parameters[13]=date_for_postgres($_REQUEST['criteriadateaps5']);
			$criteriacaspar_parameters[14]=$_REQUEST['caspar'];
			$criteriacaspar_parameters[15]=date_for_postgres($_REQUEST['caspardate']);
			if($_REQUEST['caspartotalscore']=="")
				$criteriacaspar_parameters[16]=0;
			else
				$criteriacaspar_parameters[16]=$_REQUEST['caspartotalscore'];
			
			$criteriacaspar_table_names[0]='pat_id';
			$criteriacaspar_table_names[1]='deleted';
			$criteriacaspar_table_names[2]='editor_id';
			$criteriacaspar_table_names[3]='edit_date';
			$criteriacaspar_table_names[4]='criteria1';
			$criteriacaspar_table_names[5]='date1';
			$criteriacaspar_table_names[6]='criteria2';
			$criteriacaspar_table_names[7]='date2';
			$criteriacaspar_table_names[8]='criteria3';
			$criteriacaspar_table_names[9]='date3';
			$criteriacaspar_table_names[10]='criteria4';
			$criteriacaspar_table_names[11]='date4';
			$criteriacaspar_table_names[12]='criteria5';
			$criteriacaspar_table_names[13]='date5';
			$criteriacaspar_table_names[14]='caspar';
			$criteriacaspar_table_names[15]='caspardate';
			$criteriacaspar_table_names[16]='totalscore';
			
			$patient_casparcriteria_id=$_REQUEST['patient_casparcriteria_id'];
			$criteriacaspar_edit_name[0]='patient_casparcriteria_id';
			$criteriacaspar_edit_id[0]=$patient_casparcriteria_id;
			$criteriacaspar_sumbol[0]='=';
				
			
			if($patient_casparcriteria_id=='')
			{
				$id=insert('patient_casparcriteria',$criteriacaspar_table_names,$criteriacaspar_parameters,'patient_casparcriteria_id');
				if($id!='')
					$j++;
			}
			else
			{
				$criteriacaspar_update_msg=update('patient_casparcriteria',$criteriacaspar_table_names,$criteriacaspar_parameters,$criteriacaspar_edit_name,$criteriacaspar_edit_id,$criteriacaspar_sumbol);
				if ($criteriacaspar_update_msg=="ok")
					$j++;
			}
			
		}
		else
		{
			$i++;
			$delete_criteriacaspar_parameters[0]=1;
			$delete_criteriacaspar_names[0]='deleted';
			$delete_criteriacaspar_edit_name[0]='pat_id';
			$delete_criteriacaspar_edit_id[0]=$pat_id;
			$delete_criteriacaspar_sumbol[0]='=';
			$delete_criteriacaspar_msg=update('patient_casparcriteria',$delete_criteriacaspar_names,$delete_criteriacaspar_parameters,$delete_criteriacaspar_edit_name,$delete_criteriacaspar_edit_id,$delete_criteriacaspar_sumbol);
			if ($delete_criteriacaspar_msg=="ok")
				$j++;
		}
		
		if ($j==$i)
			$criteriacaspar_msg="ok";
		else
			$criteriacaspar_msg="nok";
		//criteriacaspar table-end
		
		//criteriaperipheral table-start
		$criteriaperipheral=$_REQUEST['criteriaperipheral'];
		
		$j=0;
		$i=0;
		if($criteriaperipheral==1)
		{
			$i++;
			$criteriaperipheral_parameters[0]=$pat_id;
			$criteriaperipheral_parameters[1]=0;
			$criteriaperipheral_parameters[2]=$user_id;
			$criteriaperipheral_parameters[3]='now()';
			$criteriaperipheral_parameters[4]=$_REQUEST['criteriaperipheral1'];
			$criteriaperipheral_parameters[5]=date_for_postgres($_REQUEST['criteriaperipheraldate1']);
			$criteriaperipheral_parameters[6]=$_REQUEST['criteriaperipheral2'];
			$criteriaperipheral_parameters[7]=date_for_postgres($_REQUEST['criteriaperipheraldate2']);
			$criteriaperipheral_parameters[8]=$_REQUEST['criteriaperipheral3'];
			$criteriaperipheral_parameters[9]=date_for_postgres($_REQUEST['criteriaperipheraldate3']);
			$criteriaperipheral_parameters[10]=$_REQUEST['criteriaperipheral4'];
			$criteriaperipheral_parameters[11]=date_for_postgres($_REQUEST['criteriaperipheraldate4']);
			$criteriaperipheral_parameters[12]=$_REQUEST['criteriaperipheral5'];
			$criteriaperipheral_parameters[13]=date_for_postgres($_REQUEST['criteriaperipheraldate5']);
			$criteriaperipheral_parameters[14]=$_REQUEST['criteriaperipheral6'];
			$criteriaperipheral_parameters[15]=date_for_postgres($_REQUEST['criteriaperipheraldate6']);
			$criteriaperipheral_parameters[16]=$_REQUEST['criteriaperipheral7'];
			$criteriaperipheral_parameters[17]=date_for_postgres($_REQUEST['criteriaperipheraldate7']);
			$criteriaperipheral_parameters[18]=$_REQUEST['criteriaperipheral8'];
			$criteriaperipheral_parameters[19]=date_for_postgres($_REQUEST['criteriaperipheraldate8']);
			$criteriaperipheral_parameters[20]=$_REQUEST['criteriaperipheral9'];
			$criteriaperipheral_parameters[21]=date_for_postgres($_REQUEST['criteriaperipheraldate9']);
			$criteriaperipheral_parameters[22]=$_REQUEST['criteriaperipheral10'];
			$criteriaperipheral_parameters[23]=date_for_postgres($_REQUEST['criteriaperipheraldate10']);
			$criteriaperipheral_parameters[24]=$_REQUEST['criteriaperipheral11'];
			$criteriaperipheral_parameters[25]=date_for_postgres($_REQUEST['criteriaperipheraldate11']);
			$criteriaperipheral_parameters[26]=$_REQUEST['criteriaperipheral12'];
			$criteriaperipheral_parameters[27]=date_for_postgres($_REQUEST['criteriaperipheraldate12']);
			$criteriaperipheral_parameters[28]=$_REQUEST['criteriaperipheral13'];
			$criteriaperipheral_parameters[29]=date_for_postgres($_REQUEST['criteriaperipheraldate13']);
			$criteriaperipheral_parameters[30]=$_REQUEST['criteriaperipheral14'];
			$criteriaperipheral_parameters[31]=date_for_postgres($_REQUEST['criteriaperipheraldate14']);
			$criteriaperipheral_parameters[32]=$_REQUEST['criteriaperipheral15'];
			$criteriaperipheral_parameters[33]=date_for_postgres($_REQUEST['criteriaperipheraldate15']);
			$criteriaperipheral_parameters[34]=$_REQUEST['criteriaperipheral16'];
			$criteriaperipheral_parameters[35]=date_for_postgres($_REQUEST['criteriaperipheraldate16']);
			$criteriaperipheral_parameters[36]=$_REQUEST['set1'];
			$criteriaperipheral_parameters[37]=date_for_postgres($_REQUEST['set1_date']);
			if($_REQUEST['set1_score']=="")
				$criteriaperipheral_parameters[38]=0;
			else
				$criteriaperipheral_parameters[38]=$_REQUEST['set1_score'];
			$criteriaperipheral_parameters[39]=$_REQUEST['set2'];
			$criteriaperipheral_parameters[40]=date_for_postgres($_REQUEST['set2_date']);
			if($_REQUEST['set2_score']=="")
				$criteriaperipheral_parameters[41]=0;
			else
				$criteriaperipheral_parameters[41]=$_REQUEST['set2_score'];
			
			$criteriaperipheral_table_names[0]='pat_id';
			$criteriaperipheral_table_names[1]='deleted';
			$criteriaperipheral_table_names[2]='editor_id';
			$criteriaperipheral_table_names[3]='edit_date';
			$criteriaperipheral_table_names[4]='criteria1';
			$criteriaperipheral_table_names[5]='date1';
			$criteriaperipheral_table_names[6]='criteria2';
			$criteriaperipheral_table_names[7]='date2';
			$criteriaperipheral_table_names[8]='criteria3';
			$criteriaperipheral_table_names[9]='date3';
			$criteriaperipheral_table_names[10]='criteria4';
			$criteriaperipheral_table_names[11]='date4';
			$criteriaperipheral_table_names[12]='criteria5';
			$criteriaperipheral_table_names[13]='date5';
			$criteriaperipheral_table_names[14]='criteria6';
			$criteriaperipheral_table_names[15]='date6';
			$criteriaperipheral_table_names[16]='criteria7';
			$criteriaperipheral_table_names[17]='date7';
			$criteriaperipheral_table_names[18]='criteria8';
			$criteriaperipheral_table_names[19]='date8';
			$criteriaperipheral_table_names[20]='criteria9';
			$criteriaperipheral_table_names[21]='date9';
			$criteriaperipheral_table_names[22]='criteria10';
			$criteriaperipheral_table_names[23]='date10';
			$criteriaperipheral_table_names[24]='criteria11';
			$criteriaperipheral_table_names[25]='date11';
			$criteriaperipheral_table_names[26]='criteria12';
			$criteriaperipheral_table_names[27]='date12';
			$criteriaperipheral_table_names[28]='criteria13';
			$criteriaperipheral_table_names[29]='date13';
			$criteriaperipheral_table_names[30]='criteria14';
			$criteriaperipheral_table_names[31]='date14';
			$criteriaperipheral_table_names[32]='criteria15';
			$criteriaperipheral_table_names[33]='date15';
			$criteriaperipheral_table_names[34]='criteria16';
			$criteriaperipheral_table_names[35]='date16';
			$criteriaperipheral_table_names[36]='set1';
			$criteriaperipheral_table_names[37]='set1_date';
			$criteriaperipheral_table_names[38]='set1_score';
			$criteriaperipheral_table_names[39]='set2';
			$criteriaperipheral_table_names[40]='set2_date';
			$criteriaperipheral_table_names[41]='set2_score';
			
			$patient_peripheralcriteria_id=$_REQUEST['patient_peripheralcriteria_id'];
			$criteriaperipheral_edit_name[0]='patient_peripheralcriteria_id';
			$criteriaperipheral_edit_id[0]=$patient_peripheralcriteria_id;
			$criteriaperipheral_sumbol[0]='=';
				
			
			if($patient_peripheralcriteria_id=='')
			{
				$id=insert('patient_peripheralcriteria',$criteriaperipheral_table_names,$criteriaperipheral_parameters,'patient_peripheralcriteria_id');
				if($id!='')
					$j++;
			}
			else
			{
				$criteriaperipheral_update_msg=update('patient_peripheralcriteria',$criteriaperipheral_table_names,$criteriaperipheral_parameters,$criteriaperipheral_edit_name,$criteriaperipheral_edit_id,$criteriaperipheral_sumbol);
				if ($criteriaperipheral_update_msg=="ok")
					$j++;
			}
			
		}
		else
		{
			$i++;
			$delete_criteriaperipheral_parameters[0]=1;
			$delete_criteriaperipheral_names[0]='deleted';
			$delete_criteriaperipheral_edit_name[0]='pat_id';
			$delete_criteriaperipheral_edit_id[0]=$pat_id;
			$delete_criteriaperipheral_sumbol[0]='=';
			$delete_criteriaperipheral_msg=update('patient_peripheralcriteria',$delete_criteriaperipheral_names,$delete_criteriaperipheral_parameters,$delete_criteriaperipheral_edit_name,$delete_criteriaperipheral_edit_id,$delete_criteriaperipheral_sumbol);
			if ($delete_criteriaperipheral_msg=="ok")
				$j++;
		}
		
		if ($j==$i)
			$criteriaperipheral_msg="ok";
		else
			$criteriaperipheral_msg="nok";
		//criteriaperipheral table-end
		
		
		//criteriaaxial table-start
		$criteriaaxial=$_REQUEST['criteriaaxial'];
		
		$j=0;
		$i=0;
		if($criteriaaxial==1)
		{
			$i++;
			$criteriaaxial_parameters[0]=$pat_id;
			$criteriaaxial_parameters[1]=0;
			$criteriaaxial_parameters[2]=$user_id;
			$criteriaaxial_parameters[3]='now()';
			$criteriaaxial_parameters[4]=$_REQUEST['criteriaaxial1'];
			$criteriaaxial_parameters[5]=date_for_postgres($_REQUEST['criteriadateaxial1']);
			$criteriaaxial_parameters[6]=$_REQUEST['criteriaaxial2'];
			$criteriaaxial_parameters[7]=date_for_postgres($_REQUEST['criteriadateaxial2']);
			$criteriaaxial_parameters[8]=$_REQUEST['criteriaaxial3'];
			$criteriaaxial_parameters[9]=date_for_postgres($_REQUEST['criteriadateaxial3']);
			$criteriaaxial_parameters[10]=$_REQUEST['criteriaaxial4'];
			$criteriaaxial_parameters[11]=date_for_postgres($_REQUEST['criteriadateaxial4']);
			$criteriaaxial_parameters[12]=$_REQUEST['criteriaaxial5'];
			$criteriaaxial_parameters[13]=date_for_postgres($_REQUEST['criteriadateaxial5']);
			$criteriaaxial_parameters[14]=$_REQUEST['criteriaaxial6'];
			$criteriaaxial_parameters[15]=date_for_postgres($_REQUEST['criteriadateaxial6']);
			$criteriaaxial_parameters[16]=$_REQUEST['criteriaaxial7'];
			$criteriaaxial_parameters[17]=date_for_postgres($_REQUEST['criteriadateaxial7']);
			$criteriaaxial_parameters[18]=$_REQUEST['criteriaaxial8'];
			$criteriaaxial_parameters[19]=date_for_postgres($_REQUEST['criteriadateaxial8']);
			$criteriaaxial_parameters[20]=$_REQUEST['criteriaaxial9'];
			$criteriaaxial_parameters[21]=date_for_postgres($_REQUEST['criteriadateaxial9']);
			$criteriaaxial_parameters[22]=$_REQUEST['criteriaaxial10'];
			$criteriaaxial_parameters[23]=date_for_postgres($_REQUEST['criteriadateaxial10']);
			$criteriaaxial_parameters[24]=$_REQUEST['criteriaaxial11'];
			$criteriaaxial_parameters[25]=date_for_postgres($_REQUEST['criteriadateaxial11']);
			$criteriaaxial_parameters[26]=$_REQUEST['criteriaaxial12'];
			$criteriaaxial_parameters[27]=date_for_postgres($_REQUEST['criteriadateaxial12']);
			$criteriaaxial_parameters[28]=$_REQUEST['criteriaaxial13'];
			$criteriaaxial_parameters[29]=date_for_postgres($_REQUEST['criteriadateaxial13']);
			$criteriaaxial_parameters[30]=$_REQUEST['criteriaaxial14'];
			$criteriaaxial_parameters[31]=date_for_postgres($_REQUEST['criteriadateaxial14']);
			$criteriaaxial_parameters[32]=$_REQUEST['criteriaaxial15'];
			$criteriaaxial_parameters[33]=date_for_postgres($_REQUEST['criteriadateaxial15']);
			$criteriaaxial_parameters[34]=$_REQUEST['criteriaaxial16'];
			$criteriaaxial_parameters[35]=date_for_postgres($_REQUEST['criteriadateaxial16']);
			$criteriaaxial_parameters[36]=$_REQUEST['newyork'];
			$criteriaaxial_parameters[37]=date_for_postgres($_REQUEST['newyork_date']);
			if($_REQUEST['newyork_score']=="")
				$criteriaaxial_parameters[38]=0;
			else
				$criteriaaxial_parameters[38]=$_REQUEST['newyork_score'];
			$criteriaaxial_parameters[39]=$_REQUEST['imaging'];
			$criteriaaxial_parameters[40]=date_for_postgres($_REQUEST['imaging_date']);
			if($_REQUEST['imaging_score']=="")
				$criteriaaxial_parameters[41]=0;
			else
				$criteriaaxial_parameters[41]=$_REQUEST['imaging_score'];
			$criteriaaxial_parameters[42]=$_REQUEST['clinical'];
			$criteriaaxial_parameters[43]=date_for_postgres($_REQUEST['clinical_date']);
			if($_REQUEST['clinical_score']=="")
				$criteriaaxial_parameters[44]=0;
			else
				$criteriaaxial_parameters[44]=$_REQUEST['clinical_score'];
			$criteriaaxial_parameters[45]=$_REQUEST['radiographic'];
			$criteriaaxial_parameters[46]=date_for_postgres($_REQUEST['radiographic_date']);
			if($_REQUEST['radiographic_score']=="")
				$criteriaaxial_parameters[47]=0;
			else
				$criteriaaxial_parameters[47]=$_REQUEST['radiographic_score'];
			
			$criteriaaxial_table_names[0]='pat_id';
			$criteriaaxial_table_names[1]='deleted';
			$criteriaaxial_table_names[2]='editor_id';
			$criteriaaxial_table_names[3]='edit_date';
			$criteriaaxial_table_names[4]='criteria1';
			$criteriaaxial_table_names[5]='date1';
			$criteriaaxial_table_names[6]='criteria2';
			$criteriaaxial_table_names[7]='date2';
			$criteriaaxial_table_names[8]='criteria3';
			$criteriaaxial_table_names[9]='date3';
			$criteriaaxial_table_names[10]='criteria4';
			$criteriaaxial_table_names[11]='date4';
			$criteriaaxial_table_names[12]='criteria5';
			$criteriaaxial_table_names[13]='date5';
			$criteriaaxial_table_names[14]='criteria6';
			$criteriaaxial_table_names[15]='date6';
			$criteriaaxial_table_names[16]='criteria7';
			$criteriaaxial_table_names[17]='date7';
			$criteriaaxial_table_names[18]='criteria8';
			$criteriaaxial_table_names[19]='date8';
			$criteriaaxial_table_names[20]='criteria9';
			$criteriaaxial_table_names[21]='date9';
			$criteriaaxial_table_names[22]='criteria10';
			$criteriaaxial_table_names[23]='date10';
			$criteriaaxial_table_names[24]='criteria11';
			$criteriaaxial_table_names[25]='date11';
			$criteriaaxial_table_names[26]='criteria12';
			$criteriaaxial_table_names[27]='date12';
			$criteriaaxial_table_names[28]='criteria13';
			$criteriaaxial_table_names[29]='date13';
			$criteriaaxial_table_names[30]='criteria14';
			$criteriaaxial_table_names[31]='date14';
			$criteriaaxial_table_names[32]='criteria15';
			$criteriaaxial_table_names[33]='date15';
			$criteriaaxial_table_names[34]='criteria16';
			$criteriaaxial_table_names[35]='date16';
			$criteriaaxial_table_names[36]='newyork';
			$criteriaaxial_table_names[37]='newyork_date';
			$criteriaaxial_table_names[38]='newyork_score';
			$criteriaaxial_table_names[39]='imaging';
			$criteriaaxial_table_names[40]='imaging_date';
			$criteriaaxial_table_names[41]='imaging_score';
			$criteriaaxial_table_names[42]='clinical';
			$criteriaaxial_table_names[43]='clinical_date';
			$criteriaaxial_table_names[44]='clinical_score';
			$criteriaaxial_table_names[45]='radiographic';
			$criteriaaxial_table_names[46]='radiographic_date';
			$criteriaaxial_table_names[47]='radiographic_score';
			
			$patient_axialcriteria_id=$_REQUEST['patient_axialcriteria_id'];
			$criteriaaxial_edit_name[0]='patient_axialcriteria_id';
			$criteriaaxial_edit_id[0]=$patient_axialcriteria_id;
			$criteriaaxial_sumbol[0]='=';
				
			
			if($patient_axialcriteria_id=='')
			{
				$id=insert('patient_axialcriteria',$criteriaaxial_table_names,$criteriaaxial_parameters,'patient_axialcriteria_id');
				if($id!='')
					$j++;
			}
			else
			{
				$criteriaaxial_update_msg=update('patient_axialcriteria',$criteriaaxial_table_names,$criteriaaxial_parameters,$criteriaaxial_edit_name,$criteriaaxial_edit_id,$criteriaaxial_sumbol);
				if ($criteriaaxial_update_msg=="ok")
					$j++;
			}
			
		}
		else
		{
			$i++;
			$delete_criteriaaxial_parameters[0]=1;
			$delete_criteriaaxial_names[0]='deleted';
			$delete_criteriaaxial_edit_name[0]='pat_id';
			$delete_criteriaaxial_edit_id[0]=$pat_id;
			$delete_criteriaaxial_sumbol[0]='=';
			$delete_criteriaaxial_msg=update('patient_axialcriteria',$delete_criteriaaxial_names,$delete_criteriaaxial_parameters,$delete_criteriaaxial_edit_name,$delete_criteriaaxial_edit_id,$delete_criteriaaxial_sumbol);
			if ($delete_criteriaaxial_msg=="ok")
				$j++;
		}
		
		if ($j==$i)
			$criteriaaxial_msg="ok";
		else
			$criteriaaxial_msg="nok";
		//criteriaaxial table-end
		
		//criteriasle table-start
		$criteriasle=$_REQUEST['criteriasle'];
		
		$j=0;
		$i=0;
		if($criteriasle==1)
		{
			$i++;
			$criteriasle_parameters[0]=$pat_id;
			$criteriasle_parameters[1]=0;
			$criteriasle_parameters[2]=$user_id;
			$criteriasle_parameters[3]='now()';
			$criteriasle_parameters[4]=$_REQUEST['criteriasle19971'];
			$criteriasle_parameters[5]=$_REQUEST['criteriasle19972'];
			$criteriasle_parameters[6]=$_REQUEST['criteriasle19973'];
			$criteriasle_parameters[7]=$_REQUEST['criteriasle19974'];
			$criteriasle_parameters[8]=$_REQUEST['criteriasle19975'];
			$criteriasle_parameters[9]=$_REQUEST['criteriasle19976'];
			$criteriasle_parameters[10]=$_REQUEST['criteriasle19977'];
			$criteriasle_parameters[11]=$_REQUEST['criteriasle19978'];
			$criteriasle_parameters[12]=$_REQUEST['criteriasle19979'];
			$criteriasle_parameters[13]=$_REQUEST['criteriasle199710'];
			$criteriasle_parameters[14]=$_REQUEST['criteriasle199711'];
			$criteriasle_parameters[15]=date_for_postgres($_REQUEST['criteriadatesle19971']);
			$criteriasle_parameters[16]=date_for_postgres($_REQUEST['criteriadatesle19972']);
			$criteriasle_parameters[17]=date_for_postgres($_REQUEST['criteriadatesle19973']);
			$criteriasle_parameters[18]=date_for_postgres($_REQUEST['criteriadatesle19974']);
			$criteriasle_parameters[19]=date_for_postgres($_REQUEST['criteriadatesle19975']);
			$criteriasle_parameters[20]=date_for_postgres($_REQUEST['criteriadatesle19976']);
			$criteriasle_parameters[21]=date_for_postgres($_REQUEST['criteriadatesle19977']);
			$criteriasle_parameters[22]=date_for_postgres($_REQUEST['criteriadatesle19978']);
			$criteriasle_parameters[23]=date_for_postgres($_REQUEST['criteriadatesle19979']);
			$criteriasle_parameters[24]=date_for_postgres($_REQUEST['criteriadatesle199710']);
			$criteriasle_parameters[25]=date_for_postgres($_REQUEST['criteriadatesle199711']);
			$criteriasle_parameters[26]=$_REQUEST['sle1997'];
			
			if($_REQUEST['sle1997totalscore']=="")
				$criteriasle_parameters[27]=0;
			else
				$criteriasle_parameters[27]=$_REQUEST['sle1997totalscore'];
			$criteriasle_parameters[28]=date_for_postgres($_REQUEST['sle1997date']);
			$criteriasle_parameters[29]=$_REQUEST['criteriasle20121'];
			$criteriasle_parameters[30]=$_REQUEST['criteriasle20122'];
			$criteriasle_parameters[31]=$_REQUEST['criteriasle20123'];
			$criteriasle_parameters[32]=$_REQUEST['criteriasle20124'];
			$criteriasle_parameters[33]=$_REQUEST['criteriasle20125'];
			$criteriasle_parameters[34]=$_REQUEST['criteriasle20126'];
			$criteriasle_parameters[35]=$_REQUEST['criteriasle20127'];
			$criteriasle_parameters[36]=$_REQUEST['criteriasle20128'];
			$criteriasle_parameters[37]=$_REQUEST['criteriasle20129'];
			$criteriasle_parameters[38]=$_REQUEST['criteriasle201210'];
			$criteriasle_parameters[39]=$_REQUEST['criteriasle201211'];
			$criteriasle_parameters[40]=$_REQUEST['criteriasle2012b1'];
			$criteriasle_parameters[41]=$_REQUEST['criteriasle2012b2'];
			$criteriasle_parameters[42]=$_REQUEST['criteriasle2012b3'];
			$criteriasle_parameters[43]=$_REQUEST['criteriasle2012b4'];
			$criteriasle_parameters[44]=$_REQUEST['criteriasle2012b5'];
			$criteriasle_parameters[45]=$_REQUEST['criteriasle2012b6'];
			$criteriasle_parameters[46]=$_REQUEST['criteriasle2012c1'];
			$criteriasle_parameters[47]=date_for_postgres($_REQUEST['criteriadatesle20121']);
			$criteriasle_parameters[48]=date_for_postgres($_REQUEST['criteriadatesle20122']);
			$criteriasle_parameters[49]=date_for_postgres($_REQUEST['criteriadatesle20123']);
			$criteriasle_parameters[50]=date_for_postgres($_REQUEST['criteriadatesle20124']);
			$criteriasle_parameters[51]=date_for_postgres($_REQUEST['criteriadatesle20125']);
			$criteriasle_parameters[52]=date_for_postgres($_REQUEST['criteriadatesle20126']);
			$criteriasle_parameters[53]=date_for_postgres($_REQUEST['criteriadatesle20127']);
			$criteriasle_parameters[54]=date_for_postgres($_REQUEST['criteriadatesle20128']);
			$criteriasle_parameters[55]=date_for_postgres($_REQUEST['criteriadatesle20129']);
			$criteriasle_parameters[56]=date_for_postgres($_REQUEST['criteriadatesle201210']);
			$criteriasle_parameters[57]=date_for_postgres($_REQUEST['criteriadatesle201211']);
			$criteriasle_parameters[58]=date_for_postgres($_REQUEST['criteriadatesle2012b1']);
			$criteriasle_parameters[59]=date_for_postgres($_REQUEST['criteriadatesle2012b2']);
			$criteriasle_parameters[60]=date_for_postgres($_REQUEST['criteriadatesle2012b3']);
			$criteriasle_parameters[61]=date_for_postgres($_REQUEST['criteriadatesle2012b4']);
			$criteriasle_parameters[62]=date_for_postgres($_REQUEST['criteriadatesle2012b5']);
			$criteriasle_parameters[63]=date_for_postgres($_REQUEST['criteriadatesle2012b6']);
			$criteriasle_parameters[64]=date_for_postgres($_REQUEST['criteriadatesle2012c1']);
			$criteriasle_parameters[65]=$_REQUEST['sle2012'];
			if($_REQUEST['sle2012totalscore']=="")
				$criteriasle_parameters[66]=0;
			else
				$criteriasle_parameters[66]=$_REQUEST['sle2012totalscore'];
			$criteriasle_parameters[67]=date_for_postgres($_REQUEST['sle2012date']);
			$criteriasle_parameters[68]=$_REQUEST['criteriasle2017a1'];
			$criteriasle_parameters[69]=$_REQUEST['criteriasle2017b1'];
			if (isset($_REQUEST['criteriasle2017b21']))
					$criteriasle2017b21="1";
				else
					$criteriasle2017b21="0";
			$criteriasle_parameters[70]=$criteriasle2017b21;
			if (isset($_REQUEST['criteriasle2017b22']))
					$criteriasle2017b22="1";
				else
					$criteriasle2017b22="0";
			$criteriasle_parameters[71]=$criteriasle2017b22;
			if (isset($_REQUEST['criteriasle2017b23']))
					$criteriasle2017b23="1";
				else
					$criteriasle2017b23="0";
			$criteriasle_parameters[72]=$criteriasle2017b23;
			$criteriasle_parameters[73]=$_REQUEST['criteriasle2017b3'];
			if (isset($_REQUEST['criteriasle2017b41']))
					$criteriasle2017b41="1";
				else
					$criteriasle2017b41="0";
			$criteriasle_parameters[74]=$criteriasle2017b41;
			if (isset($_REQUEST['criteriasle2017b42']))
					$criteriasle2017b42="1";
				else
					$criteriasle2017b42="0";
			$criteriasle_parameters[75]=$criteriasle2017b42;
			if (isset($_REQUEST['criteriasle2017b43']))
					$criteriasle2017b43="1";
				else
					$criteriasle2017b43="0";
			$criteriasle_parameters[76]=$criteriasle2017b43;
			if (isset($_REQUEST['criteriasle2017b51']))
					$criteriasle2017b51="1";
				else
					$criteriasle2017b51="0";
			$criteriasle_parameters[77]=$criteriasle2017b51;
			if (isset($_REQUEST['criteriasle2017b52']))
					$criteriasle2017b52="1";
				else
					$criteriasle2017b52="0";
			$criteriasle_parameters[78]=$criteriasle2017b52;
			if (isset($_REQUEST['criteriasle2017b61']))
					$criteriasle2017b61="1";
				else
					$criteriasle2017b61="0";
			$criteriasle_parameters[79]=$criteriasle2017b61;
			if (isset($_REQUEST['criteriasle2017b62']))
					$criteriasle2017b62="1";
				else
					$criteriasle2017b62="0";
			$criteriasle_parameters[80]=$criteriasle2017b62;
			if (isset($_REQUEST['criteriasle2017b63']))
					$criteriasle2017b63="1";
				else
					$criteriasle2017b63="0";
			$criteriasle_parameters[81]=$criteriasle2017b63;
			if (isset($_REQUEST['criteriasle2017b71']))
					$criteriasle2017b71="1";
				else
					$criteriasle2017b71="0";
			$criteriasle_parameters[82]=$criteriasle2017b71;
			if (isset($_REQUEST['criteriasle2017b72']))
					$criteriasle2017b72="1";
				else
					$criteriasle2017b72="0";
			$criteriasle_parameters[83]=$criteriasle2017b72;
			if (isset($_REQUEST['criteriasle2017b73']))
					$criteriasle2017b73="1";
				else
					$criteriasle2017b73="0";
			$criteriasle_parameters[84]=$criteriasle2017b73;
			$criteriasle_parameters[85]=$_REQUEST['criteriasle2017b8'];
			if (isset($_REQUEST['criteriasle2017b91']))
					$criteriasle2017b91="1";
				else
					$criteriasle2017b91="0";
			$criteriasle_parameters[86]=$criteriasle2017b91;
			if (isset($_REQUEST['criteriasle2017b92']))
					$criteriasle2017b92="1";
				else
					$criteriasle2017b92="0";
			$criteriasle_parameters[87]=$criteriasle2017b92;
			$criteriasle_parameters[88]=$_REQUEST['criteriasle2017b10'];
			$criteriasle_parameters[89]=date_for_postgres($_REQUEST['criteriadatesle2017a1']);
			$criteriasle_parameters[90]=date_for_postgres($_REQUEST['criteriadatesle2017b1']);
			$criteriasle_parameters[91]=date_for_postgres($_REQUEST['criteriadatesle2017b21']);
			$criteriasle_parameters[92]=date_for_postgres($_REQUEST['criteriadatesle2017b3']);
			$criteriasle_parameters[93]=date_for_postgres($_REQUEST['criteriadatesle2017b41']);
			$criteriasle_parameters[94]=date_for_postgres($_REQUEST['criteriadatesle2017b51']);
			$criteriasle_parameters[95]=date_for_postgres($_REQUEST['criteriadatesle2017b61']);
			$criteriasle_parameters[96]=date_for_postgres($_REQUEST['criteriadatesle2017b71']);
			$criteriasle_parameters[97]=date_for_postgres($_REQUEST['criteriadatesle2017b8']);
			$criteriasle_parameters[98]=date_for_postgres($_REQUEST['criteriadatesle2017b91']);
			$criteriasle_parameters[99]=date_for_postgres($_REQUEST['criteriadatesle2017b10']);
			$criteriasle_parameters[100]=$_REQUEST['sle2017'];
			if($_REQUEST['sle2017totalscore']=="")
				$criteriasle_parameters[101]=0;
			else
				$criteriasle_parameters[101]=$_REQUEST['sle2017totalscore'];
			$criteriasle_parameters[102]=date_for_postgres($_REQUEST['sle2017date']);
			
			$criteriasle_parameters[103]=date_for_postgres($_REQUEST['criteriadatesle2017b22']);
			$criteriasle_parameters[104]=date_for_postgres($_REQUEST['criteriadatesle2017b23']);
			$criteriasle_parameters[105]=date_for_postgres($_REQUEST['criteriadatesle2017b42']);
			$criteriasle_parameters[106]=date_for_postgres($_REQUEST['criteriadatesle2017b43']);
			$criteriasle_parameters[107]=date_for_postgres($_REQUEST['criteriadatesle2017b52']);
			$criteriasle_parameters[108]=date_for_postgres($_REQUEST['criteriadatesle2017b62']);
			$criteriasle_parameters[109]=date_for_postgres($_REQUEST['criteriadatesle2017b63']);
			$criteriasle_parameters[110]=date_for_postgres($_REQUEST['criteriadatesle2017b72']);
			$criteriasle_parameters[111]=date_for_postgres($_REQUEST['criteriadatesle2017b73']);
			$criteriasle_parameters[112]=date_for_postgres($_REQUEST['criteriadatesle2017b92']);
			
			$criteriasle_table_names[0]='pat_id';
			$criteriasle_table_names[1]='deleted';
			$criteriasle_table_names[2]='editor_id';
			$criteriasle_table_names[3]='edit_date';
			$criteriasle_table_names[4]='criteriasle19971';
			$criteriasle_table_names[5]='criteriasle19972';
			$criteriasle_table_names[6]='criteriasle19973';
			$criteriasle_table_names[7]='criteriasle19974';
			$criteriasle_table_names[8]='criteriasle19975';
			$criteriasle_table_names[9]='criteriasle19976';
			$criteriasle_table_names[10]='criteriasle19977';
			$criteriasle_table_names[11]='criteriasle19978';
			$criteriasle_table_names[12]='criteriasle19979';
			$criteriasle_table_names[13]='criteriasle199710';
			$criteriasle_table_names[14]='criteriasle199711';
			$criteriasle_table_names[15]='criteriadatesle19971';
			$criteriasle_table_names[16]='criteriadatesle19972';
			$criteriasle_table_names[17]='criteriadatesle19973';
			$criteriasle_table_names[18]='criteriadatesle19974';
			$criteriasle_table_names[19]='criteriadatesle19975';
			$criteriasle_table_names[20]='criteriadatesle19976';
			$criteriasle_table_names[21]='criteriadatesle19977';
			$criteriasle_table_names[22]='criteriadatesle19978';
			$criteriasle_table_names[23]='criteriadatesle19979';
			$criteriasle_table_names[24]='criteriadatesle199710';
			$criteriasle_table_names[25]='criteriadatesle199711';
			$criteriasle_table_names[26]='sle1997';
			$criteriasle_table_names[27]='sle1997totalscore';
			$criteriasle_table_names[28]='sle1997date';
			$criteriasle_table_names[29]='criteriasle20121';
			$criteriasle_table_names[30]='criteriasle20122';
			$criteriasle_table_names[31]='criteriasle20123';
			$criteriasle_table_names[32]='criteriasle20124';
			$criteriasle_table_names[33]='criteriasle20125';
			$criteriasle_table_names[34]='criteriasle20126';
			$criteriasle_table_names[35]='criteriasle20127';
			$criteriasle_table_names[36]='criteriasle20128';
			$criteriasle_table_names[37]='criteriasle20129';
			$criteriasle_table_names[38]='criteriasle201210';
			$criteriasle_table_names[39]='criteriasle201211';
			$criteriasle_table_names[40]='criteriasle2012b1';
			$criteriasle_table_names[41]='criteriasle2012b2';
			$criteriasle_table_names[42]='criteriasle2012b3';
			$criteriasle_table_names[43]='criteriasle2012b4';
			$criteriasle_table_names[44]='criteriasle2012b5';
			$criteriasle_table_names[45]='criteriasle2012b6';
			$criteriasle_table_names[46]='criteriasle2012c1';
			$criteriasle_table_names[47]='criteriadatesle20121';
			$criteriasle_table_names[48]='criteriadatesle20122';
			$criteriasle_table_names[49]='criteriadatesle20123';
			$criteriasle_table_names[50]='criteriadatesle20124';
			$criteriasle_table_names[51]='criteriadatesle20125';
			$criteriasle_table_names[52]='criteriadatesle20126';
			$criteriasle_table_names[53]='criteriadatesle20127';
			$criteriasle_table_names[54]='criteriadatesle20128';
			$criteriasle_table_names[55]='criteriadatesle20129';
			$criteriasle_table_names[56]='criteriadatesle201210';
			$criteriasle_table_names[57]='criteriadatesle201211';
			$criteriasle_table_names[58]='criteriadatesle2012b1';
			$criteriasle_table_names[59]='criteriadatesle2012b2';
			$criteriasle_table_names[60]='criteriadatesle2012b3';
			$criteriasle_table_names[61]='criteriadatesle2012b4';
			$criteriasle_table_names[62]='criteriadatesle2012b5';
			$criteriasle_table_names[63]='criteriadatesle2012b6';
			$criteriasle_table_names[64]='criteriadatesle2012c1';
			$criteriasle_table_names[65]='sle2012';
			$criteriasle_table_names[66]='sle2012totalscore';
			$criteriasle_table_names[67]='sle2012date';
			$criteriasle_table_names[68]='criteriasle2017a1';
			$criteriasle_table_names[69]='criteriasle2017b1';
			$criteriasle_table_names[70]='criteriasle2017b21';
			$criteriasle_table_names[71]='criteriasle2017b22';
			$criteriasle_table_names[72]='criteriasle2017b23';
			$criteriasle_table_names[73]='criteriasle2017b3';
			$criteriasle_table_names[74]='criteriasle2017b41';
			$criteriasle_table_names[75]='criteriasle2017b42';
			$criteriasle_table_names[76]='criteriasle2017b43';
			$criteriasle_table_names[77]='criteriasle2017b51';
			$criteriasle_table_names[78]='criteriasle2017b52';
			$criteriasle_table_names[79]='criteriasle2017b61';
			$criteriasle_table_names[80]='criteriasle2017b62';
			$criteriasle_table_names[81]='criteriasle2017b63';
			$criteriasle_table_names[82]='criteriasle2017b71';
			$criteriasle_table_names[83]='criteriasle2017b72';
			$criteriasle_table_names[84]='criteriasle2017b73';
			$criteriasle_table_names[85]='criteriasle2017b8';
			$criteriasle_table_names[86]='criteriasle2017b91';
			$criteriasle_table_names[87]='criteriasle2017b92';
			$criteriasle_table_names[88]='criteriasle2017b10';
			$criteriasle_table_names[89]='criteriadatesle2017a1';
			$criteriasle_table_names[90]='criteriadatesle2017b1';
			$criteriasle_table_names[91]='criteriadatesle2017b21';
			$criteriasle_table_names[92]='criteriadatesle2017b3';
			$criteriasle_table_names[93]='criteriadatesle2017b41';
			$criteriasle_table_names[94]='criteriadatesle2017b51';
			$criteriasle_table_names[95]='criteriadatesle2017b61';
			$criteriasle_table_names[96]='criteriadatesle2017b71';
			$criteriasle_table_names[97]='criteriadatesle2017b8';
			$criteriasle_table_names[98]='criteriadatesle2017b91';
			$criteriasle_table_names[99]='criteriadatesle2017b10';
			$criteriasle_table_names[100]='sle2017';
			$criteriasle_table_names[101]='sle2017totalscore';
			$criteriasle_table_names[102]='sle2017date';
			$criteriasle_table_names[103]='criteriadatesle2017b22';
			$criteriasle_table_names[104]='criteriadatesle2017b23';
			$criteriasle_table_names[105]='criteriadatesle2017b42';
			$criteriasle_table_names[106]='criteriadatesle2017b43';
			$criteriasle_table_names[107]='criteriadatesle2017b52';
			$criteriasle_table_names[108]='criteriadatesle2017b62';
			$criteriasle_table_names[109]='criteriadatesle2017b63';
			$criteriasle_table_names[110]='criteriadatesle2017b72';
			$criteriasle_table_names[111]='criteriadatesle2017b73';
			$criteriasle_table_names[112]='criteriadatesle2017b92';
			
			
			$patient_criteriasle_id=$_REQUEST['patient_criteriasle_id'];
			$criteriasle_edit_name[0]='patient_criteriasle_id';
			$criteriasle_edit_id[0]=$patient_criteriasle_id;
			$criteriasle_sumbol[0]='=';
				
			
			if($patient_criteriasle_id=='')
			{
				$id=insert('patient_criteriasle',$criteriasle_table_names,$criteriasle_parameters,'patient_criteriasle_id');
				if($id!='')
					$j++;
			}
			else
			{
				$criteriasle_update_msg=update('patient_criteriasle',$criteriasle_table_names,$criteriasle_parameters,$criteriasle_edit_name,$criteriasle_edit_id,$criteriasle_sumbol);
				if ($criteriasle_update_msg=="ok")
					$j++;
			}
			
		}
		else
		{
			$i++;
			$delete_criteriasle_parameters[0]=1;
			$delete_criteriasle_names[0]='deleted';
			$delete_criteriasle_edit_name[0]='pat_id';
			$delete_criteriasle_edit_id[0]=$pat_id;
			$delete_criteriasle_sumbol[0]='=';
			$delete_criteriasle_msg=update('patient_criteriasle',$delete_criteriasle_names,$delete_criteriasle_parameters,$delete_criteriasle_edit_name,$delete_criteriasle_edit_id,$delete_criteriasle_sumbol);
			if ($delete_criteriasle_msg=="ok")
				$j++;
		}
		
		if ($j==$i)
			$criteriasle_msg="ok";
		else
			$criteriasle_msg="nok";
		//criteriasle table-end
		/*echo "diagnosis_msg:$diagnosis_msg</br>";
		echo "criteriapa_msg:$criteriapa_msg</br>";
		echo "criteriaaps_msg:$criteriaaps_msg</br>";
		echo "criteriacaspar_msg:$criteriacaspar_msg</br>";
		echo "criteriaperipheral_msg:$criteriaperipheral_msg</br>";
		echo "criteriaaxial_msg:$criteriaaxial_msg</br>";
		echo "criteriasle_msg:$criteriasle_msg</br>";*/
		
		if ($diagnosis_msg=="ok" and $criteriapa_msg=="ok" and $criteriaaps_msg=="ok" and $criteriacaspar_msg=="ok" and $criteriaperipheral_msg=="ok" and $criteriaaxial_msg=="ok" and $criteriasle_msg=="ok")
		{
			$save_chk="ok";
			pg_query("COMMIT") or die("Transaction commit failed\n");
		}
		else
		{
			$save_chk="nok";
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
	}
	catch(exception $e) {
		echo "e:".$e."</br>";
		echo "ROLLBACK";
		pg_query("ROLLBACK") or die("Transaction rollback failed\n");
	}
	
}
?>
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
	  <h1>
          Diagnoses
		</h1>
		<h1>
		<?php echo "<small>Patient:".$patient_id.",".$gender_str." (".$dateofbirth.") </small>"; ?>
      </h1>
      </section>

      <!-- Main content -->
      <section class="content">
	  <div class="alert alert_suc alert-success" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Successful save!</strong>
	  </div>
	  <div class="alert alert_wr alert-danger" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Unsuccessful save!</strong>
	  </div>
	  <form id="form" name="form" action="diagnoses.php" method="POST">
	  <input type="hidden" id="save" name="save" value="1">
		<input type="hidden" id="pat_id" name="pat_id" value="<?php echo $pat_id;?>">
		<div class="row">
					<div class="form-group col-md-12">
						<input type="submit" class="btn btn-primary" value="Save">
					</div>
				</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Diagnoses</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-12" id="divdiagnosis" name="divdiagnosis">
							<?php
										$sql = get_diagnosis();
														
										while($result2 = pg_fetch_array($sql))
										{	
											$diagnosis_values .= $result2['code'];
											$diagnosis_values .= " - ";
											$diagnosis_values .= $result2['value'];
											$diagnosis_values .= "!@#$%^" ;
											
											$diagnosis_ids .=$result2['diagnosis_id'];
											$diagnosis_ids .= "!@#$%^" ;
															
											$diagnosis_counter++;
										}
									?>
										<input type="hidden" value="<?php echo $diagnosis_counter; ?>" id="diagnosis_counter" name="diagnosis_counter"/>
										<input type="hidden" value="<?php echo $diagnosis_values; ?>" id="diagnosis_values" name="diagnosis_values"/>
										<input type="hidden" value="<?php echo $diagnosis_ids; ?>" id="diagnosis_ids" name="diagnosis_ids"/>
								  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="diagnosis_tbl" name="diagnosis_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-3"><b>Diagnosis</b></th>
											<th class="col-md-1"><b>ICD10</b></th>
											<th class="col-md-1"><b>Criteria</b></th>
											<th class="col-md-1"><b>Main</b></th>
											<th class="col-md-2"><b>Date of symptom onset</b></th>
											<th class="col-md-2"><b>Date of disease diagnosis according to physician</b></th>
											<th class="col-md-1"><b>Inactive</b></th>
											<th class="col-md-1"><b>Date of inactivation</b></th>
											<th class="col-md-1">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
											$order = get_diagnoses($pat_id);
											$num_rows = pg_num_rows($order);
											
											$class_criteria1=0;
											$class_criteria2=0;
											$class_criteria3=0;
											$class_criteria4=0;
											$class_criteria5=0;
											$class_criteria6=0;
											
											while($result = pg_fetch_assoc($order))
											{
												$diagnoses_id=$result['diagnoses_id'];
												$main=$result['main'];
												$symptom_date=$result['symptom_date_str'];
												if($symptom_date=="12-12-1900")
													$symptom_date="";
												$disease_date=$result['disease_date_str'];
												if($disease_date=="12-12-1900")
													$disease_date="";
												$diagnosis_id=$result['diagnosis_id'];
												$inactive=$result['inactive'];
												$inactive_date=$result['inactive_date_str'];
												if($inactive_date=="12-12-1900")
													$inactive_date="";
												
												
											
											
									?>
										  <tr id="diagnosis_tr_<?php echo $i;?>"> 
											   <td align="center">
													<input type="hidden" id="diagnosis_hidden_<?php echo $i;?>" name="diagnosis_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="diagnoses_id_<?php echo $i; ?>" id="diagnoses_id_<?php echo $i; ?>" value="<?php echo $diagnoses_id;?>">
													<select class="form-control" name="diagnosis_id_<?php echo $i; ?>" id="diagnosis_id_<?php echo $i; ?>">
														<option value="0">--</option>
														<?php
															$sql = get_diagnosis();
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$diagn_id=$result2['diagnosis_id'];
																$value=$result2['value'];
																$code=$result2['code'];
																
																
																if($diagnosis_id==$diagn_id) 
																{
																	$icd10=$result2['icd10'];
																	$criteria_msg="";
																	
																	if($inactive==0){
																	
																		$exec3 = get_criteria($diagn_id,1);
																		$result3 = pg_fetch_array($exec3);
																		$pa=$result3['active'];
																		
																		$exec3 = get_criteria($diagn_id,2);
																		$result3 = pg_fetch_array($exec3);
																		$peripheral=$result3['active'];
																		
																		$exec3 = get_criteria($diagn_id,3);
																		$result3 = pg_fetch_array($exec3);
																		$axial=$result3['active'];
																		
																		$exec3 = get_criteria($diagn_id,4);
																		$result3 = pg_fetch_array($exec3);
																		$caspar=$result3['active'];
																		
																		$exec3 = get_criteria($diagn_id,5);
																		$result3 = pg_fetch_array($exec3);
																		$sle=$result3['active'];
																		
																		$exec3 = get_criteria($diagn_id,6);
																		$result3 = pg_fetch_array($exec3);
																		$aps=$result3['active'];
																		
																		if($pa==1)
																		{
																			$criteria_msg="RΑ";
																			$class_criteria1++;
																		}
																		
																		if($peripheral==1)
																		{
																			if($criteria_msg<>"")
																				$criteria_msg .=" - Peripheral ";
																			else
																				$criteria_msg =" Peripheral ";
																			
																			$class_criteria2++;
																		}
																		
																		if($axial==1)
																		{
																			if($criteria_msg<>"")
																				$criteria_msg .=" - Axial ";
																			else
																				$criteria_msg =" Axial ";
																			$class_criteria3++;
																		}
																		
																		if($caspar==1)
																		{
																			if($criteria_msg<>"")
																				$criteria_msg .=" - Caspar ";
																			else
																				$criteria_msg =" Caspar ";
																			$class_criteria4++;
																		}
																		
																		if($sle==1)
																		{
																			if($criteria_msg<>"")
																				$criteria_msg .=" - SLE ";
																			else
																				$criteria_msg =" SLE ";
																			$class_criteria5++;
																		}
																		
																		if($aps==1)
																		{
																			if($criteria_msg<>"")
																				$criteria_msg .=" - APS ";
																			else
																				$criteria_msg =" APS ";
																			$class_criteria6++;
																		}
																	}
																}
																
														?>
														<option value="<?php echo $diagn_id; ?>" <?php if($diagnosis_id==$diagn_id) { echo "selected"; } ?>><?php echo $code." - ".$value; ?></option>
														<?php
															}
														?>
													</select>
												</td>
												<td align="center">
													<textarea class="form-control" readonly><?php echo $icd10; ?></textarea>
												</td>
												<td align="center">
													<textarea class="form-control" readonly><?php echo $criteria_msg; ?></textarea>
												</td>
												<td align="center">
													<input type="checkbox" onchange="setmain(<?php echo $i; ?>)" id="main_<?php echo $i; ?>" name="main_<?php echo $i; ?>" <?php if ($main==1) { echo "checked"; } ?>>
												</td>
												<td align="center">
													<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="symptom_date_<?php echo $i; ?>" name="symptom_date_<?php echo $i; ?>" value="<?php echo $symptom_date; ?>">
												</td>
												<td align="center">
													<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="disease_date_<?php echo $i; ?>" name="disease_date_<?php echo $i; ?>" value="<?php echo $disease_date; ?>">
												</td>
												<td align="center">
													<input type="checkbox" onchange="setmain(<?php echo $i; ?>)" id="inactive_<?php echo $i; ?>" name="inactive_<?php echo $i; ?>" <?php if ($inactive==1) { echo "checked"; } ?>>
												</td>
												<td align="center">
													<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="inactive_date_<?php echo $i; ?>" name="inactive_date_<?php echo $i; ?>" value="<?php echo $inactive_date; ?>">
												</td>
													<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="diagnosis_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
												  </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										
									?>
									</tbody>
								</table>
								<input type="hidden" name="diagnosis_numofrows" id="diagnosis_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								 <a class="btn btn-primary" href="javascript:void(0);" onclick="diagnosis_addrow();"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
					</div>
				</div><!-- diagnoses status div -->
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Classification criteria</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-12" id="divcriteria" name="divcriteria">
							<?php
								if($class_criteria1==0 and $class_criteria2==0 and $class_criteria3==0 and $class_criteria4==0 and $class_criteria5==0 and $class_criteria6==0)
								{
							?>
								<label>There are no diagnoses with classification criteria</label>
							<?php
								}
								
							?>
								<input type="hidden" class="form-control" id="criteriapa" name="criteriapa" value="<?php if($class_criteria1>0) { echo "1";} else { echo "0"; } ?>">
								<input type="hidden" class="form-control" id="criteriaperipheral" name="criteriaperipheral" value="<?php if($class_criteria2>0) { echo "1";} else { echo "0"; } ?>">
								<input type="hidden" class="form-control" id="criteriaaxial" name="criteriaaxial" value="<?php if($class_criteria3>0) { echo "1";} else { echo "0"; } ?>">
								<input type="hidden" class="form-control" id="criteriacaspar" name="criteriacaspar" value="<?php if($class_criteria4>0) { echo "1";} else { echo "0"; } ?>">
								<input type="hidden" class="form-control" id="criteriasle" name="criteriasle" value="<?php if($class_criteria5>0) { echo "1";} else { echo "0"; } ?>">
								<input type="hidden" class="form-control" id="criteriaaps" name="criteriaaps" value="<?php if($class_criteria6>0) { echo "1";} else { echo "0"; } ?>">
								<div id="accordion">
								<?php
											
								if($class_criteria1>0)
								{
									$order = get_racriteria($pat_id);
									$result2 = pg_fetch_assoc($order);
									$racriteria_id=$result2['racriteria_id'];
									$criteria1=$result2['criteria1'];
									$date1=$result2['date1_str'];
									if($date1=="12-12-1900")
										$date1="";
									$criteria2=$result2['criteria2'];
									$date2=$result2['date2_str'];
									if($date2=="12-12-1900")
										$date2="";
									$criteria3=$result2['criteria3'];
									$date3=$result2['date3_str'];
									if($date3=="12-12-1900")
										$date3="";
									$criteriab1=$result2['criteriab1'];
									$dateb1=$result2['dateb1_str'];
									if($dateb1=="12-12-1900")
										$dateb1="";
									$criteriab2=$result2['criteriab2'];
									$dateb2=$result2['dateb2_str'];
									if($dateb2=="12-12-1900")
										$dateb2="";
									$criteriab3=$result2['criteriab3'];
									$dateb3=$result2['dateb3_str'];
									if($dateb3=="12-12-1900")
										$dateb3="";
									$criteriab4=$result2['criteriab4'];
									$dateb4=$result2['dateb4_str'];
									if($dateb4=="12-12-1900")
										$dateb4="";
									$rheumatoid=$result2['rheumatoid'];
									$rheumatoiddate=$result2['rheumatoiddate_str'];
									if($rheumatoiddate=="12-12-1900")
										$rheumatoiddate="";
									$totalscore=$result2['totalscore'];
									
									$msg="";
									
									if($rheumatoid=="Yes")
										$msg="Clasiffied as RA ";
									if($rheumatoiddate<>"")
										$msg .= " at $rheumatoiddate ";
									if($totalscore<>"0" and $totalscore<>"" )
										$msg .= " with score $totalscore";
									
								?>
									
									<label>RA(ACR/EULAR 2010 RA Classification Criteria)
										<b><div id="divraclassified" name="divraclassified" <?php if($msg=="") { echo 'style="DISPLAY:none;"'; } else  { echo 'style="DISPLAY:block;"'; } ?>><?php echo $msg; ?></div></b>
									</label>
								  <div class="row">
									<div class="col-md-12">
										<div class="row">
											<div class="form-group col-md-8" id="divcriteria3" name="divcriteria3">
											  <label>A. New patient</label>
											  <select class="form-control" name="criteria3" id="criteria3">
												<option value="0" <?php if ($criteria3==0) { echo "selected"; } ?>>--</option>
												<option value="1" <?php if ($criteria3==1) { echo "selected"; } ?>>Yes</option>
												<option value="2" <?php if ($criteria3==2) { echo "selected"; } ?>>No</option>
												<option value="3" <?php if ($criteria3==3) { echo "selected"; } ?>>Don't know</option>
											  </select>
											</div>
											  <!--<div  class="form-group col-md-4" id="divdate3" name="divdate3" <?php if($criteria3==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
												<label>Date</label>
												<div class="input-group date">
												  <div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												  </div>
												  <input type="text" class="form-control" id="date3" name="date3" value="<?php echo $date3; ?>" readonly onclick="javascript:newCalendar(this);">
												</div>
											  </div>-->
										</div>
										<div id="divnewlycriteria" name="divnewlycriteria"  <?php if($criteria3==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
											<!--<label>B. Additional criteria in newly presenting patients:</label>-->
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriab1" name="divcriteriab1">
												  <label><span data-tooltip="Any swollen or tender joint on examination, which may be confirmed by imaging evidence of synovitis. Distal interphalangeal (DIP) joints, 1st carpometacarpal joints, and 1st metatarsophalangeal (MTP) joints are excluded from assessment. “Large joints” refers to shoulders, elbows, hips, knees, and ankles.“Small joints” refers to the metacarpophalangeal joints, proximal IP joints, 2nd through 5th MTP joints, thumb IP joints, and wrists.In the last category, at least 1 of the involved joints must be a small joint; the other joints can include any combination of large and additional small joints, as well as other joints not specifically listed elsewhere (e.g., temporomandibular, acromioclavicular, sternoclavicular, etc.)." data-tooltip-position="top">1. Joint involvement</span></label>
												  <select class="form-control" name="criteriab1" id="criteriab1">
													<option value="0" <?php if ($criteriab1==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriab1==1) { echo "selected"; } ?>>1 large joint (0 point)</option>
													<option value="2" <?php if ($criteriab1==2) { echo "selected"; } ?>>2-10 large joint (1 point)</option>
													<option value="3" <?php if ($criteriab1==3) { echo "selected"; } ?>>1-3 small joints (with or without involvement of large joints) (2 points)</option>
													<option value="4" <?php if ($criteriab1==4) { echo "selected"; } ?>>4-10 small joints (with or without involvement of large joints) (3 points)</option>
													<option value="5" <?php if ($criteriab1==5) { echo "selected"; } ?>>>10 joints (at least 1 small joint) (5 points)</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateb1" name="divdateb1" <?php if($criteriab1==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="dateb1" name="dateb1" value="<?php echo $dateb1; ?>">
													 </div>
													 <a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageb1" name="calcageb1">set date</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriab2" name="divcriteriab2">
												  <label><span data-tooltip="At least 1 test result is needed for classification. Low-positive refers to IU values that are higher than the upper limit of normal (ULN) for the laboratory and assay but  ≤ 3 times the ULN; high-positive refers to IU values that are > 3 times the ULN. Where rheumatoid factor (RF) information is only available as positive or negative (qualitative assessment), a positive result should be scored as low-positive for RF." data-tooltip-position="top">2. Serology</span></label>
												  <select class="form-control" name="criteriab2" id="criteriab2">
													<option value="0" <?php if ($criteriab2==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriab2==1) { echo "selected"; } ?>>Negative RF and negative ACPA (0 point)</option>
													<option value="2" <?php if ($criteriab2==2) { echo "selected"; } ?>>Low-positive RF or low-positive ACPA (≤3 times ULN) (2 points) </option>
													<option value="3" <?php if ($criteriab2==3) { echo "selected"; } ?>>High-positive RF or high-positive ACPA (>3 times ULN) (3 points) </option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateb2" name="divdateb2" <?php if($criteriab2==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="dateb2" name="dateb2" value="<?php echo $dateb2; ?>" >
													 </div>
													 <a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageb2" name="calcageb2">set date</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriab3" name="divcriteriab3">
												  <label><span data-tooltip="At least 1 test result is needed for classification.Normal/abnormal is determined by local laboratory standards." data-tooltip-position="top">3. Acute phase reactants</span></label>
												  <select class="form-control" name="criteriab3" id="criteriab3">
													<option value="0" <?php if ($criteriab3==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriab3==1) { echo "selected"; } ?>>Normal CRP and normal ESR (0 point)</option>
													<option value="2" <?php if ($criteriab3==2) { echo "selected"; } ?>>Abnormal CRP or abnormal ESR (1 point)</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateb3" name="divdateb3" <?php if($criteriab3==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="dateb3" name="dateb3" value="<?php echo $dateb3; ?>" >
													</div>
													<!-- /.input group -->
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageb3" name="calcageb3">set date</a>
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriab4" name="divcriteriab4">
												  <label><span data-tooltip="Duration of symptoms refers to patient self-report of the duration of signs or symptoms of synovitis(e.g., pain, swelling, tenderness) of joints that are clinically involved at the time of assessment, regardless of treatment status." data-tooltip-position="top">4. Duration of symptoms</span></label>
												  <select class="form-control" name="criteriab4" id="criteriab4">
													<option value="0" <?php if ($criteriab4==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriab4==1) { echo "selected"; } ?>><6 weeks (0 point)</option>
													<option value="2" <?php if ($criteriab4==2) { echo "selected"; } ?>>≥6 weeks (1 point)</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateb4" name="divdateb4" <?php if($criteriab4==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="dateb4" name="dateb4" value="<?php echo $dateb4; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageb4" name="calcageb4">set date</a>
													<!-- /.input group -->
												  </div>
											</div>
										</div>
										<label>B. Old patient</label>
										<div class="row">
											<div class="form-group col-md-8" id="divcriteria1" name="divcriteria1">
											  <label>1. Patient with erosive disease typical of RA with a history compatible with prior fulfillment of the 2010 criteria</label>
											  <input type="hidden" value="<?php echo $racriteria_id; ?>" id="racriteria_id" name="racriteria_id"/>
											  <select class="form-control" name="criteria1" id="criteria1">
												<option value="0" <?php if ($criteria1==0) { echo "selected"; } ?>>--</option>
												<option value="1" <?php if ($criteria1==1) { echo "selected"; } ?>>Yes</option>
												<option value="2" <?php if ($criteria1==2) { echo "selected"; } ?>>No</option>
												<option value="3" <?php if ($criteria1==3) { echo "selected"; } ?>>Don't know</option>
											  </select>
											</div>
											  <div  class="form-group col-md-4" id="divdate1" name="divdate1" <?php if($criteria1==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
												<label>Date</label>
												<div class="input-group date">
												  <div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												  </div>
												  <input type="text" class="form-control" id="date1" name="date1" value="<?php echo $date1; ?>" >
												</div>
												<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcage1" name="calcage1">set date</a>
												<!-- /.input group -->
											  </div>
										</div>
										<div class="row">
											<div class="form-group col-md-8" id="divcriteria2" name="divcriteria2">
											  <label>2. Patient with long-standing disease, even if inactive (with or without treatment) who, based on retrospectively available data, has previously fulfilled the 2010 criteria</label>
											  <select class="form-control" name="criteria2" id="criteria2">
												<option value="0" <?php if ($criteria2==0) { echo "selected"; } ?>>--</option>
												<option value="1" <?php if ($criteria2==1) { echo "selected"; } ?>>Yes</option>
												<option value="2" <?php if ($criteria2==2) { echo "selected"; } ?>>No</option>
												<option value="3" <?php if ($criteria2==3) { echo "selected"; } ?>>Don't know</option>
											  </select>
											</div>
											  <div  class="form-group col-md-4" id="divdate2" name="divdate2" <?php if($criteria2==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
												<label>Date</label>
												<div class="input-group date">
												  <div class="input-group-addon">
													<i class="fa fa-calendar"></i>
												  </div>
												  <input type="text" class="form-control" id="date2" name="date2" value="<?php echo $date2; ?>" >
												</div>
												<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcage2" name="calcage2">set date</a>
												<!-- /.input group -->
											  </div>
										</div>
										<div class="row">
											<div class="form-group col-md-4" id="divtotalscore" name="divtotalscore">
											  <label><span>Rheumatoid arthritis: </span><input type="text" readonly id="rheumatoid" name="rheumatoid" value="<?php echo $rheumatoid; ?>"></label>
											 </div>
											 <div class="form-group col-md-4" id="divtotalscore" name="divtotalscore">
											  <label><span>Diagnosis date: </span><input type="text" readonly id="rheumatoiddate" name="rheumatoiddate" value="<?php echo $rheumatoiddate; ?>"></label>
											  </div>
											 <div class="form-group col-md-4" id="divtotalscore" name="divtotalscore">
											  <label><span>Total score: </span><input type="text" readonly id="totalscore" name="totalscore" value="<?php echo $totalscore; ?>"></label>
											</div>
										</div>
									</div>
								</div>
								<?php
								}
								
								if($class_criteria2>0)
								{
									$order = get_peripheralcriteria($pat_id);
									$result2 = pg_fetch_assoc($order);
									$patient_peripheralcriteria_id=$result2['patient_peripheralcriteria_id'];
									$criteriaperipheral1=$result2['criteria1'];
									$criteriaperipheraldate1_str=$result2['date1_str'];
									if($criteriaperipheraldate1_str=="12-12-1900")
										$criteriaperipheraldate1_str="";
									$criteriaperipheral2=$result2['criteria2'];
									$criteriaperipheraldate2_str=$result2['date2_str'];
									if($criteriaperipheraldate2_str=="12-12-1900")
										$criteriaperipheraldate2_str="";
									$criteriaperipheral3=$result2['criteria3'];
									$criteriaperipheraldate3_str=$result2['date3_str'];
									if($criteriaperipheraldate3_str=="12-12-1900")
										$criteriaperipheraldate3_str="";
									$criteriaperipheral4=$result2['criteria4'];
									$criteriaperipheraldate4_str=$result2['date4_str'];
									if($criteriaperipheraldate4_str=="12-12-1900")
										$criteriaperipheraldate4_str="";
									$criteriaperipheral5=$result2['criteria5'];
									$criteriaperipheraldate5_str=$result2['date5_str'];
									if($criteriaperipheraldate5_str=="12-12-1900")
										$criteriaperipheraldate5_str="";
									$criteriaperipheral6=$result2['criteria6'];
									$criteriaperipheraldate6_str=$result2['date6_str'];
									if($criteriaperipheraldate6_str=="12-12-1900")
										$criteriaperipheraldate6_str="";
									$criteriaperipheral7=$result2['criteria7'];
									$criteriaperipheraldate7_str=$result2['date7_str'];
									if($criteriaperipheraldate7_str=="12-12-1900")
										$criteriaperipheraldate7_str="";
									$criteriaperipheral8=$result2['criteria8'];
									$criteriaperipheraldate8_str=$result2['date8_str'];
									if($criteriaperipheraldate8_str=="12-12-1900")
										$criteriaperipheraldate8_str="";
									$criteriaperipheral9=$result2['criteria9'];
									$criteriaperipheraldate9_str=$result2['date9_str'];
									if($criteriaperipheraldate9_str=="12-12-1900")
										$criteriaperipheraldate9_str="";
									$criteriaperipheral10=$result2['criteria10'];
									$criteriaperipheraldate10_str=$result2['date10_str'];
									if($criteriaperipheraldate10_str=="12-12-1900")
										$criteriaperipheraldate10_str="";
									$criteriaperipheral11=$result2['criteria11'];
									$criteriaperipheraldate11_str=$result2['date11_str'];
									if($criteriaperipheraldate11_str=="12-12-1900")
										$criteriaperipheraldate11_str="";
									$criteriaperipheral12=$result2['criteria12'];
									$criteriaperipheraldate12_str=$result2['date12_str'];
									if($criteriaperipheraldate12_str=="12-12-1900")
										$criteriaperipheraldate12_str="";
									$criteriaperipheral13=$result2['criteria13'];
									$criteriaperipheraldate13_str=$result2['date13_str'];
									if($criteriaperipheraldate13_str=="12-12-1900")
										$criteriaperipheraldate13_str="";
									$criteriaperipheral14=$result2['criteria14'];
									$criteriaperipheraldate14_str=$result2['date14_str'];
									if($criteriaperipheraldate14_str=="12-12-1900")
										$criteriaperipheraldate14_str="";
									$criteriaperipheral15=$result2['criteria15'];
									$criteriaperipheraldate15_str=$result2['date15_str'];
									if($criteriaperipheraldate15_str=="12-12-1900")
										$criteriaperipheraldate15_str="";
									$criteriaperipheral16=$result2['criteria16'];
									$criteriaperipheraldate16_str=$result2['date16_str'];
									if($criteriaperipheraldate16_str=="12-12-1900")
										$criteriaperipheraldate16_str="";
									$set1=$result2['set1'];
									$set1_date=$result2['set1_date_str'];
									if($set1_date=="12-12-1900")
										$set1_date="";
									$set1_score=$result2['set1_score'];
									$set2=$result2['set2'];
									$set2_date=$result2['set2_date_str'];
									if($set2_date=="12-12-1900")
										$set2_date="";
									$set2_score=$result2['set2_score'];
									
									$msg_peripheral="";
									$msg_peripheral2="";
									
									if($set1=="Yes" or $set2=="Yes")
										$msg_peripheral="Clasiffied as Peripheral SpA";
									if($set1=="Yes")
										$msg_peripheral .=" </br> according to Set1";
									
									if($set1_date<>"")
										$msg_peripheral .= " at $set1_date ";
									if($set1_score<>"" and $set1_score<>"0")
										$msg_peripheral .= " with score $set1_score";
									if($set2=="Yes")
										$msg_peripheral2 .=" according to Set2";
									if($set2_date<>"")
										$msg_peripheral2 .= " at $set2_date ";
									if($set2_score<>"" and $set2_score<>"0")
										$msg_peripheral2 .= " with score $set2_score";
								?>
								  <label>Peripheral SpA Criteria
									<b><div id="divperipheralclassified" name="divperipheralclassified" <?php if($msg_peripheral=="") { echo 'style="DISPLAY:none;"'; } else  { echo 'style="DISPLAY:block;"'; } ?>><?php echo $msg_peripheral; ?></div></b>
									<b><div id="divperipheralclassified2" name="divperipheralclassified2" <?php if($msg_peripheral2=="") { echo 'style="DISPLAY:none;"'; } else  { echo 'style="DISPLAY:block;"'; } ?>><?php echo $msg_peripheral2; ?></div></b>
								  </label>
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="form-group col-md-8" id="divperipheralcriteria1" name="divperipheralcriteria1">
													<input type="hidden" value="<?php echo $patient_peripheralcriteria_id; ?>" id="patient_peripheralcriteria_id" name="patient_peripheralcriteria_id"/>
												  <label><span data-tooltip="Currently no symptoms of spinal pain in back, dorsal, or cervical region, with at least 4/5 of the following: (1) age at onset < 40 years; (2) insidious onset; (3) improvement with exercise; (4) no improvement with rest; (5) pain at night (with improvement upon getting up)" data-tooltip-position="top">1.No current (predominant) inflammatory back pain</span></label>
												  <select class="form-control" name="criteriaperipheral1" id="criteriaperipheral1">
													<option value="0" <?php if ($criteriaperipheral1==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaperipheral1==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaperipheral1==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaperipheral1==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateperipheral1" name="divdateperipheral1" <?php if($criteriaperipheral1==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriaperipheraldate1" name="criteriaperipheraldate1" value="<?php echo $criteriaperipheraldate1_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral1" name="calcageperipheral1">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral21" name="calcageperipheral21">set date2</a>
													<!-- /.input group -->
												  </div>
												</div>
											<div class="row">
												<div class="form-group col-md-8" id="divperipheralcriteria2" name="divperipheralcriteria2">
												  <label><span data-tooltip="History of inflammatory spinal pain in back, dorsal, or cervical region, as defined by ASAS (Criterion No.1) in the past." data-tooltip-position="top">2. Inflammatory back pain in the past</span></label>
												  <select class="form-control" name="criteriaperipheral2" id="criteriaperipheral2">
													<option value="0" <?php if ($criteriaperipheral2==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaperipheral2==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaperipheral2==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaperipheral2==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateperipheral2" name="divdateperipheral2" <?php if($criteriaperipheral2==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriaperipheraldate2" name="criteriaperipheraldate2" value="<?php echo $criteriaperipheraldate2_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral2" name="calcageperipheral2">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral22" name="calcageperipheral22">set date2</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divperipheralcriteria3" name="divperipheralcriteria3">
												  <label><span data-tooltip="Current peripheral arthritis compatible with SpA (usually asymmetric and/or predominant involvement of the lower limb), diagnosed clinically by a doctor" data-tooltip-position="top">3. Current peripheral arthritis</span></label>
												  <select class="form-control" name="criteriaperipheral3" id="criteriaperipheral3">
													<option value="0" <?php if ($criteriaperipheral3==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaperipheral3==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaperipheral3==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaperipheral3==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateperipheral3" name="divdateperipheral3" <?php if($criteriaperipheral3==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriaperipheraldate3" name="criteriaperipheraldate3" value="<?php echo $criteriaperipheraldate3_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral3" name="calcageperipheral3">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral23" name="calcageperipheral23">set date2</a>
													<!-- /.input group -->
												  </div>
												  </div>
											<div class="row">
												<div class="form-group col-md-8" id="divperipheralcriteria4" name="divperipheralcriteria4">
												  <label><span data-tooltip="Past or present peripheral synovitis compatible with SpA (usually asymmetric and/or predominant involvement of the lower limb) diagnosed clinically by a doctor" data-tooltip-position="top">4. Past or present peripheral arthritis</span></label>
												  <select class="form-control" name="criteriaperipheral4" id="criteriaperipheral4">
													<option value="0" <?php if ($criteriaperipheral4==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaperipheral4==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaperipheral4==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaperipheral4==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateperipheral4" name="divdateperipheral4" <?php if($criteriaperipheral4==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriaperipheraldate4" name="criteriaperipheraldate4" value="<?php echo $criteriaperipheraldate4_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral4" name="calcageperipheral4">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral24" name="calcageperipheral24">set date2</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divperipheralcriteria5" name="divperipheralcriteria5">
												  <label><span data-tooltip="Current enthesitis at any site, diagnosed clinically by a doctor" data-tooltip-position="top">5. Current enthesitis at any site</span></label>
												  <select class="form-control" name="criteriaperipheral5" id="criteriaperipheral5">
													<option value="0" <?php if ($criteriaperipheral5==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaperipheral5==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaperipheral5==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaperipheral5==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateperipheral5" name="divdateperipheral5" <?php if($criteriaperipheral5==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriaperipheraldate5" name="criteriaperipheraldate5" value="<?php echo $criteriaperipheraldate5_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral5" name="calcageperipheral5">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral25" name="calcageperipheral25">set date2</a>
													<!-- /.input group -->
												  </div>
												  </div>
											<div class="row">
												<div class="form-group col-md-8" id="divperipheralcriteria6" name="divperipheralcriteria6">
												  <label><span data-tooltip="Past or present spontaneous pain or tenderness at examination of any of the entheses" data-tooltip-position="top">6. Past or present enthesitis at any site</span></label>
												  <select class="form-control" name="criteriaperipheral6" id="criteriaperipheral6">
													<option value="0" <?php if ($criteriaperipheral6==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaperipheral6==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaperipheral6==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaperipheral6==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateperipheral6" name="divdateperipheral6" <?php if($criteriaperipheral6==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriaperipheraldate6" name="criteriaperipheraldate6" value="<?php echo $criteriaperipheraldate6_str; ?>">
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral6" name="calcageperipheral6">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral26" name="calcageperipheral26">set date2</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divperipheralcriteria7" name="divperipheralcriteria7">
												  <label><span data-tooltip="Current dactylitis, diagnosed clinically by a doctor" data-tooltip-position="top">7. Current dactylitis</span></label>
												  <select class="form-control" name="criteriaperipheral7" id="criteriaperipheral7">
													<option value="0" <?php if ($criteriaperipheral7==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaperipheral7==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaperipheral7==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaperipheral7==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateperipheral7" name="divdateperipheral7" <?php if($criteriaperipheral7==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriaperipheraldate7" name="criteriaperipheraldate7" value="<?php echo $criteriaperipheraldate7_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral7" name="calcageperipheral7">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral27" name="calcageperipheral27">set date2</a>
													<!-- /.input group -->
												  </div>
												  </div>
											<div class="row">
												<div class="form-group col-md-8" id="divperipheralcriteria8" name="divperipheralcriteria8">
												  <label><span data-tooltip="Swelling of an entire digit on current or past physical examination" data-tooltip-position="top">8. Past or present dactylitis</span></label>
												  <select class="form-control" name="criteriaperipheral8" id="criteriaperipheral8">
													<option value="0" <?php if ($criteriaperipheral8==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaperipheral8==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaperipheral8==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaperipheral8==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateperipheral8" name="divdateperipheral8" <?php if($criteriaperipheral8==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriaperipheraldate8" name="criteriaperipheraldate8" value="<?php echo $criteriaperipheraldate8_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral8" name="calcageperipheral8">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral28" name="calcageperipheral28">set date2</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divperipheralcriteria9" name="divperipheralcriteria9">
												  <label><span data-tooltip="Past or present psoriasis, diagnosed by a doctor" data-tooltip-position="top">9. Past or present psoriasis</span></label>
												  <select class="form-control" name="criteriaperipheral9" id="criteriaperipheral9">
													<option value="0" <?php if ($criteriaperipheral9==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaperipheral9==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaperipheral9==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaperipheral9==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateperipheral9" name="divdateperipheral9" <?php if($criteriaperipheral9==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriaperipheraldate9" name="criteriaperipheraldate9" value="<?php echo $criteriaperipheraldate9_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral9" name="calcageperipheral9">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral29" name="calcageperipheral29">set date2</a>
													<!-- /.input group -->
												  </div>
												  </div>
											<div class="row">
												<div class="form-group col-md-8" id="divperipheralcriteria10" name="divperipheralcriteria10">
												  <label><span data-tooltip="Past or present Crohn disease or ulcerative colitis diagnosed by a doctor and confirmed by radiographic examination or endoscopy" data-tooltip-position="top">10. Past or present Inflammatory Bowel Disease</span></label>
												  <select class="form-control" name="criteriaperipheral10" id="criteriaperipheral10">
													<option value="0" <?php if ($criteriaperipheral10==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaperipheral10==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaperipheral10==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaperipheral10==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateperipheral10" name="divdateperipheral10" <?php if($criteriaperipheral10==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriaperipheraldate10" name="criteriaperipheraldate10" value="<?php echo $criteriaperipheraldate10_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral10" name="calcageperipheral10">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral21." name="calcageperipheral210">set date2</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divperipheralcriteria11" name="divperipheralcriteria11">
												  <label><span data-tooltip="Past or present uveitis anterior, confirmed by an ophthalmologist" data-tooltip-position="top">11. Past or present acute anterior uveitis</span></label>
												  <select class="form-control" name="criteriaperipheral11" id="criteriaperipheral11">
													<option value="0" <?php if ($criteriaperipheral11==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaperipheral11==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaperipheral11==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaperipheral11==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateperipheral11" name="divdateperipheral11" <?php if($criteriaperipheral11==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriaperipheraldate11" name="criteriaperipheraldate11" value="<?php echo $criteriaperipheraldate11_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral11" name="calcageperipheral11">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral211" name="calcageperipheral211">set date2</a>
													<!-- /.input group -->
												  </div>
												  </div>
											<div class="row">
												<div class="form-group col-md-8" id="divperipheralcriteria12" name="divperipheralcriteria2">
												  <label><span data-tooltip="Urethritis/cervicitis or diarrhoea within 1 month before the onset of arthritis or enthesitis or dactylitis" data-tooltip-position="top">12. History of recent urogenital or gastrointestinal infection</span></label>
												  <select class="form-control" name="criteriaperipheral12" id="criteriaperipheral12">
													<option value="0" <?php if ($criteriaperipheral12==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaperipheral12==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaperipheral12==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaperipheral12==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateperipheral12" name="divdateperipheral12" <?php if($criteriaperipheral12==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriaperipheraldate12" name="criteriaperipheraldate12" value="<?php echo $criteriaperipheraldate12_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral12" name="calcageperipheral12">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral212" name="calcageperipheral212">set date2</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divperipheralcriteria13" name="divperipheralcriteria13">
												  <label><span data-tooltip="Presence in 1st-degree (mother, father, sisters, brothers, children) or 2nd-degree (maternal and paternal grandparents, aunts, uncles, nieces and nephews) relatives of any of the following: (1) ankylosing spondylitis, (2) psoriasis, (3) acute uveitis, (4) reactive arthritis, (5) IBD" data-tooltip-position="top">13. Family history for SpA </span></label>
												  <select class="form-control" name="criteriaperipheral13" id="criteriaperipheral13">
													<option value="0" <?php if ($criteriaperipheral13==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaperipheral13==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaperipheral13==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaperipheral13==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateperipheral13" name="divdateperipheral13" <?php if($criteriaperipheral13==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriaperipheraldate13" name="criteriaperipheraldate13" value="<?php echo $criteriaperipheraldate13_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral13" name="calcageperipheral13">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral213" name="calcageperipheral213">set date2</a>
													<!-- /.input group -->
												  </div>
												  </div>
											<div class="row">
												<div class="form-group col-md-8" id="divperipheralcriteria14" name="divperipheralcriteria14">
												  <label><span data-tooltip="Positive testing according to standard laboratory techniques" data-tooltip-position="top">14. HLA-B27 positivity</span></label>
												  <select class="form-control" name="criteriaperipheral14" id="criteriaperipheral14">
													<option value="0" <?php if ($criteriaperipheral14==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaperipheral14==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaperipheral14==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaperipheral14==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateperipheral14" name="divdateperipheral14" <?php if($criteriaperipheral14==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriaperipheraldate14" name="criteriaperipheraldate14" value="<?php echo $criteriaperipheraldate14_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral14" name="calcageperipheral14">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral214" name="calcageperipheral214">set date2</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divperipheralcriteria15" name="divperipheralcriteria15">
												  <label><span data-tooltip="Sacroiliitis on plain radiographs : Grade ≥2 bilaterally (at least minor stenosis + small erosions) or Grade 3-4 unilaterally (significant stenosis + erosions)" data-tooltip-position="top">15. Sacroiliitis on X-Rays</span></label>
												  <select class="form-control" name="criteriaperipheral15" id="criteriaperipheral15">
													<option value="0" <?php if ($criteriaperipheral15==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaperipheral15==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaperipheral15==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaperipheral15==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateperipheral15" name="divdateperipheral15" <?php if($criteriaperipheral15==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriaperipheraldate15" name="criteriaperipheraldate15" value="<?php echo $criteriaperipheraldate15_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral15" name="calcageperipheral15">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral215" name="calcageperipheral215">set date2</a>
													<!-- /.input group -->
												  </div>
												  </div>
											<div class="row">
												<div class="form-group col-md-8" id="divperipheralcriteria16" name="divperipheralcriteria16">
												  <label><span data-tooltip="Active inflammatory lesions of sacroiliac joints on MRI with definite bone marrow oedema/osteitis suggestive of sacroiliitis associated with spondyloarthritis" data-tooltip-position="top">16.MRI sacroiliitis</span></label>
												  <select class="form-control" name="criteriaperipheral16" id="criteriaperipheral16">
													<option value="0" <?php if ($criteriaperipheral16==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaperipheral16==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaperipheral16==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaperipheral16==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateperipheral16" name="divdateperipheral16" <?php if($criteriaperipheral16==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriaperipheraldate16" name="criteriaperipheraldate16" value="<?php echo $criteriaperipheraldate16_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral16" name="calcageperipheral16">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageperipheral216" name="calcageperipheral216">set date2</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-4" id="divtotalscore" name="divtotalscore">
												  <label><span>Peripheral set1: </span><input type="text" readonly id="set1" name="set1" value="<?php echo $set1; ?>"></label>
												 </div>
												 <div class="form-group col-md-4" id="divtotalscore" name="divtotalscore">
												  <label><span>Diagnosis date: </span><input type="text" readonly id="set1_date" name="set1_date" value="<?php echo $set1_date; ?>"></label>
												  </div>
												 <div class="form-group col-md-4" id="divtotalscore" name="divtotalscore">
												  <input type="hidden" readonly id="set1_score" name="set1_score" value="0">
												</div>
											</div>
											<div class="row">
												<div class="form-group col-md-4" id="divtotalscore" name="divtotalscore">
												  <label><span>Peripheral set2: </span><input type="text" readonly id="set2" name="set2" value="<?php echo $set2; ?>"></label>
												 </div>
												 <div class="form-group col-md-4" id="divtotalscore" name="divtotalscore">
												  <label><span>Diagnosis date: </span><input type="text" readonly id="set2_date" name="set2_date" value="<?php echo $set2_date; ?>"></label>
												  </div>
												 <div class="form-group col-md-4" id="divtotalscore" name="divtotalscore">
												  <input type="hidden" readonly id="set2_score" name="set2_score" value="0">
												</div>
											</div>
										</div>
									</div>
								<?php
								}
								
								if($class_criteria3>0)
								{
									
									
									$order = get_axialcriteria($pat_id);
									$result2 = pg_fetch_assoc($order);
									$patient_axialcriteria_id=$result2['patient_axialcriteria_id'];
									$criteriaaxial1=$result2['criteria1'];
									$criteriadateaxial1_str=$result2['date1_str'];
									if($criteriadateaxial1_str=="12-12-1900")
										$criteriadateaxial1_str="";
									
									$criteriaaxial2=$result2['criteria2'];
									$criteriadateaxial2_str=$result2['date2_str'];
									if($criteriadateaxial2_str=="12-12-1900")
										$criteriadateaxial2_str="";
									$criteriaaxial3=$result2['criteria3'];
									$criteriadateaxial3_str=$result2['date3_str'];
									if($criteriadateaxial3_str=="12-12-1900")
										$criteriadateaxial3_str="";
									$criteriaaxial4=$result2['criteria4'];
									$criteriadateaxial4_str=$result2['date4_str'];
									if($criteriadateaxial4_str=="12-12-1900")
										$criteriadateaxial4_str="";
									$criteriaaxial5=$result2['criteria5'];
									$criteriadateaxial5_str=$result2['date5_str'];
									if($criteriadateaxial5_str=="12-12-1900")
										$criteriadateaxial5_str="";
									$criteriaaxial6=$result2['criteria6'];
									$criteriadateaxial6_str=$result2['date6_str'];
									if($criteriadateaxial6_str=="12-12-1900")
										$criteriadateaxial6_str="";
									$criteriaaxial7=$result2['criteria7'];
									$criteriadateaxial7_str=$result2['date7_str'];
									if($criteriadateaxial7_str=="12-12-1900")
										$criteriadateaxial7_str="";
									$criteriaaxial8=$result2['criteria8'];
									$criteriadateaxial8_str=$result2['date8_str'];
									if($criteriadateaxial8_str=="12-12-1900")
										$criteriadateaxial8_str="";
									$criteriaaxial9=$result2['criteria9'];
									$criteriadateaxial9_str=$result2['date9_str'];
									if($criteriadateaxial9_str=="12-12-1900")
										$criteriadateaxial9_str="";
									$criteriaaxial10=$result2['criteria10'];
									$criteriadateaxial10_str=$result2['date10_str'];
									if($criteriadateaxial10_str=="12-12-1900")
										$criteriadateaxial10_str="";
									$criteriaaxial11=$result2['criteria11'];
									$criteriadateaxial11_str=$result2['date11_str'];
									if($criteriadateaxial11_str=="12-12-1900")
										$criteriadateaxial11_str="";
									$criteriaaxial12=$result2['criteria12'];
									$criteriadateaxial12_str=$result2['date12_str'];
									if($criteriadateaxial12_str=="12-12-1900")
										$criteriadateaxial12_str="";
									$criteriaaxial13=$result2['criteria13'];
									$criteriadateaxial13_str=$result2['date13_str'];
									if($criteriadateaxial13_str=="12-12-1900")
										$criteriadateaxial13_str="";
									$criteriaaxial14=$result2['criteria14'];
									$criteriadateaxial14_str=$result2['date14_str'];
									if($criteriadateaxial14_str=="12-12-1900")
										$criteriadateaxial14_str="";
									$criteriaaxial15=$result2['criteria15'];
									$criteriadateaxial15_str=$result2['date15_str'];
									if($criteriadateaxial15_str=="12-12-1900")
										$criteriadateaxial15_str="";
									$criteriaaxial16=$result2['criteria16'];
									$criteriadateaxial16_str=$result2['date16_str'];
									if($criteriadateaxial16_str=="12-12-1900")
										$criteriadateaxial16_str="";
									
									
									$newyork=$result2['newyork'];
									$newyork_date=$result2['newyork_date_str'];
									if($newyork_date=="12-12-1900")
										$newyork_date="";
									$newyork_score=$result2['newyork_score'];
									$imaging=$result2['imaging'];
									$imaging_date=$result2['imaging_date_str'];
									if($imaging_date=="12-12-1900")
										$imaging_date="";
									$imaging_score=$result2['imaging_score'];
									$clinical=$result2['clinical'];
									$clinical_date=$result2['clinical_date_str'];
									if($clinical_date=="12-12-1900")
										$clinical_date="";
									$clinical_score=$result2['clinical_score'];
									$radiographic=$result2['radiographic'];
									$radiographic_date=$result2['radiographic_date_str'];
									if($radiographic_date=="12-12-1900")
										$radiographic_date="";
									$radiographic_score=$result2['radiographic_score'];
									
									$msg_newyork="";
									$msg_imaging="";
									$msg_clinical="";
									$msg_radiographic="";
									
									if($newyork=="Yes")
										$msg_newyork="Clasiffied as Axial SpA </br> according to AS (New York Modified) ";
									if($newyork_date<>"" and $newyork=="Yes")
										$msg_newyork .= " at $newyork_date ";
									if($newyork_score<>"" and $newyork_score<>"0")
										$msg_newyork .= " with score $newyork_score";
									
									if($imaging=="Yes")
										$msg_imaging=" Clasiffied as Axial SpA according to Axial SpA Imaging arm criteria ";
									if($imaging_date<>"" and $imaging=="Yes")
										$msg_imaging .= " at $imaging_date ";
									if($imaging_score<>"" and $imaging_score<>"0")
										$msg_imaging .= " with score $imaging_score";
									
									if($clinical=="Yes")
										$msg_clinical=" Clasiffied as Axial SpA according to Axial SpA Clinical arm criteria ";
									if($clinical_date<>"" and $clinical=="Yes")
										$msg_clinical .= " at $clinical_date ";
									if($clinical_score<>"" and $clinical_score<>"0")
										$msg_clinical .= " with score $clinical_score";
									
									if($radiographic=="Yes")
										$msg_radiographic=" Clasiffied as Axial SpA according to Non-radiographic Axial SpA criteria";
									if($radiographic_date<>"" and $radiographic=="Yes")
										$msg_radiographic .= " at $radiographic_date ";
									if($radiographic_score<>"" and $radiographic_score<>"0")
										$msg_radiographic .= " with score $radiographic_score";
								?>
								  <label>Axial SpA Criteria
									<b><div id="divaxialclassified" name="divaxialclassified" <?php if($msg_newyork=="") { echo 'style="DISPLAY:none;"'; } else  { echo 'style="DISPLAY:block;"'; } ?>><?php echo $msg_newyork; ?></div></b>
									<b><div id="divaxialclassified2" name="divaxialclassified2" <?php if($msg_imaging=="") { echo 'style="DISPLAY:none;"'; } else  { echo 'style="DISPLAY:block;"'; } ?>><?php echo $msg_imaging; ?></div></b>
									<b><div id="divaxialclassified3" name="divaxialclassified3" <?php if($msg_clinical=="") { echo 'style="DISPLAY:none;"'; } else  { echo 'style="DISPLAY:block;"'; } ?>><?php echo $msg_clinical; ?></div></b>
									<b><div id="divaxialclassified4" name="divaxialclassified4" <?php if($msg_radiographic=="") { echo 'style="DISPLAY:none;"'; } else  { echo 'style="DISPLAY:block;"'; } ?>><?php echo $msg_radiographic; ?></div></b>
								  </label>
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaxial1" name="divcriteriaaxial1">
													<input type="hidden" value="<?php echo $patient_axialcriteria_id; ?>" id="patient_axialcriteria_id" name="patient_axialcriteria_id"/>
												  <label><span data-tooltip="Low back pain lasting more than 3 months in a patient < 45 years old at symptom onset" data-tooltip-position="top">1. ≥3 months back pain before 45 years of age</span></label>
												  <select class="form-control" name="criteriaaxial1" id="criteriaaxial1">
													<option value="0" <?php if ($criteriaaxial1==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaxial1==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaxial1==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaaxial1==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaxial1" name="divdateaxial1" <?php if($criteriaaxial1==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaxial1" name="criteriadateaxial1" value="<?php echo $criteriadateaxial1_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial1" name="calcageaxial1">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial21" name="calcageaxial21">set date2</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial31" name="calcageaxial31">set date3</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial41" name="calcageaxial41">set date4</a>
													<!-- /.input group -->
												  </div>
												  </div>
												<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaxial2" name="divcriteriaaxial2">
												  <label><span data-tooltip="Current or history of symptoms of spinal pain in back, dorsal, or cervical region, with at least 4/5 of the following: (1) age at onset < 40 years; (2) insidious onset; (3) improvement with exercise; (4) no improvement with rest; (5) pain at night (with improvement upon getting up)" data-tooltip-position="top">2. Current or past inflammatory back pain</span></label>
												  <select class="form-control" name="criteriaaxial2" id="criteriaaxial2">
													<option value="0" <?php if ($criteriaaxial2==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaxial2==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaxial2==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaaxial2==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaxial2" name="divdateaxial2" <?php if($criteriaaxial2==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaxial2" name="criteriadateaxial2" value="<?php echo $criteriadateaxial2_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial2" name="calcageaxial2">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial22" name="calcageaxial22">set date2</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial32" name="calcageaxial32">set date3</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial42" name="calcageaxial42">set date4</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaxial3" name="divcriteriaaxial3">
												  <label><span data-tooltip="At 24–48 h after a full dose of NSAID the back pain is not present anymore or is much better" data-tooltip-position="top">3. Good response of back pain to NDAIDs</span></label>
												  <select class="form-control" name="criteriaaxial3" id="criteriaaxial3">
													<option value="0" <?php if ($criteriaaxial3==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaxial3==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaxial3==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaaxial3==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaxial3" name="divdateaxial3" <?php if($criteriaaxial3==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaxial3" name="criteriadateaxial3" value="<?php echo $criteriadateaxial3_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial3" name="calcageaxial3">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial23" name="calcageaxial23">set date2</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial33" name="calcageaxial33">set date3</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial43" name="calcageaxial43">set date4</a>
													<!-- /.input group -->
												  </div>
												  </div>
												<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaxial4" name="divcriteriaaxial4">
												  <label><span data-tooltip="Past or present peripheral synovitis compatible with SpA (usually asymmetric and/or predominant involvement of the lower limb) diagnosed clinically by a doctor" data-tooltip-position="top">4. Past or present peripheral arthritis</span></label>
												  <select class="form-control" name="criteriaaxial4" id="criteriaaxial4">
													<option value="0" <?php if ($criteriaaxial4==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaxial4==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaxial4==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaaxial4==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaxial4" name="divdateaxial4" <?php if($criteriaaxial4==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaxial4" name="criteriadateaxial4" value="<?php echo $criteriadateaxial4_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial4" name="calcageaxial4">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial24" name="calcageaxial24">set date2</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial34" name="calcageaxial34">set date3</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial44" name="calcageaxial44">set date4</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaxial5" name="divcriteriaaxial5">
												  <label><span data-tooltip="Past or present spontaneous pain or tenderness at examination at the site of the insertion of the Achilles tendon or plantar fascia at the calcaneus" data-tooltip-position="top">5. Past or present heel enthesitis</span></label>
												  <select class="form-control" name="criteriaaxial5" id="criteriaaxial5">
													<option value="0" <?php if ($criteriaaxial5==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaxial5==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaxial5==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaaxial5==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaxial5" name="divdateaxial5" <?php if($criteriaaxial5==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaxial5" name="criteriadateaxial5" value="<?php echo $criteriadateaxial5_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial5" name="calcageaxial2">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial25" name="calcageaxial22">set date2</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial35" name="calcageaxial35">set date3</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial45" name="calcageaxial45">set date4</a>
													<!-- /.input group -->
												  </div>
												  </div>
												<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaxial6" name="divcriteriaaxial6">
												  <label><span data-tooltip="Swelling of an entire digit on current or past physical examination" data-tooltip-position="top">6. Past or present dactylitis</span></label>
												  <select class="form-control" name="criteriaaxial6" id="criteriaaxial6">
													<option value="0" <?php if ($criteriaaxial6==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaxial6==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaxial6==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaaxial6==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaxial6" name="divdateaxial6" <?php if($criteriaaxial6==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaxial6" name="criteriadateaxial6" value="<?php echo $criteriadateaxial6_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial6" name="calcageaxial6">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial26" name="calcageaxial26">set date2</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial36" name="calcageaxial36">set date3</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial46" name="calcageaxial46">set date4</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaxial7" name="divcriteriaaxial7">
												  <label><span data-tooltip="Past or present psoriasis, diagnosed by a doctor" data-tooltip-position="top">7. Past or present psoriasis</span></label>
												  <select class="form-control" name="criteriaaxial7" id="criteriaaxial7">
													<option value="0" <?php if ($criteriaaxial7==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaxial7==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaxial7==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaaxial7==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaxial7" name="divdateaxial7" <?php if($criteriaaxial7==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaxial7" name="criteriadateaxial7" value="<?php echo $criteriadateaxial7_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial7" name="calcageaxial7">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial27" name="calcageaxial27">set date2</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial37" name="calcageaxial37">set date3</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial47" name="calcageaxial47">set date4</a>
													<!-- /.input group -->
												  </div>
												  </div>
												<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaxial8" name="divcriteriaaxial8">
												  <label><span data-tooltip="Past or present Crohn disease or ulcerative colitis diagnosed by a doctor and confirmed by radiographic examination or endoscopy" data-tooltip-position="top">8. Past or present Inflammatory Bowel Disease</span></label>
												  <select class="form-control" name="criteriaaxial8" id="criteriaaxial8">
													<option value="0" <?php if ($criteriaaxial8==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaxial8==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaxial8==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaaxial8==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaxial8" name="divdateaxial8" <?php if($criteriaaxial8==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaxial8" name="criteriadateaxial8" value="<?php echo $criteriadateaxial8_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial8" name="calcageaxial8">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial28" name="calcageaxial28">set date2</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial38" name="calcageaxial38">set date3</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial48" name="calcageaxial48">set date4</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaxial9" name="divcriteriaaxial9">
												  <label><span data-tooltip="Past or present uveitis anterior, confirmed by an ophthalmologist" data-tooltip-position="top">9. Past or present acute anterior uveitis</span></label>
												  <select class="form-control" name="criteriaaxial9" id="criteriaaxial9">
													<option value="0" <?php if ($criteriaaxial9==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaxial9==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaxial9==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaaxial9==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaxial9" name="divdateaxial9" <?php if($criteriaaxial9==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaxial9" name="criteriadateaxial9" value="<?php echo $criteriadateaxial9_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial9" name="calcageaxial9">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial29" name="calcageaxial29">set date2</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial39" name="calcageaxial39">set date3</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial49" name="calcageaxial49">set date4</a>
													<!-- /.input group -->
												  </div>
												  </div>
												<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaxial10" name="divcriteriaaxial10">
												  <label><span data-tooltip="Presence in 1st-degree (mother, father, sisters, brothers, children) or 2nd-degree (maternal and paternal grandparents, aunts, uncles, nieces and nephews) relatives of any of the following: (1) ankylosing spondylitis, (2) psoriasis, (3) acute uveitis, (4) reactive arthritis, (5) IBD" data-tooltip-position="top">10. Family history for SpA </span></label>
												  <select class="form-control" name="criteriaaxial10" id="criteriaaxial10">
													<option value="0" <?php if ($criteriaaxial10==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaxial10==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaxial10==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaaxial10==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaxial10" name="divdateaxial10" <?php if($criteriaaxial10==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaxial10" name="criteriadateaxial10" value="<?php echo $criteriadateaxial10_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial10" name="calcageaxial10">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial210" name="calcageaxial210">set date2</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial310" name="calcageaxial310">set date3</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial410" name="calcageaxial410">set date4</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaxial11" name="divcriteriaaxial11">
												  <label><span data-tooltip="Positive testing according to standard laboratory techniques" data-tooltip-position="top">11. HLA-B27 positivity</span></label>
												  <select class="form-control" name="criteriaaxial11" id="criteriaaxial11">
													<option value="0" <?php if ($criteriaaxial11==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaxial11==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaxial11==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaaxial11==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaxial11" name="divdateaxial11" <?php if($criteriaaxial11==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaxial11" name="criteriadateaxial11" value="<?php echo $criteriadateaxial11_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial11" name="calcageaxial11">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial211" name="calcageaxial211">set date2</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial311" name="calcageaxial311">set date3</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial411" name="calcageaxial411">set date4</a>
													<!-- /.input group -->
												  </div>
												  </div>
												<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaxial12" name="divcriteriaaxial12">
												  <label><span data-tooltip="CRP above upper normal limit in the presence of back pain, after exclusion of other causes for elevated CRP concentration" data-tooltip-position="top">12. Elevated CRP</span></label>
												  <select class="form-control" name="criteriaaxial12" id="criteriaaxial12">
													<option value="0" <?php if ($criteriaaxial12==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaxial12==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaxial12==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaaxial12==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaxial12" name="divdateaxial12" <?php if($criteriaaxial12==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaxial12" name="criteriadateaxial12" value="<?php echo $criteriadateaxial12_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial12" name="calcageaxial12">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial212" name="calcageaxial212">set date2</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial312" name="calcageaxial312">set date3</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial412" name="calcageaxial412">set date4</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaxial13" name="divcriteriaaxial13">
												  <label><span data-tooltip="Limitation of motion of the lumbar spine in the sagittal and frontal planes" data-tooltip-position="top">13. Limited spinal mobility (abnormal Schober test)</span></label>
												  <select class="form-control" name="criteriaaxial13" id="criteriaaxial13">
													<option value="0" <?php if ($criteriaaxial13==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaxial13==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaxial13==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaaxial13==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaxial13" name="divdateaxial13" <?php if($criteriaaxial13==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaxial13" name="criteriadateaxial13" value="<?php echo $criteriadateaxial13_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial13" name="calcageaxial13">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial213" name="calcageaxial213">set date2</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial313" name="calcageaxial313">set date3</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial413" name="calcageaxial413">set date4</a>
													
													<!-- /.input group -->
												  </div>
												  </div>
												<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaxial14" name="divcriteriaaxial14">
												  <label><span data-tooltip="On present physical examination, chest expansion <4 cm" data-tooltip-position="top">14. Limited chest expansion (<4 cm)</span></label>
												  <select class="form-control" name="criteriaaxial14" id="criteriaaxial14">
													<option value="0" <?php if ($criteriaaxial14==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaxial14==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaxial14==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaaxial14==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaxial14" name="divdateaxial14" <?php if($criteriaaxial14==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaxial14" name="criteriadateaxial14" value="<?php echo $criteriadateaxial14_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial14" name="calcageaxial14">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial214" name="calcageaxial214">set date2</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial314" name="calcageaxial314">set date3</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial414" name="calcageaxial414">set date4</a>
													
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaxial15" name="divcriteriaaxial15">
												  <label><span data-tooltip="Sacroiliitis on plain radiographs : Grade ≥2 bilaterally (at least minor stenosis + small erosions) or Grade 3-4 unilaterally (significant stenosis + erosions)" data-tooltip-position="top">15. Sacroiliitis on X-Rays</span></label>
												  <select class="form-control" name="criteriaaxial15" id="criteriaaxial15">
													<option value="0" <?php if ($criteriaaxial15==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaxial15==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaxial15==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaaxial15==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaxial15" name="divdateaxial15" <?php if($criteriaaxial15==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaxial15" name="criteriadateaxial15" value="<?php echo $criteriadateaxial15_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial15" name="calcageaxial15">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial215" name="calcageaxial215">set date2</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial315" name="calcageaxial315">set date3</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial415" name="calcageaxial415">set date4</a>
													
													<!-- /.input group -->
												  </div>
												  </div>
												<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaxial16" name="divcriteriaaxial16">
												  <label><span data-tooltip="Active inflammatory lesions of sacroiliac joints on MRI with definite bone marrow oedema/osteitis suggestive of sacroiliitis associated with spondyloarthritis" data-tooltip-position="top">16. MRI sacroiliitis</span></label>
												  <select class="form-control" name="criteriaaxial16" id="criteriaaxial16">
													<option value="0" <?php if ($criteriaaxial16==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaxial16==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaxial16==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriaaxial16==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaxial16" name="divdateaxial16" <?php if($criteriaaxial16==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaxial16" name="criteriadateaxial16" value="<?php echo $criteriadateaxial16_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial16" name="calcageaxial16">set date1</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial216" name="calcageaxial216">set date2</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial316" name="calcageaxial316">set date3</a>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaxial416" name="calcageaxial416">set date4</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-6" id="divtotalscore" name="divtotalscore">
												  <label><span>AS (New York Modified): </span><input type="text" readonly id="newyork" name="newyork" value="<?php echo $newyork; ?>"></label>
												 </div>
												 <div class="form-group col-md-4" id="divtotalscore" name="divtotalscore">
												  <label><span>Diagnosis date: </span><input type="text" readonly id="newyork_date" name="newyork_date" value="<?php echo $newyork_date; ?>"></label>
												  </div>
												 <div class="form-group col-md-2" id="divtotalscore" name="divtotalscore">
												 <input type="hidden" readonly id="newyork_score" name="newyork_score" value="0">
												</div>
											</div>
											<div class="row">
												<div class="form-group col-md-6" id="divtotalscore" name="divtotalscore">
												  <label><span>Axial SpA Imaging arm: </span><input type="text" readonly id="imaging" name="imaging" value="<?php echo $imaging; ?>"></label>
												 </div>
												 <div class="form-group col-md-4" id="divtotalscore" name="divtotalscore">
												  <label><span>Diagnosis date: </span><input type="text" readonly id="imaging_date" name="imaging_date" value="<?php echo $imaging_date; ?>"></label>
												  </div>
												 <div class="form-group col-md-2" id="divtotalscore" name="divtotalscore">
												 <input type="hidden" readonly id="imaging_score" name="imaging_score" value="0">
												</div>
											</div>
											<div class="row">
												<div class="form-group col-md-6" id="divtotalscore" name="divtotalscore">
												  <label><span>Axial SpA Clinical arm: </span><input type="text" readonly id="clinical" name="clinical" value="<?php echo $clinical; ?>"></label>
												 </div>
												 <div class="form-group col-md-4" id="divtotalscore" name="divtotalscore">
												  <label><span>Diagnosis date: </span><input type="text" readonly id="clinical_date" name="clinical_date" value="<?php echo $clinical_date; ?>"></label>
												  </div>
												 <div class="form-group col-md-2" id="divtotalscore" name="divtotalscore">
												  <input type="hidden" readonly id="clinical_score" name="clinical_score" value="0">
												</div>
											</div>
											<div class="row">
												<div class="form-group col-md-6" id="divtotalscore" name="divtotalscore">
												  <label><span>Non-radiographic Axial SpA: </span><input type="text" readonly id="radiographic" name="radiographic" value="<?php echo $radiographic; ?>"></label>
												 </div>
												 <div class="form-group col-md-4" id="divtotalscore" name="divtotalscore">
												  <label><span>Diagnosis date: </span><input type="text" readonly id="radiographic_date" name="radiographic_date" value="<?php echo $radiographic_date; ?>"></label>
												  </div>
												 <div class="form-group col-md-2" id="divtotalscore" name="divtotalscore">
												  <input type="hidden" readonly id="radiographic_score" name="radiographic_score" value="0">
												</div>
											</div>
										</div>
									</div>
								<?php
								}
								
								
								if($class_criteria4>0)
								{
									$order = get_casparcriteria($pat_id);
									$result2 = pg_fetch_assoc($order);
									$patient_casparcriteria_id=$result2['patient_casparcriteria_id'];
									$criteriacaspar1=$result2['criteria1'];
									$criteriadatecaspar1_str=$result2['date1_str'];
									if($criteriadatecaspar1_str=="12-12-1900")
										$criteriadatecaspar1_str="";
									$criteriacaspar2=$result2['criteria2'];
									$criteriadatecaspar2_str=$result2['date2_str'];
									if($criteriadatecaspar2_str=="12-12-1900")
										$criteriadatecaspar2_str="";
									$criteriacaspar3=$result2['criteria3'];
									$criteriadatecaspar3_str=$result2['date3_str'];
									if($criteriadatecaspar3_str=="12-12-1900")
										$criteriadatecaspar3_str="";
									$criteriacaspar4=$result2['criteria4'];
									$criteriadatecaspar4_str=$result2['date4_str'];
									if($criteriadatecaspar4_str=="12-12-1900")
										$criteriadatecaspar4_str="";
									$criteriacaspar5=$result2['criteria5'];
									$criteriadatecaspar5_str=$result2['date5_str'];
									if($criteriadatecaspar5_str=="12-12-1900")
										$criteriadatecaspar5_str="";
									$caspar=$result2['caspar'];
									$caspardate=$result2['caspardate_str'];
									if($caspardate=="12-12-1900")
										$caspardate="";
									$caspartotalscore=$result2['totalscore'];
									
									$msg_caspar="";
									
									if($caspar=="Yes")
										$msg_caspar="Clasiffied as CASPAR ";
									if($caspardate<>"")
										$msg_caspar .= " at $caspardate ";
									if($caspartotalscore<>"0" and $caspartotalscore<>"")
										$msg_caspar .= " with score $caspartotalscore/6";
								?>
								  <label>CASPAR Criteria for Psoriatic Arthritis
									<b><div id="divcasparclassified" name="divcasparclassified" <?php if($msg_caspar=="") { echo 'style="DISPLAY:none;"'; } else  { echo 'style="DISPLAY:block;"'; } ?>><?php echo $msg_caspar; ?></div></b>
								  </label>
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriacaspar1" name="divcriteriacaspar1">
												<input type="hidden" value="<?php echo $patient_casparcriteria_id; ?>" id="patient_casparcriteria_id" name="patient_casparcriteria_id"/>
												  <label><span data-tooltip="Low back pain lasting more than 3 months in a patient < 45 years old at symptom onset" data-tooltip-position="top">1.Skin Psoriasis</span></label>
												  <select class="form-control" name="criteriacaspar1" id="criteriacaspar1">
													<option value="0" <?php if ($criteriacaspar1==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriacaspar1==1) { echo "selected"; } ?>>Personal history or family history of psoriasis (in a first- or second-degree relative)(1 point)</option>
													<option value="2" <?php if ($criteriacaspar1==2) { echo "selected"; } ?>>Current skin or scalp psoriasis as judged by a physician (2 points)</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdatecaspar1" name="divdatecaspar1" <?php if($criteriacaspar1==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadatecaspar1" name="criteriadatecaspar1" value="<?php echo $criteriadatecaspar1_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagecaspar1" name="calcagecaspar1">set date</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriacaspar2" name="divcriteriacaspar2">
												  <label><span data-tooltip="Psoriatic nail dystrophy, e.g. onycholysis, pitting or hyperkeratosis during current physical examination" data-tooltip-position="top">2. Psoriatic nail dystrophy, e.g. onycholysis, pitting or hyperkeratosis during current physical examination</span></label>
												  <select class="form-control" name="criteriacaspar2" id="criteriacaspar2">
													<option value="0" <?php if ($criteriacaspar2==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriacaspar2==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriacaspar2==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriacaspar2==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdatecaspar2" name="divdatecaspar2" <?php if($criteriacaspar2==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadatecaspar2" name="criteriadatecaspar2" value="<?php echo $criteriadatecaspar2_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagecaspar2" name="calcagecaspar2">set date</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriacaspar3" name="divcriteriacaspar3">
												  <label><span data-tooltip="Negative test for rheumatoid factor" data-tooltip-position="top">3. Negative test for rheumatoid factor</span></label>
												  <select class="form-control" name="criteriacaspar3" id="criteriacaspar3">
													<option value="0" <?php if ($criteriacaspar3==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriacaspar3==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriacaspar3==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriacaspar3==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdatecaspar3" name="divdatecaspar3" <?php if($criteriacaspar3==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadatecaspar3" name="criteriadatecaspar3" value="<?php echo $criteriadatecaspar3_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagecaspar3" name="calcagecaspar3">set date</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriacaspar4" name="divcriteriacaspar4">
												  <label><span data-tooltip="Past or present dactylitis" data-tooltip-position="top">4. Past or present dactylitis</span></label>
												  <select class="form-control" name="criteriacaspar4" id="criteriacaspar4">
													<option value="0" <?php if ($criteriacaspar4==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriacaspar4==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriacaspar4==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriacaspar4==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdatecaspar4" name="divdatecaspar4" <?php if($criteriacaspar4==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadatecaspar4" name="criteriadatecaspar4" value="<?php echo $criteriadatecaspar4_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagecaspar4" name="calcagecaspar4">set date</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriacaspar5" name="divcriteriacaspar5">
												  <label><span data-tooltip="Radiographic evidence of juxta-articular new bone formation on plain radiographs of hand or foot (excluding osteophytes formation)" data-tooltip-position="top">5. Radiographic evidence of juxta-articular new bone formation on plain radiographs of hand or foot (excluding osteophytes formation)</span></label>
												  <select class="form-control" name="criteriacaspar5" id="criteriacaspar5">
													<option value="0" <?php if ($criteriacaspar5==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriacaspar5==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriacaspar5==2) { echo "selected"; } ?>>No</option>
													<option value="3" <?php if ($criteriacaspar5==3) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdatecaspar5" name="divdatecaspar5" <?php if($criteriacaspar5==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadatecaspar5" name="criteriadatecaspar5" value="<?php echo $criteriadatecaspar5_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagecaspar5" name="calcagecaspar5">set date</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-4" id="divtotalscore" name="divtotalscore">
												  <label><span>CASPAR: </span><input type="text" readonly id="caspar" name="caspar" value="<?php echo $caspar; ?>"></label>
												 </div>
												 <div class="form-group col-md-4" id="divtotalscore" name="divtotalscore">
												  <label><span>Diagnosis date: </span><input type="text" readonly id="caspardate" name="caspardate" value="<?php echo $caspardate; ?>"></label>
												  </div>
												 <div class="form-group col-md-4" id="divtotalscore" name="divtotalscore">
												  <label><span>Total score: </span><input type="text" readonly id="caspartotalscore" name="caspartotalscore" value="<?php echo $caspartotalscore; ?>"></label>
												</div>
											</div>
										</div>
									</div>
								<?php
								}
								
								if($class_criteria5>0)
								{
									$order = get_criteriasle($pat_id);
									$result2 = pg_fetch_assoc($order);
									$patient_criteriasle_id=$result2['patient_criteriasle_id'];
									$criteriasle19971=$result2['criteriasle19971'];
									$criteriasle19972=$result2['criteriasle19972'];
									$criteriasle19973=$result2['criteriasle19973'];
									$criteriasle19974=$result2['criteriasle19974'];
									$criteriasle19975=$result2['criteriasle19975'];
									$criteriasle19976=$result2['criteriasle19976'];
									$criteriasle19977=$result2['criteriasle19977'];
									$criteriasle19978=$result2['criteriasle19978'];
									$criteriasle19979=$result2['criteriasle19979'];
									$criteriasle199710=$result2['criteriasle199710'];
									$criteriasle199711=$result2['criteriasle199711'];
									$criteriadatesle19971_str=$result2['criteriadatesle19971_str'];
									if($criteriadatesle19971_str=="12-12-1900")
										$criteriadatesle19971_str="";
									$criteriadatesle19972_str=$result2['criteriadatesle19972_str'];
									if($criteriadatesle19972_str=="12-12-1900")
										$criteriadatesle19972_str="";
									$criteriadatesle19973_str=$result2['criteriadatesle19973_str'];
									if($criteriadatesle19973_str=="12-12-1900")
										$criteriadatesle19973_str="";
									$criteriadatesle19974_str=$result2['criteriadatesle19974_str'];
									if($criteriadatesle19974_str=="12-12-1900")
										$criteriadatesle19974_str="";
									$criteriadatesle19975_str=$result2['criteriadatesle19975_str'];
									if($criteriadatesle19975_str=="12-12-1900")
										$criteriadatesle19975_str="";
									$criteriadatesle19976_str=$result2['criteriadatesle19976_str'];
									if($criteriadatesle19976_str=="12-12-1900")
										$criteriadatesle19976_str="";
									$criteriadatesle19977_str=$result2['criteriadatesle19977_str'];
									if($criteriadatesle19977_str=="12-12-1900")
										$criteriadatesle19977_str="";
									$criteriadatesle19978_str=$result2['criteriadatesle19978_str'];
									if($criteriadatesle19978_str=="12-12-1900")
										$criteriadatesle19978_str="";
									$criteriadatesle19979_str=$result2['criteriadatesle19979_str'];
									if($criteriadatesle19979_str=="12-12-1900")
										$criteriadatesle19979_str="";
									$criteriadatesle199710_str=$result2['criteriadatesle199710_str'];
									if($criteriadatesle199710_str=="12-12-1900")
										$criteriadatesle199710_str="";
									$criteriadatesle199711_str=$result2['criteriadatesle199711_str'];
									if($criteriadatesle199711_str=="12-12-1900")
										$criteriadatesle199711_str="";
									$sle1997=$result2['sle1997'];
									$sle1997date=$result2['sle1997date_str'];
									if($sle1997date=="12-12-1900")
										$sle1997date="";
									$sle1997totalscore=$result2['sle1997totalscore'];
									
									$msg_sle1997="";
									
									if($sle1997=="Yes")
										$msg_sle1997="Clasiffied as SLE according to 1997 revised ACR Criteria ";
									if($sle1997date<>"")
										$msg_sle1997 .= " at $sle1997date ";
									if($sle1997totalscore<>"0" and $sle1997totalscore<>"")
										$msg_sle1997 .= " with score $sle1997totalscore";
									
									$criteriasle20121=$result2['criteriasle20121'];
									$criteriasle20122=$result2['criteriasle20122'];
									$criteriasle20123=$result2['criteriasle20123'];
									$criteriasle20124=$result2['criteriasle20124'];
									$criteriasle20125=$result2['criteriasle20125'];
									$criteriasle20126=$result2['criteriasle20126'];
									$criteriasle20127=$result2['criteriasle20127'];
									$criteriasle20128=$result2['criteriasle20128'];
									$criteriasle20129=$result2['criteriasle20129'];
									$criteriasle201210=$result2['criteriasle201210'];
									$criteriasle201211=$result2['criteriasle201211'];
									$criteriasle2012b1=$result2['criteriasle2012b1'];
									$criteriasle2012b2=$result2['criteriasle2012b2'];
									$criteriasle2012b3=$result2['criteriasle2012b3'];
									$criteriasle2012b4=$result2['criteriasle2012b4'];
									$criteriasle2012b5=$result2['criteriasle2012b5'];
									$criteriasle2012b6=$result2['criteriasle2012b6'];
									$criteriasle2012c1=$result2['criteriasle2012c1'];
									
									$criteriadatesle20121_str=$result2['criteriadatesle20121_str'];
									if($criteriadatesle20121_str=="12-12-1900")
										$criteriadatesle20121_str="";
									$criteriadatesle20122_str=$result2['criteriadatesle20122_str'];
									if($criteriadatesle20122_str=="12-12-1900")
										$criteriadatesle20122_str="";
									$criteriadatesle20123_str=$result2['criteriadatesle20123_str'];
									if($criteriadatesle20123_str=="12-12-1900")
										$criteriadatesle20123_str="";
									$criteriadatesle20124_str=$result2['criteriadatesle20124_str'];
									if($criteriadatesle20124_str=="12-12-1900")
										$criteriadatesle20124_str="";
									$criteriadatesle20125_str=$result2['criteriadatesle20125_str'];
									if($criteriadatesle20125_str=="12-12-1900")
										$criteriadatesle20125_str="";
									$criteriadatesle20126_str=$result2['criteriadatesle20126_str'];
									if($criteriadatesle20126_str=="12-12-1900")
										$criteriadatesle20126_str="";
									$criteriadatesle20127_str=$result2['criteriadatesle20127_str'];
									if($criteriadatesle20127_str=="12-12-1900")
										$criteriadatesle20127_str="";
									$criteriadatesle20128_str=$result2['criteriadatesle20128_str'];
									if($criteriadatesle20128_str=="12-12-1900")
										$criteriadatesle20128_str="";
									$criteriadatesle20129_str=$result2['criteriadatesle20129_str'];
									if($criteriadatesle20129_str=="12-12-1900")
										$criteriadatesle20129_str="";
									$criteriadatesle201210_str=$result2['criteriadatesle201210_str'];
									if($criteriadatesle201210_str=="12-12-1900")
										$criteriadatesle201210_str="";
									$criteriadatesle201211_str=$result2['criteriadatesle201211_str'];
									if($criteriadatesle201211_str=="12-12-1900")
										$criteriadatesle201211_str="";
									
									$criteriadatesle2012b1_str=$result2['criteriadatesle2012b1_str'];
									if($criteriadatesle2012b1_str=="12-12-1900")
										$criteriadatesle2012b1_str="";
									$criteriadatesle2012b2_str=$result2['criteriadatesle2012b2_str'];
									if($criteriadatesle2012b2_str=="12-12-1900")
										$criteriadatesle2012b2_str="";
									$criteriadatesle2012b3_str=$result2['criteriadatesle2012b3_str'];
									if($criteriadatesle2012b3_str=="12-12-1900")
										$criteriadatesle2012b3_str="";
									$criteriadatesle2012b4_str=$result2['criteriadatesle2012b4_str'];
									if($criteriadatesle2012b4_str=="12-12-1900")
										$criteriadatesle2012b4_str="";
									$criteriadatesle2012b5_str=$result2['criteriadatesle2012b5_str'];
									if($criteriadatesle2012b5_str=="12-12-1900")
										$criteriadatesle2012b5_str="";
									$criteriadatesle2012b6_str=$result2['criteriadatesle2012b6_str'];
									if($criteriadatesle2012b6_str=="12-12-1900")
										$criteriadatesle2012b6_str="";
									$criteriadatesle2012c1_str=$result2['criteriadatesle2012c1_str'];
									if($criteriadatesle2012c1_str=="12-12-1900")
										$criteriadatesle2012c1_str="";
									
									$sle2012=$result2['sle2012'];
									$sle2012date=$result2['sle2012date_str'];
									if($sle2012date=="12-12-1900")
										$sle2012date="";
									$sle2012totalscore=$result2['sle2012totalscore'];
									
									$msg_sle2012="";
									
									if($sle2012=="Yes")
										$msg_sle2012="Clasiffied as SLE according to 2012 SLICC Criteria ";
									if($sle2012date<>"")
										$msg_sle2012 .= " at $sle2012date ";
									//if($sle2012totalscore<>"0" and $sle2012totalscore<>"")
									//	$msg_sle2012 .= " with score $sle2012totalscore";
									
									$criteriasle2017a1=$result2['criteriasle2017a1'];
									$criteriasle2017b1=$result2['criteriasle2017b1'];
									$criteriasle2017b21=$result2['criteriasle2017b21'];
									$criteriasle2017b22=$result2['criteriasle2017b22'];
									$criteriasle2017b23=$result2['criteriasle2017b23'];
									$criteriasle2017b3=$result2['criteriasle2017b3'];
									$criteriasle2017b41=$result2['criteriasle2017b41'];
									$criteriasle2017b42=$result2['criteriasle2017b42'];
									$criteriasle2017b43=$result2['criteriasle2017b43'];
									$criteriasle2017b51=$result2['criteriasle2017b51'];
									$criteriasle2017b52=$result2['criteriasle2017b52'];
									$criteriasle2017b61=$result2['criteriasle2017b61'];
									$criteriasle2017b62=$result2['criteriasle2017b62'];
									$criteriasle2017b63=$result2['criteriasle2017b63'];
									$criteriasle2017b71=$result2['criteriasle2017b71'];
									$criteriasle2017b72=$result2['criteriasle2017b72'];
									$criteriasle2017b73=$result2['criteriasle2017b73'];
									$criteriasle2017b8=$result2['criteriasle2017b8'];
									$criteriasle2017b91=$result2['criteriasle2017b91'];
									$criteriasle2017b92=$result2['criteriasle2017b92'];
									$criteriasle2017b10=$result2['criteriasle2017b10'];
									
									$criteriadatesle2017a1_str=$result2['criteriadatesle2017a1_str'];
									if($criteriadatesle2017a1_str=="12-12-1900")
										$criteriadatesle2017a1_str="";
									$criteriadatesle2017b1_str=$result2['criteriadatesle2017b1_str'];
									if($criteriadatesle2017b1_str=="12-12-1900")
										$criteriadatesle2017b1_str="";
									$criteriadatesle2017b21_str=$result2['criteriadatesle2017b21_str'];
									if($criteriadatesle2017b21_str=="12-12-1900")
										$criteriadatesle2017b21_str="";
									$criteriadatesle2017b22_str=$result2['criteriadatesle2017b22_str'];
									if($criteriadatesle2017b22_str=="12-12-1900")
										$criteriadatesle2017b22_str="";
									$criteriadatesle2017b23_str=$result2['criteriadatesle2017b23_str'];
									if($criteriadatesle2017b23_str=="12-12-1900")
										$criteriadatesle2017b23_str="";
									$criteriadatesle2017b3_str=$result2['criteriadatesle2017b3_str'];
									if($criteriadatesle2017b3_str=="12-12-1900")
										$criteriadatesle2017b3_str="";
									$criteriadatesle2017b41_str=$result2['criteriadatesle2017b41_str'];
									if($criteriadatesle2017b41_str=="12-12-1900")
										$criteriadatesle2017b41_str="";
									$criteriadatesle2017b42_str=$result2['criteriadatesle2017b42_str'];
									if($criteriadatesle2017b42_str=="12-12-1900")
										$criteriadatesle2017b42_str="";
									$criteriadatesle2017b43_str=$result2['criteriadatesle2017b43_str'];
									if($criteriadatesle2017b43_str=="12-12-1900")
										$criteriadatesle2017b43_str="";
									$criteriadatesle2017b51_str=$result2['criteriadatesle2017b51_str'];
									if($criteriadatesle2017b51_str=="12-12-1900")
										$criteriadatesle2017b51_str="";
									$criteriadatesle2017b52_str=$result2['criteriadatesle2017b52_str'];
									if($criteriadatesle2017b52_str=="12-12-1900")
										$criteriadatesle2017b52_str="";
									$criteriadatesle2017b61_str=$result2['criteriadatesle2017b61_str'];
									if($criteriadatesle2017b61_str=="12-12-1900")
										$criteriadatesle2017b61_str="";
									$criteriadatesle2017b62_str=$result2['criteriadatesle2017b62_str'];
									if($criteriadatesle2017b62_str=="12-12-1900")
										$criteriadatesle2017b62_str="";
									$criteriadatesle2017b63_str=$result2['criteriadatesle2017b63_str'];
									if($criteriadatesle2017b63_str=="12-12-1900")
										$criteriadatesle2017b63_str="";
									$criteriadatesle2017b71_str=$result2['criteriadatesle2017b71_str'];
									if($criteriadatesle2017b71_str=="12-12-1900")
										$criteriadatesle2017b71_str="";
									$criteriadatesle2017b72_str=$result2['criteriadatesle2017b72_str'];
									if($criteriadatesle2017b72_str=="12-12-1900")
										$criteriadatesle2017b72_str="";
									$criteriadatesle2017b73_str=$result2['criteriadatesle2017b73_str'];
									if($criteriadatesle2017b73_str=="12-12-1900")
										$criteriadatesle2017b73_str="";
									$criteriadatesle2017b8_str=$result2['criteriadatesle2017b8_str'];
									if($criteriadatesle2017b8_str=="12-12-1900")
										$criteriadatesle2017b8_str="";
									$criteriadatesle2017b91_str=$result2['criteriadatesle2017b91_str'];
									if($criteriadatesle2017b91_str=="12-12-1900")
										$criteriadatesle2017b91_str="";
									$criteriadatesle2017b92_str=$result2['criteriadatesle2017b92_str'];
									if($criteriadatesle2017b92_str=="12-12-1900")
										$criteriadatesle2017b92_str="";
									$criteriadatesle2017b10_str=$result2['criteriadatesle2017b10_str'];
									if($criteriadatesle2017b10_str=="12-12-1900")
										$criteriadatesle2017b10_str="";
									
									$sle2017=$result2['sle2017'];
									$sle2017date=$result2['sle2017date_str'];
									if($sle2017date=="12-12-1900")
										$sle2017date="";
									$sle2017totalscore=$result2['sle2017totalscore'];
									
									$msg_sle2017="";
									
									if($sle2017=="Yes")
										$msg_sle2017="Clasiffied as SLE according to 2017 EULAR/ACR Criteria ";
									if($sle2017date<>"")
										$msg_sle2017 .= " at $sle2017date ";
									if($sle2017totalscore<>"0" and $sle2017totalscore<>"")
										$msg_sle2017 .= " with score $sle2017totalscore";
								
								?>
								  <label>SLE criteria (1997 ACR/SLICC/EULAR-ACR)
										<b><div id="divsle1997" name="divsle1997" <?php if($msg_sle1997=="") { echo 'style="DISPLAY:none;"'; } else  { echo 'style="DISPLAY:block;"'; } ?>><?php echo $msg_sle1997; ?></div></b>
								</label>
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle19971" name="divcriteriasle19971">
														<input type="hidden" value="<?php echo $patient_criteriasle_id; ?>" id="patient_criteriasle_id" name="patient_criteriasle_id"/>
														  <label><span data-tooltip="Fixed erythema, flat or raised, over the malar eminences, tending to spare the nasolabial folds" data-tooltip-position="top">1.Malar rash</span></label>
														  <select class="form-control" name="criteriasle19971" id="criteriasle19971">
															<option value="0" <?php if ($criteriasle19971==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle19971==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle19971==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle19971==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle19971" name="divdatesle19971" <?php if($criteriasle19971==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle19971" name="criteriadatesle19971" value="<?php echo $criteriadatesle19971_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle19971" name="calcagesle19971">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle19972" name="divcriteriasle19972">
														  <label><span data-tooltip="Erythematous raised patches with adherent keratotic scaling and follicular plugging; atrophic scarring occur in older lesions" data-tooltip-position="top">2.Discoid rash</span></label>
														  <select class="form-control" name="criteriasle19972" id="criteriasle19972">
															<option value="0" <?php if ($criteriasle19972==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle19972==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle19972==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle19972==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle19972" name="divdatesle19972" <?php if($criteriasle19972==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle19972" name="criteriadatesle19972" value="<?php echo $criteriadatesle19972_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle19972" name="calcagesle19972">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle19973" name="divcriteriasle19973">
														  <label><span data-tooltip="Skin rash as a result of unusual reaction to sunlight, by patient history or physician observation" data-tooltip-position="top">3.Photosensitivity</span></label>
														  <select class="form-control" name="criteriasle19973" id="criteriasle19973">
															<option value="0" <?php if ($criteriasle19973==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle19973==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle19973==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle19973==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle19973" name="divdatesle19973" <?php if($criteriasle19973==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle19973" name="criteriadatesle19973" value="<?php echo $criteriadatesle19973_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle19973" name="calcagesle19973">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle19974" name="divcriteriasle19974">
														  <label><span data-tooltip="Oral or nasopharyngeal ulceration" data-tooltip-position="top">4.Mucosal ulcers</span></label>
														  <select class="form-control" name="criteriasle19974" id="criteriasle19974">
															<option value="0" <?php if ($criteriasle19974==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle19974==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle19974==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle19974==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle19974" name="divdatesle19974" <?php if($criteriasle19974==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle19974" name="criteriadatesle19974" value="<?php echo $criteriadatesle19974_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle19974" name="calcagesle19974">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle19975" name="divcriteriasle19975">
														  <label><span data-tooltip="Non-erosive arthritis involving ≥2 peripheral joints, characterized by tenderness, swelling or effusion" data-tooltip-position="top">5.Synovitis</span></label>
														  <select class="form-control" name="criteriasle19975" id="criteriasle19975">
															<option value="0" <?php if ($criteriasle19975==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle19975==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle19975==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle19975==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle19975" name="divdatesle19975" <?php if($criteriasle19975==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle19975" name="criteriadatesle19975" value="<?php echo $criteriadatesle19975_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle19975" name="calcagesle19975">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle19976" name="divcriteriasle19976">
														  <label><span data-tooltip="[a] Pleuritis: convincing history of pleuritic pain or rub heard by a physician or evidence of pleural effusion[b] Pericarditis: documented by ECG or rub or evidence of pericardial effusion" data-tooltip-position="top">6.Serositis</span></label>
														  <select class="form-control" name="criteriasle19976" id="criteriasle19976">
															<option value="0" <?php if ($criteriasle19976==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle19976==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle19976==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle19976==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle19976" name="divdatesle19976" <?php if($criteriasle19976==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle19976" name="criteriadatesle19976" value="<?php echo $criteriadatesle19976_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle19976" name="calcagesle19976">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle19977" name="divcriteriasle19977">
														  <label><span data-tooltip="[a] Persistent proteinuria >0.5 g per day or >3+ if quantitation is not performed [b] Cellular casts: red cell, haemoglobin, granular tubular, or mixed" data-tooltip-position="top">7.Renal disorder</span></label>
														  <select class="form-control" name="criteriasle19977" id="criteriasle19977">
															<option value="0" <?php if ($criteriasle19977==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle19977==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle19977==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle19977==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle19977" name="divdatesle19977" <?php if($criteriasle19977==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle19977" name="criteriadatesle19977" value="<?php echo $criteriadatesle19977_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle19977" name="calcagesle19977">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle19978" name="divcriteriasle19978">
														  <label><span data-tooltip="[a] Seizures: in the absence of offending drugs or known metabolic derangements[b] Psychosis: in the absence of offending drugs or known metabolic derangements" data-tooltip-position="top">8.Neurologic disorder</span></label>
														  <select class="form-control" name="criteriasle19978" id="criteriasle19978">
															<option value="0" <?php if ($criteriasle19978==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle19978==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle19978==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle19978==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle19978" name="divdatesle19978" <?php if($criteriasle19978==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle19978" name="criteriadatesle19978" value="<?php echo $criteriadatesle19978_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle19978" name="calcagesle19978">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle19979" name="divcriteriasle19979">
														  <label><span data-tooltip="[a] Haemolytic anemia with reticulocytosis[b] Leukopenia: < 4000/mm3 at least twice OR Lymphopenia: <1500/mm3 at least twice[c] Thrombocytopenia: <100,000/mm3" data-tooltip-position="top">9.Hematologic disorder</span></label>
														  <select class="form-control" name="criteriasle19979" id="criteriasle19979">
															<option value="0" <?php if ($criteriasle19979==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle19979==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle19979==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle19979==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle19979" name="divdatesle19979" <?php if($criteriasle19979==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle19979" name="criteriadatesle19979" value="<?php echo $criteriadatesle19979_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle19979" name="calcagesle19979">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle199710" name="divcriteriasle199710">
														  <label><span data-tooltip="[a] Anti-DNA: antibody to native DNA in abnormal titer[b] Anti-Sm: presence of antibody to Sm antigen[c] Positive finding of antiphospholipid antibodies based on: (1) an abnormal serum concentration of IgG or IgM anticardiolipin antibodies, (2) a positive test result for SLE anticoagulant, or (3) a false positive serologic test for syphilis, positive for ≥6 months" data-tooltip-position="top">10.Immunologic disorder</span></label>
														  <select class="form-control" name="criteriasle199710" id="criteriasle199710">
															<option value="0" <?php if ($criteriasle199710==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle199710==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle199710==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle199710==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle199710" name="divdatesle199710" <?php if($criteriasle199710==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle199710" name="criteriadatesle199710" value="<?php echo $criteriadatesle199710_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle199710" name="calcagesle199710">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle199711" name="divcriteriasle199711">
														  <label><span data-tooltip="Abnormal titer of ANA by immunofluorescence or an equivalent assay at any point in time" data-tooltip-position="top">11.ANA</span></label>
														  <select class="form-control" name="criteriasle199711" id="criteriasle199711">
															<option value="0" <?php if ($criteriasle199711==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle199711==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle199711==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle199711==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle199711" name="divdatesle199711" <?php if($criteriasle199711==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle199711" name="criteriadatesle199711" value="<?php echo $criteriadatesle199711_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle199711" name="calcagesle199711">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-4" id="divtotalscore1997" name="divtotalscore1997">
														  <label><span>SLE (1997 revised ACR Criteria): </span><input type="text" readonly id="sle1997" name="sle1997" value="<?php echo $sle1997; ?>"></label>
														 </div>
														 <div class="form-group col-md-4" id="divtotalscore1997" name="divtotalscore1997">
														  <label><span>Diagnosis date: </span><input type="text" readonly id="sle1997date" name="sle1997date" value="<?php echo $sle1997date; ?>"></label>
														  </div>
														 <div class="form-group col-md-4" id="divtotalscore1997" name="divtotalscore1997">
														  <label><span>Total score: </span><input type="text" readonly id="sle1997totalscore" name="sle1997totalscore" value="<?php echo $sle1997totalscore; ?>"></label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<label>SLE (2012 SLICC Criteria)
									<b><div id="divsle2012" name="divsle2012" <?php if($msg_sle2012=="") { echo 'style="DISPLAY:none;"'; } else  { echo 'style="DISPLAY:block;"'; } ?>><?php echo $msg_sle2012; ?></div></b>
										  </label>
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<label>A. Clinical criteria</label>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle20121" name="divcriteriasle20121">
														  <label><span data-tooltip="Lupus malar rash [do not count if malar discoid], bullous lupus, toxic epidermal necrolysis variant of SLE, maculopapular lupus rash, photosensitive lupus rash), or subcute cutaneous lupus (nonindurated psoriaform and/or annular polycyclic lesions that resolve without scarring)" data-tooltip-position="top">1.Acute cutaneous lupus</span></label>
														  <select class="form-control" name="criteriasle20121" id="criteriasle20121">
															<option value="0" <?php if ($criteriasle20121==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle20121==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle20121==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle20121==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle20121" name="divdatesle20121" <?php if($criteriasle20121==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle20121" name="criteriadatesle20121" value="<?php echo $criteriadatesle20121_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle20121" name="calcagesle20121">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle20122" name="divcriteriasle20122">
														  <label><span data-tooltip="Classic discoid rash: localized or generalized, hypertrophic [verrucous] lupus, lupus panniculitis [profundus], mucosal lupus, lupus erythematosus tumidus, chillblains lupus, discoid lupus/lichen planus overlap" data-tooltip-position="top">2.Chronic cutaneous lupus</span></label>
														  <select class="form-control" name="criteriasle20122" id="criteriasle20122">
															<option value="0" <?php if ($criteriasle20122==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle20122==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle20122==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle20122==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle20122" name="divdatesle20122" <?php if($criteriasle20122==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle20122" name="criteriadatesle20122" value="<?php echo $criteriadatesle20122_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle20122" name="calcagesle20122">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle20123" name="divcriteriasle20123">
														  <label><span data-tooltip="Diffuse thinning or hair fragility with visible broken hairs" data-tooltip-position="top">3.Non-scarring alopecia</span></label>
														  <select class="form-control" name="criteriasle20123" id="criteriasle20123">
															<option value="0" <?php if ($criteriasle20123==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle20123==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle20123==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle20123==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle20123" name="divdatesle20123" <?php if($criteriasle20123==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle20123" name="criteriadatesle20123" value="<?php echo $criteriadatesle20123_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle20123" name="calcagesle20123">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle20124" name="divcriteriasle20124">
														  <label><span data-tooltip="Oral or nasal ulcers" data-tooltip-position="top">4.Mucosal ulcers</span></label>
														  <select class="form-control" name="criteriasle20124" id="criteriasle20124">
															<option value="0" <?php if ($criteriasle20124==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle20124==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle20124==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle20124==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle20124" name="divdatesle20124" <?php if($criteriasle20124==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle20124" name="criteriadatesle20124" value="<?php echo $criteriadatesle20124_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle20124" name="calcagesle20124">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle20125" name="divcriteriasle20125">
														  <label><span data-tooltip="Inflammatory synovitis in ≥2 joints characterized by swelling or effusion, or tenderness and ≥30 minutes of morning stiffness" data-tooltip-position="top">5.Synovitis</span></label>
														  <select class="form-control" name="criteriasle20125" id="criteriasle20125">
															<option value="0" <?php if ($criteriasle20125==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle20125==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle20125==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle20125==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle20125" name="divdatesle20125" <?php if($criteriasle20125==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle20125" name="criteriadatesle20125" value="<?php echo $criteriadatesle20125_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle20125" name="calcagesle20125">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle20126" name="divcriteriasle20126">
														  <label><span data-tooltip="[a] Pleuritis: Typical pleurisy lasting >1 day, or pleural effusions, or pleural rub[b] Pericarditis: Typical pericardial pain (pain with recumbency improved by sitting forward) for >1 day, or pericardial effusion, or pericardial rub, or pericarditis by electrocardiography" data-tooltip-position="top">6.Serositis</span></label>
														  <select class="form-control" name="criteriasle20126" id="criteriasle20126">
															<option value="0" <?php if ($criteriasle20126==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle20126==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle20126==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle20126==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle20126" name="divdatesle20126" <?php if($criteriasle20126==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle20126" name="criteriadatesle20126" value="<?php echo $criteriadatesle20126_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle20126" name="calcagesle20126">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle20127" name="divcriteriasle20127">
														  <label><span data-tooltip="[a] Urine protein/creatinine (or 24 hr urine protein) representing ≥500 mg protein/24hr[b] Red blood cell casts data-tooltip-position="top">7.Renal disorder</span></label>
														  <select class="form-control" name="criteriasle20127" id="criteriasle20127">
															<option value="0" <?php if ($criteriasle20127==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle20127==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle20127==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle20127==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle20127" name="divdatesle20127" <?php if($criteriasle20127==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle20127" name="criteriadatesle20127" value="<?php echo $criteriadatesle20127_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle20127" name="calcagesle20127">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle20128" name="divcriteriasle20128">
														  <label><span data-tooltip="[a] seizures, [b] psychosis, [c] mononeuritis multiplex, [d] myelitis, [e] peripheral or cranial neuropathy, [f] cerebritis (acute confusional state)" data-tooltip-position="top">8.Neurologic disorder</span></label>
														  <select class="form-control" name="criteriasle20128" id="criteriasle20128">
															<option value="0" <?php if ($criteriasle20128==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle20128==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle20128==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle20128==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle20128" name="divdatesle20128" <?php if($criteriasle20128==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle20128" name="criteriadatesle20128" value="<?php echo $criteriadatesle20128_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle20128" name="calcagesle20128">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle20129" name="divcriteriasle20129">
														  <label><span data-tooltip="Haemolytic anaemia" data-tooltip-position="top">9.Haemolytic anaemia</span></label>
														  <select class="form-control" name="criteriasle20129" id="criteriasle20129">
															<option value="0" <?php if ($criteriasle20129==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle20129==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle20129==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle20129==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle20129" name="divdatesle20129" <?php if($criteriasle20129==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle20129" name="criteriadatesle20129" value="<?php echo $criteriadatesle20129_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle20129" name="calcagesle20129">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle201210" name="divcriteriasle201210">
														  <label><span data-tooltip="Leukopenia (<4000/mm3 at least once), or lymphopenia (<1000/mm3 at least once)" data-tooltip-position="top">10.Leukopenia</span></label>
														  <select class="form-control" name="criteriasle201210" id="criteriasle201210">
															<option value="0" <?php if ($criteriasle201210==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle201210==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle201210==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle201210==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle201210" name="divdatesle201210" <?php if($criteriasle201210==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle201210" name="criteriadatesle201210" value="<?php echo $criteriadatesle201210_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle201210" name="calcagesle201210">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle201211" name="divcriteriasle201211">
														  <label><span data-tooltip="Thrombocytopenia (<100,000/mm3) at least once" data-tooltip-position="top">11.Thrombocytopenia</span></label>
														  <select class="form-control" name="criteriasle201211" id="criteriasle201211">
															<option value="0" <?php if ($criteriasle201211==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle201211==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle201211==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle201211==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle201211" name="divdatesle201211" <?php if($criteriasle201211==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle201211" name="criteriadatesle201211" value="<?php echo $criteriadatesle201211_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle201211" name="calcagesle201211">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<label>B. Immunological criteria</label>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2012b1" name="divcriteriasle2012b1">
														  <label><span data-tooltip="Anti-dsDNA above laboratory reference range (except ELISA: twice above laboratory reference range) " data-tooltip-position="top">1.Anti-DNA</span></label>
														  <select class="form-control" name="criteriasle2012b1" id="criteriasle2012b1">
															<option value="0" <?php if ($criteriasle2012b1==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle2012b1==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle2012b1==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle2012b1==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2012b1" name="divdatesle2012b1" <?php if($criteriasle2012b1==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2012b1" name="criteriadatesle2012b1" value="<?php echo $criteriadatesle2012b1_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2012b1" name="calcagesle2012b1">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2012b2" name="divcriteriasle2012b2">
														  <label><span data-tooltip="Anti-Sm" data-tooltip-position="top">2.Anti-Sm</span></label>
														  <select class="form-control" name="criteriasle2012b2" id="criteriasle2012b2">
															<option value="0" <?php if ($criteriasle2012b2==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle2012b2==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle2012b2==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle2012b2==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2012b2" name="divdatesle2012b2" <?php if($criteriasle2012b2==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2012b2" name="criteriadatesle2012b2" value="<?php echo $criteriadatesle2012b2_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2012b2" name="calcagesle2012b2">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2012b3" name="divcriteriasle2012b3">
														  <label><span data-tooltip="Lupus anticoagulant, false-positive test for syphilis, anticardiolipin (at least twice normal or medium-high titer), or anti-b2 glycoprotein 1" data-tooltip-position="top">3.Anti-phospholipid Ab</span></label>
														  <select class="form-control" name="criteriasle2012b3" id="criteriasle2012b3">
															<option value="0" <?php if ($criteriasle2012b3==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle2012b3==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle2012b3==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle2012b3==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2012b3" name="divdatesle2012b3" <?php if($criteriasle2012b3==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2012b3" name="criteriadatesle2012b3" value="<?php echo $criteriadatesle2012b3_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2012b3" name="calcagesle2012b3">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2012b4" name="divcriteriasle2012b4">
														  <label><span data-tooltip="Low C3, or low C4, or low CH50" data-tooltip-position="top">4.Low complement</span></label>
														  <select class="form-control" name="criteriasle2012b4" id="criteriasle2012b4">
															<option value="0" <?php if ($criteriasle2012b4==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle2012b4==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle2012b4==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle2012b4==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2012b4" name="divdatesle2012b4" <?php if($criteriasle2012b4==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2012b4" name="criteriadatesle2012b4" value="<?php echo $criteriadatesle2012b4_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2012b4" name="calcagesle2012b4">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2012b5" name="divcriteriasle2012b5">
														  <label><span data-tooltip="Direct Coombs test in the absence of haemolytic anaemia" data-tooltip-position="top">5.Coombs test</span></label>
														  <select class="form-control" name="criteriasle2012b5" id="criteriasle2012b5">
															<option value="0" <?php if ($criteriasle2012b5==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle2012b5==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle2012b5==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle2012b5==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2012b5" name="divdatesle2012b5" <?php if($criteriasle2012b5==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2012b5" name="criteriadatesle2012b5" value="<?php echo $criteriadatesle2012b5_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2012b5" name="calcagesle2012b5">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2012b6" name="divcriteriasle2012b6">
														  <label><span data-tooltip="ANA above laboratory reference range" data-tooltip-position="top">6.ANA</span></label>
														  <select class="form-control" name="criteriasle2012b6" id="criteriasle2012b6">
															<option value="0" <?php if ($criteriasle2012b6==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle2012b6==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle2012b6==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle2012b6==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2012b6" name="divdatesle2012b6" <?php if($criteriasle2012b6==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2012b6" name="criteriadatesle2012b6" value="<?php echo $criteriadatesle2012b6_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2012b6" name="calcagesle2012b6">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2012c1" name="divcriteriasle2012c1">
														  <label>C. Renal criterion: biopsy-proven lupus nephritis in the presence of positive ANA or anti-dsDNA</label>
														  <select class="form-control" name="criteriasle2012c1" id="criteriasle2012c1">
															<option value="0" <?php if ($criteriasle2012c1==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle2012c1==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle2012c1==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle2012c1==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2012c1" name="divdatesle2012c1" <?php if($criteriasle2012c1==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2012c1" name="criteriadatesle2012c1" value="<?php echo $criteriadatesle2012c1_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2012c1" name="calcagesle2012c1">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-4" id="divtotalscore2012" name="divtotalscore2012">
														  <label><span>SLE (2012 SLICC Criteria): </span><input type="text" readonly id="sle2012" name="sle2012" value="<?php echo $sle2012; ?>"></label>
														 </div>
														 <div class="form-group col-md-4" id="divtotalscore2012" name="divtotalscore2012">
														  <label><span>Diagnosis date: </span><input type="text" readonly id="sle2012date" name="sle2012date" value="<?php echo $sle2012date; ?>"></label>
														  </div>
														 <div class="form-group col-md-4" id="divtotalscore2012" name="divtotalscore2012">
														 <!-- <label><span>Total score: </span>--><input type="hidden" readonly id="sle2012totalscore" name="sle2012totalscore" value="<?php echo $sle2012totalscore; ?>"><!--</label>-->
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<label>SLE (2019 EULAR/ACR Criteria)
									<b><div id="divsle2017" name="divsle2017" <?php if($msg_sle2017=="") { echo 'style="DISPLAY:none;"'; } else  { echo 'style="DISPLAY:block;"'; } ?>><?php echo $msg_sle2017; ?></div></b>
										  </label>
									<div class="row">
										<div class="col-md-12">
											
											<div class="row">
												<div class="col-md-12">
													<div class="row">
														<label>A. Entry criterion</label>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017a1" name="divcriteriasle2017a1">
														  <label><span data-tooltip="ANA above 1:80 by Hep2 immunofluorescence" data-tooltip-position="top">1.ANA</span></label>
														  <select class="form-control" name="criteriasle2017a1" id="criteriasle2017a1">
															<option value="0" <?php if ($criteriasle2017a1==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle2017a1==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle2017a1==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle2017a1==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2017a1" name="divdatesle2017a1" <?php if($criteriasle2017a1==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017a1" name="criteriadatesle2017a1" value="<?php echo $criteriadatesle2017a1_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017a1" name="calcagesle2017a1">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<label>B. Clinical and Immunological criteria</label>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b1" name="divcriteriasle2017b1">
														  <label><span data-tooltip="Fever: >38.3 Celsius with no other source identified" data-tooltip-position="top">1.Constitutional (2 points)</span></label>
														  <select class="form-control" name="criteriasle2017b1" id="criteriasle2017b1">
															<option value="0" <?php if ($criteriasle2017b1==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle2017b1==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle2017b1==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle2017b1==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2017b1" name="divdatesle2017b1" <?php if($criteriasle2017b1==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b1" name="criteriadatesle2017b1" value="<?php echo $criteriadatesle2017b1_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b1" name="calcagesle2017b1">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b21" name="divcriteriasle2017b21">
														  <label>2.Cutaneous</label>
														  </br>A. Non-scarring alopecia OR Oral ulcers (2 points): <input type="checkbox" name="criteriasle2017b21" id="criteriasle2017b21" <?php if ($criteriasle2017b21==1) { echo "checked"; } ?>>
														  </div>
														  <div   class="form-group col-md-4"  id="divdatesle2017b21" name="divdatesle2017b21" <?php if($criteriasle2017b21==1 ) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date A.</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b21" name="criteriadatesle2017b21" value="<?php echo $criteriadatesle2017b21_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b21" name="calcagesle2017b21">set date</a>
															</div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b22" name="divcriteriasle2017b22">
														  B. ACLE: malar rash or maculopapular lupus rash (6 points): <input type="checkbox" name="criteriasle2017b22" id="criteriasle2017b22" <?php if ($criteriasle2017b22==1) { echo "checked"; } ?>>
														 </div>
														  <div  class="form-group col-md-4" id="divdatesle2017b22" name="divdatesle2017b22" <?php if($criteriasle2017b22==1 ) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date B.</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b22" name="criteriadatesle2017b22" value="<?php echo $criteriadatesle2017b22_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b22" name="calcagesle2017b22">set date</a>
															</div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b23" name="divcriteriasle2017b23">
														  C. SCLE-(annular or papulosquamous eruption) OR DLE- classic discoid rash (4 points): <input type="checkbox" name="criteriasle2017b23" id="criteriasle2017b23" <?php if ($criteriasle2017b23==1) { echo "checked"; } ?>>
														</div>
														  <div class="form-group col-md-4"  id="divdatesle2017b23" name="divdatesle2017b23" <?php if($criteriasle2017b23==1 ) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date C.</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b23" name="criteriadatesle2017b23" value="<?php echo $criteriadatesle2017b23_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b23" name="calcagesle2017b23">set date</a>
															</div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b3" name="divcriteriasle2017b3">
														  <label><span data-tooltip="Synovitis in 2 or more joints with swelling or effusion, or tenderness and ≥30 minutes of morning stiffness" data-tooltip-position="top">3.Synovitis (6 points)</span></label>
														  <select class="form-control" name="criteriasle2017b3" id="criteriasle2017b3">
															<option value="0" <?php if ($criteriasle2017b3==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle2017b3==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle2017b3==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle2017b3==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2017b3" name="divdatesle2017b3" <?php if($criteriasle2017b3==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b3" name="criteriadatesle2017b3" value="<?php echo $criteriadatesle2017b3_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b3" name="calcagesle2017b3">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b4" name="divcriteriasle2017b4">
														  <label>4.Neurologic disorder</label>
														  </br>A. Delirium (2 points): <input type="checkbox" name="criteriasle2017b41" id="criteriasle2017b41" <?php if ($criteriasle2017b41==1) { echo "checked"; } ?>>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2017b41" name="divdatesle2017b41" <?php if($criteriasle2017b41==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date A.</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b41" name="criteriadatesle2017b41" value="<?php echo $criteriadatesle2017b41_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b41" name="calcagesle2017b41">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b4" name="divcriteriasle2017b4">
														  B. Psychosis (3 points): <input type="checkbox" name="criteriasle2017b42" id="criteriasle2017b42" <?php if ($criteriasle2017b42==1) { echo "checked"; } ?>>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2017b42" name="divdatesle2017b42" <?php if($criteriasle2017b42==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date B.</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b42" name="criteriadatesle2017b42" value="<?php echo $criteriadatesle2017b42_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b42" name="calcagesle2017b42">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b4" name="divcriteriasle2017b4">
														  C. Seizure (5 points): <input type="checkbox" name="criteriasle2017b43" id="criteriasle2017b43" <?php if ($criteriasle2017b43==1) { echo "checked"; } ?>>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2017b43" name="divdatesle2017b43" <?php if($criteriasle2017b43==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date C.</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b43" name="criteriadatesle2017b43" value="<?php echo $criteriadatesle2017b43_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b43" name="calcagesle2017b43">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b51" name="divcriteriasle2017b51">
														  <label>5.Serositis</label>
														 </br><span data-tooltip="A. Acute pericarditis with ≥2 of: (1) pericardial chest pain (sharp, worse with inspiration, improved by leaning forward), (2) pericardial rub, (3) EKG with new widespread ST-elevation or PR depression, (4) new or worsened pericardial effusion on imaging (such as ultrasound, x-ray, CT scan, MRI)" data-tooltip-position="top">A. pericarditis (6 points):</span> <input type="checkbox" name="criteriasle2017b51" id="criteriasle2017b51" <?php if ($criteriasle2017b51==1) { echo "checked"; } ?>>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2017b51" name="divdatesle2017b51" <?php if($criteriasle2017b51==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date A.</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b51" name="criteriadatesle2017b51" value="<?php echo $criteriadatesle2017b51_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b51" name="calcagesle2017b51">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b5" name="divcriteriasle2017b5">
														  B. Pleural and/or pericardial effusion with NO evidence of pericarditis (5 points): <input type="checkbox" name="criteriasle2017b52" id="criteriasle2017b52" <?php if ($criteriasle2017b52==1) { echo "checked"; } ?>>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2017b52" name="divdatesle2017b52" <?php if($criteriasle2017b52==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date B.</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b52" name="criteriadatesle2017b52" value="<?php echo $criteriadatesle2017b52_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b52" name="calcagesle2017b52">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b61" name="divcriteriasle2017b61">
														  <label>6.Hematologic disorder</label>
														  </br>A. Leukopenia <4000/mm3 at least once (3 points): <input type="checkbox" name="criteriasle2017b61" id="criteriasle2017b61" <?php if ($criteriasle2017b61==1) { echo "checked"; } ?>>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2017b61" name="divdatesle2017b61" <?php if($criteriasle2017b61==1 ) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date A.</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b61" name="criteriadatesle2017b61" value="<?php echo $criteriadatesle2017b61_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b61" name="calcagesle2017b61">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b62" name="divcriteriasle2017b62">
														  B. Thrombocytopenia <100,000/mm3 at least once (4 points): <input type="checkbox" name="criteriasle2017b62" id="criteriasle2017b62" <?php if ($criteriasle2017b62==1) { echo "checked"; } ?>>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2017b62" name="divdatesle2017b62" <?php if($criteriasle2017b62==1 ) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date B.</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b62" name="criteriadatesle2017b62" value="<?php echo $criteriadatesle2017b62_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b62" name="calcagesle2017b62">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b63" name="divcriteriasle2017b63">
														  C. Hemolytic anemia (4 points): <input type="checkbox" name="criteriasle2017b63" id="criteriasle2017b63" <?php if ($criteriasle2017b63==1) { echo "checked"; } ?>>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2017b63" name="divdatesle2017b63" <?php if($criteriasle2017b63==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date C.</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b63" name="criteriadatesle2017b63" value="<?php echo $criteriadatesle2017b63_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b63" name="calcagesle2017b63">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b71" name="divcriteriasle2017b71">
														  <label>7.Renal disorder</label>
														  </br>A. Proteinuria > 0.5gr/24h (4 points): <input type="checkbox" name="criteriasle2017b71" id="criteriasle2017b71" <?php if ($criteriasle2017b71==1) { echo "checked"; } ?>>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2017b71" name="divdatesle2017b71" <?php if($criteriasle2017b71==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date A.</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b71" name="criteriadatesle2017b71" value="<?php echo $criteriadatesle2017b71_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b71" name="calcagesle2017b71">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b72" name="divcriteriasle2017b72">
														  B. Lupus nephritis class II or V on renal biopsy (8 points): <input type="checkbox" name="criteriasle2017b72" id="criteriasle2017b72" <?php if ($criteriasle2017b72==1) { echo "checked"; } ?>>
														 </div>
														  <div  class="form-group col-md-4" id="divdatesle2017b72" name="divdatesle2017b72" <?php if($criteriasle2017b72==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date B.</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b72" name="criteriadatesle2017b72" value="<?php echo $criteriadatesle2017b72_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b72" name="calcagesle2017b72">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b73" name="divcriteriasle2017b73">
														  C. Lupus nephritis class III or IV on renal biopsy (10 points): <input type="checkbox" name="criteriasle2017b73" id="criteriasle2017b73" <?php if ($criteriasle2017b73==1) { echo "checked"; } ?>>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2017b73" name="divdatesle2017b73" <?php if($criteriasle2017b73==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date C.</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b73" name="criteriadatesle2017b73" value="<?php echo $criteriadatesle2017b73_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b73" name="calcagesle2017b73">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b8" name="divcriteriasle2017b8">
														  <label><span data-tooltip="Anticardiolipin IgG (>40 GPL units) or anti-β2GP1 IgG (>40 units) or  lupus anticoagulant positive" data-tooltip-position="top">8.Antiphospholipid Ab (2 points)</span></label>
														  <select class="form-control" name="criteriasle2017b8" id="criteriasle2017b8">
															<option value="0" <?php if ($criteriasle2017b8==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle2017b8==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle2017b8==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle2017b8==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2017b8" name="divdatesle2017b8" <?php if($criteriasle2017b8==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b8" name="criteriadatesle2017b8" value="<?php echo $criteriadatesle2017b8_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b8" name="calcagesle2017b8">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b91" name="divcriteriasle2017b91">
														  <label>9.Complement proteins</label>
														  </br>A. Low C3 or low C4 (3 points): <input type="checkbox" name="criteriasle2017b91" id="criteriasle2017b91" <?php if ($criteriasle2017b91==1) { echo "checked"; } ?>>
														 </div>
														  <div  class="form-group col-md-4" id="divdatesle2017b91" name="divdatesle2017b91" <?php if($criteriasle2017b91==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date A.</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b91" name="criteriadatesle2017b91" value="<?php echo $criteriadatesle2017b91_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b91" name="calcagesle2017b91">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b92" name="divcriteriasle2017b92">
														  B. Low C3  and  low C4 (4 points): <input type="checkbox" name="criteriasle2017b92" id="criteriasle2017b92" <?php if ($criteriasle2017b92==1) { echo "checked"; } ?>>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2017b92" name="divdatesle2017b92" <?php if($criteriasle2017b92==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date B.</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b92" name="criteriadatesle2017b92" value="<?php echo $criteriadatesle2017b92_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b92" name="calcagesle2017b92">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-8" id="divcriteriasle2017b10" name="divcriteriasle2017b10">
														  <label>10.Anti-dsDNA OR anti-Sm (6 points)</label>
														  <select class="form-control" name="criteriasle2017b10" id="criteriasle2017b10">
															<option value="0" <?php if ($criteriasle2017b10==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if ($criteriasle2017b10==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if ($criteriasle2017b10==2) { echo "selected"; } ?>>No</option>
															<option value="3" <?php if ($criteriasle2017b10==3) { echo "selected"; } ?>>Don't know</option>
														  </select>
														</div>
														  <div  class="form-group col-md-4" id="divdatesle2017b10" name="divdatesle2017b10" <?php if($criteriasle2017b10==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
															<label>Date</label>
															<div class="input-group date">
															  <div class="input-group-addon">
																<i class="fa fa-calendar"></i>
															  </div>
															  <input type="text" class="form-control" id="criteriadatesle2017b10" name="criteriadatesle2017b10" value="<?php echo $criteriadatesle2017b10_str; ?>" >
															</div>
															<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcagesle2017b10" name="calcagesle2017b10">set date</a>
															<!-- /.input group -->
														  </div>
													</div>
													<div class="row">
														<div class="form-group col-md-4" id="divtotalscore2017" name="divtotalscore2017">
														  <label><span>SLE (2017 EULAR/ACR Criteria): </span><input type="text" readonly id="sle2017" name="sle2017" value="<?php echo $sle2017; ?>"></label>
														 </div>
														 <div class="form-group col-md-4" id="divtotalscore2017" name="divtotalscore2017">
														  <label><span>Diagnosis date: </span><input type="text" readonly id="sle2017date" name="sle2017date" value="<?php echo $sle2017date; ?>"></label>
														  </div>
														 <div class="form-group col-md-4" id="divtotalscore2017" name="divtotalscore2017">
														  <label><span>Total score: </span><input type="text" readonly id="sle2017totalscore" name="sle2017totalscore" value="<?php echo $sle2017totalscore; ?>"></label>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									
								<?php
								}
								
								if($class_criteria6>0)
								{
									$order = get_apscriteria($pat_id);
									$result2 = pg_fetch_assoc($order);
									$patient_apscriteria_id=$result2['patient_apscriteria_id'];
									$criteriaaps1=$result2['criteria1'];
									$criteriadateaps1_str=$result2['date1_str'];
									if($criteriadateaps1_str=="12-12-1900")
										$criteriadateaps1_str="";
									$criteriaaps2=$result2['criteria2'];
									$criteriadateaps2_str=$result2['date2_str'];
									if($criteriadateaps2_str=="12-12-1900")
										$criteriadateaps2_str="";
									$criteriaaps3=$result2['criteria3'];
									$criteriadateaps3_str=$result2['date3_str'];
									if($criteriadateaps3_str=="12-12-1900")
										$criteriadateaps3_str="";
									$criteriaaps4=$result2['criteriab1'];
									$criteriadateaps4_str=$result2['dateb1_str'];
									if($criteriadateaps4_str=="12-12-1900")
										$criteriadateaps4_str="";
									$criteriaaps5=$result2['criteriab2'];
									$criteriadateaps5_str=$result2['dateb2_str'];
									if($criteriadateaps5_str=="12-12-1900")
										$criteriadateaps5_str="";
									$criteriaaps6=$result2['criteriab3'];
									$criteriadateaps6_str=$result2['dateb3_str'];
									if($criteriadateaps6_str=="12-12-1900")
										$criteriadateaps6_str="";
									$criteriaaps7=$result2['criteriab4'];
									$criteriadateaps7_str=$result2['dateb4_str'];
									if($criteriadateaps7_str=="12-12-1900")
										$criteriadateaps7_str="";
									$criteriaaps8=$result2['criteriab5'];
									$criteriadateaps8_str=$result2['dateb4_str'];
									if($criteriadateaps8_str=="12-12-1900")
										$criteriadateaps8_str="";
									$aps=$result2['aps'];
									$apsdate=$result2['apsdate_str'];
									if($apsdate=="12-12-1900")
										$apsdate="";
									$apstotalscore=$result2['totalscore'];
									
									$msg_aps="";
									
									if($aps=="Yes")
										$msg_aps="Clasiffied as APS ";
									if($apsdate<>"")
										$msg_aps .= " at $apsdate ";
									if($apstotalscore<>"0" and $apstotalscore<>"")
										$msg_aps .= " with score $apstotalscore";
								?>
								
								  <label>APS criteria
								  <b><div id="divapsclassified" name="divapsclassified" <?php if($msg_aps=="") { echo 'style="DISPLAY:none;"'; } else  { echo 'style="DISPLAY:block;"'; } ?>><?php echo $msg_aps; ?></div></b>
								  </label>
									<div class="row">
										<div class="col-md-12">
											<div class="row">
												<div class="form-group col-md-12" id="divcriteria3" name="divcriteria3">
												  <label>A. Laboratory criteria</label>
												</div>
											</div>
											<div class="row">
												<input type="hidden" value="<?php echo $patient_apscriteria_id; ?>" id="patient_apscriteria_id" name="patient_apscriteria_id"/>
												<div class="form-group col-md-8" id="divcriteriaaps1" name="divcriteriaaps1">
												  <label><span data-tooltip="Anticardiolipin antibody (IgG and/or IgM) in serum or plasma, present in medium or high titers (>40 GPL/MPL or >99th percentile) on 2 or more occasions at least 12 weeks apart, measured by ELISA" data-tooltip-position="top">1.Anticardiolipin antibody present</span></label>
												  <select class="form-control" name="criteriaaps1" id="criteriaaps1">
													<option value="0" <?php if ($criteriaaps1==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaps1==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaps1==2) { echo "selected"; } ?>>No</option>
													<option value="2" <?php if ($criteriaaps1==2) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaps1" name="divdateaps1" <?php if($criteriaaps1==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaps1" name="criteriadateaps1" value="<?php echo $criteriadateaps1_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaps1" name="calcageaps1">set date</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaps2" name="divcriteriaaps2">
												  <label><span data-tooltip="Lupus anticoagulant present in plasma on 2 or more occasions at least 12 weeks apart detected according to the guidelines of ISTH" data-tooltip-position="top">2.Lupus anticoagulant present</span></label>
												  <select class="form-control" name="criteriaaps2" id="criteriaaps2">
													<option value="0" <?php if ($criteriaaps2==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaps2==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaps2==2) { echo "selected"; } ?>>No</option>
													<option value="2" <?php if ($criteriaaps2==2) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaps2" name="divdateaps2" <?php if($criteriaaps2==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaps2" name="criteriadateaps2" value="<?php echo $criteriadateaps2_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaps2" name="calcageaps2">set date</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaps3" name="divcriteriaaps3">
												  <label><span data-tooltip="Anti-β2 glycoprotein-I antibody (IgG and/or IgM) in serum or plasma, present in medium or high titers (>40 GPL/MPL or >99th percentile) on 2 or more occasions at least 12 weeks apart, measured by ELISA" data-tooltip-position="top">3.Anti-β2 glycoprotein-I antibody present</span></label>
												  <select class="form-control" name="criteriaaps3" id="criteriaaps3">
													<option value="0" <?php if ($criteriaaps3==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaps3==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaps3==2) { echo "selected"; } ?>>No</option>
													<option value="2" <?php if ($criteriaaps3==2) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaps3" name="divdateaps3" <?php if($criteriaaps3==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaps3" name="criteriadateaps3" value="<?php echo $criteriadateaps3_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaps3" name="calcageaps3">set date</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-12" id="divcriteria3" name="divcriteria3">
												  <label>B. Clinical criteria</label>
												</div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaps4" name="divcriteriaaps4">
												  <label><span data-tooltip="" data-tooltip-position="top">1.Arterial thrombosis</span></label>
												  <select class="form-control" name="criteriaaps4" id="criteriaaps4">
													<option value="0" <?php if ($criteriaaps4==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaps4==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaps4==2) { echo "selected"; } ?>>No</option>
													<option value="2" <?php if ($criteriaaps4==2) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaps4" name="divdateaps4" <?php if($criteriaaps4==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaps4" name="criteriadateaps4" value="<?php echo $criteriadateaps4_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaps4" name="calcageaps4">set date</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaps5" name="divcriteriaaps5">
												  <label><span data-tooltip="" data-tooltip-position="top">2.Vascular thrombosis</span></label>
												  <select class="form-control" name="criteriaaps5" id="criteriaaps5">
													<option value="0" <?php if ($criteriaaps5==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaps5==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaps5==2) { echo "selected"; } ?>>No</option>
													<option value="2" <?php if ($criteriaaps5==2) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaps5" name="divdateaps5" <?php if($criteriaaps5==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaps5" name="criteriadateaps5" value="<?php echo $criteriadateaps5_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaps5" name="calcageaps5">set date</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaps6" name="divcriteriaaps6">
												  <label><span data-tooltip="Recurrent (≥3) pregnancy losses before the 10th week of gestation (if YES, date of last pregnancy loss)" data-tooltip-position="top">3.Recurrent pregnancy losses</span></label>
												  <select class="form-control" name="criteriaaps6" id="criteriaaps6">
													<option value="0" <?php if ($criteriaaps6==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaps6==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaps6==2) { echo "selected"; } ?>>No</option>
													<option value="2" <?php if ($criteriaaps6==2) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaps6" name="divdateaps6" <?php if($criteriaaps6==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaps6" name="criteriadateaps6" value="<?php echo $criteriadateaps6_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaps6" name="calcageaps6">set date</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaps7" name="divcriteriaaps7">
												  <label><span data-tooltip="Unexplained death of featus at or beyond the 10th week of gestation" data-tooltip-position="top">4.Death of featus ≥10th week</span></label>
												  <select class="form-control" name="criteriaaps7" id="criteriaaps7">
													<option value="0" <?php if ($criteriaaps7==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaps7==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaps7==2) { echo "selected"; } ?>>No</option>
													<option value="2" <?php if ($criteriaaps7==2) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaps7" name="divdateaps7" <?php if($criteriaaps7==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaps7" name="criteriadateaps7" value="<?php echo $criteriadateaps7_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaps7" name="calcageaps7">set date</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-8" id="divcriteriaaps8" name="divcriteriaaps8">
												  <label><span data-tooltip="Pre-term birth (before 34th week of gestation) due to (pre-)eclampsia or placental insufficiency" data-tooltip-position="top">5.Pre-term birth due to pre-eclampsia/eclampsia/placental insufficiency</span></label>
												  <select class="form-control" name="criteriaaps8" id="criteriaaps8">
													<option value="0" <?php if ($criteriaaps8==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if ($criteriaaps8==1) { echo "selected"; } ?>>Yes</option>
													<option value="2" <?php if ($criteriaaps8==2) { echo "selected"; } ?>>No</option>
													<option value="2" <?php if ($criteriaaps8==2) { echo "selected"; } ?>>Don't know</option>
												  </select>
												</div>
												  <div  class="form-group col-md-4" id="divdateaps8" name="divdateaps8" <?php if($criteriaaps8==0) { ?> style="display:none;" <?php } else { ?> style="display:block;" <?php } ?>>
													<label>Date</label>
													<div class="input-group date">
													  <div class="input-group-addon">
														<i class="fa fa-calendar"></i>
													  </div>
													  <input type="text" class="form-control" id="criteriadateaps8" name="criteriadateaps8" value="<?php echo $criteriadateaps8_str; ?>" >
													</div>
													<a class="btn btn-primary btn-sm" href="javascript:void(0);" id="calcageaps8" name="calcageaps8">set date</a>
													<!-- /.input group -->
												  </div>
											</div>
											<div class="row">
												<div class="form-group col-md-4" id="divtotalscore" name="divtotalscore">
												  <label><span>ANTIPHOSPHOLIPID SYNDROME (APS): </span><input type="text" readonly id="aps" name="aps" value="<?php echo $aps; ?>"></label>
												 </div>
												 <div class="form-group col-md-4" id="divtotalscore" name="divtotalscore">
												  <label><span>Diagnosis date: </span><input type="text" readonly id="apsdate" name="apsdate" value="<?php echo $apsdate; ?>"></label>
												  </div>
												 <div class="form-group col-md-4" id="divtotalscore" name="divtotalscore">
												  <input type="hidden" readonly id="apstotalscore" name="apstotalscore" value="0">
												</div>
											</div>
										</div>
									</div>
								<?php
								}
								?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<input type="submit" id="submitbtn" name="submitbtn" class="btn btn-primary" value="Save">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				&nbsp;
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				&nbsp;
			</div>
		</div>
		</form>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
   <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>

$(function () {
	  
	   $('#form').submit(function(){
		
		var msg="";
		 
		var sum_active=0;
		var sum_inactive=0;
		var sum_active_main=0;
		var sum_inactive_main=0;
		var sum_date=0;
		 
			
		var rows=document.getElementById('diagnosis_numofrows').value;
		for(i = 1; i <= rows; i++)
		{
			if($('#main_'+i).prop('checked') && document.getElementById('inactive_'+i).checked==true)
				sum_inactive_main++;
			
			if($('#main_'+i).prop('checked') && document.getElementById('inactive_'+i).checked==false)
				sum_active_main++;
			
			if(document.getElementById('inactive_'+i).checked==false)
				sum_active++;
			
			if(document.getElementById('inactive_'+i).checked==true)
				sum_inactive++;
			
			if($('#disease_date_'+i).val()=="" && $('#diagnosis_hidden_'+i).val()!="-1")
				sum_date++;
		}
	
		if(sum_active>0 && sum_active_main==0)
			msg +='-You must have at least one main diagnosis for active diagnosis!\n';
		
		if(sum_inactive>0 && sum_inactive_main==0)
			msg +='-You must have at least one main diagnosis for inactive diagnosis!\n';
		
		if(sum_date>0)
			msg +='-You must fill all date of diasease diagnosis according to physician!\n';
			
		if(msg != '')
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
		
		
	  });
	  
//function setmain(id)
//{
	
//}


$('#dateb1').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#dateb2').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#dateb3').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#dateb4').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#date1').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#date2').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });


/*$('#dateb1').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#dateb2').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#dateb3').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#dateb4').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#date1').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#date2').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
*/


$('#criteriaperipheraldate1').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriaperipheraldate2').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriaperipheraldate3').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriaperipheraldate4').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriaperipheraldate5').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriaperipheraldate6').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriaperipheraldate7').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriaperipheraldate8').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriaperipheraldate9').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriaperipheraldate10').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriaperipheraldate11').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriaperipheraldate12').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriaperipheraldate13').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriaperipheraldate14').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriaperipheraldate15').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriaperipheraldate16').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });


/*
$('#criteriaperipheraldate1').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriaperipheraldate2').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriaperipheraldate3').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriaperipheraldate4').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriaperipheraldate5').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriaperipheraldate6').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriaperipheraldate7').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriaperipheraldate8').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriaperipheraldate9').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriaperipheraldate10').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriaperipheraldate11').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriaperipheraldate12').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriaperipheraldate13').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriaperipheraldate14').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriaperipheraldate15').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriaperipheraldate16').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
*/

$('#criteriadateaxial1').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaxial2').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaxial3').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaxial4').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaxial5').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaxial6').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaxial7').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaxial8').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaxial9').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaxial10').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaxial11').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaxial12').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaxial13').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaxial14').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaxial15').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaxial16').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });

/*
$('#criteriadateaxial1').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaxial2').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaxial3').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaxial4').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaxial5').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaxial6').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaxial7').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaxial8').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaxial9').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaxial10').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaxial11').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaxial12').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaxial13').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaxial14').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaxial15').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaxial16').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
*/

$('#criteriadatecaspar1').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatecaspar2').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatecaspar3').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatecaspar4').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatecaspar5').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });

/*
$('#criteriadatecaspar1').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatecaspar2').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatecaspar3').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatecaspar4').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatecaspar5').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
*/

$('#criteriadatesle19971').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle19972').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle19973').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle19974').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle19975').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle19976').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle19977').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle19978').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle19979').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle199710').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle199711').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });

/*
$('#criteriadatesle19971').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle19972').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle19973').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle19974').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle19975').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle19976').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle19977').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle19978').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle19979').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle199710').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle199711').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
*/

$('#criteriadatesle20121').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle20122').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle20123').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle20124').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle20125').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle20126').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle20127').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle20128').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle20129').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle201210').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle201211').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });

/*

$('#criteriadatesle20121').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle20122').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle20123').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle20124').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle20125').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle20126').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle20127').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle20128').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle20129').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle201210').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle201211').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
*/

$('#criteriadatesle2012b1').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2012b2').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2012b3').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2012b4').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2012b5').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2012b6').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2012c1').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017a1').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });

/*
$('#criteriadatesle2012b1').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2012b2').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2012b3').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2012b4').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2012b5').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2012b6').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2012c1').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017a1').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
*/

$('#criteriadatesle2017b1').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017b21').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017b22').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017b23').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017b3').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017b41').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017b42').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017b43').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017b51').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017b52').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017b61').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017b62').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017b63').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017b71').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017b72').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017b73').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017b8').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017b91').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017b92').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadatesle2017b10').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });

/*
$('#criteriadatesle2017b1').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017b21').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017b22').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017b23').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017b3').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017b41').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017b42').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017b43').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017b51').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017b52').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017b61').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017b62').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017b63').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017b71').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017b72').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017b73').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017b8').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017b91').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017b92').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadatesle2017b10').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
*/

$('#criteriadateaps1').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaps2').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaps3').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaps4').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaps5').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaps6').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaps7').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
$('#criteriadateaps8').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });

/*
$('#criteriadateaps1').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaps2').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaps3').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaps4').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaps5').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaps6').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaps7').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
$('#criteriadateaps8').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	});
*/
	


$(function () {


$("#criteriasle2017a1").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle2017a1").show();
	 else
		 $("#divdatesle2017a1").hide();
    });
	
	$("#criteriasle2017b1").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle2017b1").show();
	 else
		 $("#divdatesle2017b1").hide();
    });
	
	/*$('#criteriasle2017b21').click(function() {
        if (!($('#criteriasle2017b21').is(':checked')) && !($('#criteriasle2017b22').is(':checked')) && !($('#criteriasle2017b23').is(':checked'))) 
           $("#divdatesle2017b2").hide();
	   else
		   $("#divdatesle2017b2").show();
    });
	
	$('#criteriasle2017b22').click(function() {
        if (!($('#criteriasle2017b21').is(':checked')) && !($('#criteriasle2017b22').is(':checked')) && !($('#criteriasle2017b23').is(':checked'))) 
           $("#divdatesle2017b2").hide();
	   else
		   $("#divdatesle2017b2").show();
    });
	
	$('#criteriasle2017b23').click(function() {
        if (!($('#criteriasle2017b21').is(':checked')) && !($('#criteriasle2017b22').is(':checked')) && !($('#criteriasle2017b23').is(':checked'))) 
           $("#divdatesle2017b2").hide();
	   else
		   $("#divdatesle2017b2").show();
    });*/
	
	$('#criteriasle2017b21').click(function() {
        if (!($('#criteriasle2017b21').is(':checked'))) 
           $("#divdatesle2017b21").hide();
	   else
		   $("#divdatesle2017b21").show();
    });
	
	$('#criteriasle2017b22').click(function() {
       
	   if (!($('#criteriasle2017b22').is(':checked'))) 
           $("#divdatesle2017b22").hide();
	   else
		   $("#divdatesle2017b22").show();
    });
	
	$('#criteriasle2017b23').click(function() {
      
	   if (!($('#criteriasle2017b23').is(':checked'))) 
           $("#divdatesle2017b23").hide();
	   else
		   $("#divdatesle2017b23").show();
    });
	
	$("#criteriasle2017b3").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle2017b3").show();
	 else
		 $("#divdatesle2017b3").hide();
    });
	
	$('#criteriasle2017b41').click(function() {
        if (!($('#criteriasle2017b41').is(':checked'))) 
           $("#divdatesle2017b41").hide();
	   else
		   $("#divdatesle2017b41").show();
    });
	
	$('#criteriasle2017b42').click(function() {
        if (!($('#criteriasle2017b42').is(':checked'))) 
           $("#divdatesle2017b42").hide();
	   else
		   $("#divdatesle2017b42").show();
    });
	
	$('#criteriasle2017b43').click(function() {
        if (!($('#criteriasle2017b43').is(':checked'))) 
           $("#divdatesle2017b43").hide();
	   else
		   $("#divdatesle2017b43").show();
    });
	
	$('#criteriasle2017b51').click(function() {
        if (!($('#criteriasle2017b51').is(':checked'))) 
           $("#divdatesle2017b51").hide();
	   else
		   $("#divdatesle2017b51").show();
    });
	
	$('#criteriasle2017b52').click(function() {
        if (!($('#criteriasle2017b52').is(':checked'))) 
           $("#divdatesle2017b52").hide();
	   else
		   $("#divdatesle2017b52").show();
    });
	
	$('#criteriasle2017b61').click(function() {
        if (!($('#criteriasle2017b61').is(':checked'))) 
           $("#divdatesle2017b61").hide();
	   else
		   $("#divdatesle2017b61").show();
    });
	
	$('#criteriasle2017b62').click(function() {
        if (!($('#criteriasle2017b62').is(':checked'))) 
           $("#divdatesle2017b62").hide();
	   else
		   $("#divdatesle2017b62").show();
    });
	
	$('#criteriasle2017b63').click(function() {
        if (!($('#criteriasle2017b63').is(':checked'))) 
           $("#divdatesle2017b63").hide();
	   else
		   $("#divdatesle2017b63").show();
    });
	
	$('#criteriasle2017b71').click(function() {
        if (!($('#criteriasle2017b71').is(':checked'))) 
           $("#divdatesle2017b71").hide();
	   else
		   $("#divdatesle2017b71").show();
    });
	
	$('#criteriasle2017b72').click(function() {
        if (!($('#criteriasle2017b72').is(':checked'))) 
           $("#divdatesle2017b72").hide();
	   else
		   $("#divdatesle2017b72").show();
    });
	
	$('#criteriasle2017b73').click(function() {
        if (!($('#criteriasle2017b73').is(':checked'))) 
           $("#divdatesle2017b73").hide();
	   else
		   $("#divdatesle2017b73").show();
    });
	
	$("#criteriasle2017b8").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle2017b8").show();
	 else
		 $("#divdatesle2017b8").hide();
    });
	
	$('#criteriasle2017b91').click(function() {
        if (!($('#criteriasle2017b91').is(':checked'))) 
           $("#divdatesle2017b91").hide();
	   else
		   $("#divdatesle2017b91").show();
    });
	
	$('#criteriasle2017b92').click(function() {
        if (!($('#criteriasle2017b92').is(':checked'))) 
           $("#divdatesle2017b92").hide();
	   else
		   $("#divdatesle2017b92").show();
    });
	
	$("#criteriasle2017b10").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle2017b10").show();
	 else
		 $("#divdatesle2017b10").hide();
    });

$("#calcagesle2017a1").click(function() { setdatesle2017('calcagesle2017a1'); });
$("#calcagesle2017b1").click(function() { setdatesle2017('calcagesle2017b1'); });
$("#calcagesle2017b21").click(function() { setdatesle2017('calcagesle2017b21'); });
$("#calcagesle2017b22").click(function() { setdatesle2017('calcagesle2017b22'); });
$("#calcagesle2017b23").click(function() { setdatesle2017('calcagesle2017b23'); });
$("#calcagesle2017b3").click(function() { setdatesle2017('calcagesle2017b3'); });
$("#calcagesle2017b41").click(function() { setdatesle2017('calcagesle2017b41'); });
$("#calcagesle2017b42").click(function() { setdatesle2017('calcagesle2017b42'); });
$("#calcagesle2017b43").click(function() { setdatesle2017('calcagesle2017b43'); });
$("#calcagesle2017b51").click(function() { setdatesle2017('calcagesle2017b51'); });
$("#calcagesle2017b52").click(function() { setdatesle2017('calcagesle2017b52'); });
$("#calcagesle2017b61").click(function() { setdatesle2017('calcagesle2017b61'); });
$("#calcagesle2017b62").click(function() { setdatesle2017('calcagesle2017b62'); });
$("#calcagesle2017b63").click(function() { setdatesle2017('calcagesle2017b63'); });
$("#calcagesle2017b71").click(function() { setdatesle2017('calcagesle2017b71'); });
$("#calcagesle2017b72").click(function() { setdatesle2017('calcagesle2017b72'); });
$("#calcagesle2017b73").click(function() { setdatesle2017('calcagesle2017b73'); });
$("#calcagesle2017b8").click(function() { setdatesle2017('calcagesle2017b8'); });
$("#calcagesle2017b91").click(function() { setdatesle2017('calcagesle2017b91'); });
$("#calcagesle2017b92").click(function() { setdatesle2017('calcagesle2017b92'); });
$("#calcagesle2017b10").click(function() { setdatesle2017('calcagesle2017b10'); });
	
$("#criteriasle2017a1").change(function() { calculatesle2017(); });
$("#criteriasle2017b1").change(function() { calculatesle2017(); });
$("#criteriasle2017b21").click(function() { calculatesle2017(); });
$("#criteriasle2017b22").click(function() { calculatesle2017(); });
$("#criteriasle2017b23").click(function() { calculatesle2017(); });
$("#criteriasle2017b3").change(function() { calculatesle2017(); });
$("#criteriasle2017b41").click(function() { calculatesle2017(); });
$("#criteriasle2017b42").click(function() { calculatesle2017(); });
$("#criteriasle2017b43").click(function() { calculatesle2017(); });
$("#criteriasle2017b51").click(function() { calculatesle2017(); });
$("#criteriasle2017b52").click(function() { calculatesle2017(); });
$("#criteriasle2017b61").click(function() { calculatesle2017(); });
$("#criteriasle2017b62").click(function() { calculatesle2017(); });
$("#criteriasle2017b63").click(function() { calculatesle2017(); });
$("#criteriasle2017b71").click(function() { calculatesle2017(); });
$("#criteriasle2017b72").click(function() { calculatesle2017(); });
$("#criteriasle2017b73").click(function() { calculatesle2017(); });
$("#criteriasle2017b8").change(function() { calculatesle2017(); });
$("#criteriasle2017b91").click(function() { calculatesle2017(); });
$("#criteriasle2017b92").click(function() { calculatesle2017(); });
$("#criteriasle2017b10").change(function() { calculatesle2017(); });

function calculatesle2017(date)
{
		var vala1 = $("#criteriasle2017a1").val();
		var valb1 = $("#criteriasle2017b1").val();
		var valb21 = $("#criteriasle2017b21").val();
		var valb22 = $("#criteriasle2017b22").val();
		var valb23 = $("#criteriasle2017b23").val();
		var valb3 = $("#criteriasle2017b3").val();
		var valb41 = $("#criteriasle2017b41").val();
		var valb42 = $("#criteriasle2017b42").val();
		var valb43 = $("#criteriasle2017b43").val();
		var valb51 = $("#criteriasle2017b51").val();
		var valb52 = $("#criteriasle2017b52").val();
		var valb61 = $("#criteriasle2017b61").val();
		var valb62 = $("#criteriasle2017b62").val();
		var valb63 = $("#criteriasle2017b63").val();
		var valb71 = $("#criteriasle2017b71").val();
		var valb72 = $("#criteriasle2017b72").val();
		var valb73 = $("#criteriasle2017b73").val();
		var valb8 = $("#criteriasle2017b8").val();
		var valb91 = $("#criteriasle2017b91").val();
		var valb92 = $("#criteriasle2017b92").val();
		var valb10 = $("#criteriasle2017b10").val();
		
		var inta1=0;
		var int1=0;
		var int2=0;
		var int3=0;
		var int4=0;
		var int5=0;
		var int6=0;
		var int7=0;
		var int8=0;
		var int9=0;
		var int10=0;
		
		if(vala1==1)
			inta1=1;
		if(valb1==1)
			int1=2;
		if(valb3==1)
			int3=6;
		if(valb8==1)
			int8=2;
		if(valb10==1)
			int10=6;
		
		if ($('#criteriasle2017b21').is(':checked')) 
			int2=2;
		if ($('#criteriasle2017b23').is(':checked')) 
			int2=4;
		if ($('#criteriasle2017b22').is(':checked')) 
			int2=6;
		
		if ($('#criteriasle2017b41').is(':checked')) 
			int4=2;
		if ($('#criteriasle2017b42').is(':checked')) 
			int4=3;
		if ($('#criteriasle2017b43').is(':checked')) 
			int4=5;
		
		if ($('#criteriasle2017b52').is(':checked')) 
			int5=5;
		if ($('#criteriasle2017b51').is(':checked')) 
			int5=6;
		
		if ($('#criteriasle2017b61').is(':checked')) 
			int6=3;
		if ($('#criteriasle2017b62').is(':checked')) 
			int6=4;
		if ($('#criteriasle2017b63').is(':checked')) 
			int6=4;
		
		if ($('#criteriasle2017b71').is(':checked')) 
			int7=4;
		if ($('#criteriasle2017b72').is(':checked')) 
			int7=8;
		if ($('#criteriasle2017b73').is(':checked')) 
			int7=10;
		
		if ($('#criteriasle2017b91').is(':checked')) 
			int9=3;
		if ($('#criteriasle2017b92').is(':checked')) 
			int9=4;
		
		
		
		var sum = int1 + int2 + int3 + int4 + int5 + int6 + int7 + int8 + int9 + int10;
		
		if (inta1==1 && sum>=10)
		{
			if($("#sle2017date").val()=="")
				$("#divsle2017").html("<label>Clasiffied as SLE according to 2017 EULAR/ACR Criteria with score " + sum + "</label>");
			else
				$("#divsle2017").html("<label>Clasiffied as SLE according to 2017 EULAR/ACR Criteria at " + $("#sle2017date").val()+" with score " + sum + "</label>");
			
			$("#sle2017").val('Yes');
			$("#sle2017totalscore").val(sum);
			$("#divsle2017").show();
			if($("#sle2017date").val()=="")
				alert("set 2017 EULAR/ACR Criteria date!");
		}
		else
		{
			$("#divsle2017").empty();
			$("#divsle2017").hide();
			$("#sle2017").val('No');
			$("#sle2017totalscore").val('');
			$("#sle2017date").val('');
		}
	}
	
	
function setdatesle2017(date)
{
		if(date=="calcagesle2017a1")
			$("#sle2017date").val($("#criteriadatesle2017a1").val());
		if(date=="calcagesle2017b1")
			$("#sle2017date").val($("#criteriadatesle2017b1").val());
		if(date=="calcagesle2017b21")
			$("#sle2017date").val($("#criteriadatesle2017b21").val());
		if(date=="calcagesle2017b22")
			$("#sle2017date").val($("#criteriadatesle2017b22").val());
		if(date=="calcagesle2017b23")
			$("#sle2017date").val($("#criteriadatesle2017b23").val());
		if(date=="calcagesle2017b3")
			$("#sle2017date").val($("#criteriadatesle2017b3").val());
		if(date=="calcagesle2017b41")
			$("#sle2017date").val($("#criteriadatesle2017b41").val());
		if(date=="calcagesle2017b42")
			$("#sle2017date").val($("#criteriadatesle2017b42").val());
		if(date=="calcagesle2017b43")
			$("#sle2017date").val($("#criteriadatesle2017b43").val());
		if(date=="calcagesle2017b51")
			$("#sle2017date").val($("#criteriadatesle2017b51").val());
		if(date=="calcagesle2017b52")
			$("#sle2017date").val($("#criteriadatesle2017b52").val());
		if(date=="calcagesle2017b61")
			$("#sle2017date").val($("#criteriadatesle2017b61").val());
		if(date=="calcagesle2017b62")
			$("#sle2017date").val($("#criteriadatesle2017b62").val());
		if(date=="calcagesle2017b63")
			$("#sle2017date").val($("#criteriadatesle2017b63").val());
		if(date=="calcagesle2017b71")
			$("#sle2017date").val($("#criteriadatesle2017b71").val());
		if(date=="calcagesle2017b72")
			$("#sle2017date").val($("#criteriadatesle2017b72").val());
		if(date=="calcagesle2017b73")
			$("#sle2017date").val($("#criteriadatesle2017b73").val());
		if(date=="calcagesle2017b8")
			$("#sle2017date").val($("#criteriadatesle2017b8").val());
		if(date=="calcagesle2017b91")
			$("#sle2017date").val($("#criteriadatesle2017b91").val());
		if(date=="calcagesle2017b92")
			$("#sle2017date").val($("#criteriadatesle2017b92").val());
		if(date=="calcagesle2017b10")
			$("#sle2017date").val($("#criteriadatesle2017b10").val());
			
		if($("#sle2017").val()=="Yes")
		{
			$("#divsle2017").html("<label>Clasiffied as SLE according to 2017 EULAR/ACR Criteria at " + $("#sle2017date").val()+" with score " + $("#sle2017totalscore").val() + "</label>");
			$("#divsle2017").show();
		}
		else
		{
			$("#divsle2017").empty();
			$("#divsle2017").hide();
		}
}

	
$("#criteriasle20121").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle20121").show();
	 else
		 $("#divdatesle20121").hide();
    });
	
$("#criteriasle20122").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle20122").show();
	 else
		 $("#divdatesle20122").hide();
    });
	
$("#criteriasle20123").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle20123").show();
	 else
		 $("#divdatesle20123").hide();
    });
	
$("#criteriasle20124").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle20124").show();
	 else
		 $("#divdatesle20124").hide();
    });
	
	$("#criteriasle20125").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle20125").show();
	 else
		 $("#divdatesle20125").hide();
    });
	
	$("#criteriasle20126").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle20126").show();
	 else
		 $("#divdatesle20126").hide();
    });
	
	$("#criteriasle20127").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle20127").show();
	 else
		 $("#divdatesle20127").hide();
    });
	
	$("#criteriasle20128").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle20128").show();
	 else
		 $("#divdatesle20128").hide();
    });
	
	$("#criteriasle20129").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle20129").show();
	 else
		 $("#divdatesle20129").hide();
    });
	
	$("#criteriasle201210").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle201210").show();
	 else
		 $("#divdatesle201210").hide();
    });
	
	$("#criteriasle201211").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle201211").show();
	 else
		 $("#divdatesle201211").hide();
    });

$("#criteriasle2012b1").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle2012b1").show();
	 else
		 $("#divdatesle2012b1").hide();
    });
	
$("#criteriasle2012b2").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle2012b2").show();
	 else
		 $("#divdatesle2012b2").hide();
    });
	
$("#criteriasle2012b3").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle2012b3").show();
	 else
		 $("#divdatesle2012b3").hide();
    });
	
$("#criteriasle2012b4").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle2012b4").show();
	 else
		 $("#divdatesle2012b4").hide();
    });
	
	$("#criteriasle2012b5").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle2012b5").show();
	 else
		 $("#divdatesle2012b5").hide();
    });
	
	$("#criteriasle2012b6").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle2012b6").show();
	 else
		 $("#divdatesle2012b6").hide();
    });
	
	$("#criteriasle2012c1").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle2012c1").show();
	 else
		 $("#divdatesle2012c1").hide();
    });

$("#criteriasle20121").change(function() { calculatesle2012(); });
$("#criteriasle20122").change(function() { calculatesle2012(); });
$("#criteriasle20123").change(function() { calculatesle2012(); });
$("#criteriasle20124").change(function() { calculatesle2012(); });
$("#criteriasle20125").change(function() { calculatesle2012(); });
$("#criteriasle20126").change(function() { calculatesle2012(); });
$("#criteriasle20127").change(function() { calculatesle2012(); });
$("#criteriasle20128").change(function() { calculatesle2012(); });
$("#criteriasle20129").change(function() { calculatesle2012(); });
$("#criteriasle201210").change(function() { calculatesle2012(); });
$("#criteriasle201211").change(function() { calculatesle2012(); });
$("#criteriasle2012b1").change(function() { calculatesle2012(); });
$("#criteriasle2012b2").change(function() { calculatesle2012(); });
$("#criteriasle2012b3").change(function() { calculatesle2012(); });
$("#criteriasle2012b4").change(function() { calculatesle2012(); });
$("#criteriasle2012b5").change(function() { calculatesle2012(); });
$("#criteriasle2012b6").change(function() { calculatesle2012(); });
$("#criteriasle2012c1").change(function() { calculatesle2012(); });

$("#calcagesle20121").click(function() { setdatesle2012('calcagesle20121'); });
$("#calcagesle20122").click(function() { setdatesle2012('calcagesle20122'); });
$("#calcagesle20123").click(function() { setdatesle2012('calcagesle20123'); });
$("#calcagesle20124").click(function() { setdatesle2012('calcagesle20124'); });
$("#calcagesle20125").click(function() { setdatesle2012('calcagesle20125'); });
$("#calcagesle20126").click(function() { setdatesle2012('calcagesle20126'); });
$("#calcagesle20127").click(function() { setdatesle2012('calcagesle20127'); });
$("#calcagesle20128").click(function() { setdatesle2012('calcagesle20128'); });
$("#calcagesle20129").click(function() { setdatesle2012('calcagesle20129'); });
$("#calcagesle201210").click(function() { setdatesle2012('calcagesle201210'); });
$("#calcagesle2012b11").click(function() { setdatesle2012('calcagesle2012b11'); });
$("#calcagesle2012b1").click(function() { setdatesle2012('calcagesle2012b1'); });
$("#calcagesle2012b2").click(function() { setdatesle2012('calcagesle2012b2'); });
$("#calcagesle2012b3").click(function() { setdatesle2012('calcagesle2012b3'); });
$("#calcagesle2012b4").click(function() { setdatesle2012('calcagesle2012b4'); });
$("#calcagesle2012b5").click(function() { setdatesle2012('calcagesle2012b5'); });
$("#calcagesle2012b6").click(function() { setdatesle2012('calcagesle2012b6'); });	
$("#calcagesle2012c1").click(function() { setdatesle2012('calcagesle2012c1'); });
	
	function calculatesle2012(date)
	{
		var val1 = $("#criteriasle20121").val();
		var val2 = $("#criteriasle20122").val();
		var val3 = $("#criteriasle20123").val();
		var val4 = $("#criteriasle20124").val();
		var val5 = $("#criteriasle20125").val();
		var val6 = $("#criteriasle20126").val();
		var val7 = $("#criteriasle20127").val();
		var val8 = $("#criteriasle20128").val();
		var val9 = $("#criteriasle20129").val();
		var val10 = $("#criteriasle201210").val();
		var val11 = $("#criteriasle201211").val();
		var valb1 = $("#criteriasle2012b1").val();
		var valb2 = $("#criteriasle2012b2").val();
		var valb3 = $("#criteriasle2012b3").val();
		var valb4 = $("#criteriasle2012b4").val();
		var valb5 = $("#criteriasle2012b5").val();
		var valb6 = $("#criteriasle2012b6").val();
		var valc1 = $("#criteriasle2012c1").val();
		
		var int1=0;
		var int2=0;
		var int3=0;
		var int4=0;
		var int5=0;
		var int6=0;
		var int7=0;
		var int8=0;
		var int9=0;
		var int10=0;
		var int11=0;
		var intb1=0;
		var intb2=0;
		var intb3=0;
		var intb4=0;
		var intb5=0;
		var intb6=0;
		
		if(val1==1)
			int1=1;
		if(val2==1)
			int2=1;
		if(val3==1)
			int3=1;
		if(val4==1)
			int4=1;
		if(val5==1)
			int5=1;
		if(val6==1)
			int6=1;
		if(val7==1)
			int7=1;
		if(val8==1)
			int8=1;
		if(val9==1)
			int9=1;
		if(val10==1)
			int10=1;
		if(val11==1)
			int11=1;
		if(valb1==1)
			intb1=1;
		if(valb2==1)
			intb2=1;
		if(valb3==1)
			intb3=1;
		if(valb4==1)
			intb4=1;
		if(valb5==1)
			intb5=1;
		if(valb6==1)
			intb6=1;
		
		var inta=0;
		var intb=0;
		var intsum=0;
		inta=int1 + int2 + int3 + int4 + int5 + int6 + int7 + int8 + int9 + int10 + int11;
		intb=intb1 + intb2 + intb3 + intb4 + intb5 + intb6;
		intsum=inta+intb;
		
		if (valc1==1 || (intsum >= 4 && inta >= 1 && intb >= 1))
		{
			if($("#sle2012date").val()=="")
				$("#divsle2012").html("<label>Clasiffied as SLE according to 2012 SLICC Criteria</label>");
			else
				$("#divsle2012").html("<label>Clasiffied as SLE according to 2012 SLICC Criteria at " + $("#sle2012date").val()+"</label>");
			
			$("#sle2012").val('Yes');
			//$("#sle2012totalscore").val(sum);
			$("#divsle2012").show();
			if($("#sle2012date").val()=="")
				alert("set 2012 SLICC Criteria Diagnosis date!");
		}
		else
		{
			$("#divsle2012").empty();
			$("#divsle2012").hide();
			$("#sle2012").val('No');
			//$("#sle2012totalscore").val('');
			$("#sle2012date").val('');
		}
	}
	
	function setdatesle2012(date)
	{
		if(date=="calcagesle20121")
			$("#sle2012date").val($("#criteriadatesle20121").val());
		if(date=="calcagesle20122")
			$("#sle2012date").val($("#criteriadatesle20122").val());
		if(date=="calcagesle20123")
			$("#sle2012date").val($("#criteriadatesle20123").val());
		if(date=="calcagesle20124")
			$("#sle2012date").val($("#criteriadatesle20124").val());
		if(date=="calcagesle20125")
			$("#sle2012date").val($("#criteriadatesle20125").val());
		if(date=="calcagesle20126")
			$("#sle2012date").val($("#criteriadatesle20126").val());
		if(date=="calcagesle20127")
			$("#sle2012date").val($("#criteriadatesle20127").val());
		if(date=="calcagesle20128")
			$("#sle2012date").val($("#criteriadatesle20128").val());
		if(date=="calcagesle20129")
			$("#sle2012date").val($("#criteriadatesle20129").val());
		if(date=="calcagesle201210")
			$("#sle2012date").val($("#criteriadatesle201210").val());
		if(date=="calcagesle201211")
			$("#sle2012date").val($("#criteriadatesle201211").val());
		if(date=="calcagesle2012b1")
			$("#sle2012date").val($("#criteriadatesle2012b1").val());
		if(date=="calcagesle2012b2")
			$("#sle2012date").val($("#criteriadatesle2012b2").val());
		if(date=="calcagesle2012b3")
			$("#sle2012date").val($("#criteriadatesle2012b3").val());
		if(date=="calcagesle2012b4")
			$("#sle2012date").val($("#criteriadatesle2012b4").val());
		if(date=="calcagesle2012b5")
			$("#sle2012date").val($("#criteriadatesle2012b5").val());
		if(date=="calcagesle2012b6")
			$("#sle2012date").val($("#criteriadatesle2012b6").val());
		if(date=="calcagesle2012c1")
			$("#sle2012date").val($("#criteriadatesle2012c1").val());
			
		if($("#sle2012").val()=="Yes")
		{
			$("#divsle2012").html("<label>Clasiffied as SLE according to 2012 SLICC Criteria at " + $("#sle2012date").val()+"</label>");
			$("#divsle2012").show();
		}
		else
		{
			$("#divsle2012").empty();
			$("#divsle2012").hide();
		}
}

$("#criteriasle19971").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle19971").show();
	 else
		 $("#divdatesle19971").hide();
    });
	
$("#criteriasle19972").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle19972").show();
	 else
		 $("#divdatesle19972").hide();
    });
	
$("#criteriasle19973").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle19973").show();
	 else
		 $("#divdatesle19973").hide();
    });
	
$("#criteriasle19974").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle19974").show();
	 else
		 $("#divdatesle19974").hide();
    });
	
	$("#criteriasle19975").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle19975").show();
	 else
		 $("#divdatesle19975").hide();
    });
	
	$("#criteriasle19976").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle19976").show();
	 else
		 $("#divdatesle19976").hide();
    });
	
	$("#criteriasle19977").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle19977").show();
	 else
		 $("#divdatesle19977").hide();
    });
	
	$("#criteriasle19978").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle19978").show();
	 else
		 $("#divdatesle19978").hide();
    });
	
	$("#criteriasle19979").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle19979").show();
	 else
		 $("#divdatesle19979").hide();
    });
	
	$("#criteriasle199710").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle199710").show();
	 else
		 $("#divdatesle199710").hide();
    });
	
	$("#criteriasle199711").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatesle199711").show();
	 else
		 $("#divdatesle199711").hide();
    });

$("#criteriasle19971").change(function() { calculatesle1997(); });
$("#criteriasle19972").change(function() { calculatesle1997(); });
$("#criteriasle19973").change(function() { calculatesle1997(); });
$("#criteriasle19974").change(function() { calculatesle1997(); });
$("#criteriasle19975").change(function() { calculatesle1997(); });
$("#criteriasle19976").change(function() { calculatesle1997(); });
$("#criteriasle19977").change(function() { calculatesle1997(); });
$("#criteriasle19978").change(function() { calculatesle1997(); });
$("#criteriasle19979").change(function() { calculatesle1997(); });
$("#criteriasle199710").change(function() { calculatesle1997(); });
$("#criteriasle199711").change(function() { calculatesle1997(); });
$("#calcagesle19971").click(function() { setdatesle1997('calcagesle19971'); });
$("#calcagesle19972").click(function() { setdatesle1997('calcagesle19972'); });
$("#calcagesle19973").click(function() { setdatesle1997('calcagesle19973'); });
$("#calcagesle19974").click(function() { setdatesle1997('calcagesle19974'); });
$("#calcagesle19975").click(function() { setdatesle1997('calcagesle19975'); });
$("#calcagesle19976").click(function() { setdatesle1997('calcagesle19976'); });
$("#calcagesle19977").click(function() { setdatesle1997('calcagesle19977'); });
$("#calcagesle19978").click(function() { setdatesle1997('calcagesle19978'); });
$("#calcagesle19979").click(function() { setdatesle1997('calcagesle19979'); });
$("#calcagesle199710").click(function() { setdatesle1997('calcagesle199710'); });
$("#calcagesle199711").click(function() { setdatesle1997('calcagesle199711'); });

function calculatesle1997()
{
		var val1 = $("#criteriasle19971").val();
		var val2 = $("#criteriasle19972").val();
		var val3 = $("#criteriasle19973").val();
		var val4 = $("#criteriasle19974").val();
		var val5 = $("#criteriasle19975").val();
		var val6 = $("#criteriasle19976").val();
		var val7 = $("#criteriasle19977").val();
		var val8 = $("#criteriasle19978").val();
		var val9 = $("#criteriasle19979").val();
		var val10 = $("#criteriasle199710").val();
		var val11 = $("#criteriasle199711").val();
		
		var int1=0;
		var int2=0;
		var int3=0;
		var int4=0;
		var int5=0;
		var int6=0;
		var int7=0;
		var int8=0;
		var int9=0;
		var int10=0;
		var int11=0;
		if(val1==1)
			int1=1;
		if(val2==1)
			int2=1;
		if(val3==1)
			int3=1;
		if(val4==1)
			int4=1;
		if(val5==1)
			int5=1;
		if(val6==1)
			int6=1;
		if(val7==1)
			int7=1;
		if(val8==1)
			int8=1;
		if(val9==1)
			int9=1;
		if(val10==1)
			int10=1;
		if(val11==1)
			int11=1;
		
		var sum=int1+ int2 + int3 + int4 + int5 + int6 + int7 + int8 + int9 + int10 + int11;
		if (sum>=4)
		{
			if($("#sle1997date").val()=="")
				$("#divsle1997").html("<label>Clasiffied as SLE according to 1997 revised ACR Criteria</label>");
			else
				$("#divsle1997").html("<label>Clasiffied as SLE according to 1997 revised ACR Criteria at " + $("#sle1997date").val()+"</label>");
			
			$("#sle1997").val('Yes');
			$("#sle1997totalscore").val(sum);
			$("#divsle1997").show();
			if($("#sle1997date").val()=="")
				alert("set 1997 revised ACR Criteria Diagnosis date!");
		}
		else
		{
			$("#divsle1997").empty();
			$("#divsle1997").hide();
			$("#sle1997").val('No');
			$("#sle1997totalscore").val('');
			$("#sle1997date").val('');
		}
		
		
}	

function setdatesle1997(date)
{
		if(date=="calcagesle19971")
			$("#sle1997date").val($("#criteriadatesle19971").val());
		if(date=="calcagesle19972")
			$("#sle1997date").val($("#criteriadatesle19972").val());
		if(date=="calcagesle19973")
			$("#sle1997date").val($("#criteriadatesle19973").val());
		if(date=="calcagesle19974")
			$("#sle1997date").val($("#criteriadatesle19974").val());
		if(date=="calcagesle19975")
			$("#sle1997date").val($("#criteriadatesle19975").val());
		if(date=="calcagesle19976")
			$("#sle1997date").val($("#criteriadatesle19976").val());
		if(date=="calcagesle19977")
			$("#sle1997date").val($("#criteriadatesle19977").val());
		if(date=="calcagesle19978")
			$("#sle1997date").val($("#criteriadatesle19978").val());
		if(date=="calcagesle19979")
			$("#sle1997date").val($("#criteriadatesle19979").val());
		if(date=="calcagesle199710")
			$("#sle1997date").val($("#criteriadatesle199710").val());
		if(date=="calcagesle199711")
			$("#sle1997date").val($("#criteriadatesle199711").val());
		
		if($("#sle1997").val()=="Yes")
		{
			$("#divsle1997").html("<label>Clasiffied as SLE according to 1997 revised ACR Criteria at " + $("#sle1997date").val()+"</label>");
			$("#divsle1997").show();
		}
		else
		{
			$("#divsle1997").empty();
			$("#divsle1997").hide();
		}
}
	
$("#criteriaaxial1").change(function() { calculateaxial(); });
$("#criteriaaxial2").change(function() { calculateaxial(); });
$("#criteriaaxial3").change(function() { calculateaxial(); });
$("#criteriaaxial4").change(function() { calculateaxial(); });
$("#criteriaaxial5").change(function() { calculateaxial(); });
$("#criteriaaxial6").change(function() { calculateaxial(); });
$("#criteriaaxial7").change(function() { calculateaxial(); });
$("#criteriaaxial8").change(function() { calculateaxial(); });
$("#criteriaaxial9").change(function() { calculateaxial(); });
$("#criteriaaxial10").change(function() { calculateaxial(); });
$("#criteriaaxial11").change(function() { calculateaxial(); });
$("#criteriaaxial12").change(function() { calculateaxial(); });
$("#criteriaaxial13").change(function() { calculateaxial(); });
$("#criteriaaxial14").change(function() { calculateaxial(); });
$("#criteriaaxial15").change(function() { calculateaxial(); });
$("#criteriaaxial16").change(function() { calculateaxial(); });
$("#calcageaxial1").click(function() { setdateaxial('calcageaxial1'); });
$("#calcageaxial2").click(function() { setdateaxial('calcageaxial2'); });
$("#calcageaxial3").click(function() { setdateaxial('calcageaxial3'); });
$("#calcageaxial4").click(function() { setdateaxial('calcageaxial4'); });
$("#calcageaxial5").click(function() { setdateaxial('calcageaxial5'); });
$("#calcageaxial6").click(function() { setdateaxial('calcageaxial6'); });
$("#calcageaxial7").click(function() { setdateaxial('calcageaxial7'); });
$("#calcageaxial8").click(function() { setdateaxial('calcageaxial8'); });
$("#calcageaxial9").click(function() { setdateaxial('calcageaxial9'); });
$("#calcageaxial10").click(function() { setdateaxial('calcageaxial10'); });
$("#calcageaxial11").click(function() { setdateaxial('calcageaxial11'); });
$("#calcageaxial12").click(function() { setdateaxial('calcageaxial12'); });
$("#calcageaxial13").click(function() { setdateaxial('calcageaxial13'); });
$("#calcageaxial14").click(function() { setdateaxial('calcageaxial14'); });
$("#calcageaxial15").click(function() { setdateaxial('calcageaxial15'); });
$("#calcageaxial16").click(function() { setdateaxial('calcageaxial16'); });

$("#calcageaxial21").click(function() { setdateaxial2('calcageaxial21'); });
$("#calcageaxial22").click(function() { setdateaxial2('calcageaxial22'); });
$("#calcageaxial23").click(function() { setdateaxial2('calcageaxial23'); });
$("#calcageaxial24").click(function() { setdateaxial2('calcageaxial24'); });
$("#calcageaxial25").click(function() { setdateaxial2('calcageaxial25'); });
$("#calcageaxial26").click(function() { setdateaxial2('calcageaxial26'); });
$("#calcageaxial27").click(function() { setdateaxial2('calcageaxial27'); });
$("#calcageaxial28").click(function() { setdateaxial2('calcageaxial28'); });
$("#calcageaxial29").click(function() { setdateaxial2('calcageaxial29'); });
$("#calcageaxial210").click(function() { setdateaxial2('calcageaxial210'); });
$("#calcageaxial211").click(function() { setdateaxial2('calcageaxial211'); });
$("#calcageaxial212").click(function() { setdateaxial2('calcageaxial212'); });
$("#calcageaxial213").click(function() { setdateaxial2('calcageaxial213'); });
$("#calcageaxial214").click(function() { setdateaxial2('calcageaxial214'); });
$("#calcageaxial215").click(function() { setdateaxial2('calcageaxial215'); });
$("#calcageaxial216").click(function() { setdateaxial2('calcageaxial216'); });

$("#calcageaxial31").click(function() { setdateaxial3('calcageaxial31'); });
$("#calcageaxial32").click(function() { setdateaxial3('calcageaxial32'); });
$("#calcageaxial33").click(function() { setdateaxial3('calcageaxial33'); });
$("#calcageaxial34").click(function() { setdateaxial3('calcageaxial34'); });
$("#calcageaxial35").click(function() { setdateaxial3('calcageaxial35'); });
$("#calcageaxial36").click(function() { setdateaxial3('calcageaxial36'); });
$("#calcageaxial37").click(function() { setdateaxial3('calcageaxial37'); });
$("#calcageaxial38").click(function() { setdateaxial3('calcageaxial38'); });
$("#calcageaxial39").click(function() { setdateaxial3('calcageaxial39'); });
$("#calcageaxial310").click(function() { setdateaxial3('calcageaxial310'); });
$("#calcageaxial311").click(function() { setdateaxial3('calcageaxial311'); });
$("#calcageaxial312").click(function() { setdateaxial3('calcageaxial312'); });
$("#calcageaxial313").click(function() { setdateaxial3('calcageaxial313'); });
$("#calcageaxial314").click(function() { setdateaxial3('calcageaxial314'); });
$("#calcageaxial315").click(function() { setdateaxial3('calcageaxial315'); });
$("#calcageaxial316").click(function() { setdateaxial3('calcageaxial316'); });

$("#calcageaxial41").click(function() { setdateaxial4('calcageaxial41'); });
$("#calcageaxial42").click(function() { setdateaxial4('calcageaxial42'); });
$("#calcageaxial43").click(function() { setdateaxial4('calcageaxial43'); });
$("#calcageaxial44").click(function() { setdateaxial4('calcageaxial44'); });
$("#calcageaxial45").click(function() { setdateaxial4('calcageaxial45'); });
$("#calcageaxial46").click(function() { setdateaxial4('calcageaxial46'); });
$("#calcageaxial47").click(function() { setdateaxial4('calcageaxial47'); });
$("#calcageaxial48").click(function() { setdateaxial4('calcageaxial48'); });
$("#calcageaxial49").click(function() { setdateaxial4('calcageaxial49'); });
$("#calcageaxial410").click(function() { setdateaxial4('calcageaxial410'); });
$("#calcageaxial411").click(function() { setdateaxial4('calcageaxial411'); });
$("#calcageaxial412").click(function() { setdateaxial4('calcageaxial412'); });
$("#calcageaxial413").click(function() { setdateaxial4('calcageaxial413'); });
$("#calcageaxial414").click(function() { setdateaxial4('calcageaxial414'); });
$("#calcageaxial415").click(function() { setdateaxial4('calcageaxial415'); });
$("#calcageaxial416").click(function() { setdateaxial4('calcageaxial416'); });

function setdateaxial(date)
{
		if(date=="calcageaxial1")
			$("#newyork_date").val($("#criteriadateaxial1").val());
		if(date=="calcageaxial2")
			$("#newyork_date").val($("#criteriadateaxial2").val());
		if(date=="calcageaxial3")
			$("#newyork_date").val($("#criteriadateaxial3").val());
		if(date=="calcageaxial4")
			$("#newyork_date").val($("#criteriadateaxial4").val());
		if(date=="calcageaxial5")
			$("#newyork_date").val($("#criteriadateaxial5").val());
		if(date=="calcageaxial6")
			$("#newyork_date").val($("#criteriadateaxial6").val());
		if(date=="calcageaxial7")
			$("#newyork_date").val($("#criteriadateaxial7").val());
		if(date=="calcageaxial8")
			$("#newyork_date").val($("#criteriadateaxial8").val());
		if(date=="calcageaxial9")
			$("#newyork_date").val($("#criteriadateaxial9").val());
		if(date=="calcageaxial10")
			$("#newyork_date").val($("#criteriadateaxial10").val());
		if(date=="calcageaxial11")
			$("#newyork_date").val($("#criteriadateaxial11").val());
		if(date=="calcageaxial12")
			$("#newyork_date").val($("#criteriadateaxial12").val());
		if(date=="calcageaxial13")
			$("#newyork_date").val($("#criteriadateaxial13").val());
		if(date=="calcageaxial14")
			$("#newyork_date").val($("#criteriadateaxial14").val());
		if(date=="calcageaxial15")
			$("#newyork_date").val($("#criteriadateaxial15").val());
		if(date=="calcageaxial16")
			$("#newyork_date").val($("#criteriadateaxial16").val());
		
		if($("#newyork").val()=="Yes")
		{
			$("#divaxialclassified").html("<label>Clasiffied as Axial SpA according to AS (New York Modified) at " + $("#newyork_date").val()+"</label>");
			$("#divaxialclassified").show();
		}
		else
		{
			$("#divaxialclassified").empty();
			$("#divaxialclassified").hide();
		}
}

function setdateaxial4(date)
{
		if(date=="calcageaxial41")
			$("#radiographic_date").val($("#criteriadateaxial1").val());
		if(date=="calcageaxial42")
			$("#radiographic_date").val($("#criteriadateaxial2").val());
		if(date=="calcageaxial43")
			$("#radiographic_date").val($("#criteriadateaxial3").val());
		if(date=="calcageaxial44")
			$("#radiographic_date").val($("#criteriadateaxial4").val());
		if(date=="calcageaxial45")
			$("#radiographic_date").val($("#criteriadateaxial5").val());
		if(date=="calcageaxial46")
			$("#radiographic_date").val($("#criteriadateaxial6").val());
		if(date=="calcageaxial47")
			$("#radiographic_date").val($("#criteriadateaxial7").val());
		if(date=="calcageaxial48")
			$("#radiographic_date").val($("#criteriadateaxial8").val());
		if(date=="calcageaxial49")
			$("#radiographic_date").val($("#criteriadateaxial9").val());
		if(date=="calcageaxial410")
			$("#radiographic_date").val($("#criteriadateaxial10").val());
		if(date=="calcageaxial411")
			$("#radiographic_date").val($("#criteriadateaxial11").val());
		if(date=="calcageaxial412")
			$("#radiographic_date").val($("#criteriadateaxial12").val());
		if(date=="calcageaxial413")
			$("#radiographic_date").val($("#criteriadateaxial13").val());
		if(date=="calcageaxial414")
			$("#radiographic_date").val($("#criteriadateaxial14").val());
		if(date=="calcageaxial415")
			$("#radiographic_date").val($("#criteriadateaxial15").val());
		if(date=="calcageaxial416")
			$("#radiographic_date").val($("#criteriadateaxial16").val());
		
		if($("#radiographic").val()=="Yes")
		{
			$("#divaxialclassified4").html("<label>Clasiffied as Axial SpA according to Non-radiographic Axial SpA criteria at " + $("#radiographic_date").val()+"</label>");
			$("#divaxialclassified4").show();
		}
		else
		{
			$("#divaxialclassified4").empty();
			$("#divaxialclassified4").hide();
		}
}

function setdateaxial3(date)
{
		if(date=="calcageaxial31")
			$("#clinical_date").val($("#criteriadateaxial1").val());
		if(date=="calcageaxial32")
			$("#clinical_date").val($("#criteriadateaxial2").val());
		if(date=="calcageaxial33")
			$("#clinical_date").val($("#criteriadateaxial3").val());
		if(date=="calcageaxial34")
			$("#clinical_date").val($("#criteriadateaxial4").val());
		if(date=="calcageaxial35")
			$("#clinical_date").val($("#criteriadateaxial5").val());
		if(date=="calcageaxial36")
			$("#clinical_date").val($("#criteriadateaxial6").val());
		if(date=="calcageaxial37")
			$("#clinical_date").val($("#criteriadateaxial7").val());
		if(date=="calcageaxial38")
			$("#clinical_date").val($("#criteriadateaxial8").val());
		if(date=="calcageaxial39")
			$("#clinical_date").val($("#criteriadateaxial9").val());
		if(date=="calcageaxial310")
			$("#clinical_date").val($("#criteriadateaxial10").val());
		if(date=="calcageaxial311")
			$("#clinical_date").val($("#criteriadateaxial11").val());
		if(date=="calcageaxial312")
			$("#clinical_date").val($("#criteriadateaxial12").val());
		if(date=="calcageaxial313")
			$("#clinical_date").val($("#criteriadateaxial13").val());
		if(date=="calcageaxial314")
			$("#clinical_date").val($("#criteriadateaxial14").val());
		if(date=="calcageaxial315")
			$("#clinical_date").val($("#criteriadateaxial15").val());
		if(date=="calcageaxial316")
			$("#clinical_date").val($("#criteriadateaxial16").val());
		
		if($("#clinical").val()=="Yes")
		{
			$("#divaxialclassified3").html("<label>Clasiffied as Axial SpA according to Axial SpA Clinical arm criteria at " + $("#clinical_date").val()+"</label>");
			$("#divaxialclassified3").show();
		}
		else
		{
			$("#divaxialclassified3").empty();
			$("#divaxialclassified3").hide();
		}
}

function setdateaxial2(date)
{
		if(date=="calcageaxial21")
			$("#imaging_date").val($("#criteriadateaxial1").val());
		if(date=="calcageaxial22")
			$("#imaging_date").val($("#criteriadateaxial2").val());
		if(date=="calcageaxial23")
			$("#imaging_date").val($("#criteriadateaxial3").val());
		if(date=="calcageaxial24")
			$("#imaging_date").val($("#criteriadateaxial4").val());
		if(date=="calcageaxial25")
			$("#imaging_date").val($("#criteriadateaxial5").val());
		if(date=="calcageaxial26")
			$("#imaging_date").val($("#criteriadateaxial6").val());
		if(date=="calcageaxial27")
			$("#imaging_date").val($("#criteriadateaxial7").val());
		if(date=="calcageaxial28")
			$("#imaging_date").val($("#criteriadateaxial8").val());
		if(date=="calcageaxial29")
			$("#imaging_date").val($("#criteriadateaxial9").val());
		if(date=="calcageaxial210")
			$("#imaging_date").val($("#criteriadateaxial10").val());
		if(date=="calcageaxial211")
			$("#imaging_date").val($("#criteriadateaxial11").val());
		if(date=="calcageaxial212")
			$("#imaging_date").val($("#criteriadateaxial12").val());
		if(date=="calcageaxial213")
			$("#imaging_date").val($("#criteriadateaxial13").val());
		if(date=="calcageaxial214")
			$("#imaging_date").val($("#criteriadateaxial14").val());
		if(date=="calcageaxial215")
			$("#imaging_date").val($("#criteriadateaxial15").val());
		if(date=="calcageaxial216")
			$("#imaging_date").val($("#criteriadateaxial16").val());
		
		if($("#imaging").val()=="Yes")
		{
			$("#divaxialclassified2").html("<label>Clasiffied as Axial SpA according to Axial SpA Imaging arm criteria at " + $("#imaging_date").val()+"</label>");
			$("#divaxialclassified2").show();
		}
		else
		{
			$("#divaxialclassified2").empty();
			$("#divaxialclassified2").hide();
		}
}

function calculateaxial()
{
		var val1 = $("#criteriaaxial1").val();
		var val2 = $("#criteriaaxial2").val();
		var val3 = $("#criteriaaxial3").val();
		var val4 = $("#criteriaaxial4").val();
		var val5 = $("#criteriaaxial5").val();
		var val6 = $("#criteriaaxial6").val();
		var val7 = $("#criteriaaxial7").val();
		var val8 = $("#criteriaaxial8").val();
		var val9 = $("#criteriaaxial9").val();
		var val10 = $("#criteriaaxial10").val();
		var val11 = $("#criteriaaxial11").val();
		var val12 = $("#criteriaaxial12").val();
		var val13 = $("#criteriaaxial13").val();
		var val14 = $("#criteriaaxial14").val();
		var val15 = $("#criteriaaxial15").val();
		var val16 = $("#criteriaaxial16").val();
		
		var criteria1=0;
		var criteria2=0;
		
		if(val2==1 || val13==1 || val14==1)
			criteria1 = 1;
		
		if(val15==1)
			criteria2 = 1;
		
		
		if(criteria1==1 && criteria2==1)
		{
			if($("#newyork_date").val()=="")
				$("#divaxialclassified").html("<label>Clasiffied as Axial SpA according to AS (New York Modified)</label>");
			else
				$("#divaxialclassified").html("<label>Clasiffied as Axial SpA according to AS (New York Modified) at " + $("#newyork_date").val() + "</label>");
			$("#newyork").val('Yes');
			//$("#apstotalscore").val(total_score);
			$("#divaxialclassified").show();
			if($("#newyork_date").val()=="")
				alert("set Ankylosing Spondylitis (setdate) Diagnosis date!");
		}
		else
		{
			$("#divaxialclassified").empty();
			$("#divaxialclassified").hide();
			$("#newyork").val('No');
			//$("#apstotalscore").val('');
			$("#newyork_date").val('');
		}
		
		var set2entry=0;
		var group1=0;
		var group2=0;
		
		if(val1==1)
			set2entry = 1;
		
		if(val15==1 || val16==1)
			group1 = 1;
		
		if(val2==1 || val3==1 || val4==1 || val5==1 || val6==1 || val7==1 || val8==1 || val9==1 || val10==1 || val11==1 || val12==1)
			group2 = 1;
		
		
		if(set2entry==1 && group1==1 && group2==1)
		{
			if($("#imaging_date").val()=="")
				$("#divaxialclassified2").html("<label>Clasiffied as Axial SpA according to Axial SpA Imaging arm criteria</label>");
			else
				$("#divaxialclassified2").html("<label>Clasiffied as Axial SpA according to Axial SpA Imaging arm criteria at " + $("#imaging_date").val() + "</label>");
			$("#imaging").val('Yes');
			//$("#apstotalscore").val(total_score);
			$("#divaxialclassified2").show();
			if($("#imaging_date").val()=="")
				alert("set Axial SpA (radiographic) (setdate2) Diagnosis date!");
		}
		else
		{
			$("#divaxialclassified2").empty();
			$("#divaxialclassified2").hide();
			$("#imaging").val('No');
			//$("#apstotalscore").val('');
			$("#imaging_date").val('');
		}
		
		var set3entry=0;
		var group31=0;
		var group32=0;
		
		if(val1==1)
			set3entry = 1;
		
		if(val11==1)
			group31 = 1;
		
		if(val2==1)
			group32 = group32 + 1;
		if (val3==1) 
			group32 = group32 + 1;
		if (val4==1) 
			group32 = group32 + 1;
		if (val5==1) 
			group32 = group32 + 1;
		if (val6==1) 
			group32 = group32 + 1;
		if (val7==1) 
			group32 = group32 + 1;
		if (val8==1) 
			group32 = group32 + 1;
		if (val9==1) 
			group32 = group32 + 1;
		if (val10==1) 
			group32 = group32 + 1;
		if (val12==1) 
			group32 = group32 + 1;
			
		
		
		if(set3entry==1 && group31==1 && group32>=2)
		{
			if($("#clinical_date").val()=="")
				$("#divaxialclassified3").html("<label>Clasiffied as Axial SpA according to Axial SpA Clinical arm criteria</label>");
			else
				$("#divaxialclassified3").html("<label>Clasiffied as Axial SpA according to Axial SpA Clinical arm criteria at " + $("#clinical_date").val() + "</label>");
			$("#clinical").val('Yes');
			//$("#apstotalscore").val(total_score);
			$("#divaxialclassified3").show();
			if($("#clinical_date").val()=="")
				alert("set Axial SpA (clinical)(setdate3) Diagnosis date!");
		}
		else
		{
			$("#divaxialclassified3").empty();
			$("#divaxialclassified3").hide();
			$("#clinical").val('No');
			//$("#apstotalscore").val('');
			$("#clinical_date").val('');
		}
		
		var set4entry=0;
		var group41=0;
		var group42=0;
		var group43=0;
		
		if(val1==1)
			set4entry = 1;
		
		if(val16==1)
			group41 = 1;
		
		if(val15==2)
			group42 = 1;
		
		if(val2==1 || val3==1 || val4==1 || val5==1 || val6==1 || val7==1 || val8==1 || val9==1 || val10==1 || val11==1 || val12==1)
			group43 = 1;
		
		
		if(set4entry==1 && group41==1 && group42==1 && group43==1)
		{
			if($("#radiographic_date").val()=="")
				$("#divaxialclassified4").html("<label>Clasiffied as Axial SpA according to Non-radiographic Axial SpA criteria</label>");
			else
				$("#divaxialclassified4").html("<label>Clasiffied as Axial SpA according to Non-radiographic Axial SpA criteria at " + $("#radiographic_date").val() + "</label>");
			$("#radiographic").val('Yes');
			//$("#apstotalscore").val(total_score);
			$("#divaxialclassified4").show();
			if($("#radiographic_date").val()=="")
				alert("set non-radiographic Axial SpA  (setdate4) Diagnosis date!");
		}
		else
		{
			$("#divaxialclassified4").empty();
			$("#divaxialclassified4").hide();
			$("#radiographic").val('No');
			//$("#apstotalscore").val('');
			$("#radiographic_date").val('');
		}
}	

$("#criteriaperipheral1").change(function() { calculateperipheral(); });
$("#criteriaperipheral2").change(function() { calculateperipheral(); });
$("#criteriaperipheral3").change(function() { calculateperipheral(); });
$("#criteriaperipheral4").change(function() { calculateperipheral(); });
$("#criteriaperipheral5").change(function() { calculateperipheral(); });
$("#criteriaperipheral6").change(function() { calculateperipheral(); });
$("#criteriaperipheral7").change(function() { calculateperipheral(); });
$("#criteriaperipheral8").change(function() { calculateperipheral(); });
$("#criteriaperipheral9").change(function() { calculateperipheral(); });
$("#criteriaperipheral10").change(function() { calculateperipheral(); });
$("#criteriaperipheral11").change(function() { calculateperipheral(); });
$("#criteriaperipheral12").change(function() { calculateperipheral(); });
$("#criteriaperipheral13").change(function() { calculateperipheral(); });
$("#criteriaperipheral14").change(function() { calculateperipheral(); });
$("#criteriaperipheral15").change(function() { calculateperipheral(); });
$("#criteriaperipheral16").change(function() { calculateperipheral(); });
$("#calcageperipheral1").click(function() { setdateperipheral('calcageperipheral1'); });
$("#calcageperipheral2").click(function() { setdateperipheral('calcageperipheral2'); });
$("#calcageperipheral3").click(function() { setdateperipheral('calcageperipheral3'); });
$("#calcageperipheral4").click(function() { setdateperipheral('calcageperipheral4'); });
$("#calcageperipheral5").click(function() { setdateperipheral('calcageperipheral5'); });
$("#calcageperipheral6").click(function() { setdateperipheral('calcageperipheral6'); });
$("#calcageperipheral7").click(function() { setdateperipheral('calcageperipheral7'); });
$("#calcageperipheral8").click(function() { setdateperipheral('calcageperipheral8'); });
$("#calcageperipheral9").click(function() { setdateperipheral('calcageperipheral9'); });
$("#calcageperipheral10").click(function() { setdateperipheral('calcageperipheral10'); });
$("#calcageperipheral11").click(function() { setdateperipheral('calcageperipheral11'); });
$("#calcageperipheral12").click(function() { setdateperipheral('calcageperipheral12'); });
$("#calcageperipheral13").click(function() { setdateperipheral('calcageperipheral13'); });
$("#calcageperipheral14").click(function() { setdateperipheral('calcageperipheral14'); });
$("#calcageperipheral15").click(function() { setdateperipheral('calcageperipheral15'); });
$("#calcageperipheral16").click(function() { setdateperipheral('calcageperipheral16'); });

$("#calcageperipheral21").click(function() { setdateperipheral2('calcageperipheral21'); });
$("#calcageperipheral22").click(function() { setdateperipheral2('calcageperipheral22'); });
$("#calcageperipheral23").click(function() { setdateperipheral2('calcageperipheral23'); });
$("#calcageperipheral24").click(function() { setdateperipheral2('calcageperipheral24'); });
$("#calcageperipheral25").click(function() { setdateperipheral2('calcageperipheral25'); });
$("#calcageperipheral26").click(function() { setdateperipheral2('calcageperipheral26'); });
$("#calcageperipheral27").click(function() { setdateperipheral2('calcageperipheral27'); });
$("#calcageperipheral28").click(function() { setdateperipheral2('calcageperipheral28'); });
$("#calcageperipheral29").click(function() { setdateperipheral2('calcageperipheral29'); });
$("#calcageperipheral210").click(function() { setdateperipheral2('calcageperipheral210'); });
$("#calcageperipheral211").click(function() { setdateperipheral2('calcageperipheral211'); });
$("#calcageperipheral212").click(function() { setdateperipheral2('calcageperipheral212'); });
$("#calcageperipheral213").click(function() { setdateperipheral2('calcageperipheral213'); });
$("#calcageperipheral214").click(function() { setdateperipheral2('calcageperipheral214'); });
$("#calcageperipheral215").click(function() { setdateperipheral2('calcageperipheral215'); });
$("#calcageperipheral216").click(function() { setdateperipheral2('calcageperipheral216'); });

function setdateperipheral(date)
{
		if(date=="calcageperipheral1")
			$("#set1_date").val($("#criteriaperipheraldate1").val());
		if(date=="calcageperipheral2")
			$("#set1_date").val($("#criteriaperipheraldate2").val());
		if(date=="calcageperipheral3")
			$("#set1_date").val($("#criteriaperipheraldate3").val());
		if(date=="calcageperipheral4")
			$("#set1_date").val($("#criteriaperipheraldate4").val());
		if(date=="calcageperipheral5")
			$("#set1_date").val($("#criteriaperipheraldate5").val());
		if(date=="calcageperipheral6")
			$("#set1_date").val($("#criteriaperipheraldate6").val());
		if(date=="calcageperipheral7")
			$("#set1_date").val($("#criteriaperipheraldate7").val());
		if(date=="calcageperipheral8")
			$("#set1_date").val($("#criteriaperipheraldate8").val());
		if(date=="calcageperipheral9")
			$("#set1_date").val($("#criteriaperipheraldate9").val());
		if(date=="calcageperipheral10")
			$("#set1_date").val($("#criteriaperipheraldate10").val());
		if(date=="calcageperipheral11")
			$("#set1_date").val($("#criteriaperipheraldate11").val());
		if(date=="calcageperipheral12")
			$("#set1_date").val($("#criteriaperipheraldate12").val());
		if(date=="calcageperipheral13")
			$("#set1_date").val($("#criteriaperipheraldate13").val());
		if(date=="calcageperipheral14")
			$("#set1_date").val($("#criteriaperipheraldate14").val());
		if(date=="calcageperipheral15")
			$("#set1_date").val($("#criteriaperipheraldate15").val());
		if(date=="calcageperipheral16")
			$("#set1_date").val($("#criteriaperipheraldate16").val());
		
		if($("#set1").val()=="Yes")
		{
			$("#divperipheralclassified").html("<label>Clasiffied as peripheral SpA </br> according to set1 at " + $("#set1_date").val()+"</label>");
			$("#divperipheralclassified").show();
		}
		else
		{
			$("#divperipheralclassified").empty();
			$("#divperipheralclassified").hide();
		}
}

function setdateperipheral2(date)
{
		if(date=="calcageperipheral21")
			$("#set2_date").val($("#criteriaperipheraldate1").val());
		if(date=="calcageperipheral22")
			$("#set2_date").val($("#criteriaperipheraldate2").val());
		if(date=="calcageperipheral23")
			$("#set2_date").val($("#criteriaperipheraldate3").val());
		if(date=="calcageperipheral24")
			$("#set2_date").val($("#criteriaperipheraldate4").val());
		if(date=="calcageperipheral25")
			$("#set2_date").val($("#criteriaperipheraldate5").val());
		if(date=="calcageperipheral26")
			$("#set2_date").val($("#criteriaperipheraldate6").val());
		if(date=="calcageperipheral27")
			$("#set2_date").val($("#criteriaperipheraldate7").val());
		if(date=="calcageperipheral28")
			$("#set2_date").val($("#criteriaperipheraldate8").val());
		if(date=="calcageperipheral29")
			$("#set2_date").val($("#criteriaperipheraldate9").val());
		if(date=="calcageperipheral210")
			$("#set2_date").val($("#criteriaperipheraldate10").val());
		if(date=="calcageperipheral211")
			$("#set2_date").val($("#criteriaperipheraldate11").val());
		if(date=="calcageperipheral212")
			$("#set2_date").val($("#criteriaperipheraldate12").val());
		if(date=="calcageperipheral213")
			$("#set2_date").val($("#criteriaperipheraldate13").val());
		if(date=="calcageperipheral214")
			$("#set2_date").val($("#criteriaperipheraldate14").val());
		if(date=="calcageperipheral215")
			$("#set2_date").val($("#criteriaperipheraldate15").val());
		if(date=="calcageperipheral216")
			$("#set2_date").val($("#criteriaperipheraldate16").val());
		
		if($("#set2").val()=="Yes")
		{
			$("#divperipheralclassified2").html("<label>Clasiffied as peripheral SpA according to set2 at " + $("#set2_date").val()+"</label>");
			$("#divperipheralclassified2").show();
		}
		else
		{
			$("#divperipheralclassified2").empty();
			$("#divperipheralclassified2").hide();
		}
}

function calculateperipheral()
{
		var val1 = $("#criteriaperipheral1").val();
		var val2 = $("#criteriaperipheral2").val();
		var val3 = $("#criteriaperipheral3").val();
		var val4 = $("#criteriaperipheral4").val();
		var val5 = $("#criteriaperipheral5").val();
		var val6 = $("#criteriaperipheral6").val();
		var val7 = $("#criteriaperipheral7").val();
		var val8 = $("#criteriaperipheral8").val();
		var val9 = $("#criteriaperipheral9").val();
		var val10 = $("#criteriaperipheral10").val();
		var val11 = $("#criteriaperipheral11").val();
		var val12 = $("#criteriaperipheral12").val();
		var val13 = $("#criteriaperipheral13").val();
		var val14 = $("#criteriaperipheral14").val();
		var val15 = $("#criteriaperipheral15").val();
		var val16 = $("#criteriaperipheral16").val();
		
		var entry1=0;
		var totalentry=0;
		var rest=0
		
		if(val3==1 || val5==1 || val7==1)
			entry1 = 1;
		
		if(entry1==1 && val1==1)
			totalentry = 1;
		
		if(val9==1 || val10==1 || val11==1  || val12==1 || val14==1 || val15==1 || val16==1)
			rest = 1;
		
		
		if(totalentry==1 && rest==1)
		{
			if($("#set1_date").val()=="")
				$("#divperipheralclassified").html("<label>Clasiffied as peripheral SpA according to set1</label>");
			else
				$("#divperipheralclassified").html("<label>Clasiffied as peripheral SpA according to set1 at " + $("#set1_date").val() + "</label>");
			$("#set1").val('Yes');
			//$("#apstotalscore").val(total_score);
			$("#divperipheralclassified").show();
			if($("#set1_date").val()=="")
				alert("set peripheral set1 Diagnosis date!");
		}
		else
		{
			$("#divperipheralclassified").empty();
			$("#divperipheralclassified").hide();
			$("#set1").val('No');
			//$("#apstotalscore").val('');
			$("#set1_date").val('');
		}
		
		var set2entry=0;
		var group1=0;
		var group2=0;
		var group3=0;
		var combination1=0;
		var combination2=0;
		var combination3=0;
		
		if(val1==1)
			set2entry = 1;
		
		var int2=0;
		var int3=0;
		var int4=0;
		var int5=0;
		var int6=0;
		var int7=0;
		var int8=0;
		var int13=0;
		if(val2==1)
			int2=1;
		if(val3==1)
			int3=1;
		if(val4==1)
			int4=1;
		if(val5==1)
			int5=1;
		if(val6==1)
			int6=1;
		if(val7==1)
			int7=1;
		if(val8==1)
			int8=1;
		if(val13==1)
			int13=1;
		
		if((int2+int6+int8+int13)>=2)
			group1=1;
		
		if((int2+int4+int8+int13)>=2)
			group2=1;
		
		if((int2+int4+int6+int13)>=2)
			group3=1;
		
		if(set2entry==1 && val3==1 && group1==1)
			combination1=1;
		
		if(set2entry==1 && val5==1 && group2==1)
			combination2=1;
		
		if(set2entry==1 && val7==1 && group3==1)
			combination3=1;
		
		if(combination1==1 || combination2==1 || combination3==1)
		{
			if($("#set2_date").val()=="")
				$("#divperipheralclassified2").html("<label>Clasiffied as peripheral SpA according to set2</label>");
			else
				$("#divperipheralclassified2").html("<label>Clasiffied as peripheral SpA according to set2 at " + $("#set2_date").val() + "</label>");
			$("#set2").val('Yes');
			//$("#apstotalscore").val(total_score);
			$("#divperipheralclassified2").show();
			if($("#set2_date").val()=="")
				alert("set peripheral set2 Diagnosis date!");
		}
		else
		{
			$("#divperipheralclassified2").empty();
			$("#divperipheralclassified2").hide();
			$("#set2").val('No');
			//$("#apstotalscore").val('');
			$("#set2_date").val('');
		}
}	

$("#criteriaaps1").change(function() { calculateaps(); });
$("#criteriaaps2").change(function() { calculateaps(); });
$("#criteriaaps3").change(function() { calculateaps(); });
$("#criteriaaps4").change(function() { calculateaps(); });
$("#criteriaaps5").change(function() { calculateaps(); });
$("#criteriaaps6").change(function() { calculateaps(); });
$("#criteriaaps7").change(function() { calculateaps(); });
$("#criteriaaps8").change(function() { calculateaps(); });
$("#calcageaps1").click(function() { setdateaps('calcageaps1'); });
$("#calcageaps2").click(function() { setdateaps('calcageaps2'); });
$("#calcageaps3").click(function() { setdateaps('calcageaps3'); });
$("#calcageaps4").click(function() { setdateaps('calcageaps4'); });
$("#calcageaps5").click(function() { setdateaps('calcageaps5'); });
$("#calcageaps6").click(function() { setdateaps('calcageaps6'); });
$("#calcageaps7").click(function() { setdateaps('calcageaps7'); });
$("#calcageaps8").click(function() { setdateaps('calcageaps8'); });

function setdateaps(date)
{
		if(date=="calcageaps1")
			$("#apsdate").val($("#criteriadateaps1").val());
		if(date=="calcageaps2")
			$("#apsdate").val($("#criteriadateaps2").val());
		if(date=="calcageaps3")
			$("#apsdate").val($("#criteriadateaps3").val());
		if(date=="calcageaps4")
			$("#apsdate").val($("#criteriadateaps4").val());
		if(date=="calcageaps5")
			$("#apsdate").val($("#criteriadateaps5").val());
		if(date=="calcageaps6")
			$("#apsdate").val($("#criteriadateaps6").val());
		if(date=="calcageaps7")
			$("#apsdate").val($("#criteriadateaps7").val());
		if(date=="calcageaps8")
			$("#apsdate").val($("#criteriadateap8").val());
		if($("#aps").val()=="Yes")
		{
			$("#divapsclassified").html("<label>Clasiffied as APS at " + $("#apsdate").val()+"</label>");
			$("#divapsclassified").show();
		}
		else
		{
			$("#divapsclassified").empty();
			$("#divapsclassified").hide();
		}
}

function calculateaps()
{
		var first_val = $("#criteriaaps1").val();
		var second_val = $("#criteriaaps2").val();
		var third_val = $("#criteriaaps3").val();
		var forth_val = $("#criteriaaps4").val();
		var fifth_val = $("#criteriaaps5").val();
		var sixth_val = $("#criteriaaps6").val();
		var seventh_val = $("#criteriaaps7").val();
		var eigth_val = $("#criteriaaps8").val();
		//var previous_total_score = $("#caspartotalscore").val();
		
		var total_score1=0;
		var total_score2=0;
		
		if(first_val==1 || second_val==1 || third_val==1)
			total_score1 = 1;
		
		if(forth_val==1 || fifth_val==1 || sixth_val==1 || seventh_val==1 || eigth_val==1)
			total_score2 = 1;
		
		
		
		if(total_score1==1 && total_score2==1)
		{
			if($("#apsdate").val()=="")
				$("#divapsclassified").html("<label>Clasiffied as APS</label>");
			else
				$("#divapsclassified").html("<label>Clasiffied as APS at " + $("#apsdate").val() + "</label>");
			$("#aps").val('Yes');
			//$("#apstotalscore").val(total_score);
			$("#divapsclassified").show();
			if($("#apsdate").val()=="")
				alert("set APS Diagnosis date!");
		}
		else
		{
			$("#divapsclassified").empty();
			$("#divapsclassified").hide();
			$("#aps").val('No');
			//$("#apstotalscore").val('');
			$("#apsdate").val('');
		}
}	

$("#criteriacaspar1").change(function() { calculatecaspar(); });
$("#criteriacaspar2").change(function() { calculatecaspar(); });
$("#criteriacaspar3").change(function() { calculatecaspar(); });
$("#criteriacaspar4").change(function() { calculatecaspar(); });
$("#criteriacaspar5").change(function() { calculatecaspar(); });
$("#calcagecaspar1").click(function() { setdatecaspar('calcagecaspar1'); });
$("#calcagecaspar2").click(function() { setdatecaspar('calcagecaspar2'); });
$("#calcagecaspar3").click(function() { setdatecaspar('calcagecaspar3'); });
$("#calcagecaspar4").click(function() { setdatecaspar('calcagecaspar4'); });
$("#calcagecaspar5").click(function() { setdatecaspar('calcagecaspar5'); });

function setdatecaspar(date)
{
		if(date=="calcagecaspar1")
			$("#caspardate").val($("#criteriadatecaspar1").val());
		if(date=="calcagecaspar2")
			$("#caspardate").val($("#criteriadatecaspar2").val());
		if(date=="calcagecaspar3")
			$("#caspardate").val($("#criteriadatecaspar3").val());
		if(date=="calcagecaspar4")
			$("#caspardate").val($("#criteriadatecaspar4").val());
		if(date=="calcagecaspar5")
			$("#caspardate").val($("#criteriadatecaspar5").val());
		if($("#caspar").val()=='Yes')
		{
			$("#divcasparclassified").html("<label>Clasiffied as CASPAR at " + $("#caspardate").val() + " with score " + $("#caspartotalscore").val() + "/6</label>");
			$("#divcasparclassified").show();
		}
		else
		{
			$("#divcasparclassified").empty();
			$("#divcasparclassified").hide();
		}
			
}

function calculatecaspar()
{
		var first_val = $("#criteriacaspar1").val();
		var second_val = $("#criteriacaspar2").val();
		var third_val = $("#criteriacaspar3").val();
		var forth_val = $("#criteriacaspar4").val();
		var fifth_val = $("#criteriacaspar5").val();
		var previous_total_score = $("#caspartotalscore").val();
		
		var total_score=0;
		
		if(first_val==1)
			total_score += 1;
		else if(first_val==2)
			total_score += 2;
		
		if(second_val==1)
			total_score += 1;
		
		if(third_val==1)
			total_score += 1;
		
		if(forth_val==1)
			total_score += 1;
		
		if(fifth_val==1)
			total_score += 1;
		
		if(total_score>=3)
		{
			if($("#caspardate").val()=="")
				$("#divcasparclassified").html("<label>Clasiffied as CASPAR with score "+total_score+"/6</label>");
			else
				$("#divcasparclassified").html("<label>Clasiffied as CASPAR at " + $("#caspardate").val() + " with score "+total_score+"/6</label>");
			$("#caspar").val('Yes');
			$("#caspartotalscore").val(total_score);
			$("#divcasparclassified").show();
			if(previous_total_score<3)
				alert("set CASPAR Diagnosis date!");
		}
		else
		{
			$("#divcasparclassified").empty();
			$("#divcasparclassified").hide();
			$("#caspar").val('No');
			$("#caspartotalscore").val('');
			$("#caspardate").val('');
		}
}	


    $("#criteriaaps1").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaps1").hide();
	 else
		 $("#divdateaps1").show();
    });
	
	$("#criteriaaps2").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaps2").hide();
	 else
		 $("#divdateaps2").show();
    });
	
	$("#criteriaaps3").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaps3").hide();
	 else
		 $("#divdateaps3").show();
    });
	
	$("#criteriaaps4").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaps4").hide();
	 else
		 $("#divdateaps4").show();
    });
	
	$("#criteriaaps5").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaps5").hide();
	 else
		 $("#divdateaps5").show();
    });
	
	$("#criteriaaps6").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaps6").hide();
	 else
		 $("#divdateaps6").show();
    });
	
	$("#criteriaaps7").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaps7").hide();
	 else
		 $("#divdateaps7").show();
    });
	
	$("#criteriaaps8").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaps8").hide();
	 else
		 $("#divdateaps8").show();
    });
	
    $("#criteriacaspar1").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdatecaspar1").hide();
	 else
		 $("#divdatecaspar1").show();
    });
	
	$("#criteriacaspar2").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatecaspar2").show();
	 else
		 $("#divdatecaspar2").hide();
    });
	
	$("#criteriacaspar3").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatecaspar3").show();
	 else
		 $("#divdatecaspar3").hide();
    });
	
	$("#criteriacaspar4").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatecaspar4").show();
	 else
		 $("#divdatecaspar4").hide();
    });
	
	$("#criteriacaspar5").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdatecaspar5").show();
	 else
		 $("#divdatecaspar5").hide();
    });
	
	$("#criteriaaxial1").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaxial1").hide();
	 else
		 $("#divdateaxial1").show();
    });
	
	$("#criteriaaxial2").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaxial2").hide();
	 else
		 $("#divdateaxial2").show();
    });
	
	$("#criteriaaxial3").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaxial3").hide();
	 else
		 $("#divdateaxial3").show();
    });
	
	$("#criteriaaxial4").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaxial4").hide();
	 else
		 $("#divdateaxial4").show();
    });
	
	$("#criteriaaxial5").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaxial5").hide();
	 else
		 $("#divdateaxial5").show();
    });
	
	$("#criteriaaxial6").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaxial6").hide();
	 else
		 $("#divdateaxial6").show();
    });
	
	$("#criteriaaxial7").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaxial7").hide();
	 else
		 $("#divdateaxial7").show();
    });
	
	$("#criteriaaxial8").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaxial8").hide();
	 else
		 $("#divdateaxial8").show();
    });
	
	$("#criteriaaxial9").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaxial9").hide();
	 else
		 $("#divdateaxial9").show();
    });
	
	$("#criteriaaxial10").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaxial10").hide();
	 else
		 $("#divdateaxial10").show();
    });
	
	$("#criteriaaxial11").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaxial11").hide();
	 else
		 $("#divdateaxial11").show();
    });
	
	$("#criteriaaxial12").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaxial12").hide();
	 else
		 $("#divdateaxial12").show();
    });
	
	$("#criteriaaxial13").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaxial13").hide();
	 else
		 $("#divdateaxial13").show();
    });
	
	$("#criteriaaxial14").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaxial14").hide();
	 else
		 $("#divdateaxial14").show();
    });
	
	$("#criteriaaxial15").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaxial15").hide();
	 else
		 $("#divdateaxial15").show();
    });
	
	$("#criteriaaxial16").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdateaxial16").hide();
	 else
		 $("#divdateaxial16").show();
    });
	
    $("#criteriaperipheral1").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdateperipheral1").show();
	 else
		 $("#divdateperipheral1").hide();
    });
	
	$("#criteriaperipheral2").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdateperipheral2").show();
	 else
		 $("#divdateperipheral2").hide();
    });
	
	$("#criteriaperipheral3").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdateperipheral3").show();
	 else
		 $("#divdateperipheral3").hide();
    });
	
	$("#criteriaperipheral4").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdateperipheral4").show();
	 else
		 $("#divdateperipheral4").hide();
    });
	
	$("#criteriaperipheral5").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdateperipheral5").show();
	 else
		 $("#divdateperipheral5").hide();
    });
	
	$("#criteriaperipheral6").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdateperipheral6").show();
	 else
		 $("#divdateperipheral6").hide();
    });
	
	$("#criteriaperipheral7").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdateperipheral7").show();
	 else
		 $("#divdateperipheral7").hide();
    });
	
	$("#criteriaperipheral8").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdateperipheral8").show();
	 else
		 $("#divdateperipheral8").hide();
    });
	
	$("#criteriaperipheral9").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdateperipheral9").show();
	 else
		 $("#divdateperipheral9").hide();
    });
	
	$("#criteriaperipheral10").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdateperipheral10").show();
	 else
		 $("#divdateperipheral10").hide();
    });
	
	$("#criteriaperipheral11").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdateperipheral11").show();
	 else
		 $("#divdateperipheral11").hide();
    });
	
	$("#criteriaperipheral12").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdateperipheral12").show();
	 else
		 $("#divdateperipheral12").hide();
    });
	
	$("#criteriaperipheral13").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdateperipheral13").show();
	 else
		 $("#divdateperipheral13").hide();
    });
	
	$("#criteriaperipheral14").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdateperipheral14").show();
	 else
		 $("#divdateperipheral14").hide();
    });
	
	$("#criteriaperipheral15").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdateperipheral15").show();
	 else
		 $("#divdateperipheral15").hide();
    });
	
	$("#criteriaperipheral16").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==1)
		 $("#divdateperipheral16").show();
	 else
		 $("#divdateperipheral16").hide();
    });
	
	   $('#form').submit(function(){
		
		var msg="";
		 
		if($('#criteria1').val()!='0' && $("#date1").val()=='')
			msg +='-Fill Entry criteria 1 date!\n';
		if($('#criteria2').val()!='0' && $("#date2").val()=='')
			msg +='-Fill Entry criteria 2 date!\n';
		if($('#criteria3').val()!='0' && $("#date3").val()=='')
			msg +='-Fill Entry criteria 3 date!\n';
		if($('#criteriab1').val()!='0' && $("#dateb1").val()=='')
			msg +='-Fill Additional criteria 1 date!\n';
		if($('#criteriab2').val()!='0' && $("#dateb2").val()=='')
			msg +='-Fill Additional criteria 2 date!\n';
		if($('#criteriab3').val()!='0' && $("#dateb3").val()=='')
			msg +='-Fill Additional criteria 3 date!\n';
		if($('#criteriab4').val()!='0' && $("#dateb4").val()=='')
			msg +='-Fill Additional criteria 4 date!\n';
		 	 
			
		if(msg != '')
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
		
		
	  });	
	  
$("#criteria1").change(function() { calculate(); });
$("#criteria2").change(function() { calculate(); });
$("#criteria3").change(function() { calculate(); });
$("#criteriab1").change(function() { calculate(); });
$("#criteriab2").change(function() { calculate(); });
$("#criteriab3").change(function() { calculate(); });
$("#criteriab4").change(function() { calculate(); });
$("#calcageb1").click(function() { setdate('calcageb1'); });
$("#calcageb2").click(function() { setdate('calcageb2'); });
$("#calcageb3").click(function() { setdate('calcageb3'); });
$("#calcageb4").click(function() { setdate('calcageb4'); });
$("#calcage1").click(function() { setdate('calcage1'); });
$("#calcage2").click(function() { setdate('calcage2'); });



function setdate(date)
{
		if(date=="calcageb1")
			$("#rheumatoiddate").val($("#dateb1").val());
		if(date=="calcageb2")
			$("#rheumatoiddate").val($("#dateb2").val());
		if(date=="calcageb3")
			$("#rheumatoiddate").val($("#dateb3").val());
		if(date=="calcageb4")
			$("#rheumatoiddate").val($("#dateb4").val());
		if(date=="calcage1")
			$("#rheumatoiddate").val($("#date1").val());
		if(date=="calcage2")
			$("#rheumatoiddate").val($("#date2").val());
		
		if($("#rheumatoid").val()=='Yes')
		{
			$("#divraclassified").html("<label>Clasiffied as RA at " + $("#rheumatoiddate").val() + " with score " + $("#totalscore").val() + "/10</label>");
			$("#divraclassified").show();
		}
		else
		{
			$("#divraclassified").empty();
			$("#divraclassified").hide();
		}
			
}

function calculate()
{
		var first_val = $("#criteria1").val();
		var second_val = $("#criteria2").val();
		var third_val = $("#criteria3").val();
		var forth_val = $("#criteriab1").val();
		var fifth_val = $("#criteriab2").val();
		var sixth_val = $("#criteriab3").val();
		var seventh_val = $("#criteriab4").val();
		var previous_total_score = $("#totalscore").val();
		
		//MM/DD/YYYY kai emas einai DD-MM-YYYY
		var first_val_date = "";
		var second_val_date = "";
		
		
		if ($("#date1").val()=="")
			first_val_date ="";
		else
			first_val_date = ($("#date1").val()).split("-")[1]+"/"+($("#date1").val()).split("-")[0]+"/"+($("#date1").val()).split("-")[2];
		
		if ($("#date2").val()=="")
			second_val_date ="";
		else
			second_val_date = ($("#date2").val()).split("-")[1]+"/"+($("#date2").val()).split("-")[0]+"/"+($("#date2").val()).split("-")[2];
		
		/*var forth_val_date = "";
		var fifth_val_date = "";
		var sixth_val_date = "";
		var seventh_val_date = "";
		if ($("#dateb1").val()=="")
			forth_val_date ="";
		else
			forth_val_date = ($("#dateb1").val()).split("-")[1]+"/"+($("#dateb1").val()).split("-")[0]+"/"+($("#dateb1").val()).split("-")[2];
		
		if ($("#dateb2").val()=="")
			fifth_val_date ="";
		else
			fifth_val_date = ($("#dateb2").val()).split("-")[1]+"/"+($("#dateb2").val()).split("-")[0]+"/"+($("#dateb2").val()).split("-")[2];
		
		if ($("#dateb3").val()=="")
			sixth_val_date ="";
		else
			sixth_val_date = ($("#dateb3").val()).split("-")[1]+"/"+($("#dateb3").val()).split("-")[0]+"/"+($("#dateb3").val()).split("-")[2];
		
		if ($("#dateb4").val()=="")
			seventh_val_date ="";
		else
			seventh_val_date = ($("#dateb4").val()).split("-")[1]+"/"+($("#dateb4").val()).split("-")[0]+"/"+($("#dateb4").val()).split("-")[2];
		
		*/
		
		
		var total_score=0;
		
		if(forth_val==2)
			total_score += 1;
		else if(forth_val==3)
			total_score += 2;
		else if(forth_val==4)
			total_score += 3;
		else if(forth_val==5)
			total_score += 5;
		
		if(fifth_val==2)
			total_score += 2;
		else if(fifth_val==3)
			total_score += 3;
		
		if(sixth_val==2)
			total_score += 1;
		
		if(seventh_val==2)
			total_score += 1;
		
		var date='';
		
		if(first_val==1 || second_val==1)
		{
			if(first_val==1 && second_val!=1)
			{
				if(first_val_date!="")
				{	
					var lower_date = new Date();
					var d1 = new Date(first_val_date);
					lower_date=d1;
					date=lower_date.format("dd-mm-yyyy");
					$("#divraclassified").html("<label>Clasiffied as RA at "+date+"</label>");
				}
				else
					$("#divraclassified").html("<label>Clasiffied as RA</label>");
			}
			else if(first_val!=1 && second_val==1)
			{
				if(second_val_date!="")
				{	
					var lower_date = new Date();
					var d1 = new Date(second_val_date);
					lower_date=d1;
					date=lower_date.format("dd-mm-yyyy");
					$("#divraclassified").html("<label>Clasiffied as RA at "+date+"</label>");
				}
				else
					$("#divraclassified").html("<label>Clasiffied as RA</label>");
			}
			else if(second_val==1 && first_val==1)
			{
				var d1 = new Date(first_val_date);
				var d2 = new Date(second_val_date);
				var lower_date = new Date();
				
				if(first_val_date=="" && second_val_date!="")
				{
					lower_date=d2;
					date=lower_date.format("dd-mm-yyyy");
					$("#divraclassified").html("<label>Clasiffied as RA at "+date+"</label>");
				}
				else if(first_val_date!="" && second_val_date=="")
				{
					lower_date=d1;
					date=lower_date.format("dd-mm-yyyy");
					$("#divraclassified").html("<label>Clasiffied as RA at "+date+"</label>");
				}
				else if(first_val_date=="" && second_val_date=="")
					$("#divraclassified").html("<label>Clasiffied as RA</label>");
				else
				{
					if(d1.getTime()>d2.getTime())
						lower_date=d2;
					else
						lower_date=d1;
					
					date=lower_date.format("dd-mm-yyyy");
					$("#divraclassified").html("<label>Clasiffied as RA at "+date+"</label>");
				}
			}
			
			$("#rheumatoiddate").val(date);
			$("#rheumatoid").val('Yes');
			$("#totalscore").val('');
			$("#divraclassified").show();
		}
		else
		{
			if(third_val==1)
			{	
				if(total_score>=6)
				{
					
					/*var lower_date = new Date();
					if(forth_val<2 || forth_val_date=="")
					{
						
						forth_val_date="";
						d1 = new Date("12/12/2200");
					}
					else
						d1 = new Date(forth_val_date);
					
					if(fifth_val<2 || fifth_val_date=="")
					{
						fifth_val_date="";
						d2 = new Date("12/12/2200");
					}
					else
						d2 = new Date(fifth_val_date);
					
					if(sixth_val<2 || sixth_val_date=="")
					{
						sixth_val_date="";
						d3 = new Date("12/12/2200");
					}
					else
						d3 = new Date(sixth_val_date);
					
					if(seventh_val<2 || seventh_val_date=="")
					{
						seventh_val_date="";
						d4 = new Date("12/12/2200");
					}
					else
						d4 = new Date(seventh_val_date);
					
					
					if(d1.getTime()>d2.getTime())
						lower_date=d2;
					else
						lower_date=d1;
					
					if(lower_date.getTime()>d3.getTime())
						lower_date=d3;
					
					if(lower_date.getTime()>d4.getTime())
						lower_date=d4;
					
					if(lower_date.format("dd/mm/yyyy")!="12/12/2200")
					{
						date=lower_date.format("dd-mm-yyyy");
						$("#divraclassified").html("<label>Clasiffied as RA at " + date + " with score "+total_score+"/10</label>");
					}
					else
						$("#divraclassified").html("<label>Clasiffied as RA with score "+total_score+"/10</label>");
					*/
					
					$("#divraclassified").html("<label>Clasiffied as RA with score "+total_score+"/10</label>");
					$("#rheumatoid").val('Yes');
					$("#totalscore").val(total_score);
					//$("#rheumatoiddate").val(date);
					$("#divraclassified").show();
					if(previous_total_score<6)
						alert("set Diagnosis date!");
				}
				else
				{
					$("#divraclassified").empty();
					$("#rheumatoid").val('No');
					$("#totalscore").val('');
					$("#rheumatoiddate").val('');
				}
			}
			else
			{
				$("#divraclassified").empty();
				$("#rheumatoid").val('');
				$("#totalscore").val('');
				$("#rheumatoiddate").val('');
			}
		}
			
		
		
}	

  
	$("#criteria1").change(function(){
     var criteria1_val = $(this).val();
	 if(criteria1_val==0)
		 $("#divdate1").hide();
	 else
		 $("#divdate1").show();
    });
	
	$("#criteria2").change(function(){
     var criteria2_val = $(this).val();
	 if(criteria2_val==0)
		 $("#divdate2").hide();
	 else
		 $("#divdate2").show();
    });
	
	$("#criteria3").change(function(){
     var criteria3_val = $(this).val();
	 /*if(criteria3_val==0)
		 $("#divdate3").hide();
	 else
		 $("#divdate3").show();
	 */
	 
	 if(criteria3_val==1)
		 $("#divnewlycriteria").show();
	 else
		 $("#divnewlycriteria").hide();
	 
    });
	
	$("#criteriab1").change(function(){
     var criteriab1_val = $(this).val();
	 if(criteriab1_val==0)
		 $("#divdateb1").hide();
	 else
		 $("#divdateb1").show();
    });
	
	$("#criteriab2").change(function(){
     var criteriab2_val = $(this).val();
	 if(criteriab2_val==0)
		 $("#divdateb2").hide();
	 else
		 $("#divdateb2").show();
    });

	$("#criteriab3").change(function(){
     var criteriab3_val = $(this).val();
	 if(criteriab3_val==0)
		 $("#divdateb3").hide();
	 else
		 $("#divdateb3").show();
    });
	
	$("#criteriab4").change(function(){
     var criteriab4_val = $(this).val();
	 if(criteriab4_val==0)
		 $("#divdateb4").hide();
	 else
		 $("#divdateb4").show();
    });

/*$('#date1 input').datepicker({ format: "dd-mm-yyyy" });
$('#date2 input').datepicker({ format: "dd-mm-yyyy" });
$('#date3 input').datepicker({ format: "dd-mm-yyyy" });
$('#dateb1 input').datepicker({ format: "dd-mm-yyyy" });
$('#dateb2 input').datepicker({ format: "dd-mm-yyyy" });
$('#dateb3 input').datepicker({ format: "dd-mm-yyyy" });
$('#dateb4 input').datepicker({ format: "dd-mm-yyyy" });*/
	
$( function() {
    $( "#accordion" ).accordion({
     heightStyle: "content",
	 active: false,
	 collapsible: true
    });
  } );

function setmain(id)
{
	var rows=document.getElementById('diagnosis_numofrows').value;
	//var inactive=document.getElementById('inactive_'+id).checked;
	//alert (id);
	//alert(document.getElementById('inactive_'+id).checked);
	//ean to checkbox einai inactive tote kane uncheck ola ta inactive
	if(document.getElementById('main_'+id).checked==true)
	{
		if (document.getElementById('inactive_'+id).checked==true) {
			for(i = 1; i <= rows; i++){
				if(i!=id && document.getElementById('inactive_'+i).checked==true)
					$('#main_'+i).prop('checked', false);
			}
		}
		else{
			for(i = 1; i <= rows; i++){
				//alert(document.getElementById('inactive_'+i).checked);
				if(i!=id && document.getElementById('inactive_'+i).checked==false)
					$('#main_'+i).prop('checked', false);
			}
		}
	}
}

function diagnosis_addrow()
{
  var tbl = document.getElementById('diagnosis_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'diagnosis_tr_'+iteration;
  document.getElementById('diagnosis_numofrows').value = iteration;
  
  var diagnosis_ids=document.getElementById('diagnosis_ids').value;
	var diagnosis_values=document.getElementById('diagnosis_values').value;
	var diagnosis_counter=document.getElementById('diagnosis_counter').value;
	var diagnosis_value=diagnosis_values.split('!@#$%^');
	var diagnosis_id=diagnosis_ids.split('!@#$%^');
	
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('select');
  el1.name = 'diagnosis_id_' + iteration;
  el1.id = 'diagnosis_id_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
  
	var opt=new Option("--",0);
	el1.add(opt,undefined);
	for(i = 0; i < diagnosis_counter; i++)
	{
		var opt=new Option(diagnosis_value[i],diagnosis_id[i]);
		el1.add(opt,undefined);
	}
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'diagnosis_hidden_' + iteration;
  el2.id = 'diagnosis_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'diagnoses_id_' + iteration;
  el3.id = 'diagnoses_id_' + iteration;
  el3.value='';
  
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
  var r22 = row.insertCell(1);
  r22.align='center';
  
  var r23 = row.insertCell(2);
  r23.align='center';
  
  var r2 = row.insertCell(3);
  r2.align='center';
  var el4 = document.createElement('input');
  el4.type = 'checkbox';
  el4.name = 'main_' + iteration;
  el4.id = 'main_' + iteration;
  el4.onchange =function(){ setmain(iteration)};
  
  r2.appendChild(el4);
  
	var r3 = row.insertCell(4);
  r3.align='center';
  var el5 = document.createElement('input');
  el5.type = 'text';
  el5.name = 'symptom_date_' + iteration;
  el5.id = 'symptom_date_' + iteration;
  el5.className="form-control";
  el5.style.width='100%';
  
  r3.appendChild(el5);
  
  var r4 = row.insertCell(5);
  r4.align='center';
  
  var el6 = document.createElement('input');
  el6.type = 'text';
  el6.name = 'disease_date_' + iteration;
  el6.id = 'disease_date_' + iteration;
  el6.className="form-control";
  el6.style.width='100%';
  
  r4.appendChild(el6);
  
  var r26 = row.insertCell(6);
  r26.align='center';
  var el46 = document.createElement('input');
  el46.type = 'checkbox';
  el46.name = 'inactive_' + iteration;
  el46.id = 'inactive_' + iteration;
  el46.onchange =function(){ setmain(iteration)};
  
  r26.appendChild(el46);
  
  var r47 = row.insertCell(7);
  r47.align='center';
  
  var el67 = document.createElement('input');
  el67.type = 'text';
  el67.name = 'inactive_date_' + iteration;
  el67.id = 'inactive_date_' + iteration;
  el67.className="form-control";
  el67.style.width='100%';
  
  r47.appendChild(el67);
  
  var r5 = row.insertCell(8);
  r5.align='center';
  

  var el7 = document.createElement('a');
  el7.name = 'a_' + iteration;
  el7.id = 'a_' + iteration;
  el7.title = 'Delete';
  el7.href="javascript:avoid(0)";
  var icon = document.createElement('i');
  icon.className='fa fa-trash-o';
  el7.appendChild(icon);
  el7.onclick =function(){diagnosis_removerow(iteration)};
		 
  r5.appendChild(el7);
  
  /*$('#symptom_date_'+ iteration).datepicker({
		  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })*/
  $('#symptom_date_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
  
  /*$('#disease_date_'+ iteration).datepicker({
		  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })*/
  $('#disease_date_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
  
  $('#inactive_date_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
}

function diagnosis_removerow(row)
{
	document.getElementById('diagnosis_hidden_'+row).value='-1';
	document.getElementById('diagnosis_tr_'+row).style.display='none';
}

$(document).ready(function () {
	
for(i=1;i<=document.getElementById('diagnosis_numofrows').value;i++)
{
	/*$('#symptom_date_'+i).datepicker({
	   dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		 onClose2:function(dateText) {}
	});*/
	
	$('#symptom_date_'+i).inputmask();
	
	
	/*$('#disease_date_'+i).datepicker({
	  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	});*/
	
	$('#disease_date_'+i).inputmask();
	
	$('#inactive_date_'+i).inputmask();
}
});
</script>
<?php
	
if($save==1)
{
	if($save_chk=="ok")
	{
?>
<script>
$(".alert_suc").show();
window.setTimeout(function() {
    $(".alert_suc").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
	else
	{
?>
<script>
$(".alert_wr").show();
window.setTimeout(function() {
    $(".alert_wr").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
}
?>
</body>
</html>
