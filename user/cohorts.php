		
<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	$pat_id=$_REQUEST['pat_id'];
	$save=$_REQUEST['save'];
	$patient_cohort_id=$_REQUEST['patient_cohort_id'];
	echo $patient_cohort_id;
	if($pat_id<>"")
		$_SESSION['pat_id']=$pat_id;
	
	$pat_id=$_SESSION['pat_id'];
	$_SESSION['patient_cohort_id']="";
	$_SESSION['patient_followup_id']="";
	$_SESSION['patient_event_id']="";
	
	$exec = get_patient_demographics($pat_id);
	$result = pg_fetch_array($exec);
	$dateofinclusion=$result['dateofinclusion_str'];
	if($dateofinclusion=="12-12-1900")
		$dateofinclusion="";
	$dateofbirth=$result['dateofbirth_str'];
	if($dateofbirth=="12-12-1900")
		$dateofbirth="";
	$gender=$result['gender'];
	if($gender==1)
		$gender_str="Female";
	else if($gender==2)
		$gender_str="Male";
	$patient_id=$result['patient_id'];
	if($save==1 and $patient_cohort_id<>"")
	{
		pg_query("BEGIN") or die("Could not start transaction\n");
		try
		{
			$table_names[0]='deleted';
			$parameters[0]=1;
			$edit_name[0]='patient_cohort_id';
			$edit_id[0]=$patient_cohort_id;
			$sumbol[0]='=';
			$msg_cohort=update('patient_cohort',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
			$msg_cohort_drugs=update('patient_cohort_drugs',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
			$msg_cohort_cohort_drugs_period=update('patient_cohort_drugs_period',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
			$msg_patient_cohort_previous_drugs=update('patient_cohort_previous_drugs',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
			$msg_patient_corticosteroids=update('patient_corticosteroids',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
			$msg_patient_lookup_drugs=update('patient_lookup_drugs',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
			$msg_patient_event=update('patient_event',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
			$msg_patient_followup=update('patient_followup',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
			$msg_patient_followup_drugs=update('patient_followup_drugs',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
			$msg_patient_followup_drugs_period=update('patient_followup_drugs_period',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
			
			
			if ($msg_cohort=="ok" and $msg_cohort_drugs=="ok" and $msg_cohort_cohort_drugs_period=="ok" and $msg_patient_cohort_previous_drugs=="ok" and $msg_patient_corticosteroids=="ok" and $msg_patient_lookup_drugs=="ok" and $msg_patient_event=="ok" and $msg_patient_followup=="ok" and $msg_patient_followup_drugs=="ok" and $msg_patient_followup_drugs_period=="ok")
			{
				$save_chk="ok";
				pg_query("COMMIT") or die("Transaction commit failed\n");
			}
			else
			{
				$save_chk="nok";
				pg_query("ROLLBACK") or die("Transaction rollback failed\n");
			}
			
		}
		catch(exception $e) {
			echo "e:".$e."</br>";
			echo "ROLLBACK";
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
	}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
	   <h1>
          Cohort
		</h1>
		<h1>
		<?php echo "<small>Patient:".$patient_id.",".$gender_str." (".$dateofbirth.") </small>"; ?>
      </h1>
      </section>

      <!-- Main content -->
      <section class="content">
	  <form id="form" name="form" action="cohorts.php" method="POST">
	  <input type="hidden" id="save" name="save" value="1">
		<input type="hidden" id="pat_id" name="pat_id" value="<?php echo $pat_id;?>">
		<input type="hidden" id="patient_cohort_id" name="patient_cohort_id" value="">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Cohorts</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-12" id="divdiagnosis" name="divdiagnosis">
								<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="patient_biobanking_tbl" name="patient_biobanking_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-3"><b>Cohort name</b></th>
											<th class="col-md-2"><b>Treatment nbr</b></th>
											<th class="col-md-3"><b>Start date</b></th>
											<th class="col-md-2"><b>Stop date</b></th>
											<th class="col-md-1">&nbsp;</th>
											<th class="col-md-1">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
											$order = get_cohorts($pat_id);
											$num_rows = pg_num_rows($order);
											
											$checkstopcohorts=0;
											while($result = pg_fetch_assoc($order))
											{
												$patient_cohort_id=$result['patient_cohort_id'];
												$cohort_name_str=$result['cohort_name_str'];
												$treatment_nbr=$result['treatment_nbr'];
												$start_date=$result['start_date_str'];
												if($start_date=="12-12-1900")
													$start_date="";
												$stop_date=$result['stop_date_str'];
												if($stop_date=="12-12-1900" or $stop_date=="")
												{
													$stop_date="";
													$checkstopcohorts++;
												}
											
									?>
										  <tr id="patient_biobanking_tr_<?php echo $i;?>"> 
												<td align="center">
													<label><?php echo $cohort_name_str; ?></label>
												</td>
												<td align="center">
													<label><?php echo $treatment_nbr; ?></label>
												</td>
												<td align="center">
													<label><?php echo $start_date; ?></label>
												</td>
												<td align="center">
													<label><?php echo $stop_date; ?></label>
												</td>
												<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="deletecohort(<?php echo $patient_cohort_id; ?>)"><i class="fa fa-trash"></i><span></a>
												  </td>
													<td align="center">
													<a title="Edit" href="cohort_edit.php?patient_cohort_id=<?php echo $patient_cohort_id; ?>"><i class="fa fa-edit"></i><span></a>
												  </td>
												  	
												</tr>
									<?php
											$i++;
											$j++;
											}
										
									?>
									</tbody>
								</table>
								<br>
									<input type="hidden" id="checkstopcohorts" name="checkstopcohorts" value="<?php echo $checkstopcohorts; ?>" >
								 <a class="btn btn-primary" href="cohort_edit.php" id="newcohort" name="newcohort"><i class="fa fa-plus"></i> New Cohort</a>
							 </div>
						</div>
					</div>
				</div><!-- diagnoses status div -->
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				&nbsp;
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				&nbsp;
			</div>
		</div>
		</form>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
   <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<?php

if($save==1)
{
	if($save_chk=="ok")
	{
?>
<script>
alert("Record deleted!");
</script>
<?php
	}
	else
	{
?>
<script>
alert("unsuccessful record deletion!");
</script>
<?php
	}
}
?>
<script>
$("#newcohort").click(function(){
		
		var checkstopcohorts = $("#checkstopcohorts").val();
		if(checkstopcohorts > 0)
		{
			alert("You can't start new cohort because you have open cohorts!");
			return false
		}
		else
			return true;
});

function deletecohort(patient_cohort_id)
{
	if (confirm("Are you sure about this action? All data available for this record will be deleted."))
	{
		$("#patient_cohort_id").val(patient_cohort_id);
		document.getElementById('form').submit();
	}
	else
	{
		return false;
	}
}
</script>
</body>
</html>
