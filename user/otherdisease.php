		
<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	$save=$_REQUEST['save'];
	$pat_id=$_REQUEST['pat_id'];
	if($pat_id<>"")
		$_SESSION['pat_id']=$pat_id;
	
	$pat_id=$_SESSION['pat_id'];
	
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<?php
if($save==1)
{
	pg_query("BEGIN") or die("Could not start transaction\n");
	try
	{
		
		//patient_neuropsychiatric_sle table-start
		
		$j=1;
		$patient_neuropsychiatric_sle_numofrows=$_REQUEST['patient_neuropsychiatric_sle_numofrows'];
		for ($i=1;$i<=$patient_neuropsychiatric_sle_numofrows;$i++)
		{
			$patient_neuropsychiatric_sle_hidden=$_REQUEST['patient_neuropsychiatric_sle_hidden_'.$i];
			$patient_neuropsychiatric_sle_id=$_REQUEST['patient_neuropsychiatric_sle_id_'.$i];
			if($patient_neuropsychiatric_sle_hidden=='-1')
			{
				if($patient_neuropsychiatric_sle_id<>"")
				{
					$patient_neuropsychiatric_sle_hdn_parameters[0]=1;
					$patient_neuropsychiatric_sle_hdn_names[0]='deleted';
					$patient_neuropsychiatric_sle_hdn_edit_name[0]='patient_neuropsychiatric_sle_id';
					$patient_neuropsychiatric_sle_hdn_edit_id[0]=$patient_neuropsychiatric_sle_id;
					$patient_neuropsychiatric_sle_hdn_sumbol[0]='=';
					$patient_neuropsychiatric_sle_msg=update('patient_neuropsychiatric_sle',$patient_neuropsychiatric_sle_hdn_names,$patient_neuropsychiatric_sle_hdn_parameters,$patient_neuropsychiatric_sle_hdn_edit_name,$patient_neuropsychiatric_sle_hdn_edit_id,$patient_neuropsychiatric_sle_hdn_sumbol);
					if ($patient_neuropsychiatric_sle_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
				$j++;
		}
		
		if ($j==$i)
			$neuropsychiatric_sle_msg="ok";
		else
			$neuropsychiatric_sle_msg="nok";
		//patient_neuropsychiatric_sle table-end
		
		//patient_lupus_nefritis table-start
		
		$j=1;
		$patient_lupus_nefritis_numofrows=$_REQUEST['patient_lupus_nefritis_numofrows'];
		for ($i=1;$i<=$patient_lupus_nefritis_numofrows;$i++)
		{
			$patient_lupus_nefritis_hidden=$_REQUEST['patient_lupus_nefritis_hidden_'.$i];
			$patient_lupus_nefritis_id=$_REQUEST['patient_lupus_nefritis_id_'.$i];
			if($patient_lupus_nefritis_hidden=='-1')
			{
				if($patient_lupus_nefritis_id<>"")
				{
					$patient_lupus_nefritis_hdn_parameters[0]=1;
					$patient_lupus_nefritis_hdn_names[0]='deleted';
					$patient_lupus_nefritis_hdn_edit_name[0]='patient_lupus_nefritis_id';
					$patient_lupus_nefritis_hdn_edit_id[0]=$patient_lupus_nefritis_id;
					$patient_lupus_nefritis_hdn_sumbol[0]='=';
					$patient_lupus_nefritis_msg=update('patient_lupus_nefritis',$patient_lupus_nefritis_hdn_names,$patient_lupus_nefritis_hdn_parameters,$patient_lupus_nefritis_hdn_edit_name,$patient_lupus_nefritis_hdn_edit_id,$patient_lupus_nefritis_hdn_sumbol);
					if ($patient_lupus_nefritis_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
				$j++;
		}
		
		if ($j==$i)
			$lupus_nefritis_msg="ok";
		else
			$lupus_nefritis_msg="nok";
		//patient_lupus_nefritis table-end
		
		//patient_antibodies table-start
		
		$j=1;
		$patient_antibodies_numofrows=$_REQUEST['patient_antibodies_numofrows'];
		for ($i=1;$i<=$patient_antibodies_numofrows;$i++)
		{
			$patient_antibodies_hidden=$_REQUEST['patient_antibodies_hidden_'.$i];
			$patient_autoantibodies_id=$_REQUEST['patient_autoantibodies_id_'.$i];
			if($patient_antibodies_hidden=='-1')
			{
				if($patient_autoantibodies_id<>"")
				{
					$patient_antibodies_hdn_parameters[0]=1;
					$patient_antibodies_hdn_names[0]='deleted';
					$patient_antibodies_hdn_edit_name[0]='patient_autoantibodies_id';
					$patient_antibodies_hdn_edit_id[0]=$patient_autoantibodies_id;
					$patient_antibodies_hdn_sumbol[0]='=';
					$patient_antibodies_msg=update('patient_autoantibodies',$patient_antibodies_hdn_names,$patient_antibodies_hdn_parameters,$patient_antibodies_hdn_edit_name,$patient_antibodies_hdn_edit_id,$patient_antibodies_hdn_sumbol);
					if ($patient_antibodies_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
			{
				
				$patient_antibodies_parameters[0]=$_REQUEST['antibodies_id_'.$i];
				$patient_antibodies_parameters[1]=date_for_postgres($_REQUEST['patient_antibodies_date_'.$i]);
				$patient_antibodies_parameters[2]=$user_id;
				$patient_antibodies_parameters[3]='now()';
				$patient_antibodies_parameters[4]=$_REQUEST['patient_antibodies_options_'.$i];
				$patient_antibodies_parameters[5]=str_replace("'", "''",$_REQUEST['patient_antibodies_titre_'.$i]);
				$patient_antibodies_table_names[0]='antibodies_id';
				$patient_antibodies_table_names[1]='patient_antibodies_date';
				$patient_antibodies_table_names[2]='editor_id';
				$patient_antibodies_table_names[3]='edit_date';
				$patient_antibodies_table_names[4]='patient_antibodies_options';
				$patient_antibodies_table_names[5]='patient_antibodies_titre';
				$patient_antibodies_edit_name[0]='patient_autoantibodies_id';
				$patient_antibodies_edit_id[0]=$patient_autoantibodies_id;
				$patient_antibodies_sumbol[0]='=';
				
				if($patient_autoantibodies_id=='')
				{
					
					$patient_antibodies_parameters[6]=$pat_id;
					$patient_antibodies_parameters[7]=0;
					
					$patient_antibodies_table_names[6]='pat_id';
					$patient_antibodies_table_names[7]='deleted';
					
					$id=insert('patient_autoantibodies',$patient_antibodies_table_names,$patient_antibodies_parameters,'patient_autoantibodies_id');
					if($id!='')
						$j++;
				}
				else
				{
					$patient_antibodies_msg=update('patient_autoantibodies',$patient_antibodies_table_names,$patient_antibodies_parameters,$patient_antibodies_edit_name,$patient_antibodies_edit_id,$patient_antibodies_sumbol);
					if ($patient_antibodies_msg=="ok")
						$j++;
				}
			}
		}
		
		if ($j==$i)
			$antibodies_msg="ok";
		else
			$antibodies_msg="nok";
		//patient_antibodies table-end
		
		//patient_ra_extraarticular table-start
		
		$j=1;
		$patient_ra_extraarticular_numofrows=$_REQUEST['patient_ra_extraarticular_numofrows'];
		for ($i=1;$i<=$patient_ra_extraarticular_numofrows;$i++)
		{
			$patient_ra_extraarticular_hidden=$_REQUEST['patient_ra_extraarticular_hidden_'.$i];
			$patient_ra_extraarticular_id=$_REQUEST['patient_ra_extraarticular_id_'.$i];
			if($patient_ra_extraarticular_hidden=='-1')
			{
				if($patient_ra_extraarticular_id<>"")
				{
					$patient_ra_extraarticular_hdn_parameters[0]=1;
					$patient_ra_extraarticular_hdn_names[0]='deleted';
					$patient_ra_extraarticular_hdn_edit_name[0]='patient_ra_extraarticular_id';
					$patient_ra_extraarticular_hdn_edit_id[0]=$patient_ra_extraarticular_id;
					$patient_ra_extraarticular_hdn_sumbol[0]='=';
					$patient_ra_extraarticular_msg=update('patient_ra_extraarticular',$patient_ra_extraarticular_hdn_names,$patient_ra_extraarticular_hdn_parameters,$patient_ra_extraarticular_hdn_edit_name,$patient_ra_extraarticular_hdn_edit_id,$patient_ra_extraarticular_hdn_sumbol);
					if ($patient_ra_extraarticular_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
			{
				
				$patient_ra_extraarticular_parameters[0]=$_REQUEST['ra_extraarticulars_id_'.$i];
				$patient_ra_extraarticular_parameters[1]=date_for_postgres($_REQUEST['ra_extraarticular_date_'.$i]);
				$patient_ra_extraarticular_parameters[2]=$user_id;
				$patient_ra_extraarticular_parameters[3]='now()';
				$patient_ra_extraarticular_table_names[0]='ra_extraarticular_id';
				$patient_ra_extraarticular_table_names[1]='date';
				$patient_ra_extraarticular_table_names[2]='editor_id';
				$patient_ra_extraarticular_table_names[3]='edit_date';
				$patient_ra_extraarticular_edit_name[0]='patient_ra_extraarticular_id';
				$patient_ra_extraarticular_edit_id[0]=$patient_ra_extraarticular_id;
				$patient_ra_extraarticular_sumbol[0]='=';
				
				if($patient_ra_extraarticular_id=='')
				{
					
					$patient_ra_extraarticular_parameters[4]=$pat_id;
					$patient_ra_extraarticular_parameters[5]=0;
					
					$patient_ra_extraarticular_table_names[4]='pat_id';
					$patient_ra_extraarticular_table_names[5]='deleted';
					
					$id=insert('patient_ra_extraarticular',$patient_ra_extraarticular_table_names,$patient_ra_extraarticular_parameters,'patient_ra_extraarticular_id');
					if($id!='')
						$j++;
				}
				else
				{
					$patient_ra_extraarticular_msg=update('patient_ra_extraarticular',$patient_ra_extraarticular_table_names,$patient_ra_extraarticular_parameters,$patient_ra_extraarticular_edit_name,$patient_ra_extraarticular_edit_id,$patient_ra_extraarticular_sumbol);
					if ($patient_ra_extraarticular_msg=="ok")
						$j++;
				}
			}
		}
		
		if ($j==$i)
			$ra_extraarticular_msg="ok";
		else
			$ra_extraarticular_msg="nok";
		//patient_ra_extraarticular table-end
		
		//patient_spa_extraarticular table-start
		
		$j=1;
		$patient_spa_extraarticular_numofrows=$_REQUEST['patient_spa_extraarticular_numofrows'];
		for ($i=1;$i<=$patient_spa_extraarticular_numofrows;$i++)
		{
			$patient_spa_extraarticular_hidden=$_REQUEST['patient_spa_extraarticular_hidden_'.$i];
			$patient_spa_extraarticular_id=$_REQUEST['patient_spa_extraarticular_id_'.$i];
			if($patient_spa_extraarticular_hidden=='-1')
			{
				if($patient_spa_extraarticular_id<>"")
				{
					$patient_spa_extraarticular_hdn_parameters[0]=1;
					$patient_spa_extraarticular_hdn_names[0]='deleted';
					$patient_spa_extraarticular_hdn_edit_name[0]='patient_spa_extraarticular_id';
					$patient_spa_extraarticular_hdn_edit_id[0]=$patient_spa_extraarticular_id;
					$patient_spa_extraarticular_hdn_sumbol[0]='=';
					$patient_spa_extraarticular_msg=update('patient_spa_extraarticular',$patient_spa_extraarticular_hdn_names,$patient_spa_extraarticular_hdn_parameters,$patient_spa_extraarticular_hdn_edit_name,$patient_spa_extraarticular_hdn_edit_id,$patient_spa_extraarticular_hdn_sumbol);
					if ($patient_spa_extraarticular_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
			{
				
				$patient_spa_extraarticular_parameters[0]=$_REQUEST['spa_extraarticulars_id_'.$i];
				$patient_spa_extraarticular_parameters[1]=date_for_postgres($_REQUEST['spa_extraarticular_date_'.$i]);
				$patient_spa_extraarticular_parameters[2]=$user_id;
				$patient_spa_extraarticular_parameters[3]='now()';
				$patient_spa_extraarticular_table_names[0]='spa_extraarticular_id';
				$patient_spa_extraarticular_table_names[1]='date';
				$patient_spa_extraarticular_table_names[2]='editor_id';
				$patient_spa_extraarticular_table_names[3]='edit_date';
				$patient_spa_extraarticular_edit_name[0]='patient_spa_extraarticular_id';
				$patient_spa_extraarticular_edit_id[0]=$patient_spa_extraarticular_id;
				$patient_spa_extraarticular_sumbol[0]='=';
				
				if($patient_spa_extraarticular_id=='')
				{
					
					$patient_spa_extraarticular_parameters[4]=$pat_id;
					$patient_spa_extraarticular_parameters[5]=0;
					
					$patient_spa_extraarticular_table_names[4]='pat_id';
					$patient_spa_extraarticular_table_names[5]='deleted';
					
					$id=insert('patient_spa_extraarticular',$patient_spa_extraarticular_table_names,$patient_spa_extraarticular_parameters,'patient_spa_extraarticular_id');
					if($id!='')
						$j++;
				}
				else
				{
					$patient_spa_extraarticular_msg=update('patient_spa_extraarticular',$patient_spa_extraarticular_table_names,$patient_spa_extraarticular_parameters,$patient_spa_extraarticular_edit_name,$patient_spa_extraarticular_edit_id,$patient_spa_extraarticular_sumbol);
					if ($patient_spa_extraarticular_msg=="ok")
						$j++;
				}
			}
		}
		
		if ($j==$i)
			$spa_extraarticular_msg="ok";
		else
			$spa_extraarticular_msg="nok";
		//patient_spa_extraarticular table-end
		
		//patient_pulmonary table-start
		
		$j=1;
		$patient_pulmonary_numofrows=$_REQUEST['patient_pulmonary_numofrows'];
		for ($i=1;$i<=$patient_pulmonary_numofrows;$i++)
		{
			$patient_pulmonary_hidden=$_REQUEST['patient_pulmonary_hidden_'.$i];
			$patient_pulmonary_id=$_REQUEST['patient_pulmonary_id_'.$i];
			if($patient_pulmonary_hidden=='-1')
			{
				if($patient_pulmonary_id<>"")
				{
					$patient_pulmonary_hdn_parameters[0]=1;
					$patient_pulmonary_hdn_names[0]='deleted';
					$patient_pulmonary_hdn_edit_name[0]='patient_pulmonary_id';
					$patient_pulmonary_hdn_edit_id[0]=$patient_pulmonary_id;
					$patient_pulmonary_hdn_sumbol[0]='=';
					$patient_pulmonary_msg=update('patient_pulmonary',$patient_pulmonary_hdn_names,$patient_pulmonary_hdn_parameters,$patient_pulmonary_hdn_edit_name,$patient_pulmonary_hdn_edit_id,$patient_pulmonary_hdn_sumbol);
					if ($patient_pulmonary_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
				$j++;
			/*{
				
				$patient_pulmonary_parameters[0]=$_REQUEST['group_'.$i];
				$patient_pulmonary_parameters[1]=$_REQUEST['content_'.$i];
				$patient_pulmonary_parameters[2]=$_REQUEST['specifications_'.$i];
				$patient_pulmonary_parameters[3]=date_for_postgres($_REQUEST['pulmonary_date_'.$i]);
				$patient_pulmonary_parameters[4]=$user_id;
				$patient_pulmonary_parameters[5]='now()';
				$patient_pulmonary_table_names[0]='group_id';
				$patient_pulmonary_table_names[1]='content';
				$patient_pulmonary_table_names[2]='specifications';
				$patient_pulmonary_table_names[3]='pulmonary_date';
				$patient_pulmonary_table_names[4]='editor_id';
				$patient_pulmonary_table_names[5]='edit_date';
				$patient_pulmonary_edit_name[0]='patient_pulmonary_id';
				$patient_pulmonary_edit_id[0]=$patient_pulmonary_id;
				$patient_pulmonary_sumbol[0]='=';
				
				if($patient_pulmonary_id=='')
				{
					
					$patient_pulmonary_parameters[6]=$pat_id;
					$patient_pulmonary_parameters[7]=0;
					
					$patient_pulmonary_table_names[6]='pat_id';
					$patient_pulmonary_table_names[7]='deleted';
					
					$id=insert('patient_pulmonary',$patient_pulmonary_table_names,$patient_pulmonary_parameters,'patient_pulmonary_id');
					if($id!='')
						$j++;
				}
				else
				{
					$patient_pulmonary_msg=update('patient_pulmonary',$patient_pulmonary_table_names,$patient_pulmonary_parameters,$patient_pulmonary_edit_name,$patient_pulmonary_edit_id,$patient_pulmonary_sumbol);
					if ($patient_pulmonary_msg=="ok")
						$j++;
				}
				
			}*/
			
		}
		if ($j==$i)
			$pulmonary_msg="ok";
		else
			$pulmonary_msg="nok";
		//patient_pulmonary table-end
		
		//patient_cutaneous table-start
		
		$j=1;
		$patient_cutaneous_numofrows=$_REQUEST['patient_cutaneous_numofrows'];
		for ($i=1;$i<=$patient_cutaneous_numofrows;$i++)
		{
			$patient_cutaneous_hidden=$_REQUEST['patient_cutaneous_hidden_'.$i];
			$patient_cutaneous_id=$_REQUEST['patient_cutaneous_id_'.$i];
			if($patient_cutaneous_hidden=='-1')
			{
				if($patient_cutaneous_id<>"")
				{
					$patient_cutaneous_hdn_parameters[0]=1;
					$patient_cutaneous_hdn_names[0]='deleted';
					$patient_cutaneous_hdn_edit_name[0]='patient_cutaneous_id';
					$patient_cutaneous_hdn_edit_id[0]=$patient_cutaneous_id;
					$patient_cutaneous_hdn_sumbol[0]='=';
					$patient_cutaneous_msg=update('patient_cutaneous',$patient_cutaneous_hdn_names,$patient_cutaneous_hdn_parameters,$patient_cutaneous_hdn_edit_name,$patient_cutaneous_hdn_edit_id,$patient_cutaneous_hdn_sumbol);
					if ($patient_cutaneous_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
			{
				
				$patient_cutaneous_parameters[0]=$_REQUEST['skin_biopsy_'.$i];
				$patient_cutaneous_parameters[1]=date_for_postgres($_REQUEST['patient_cutaneous_date_'.$i]);
				$patient_cutaneous_parameters[2]=$user_id;
				$patient_cutaneous_parameters[3]='now()';
				$patient_cutaneous_parameters[4]=$_REQUEST['cle_subtype_id_'.$i];
				$patient_cutaneous_parameters[5]=$_REQUEST['performed_'.$i];
				$patient_cutaneous_table_names[0]='skin_biopsy';
				$patient_cutaneous_table_names[1]='patient_cutaneous_date';
				$patient_cutaneous_table_names[2]='editor_id';
				$patient_cutaneous_table_names[3]='edit_date';
				$patient_cutaneous_table_names[4]='cle_subtype_id';
				$patient_cutaneous_table_names[5]='performed';
				$patient_cutaneous_edit_name[0]='patient_cutaneous_id';
				$patient_cutaneous_edit_id[0]=$patient_cutaneous_id;
				$patient_cutaneous_sumbol[0]='=';
				
				if($patient_cutaneous_id=='')
				{
					
					$patient_cutaneous_parameters[6]=$pat_id;
					$patient_cutaneous_parameters[7]=0;
					
					$patient_cutaneous_table_names[6]='pat_id';
					$patient_cutaneous_table_names[7]='deleted';
					
					$id=insert('patient_cutaneous',$patient_cutaneous_table_names,$patient_cutaneous_parameters,'patient_cutaneous_id');
					if($id!='')
						$j++;
				}
				else
				{
					$patient_cutaneous_msg=update('patient_cutaneous',$patient_cutaneous_table_names,$patient_cutaneous_parameters,$patient_cutaneous_edit_name,$patient_cutaneous_edit_id,$patient_cutaneous_sumbol);
					if ($patient_cutaneous_msg=="ok")
						$j++;
				}
			}
		}
		
		if ($j==$i)
			$cutaneous_msg="ok";
		else
			$cutaneous_msg="nok";
		//patient_cutaneous table-end
		
		if ($lupus_nefritis_msg=="ok" and $cutaneous_msg=="ok" and $ra_extraarticular_msg=="ok" and $spa_extraarticular_msg=="ok" and $antibodies_msg=="ok" and $pulmonary_msg=="ok")
		{
			$save_chk="ok";
			pg_query("COMMIT") or die("Transaction commit failed\n");
		}
		else
		{
			$save_chk="nok";
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
	}
	catch(exception $e) {
		echo "e:".$e."</br>";
		echo "ROLLBACK";
		pg_query("ROLLBACK") or die("Transaction rollback failed\n");
	}
	
}

$exec = get_patient_demographics($pat_id);
$result = pg_fetch_array($exec);
$dateofinclusion=$result['dateofinclusion_str'];
if($dateofinclusion=="12-12-1900")
	$dateofinclusion="";
$dateofbirth=$result['dateofbirth_str'];
if($dateofbirth=="12-12-1900")
	$dateofbirth="";
$gender=$result['gender'];
if($gender==1)
	$gender_str="Female";
else if($gender==2)
	$gender_str="Male";
$patient_id=$result['patient_id'];
?>
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
	   <h1>
          Other disease characteristics
		</h1>
		<h1>
		<?php echo "<small>Patient:".$patient_id.",".$gender_str." (".$dateofbirth.") </small>"; ?>
      </h1>
      </section>

      <!-- Main content -->
      <section class="content">
	  <div class="alert alert_suc alert-success" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Successful save!</strong>
	  </div>
	  <div class="alert alert_wr alert-danger" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Unsuccessful save!</strong>
	  </div>
	  <form id="form" action="otherdisease.php" method="POST">
	  <input type="hidden" id="save" name="save" value="1">
		<input type="hidden" id="pat_id" name="pat_id" value="<?php echo $pat_id;?>">
		
		<?php
		//Βρες ποια από τα other characteristics είναι ενεργά
		
			$order = get_diagnoses($pat_id);
			$num_rows = pg_num_rows($order);
			$show_characteristics1=0;
			$show_characteristics2=0;
			$show_characteristics3=0;
			$show_characteristics4=0;
			$show_characteristics5=0;
			$show_characteristics6=0;
			$show_characteristics7=0;
			
			while($result = pg_fetch_assoc($order))
			{
				$diagnosis_id=$result['diagnosis_id'];
				
				$exec3 = get_othercriteria($diagnosis_id,1);
				$result3 = pg_fetch_array($exec3);
				$other_characteristics1=$result3['active'];
				if($other_characteristics1==1)
					$show_characteristics1++;
				
								
				$exec3 = get_othercriteria($diagnosis_id,2);
				$result3 = pg_fetch_array($exec3);
				$other_characteristics2=$result3['active'];
				if($other_characteristics2==1)
					$show_characteristics2++;
				
								
				$exec3 = get_othercriteria($diagnosis_id,3);
				$result3 = pg_fetch_array($exec3);
				$other_characteristics3=$result3['active'];
				if($other_characteristics3==1)
					$show_characteristics3++;
				
								
				$exec3 = get_othercriteria($diagnosis_id,4);
				$result3 = pg_fetch_array($exec3);
				$other_characteristics4=$result3['active'];
				if($other_characteristics4==1)
					$show_characteristics4++;
				
								
				$exec3 = get_othercriteria($diagnosis_id,5);
				$result3 = pg_fetch_array($exec3);
				$other_characteristics5=$result3['active'];
				if($other_characteristics5==1)
					$show_characteristics5++;
				
								
				$exec3 = get_othercriteria($diagnosis_id,6);
				$result3 = pg_fetch_array($exec3);
				$other_characteristics6=$result3['active'];
				if($other_characteristics6==1)
					$show_characteristics6++;
						
				
				$exec3 = get_othercriteria($diagnosis_id,7);
				$result3 = pg_fetch_array($exec3);
				$other_characteristics7=$result3['active'];
				if($other_characteristics7==1)
					$show_characteristics7++;
								
			}
			
			/*echo "show_characteristics1:$show_characteristics1</br>";
			echo "show_characteristics2:$show_characteristics2</br>";
			echo "show_characteristics3:$show_characteristics3</br>";
			echo "show_characteristics4:$show_characteristics4</br>";
			echo "show_characteristics5:$show_characteristics5</br>";
			echo "show_characteristics6:$show_characteristics6</br>";
			echo "show_characteristics7:$show_characteristics7</br>";*/
		?>
		<?php
		if($show_characteristics1>0 or $show_characteristics2>0 or $show_characteristics3>0 or $show_characteristics4>0 or $show_characteristics5>0 or $show_characteristics6>0 or $show_characteristics7>0)
		{
		?>
		<div class="row">
			<div class="form-group col-md-12">
				<input type="submit" class="btn btn-primary" value="Save">
			</div>
		</div>
		<?php
		}
		
		if($show_characteristics1>0)
		{
			
			$exec2 = get_explanation($id);
				$result2 = pg_fetch_array($exec2);
				$autoantibodies_explanation_id=$result2['autoantibodies_explanation_id'];
				
				
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Autoantibodies</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-12 col-xs-12" id="divautoantibodies" name="divautoantibodies">
								<?php
										$sql = get_lookup_tbl_id('autoantibodies');
										$autoantibodies_counter=0;				
										while($result2 = pg_fetch_array($sql))
										{	
											$autoantibodies_values .= $result2['value'];
											$autoantibodies_values .= "!@#$%^" ;
											$autoantibodies_ids .=$result2['id'];
											$autoantibodies_ids .= "!@#$%^" ;
															
											$autoantibodies_counter++;
										}
										
									?>
										<input type="hidden" value="<?php echo $autoantibodies_counter; ?>" id="autoantibodies_counter" name="autoantibodies_counter"/>
										<input type="hidden" value="<?php echo $autoantibodies_values; ?>" id="autoantibodies_values" name="autoantibodies_values"/>
										<input type="hidden" value="<?php echo $autoantibodies_ids; ?>" id="autoantibodies_ids" name="autoantibodies_ids"/>
										
										<?php
										$sql = get_explanations();				
										while($result2 = pg_fetch_array($sql))
										{	
											$autoantibodies_tooltip_values .= $result2['explanation'];
											$autoantibodies_tooltip_values .= "!@#$%^" ;
											$autoantibodies_tooltip_ids .=$result2['lookup_tbl_val_id'];
											$autoantibodies_tooltip_ids .= "!@#$%^" ;
															
											$autoantibodies_tooltip_counter++;
										}
										
									?>
										<input type="hidden" value="<?php echo $autoantibodies_tooltip_counter; ?>" id="autoantibodies_tooltip_counter" name="autoantibodies_tooltip_counter"/>
										<input type="hidden" value="<?php echo $autoantibodies_tooltip_values; ?>" id="autoantibodies_tooltip_values" name="autoantibodies_tooltip_values"/>
										<input type="hidden" value="<?php echo $autoantibodies_tooltip_ids; ?>" id="autoantibodies_tooltip_ids" name="autoantibodies_tooltip_ids"/>
								  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="patient_antibodies_tbl" name="patient_antibodies_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-3"><b>Item</b></th>
											<th class="col-md-2"><b>Options</b></th>
											<th class="col-md-3"><b>Titre</b></th>
											<th class="col-md-2"><b>Date</b></th>
											<th class="col-md-2">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
											$order = get_patient_antibodies($pat_id);
											$num_rows = pg_num_rows($order);
											
											while($result = pg_fetch_assoc($order))
											{
												$patient_autoantibodies_id=$result['patient_autoantibodies_id'];
												$antibodies_id=$result['antibodies_id'];
												$patient_antibodies_options=$result['patient_antibodies_options'];
												$patient_antibodies_titre=$result['patient_antibodies_titre'];
												$patient_antibodies_date=$result['patient_antibodies_date_str'];
												if($patient_antibodies_date=="12-12-1900")
													$patient_antibodies_date="";
											
									?>
										  <tr id="patient_antibodies_tr_<?php echo $i;?>"> 
											   <td align="center">
													<input type="hidden" id="patient_antibodies_hidden_<?php echo $i;?>" name="patient_antibodies_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="patient_autoantibodies_id_<?php echo $i; ?>" id="patient_autoantibodies_id_<?php echo $i; ?>" value="<?php echo $patient_autoantibodies_id;?>">
													<select class="form-control"  onchange="settooltip('<?php echo $i; ?>');"  name="antibodies_id_<?php echo $i; ?>" id="antibodies_id_<?php echo $i; ?>">
														<option value="0">--</option>
														<?php
															$sql = get_lookup_tbl_id('autoantibodies');
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$antibodies_id2=$result2['id'];
																$value=$result2['value'];
														?>
															<option value="<?php echo $antibodies_id2; ?>" <?php if($antibodies_id==$antibodies_id2) { echo "selected"; } ?>><?php echo $value; ?></option>
														<?php
															}
															
															$exec3 = get_explanation($antibodies_id);
															$result3 = pg_fetch_array($exec3);
															$explanation=$result3['explanation'];
																
														?>
														
														
													</select>
												</td>
												<td align="center">
													<select class="form-control" title="<?php echo $explanation; ?>" name="patient_antibodies_options_<?php echo $i; ?>" id="patient_antibodies_options_<?php echo $i; ?>">
														<option value="0">--</option>
														<option value="1" <?php if($patient_antibodies_options==1) { echo "selected"; } ?>>Unknown</option>
														<option value="2" <?php if($patient_antibodies_options==2) { echo "selected"; } ?>>Negative</option>
														<option value="3" <?php if($patient_antibodies_options==3) { echo "selected"; } ?>>Low positive</option>
														<option value="4" <?php if($patient_antibodies_options==4) { echo "selected"; } ?>>Medium positive</option>
														<option value="5" <?php if($patient_antibodies_options==5) { echo "selected"; } ?>>High positive</option>
													</select>
													<!--<i class="fa fa-question-circle 2x"  title="<?php //echo $explanation; ?>" name="tooltip_<?php //echo $i; ?>" id="tooltip_<?php //echo $i; ?>"></i>-->
												</td>
												<td align="center">
													<input type="text" class="form-control" id="patient_antibodies_titre_<?php echo $i; ?>" name="patient_antibodies_titre_<?php echo $i; ?>" value="<?php echo $patient_antibodies_titre; ?>">
												</td>
												<td align="center">
													<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="patient_antibodies_date_<?php echo $i; ?>" name="patient_antibodies_date_<?php echo $i; ?>" value="<?php echo $patient_antibodies_date; ?>">
												</td>
													<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="patient_antibodies_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
												  </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										
									?>
									</tbody>
								</table>
								<input type="hidden" name="patient_antibodies_numofrows" id="patient_antibodies_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								 <a class="btn btn-primary" href="javascript:void(0);" onclick="patient_antibodies_addrow();"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
					</div>
				</div><!-- diagnoses status div -->
			</div>
		</div>
		<?php
		}
		?>
		<?php
		if($show_characteristics2>0)
		{
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">RA extraarticular manifestations</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-8 col-xs-12" id="divdiagnosis" name="divdiagnosis">
							<?php
										$sql = get_lookup_tbl_id('ra_extraarticular');
										$ra_extraarticular_counter=0;				
										while($result2 = pg_fetch_array($sql))
										{	
											$ra_extraarticular_values .= $result2['value'];
											$ra_extraarticular_values .= "!@#$%^" ;
											$ra_extraarticular_ids .=$result2['id'];
											$ra_extraarticular_ids .= "!@#$%^" ;
															
											$ra_extraarticular_counter++;
										}
									?>
										<input type="hidden" value="<?php echo $ra_extraarticular_counter; ?>" id="ra_extraarticular_counter" name="ra_extraarticular_counter"/>
										<input type="hidden" value="<?php echo $ra_extraarticular_values; ?>" id="ra_extraarticular_values" name="ra_extraarticular_values"/>
										<input type="hidden" value="<?php echo $ra_extraarticular_ids; ?>" id="ra_extraarticular_ids" name="ra_extraarticular_ids"/>
								  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="patient_ra_extraarticular_tbl" name="patient_ra_extraarticular_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-5"><b>Item</b></th>
											<th class="col-md-5"><b>Date</b></th>
											<th class="col-md-2">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
											$order = get_ra_extraarticular($pat_id);
											$num_rows = pg_num_rows($order);
											
											while($result = pg_fetch_assoc($order))
											{
												$patient_ra_extraarticular_id=$result['patient_ra_extraarticular_id'];
												$ra_extraarticular_id=$result['ra_extraarticular_id'];
												$ra_extraarticular_date=$result['date_str'];
												if($ra_extraarticular_date=="12-12-1900")
													$ra_extraarticular_date="";
											
									?>
										  <tr id="patient_ra_extraarticular_tr_<?php echo $i;?>"> 
											   <td align="center">
													<input type="hidden" id="patient_ra_extraarticular_hidden_<?php echo $i;?>" name="patient_ra_extraarticular_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="patient_ra_extraarticular_id_<?php echo $i; ?>" id="patient_ra_extraarticular_id_<?php echo $i; ?>" value="<?php echo $patient_ra_extraarticular_id;?>">
													<select class="form-control" name="ra_extraarticulars_id_<?php echo $i; ?>" id="ra_extraarticulars_id_<?php echo $i; ?>">
														<option value="0">--</option>
														<?php
															$sql = get_lookup_tbl_id('ra_extraarticular');
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$ra_extraarticular_id2=$result2['id'];
																$value=$result2['value'];
														?>
															<option value="<?php echo $ra_extraarticular_id2; ?>" <?php if($ra_extraarticular_id==$ra_extraarticular_id2) { echo "selected"; } ?>><?php echo $value; ?></option>
														<?php
															}
																
														?>
														
														
													</select>
												</td>
												<td align="center">
													<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="ra_extraarticular_date_<?php echo $i; ?>" name="ra_extraarticular_date_<?php echo $i; ?>" value="<?php echo $ra_extraarticular_date; ?>">
												</td>
													<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="patient_ra_extraarticular_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
												  </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										
									?>
									</tbody>
								</table>
								<input type="hidden" name="patient_ra_extraarticular_numofrows" id="patient_ra_extraarticular_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								 <a class="btn btn-primary" href="javascript:void(0);" onclick="patient_ra_extraarticular_addrow();"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
					</div>
				</div><!-- diagnoses status div -->
			</div>
		</div>
		<?php
		}
		?>
		<?php
		if($show_characteristics3>0)
		{
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">SpA extraarticular manifestations</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group  col-md-8 col-xs-12" id="divdiagnosis" name="divdiagnosis">
							<?php
										$sql = get_lookup_tbl_id('spa_extraarticular');
										$spa_extraarticular_counter=0;				
										while($result2 = pg_fetch_array($sql))
										{	
											$spa_extraarticular_values .= $result2['value'];
											$spa_extraarticular_values .= "!@#$%^" ;
											$spa_extraarticular_ids .=$result2['id'];
											$spa_extraarticular_ids .= "!@#$%^" ;
															
											$spa_extraarticular_counter++;
										}
									?>
										<input type="hidden" value="<?php echo $spa_extraarticular_counter; ?>" id="spa_extraarticular_counter" name="spa_extraarticular_counter"/>
										<input type="hidden" value="<?php echo $spa_extraarticular_values; ?>" id="spa_extraarticular_values" name="spa_extraarticular_values"/>
										<input type="hidden" value="<?php echo $spa_extraarticular_ids; ?>" id="spa_extraarticular_ids" name="spa_extraarticular_ids"/>
								  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="patient_spa_extraarticular_tbl" name="patient_spa_extraarticular_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-5"><b>Item</b></th>
											<th class="col-md-5"><b>Date</b></th>
											<th class="col-md-2">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
											$order = get_spa_extraarticular($pat_id);
											$num_rows = pg_num_rows($order);
											
											while($result = pg_fetch_assoc($order))
											{
												$patient_spa_extraarticular_id=$result['patient_spa_extraarticular_id'];
												$spa_extraarticular_id=$result['spa_extraarticular_id'];
												$spa_extraarticular_date=$result['date_str'];
												if($spa_extraarticular_date=="12-12-1900")
													$spa_extraarticular_date="";
											
									?>
										  <tr id="patient_spa_extraarticular_tr_<?php echo $i;?>"> 
											   <td align="center">
													<input type="hidden" id="patient_spa_extraarticular_hidden_<?php echo $i;?>" name="patient_spa_extraarticular_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="patient_spa_extraarticular_id_<?php echo $i; ?>" id="patient_spa_extraarticular_id_<?php echo $i; ?>" value="<?php echo $patient_spa_extraarticular_id;?>">
													<select class="form-control" name="spa_extraarticulars_id_<?php echo $i; ?>" id="spa_extraarticulars_id_<?php echo $i; ?>">
														<option value="0">--</option>
														<?php
															$sql = get_lookup_tbl_id('spa_extraarticular');
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$spa_extraarticular_id2=$result2['id'];
																$value=$result2['value'];
														?>
															<option value="<?php echo $spa_extraarticular_id2; ?>" <?php if($spa_extraarticular_id==$spa_extraarticular_id2) { echo "selected"; } ?>><?php echo $value; ?></option>
														<?php
															}
																
														?>
														
														
													</select>
												</td>
												<td align="center">
													<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="spa_extraarticular_date_<?php echo $i; ?>" name="spa_extraarticular_date_<?php echo $i; ?>" value="<?php echo $spa_extraarticular_date; ?>">
												</td>
													<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="patient_spa_extraarticular_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
												  </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										
									?>
									</tbody>
								</table>
								<input type="hidden" name="patient_spa_extraarticular_numofrows" id="patient_spa_extraarticular_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								 <a class="btn btn-primary" href="javascript:void(0);" onclick="patient_spa_extraarticular_addrow();"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
					</div>
				</div><!-- Neuropsychiatric SLE div -->
			</div>
		</div>
		<?php
		}
		?>
		<?php
		if($show_characteristics4>0)
		{
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Neuropsychiatric SLE</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-8 col-xs-12" id="divneuropsychiatric" name="divneuropsychiatric">
								<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="patient_neuropsychiatric_tbl" name="patient_neuropsychiatric_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-4"><b>Neuropsychiatric SLE</b></th>
											<th class="col-md-4"><b>Date</b></th>
											<th class="col-md-2">&nbsp;</th>
											<th class="col-md-2">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
											$order = get_patient_neuropsychiatric($pat_id);
											$num_rows = pg_num_rows($order);
											
											while($result = pg_fetch_assoc($order))
											{
												$patient_neuropsychiatric_sle_id=$result['patient_neuropsychiatric_sle_id'];
												$patient_neuropsychiatric_sle=$result['patient_neuropsychiatric_sle'];
												$patient_neuropsychiatric_sle_date=$result['patient_neuropsychiatric_sle_date_str'];
												if($patient_neuropsychiatric_sle_date=="12-12-1900")
													$patient_neuropsychiatric_sle_date="";
											
									?>
										  <tr id="patient_neuropsychiatric_sle_tr_<?php echo $i;?>"> 
											   <td align="center">
													<input type="hidden" id="patient_neuropsychiatric_sle_hidden_<?php echo $i;?>" name="patient_neuropsychiatric_sle_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="patient_neuropsychiatric_sle_id_<?php echo $i; ?>" id="patient_neuropsychiatric_sle_id_<?php echo $i; ?>" value="<?php echo $patient_neuropsychiatric_sle_id;?>">
													<?php 
														if($patient_neuropsychiatric_sle==0)
															$patient_neuropsychiatric_sle_val="";
														else if($patient_neuropsychiatric_sle==1)
															$patient_neuropsychiatric_sle_val="Acute confusional state";
														else if($patient_neuropsychiatric_sle==2)
															$patient_neuropsychiatric_sle_val="Guillain-Barre syndrome";
														else if($patient_neuropsychiatric_sle==3)
															$patient_neuropsychiatric_sle_val="Anxiety disorder";
														else if($patient_neuropsychiatric_sle==4)
															$patient_neuropsychiatric_sle_val="Aseptic meningitis";
														else if($patient_neuropsychiatric_sle==5)
															$patient_neuropsychiatric_sle_val="Autoimmune inner disease";
														else if($patient_neuropsychiatric_sle==6)
															$patient_neuropsychiatric_sle_val="Autonomic neuropathy";
														else if($patient_neuropsychiatric_sle==7)
															$patient_neuropsychiatric_sle_val="Cerebrovascular disease / TIA";
														else if($patient_neuropsychiatric_sle==8)
															$patient_neuropsychiatric_sle_val="Cognitive dysfunction";
														else if($patient_neuropsychiatric_sle==9)
															$patient_neuropsychiatric_sle_val="Cranial neuropathy";
														else if($patient_neuropsychiatric_sle==10)
															$patient_neuropsychiatric_sle_val="Demyelination";
														else if($patient_neuropsychiatric_sle==11)
															$patient_neuropsychiatric_sle_val="Headache";
														else if($patient_neuropsychiatric_sle==12)
															$patient_neuropsychiatric_sle_val="Mononeuropathy";
														else if($patient_neuropsychiatric_sle==13)
															$patient_neuropsychiatric_sle_val="Mood disorder / Depression";
														else if($patient_neuropsychiatric_sle==14)
															$patient_neuropsychiatric_sle_val="Movement disorder / chorea";
														else if($patient_neuropsychiatric_sle==15)
															$patient_neuropsychiatric_sle_val="Myasthenia gravis";
														else if($patient_neuropsychiatric_sle==16)
															$patient_neuropsychiatric_sle_val="Myelopathy";
														else if($patient_neuropsychiatric_sle==17)
															$patient_neuropsychiatric_sle_val="Plexopathy";
														else if($patient_neuropsychiatric_sle==18)
															$patient_neuropsychiatric_sle_val="Polyneuropathy";
														else if($patient_neuropsychiatric_sle==19)
															$patient_neuropsychiatric_sle_val="Psychosis";
														else if($patient_neuropsychiatric_sle==20)
															$patient_neuropsychiatric_sle_val="Seizure disorder";
													?>
													<input type="text" readonly="true" class="form-control pull-right" name="patient_neuropsychiatric_sle_<?php echo $i; ?>" id="patient_neuropsychiatric_sle_<?php echo $i; ?>" value="<?php echo $patient_neuropsychiatric_sle_val;?>">
												</td>
												<td align="center">
													<input type="text" readonly="true" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="patient_neuropsychiatric_sle_date_<?php echo $i; ?>" name="patient_neuropsychiatric_sle_date_<?php echo $i; ?>" value="<?php echo $patient_neuropsychiatric_sle_date; ?>">
												</td>
												<td align=center>
													<a id="patient_neuropsychiatric_sle_edit_<?php echo $i; ?>" name="patient_neuropsychiatric_sle_edit_<?php echo $i; ?>" title="edit" onclick="javascript:popup2('neuropsychiatric.php?pat_id=<?php echo $pat_id;?>&patient_neuropsychiatric_sle_id=<?php echo $patient_neuropsychiatric_sle_id; ?>&row=<?php echo $i; ?>');" href="javascript:void();" target="_self" ><i class="fa fa-edit"></i><span></a>
												</td>
													<td align="center">
													<a id="patient_neuropsychiatric_sle_delete_<?php echo $i; ?>" name="patient_neuropsychiatric_sle_delete_<?php echo $i; ?>" title="Delete" href="javascript:void(0);" onclick="if (confirm('Are you sure to delete?')) { patient_neuropsychiatric_sle_removerow(<?php echo $i; ?>); }"><i class="fa fa-trash"></i><span></a>
												  </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										
									?>
									</tbody>
								</table>
								<input type="hidden" name="patient_neuropsychiatric_sle_numofrows" id="patient_neuropsychiatric_sle_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								<a class="btn btn-primary" onclick="javascript:popup2('neuropsychiatric.php?pat_id=<?php echo $pat_id;?>');" href="javascript:void();" target="_self"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
					</div>
				</div><!-- Neuropsychiatric SLE div -->
			</div>
		</div>
		<?php
		}
		?>
		<?php
		if($show_characteristics5>0)
		{
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Lupus Nephritis</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-8 col-xs-12" id="divnephritis" name="divnephritis">
								<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="patient_lupus_nefritis_tbl" name="patient_lupus_nefritis_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-4"><b>Biobsy diagnosis</b></th>
											<th class="col-md-4"><b>Date</b></th>
											<th class="col-md-2">&nbsp;</th>
											<th class="col-md-2">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
											$order = get_patient_lupus_nefritis($pat_id);
											$num_rows = pg_num_rows($order);
											
											while($result = pg_fetch_assoc($order))
											{
												$patient_lupus_nefritis_id=$result['patient_lupus_nefritis_id'];
												$patient_lupus_nefritis_biopsy_diagnosis=$result['patient_lupus_nefritis_biopsy_diagnosis'];
												$patient_lupus_nefritis_date=$result['patient_lupus_nefritis_date_str'];
												if($patient_lupus_nefritis_date=="12-12-1900")
													$patient_lupus_nefritis_date="";
											
									?>
										  <tr id="patient_lupus_nefritis_tr_<?php echo $i;?>"> 
											   <td align="center">
													<input type="hidden" id="patient_lupus_nefritis_hidden_<?php echo $i;?>" name="patient_lupus_nefritis_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="patient_lupus_nefritis_id_<?php echo $i; ?>" id="patient_lupus_nefritis_id_<?php echo $i; ?>" value="<?php echo $patient_lupus_nefritis_id;?>">
													<?php 
														if($patient_lupus_nefritis_biopsy_diagnosis==0)
															$patient_lupus_nefritis="";
														else if($patient_lupus_nefritis_biopsy_diagnosis==1)
															$patient_lupus_nefritis="Lupus Nephritis";
														else if($patient_lupus_nefritis_biopsy_diagnosis==2)
															$patient_lupus_nefritis="IgA nephropathy";
														else if($patient_lupus_nefritis_biopsy_diagnosis==3)
															$patient_lupus_nefritis="FSGS";
														else if($patient_lupus_nefritis_biopsy_diagnosis==4)
															$patient_lupus_nefritis="Membranoproliferative nephritis";
														else if($patient_lupus_nefritis_biopsy_diagnosis==5)
															$patient_lupus_nefritis="Other kidney disease";
													?>
													<input type="text" readonly="true" class="form-control pull-right" name="patient_lupus_nefritis_biopsy_diagnosis_<?php echo $i; ?>" id="patient_lupus_nefritis_biopsy_diagnosis_<?php echo $i; ?>" value="<?php echo $patient_lupus_nefritis;?>">
												</td>
												<td align="center">
													<input type="text" readonly="true" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="patient_lupus_nefritis_date_<?php echo $i; ?>" name="patient_lupus_nefritis_date_<?php echo $i; ?>" value="<?php echo $patient_lupus_nefritis_date; ?>">
												</td>
												<td align=center>
													<a id="patient_lupus_nefritis_edit_<?php echo $i; ?>" name="patient_lupus_nefritis_edit_<?php echo $i; ?>" title="edit" onclick="javascript:popup2('lupus.php?pat_id=<?php echo $pat_id;?>&patient_lupus_nefritis_id=<?php echo $patient_lupus_nefritis_id; ?>&row=<?php echo $i; ?>');" href="javascript:void();" target="_self" ><i class="fa fa-edit"></i><span></a>
												</td>
													<td align="center">
													<a id="patient_lupus_nefritis_delete_<?php echo $i; ?>" name="patient_lupus_nefritis_delete_<?php echo $i; ?>" title="Delete" href="javascript:void(0);" onclick="if (confirm('Are you sure to delete?')) { patient_lupus_nefritis_removerow(<?php echo $i; ?>); }"><i class="fa fa-trash"></i><span></a>
												  </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										
									?>
									</tbody>
								</table>
								<input type="hidden" name="patient_lupus_nefritis_numofrows" id="patient_lupus_nefritis_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								<a class="btn btn-primary" onclick="javascript:popup2('lupus.php?pat_id=<?php echo $pat_id;?>');" href="javascript:void();" target="_self"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
					</div>
				</div><!-- diagnoses status div -->
			</div>
		</div>
		<?php
		}
		?>
		<?php
		if($show_characteristics6>0)
		{
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Cutaneous Lupus</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-12 col-xs-12" id="divcutaneous" name="divcutaneous">
								<?php
										$sql = get_lookup_tbl_values('cle_subtype');
										$cle_subtype_counter=0;				
										while($result2 = pg_fetch_array($sql))
										{	
											$cle_subtype_values .= $result2['value'];
											$cle_subtype_values .= "!@#$%^" ;
											$cle_subtype_ids .=$result2['id'];
											$cle_subtype_ids .= "!@#$%^" ;
															
											$cle_subtype_counter++;
										}
									?>
										<input type="hidden" value="<?php echo $cle_subtype_counter; ?>" id="cle_subtype_counter" name="cle_subtype_counter"/>
										<input type="hidden" value="<?php echo $cle_subtype_values; ?>" id="cle_subtype_values" name="cle_subtype_values"/>
										<input type="hidden" value="<?php echo $cle_subtype_ids; ?>" id="cle_subtype_ids" name="cle_subtype_ids"/>
										
										<?php
										$sql = get_lookup_tbl_values('skin_biopsy');
										$skin_biopsy_counter=0;				
										while($result2 = pg_fetch_array($sql))
										{	
											$skin_biopsy_values .= $result2['value'];
											$skin_biopsy_values .= "!@#$%^" ;
											$skin_biopsy_ids .=$result2['id'];
											$skin_biopsy_ids .= "!@#$%^" ;
															
											$skin_biopsy_counter++;
										}
									?>
										<input type="hidden" value="<?php echo $skin_biopsy_counter; ?>" id="skin_biopsy_counter" name="skin_biopsy_counter"/>
										<input type="hidden" value="<?php echo $skin_biopsy_values; ?>" id="skin_biopsy_values" name="skin_biopsy_values"/>
										<input type="hidden" value="<?php echo $skin_biopsy_ids; ?>" id="skin_biopsy_ids" name="skin_biopsy_ids"/>
										
								  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="patient_cutaneous_tbl" name="patient_cutaneous_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-4"><b>CLE subtype</b></th>
											<th class="col-md-2"><b>Date</b></th>
											<th class="col-md-2"><b>Skin biopsy performed</b></th>
											<th class="col-md-3"><b>Skin biopsy</b></th>
											<th class="col-md-1">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
											$order = get_patient_cutaneous($pat_id);
											$num_rows = pg_num_rows($order);
											
											while($result = pg_fetch_assoc($order))
											{
												$patient_cutaneous_id=$result['patient_cutaneous_id'];
												$cle_subtype_id=$result['cle_subtype_id'];
												$performed=$result['performed'];
												$skin_biopsy=$result['skin_biopsy'];
												$patient_cutaneous_date=$result['patient_cutaneous_date_str'];
												if($patient_cutaneous_date=="12-12-1900")
													$patient_cutaneous_date="";
											
									?>
										  <tr id="patient_cutaneous_tr_<?php echo $i;?>"> 
											   <td align="center">
													<input type="hidden" id="patient_cutaneous_hidden_<?php echo $i;?>" name="patient_cutaneous_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="patient_cutaneous_id_<?php echo $i; ?>" id="patient_cutaneous_id_<?php echo $i; ?>" value="<?php echo $patient_cutaneous_id;?>">
													<select class="form-control" name="cle_subtype_id_<?php echo $i; ?>" id="cle_subtype_id_<?php echo $i; ?>">
														<option value="0">--</option>
														<?php
															$sql = get_lookup_tbl_values('cle_subtype');
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$cle_subtype_id2=$result2['id'];
																$value=$result2['value'];
														?>
															<option value="<?php echo $cle_subtype_id2; ?>" <?php if($cle_subtype_id==$cle_subtype_id2) { echo "selected"; } ?>><?php echo $value; ?></option>
														<?php
															}
																
														?>
														
														
													</select>
												</td>
												<td align="center">
													<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="patient_cutaneous_date_<?php echo $i; ?>" name="patient_cutaneous_date_<?php echo $i; ?>" value="<?php echo $patient_cutaneous_date; ?>">
												</td>
												<td align="center">
													<select class="form-control" name="performed_<?php echo $i; ?>" id="performed_<?php echo $i; ?>">
														<option value="0">--</option>
														<option value="1" <?php if($performed==1) { echo "selected"; } ?>>Yes</option>
														<option value="2" <?php if($performed==2) { echo "selected"; } ?>>No</option>
													</select>
												</td>
												<td align="center">
													<select class="form-control" name="skin_biopsy_<?php echo $i; ?>" id="skin_biopsy_<?php echo $i; ?>">
														<option value="0">--</option>
														<?php
															$sql = get_lookup_tbl_values('skin_biopsy');
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$skin_biopsy2=$result2['id'];
																$value=$result2['value'];
														?>
															<option value="<?php echo $skin_biopsy2; ?>" <?php if($skin_biopsy==$skin_biopsy2) { echo "selected"; } ?>><?php echo $value; ?></option>
														<?php
															}
																
														?>
													</select>
												</td>
													<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="if (confirm('Are you sure to delete?')) { patient_cutaneous_removerow(<?php echo $i; ?>); }"><i class="fa fa-trash"></i><span></a>
												  </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										
									?>
									</tbody>
								</table>
								<input type="hidden" name="patient_cutaneous_numofrows" id="patient_cutaneous_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								 <a class="btn btn-primary" href="javascript:void(0);" onclick="patient_cutaneous_addrow();"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
					</div>
				</div><!-- cutaneous lupus div -->
			</div>
		</div>
		<?php
		}
		?>
		<?php
		if($show_characteristics7>0)
		{
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Pulmonary involvement in autoimmune disease</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-12" id="divpulmonary" name="divpulmonary">
							<?php
										/*$sql = get_lookup_tbl_values("pulmonary_group");
										$pulmonary_group_counter=0;
										
										while($result2 = pg_fetch_array($sql))
										{	
											$pulmonary_group_values .= $result2['value'];
											$pulmonary_group_values .= "!@#$%^" ;
											
											$pulmonary_group_ids .=$result2['id'];
											$pulmonary_group_ids .= "!@#$%^" ;
															
											$pulmonary_group_counter++;
										}*/
									?>
										<!--<input type="hidden" value="<?php echo $pulmonary_group_counter; ?>" id="pulmonary_group_counter" name="pulmonary_group_counter"/>
										<input type="hidden" value="<?php echo $pulmonary_group_values; ?>" id="pulmonary_group_values" name="pulmonary_group_values"/>
										<input type="hidden" value="<?php echo $pulmonary_group_ids; ?>" id="pulmonary_group_ids" name="pulmonary_group_ids"/>-->
								  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="patient_pulmonary_tbl" name="patient_pulmonary_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-3"><b>Group</b></th>
											<th class="col-md-3"><b>Content/Values</b></th>
											<th class="col-md-2"><b>Specifications</b></th>
											<th class="col-md-2"><b>Date</b></th>
											<th class="col-md-1">&nbsp;</th>
											<th class="col-md-1">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
											$order = get_pulmonary($pat_id);
											$num_rows = pg_num_rows($order);
											
											while($result = pg_fetch_assoc($order))
											{
												$patient_pulmonary_id=$result['patient_pulmonary_id'];
												$group=$result['group_id'];
												$content=$result['content'];
												$specifications=$result['specifications'];
												$pulmonary_date=$result['pulmonary_date'];
												if($pulmonary_date=="12-12-1900")
													$pulmonary_date="";
												/*$fvc=$result['fvc'];
												$fev1=$result['fev1'];
												$tlcosb=$result['tlcosb'];
												$tlcova=$result['tlcova'];
												$tlc=$result['tlc'];*/
												
									?>
										  <tr id="patient_pulmonary_tr_<?php echo $i;?>">
												<td align="center">
													<input type="hidden" id="patient_pulmonary_hidden_<?php echo $i;?>" name="patient_pulmonary_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="patient_pulmonary_id_<?php echo $i; ?>" id="patient_pulmonary_id_<?php echo $i; ?>" value="<?php echo $patient_pulmonary_id;?>">
													<!--<select onchange="setpulmonarycontent(<?php echo $i; ?>)" class="form-control" name="group_<?php echo $i; ?>" id="group_<?php echo $i; ?>">
														<option value="0">--</option>-->
														<?php
															$sql = get_lookup_tbl_values("pulmonary_group");
															
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$id=$result2['id'];
																$value=$result2['value'];
																
																
																if($group==$id) 
																	$group_val=$value;
															}
														?>
														<input type="text" readonly="true" class="form-control pull-right" name="group_<?php echo $i; ?>" id="group_<?php echo $i; ?>" value="<?php echo $group_val; ?>">
													<!--</select>-->
												</td>
											   <td align="center">
													<!--<select onchange="setpulmonaryspecifications(<?php echo $i; ?>)" class="form-control" name="content_<?php echo $i; ?>" id="content_<?php echo $i; ?>">
														<option value="0">--</option>-->
														<?php
															$sql = get_lookup_val($group,'pulmonary_values');
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$id=$result2['id'];
																$value=$result2['value'];
																if($content==$id) 
																	$content_val=$value;
															}
														?>
														<input type="text" readonly="true" class="form-control pull-right" name="content_<?php echo $i; ?>" id="content_<?php echo $i; ?>" value="<?php echo $content_val; ?>">
													<!--</select>-->
												</td>
												 <td align="center">
													<!--<select class="form-control" name="specifications_<?php echo $i; ?>" id="specifications_<?php echo $i; ?>">
														<option value="0">--</option>-->
														<?php
															$sql = get_lookup_val($content,'pulmonary_specifications');
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$id=$result2['id'];
																$value=$result2['value'];
																if($specifications==$id) 
																	$specifications_val=$value;
															}
														?>
														<input type="text" readonly="true" class="form-control pull-right" name="specifications_<?php echo $i; ?>" id="specifications_<?php echo $i; ?>" value="<?php echo $specifications_val; ?>">
													<!--</select>-->
												</td>
												<td align="center">
													<input type="text"  readonly="true" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="pulmonary_date_<?php echo $i; ?>" name="pulmonary_date_<?php echo $i; ?>" value="<?php echo $pulmonary_date; ?>">
												</td>
												<td align=center>
													<a id="patient_pulmonary_edit_<?php echo $i; ?>" name="patient_pulmonary_edit_<?php echo $i; ?>" title="edit" onclick="javascript:popup2('pulmonary.php?pat_id=<?php echo $pat_id;?>&patient_pulmonary_id=<?php echo $patient_pulmonary_id; ?>&row=<?php echo $i; ?>');" href="javascript:void();" target="_self" ><i class="fa fa-edit"></i><span></a>
												</td>
												<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="patient_pulmonary_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
												 </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										
									?>
									</tbody>
								</table>
								<input type="hidden" name="patient_pulmonary_numofrows" id="patient_pulmonary_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								 <!--<a class="btn btn-primary" href="javascript:void(0);" onclick="patient_pulmonary_addrow();"><i class="fa fa-plus"></i> New record</a>-->
								 <a class="btn btn-primary" onclick="javascript:popup2('pulmonary.php?pat_id=<?php echo $pat_id;?>');" href="javascript:void();" target="_self"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
					</div>
				</div><!-- pulmonary status div -->
			</div>
		</div>
		<?php
		}
		?>
		<?php
		if($show_characteristics1>0 or $show_characteristics2>0 or $show_characteristics3>0 or $show_characteristics4>0 or $show_characteristics5>0 or $show_characteristics6>0 or $show_characteristics7>0)
		{
		?>
		<div class="row">
			<div class="form-group col-md-12">
				<input type="submit" class="btn btn-primary" value="Save">
			</div>
		</div>
		<?php
		}
		else
		{
		?>
		<div class="row">
			<div class="form-group col-md-12">
				<label>There isn't any "other disease charastestic" for this diagnoses</label>
			</div>
		</div>
		<?php
		}
		?>
		<div class="row">
			<div class="form-group col-md-12">
				&nbsp;
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				&nbsp;
			</div>
		</div>
		</form>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
   <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>

function settooltip(id){
	var e = document.getElementById('antibodies_id_'+id);
	var strUser = e.options[e.selectedIndex].value;
	
	var autoantibodies_tooltip_ids=document.getElementById('autoantibodies_tooltip_ids').value;
	var autoantibodies_tooltip_values=document.getElementById('autoantibodies_tooltip_values').value;
	var autoantibodies_tooltip_counter=document.getElementById('autoantibodies_tooltip_counter').value;
	var autoantibodies_tooltip_value=autoantibodies_tooltip_values.split('!@#$%^');
	var autoantibodies_tooltip_id=autoantibodies_tooltip_ids.split('!@#$%^');
	
	for(i = 0; i < autoantibodies_tooltip_counter; i++)
	{
		
		if(autoantibodies_tooltip_id[i]===strUser)
		{
			//document.getElementById('tooltip_'+id).setAttribute('title', autoantibodies_tooltip_value[i]);
			document.getElementById('patient_antibodies_options_'+id).setAttribute('title', autoantibodies_tooltip_value[i]);
		}
	}
	
	

}
  
$(document).ready(function() {


for(i=1;i<=document.getElementById('patient_cutaneous_numofrows').value;i++)
{
	$('#patient_cutaneous_date_'+i).datepicker({
	  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		onClose2:function(dateText) {}
	})
	$('#patient_cutaneous_date_'+i).inputmask();
}

for(i=1;i<=document.getElementById('patient_ra_extraarticular_numofrows').value;i++)
{
	$('#ra_extraarticular_date_'+i).datepicker({
	  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		onClose2:function(dateText) {}
	})
	$('#ra_extraarticular_date_'+i).inputmask();
}

for(i=1;i<=document.getElementById('patient_antibodies_numofrows').value;i++)
{
	$('#patient_antibodies_date_'+i).datepicker({
	 dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		onClose2:function(dateText) {}
	})
	$('#patient_antibodies_date_'+i).inputmask();
}


/*for(i=1;i<=document.getElementById('patient_pulmonary_numofrows').value;i++)
{
	$('#pulmonary_date_'+i).datepicker({
	  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		onClose2:function(dateText) {}
	})
	$('#pulmonary_date_'+i).inputmask();
}*/

for(i=1;i<=document.getElementById('patient_spa_extraarticular_numofrows').value;i++)
{
	$('#spa_extraarticular_date_'+i).datepicker({
	  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		onClose2:function(dateText) {}
	})
	$('#spa_extraarticular_date_'+i).inputmask();
}
});

$(document).ajaxStart(function(){
		$('body').loading();
		
    });
    $(document).ajaxComplete(function(){
       $('body').loading('stop');
    });

function patient_neuropsychiatric_sle_addrow(pat_id,patient_neuropsychiatric_sle_id)
{
  var tbl = document.getElementById('patient_neuropsychiatric_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'patient_neuropsychiatric_sle_tr_'+iteration;
  document.getElementById('patient_neuropsychiatric_sle_numofrows').value = iteration;
 		
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('input');
  el1.name = 'patient_neuropsychiatric_sle_' + iteration;
  el1.id = 'patient_neuropsychiatric_sle_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
  el1.readOnly='true';
  
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'patient_neuropsychiatric_sle_hidden_' + iteration;
  el2.id = 'patient_neuropsychiatric_sle_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'patient_neuropsychiatric_sle_id_' + iteration;
  el3.id = 'patient_neuropsychiatric_sle_id_' + iteration;
  el3.value='';
  
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
  var r2 = row.insertCell(1);
  r2.align='center';
  var el4 = document.createElement('input');
  el4.name = 'patient_neuropsychiatric_sle_date_' + iteration;
  el4.id = 'patient_neuropsychiatric_sle_date_' + iteration;
  el4.style.width='100%';
  el4.className='form-control';
  el4.readOnly='true';
	
  r2.appendChild(el4);
  
  var r5 = row.insertCell(2);
  r5.align='center';
  
  var el7 = document.createElement('a');
  el7.name = 'patient_neuropsychiatric_sle_edit_' + iteration;
  el7.id = 'patient_neuropsychiatric_sle_edit_' + iteration;
  el7.title = 'Edit';
  el7.href="javascript:avoid(0)";
  var icon = document.createElement('i');
  icon.className='fa fa-edit';
  el7.appendChild(icon);
  el7.onclick =function(){popup2('neuropsychiatric.php?pat_id='+pat_id+'&patient_neuropsychiatric_sle_id='+patient_neuropsychiatric_sle_id+'&row='+iteration)};
  
  r5.appendChild(el7);
  
  var r6 = row.insertCell(3);
  r6.align='center';
  
  var el8 = document.createElement('a');
  el8.name = 'patient_neuropsychiatric_sle_delete_' + iteration;
  el8.id = 'patient_neuropsychiatric_sle_delete_' + iteration;
  el8.title = 'Delete';
  el8.href="javascript:avoid(0)";
  var icon = document.createElement('i');
  icon.className='fa fa-trash-o';
  el8.appendChild(icon);
  el8.onclick =function(){if (confirm("Are you sure to delete!")) { patient_neuropsychiatric_sle_removerow(iteration); }};
		 
  r6.appendChild(el8);
}

function patient_neuropsychiatric_sle_removerow(row)
{
	document.getElementById('patient_neuropsychiatric_sle_hidden_'+row).value='-1';
	document.getElementById('patient_neuropsychiatric_sle_tr_'+row).style.display='none';
}

	
function patient_pulmonary_addrow()
{
  var tbl = document.getElementById('patient_pulmonary_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'patient_pulmonary_tr_'+iteration;
  document.getElementById('patient_pulmonary_numofrows').value = iteration;
								 
								 
	/*var pulmonary_group_ids=document.getElementById('pulmonary_group_ids').value;
	var pulmonary_group_values=document.getElementById('pulmonary_group_values').value;
	var pulmonary_group_counter=document.getElementById('pulmonary_group_counter').value;
	var pulmonary_group_value=pulmonary_group_values.split('!@#$%^');
	var pulmonary_group_id=pulmonary_group_ids.split('!@#$%^');*/
	
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('input');
  el1.name = 'group_' + iteration;
  el1.id = 'group_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
  el1.readOnly='true';
  
	/*var opt=new Option("--",0);
	el1.add(opt,undefined);
	for(i = 0; i < pulmonary_group_counter; i++)
	{
		var opt=new Option(pulmonary_group_value[i],pulmonary_group_id[i]);
		el1.add(opt,undefined);
	}*/
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'patient_pulmonary_hidden_' + iteration;
  el2.id = 'patient_pulmonary_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'patient_pulmonary_id_' + iteration;
  el3.id = 'patient_pulmonary_id_' + iteration;
  el3.value='';
  
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
	var r2 = row.insertCell(1);
	r2.align='center';
	
	var el4 = document.createElement('input');
	el4.name = 'content_' + iteration;
	el4.id = 'content_' + iteration;
	//el4.onchange =function(){setpulmonaryspecifications(iteration)};
	el4.style.width='100%';
	el4.className='form-control';
	el4.readOnly='true';
	
	
  
	//var opt=new Option("--",0);
	//el4.add(opt,undefined);
	r2.appendChild(el4);
  
  var r3 = row.insertCell(2);
	r3.align='center';
	
	var el5 = document.createElement('input');
	el5.name = 'specifications_' + iteration;
	el5.id = 'specifications_' + iteration;
	el5.style.width='100%';
	el5.className='form-control';
	el5.readOnly='true';
	
	//var opt=new Option("--",0);
	//el5.add(opt,undefined);
	r3.appendChild(el5);
	
	var r4 = row.insertCell(3);
	r4.align='center';
	var el6 = document.createElement('input');
	el6.type = 'text';
	el6.name = 'pulmonary_date_' + iteration;
	el6.id = 'pulmonary_date_' + iteration;
	el6.className="form-control";
	el6.style.width='100%';
    el6.readOnly='true';
	
	r4.appendChild(el6);
  
	var r5 = row.insertCell(4);
	  r5.align='center';
	  
	  var el7 = document.createElement('a');
	  el7.name = 'patient_pulmonary_edit_' + iteration;
	  el7.id = 'patient_pulmonary_edit_' + iteration;
	  el7.title = 'Edit';
	  el7.href="javascript:avoid(0)";
	  var icon = document.createElement('i');
	  icon.className='fa fa-edit';
	  el7.appendChild(icon);
	  el7.onclick =function(){popup2('pulmonary.php?pat_id='+pat_id+'&patient_pulmonary_id='+patient_pulmonary_id+'&row='+iteration)};
	  
	  r5.appendChild(el7);
  
	var r6 = row.insertCell(5);
	r6.align='center';
	
	var el8 = document.createElement('a');
	el8.name = 'a_' + iteration;
	el8.id = 'a_' + iteration;
	el8.title = 'Delete';
	el8.href="javascript:avoid(0)";
	var icon = document.createElement('i');
	icon.className='fa fa-trash-o';
	el8.appendChild(icon);
	el8.onclick =function(){patient_pulmonary_removerow(iteration)};
	
	 r6.appendChild(el8);
	
	/*$('#pulmonary_date_'+ iteration).datepicker({
		 dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		onClose2:function(dateText) {}
  })
  $('#pulmonary_date_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });*/
}

function patient_pulmonary_removerow(row)
{
	document.getElementById('patient_pulmonary_hidden_'+row).value='-1';
	document.getElementById('patient_pulmonary_tr_'+row).style.display='none';
}

function patient_lupus_nefritis_addrow(pat_id,patient_lupus_nefritis_id)
{
  var tbl = document.getElementById('patient_lupus_nefritis_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'patient_lupus_nefritis_tr_'+iteration;
  document.getElementById('patient_lupus_nefritis_numofrows').value = iteration;
 		
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('input');
  el1.name = 'patient_lupus_nefritis_biopsy_diagnosis_' + iteration;
  el1.id = 'patient_lupus_nefritis_biopsy_diagnosis_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
  el1.readOnly='true';
  
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'patient_lupus_nefritis_hidden_' + iteration;
  el2.id = 'patient_lupus_nefritis_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'patient_lupus_nefritis_id_' + iteration;
  el3.id = 'patient_lupus_nefritis_id_' + iteration;
  el3.value='';
  
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
  var r2 = row.insertCell(1);
  r2.align='center';
  var el4 = document.createElement('input');
  el4.name = 'patient_lupus_nefritis_date_' + iteration;
  el4.id = 'patient_lupus_nefritis_date_' + iteration;
  el4.style.width='100%';
  el4.className='form-control';
  el4.readOnly='true';
	
  r2.appendChild(el4);
  
  var r5 = row.insertCell(2);
  r5.align='center';
  
  var el7 = document.createElement('a');
  el7.name = 'patient_lupus_nefritis_edit_' + iteration;
  el7.id = 'patient_lupus_nefritis_edit_' + iteration;
  el7.title = 'Edit';
  el7.href="javascript:avoid(0)";
  var icon = document.createElement('i');
  icon.className='fa fa-edit';
  el7.appendChild(icon);
  el7.onclick =function(){popup2('lupus.php?pat_id='+pat_id+'&patient_lupus_nefritis_id='+patient_lupus_nefritis_id+'&row='+iteration)};
  
  r5.appendChild(el7);
  
  var r6 = row.insertCell(3);
  r6.align='center';
  
  var el8 = document.createElement('a');
  el8.name = 'patient_lupus_nefritis_delete_' + iteration;
  el8.id = 'patient_lupus_nefritis_delete_' + iteration;
  el8.title = 'Delete';
  el8.href="javascript:avoid(0)";
  var icon = document.createElement('i');
  icon.className='fa fa-trash-o';
  el8.appendChild(icon);
  el8.onclick =function(){if (confirm("Are you sure to delete!")) { patient_lupus_nefritis_removerow(iteration); }};
		 
  r6.appendChild(el8);
}

function patient_lupus_nefritis_removerow(row)
{
	document.getElementById('patient_lupus_nefritis_hidden_'+row).value='-1';
	document.getElementById('patient_lupus_nefritis_tr_'+row).style.display='none';
}


function patient_cutaneous_addrow()
{
  var tbl = document.getElementById('patient_cutaneous_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'patient_cutaneous_tr_'+iteration;
  document.getElementById('patient_cutaneous_numofrows').value = iteration;
 										
	var skin_biopsy_ids=document.getElementById('skin_biopsy_ids').value;
	var skin_biopsy_values=document.getElementById('skin_biopsy_values').value;
	var skin_biopsy_counter=document.getElementById('skin_biopsy_counter').value;
	var skin_biopsy_value=skin_biopsy_values.split('!@#$%^');
	var skin_biopsy_id=skin_biopsy_ids.split('!@#$%^');
	
	var cle_subtype_ids=document.getElementById('cle_subtype_ids').value;
	var cle_subtype_values=document.getElementById('cle_subtype_values').value;
	var cle_subtype_counter=document.getElementById('cle_subtype_counter').value;
	var cle_subtype_value=cle_subtype_values.split('!@#$%^');
	var cle_subtype_id=cle_subtype_ids.split('!@#$%^');
	
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('select');
  el1.name = 'cle_subtype_id_' + iteration;
  el1.id = 'cle_subtype_id_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
  
	var opt=new Option("--",0);
	el1.add(opt,undefined);
	for(i = 0; i < cle_subtype_counter; i++)
	{
		var opt=new Option(cle_subtype_value[i],cle_subtype_id[i]);
		el1.add(opt,undefined);
	}
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'patient_cutaneous_hidden_' + iteration;
  el2.id = 'patient_cutaneous_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'patient_cutaneous_id_' + iteration;
  el3.id = 'patient_cutaneous_id_' + iteration;
  el3.value='';
  
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
  var r4 = row.insertCell(1);
  r4.align='center';
  
  var el6 = document.createElement('input');
  el6.type = 'text';
  el6.name = 'patient_cutaneous_date_' + iteration;
  el6.id = 'patient_cutaneous_date_' + iteration;
  el6.className="form-control";
  el6.style.width='100%';
  
  r4.appendChild(el6);
  
  var r2 = row.insertCell(2);
  r2.align='center';
  var el4 = document.createElement('select');
  el4.name = 'performed_' + iteration;
  el4.id = 'performed_' + iteration;
  el4.style.width='100%';
  el4.className='form-control';
  
	var opt=new Option("--",0);
	el4.add(opt,undefined);
	var opt=new Option("Yes",1);
	el4.add(opt,undefined);
	var opt=new Option("No",2);
	el4.add(opt,undefined);
	
  r2.appendChild(el4);
  
  var r3 = row.insertCell(3);
  r3.align='center';
  
  var el5 = document.createElement('select');
  el5.name = 'skin_biopsy_' + iteration;
  el5.id = 'skin_biopsy_' + iteration;
  el5.style.width='100%';
  el5.className='form-control';
  
	var opt=new Option("--",0);
	el5.add(opt,undefined);
	for(i = 0; i < skin_biopsy_counter; i++)
	{
		var opt=new Option(skin_biopsy_value[i],skin_biopsy_id[i]);
		el5.add(opt,undefined);
	}
   
  
  r3.appendChild(el5);
  
  var r5 = row.insertCell(4);
  r5.align='center';
  
  var el7 = document.createElement('a');
  el7.name = 'a_' + iteration;
  el7.id = 'a_' + iteration;
  el7.title = 'Delete';
  el7.href="javascript:avoid(0)";
  var icon = document.createElement('i');
  icon.className='fa fa-trash-o';
  el7.appendChild(icon);
  el7.onclick =function(){if (confirm("Are you sure to delete!")) { patient_cutaneous_removerow(iteration); }};
		 
  r5.appendChild(el7);
  
  $('#patient_cutaneous_date_'+ iteration).datepicker({
		  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		onClose2:function(dateText) {}
  })
  $('#patient_cutaneous_date_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
}

function patient_cutaneous_removerow(row)
{
	document.getElementById('patient_cutaneous_hidden_'+row).value='-1';
	document.getElementById('patient_cutaneous_tr_'+row).style.display='none';
}


function setpulmonarycontent(id)
{
        var group_id = $("#group_"+id).val();
		
        $.ajax({
            url: 'populate_content.php',
            type: 'post',
            data: {
				id:group_id
				},
            dataType: 'json',
            success:function(response){
			
                var len = response.length;
				
                $("#content_"+id).empty();
				$("#content_"+id).append("<option value='0'>--</option>");
				$("#specifications_"+id).empty();
				$("#specifications_"+id).append("<option value='0'>--</option>");
				
                for( var i = 0; i<len; i++){
                    var id2 = response[i]['id'];
					var value = response[i]['value'];
					
                    $("#content_"+id).append("<option value='"+id2+"'>"+value+"</option>");
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }

function setpulmonaryspecifications(id)
{
        var content_id = $("#content_"+id).val();
		
        $.ajax({
            url: 'populate_specifications.php',
            type: 'post',
            data: {
				id:content_id
				},
            dataType: 'json',
            success:function(response){
			
                var len = response.length;
				
                $("#specifications_"+id).empty();
				$("#specifications_"+id).append("<option value='0'>--</option>");
				
                for( var i = 0; i<len; i++){
                    var id2 = response[i]['id'];
					var value = response[i]['value'];
					
                    $("#specifications_"+id).append("<option value='"+id2+"'>"+value+"</option>");
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }

function patient_antibodies_addrow()
{
  var tbl = document.getElementById('patient_antibodies_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'patient_antibodies_tr_'+iteration;
  document.getElementById('patient_antibodies_numofrows').value = iteration;
  
	var autoantibodies_ids=document.getElementById('autoantibodies_ids').value;
	var autoantibodies_values=document.getElementById('autoantibodies_values').value;
	var autoantibodies_counter=document.getElementById('autoantibodies_counter').value;
	var autoantibodies_value=autoantibodies_values.split('!@#$%^');
	var autoantibodies_id=autoantibodies_ids.split('!@#$%^');
	
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('select');
  el1.name = 'antibodies_id_' + iteration;
  el1.id = 'antibodies_id_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
  el1.onchange =function(){ settooltip(iteration)};
  
	var opt=new Option("--",0);
	el1.add(opt,undefined);
	for(i = 0; i < autoantibodies_counter; i++)
	{
		var opt=new Option(autoantibodies_value[i],autoantibodies_id[i]);
		el1.add(opt,undefined);
	}
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'patient_antibodies_hidden_' + iteration;
  el2.id = 'patient_antibodies_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'patient_autoantibodies_id_' + iteration;
  el3.id = 'patient_autoantibodies_id_' + iteration;
  el3.value='';
  
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
  var r2 = row.insertCell(1);
  r2.align='center';
  var el4 = document.createElement('select');
  el4.name = 'patient_antibodies_options_' + iteration;
  el4.id = 'patient_antibodies_options_' + iteration;
  el4.style.width='100%';
  el4.className='form-control';
  
	var opt=new Option("--",0);
	el4.add(opt,undefined);
	var opt=new Option("Unknown",1);
	el4.add(opt,undefined);
	var opt=new Option("Negative",2);
	el4.add(opt,undefined);
	var opt=new Option("Low positive",3);
	el4.add(opt,undefined);
	var opt=new Option("Medium positive",4);
	el4.add(opt,undefined);
	var opt=new Option("High positive",5);
	el4.add(opt,undefined);
	
  //var icon = document.createElement('i');
  //icon.name = 'tooltip_' + iteration;
  //icon.id = 'tooltip_' + iteration;
  //icon.className='fa fa-question-circle 2x';
  
  r2.appendChild(el4);
  //r2.appendChild(icon);
  
  var r3 = row.insertCell(2);
  r3.align='center';
  
  var el5 = document.createElement('input');
  el5.type = 'text';
  el5.name = 'patient_antibodies_titre_' + iteration;
  el5.id = 'patient_antibodies_titre_' + iteration;
  el5.className="form-control";
  el5.style.width='100%';
  
  r3.appendChild(el5);
  
  var r4 = row.insertCell(3);
  r4.align='center';
  
  var el6 = document.createElement('input');
  el6.type = 'text';
  el6.name = 'patient_antibodies_date_' + iteration;
  el6.id = 'patient_antibodies_date_' + iteration;
  el6.className="form-control";
  el6.style.width='100%';
  
  r4.appendChild(el6);
  
  var r5 = row.insertCell(4);
  r5.align='center';
  
  var el7 = document.createElement('a');
  el7.name = 'a_' + iteration;
  el7.id = 'a_' + iteration;
  el7.title = 'Delete';
  el7.href="javascript:avoid(0)";
  var icon = document.createElement('i');
  icon.className='fa fa-trash-o';
  el7.appendChild(icon);
  el7.onclick =function(){patient_antibodies_removerow(iteration)};
		 
  r5.appendChild(el7);
  
  $('#patient_antibodies_date_'+ iteration).datepicker({
		  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		onClose2:function(dateText) {}
  })
  $('#patient_antibodies_date_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
}

function patient_antibodies_removerow(row)
{
	document.getElementById('patient_antibodies_hidden_'+row).value='-1';
	document.getElementById('patient_antibodies_tr_'+row).style.display='none';
}

function patient_spa_extraarticular_addrow()
{
  var tbl = document.getElementById('patient_spa_extraarticular_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'patient_spa_extraarticular_tr_'+iteration;
  document.getElementById('patient_spa_extraarticular_numofrows').value = iteration;
  
	var spa_extraarticular_ids=document.getElementById('spa_extraarticular_ids').value;
	var spa_extraarticular_values=document.getElementById('spa_extraarticular_values').value;
	var spa_extraarticular_counter=document.getElementById('spa_extraarticular_counter').value;
	var spa_extraarticular_value=spa_extraarticular_values.split('!@#$%^');
	var spa_extraarticular_id=spa_extraarticular_ids.split('!@#$%^');
	
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('select');
  el1.name = 'spa_extraarticulars_id_' + iteration;
  el1.id = 'spa_extraarticulars_id_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
  
	var opt=new Option("--",0);
	el1.add(opt,undefined);
	for(i = 0; i < spa_extraarticular_counter; i++)
	{
		var opt=new Option(spa_extraarticular_value[i],spa_extraarticular_id[i]);
		el1.add(opt,undefined);
	}
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'patient_spa_extraarticular_hidden_' + iteration;
  el2.id = 'patient_spa_extraarticular_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'patient_spa_extraarticular_id_' + iteration;
  el3.id = 'patient_spa_extraarticular_id_' + iteration;
  el3.value='';
  
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
  var r3 = row.insertCell(1);
  r3.align='center';
  var el5 = document.createElement('input');
  el5.type = 'text';
  el5.name = 'spa_extraarticular_date_' + iteration;
  el5.id = 'spa_extraarticular_date_' + iteration;
  el5.className="form-control";
  el5.style.width='100%';
  
  r3.appendChild(el5);
  
  var r5 = row.insertCell(2);
  r5.align='center';
  
  var el7 = document.createElement('a');
  el7.name = 'a_' + iteration;
  el7.id = 'a_' + iteration;
  el7.title = 'Delete';
  el7.href="javascript:avoid(0)";
  var icon = document.createElement('i');
  icon.className='fa fa-trash-o';
  el7.appendChild(icon);
  el7.onclick =function(){patient_spa_extraarticular_removerow(iteration)};
		 
  r5.appendChild(el7);
  
  $('#spa_extraarticular_date_'+ iteration).datepicker({
		 dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		onClose2:function(dateText) {}
  })
  $('#spa_extraarticular_date_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
}

function patient_spa_extraarticular_removerow(row)
{
	document.getElementById('patient_spa_extraarticular_hidden_'+row).value='-1';
	document.getElementById('patient_spa_extraarticular_tr_'+row).style.display='none';
}

function patient_ra_extraarticular_addrow()
{
  var tbl = document.getElementById('patient_ra_extraarticular_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'patient_ra_extraarticular_tr_'+iteration;
  document.getElementById('patient_ra_extraarticular_numofrows').value = iteration;
  
	var ra_extraarticular_ids=document.getElementById('ra_extraarticular_ids').value;
	var ra_extraarticular_values=document.getElementById('ra_extraarticular_values').value;
	var ra_extraarticular_counter=document.getElementById('ra_extraarticular_counter').value;
	var ra_extraarticular_value=ra_extraarticular_values.split('!@#$%^');
	var ra_extraarticular_id=ra_extraarticular_ids.split('!@#$%^');
	
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('select');
  el1.name = 'ra_extraarticulars_id_' + iteration;
  el1.id = 'ra_extraarticulars_id_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
  
	var opt=new Option("--",0);
	el1.add(opt,undefined);
	for(i = 0; i < ra_extraarticular_counter; i++)
	{
		var opt=new Option(ra_extraarticular_value[i],ra_extraarticular_id[i]);
		el1.add(opt,undefined);
	}
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'patient_ra_extraarticular_hidden_' + iteration;
  el2.id = 'patient_ra_extraarticular_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'patient_ra_extraarticular_id_' + iteration;
  el3.id = 'patient_ra_extraarticular_id_' + iteration;
  el3.value='';
  
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
  var r3 = row.insertCell(1);
  r3.align='center';
  var el5 = document.createElement('input');
  el5.type = 'text';
  el5.name = 'ra_extraarticular_date_' + iteration;
  el5.id = 'ra_extraarticular_date_' + iteration;
  el5.className="form-control";
  el5.style.width='100%';
  r3.appendChild(el5);
  
  var r5 = row.insertCell(2);
  r5.align='center';
  
  var el7 = document.createElement('a');
  el7.name = 'a_' + iteration;
  el7.id = 'a_' + iteration;
  el7.title = 'Delete';
  el7.href="javascript:avoid(0)";
  var icon = document.createElement('i');
  icon.className='fa fa-trash-o';
  el7.appendChild(icon);
  el7.onclick =function(){patient_ra_extraarticular_removerow(iteration)};
		 
  r5.appendChild(el7);
  
   $('#ra_extraarticular_date_'+ iteration).datepicker({
		 dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		onClose2:function(dateText) {}
  })
  $('#ra_extraarticular_date_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
  
}

function patient_ra_extraarticular_removerow(row)
{
	document.getElementById('patient_ra_extraarticular_hidden_'+row).value='-1';
	document.getElementById('patient_ra_extraarticular_tr_'+row).style.display='none';
}

</script>
<?php
if($save==1)
{
	if($save_chk=="ok")
	{
?>
<script>
$(".alert_suc").show();
window.setTimeout(function() {
    $(".alert_suc").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
	else
	{
?>
<script>
$(".alert_wr").show();
window.setTimeout(function() {
    $(".alert_wr").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
}
?>
</body>
</html>
