		
<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	$save=$_REQUEST['save'];
	$pat_id=$_REQUEST['pat_id'];
	if($pat_id<>"")
		$_SESSION['pat_id']=$pat_id;
	
	$pat_id=$_SESSION['pat_id'];
	
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<?php
if($save==1)
{
	pg_query("BEGIN") or die("Could not start transaction\n");
	try
	{
		//PREVIOUS ANTI-RHEUMATIC DRUG TREATMENT  table-start
		
		$j=1;
		$patient_previous_drugs_numofrows=$_REQUEST['patient_previous_drugs_numofrows'];
		for ($i=1;$i<=$patient_previous_drugs_numofrows;$i++)
		{
			$patient_previous_drugs_hidden=$_REQUEST['patient_previous_drugs_hidden_'.$i];
			$patient_previous_drugs_id=$_REQUEST['patient_previous_drugs_id_'.$i];
			if($patient_previous_drugs_hidden=='-1')
			{
				if($patient_previous_drugs_id<>"")
				{
					$patient_previous_drugs_hdn_parameters[0]=1;
					$patient_previous_drugs_hdn_names[0]='deleted';
					$patient_previous_drugs_hdn_edit_name[0]='patient_previous_drugs_id';
					$patient_previous_drugs_hdn_edit_id[0]=$patient_previous_drugs_id;
					$patient_previous_drugs_hdn_sumbol[0]='=';
					$patient_previous_drugs_msg=update('patient_previous_drugs',$patient_previous_drugs_hdn_names,$patient_previous_drugs_hdn_parameters,$patient_previous_drugs_hdn_edit_name,$patient_previous_drugs_hdn_edit_id,$patient_previous_drugs_hdn_sumbol);
					if ($patient_previous_drugs_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
			{
				$patient_previous_drugs_parameters[0]=$_REQUEST['reason_of_discontinuation_id_'.$i];
				$patient_previous_drugs_parameters[1]=$_REQUEST['drugs_id_'.$i];
				$patient_previous_drugs_parameters[2]=date_for_postgres($_REQUEST['startdate_'.$i]);
				$patient_previous_drugs_parameters[3]=$_REQUEST['treatment_stop_'.$i];
				$patient_previous_drugs_parameters[4]=date_for_postgres($_REQUEST['enddate_'.$i]);
				$patient_previous_drugs_parameters[5]=$user_id;
				$patient_previous_drugs_parameters[6]='now()';
				//$patient_previous_drugs_parameters[7]=$_REQUEST['route_of_administration_'.$i];;
				$patient_previous_drugs_table_names[0]='reason_of_discontinuation_id';
				$patient_previous_drugs_table_names[1]='drugs_id';
				$patient_previous_drugs_table_names[2]='start_date';
				$patient_previous_drugs_table_names[3]='treatment_stop';
				$patient_previous_drugs_table_names[4]='end_date';
				$patient_previous_drugs_table_names[5]='editor_id';
				$patient_previous_drugs_table_names[6]='edit_date';
				//$patient_previous_drugs_table_names[7]='route_of_administration';
				$patient_previous_drugs_edit_name[0]='patient_previous_drugs_id';
				$patient_previous_drugs_edit_id[0]=$patient_previous_drugs_id;
				$patient_previous_drugs_sumbol[0]='=';
				
				if($patient_previous_drugs_id=='')
				{
					
					$patient_previous_drugs_parameters[7]=$pat_id;
					$patient_previous_drugs_parameters[8]=0;
					
					$patient_previous_drugs_table_names[7]='pat_id';
					$patient_previous_drugs_table_names[8]='deleted';
					
					$id=insert('patient_previous_drugs',$patient_previous_drugs_table_names,$patient_previous_drugs_parameters,'patient_previous_drugs_id');
					if($id!='')
						$j++;
				}
				else
				{
					$patient_previous_drugs_msg=update('patient_previous_drugs',$patient_previous_drugs_table_names,$patient_previous_drugs_parameters,$patient_previous_drugs_edit_name,$patient_previous_drugs_edit_id,$patient_previous_drugs_sumbol);
					if ($patient_previous_drugs_msg=="ok")
						$j++;
				}
			}
		}
		
		if ($j==$i)
			$antirheumatictreat_msg="ok";
		else
			$antirheumatictreat_msg="nok";
		//PREVIOUS ANTI-RHEUMATIC DRUG TREATMENT  table-end
		
		
		
		if ($antirheumatictreat_msg=="ok")
		{
			$save_chk="ok";
			pg_query("COMMIT") or die("Transaction commit failed\n");
		}
		else
		{
			$save_chk="nok";
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
	}
	catch(exception $e) {
		echo "e:".$e."</br>";
		echo "ROLLBACK";
		pg_query("ROLLBACK") or die("Transaction rollback failed\n");
	}
	
}

$exec = get_patient_demographics($pat_id);
$result = pg_fetch_array($exec);
$dateofinclusion=$result['dateofinclusion_str'];
if($dateofinclusion=="12-12-1900")
	$dateofinclusion="";
$dateofbirth=$result['dateofbirth_str'];
if($dateofbirth=="12-12-1900")
	$dateofbirth="";
$gender=$result['gender'];
if($gender==1)
	$gender_str="Female";
else if($gender==2)
	$gender_str="Male";
$patient_id=$result['patient_id'];
?>
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
	   <h1>
         Previous Anti-Rheumatic drug treatment
		</h1>
		<h1>
		<?php echo "<small>Patient:".$patient_id.",".$gender_str." (".$dateofbirth.") </small>"; ?>
      </h1>
      </section>

      <!-- Main content -->
      <section class="content">
	  <div class="alert alert_suc alert-success" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Successful save!</strong>
	  </div>
	  <div class="alert alert_wr alert-danger" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Unsuccessful save!</strong>
	  </div>
	  <form id="form" action="antirheumatictreat.php" method="POST">
	  <input type="hidden" id="save" name="save" value="1">
		<input type="hidden" id="pat_id" name="pat_id" value="<?php echo $pat_id;?>">
		<div class="row">
			<div class="form-group col-md-12">
				<input type="submit" class="btn btn-primary" value="Save">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Previous Anti-Rheumatic drug treatment</small></h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-12" id="divdiseases" name="divdiseases">
							<?php
										$sql = get_drugs_anti();
										$previous_drugs_counter=0;
										
										while($result2 = pg_fetch_array($sql))
										{	
											$previous_drugs_values .= $result2['code'];
											$previous_drugs_values .= "-";
											$previous_drugs_values .= $result2['substance'];
											$previous_drugs_values .= " (";
											$previous_drugs_values .= $result2['route_of_administration_val'];
											$previous_drugs_values .= ")";
											$previous_drugs_values .= "!@#$%^" ;
											
											
											$previous_drugs_ids .=$result2['drugs_id'];
											$previous_drugs_ids .= "!@#$%^" ;
															
											$previous_drugs_counter++;
										}
									?>
										<input type="hidden" value="<?php echo $previous_drugs_counter; ?>" id="previous_drugs_counter" name="previous_drugs_counter"/>
										<input type="hidden" value="<?php echo $previous_drugs_values; ?>" id="previous_drugs_values" name="previous_drugs_values"/>
										<input type="hidden" value="<?php echo $previous_drugs_ids; ?>" id="previous_drugs_ids" name="previous_drugs_ids"/>
									<?php
										$sql = get_discontinuation_reason();
										$discontinuation_reason_counter=0;
										
										while($result2 = pg_fetch_array($sql))
										{	
											$discontinuation_reason_values .= $result2['value'];
											$discontinuation_reason_values .= "!@#$%^" ;
											
											$discontinuation_reason_ids .=$result2['discontinuation_reason_id'];
											$discontinuation_reason_ids .= "!@#$%^" ;
															
											$discontinuation_reason_counter++;
										}
									?>
										<input type="hidden" value="<?php echo $discontinuation_reason_counter; ?>" id="discontinuation_reason_counter" name="discontinuation_reason_counter"/>
										<input type="hidden" value="<?php echo $discontinuation_reason_values; ?>" id="discontinuation_reason_values" name="discontinuation_reason_values"/>
										<input type="hidden" value="<?php echo $discontinuation_reason_ids; ?>" id="discontinuation_reason_ids" name="discontinuation_reason_ids"/>
									<?php
										/*$sql = get_lookup_tbl_values('route_of_administration');
										$route_counter=0;				
										while($result2 = pg_fetch_array($sql))
										{	
											$route_values .= $result2['value'];
											$route_values .= "!@#$%^" ;
											$route_ids .=$result2['id'];
											$route_ids .= "!@#$%^" ;
															
											$route_counter++;
										}*/
									?>
										<!--<input type="hidden" value="<?php echo $route_counter; ?>" id="route_counter" name="route_counter"/>
										<input type="hidden" value="<?php echo $route_values; ?>" id="route_values" name="route_values"/>
										<input type="hidden" value="<?php echo $route_ids; ?>" id="route_ids" name="route_ids"/>-->
								  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="patient_previous_drugs_tbl" name="patient_previous_drugs_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-3"><b>Drug</b></th>
											<!--<th class="col-md-2"><b>Route of</br>administration</b></th>
											<th class="col-md-2"><b>Treatment duration</br>(months)</b></th>
											<th class="col-md-1"><b>Year of</br>therapy start</b></th>-->
											<th class="col-md-1"><b>Date of</br>treatment start</b></th>
											<th class="col-md-1"><b>Treatment stop</b></th>
											<th class="col-md-1"><b>Date of</br>treatment stop</b></th>
											<th class="col-md-2"><b>Reason of</br>discontinuation</b></th>
											<th class="col-md-1">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
											$order = get_patient_previous_drugs($pat_id);
											$num_rows = pg_num_rows($order);
											
											while($result = pg_fetch_assoc($order))
											{
												$route_of_administration=$result['route_of_administration'];
												$patient_previous_drugs_id=$result['patient_previous_drugs_id'];
												$drugs_id=$result['drugs_id'];
												$discontinuation_reason_id=$result['reason_of_discontinuation_id'];
												$treatment_stop=$result['treatment_stop'];
												
												$start_date_str=$result['start_date_str'];
												if($start_date_str=="12-12-1900")
													$start_date_str="";
												$end_date_str=$result['end_date_str'];
												if($end_date_str=="12-12-1900")
													$end_date_str="";
									?>
										  <tr id="patient_previous_drugs_tr_<?php echo $i;?>">
												<td align="center">
													<input type="hidden" id="patient_previous_drugs_hidden_<?php echo $i;?>" name="patient_previous_drugs_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="patient_previous_drugs_id_<?php echo $i; ?>" id="patient_previous_drugs_id_<?php echo $i; ?>" value="<?php echo $patient_previous_drugs_id;?>">
													<select  class="form-control" name="drugs_id_<?php echo $i; ?>" id="drugs_id_<?php echo $i; ?>">
														<option value="0">--</option>
														<?php
															$sql = get_drugs_anti();
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$drugs_id2=$result2['drugs_id'];
																$value = $result2['code'];
																$value .= "-";
																$value .= $result2['substance'];
																$value .= " (";
																$value .= $result2['route_of_administration_val'];
																$value .= ")";
																
																
														?>
														<option value="<?php echo $drugs_id2; ?>" <?php if($drugs_id==$drugs_id2) { echo "selected"; } ?>><?php echo $value; ?></option>
														<?php
															}
														?>
													</select>
												</td>
												<!--<td align="center">
													<select class="form-control" name="route_of_administration_<?php /* echo $i; ?>" id="route_of_administration_<?php echo $i; ?>">
														<option value="0" <?php if($route_of_administration==0) { echo "selected"; } ?>>--</option>
														<?php
															$sql = get_lookup_tbl_values('route_of_administration');
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$route=$result2['id'];
																$value=$result2['value'];
														?>
															<option value="<?php echo $route; ?>" <?php if($route_of_administration==$route) { echo "selected"; } ?>><?php echo $value; ?></option>
														<?php
															}*/
																
														?>
														
														
													</select>
												</td>-->
												<td align="center">
													<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="startdate_<?php echo $i; ?>"  name="startdate_<?php echo $i; ?>" value="<?php echo $start_date_str; ?>">
												</td>
											   <td align="center">
													<select class="form-control" name="treatment_stop_<?php echo $i; ?>" id="treatment_stop_<?php echo $i; ?>">
														<option value="0" <?php if($treatment_stop==0) { echo "selected"; } ?>>--</option>
														<option value="1" <?php if($treatment_stop==1) { echo "selected"; } ?>>Yes</option>
														<option value="2" <?php if($treatment_stop==2) { echo "selected"; } ?>>No</option>
													</select>
												</td>
												<td align="center">
													<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="enddate_<?php echo $i; ?>"  name="enddate_<?php echo $i; ?>" value="<?php echo $end_date_str; ?>">
												</td>
												 <td align="center">
													<select class="form-control" name="reason_of_discontinuation_id_<?php echo $i; ?>" id="reason_of_discontinuation_id_<?php echo $i; ?>">
														<option value="0">--</option>
														<?php
															
															$sql = get_discontinuation_reason();
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$discontinuation_reason_id2=$result2['discontinuation_reason_id'];
																$value=$result2['value'];
																
															?>
															<option value="<?php echo $discontinuation_reason_id2; ?>" <?php if($discontinuation_reason_id==$discontinuation_reason_id2) { echo "selected"; } ?>><?php echo $value; ?></option>
															<?php
															}
														?>
													</select>
												</td>
												<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="patient_previous_drugs_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
												 </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										
									?>
									</tbody>
								</table>
								<input type="hidden" name="patient_previous_drugs_numofrows" id="patient_previous_drugs_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								 <a class="btn btn-primary" href="javascript:void(0);" onclick="patient_previous_drugs_addrow();"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
					</div>
				</div><!-- diagnoses status div -->
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<input type="submit" class="btn btn-primary" value="Save">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				&nbsp;
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				&nbsp;
			</div>
		</div>
		</form>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
   <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>

jQuery(document).ready(function() {

for(i=1;i<=document.getElementById('patient_previous_drugs_numofrows').value;i++)
{		$('#startdate_'+i).datepicker({
		   dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
		})
		$('#startdate_'+i).inputmask();
		
		$('#enddate_'+i).datepicker({
		  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
		})
		$('#enddate_'+i).inputmask();
}

 });
	
function patient_previous_drugs_addrow()
{
  var tbl = document.getElementById('patient_previous_drugs_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'patient_previous_drugs_tr_'+iteration;
  document.getElementById('patient_previous_drugs_numofrows').value = iteration;
	
	var previous_drugs_ids=document.getElementById('previous_drugs_ids').value;
	var previous_drugs_values=document.getElementById('previous_drugs_values').value;
	var previous_drugs_counter=document.getElementById('previous_drugs_counter').value;
	var previous_drugs_value=previous_drugs_values.split('!@#$%^');
	var previous_drugs_id=previous_drugs_ids.split('!@#$%^');
	
	var discontinuation_reason_ids=document.getElementById('discontinuation_reason_ids').value;
	var discontinuation_reason_values=document.getElementById('discontinuation_reason_values').value;
	var discontinuation_reason_counter=document.getElementById('discontinuation_reason_counter').value;
	var discontinuation_reason_value=discontinuation_reason_values.split('!@#$%^');
	var discontinuation_reason_id=discontinuation_reason_ids.split('!@#$%^');
	
	/*var route_ids=document.getElementById('route_ids').value;
	var route_values=document.getElementById('route_values').value;
	var route_counter=document.getElementById('route_counter').value;
	var route_value=route_values.split('!@#$%^');
	var route_id=route_ids.split('!@#$%^');*/
	
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('select');
  el1.name = 'drugs_id_' + iteration;
  el1.id = 'drugs_id_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
  
	var opt=new Option("--",0);
	el1.add(opt,undefined);
	for(i = 0; i < previous_drugs_counter; i++)
	{
		var opt=new Option(previous_drugs_value[i],previous_drugs_id[i]);
		el1.add(opt,undefined);
	}
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'patient_previous_drugs_hidden_' + iteration;
  el2.id = 'patient_previous_drugs_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'patient_previous_drugs_id_' + iteration;
  el3.id = 'patient_previous_drugs_id_' + iteration;
  el3.value='';
  
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
  /*var r11 = row.insertCell(1);
  r11.align='center';
  
  var el11 = document.createElement('select');
  el11.name = 'route_of_administration_' + iteration;
  el11.id = 'route_of_administration_' + iteration;
  el11.style.width='100%';
  el11.className='form-control';
  
	var opt=new Option("--",0);
	el11.add(opt,undefined);
	for(i = 0; i < route_counter; i++)
	{
		var opt=new Option(route_value[i],route_id[i]);
		el11.add(opt,undefined);
	}
	r11.appendChild(el11);*/
	 
	var r2 = row.insertCell(1);
	r2.align='center';
	
	var el4 = document.createElement('input');
	el4.type = 'text';
	el4.name = 'startdate_' + iteration;
	el4.id = 'startdate_' + iteration;
	el4.style.width='100%';
	el4.className='form-control';
  
	r2.appendChild(el4);
  
	
	var r4 = row.insertCell(2);
	r4.align='center';
  
	var el6 = document.createElement('select');
	el6.name = 'treatment_stop_' + iteration;
	el6.id = 'treatment_stop_' + iteration;
	el6.style.width='100%';
	el6.className='form-control';
  
	var opt=new Option("--",0);
	el6.add(opt,undefined);
	var opt=new Option("Yes",1);
	el6.add(opt,undefined);
	var opt=new Option("No",2);
	el6.add(opt,undefined);
	
	r4.appendChild(el6);
	
	var r22 = row.insertCell(3);
	r22.align='center';
	
	var el44 = document.createElement('input');
	el44.type = 'text';
	el44.name = 'enddate_' + iteration;
	el44.id = 'enddate_' + iteration;
	el44.style.width='100%';
	el44.className='form-control';
  
	r22.appendChild(el44);
  
		var r5 = row.insertCell(4);
		r5.align='center';
  
		var el7 = document.createElement('select');
		el7.name = 'reason_of_discontinuation_id_' + iteration;
		el7.id = 'reason_of_discontinuation_id_' + iteration;
		el7.style.width='100%';
		el7.className='form-control';
	  
		var opt=new Option("--",0);
		el7.add(opt,undefined);
		for(i = 0; i < discontinuation_reason_counter; i++)
		{
			var opt=new Option(discontinuation_reason_value[i],discontinuation_reason_id[i]);
			el7.add(opt,undefined);
		}
  
	r5.appendChild(el7);
	
	var r6 = row.insertCell(5);
	r6.align='center';
	
	var el8 = document.createElement('a');
	el8.name = 'a_' + iteration;
	el8.id = 'a_' + iteration;
	el8.title = 'Delete';
	el8.href="javascript:avoid(0)";
	var icon = document.createElement('i');
	icon.className='fa fa-trash-o';
	el8.appendChild(icon);
	el8.onclick =function(){patient_previous_drugs_removerow(iteration)};
		 
	r6.appendChild(el8);
	
	$('#startdate_'+iteration).datepicker({
		  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  $('#startdate_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
		
		$('#enddate_'+iteration).datepicker({
		  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  $('#enddate_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
}

function patient_previous_drugs_removerow(row)
{
	document.getElementById('patient_previous_drugs_hidden_'+row).value='-1';
	document.getElementById('patient_previous_drugs_tr_'+row).style.display='none';
}


</script>
<?php
	
if($save==1)
{
	if($save_chk=="ok")
	{
?>
<script>
$(".alert_suc").show();
window.setTimeout(function() {
    $(".alert_suc").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
	else
	{
?>
<script>
$(".alert_wr").show();
window.setTimeout(function() {
    $(".alert_wr").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
}
?>
</body>
</html>
