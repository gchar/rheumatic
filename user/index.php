<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	$_SESSION['pat_id']="";
	$_SESSION['patient_cohort_id']="";
	$_SESSION['patient_followup_id']="";
	$_SESSION['patient_event_id']="";
	
	$save=$_REQUEST['save'];
	$pat_id_deleted=$_REQUEST['pat_id_deleted'];
	$search_pat_id=$_REQUEST['search_pat_id'];
	$search_secondary_id=$_REQUEST['search_secondary_id'];
	$search_id=$_REQUEST['search_id'];
	
	
	
	if($save==1 and $pat_id_deleted<>"")
	{
		
		pg_query("BEGIN") or die("Could not start transaction\n");
		try
		{
			
			$sql = "SELECT public.deletepatient('".$pat_id_deleted."')";
			$order = pg_query($sql);
			$result = pg_fetch_array($order);
			
			if ($result['deletepatient']=="OK")
			{
				$save_chk="ok";
				pg_query("COMMIT") or die("Transaction commit failed\n");
			}
			else
			{
				$save_chk="nok";
				pg_query("ROLLBACK") or die("Transaction rollback failed\n");
			}
			
		}
		catch(exception $e) {
			echo "e:".$e."</br>";
			echo "ROLLBACK";
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
	}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <small>Patients</small>
        </h1>
      </section>

      <!-- Main content -->
      <section class="content">

      <!-- Your Page Content Here -->
		<div class="box">
            <!-- /.box-header -->
			<div class="box-header with-border">
				<h3 class="box-title">Search patients criteria</h3>
			</div>
            <div class="box-body">
			 <form id="form1" action="#" method="POST">
				<input type="hidden" id="save" name="save" value="1">
				<input type="hidden" id="pat_id_deleted" name="pat_id_deleted">
				<div class="row">
					<div class="form-group col-md-4">
						<label>Patient id</label>
						<input type="text" class="form-control" value="<?php echo $search_pat_id; ?>" id="search_pat_id" name="search_pat_id">
					</div>
					<div class="form-group col-md-4">
						<label>Secondary id</label>
						<input type="text" class="form-control" value="<?php echo $search_secondary_id; ?>" id="search_secondary_id" name="search_secondary_id">
					</div>
					<div class="form-group col-md-4">
						<label>id</label>
						<input type="text" class="form-control" value="<?php echo $search_id; ?>" id="search_id" name="search_id">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
					  <input type="submit" class="btn btn-primary" value="Search patients">
					  <a href="demographics.php" class="btn btn-primary"><i class="fa fa-user-plus" aria-hidden="true"></i> New patient</a>
					</div>
				</div>
			</form>
			</div>
			</div>
			<?php
			if($save==1)
			{
			?>
			<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Results</h3>
			</div>
				 <div class="box-body">
			<?php
				$exec = get_patients_table($search_pat_id,$search_secondary_id,$search_id);
				$num_rows = pg_num_rows($exec);
				if ($num_rows=='0')
				{
				?>
				<tr align=center><td colspan=3>
				<?php
					echo "No records!!!";
				?>
				</td></tr>
				</table>
				<br>
				<?php
				}
				else
				{
				?>
				<form id="form2" action="demographics.php" method="POST">
				  <input type="hidden" id="pat_id" name="pat_id" value="<?php echo $pat_id;?>">
				
			<div class="row">
			  <div class="col-md-8">
			  <!--<a class="btn btn-primary" href="demographics.php" target="_self"><i class="fa fa-plus"></i> Add patient</a>-->
			  </br></br>
				<table id="table_jq" class="table table-bordered table-striped">
					<thead>
						<tr class="gradeC">
							<th>Patient id</th>
							<th>Secondary id</th>
							<th>Date of birth</th>
							<th>Id</th>
							<th>Age at inclusion</th>
							<th>Date of inclusion</th>
							<th>Delete</th>
							<th>Edit</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$i=0;
						while($result = pg_fetch_array($exec))
						{
							
							/*$pat_id=$result['pat_id'];
							$patient_id=$result['patient_id'];
							$secondary_id=$result['secondary_id'];
							$dateofinclusion=$result['dateofinclusion_str'];
							$dateofbirth=$result['dateofbirth_str'];
							$id=$result['id'];
							$ageatinclusion=$result['ageatinclusion'];*/
								$i++;
					?>
						<tr class="gradeA" align="center" >
							<td>&nbsp;<?php echo $result['patient_id']; ?></td>
							<td>&nbsp;<?php echo $result['secondary_id']; ?></td>
							<td>&nbsp;<?php echo $result['dateofbirth_str']; ?></td>
							<td>&nbsp;<?php echo $result['id']; ?></td>
							<td>&nbsp;<?php echo $result['ageatinclusion']; ?></td>
							<td>&nbsp;<?php echo $result['dateofinclusion_str']; ?></td>
							<td align="center"><a title="Delete" href="javascript:void(0);" onclick="deletepatient(<?php echo $result['pat_id']; ?>)"><i class="fa fa-trash"></i><span></a></td>
							<td>
								<!--<input value="edit" class="btn btn-primary" type="submit" onclick="setPatid(<?php echo $pat_id; ?>)">-->
								
								<a href="#" class="btn btn-primary" onclick="setPatid(<?php echo $result['pat_id']; ?>)">edit</a>
							</td>
						</tr>
						<?php
						}
						?>
					</tbody>
				</table> <?php
				}
			?>
			</div>
			</div>
			 </form>
       <!-- /.box-body -->
       </div>
       <!-- /.box -->
		</div>
			<?php
			}
				?>
				<br>
			<!--<a class="btn btn-primary link" href="demographics.php" target="_self"><i class="fa fa-plus"></i> Add patient</a>-->
            
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<?php

if($save==1 and $pat_id_deleted<>"")
{
	if($save_chk=="ok")
	{
?>
<script>
alert("Record deleted!");
</script>
<?php
	}
	else
	{
?>
<script>
alert("unsuccessful record deletion!");
</script>
<?php
	}
}
?>
<script type="text/javascript" charset="utf-8">
function setPatid(patid)
{
	$('#pat_id').val(patid);
	$('#form2').submit();
}

	$(document).ready(function() {
		
		$('#table_jq').dataTable({
			"aoColumnDefs": [
    { "type": "num" },
	{ "type": "num" },
	{ "type": "date" },
	{ "type": "num" },
	{ "type": "num" },
	{ "type": "date" },
	{ "type": "num" }
  ]
		});
	});

function deletepatient(pat_id)
{
	if (confirm("Are you sure about this action? All data available for this record will be deleted."))
	{
		$('#pat_id_deleted').val(pat_id);
		$('#form1').submit();
	}
	else
	{
		return false;
	}
}
</script>
</body>
</html>
