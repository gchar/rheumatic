<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	$save=$_REQUEST['save'];
	$pat_id=$_REQUEST['pat_id'];
	$new=$_REQUEST['new'];
	if($pat_id<>"")
		$_SESSION['pat_id']=$pat_id;
	
	$patient_cohort_id=$_REQUEST['patient_cohort_id'];
	if($patient_cohort_id<>"")
		$_SESSION['patient_cohort_id']=$patient_cohort_id;
	$patient_cohort_id=$_SESSION['patient_cohort_id'];
	
	$pat_id=$_SESSION['pat_id'];
	$patient_followup_id=$_REQUEST['patient_followup_id'];
	
	if($patient_followup_id<>"")
		$_SESSION['patient_followup_id']=$patient_followup_id;
	
	if($new==1){
		$_SESSION['patient_followup_id']="";
		$_SESSION['patient_event_id']="";
		$patient_followup_id="";
		$patient_event_id="";
	}
	else
		$patient_followup_id=$_SESSION['patient_followup_id'];
	
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<?php
if($save==1)
{
	pg_query("BEGIN") or die("Could not start transaction\n");
	try
	{
		
		/*$parameters[0]=$_REQUEST['followup_name'];
		$parameters[1]=str_replace("'", "''",$_REQUEST['treatment_nbr']);
		$parameters[4]=$_REQUEST['previous_dmards_nbr'];
		$parameters[5]=$_REQUEST['ongoing_dmards_nbr'];
		$table_names[0]='followup_name';
		$table_names[1]='treatment_nbr';
		$table_names[4]='previous_dmards_nbr';
		$table_names[5]='ongoing_dmards_nbr';*/
		
		$parameters[0]=date_for_postgres($_REQUEST['start_date']);
		$parameters[1]=$_REQUEST['physician_assigning'];
		$table_names[0]='start_date';
		$table_names[1]='physician_assigning';
		$parameters[2]=$pat_id;
		$table_names[2]='pat_id';
		$parameters[3]=$user_id;
		$table_names[3]='editor_id';
		$parameters[4]='now()';
		$table_names[4]='edit_date';
		$parameters[5]=0;
		$table_names[5]='deleted';
		$parameters[6]=$patient_cohort_id;
		$table_names[6]='patient_cohort_id';
		$parameters[7]=$_REQUEST['euroqol'];
		$table_names[7]='euroqol';
		$parameters[8]=$_REQUEST['wpai'];
		$table_names[8]='wpai';
		$parameters[9]=$_REQUEST['hads'];
		$table_names[9]='hads';
		$parameters[10]=$_REQUEST['sf36'];
		$table_names[10]='sf36';
		$parameters[11]=$_REQUEST['common'];
		$table_names[11]='common';
		$parameters[12]=$_REQUEST['spa'];
		$table_names[12]='spa';
		$parameters[13]=$_REQUEST['fumonthinclusion'];
		$table_names[13]='fumonthinclusion';
		$parameters[14]=$_REQUEST['fumonthcohort'];
		$table_names[14]='fumonthcohort';
		$parameters[15]=$_REQUEST['tobacco'];
		if($_REQUEST['tobacco']==2)
		{
			$parameters[16]=str_replace("'", "''",$_REQUEST['smokingstarting']);
			$parameters[17]=$_REQUEST['smokingstopped'];
			$parameters[18]=str_replace("'", "''",$_REQUEST['smokingyear']);
			$parameters[19]=$_REQUEST['decadefirst'];
			$parameters[20]=$_REQUEST['decadesecond'];
			$parameters[21]=$_REQUEST['decadethird'];
			$parameters[22]=$_REQUEST['decadeforth'];
			$parameters[23]=$_REQUEST['decadefifth'];
			$parameters[24]=$_REQUEST['decadesixth'];
			$parameters[25]=$_REQUEST['decadeseventh'];
			$parameters[26]=$_REQUEST['decadeeight'];
			$parameters[27]=str_replace("'", "''",$_REQUEST['averagepackperday']);
			$parameters[28]=str_replace("'", "''",$_REQUEST['averagepackperyear']);
		}
		else
		{
			$parameters[16]=0;
			$parameters[17]=0;
			$parameters[18]=0;
			$parameters[19]=0;
			$parameters[20]=0;
			$parameters[21]=0;
			$parameters[22]=0;
			$parameters[23]=0;
			$parameters[24]=0;
			$parameters[25]=0;
			$parameters[26]=0;
			$parameters[27]=0;
			$parameters[28]=0;
		}
		$table_names[15]='tobacco';
		$table_names[16]='smokingstarting';
		$table_names[17]='smokingstopped';
		$table_names[18]='smokingyear';
		$table_names[19]='decadefirst';
		$table_names[20]='decadesecond';
		$table_names[21]='decadethird';
		$table_names[22]='decadeforth';
		$table_names[23]='decadefifth';
		$table_names[24]='decadesixth';
		$table_names[25]='decadeseventh';
		$table_names[26]='decadeeight';
		$table_names[27]='averagepackperday';
		$table_names[28]='averagepackperyear';
		$parameters[29]=$_REQUEST['sle'];
		$table_names[29]='sle';
		$parameters[30]=$_REQUEST['sle_severity'];
		$table_names[30]='sle_severity';
		$parameters[31]=$_REQUEST['asas'];
		$table_names[31]='asas';
		$parameters[32]=$_REQUEST['psaid'];
		$table_names[32]='psaid';
		
		
		$edit_name[0]='patient_followup_id';
		$edit_id[0]=$patient_followup_id;
		$sumbol[0]='=';
		if($patient_followup_id=='')
		{
			$patient_followup_id=insert('patient_followup',$table_names,$parameters,'patient_followup_id');
			if($patient_followup_id!='')
				$msg="ok";
		}
		else
		{
			$msg=update('patient_followup',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
		}
		
		//DRUG TREATMENT  table-start
		
		//patient_lookup_drugs delete
		$patient_cohort_drugs_hdn_parameters2[0]=1;
		$patient_cohort_drugs_hdn_names2[0]='deleted';
		$patient_cohort_drugs_hdn_edit_name2[0]='patient_followup_id';
		$patient_cohort_drugs_hdn_edit_id2[0]=$patient_followup_id;
		$patient_cohort_drugs_hdn_sumbol2[0]='=';
		$patient_cohort_drugs_hdn_edit_name2[1]='drug_flag';
		$patient_cohort_drugs_hdn_edit_id2[1]=1;
		$patient_cohort_drugs_hdn_sumbol2[1]='=';
		$patient_cohort_drugs_hdn_edit_name2[2]='fumonth_cohort';
		$patient_cohort_drugs_hdn_edit_id2[2]=$_REQUEST['fumonthcohort'];
		$patient_cohort_drugs_hdn_sumbol2[2]='=';
						
		$patient_cohort_drugs_msg2=update('patient_lookup_drugs',$patient_cohort_drugs_hdn_names2,$patient_cohort_drugs_hdn_parameters2,$patient_cohort_drugs_hdn_edit_name2,$patient_cohort_drugs_hdn_edit_id2,$patient_cohort_drugs_hdn_sumbol2);
		
		
		$j=1;
		$patient_cohort_drugs_numofrows=$_REQUEST['patient_cohort_drugs_numofrows'];
		for ($i=1;$i<=$patient_cohort_drugs_numofrows;$i++)
		{
			$patient_cohort_drugs_hidden=$_REQUEST['patient_cohort_drugs_hidden_'.$i];
			if($patient_cohort_drugs_hidden=='-1')
			{
				if ($_REQUEST['patient_cohort_drugs_type_'.$i]=='cohort')
				{
					$patient_cohort_drugs_id=$_REQUEST['patient_cohort_drugs_id_'.$i];
					if($patient_cohort_drugs_id<>"")
					{
						$patient_cohort_drugs_hdn_parameters[0]=1;
						$patient_cohort_drugs_hdn_names[0]='deleted';
						$patient_cohort_drugs_hdn_edit_name[0]='patient_cohort_drugs_id';
						$patient_cohort_drugs_hdn_edit_id[0]=$patient_cohort_drugs_id;
						$patient_cohort_drugs_hdn_sumbol[0]='=';
						$patient_cohort_drugs_msg=update('patient_cohort_drugs',$patient_cohort_drugs_hdn_names,$patient_cohort_drugs_hdn_parameters,$patient_cohort_drugs_hdn_edit_name,$patient_cohort_drugs_hdn_edit_id,$patient_cohort_drugs_hdn_sumbol);
						
						//patient_lookup_drugs delete
						/*$patient_cohort_drugs_hdn_parameters2[0]=1;
						$patient_cohort_drugs_hdn_names2[0]='deleted';
						$patient_cohort_drugs_hdn_edit_name2[0]='drugs_id';
						$patient_cohort_drugs_hdn_edit_id2[0]=$_REQUEST['drugs_id_'.$i];
						$patient_cohort_drugs_hdn_sumbol2[0]='=';
						$patient_cohort_drugs_hdn_edit_name2[1]='patient_followup_id';
						$patient_cohort_drugs_hdn_edit_id2[1]=$patient_followup_id;
						$patient_cohort_drugs_hdn_sumbol2[1]='=';
						$patient_cohort_drugs_hdn_edit_name2[2]='drug_flag';
						$patient_cohort_drugs_hdn_edit_id2[2]=1;
						$patient_cohort_drugs_hdn_sumbol2[2]='=';
						
						$patient_cohort_drugs_msg2=update('patient_lookup_drugs',$patient_cohort_drugs_hdn_names2,$patient_cohort_drugs_hdn_parameters2,$patient_cohort_drugs_hdn_edit_name2,$patient_cohort_drugs_hdn_edit_id2,$patient_cohort_drugs_hdn_sumbol2);
						*/
						
						/* Αφαιρέθηκε στις 2021-11-30 γιατι στην αρχή γίνονται όλα τα patient_lookup_drugs deleted=1 οπότε δεν χρειάζεται να υπάρχει
						$current_drugs_id=$_REQUEST['drugs_id_'.$i];
						$q1 = "select patient_lookup_drugs_id from patient_lookup_drugs where deleted=0 and patient_followup_id=$patient_followup_id and drugs_id=$current_drugs_id and drug_flag=1";
						$q_exec = pg_query($q1);
						$q_reason=pg_fetch_array($q_exec); 
						
						$patient_lookup_drugs_id=$q_reason['patient_lookup_drugs_id'];
						
						$patient_cohort_drugs_hdn_parameters2[0]=1;
						$patient_cohort_drugs_hdn_names2[0]='deleted';
						$patient_cohort_drugs_hdn_edit_name2[0]='patient_lookup_drugs_id';
						$patient_cohort_drugs_hdn_edit_id2[0]=$patient_lookup_drugs_id;
						$patient_cohort_drugs_hdn_sumbol2[0]='=';
						
						$patient_cohort_drugs_msg2=update('patient_lookup_drugs',$patient_cohort_drugs_hdn_names2,$patient_cohort_drugs_hdn_parameters2,$patient_cohort_drugs_hdn_edit_name2,$patient_cohort_drugs_hdn_edit_id2,$patient_cohort_drugs_hdn_sumbol2);
						
						if ($patient_cohort_drugs_msg=="ok" and $patient_cohort_drugs_msg2=="ok")
						*/
						if ($patient_cohort_drugs_msg=="ok")
							$j++;
					}
					else
						$j++;
					
					$drugs_id="";
				}
				else if ($_REQUEST['patient_cohort_drugs_type_'.$i]=='period')
				{
					$patient_cohort_drugs_period_id=$_REQUEST['patient_cohort_drugs_period_id_'.$i];
					if($patient_cohort_drugs_period_id<>"")
					{
						$patient_cohort_drugs_hdn_parameters[0]=1;
						$patient_cohort_drugs_hdn_names[0]='deleted';
						$patient_cohort_drugs_hdn_edit_name[0]='patient_cohort_drugs_period_id';
						$patient_cohort_drugs_hdn_edit_id[0]=$patient_cohort_drugs_period_id;
						$patient_cohort_drugs_hdn_sumbol[0]='=';
						$patient_cohort_drugs_msg=update('patient_cohort_drugs_period',$patient_cohort_drugs_hdn_names,$patient_cohort_drugs_hdn_parameters,$patient_cohort_drugs_hdn_edit_name,$patient_cohort_drugs_hdn_edit_id,$patient_cohort_drugs_hdn_sumbol);
						if ($patient_cohort_drugs_msg=="ok")
							$j++;
					}
					else
						$j++;
				}
				else if ($_REQUEST['patient_cohort_drugs_type_'.$i]=='period_label')
				{
					$j++;
				}
				
			}
			else
			{
				if ($_REQUEST['patient_cohort_drugs_type_'.$i]=='cohort')
				{
					$patient_cohort_drugs_id=$_REQUEST['patient_cohort_drugs_id_'.$i];
					
					//patient_lookup_drugs check if is the first else make insert or update
					if ($drugs_id<>"")
					{
						$patient_lookup_drugs_parameters[0]=$pat_id;
						$patient_lookup_drugs_parameters[1]=0;
						$patient_lookup_drugs_parameters[2]=$user_id;
						$patient_lookup_drugs_parameters[3]='now()';
						$patient_lookup_drugs_parameters[4]=$patient_cohort_id;
						$patient_lookup_drugs_parameters[5]=$patient_followup_id;
						$patient_lookup_drugs_parameters[6]=$_REQUEST['fumonthcohort'];
						$patient_lookup_drugs_parameters[7]=$_REQUEST['fumonthinclusion'];
						$patient_lookup_drugs_parameters[8]=$drugs_id;
						$patient_lookup_drugs_parameters[9]=$route;
						$patient_lookup_drugs_parameters[10]=$maintherapy;
						$patient_lookup_drugs_parameters[11]=round($weekdosage, 2);
						$patient_lookup_drugs_parameters[12]=1;
						$patient_lookup_drugs_parameters[13]=round($currentdosage, 2);
						
						
						$patient_lookup_drugs_table_names[0]='pat_id';
						$patient_lookup_drugs_table_names[1]='deleted';
						$patient_lookup_drugs_table_names[2]='editor_id';
						$patient_lookup_drugs_table_names[3]='edit_date';
						$patient_lookup_drugs_table_names[4]='patient_cohort_id';
						$patient_lookup_drugs_table_names[5]='patient_followup_id';
						$patient_lookup_drugs_table_names[6]='fumonth_cohort';
						$patient_lookup_drugs_table_names[7]='fumonth_inclusion';
						$patient_lookup_drugs_table_names[8]='drugs_id';
						$patient_lookup_drugs_table_names[9]='route';
						$patient_lookup_drugs_table_names[10]='maintherapy';
						$patient_lookup_drugs_table_names[11]='weekdosage';
						$patient_lookup_drugs_table_names[12]='drug_flag';
						$patient_lookup_drugs_table_names[13]='currentdosage';
						
						$patient_lookup_drugs_edit_name[0]='patient_followup_id';
						$patient_lookup_drugs_edit_id[0]=$patient_followup_id;
						$patient_lookup_drugs_sumbol[0]='=';
						$patient_lookup_drugs_edit_name[1]='drugs_id';
						$patient_lookup_drugs_edit_id[1]=$drugs_id;
						$patient_lookup_drugs_sumbol[1]='=';
						$patient_lookup_drugs_edit_name[2]='drug_flag';
						$patient_lookup_drugs_edit_id[2]=1;
						$patient_lookup_drugs_sumbol[2]='=';
						
						
						$q1 = "select patient_lookup_drugs_id from patient_lookup_drugs where deleted=0 and patient_followup_id=$patient_followup_id and drugs_id=$drugs_id and drug_flag=1";
						$q_exec = pg_query($q1);
						$q_reason=pg_fetch_array($q_exec); 
						$patient_lookup_drugs_id=$q_reason['patient_lookup_drugs_id'];
						
						if($patient_lookup_drugs_id=='')
						{
							$patient_lookup_drugs_id=insert('patient_lookup_drugs',$patient_lookup_drugs_table_names,$patient_lookup_drugs_parameters,'patient_lookup_drugs_id');
							if($patient_lookup_drugs_id!='')
								$patient_lookup_drugs_msg="ok";
						}
						else
							$patient_lookup_drugs_msg=update('patient_lookup_drugs',$patient_lookup_drugs_table_names,$patient_lookup_drugs_parameters,$patient_lookup_drugs_edit_name,$patient_lookup_drugs_edit_id,$patient_lookup_drugs_sumbol);
					}
					else
						$patient_lookup_drugs_msg="ok";
					
					if (isset($_REQUEST['main_'.$i]))
						$main="1";
					else
						$main="0";
		
					$patient_cohort_drugs_edit_name[0]='patient_cohort_drugs_id';
					$patient_cohort_drugs_edit_id[0]=$patient_cohort_drugs_id;
					$patient_cohort_drugs_sumbol[0]='=';
					
					//patient_lookup_drugs initialazation
					$patient_lookup_drugs_id="";
					$drugs_id="";
					$route="";
					$maintherapy="";
					$weekdosage="";
					$currentdosage="0";
					$trans="";
					
					//patient_lookup_drugs set parameters
					$drugs_id=$_REQUEST['drugs_id_'.$i];
					$maintherapy=$main;
					$sql = get_drug($drugs_id);
					$result2 = pg_fetch_array($sql);
					$route=$result2['route_of_administration'];
					
					if($patient_cohort_drugs_id=='')
					{
						$patient_cohort_drugs_parameters[0]=$user_id;
						$patient_cohort_drugs_parameters[1]='now()';
						$patient_cohort_drugs_parameters[2]=$_REQUEST['drugs_id_'.$i];
						$patient_cohort_drugs_parameters[3]='';
						//$patient_cohort_drugs_parameters[4]=$_REQUEST['route_of_administration_'.$i];
						$patient_cohort_drugs_parameters[4]=$patient_cohort_id;
						$patient_cohort_drugs_parameters[5]=0;
						$patient_cohort_drugs_parameters[6]=date_for_postgres($_REQUEST['startdate_'.$i]);
						$patient_cohort_drugs_parameters[7]=date_for_postgres($_REQUEST['stopdate_'.$i]);
						$patient_cohort_drugs_parameters[8]=$main;
						$patient_cohort_drugs_parameters[9]=$patient_followup_id;
						$patient_cohort_drugs_parameters[10]=$pat_id;
						$patient_cohort_drugs_parameters[11]=0;
						
											
						$patient_cohort_drugs_table_names[0]='editor_id';
						$patient_cohort_drugs_table_names[1]='edit_date';
						$patient_cohort_drugs_table_names[2]='drugs_id';
						$patient_cohort_drugs_table_names[3]='dose';
						//$patient_cohort_drugs_table_names[4]='route_of_administration';
						$patient_cohort_drugs_table_names[4]='patient_cohort_id';
						$patient_cohort_drugs_table_names[5]='frequency';
						$patient_cohort_drugs_table_names[6]='startdate';
						$patient_cohort_drugs_table_names[7]='stopdate';
						$patient_cohort_drugs_table_names[8]='main';
						$patient_cohort_drugs_table_names[9]='patient_followup_id';
						$patient_cohort_drugs_table_names[10]='pat_id';
						$patient_cohort_drugs_table_names[11]='deleted';
						
						$patient_cohort_drugs_id=insert('patient_cohort_drugs',$patient_cohort_drugs_table_names,$patient_cohort_drugs_parameters,'patient_cohort_drugs_id');
						if($patient_cohort_drugs_id!='' and $patient_lookup_drugs_msg=="ok")
							$j++;
					}
					else
					{
						$patient_cohort_drugs_parameters2[0]=$user_id;
						$patient_cohort_drugs_parameters2[1]='now()';
						$patient_cohort_drugs_parameters2[2]=$_REQUEST['drugs_id_'.$i];
						$patient_cohort_drugs_parameters2[3]='';
						//$patient_cohort_drugs_parameters2[4]=$_REQUEST['route_of_administration_'.$i];
						$patient_cohort_drugs_parameters2[4]=$patient_cohort_id;
						$patient_cohort_drugs_parameters2[5]=0;
						$patient_cohort_drugs_parameters2[6]=date_for_postgres($_REQUEST['startdate_'.$i]);
						$patient_cohort_drugs_parameters2[7]=date_for_postgres($_REQUEST['stopdate_'.$i]);
						$patient_cohort_drugs_parameters2[8]=$main;
						
						$patient_cohort_drugs_table_names2[0]='editor_id';
						$patient_cohort_drugs_table_names2[1]='edit_date';
						$patient_cohort_drugs_table_names2[2]='drugs_id';
						$patient_cohort_drugs_table_names2[3]='dose';
						//$patient_cohort_drugs_table_names2[4]='route_of_administration';
						$patient_cohort_drugs_table_names2[4]='patient_cohort_id';
						$patient_cohort_drugs_table_names2[5]='frequency';
						$patient_cohort_drugs_table_names2[6]='startdate';
						$patient_cohort_drugs_table_names2[7]='stopdate';
						$patient_cohort_drugs_table_names2[8]='main';
						
						$patient_cohort_drugs_msg=update('patient_cohort_drugs',$patient_cohort_drugs_table_names2,$patient_cohort_drugs_parameters2,$patient_cohort_drugs_edit_name,$patient_cohort_drugs_edit_id,$patient_cohort_drugs_sumbol);
						if ($patient_cohort_drugs_msg=="ok" and $patient_lookup_drugs_msg=="ok")
							$j++;
					}
				}
				else if ($_REQUEST['patient_cohort_drugs_type_'.$i]=='period')
				{
					$patient_cohort_drugs_period_id=$_REQUEST['patient_cohort_drugs_period_id_'.$i];
					
					
					
					$patient_cohort_drugs_period_edit_name[0]='patient_cohort_drugs_period_id';
					$patient_cohort_drugs_period_edit_id[0]=$patient_cohort_drugs_period_id;
					$patient_cohort_drugs_period_sumbol[0]='=';
					
					if($patient_cohort_drugs_period_id=='')
					{
						$patient_cohort_drugs_period_parameters[0]=$user_id;
						$patient_cohort_drugs_period_parameters[1]='now()';
						$patient_cohort_drugs_period_parameters[2]=$patient_cohort_drugs_id;
						$patient_cohort_drugs_period_parameters[3]=str_replace("'", "''",$_REQUEST['dose_'.$i]);
						$patient_cohort_drugs_period_parameters[4]=$patient_cohort_id;
						$patient_cohort_drugs_period_parameters[5]=$_REQUEST['frequency_'.$i];
						$patient_cohort_drugs_period_parameters[6]=date_for_postgres($_REQUEST['startdate_'.$i]);
						$patient_cohort_drugs_period_parameters[7]=date_for_postgres($_REQUEST['stopdate_'.$i]);
						$patient_cohort_drugs_period_parameters[8]=$patient_followup_id;
						$patient_cohort_drugs_period_parameters[9]=$_REQUEST['reason_'.$i];
						$patient_cohort_drugs_period_parameters[10]=0;
						
						$patient_cohort_drugs_period_table_names[0]='editor_id';
						$patient_cohort_drugs_period_table_names[1]='edit_date';
						$patient_cohort_drugs_period_table_names[2]='patient_cohort_drugs_id';
						$patient_cohort_drugs_period_table_names[3]='dose';
						$patient_cohort_drugs_period_table_names[4]='patient_cohort_id';
						$patient_cohort_drugs_period_table_names[5]='frequency';
						$patient_cohort_drugs_period_table_names[6]='startdate';
						$patient_cohort_drugs_period_table_names[7]='stopdate';
						$patient_cohort_drugs_period_table_names[8]='patient_followup_id';
						$patient_cohort_drugs_period_table_names[9]='reason';	
						$patient_cohort_drugs_period_table_names[10]='deleted';
						
						$patient_cohort_drugs_period_id=insert('patient_cohort_drugs_period',$patient_cohort_drugs_period_table_names,$patient_cohort_drugs_period_parameters,'patient_cohort_drugs_period_id');
						if($patient_cohort_drugs_period_id!='')
							$j++;
					}
					else
					{
						$patient_cohort_drugs_period_parameters2[0]=$user_id;
						$patient_cohort_drugs_period_parameters2[1]='now()';
						$patient_cohort_drugs_period_parameters2[2]=$patient_cohort_drugs_id;
						$patient_cohort_drugs_period_parameters2[3]=str_replace("'", "''",$_REQUEST['dose_'.$i]);
						$patient_cohort_drugs_period_parameters2[4]=$patient_cohort_id;
						$patient_cohort_drugs_period_parameters2[5]=$_REQUEST['frequency_'.$i];
						$patient_cohort_drugs_period_parameters2[6]=date_for_postgres($_REQUEST['startdate_'.$i]);
						$patient_cohort_drugs_period_parameters2[7]=date_for_postgres($_REQUEST['stopdate_'.$i]);
						$patient_cohort_drugs_period_parameters2[8]=$_REQUEST['reason_'.$i];
						
						$patient_cohort_drugs_period_table_names2[0]='editor_id';
						$patient_cohort_drugs_period_table_names2[1]='edit_date';
						$patient_cohort_drugs_period_table_names2[2]='patient_cohort_drugs_id';
						$patient_cohort_drugs_period_table_names2[3]='dose';
						$patient_cohort_drugs_period_table_names2[4]='patient_cohort_id';
						$patient_cohort_drugs_period_table_names2[5]='frequency';
						$patient_cohort_drugs_period_table_names2[6]='startdate';
						$patient_cohort_drugs_period_table_names2[7]='stopdate';
						$patient_cohort_drugs_period_table_names2[8]='reason';	
						
						$patient_cohort_drugs_period_msg=update('patient_cohort_drugs_period',$patient_cohort_drugs_period_table_names2,$patient_cohort_drugs_period_parameters2,$patient_cohort_drugs_period_edit_name,$patient_cohort_drugs_period_edit_id,$patient_cohort_drugs_period_sumbol);
						if ($patient_cohort_drugs_period_msg=="ok")
							$j++;
					}
					
					//patient_lookup_drugs set weekdosage and currentdosage
					if($_REQUEST['start_date']<>"" and $_REQUEST['startdate_'.$i]<>"")
					{
						$date_start=date_create($_REQUEST['start_date']);
						$date_start_curr=date_create($_REQUEST['start_date']);
						$date_start = date_sub($date_start,date_interval_create_from_date_string("90 days"));
						
						$date_start=date_format($date_start,"Y-m-d");
						$date_start_curr=date_format($date_start_curr,"Y-m-d");
						$date_start_time=strtotime($date_start);
						$date_start_curr_time=strtotime($date_start_curr);
						
						$date_end=date_create($_REQUEST['start_date']);
						$date_end = date_format($date_end,"Y-m-d");
						$date_end_time=strtotime($date_end);
												
						$date_period_start=date_create($_REQUEST['startdate_'.$i]);
						$date_period_start=date_format($date_period_start,"Y-m-d");
						$date_period_start_time=strtotime($date_period_start);
						
						if($_REQUEST['stopdate_'.$i]<>"")
							$date_period_end=date_create($_REQUEST['stopdate_'.$i]);
						else
							$date_period_end=date_create($_REQUEST['start_date']);
						
						$date_period_end=date_format($date_period_end,"Y-m-d");
						$date_period_end_time=strtotime($date_period_end);
						
						if ($date_start_time > $date_period_start_time)
							$compare_start_date = date_create($date_start);
						else
							$compare_start_date = date_create($date_period_start);
						
						if ($date_end_time < $date_period_end_time)
							$compare_end_date = date_create($date_end);
						else
							$compare_end_date = date_create($date_period_end);
						
						$compare_start_date=date_format($compare_start_date,"Y-m-d");
						$compare_end_date=date_format($compare_end_date,"Y-m-d");
						
						if(strtotime($compare_start_date)<strtotime($compare_end_date))
							//$days_diff=(strtotime($compare_end_date)-strtotime($compare_start_date))/(60*60*24*7);
							$days_diff=(strtotime($compare_end_date)-strtotime($compare_start_date))/(60*60*24);
						else
							$days_diff=0;

						
						$q1 = "select explain from drugs_frequency where drugs_frequency_id=".$_REQUEST['frequency_'.$i];
						$q_exec = pg_query($q1);
						$q_reason=pg_fetch_array($q_exec); 
						$trans=$q_reason['explain'];
						$weekdosage = $weekdosage + ($days_diff*$_REQUEST['dose_'.$i]*$trans)/90;
						
						if($compare_start_date<=$date_start_curr and $date_start_curr<=$compare_end_date)
							$currentdosage = $_REQUEST['dose_'.$i]*$trans;
					}					
				}
				else if ($_REQUEST['patient_cohort_drugs_type_'.$i]=='period_label')
				{
					$j++;
				}
			
				
			}
			
			//patient_lookup_drugs set last record
				if($i==$patient_cohort_drugs_numofrows)
				{
					//patient_lookup_drugs check if is the first else make insert or update
					if ($drugs_id<>"")
					{
						$patient_lookup_drugs_parameters[0]=$pat_id;
						$patient_lookup_drugs_parameters[1]=0;
						$patient_lookup_drugs_parameters[2]=$user_id;
						$patient_lookup_drugs_parameters[3]='now()';
						$patient_lookup_drugs_parameters[4]=$patient_cohort_id;
						$patient_lookup_drugs_parameters[5]=$patient_followup_id;
						$patient_lookup_drugs_parameters[6]=$_REQUEST['fumonthcohort'];
						$patient_lookup_drugs_parameters[7]=$_REQUEST['fumonthinclusion'];
						$patient_lookup_drugs_parameters[8]=$drugs_id;
						$patient_lookup_drugs_parameters[9]=$route;
						$patient_lookup_drugs_parameters[10]=$maintherapy;
						$patient_lookup_drugs_parameters[11]=round($weekdosage, 2);
						$patient_lookup_drugs_parameters[12]=1;
						$patient_lookup_drugs_parameters[13]=round($currentdosage, 2);
						
						$patient_lookup_drugs_table_names[0]='pat_id';
						$patient_lookup_drugs_table_names[1]='deleted';
						$patient_lookup_drugs_table_names[2]='editor_id';
						$patient_lookup_drugs_table_names[3]='edit_date';
						$patient_lookup_drugs_table_names[4]='patient_cohort_id';
						$patient_lookup_drugs_table_names[5]='patient_followup_id';
						$patient_lookup_drugs_table_names[6]='fumonth_cohort';
						$patient_lookup_drugs_table_names[7]='fumonth_inclusion';
						$patient_lookup_drugs_table_names[8]='drugs_id';
						$patient_lookup_drugs_table_names[9]='route';
						$patient_lookup_drugs_table_names[10]='maintherapy';
						$patient_lookup_drugs_table_names[11]='weekdosage';
						$patient_lookup_drugs_table_names[12]='drug_flag';
						$patient_lookup_drugs_table_names[13]='currentdosage';
						
						$patient_lookup_drugs_edit_name[0]='patient_followup_id';
						$patient_lookup_drugs_edit_id[0]=$patient_followup_id;
						$patient_lookup_drugs_sumbol[0]='=';
						$patient_lookup_drugs_edit_name[1]='drugs_id';
						$patient_lookup_drugs_edit_id[1]=$drugs_id;
						$patient_lookup_drugs_sumbol[1]='=';
						$patient_lookup_drugs_edit_name[2]='drug_flag';
						$patient_lookup_drugs_edit_id[2]=1;
						$patient_lookup_drugs_sumbol[2]='=';
						
						$q1 = "select patient_lookup_drugs_id from patient_lookup_drugs where deleted=0 and patient_followup_id=$patient_followup_id and drugs_id=$drugs_id and drug_flag=1";
						$q_exec = pg_query($q1);
						$q_reason=pg_fetch_array($q_exec); 
						$patient_lookup_drugs_id=$q_reason['patient_lookup_drugs_id'];
						
						if($patient_lookup_drugs_id=='')
						{
							$patient_lookup_drugs_id=insert('patient_lookup_drugs',$patient_lookup_drugs_table_names,$patient_lookup_drugs_parameters,'patient_lookup_drugs_id');
							if($patient_lookup_drugs_id!='')
								$patient_lookup_drugs_msg="ok";
						}
						else
							$patient_lookup_drugs_msg=update('patient_lookup_drugs',$patient_lookup_drugs_table_names,$patient_lookup_drugs_parameters,$patient_lookup_drugs_edit_name,$patient_lookup_drugs_edit_id,$patient_lookup_drugs_sumbol);
					}
					else
						$patient_lookup_drugs_msg="ok";
					
				}
		}
		
		if ($j==$i)
			$cohort_drugs_msg="ok";
		else
			$cohort_drugs_msg="nok";
		//DRUG TREATMENT  table-end
		
		//corticosteroids  table-start
		
		
		$j=1;
		$patient_corticosteroids_numofrows=$_REQUEST['patient_corticosteroids_numofrows'];
		$weekdosage_j="";
		$weekdosage_j1="";
		$weekdosage_j2="";
		$meter_j="0";
		$meter_j1="0";
		$meter_j2="0";
		
		for ($i=1;$i<=$patient_corticosteroids_numofrows;$i++)
		{
			$patient_corticosteroids_hidden=$_REQUEST['patient_corticosteroids_hidden_'.$i];
			if($patient_corticosteroids_hidden=='-1')
			{
				$patient_corticosteroids_id=$_REQUEST['patient_corticosteroids_id_'.$i];
				if($patient_corticosteroids_id<>"")
				{
					$patient_corticosteroids_hdn_parameters[0]=1;
					$patient_corticosteroids_hdn_names[0]='deleted';
					$patient_corticosteroids_hdn_edit_name[0]='patient_corticosteroids_id';
					$patient_corticosteroids_hdn_edit_id[0]=$patient_corticosteroids_id;
					$patient_corticosteroids_hdn_sumbol[0]='=';
					$patient_corticosteroids_msg=update('patient_corticosteroids',$patient_corticosteroids_hdn_names,$patient_corticosteroids_hdn_parameters,$patient_corticosteroids_hdn_edit_name,$patient_corticosteroids_hdn_edit_id,$patient_corticosteroids_hdn_sumbol);
					
					//patient_lookup_drugs delete
					/*$patient_cohort_drugs_hdn_parameters2[0]=1;
					$patient_cohort_drugs_hdn_names2[0]='deleted';
					$patient_cohort_drugs_hdn_edit_name2[0]='drugs_id';
					$patient_cohort_drugs_hdn_edit_id2[0]=$_REQUEST['drugs_id_'.$i];
					$patient_cohort_drugs_hdn_sumbol2[0]='=';
					$patient_cohort_drugs_hdn_edit_name2[1]='patient_followup_id';
					$patient_cohort_drugs_hdn_edit_id2[1]=$patient_followup_id;
					$patient_cohort_drugs_hdn_sumbol2[1]='=';
					$patient_cohort_drugs_hdn_edit_name2[2]='drug_flag';
					$patient_cohort_drugs_hdn_edit_id2[2]=2;
					$patient_cohort_drugs_hdn_sumbol2[2]='=';
					$patient_cohort_drugs_hdn_edit_name2[3]='fumonth_cohort';
					$patient_cohort_drugs_hdn_edit_id2[3]=$_REQUEST['fumonthcohort'];
					$patient_cohort_drugs_hdn_sumbol2[3]='=';	
					
					$patient_cohort_drugs_msg2=update('patient_lookup_drugs',$patient_cohort_drugs_hdn_names2,$patient_cohort_drugs_hdn_parameters2,$patient_cohort_drugs_hdn_edit_name2,$patient_cohort_drugs_hdn_edit_id2,$patient_cohort_drugs_hdn_sumbol2);
					*/
					if ($patient_cohort_drugs_msg=="ok")
						$j++;
					
				}
				else
					$j++;
				
			}
			else
			{
				$patient_corticosteroids_id=$_REQUEST['patient_corticosteroids_id_'.$i];
					
				$patient_corticosteroids_parameters[0]=$user_id;
				$patient_corticosteroids_parameters[1]='now()';
				$patient_corticosteroids_parameters[2]=$_REQUEST['corticosteroids_id_'.$i];
				$patient_corticosteroids_parameters[3]=$patient_cohort_id;
				$patient_corticosteroids_parameters[4]=date_for_postgres($_REQUEST['corticosteroids_date_'.$i]);
				$patient_corticosteroids_parameters[5]=$patient_followup_id;
				$patient_corticosteroids_parameters[6]=$pat_id;
				$patient_corticosteroids_parameters[7]=0;
																
				$patient_corticosteroids_table_names[0]='editor_id';
				$patient_corticosteroids_table_names[1]='edit_date';
				$patient_corticosteroids_table_names[2]='corticosteroids_id';
				$patient_corticosteroids_table_names[3]='patient_cohort_id';
				$patient_corticosteroids_table_names[4]='date';
				$patient_corticosteroids_table_names[5]='patient_followup_id';
				$patient_corticosteroids_table_names[6]='pat_id';
				$patient_corticosteroids_table_names[7]='deleted';
						
				$patient_corticosteroids_edit_name[0]='patient_corticosteroids_id';
				$patient_corticosteroids_edit_id[0]=$patient_corticosteroids_id;
				$patient_corticosteroids_sumbol[0]='=';
					
				if($patient_corticosteroids_id=='')
				{	
					$patient_corticosteroids_id=insert('patient_corticosteroids',$patient_corticosteroids_table_names,$patient_corticosteroids_parameters,'patient_corticosteroids_id');
					if($patient_corticosteroids_id!='')
						$j++;
				}
				else
				{
					$patient_corticosteroids_msg=update('patient_corticosteroids',$patient_corticosteroids_table_names,$patient_corticosteroids_parameters,$patient_corticosteroids_edit_name,$patient_corticosteroids_edit_id,$patient_corticosteroids_sumbol);
					if ($patient_corticosteroids_msg=="ok")
						$j++;
				}
				
				//patient_lookup_corticosteroids_drugs set weekdosage
				$date_start=date_create($_REQUEST['start_date']);
				$date_start = date_sub($date_start,date_interval_create_from_date_string("90 days"));
					
				$date_start=date_format($date_start,"Y-m-d");
				$date_start_time=strtotime($date_start);
					
				$date_end=date_create($_REQUEST['start_date']);
				$date_end = date_format($date_end,"Y-m-d");
				$date_end_time=strtotime($date_end);
					
				$corticosteroids_date=date_create($_REQUEST['corticosteroids_date_'.$i]);
				$corticosteroids_date=date_format($corticosteroids_date,"Y-m-d");
				$corticosteroids_date=strtotime($corticosteroids_date);
					
				$q1 = "select dose,code from corticosteroids where corticosteroids_id=".$_REQUEST['corticosteroids_id_'.$i];
				$q_exec = pg_query($q1);
				$q_reason=pg_fetch_array($q_exec); 
				$dose=$q_reason['dose'];
				$code=$q_reason['code'];
				
				
				
				if ( $date_start_time <= $corticosteroids_date and $corticosteroids_date<=$date_end_time )
				{
					if($code=="J")
					{
						$weekdosage_j=$weekdosage_j+$dose;
						$meter_j++;
					}
					else if($code=="J1")
					{
						$weekdosage_j1=$weekdosage_j1+$dose;
						$meter_j1++;
					}
					else if($code=="J2")
					{
						$weekdosage_j2=$weekdosage_j2+$dose;
						$meter_j2++;
					}
				}
			}
		}
		
		$j++;
		if($meter_j>0)
		{
			$patient_lookup_drugs_parameters[0]=$pat_id;
			$patient_lookup_drugs_parameters[1]=0;
			$patient_lookup_drugs_parameters[2]=$user_id;
			$patient_lookup_drugs_parameters[3]='now()';
			$patient_lookup_drugs_parameters[4]=$patient_cohort_id;
			$patient_lookup_drugs_parameters[5]=$patient_followup_id;
			$patient_lookup_drugs_parameters[6]=$_REQUEST['fumonthcohort'];
			$patient_lookup_drugs_parameters[7]=$_REQUEST['fumonthinclusion'];
			$patient_lookup_drugs_parameters[8]=0;
			$patient_lookup_drugs_parameters[9]=0;
			$patient_lookup_drugs_parameters[10]=0;
			$patient_lookup_drugs_parameters[11]=round($weekdosage_j, 2);
			$patient_lookup_drugs_parameters[12]=2;
						
						
			$patient_lookup_drugs_table_names[0]='pat_id';
			$patient_lookup_drugs_table_names[1]='deleted';
			$patient_lookup_drugs_table_names[2]='editor_id';
			$patient_lookup_drugs_table_names[3]='edit_date';
			$patient_lookup_drugs_table_names[4]='patient_cohort_id';
			$patient_lookup_drugs_table_names[5]='patient_followup_id';
			$patient_lookup_drugs_table_names[6]='fumonth_cohort';
			$patient_lookup_drugs_table_names[7]='fumonth_inclusion';
			$patient_lookup_drugs_table_names[8]='drugs_id';
			$patient_lookup_drugs_table_names[9]='route';
			$patient_lookup_drugs_table_names[10]='maintherapy';
			$patient_lookup_drugs_table_names[11]='weekdosage';
			$patient_lookup_drugs_table_names[12]='drug_flag';
						
			$patient_lookup_drugs_edit_name[0]='patient_followup_id';
			$patient_lookup_drugs_edit_id[0]=$patient_followup_id;
			$patient_lookup_drugs_sumbol[0]='=';
			$patient_lookup_drugs_edit_name[1]='drugs_id';
			$patient_lookup_drugs_edit_id[1]=0;
			$patient_lookup_drugs_sumbol[1]='=';
			$patient_lookup_drugs_edit_name[2]='drug_flag';
			$patient_lookup_drugs_edit_id[2]=2;
			$patient_lookup_drugs_sumbol[2]='=';
						
			$q1 = "select patient_lookup_drugs_id from patient_lookup_drugs where deleted=0 and patient_followup_id=$patient_followup_id and drugs_id=0 and drug_flag=2";
			$q_exec = pg_query($q1);
			$q_reason=pg_fetch_array($q_exec); 
			$patient_lookup_drugs_id=$q_reason['patient_lookup_drugs_id'];
					
			if($patient_lookup_drugs_id=='')
			{
				$patient_lookup_drugs_id=insert('patient_lookup_drugs',$patient_lookup_drugs_table_names,$patient_lookup_drugs_parameters,'patient_lookup_drugs_id');
				if($patient_lookup_drugs_id!='')
					$i++;
			}
			else
			{
				$patient_lookup_drugs_msg=update('patient_lookup_drugs',$patient_lookup_drugs_table_names,$patient_lookup_drugs_parameters,$patient_lookup_drugs_edit_name,$patient_lookup_drugs_edit_id,$patient_lookup_drugs_sumbol);
				if($patient_lookup_drugs_msg=='ok')
					$i++;		
			}
		}
		else
		{
			//patient_lookup_drugs delete
			$patient_cohort_drugs_hdn_parameters[0]=1;
			$patient_cohort_drugs_hdn_names[0]='deleted';
			$patient_cohort_drugs_hdn_edit_name[0]='drugs_id';
			$patient_cohort_drugs_hdn_edit_id[0]=0;
			$patient_cohort_drugs_hdn_sumbol[0]='=';
			$patient_cohort_drugs_hdn_edit_name[1]='patient_followup_id';
			$patient_cohort_drugs_hdn_edit_id[1]=$patient_followup_id;
			$patient_cohort_drugs_hdn_sumbol[1]='=';
			$patient_cohort_drugs_hdn_edit_name[2]='drug_flag';
			$patient_cohort_drugs_hdn_edit_id[2]=2;
			$patient_cohort_drugs_hdn_sumbol[2]='=';
			
						
			$patient_cohort_drugs_msg2=update('patient_lookup_drugs',$patient_cohort_drugs_hdn_names,$patient_cohort_drugs_hdn_parameters,$patient_cohort_drugs_hdn_edit_name,$patient_cohort_drugs_hdn_edit_id,$patient_cohort_drugs_hdn_sumbol);
			if ($patient_cohort_drugs_msg2=="ok")
				$i++;
		}
		
		$j++;
		if($meter_j1>0)
		{
			$patient_lookup_drugs_parameters[0]=$pat_id;
			$patient_lookup_drugs_parameters[1]=0;
			$patient_lookup_drugs_parameters[2]=$user_id;
			$patient_lookup_drugs_parameters[3]='now()';
			$patient_lookup_drugs_parameters[4]=$patient_cohort_id;
			$patient_lookup_drugs_parameters[5]=$patient_followup_id;
			$patient_lookup_drugs_parameters[6]=$_REQUEST['fumonthcohort'];
			$patient_lookup_drugs_parameters[7]=$_REQUEST['fumonthinclusion'];
			$patient_lookup_drugs_parameters[8]=1;
			$patient_lookup_drugs_parameters[9]=0;
			$patient_lookup_drugs_parameters[10]=0;
			$patient_lookup_drugs_parameters[11]=round($weekdosage_j1, 2);
			$patient_lookup_drugs_parameters[12]=2;
						
						
			$patient_lookup_drugs_table_names[0]='pat_id';
			$patient_lookup_drugs_table_names[1]='deleted';
			$patient_lookup_drugs_table_names[2]='editor_id';
			$patient_lookup_drugs_table_names[3]='edit_date';
			$patient_lookup_drugs_table_names[4]='patient_cohort_id';
			$patient_lookup_drugs_table_names[5]='patient_followup_id';
			$patient_lookup_drugs_table_names[6]='fumonth_cohort';
			$patient_lookup_drugs_table_names[7]='fumonth_inclusion';
			$patient_lookup_drugs_table_names[8]='drugs_id';
			$patient_lookup_drugs_table_names[9]='route';
			$patient_lookup_drugs_table_names[10]='maintherapy';
			$patient_lookup_drugs_table_names[11]='weekdosage';
			$patient_lookup_drugs_table_names[12]='drug_flag';
						
			$patient_lookup_drugs_edit_name[0]='patient_followup_id';
			$patient_lookup_drugs_edit_id[0]=$patient_followup_id;
			$patient_lookup_drugs_sumbol[0]='=';
			$patient_lookup_drugs_edit_name[1]='drugs_id';
			$patient_lookup_drugs_edit_id[1]=1;
			$patient_lookup_drugs_sumbol[1]='=';
			$patient_lookup_drugs_edit_name[2]='drug_flag';
			$patient_lookup_drugs_edit_id[2]=2;
			$patient_lookup_drugs_sumbol[2]='=';
			
			$q1 = "select patient_lookup_drugs_id from patient_lookup_drugs where deleted=0 and patient_followup_id=$patient_followup_id and drugs_id=1 and drug_flag=2";
			$q_exec = pg_query($q1);
			$q_reason=pg_fetch_array($q_exec); 
			$patient_lookup_drugs_id2=$q_reason['patient_lookup_drugs_id'];
					
			if($patient_lookup_drugs_id2=='')
			{
				$patient_lookup_drugs_id2=insert('patient_lookup_drugs',$patient_lookup_drugs_table_names,$patient_lookup_drugs_parameters,'patient_lookup_drugs_id');
				if($patient_lookup_drugs_id2!='')
					$i++;
			}
			else
			{	$patient_lookup_drugs_msg=update('patient_lookup_drugs',$patient_lookup_drugs_table_names,$patient_lookup_drugs_parameters,$patient_lookup_drugs_edit_name,$patient_lookup_drugs_edit_id,$patient_lookup_drugs_sumbol);
				if($patient_lookup_drugs_msg=='ok')
					$i++;		
			}
		}
		else
		{
			//patient_lookup_drugs delete
			$patient_cohort_drugs_hdn_parameters[0]=1;
			$patient_cohort_drugs_hdn_names[0]='deleted';
			$patient_cohort_drugs_hdn_edit_name[0]='drugs_id';
			$patient_cohort_drugs_hdn_edit_id[0]=1;
			$patient_cohort_drugs_hdn_sumbol[0]='=';
			$patient_cohort_drugs_hdn_edit_name[1]='patient_followup_id';
			$patient_cohort_drugs_hdn_edit_id[1]=$patient_followup_id;
			$patient_cohort_drugs_hdn_sumbol[1]='=';
			$patient_cohort_drugs_hdn_edit_name[2]='drug_flag';
			$patient_cohort_drugs_hdn_edit_id[2]=2;
			$patient_cohort_drugs_hdn_sumbol[2]='=';
						
			$patient_cohort_drugs_msg2=update('patient_lookup_drugs',$patient_cohort_drugs_hdn_names,$patient_cohort_drugs_hdn_parameters,$patient_cohort_drugs_hdn_edit_name,$patient_cohort_drugs_hdn_edit_id,$patient_cohort_drugs_hdn_sumbol);
			if ($patient_cohort_drugs_msg2=="ok")
				$i++;
		}
		
		$j++;
		if($meter_j2>0)
		{
			$patient_lookup_drugs_parameters[0]=$pat_id;
			$patient_lookup_drugs_parameters[1]=0;
			$patient_lookup_drugs_parameters[2]=$user_id;
			$patient_lookup_drugs_parameters[3]='now()';
			$patient_lookup_drugs_parameters[4]=$patient_cohort_id;
			$patient_lookup_drugs_parameters[5]=$patient_followup_id;
			$patient_lookup_drugs_parameters[6]=$_REQUEST['fumonthcohort'];
			$patient_lookup_drugs_parameters[7]=$_REQUEST['fumonthinclusion'];
			$patient_lookup_drugs_parameters[8]=2;
			$patient_lookup_drugs_parameters[9]=0;
			$patient_lookup_drugs_parameters[10]=0;
			$patient_lookup_drugs_parameters[11]=round($weekdosage_j2, 2);
			$patient_lookup_drugs_parameters[12]=2;
						
						
			$patient_lookup_drugs_table_names[0]='pat_id';
			$patient_lookup_drugs_table_names[1]='deleted';
			$patient_lookup_drugs_table_names[2]='editor_id';
			$patient_lookup_drugs_table_names[3]='edit_date';
			$patient_lookup_drugs_table_names[4]='patient_cohort_id';
			$patient_lookup_drugs_table_names[5]='patient_followup_id';
			$patient_lookup_drugs_table_names[6]='fumonth_cohort';
			$patient_lookup_drugs_table_names[7]='fumonth_inclusion';
			$patient_lookup_drugs_table_names[8]='drugs_id';
			$patient_lookup_drugs_table_names[9]='route';
			$patient_lookup_drugs_table_names[10]='maintherapy';
			$patient_lookup_drugs_table_names[11]='weekdosage';
			$patient_lookup_drugs_table_names[12]='drug_flag';
						
			$patient_lookup_drugs_edit_name[0]='patient_followup_id';
			$patient_lookup_drugs_edit_id[0]=$patient_followup_id;
			$patient_lookup_drugs_sumbol[0]='=';
			$patient_lookup_drugs_edit_name[1]='drugs_id';
			$patient_lookup_drugs_edit_id[1]=2;
			$patient_lookup_drugs_sumbol[1]='=';
			$patient_lookup_drugs_edit_name[2]='drug_flag';
			$patient_lookup_drugs_edit_id[2]=2;
			$patient_lookup_drugs_sumbol[2]='=';
			
			$q1 = "select patient_lookup_drugs_id from patient_lookup_drugs where deleted=0 and patient_followup_id=$patient_followup_id and drugs_id=2 and drug_flag=2";
			$q_exec = pg_query($q1);
			$q_reason=pg_fetch_array($q_exec); 
			$patient_lookup_drugs_id3=$q_reason['patient_lookup_drugs_id'];
					
			if($patient_lookup_drugs_id3=='')
			{
				$patient_lookup_drugs_id3=insert('patient_lookup_drugs',$patient_lookup_drugs_table_names,$patient_lookup_drugs_parameters,'patient_lookup_drugs_id');
				if($patient_lookup_drugs_id3!='')
					$i++;
			}
			else
			{
				$patient_lookup_drugs_msg=update('patient_lookup_drugs',$patient_lookup_drugs_table_names,$patient_lookup_drugs_parameters,$patient_lookup_drugs_edit_name,$patient_lookup_drugs_edit_id,$patient_lookup_drugs_sumbol);
				if($patient_lookup_drugs_msg=='ok')
					$i++;		

			}
		}
		else
		{
			//patient_lookup_drugs delete
			$patient_cohort_drugs_hdn_parameters[0]=1;
			$patient_cohort_drugs_hdn_names[0]='deleted';
			$patient_cohort_drugs_hdn_edit_name[0]='drugs_id';
			$patient_cohort_drugs_hdn_edit_id[0]=2;
			$patient_cohort_drugs_hdn_sumbol[0]='=';
			$patient_cohort_drugs_hdn_edit_name[1]='patient_followup_id';
			$patient_cohort_drugs_hdn_edit_id[1]=$patient_followup_id;
			$patient_cohort_drugs_hdn_sumbol[1]='=';
			$patient_cohort_drugs_hdn_edit_name[2]='drug_flag';
			$patient_cohort_drugs_hdn_edit_id[2]=2;
			$patient_cohort_drugs_hdn_sumbol[2]='=';
						
			$patient_cohort_drugs_msg2=update('patient_lookup_drugs',$patient_cohort_drugs_hdn_names,$patient_cohort_drugs_hdn_parameters,$patient_cohort_drugs_hdn_edit_name,$patient_cohort_drugs_hdn_edit_id,$patient_cohort_drugs_hdn_sumbol);
			if ($patient_cohort_drugs_msg2=="ok")
				$i++;
		}
		
		
		if ($j==$i)	
			$cohort_corticosteroids_msg="ok";
		else
			$cohort_corticosteroids_msg="nok";
		//corticosteroids  table-end
		
		//analgesics  table-start
		
		//patient_lookup_drugs delete
		$patient_cohort_drugs_hdn_parameters2[0]=1;
		$patient_cohort_drugs_hdn_names2[0]='deleted';
		$patient_cohort_drugs_hdn_edit_name2[0]='patient_followup_id';
		$patient_cohort_drugs_hdn_edit_id2[0]=$patient_followup_id;
		$patient_cohort_drugs_hdn_sumbol2[0]='=';
		$patient_cohort_drugs_hdn_edit_name2[1]='drug_flag';
		$patient_cohort_drugs_hdn_edit_id2[1]=3;
		$patient_cohort_drugs_hdn_sumbol2[1]='=';
						
		$patient_cohort_drugs_msg2=update('patient_lookup_drugs',$patient_cohort_drugs_hdn_names2,$patient_cohort_drugs_hdn_parameters2,$patient_cohort_drugs_hdn_edit_name2,$patient_cohort_drugs_hdn_edit_id2,$patient_cohort_drugs_hdn_sumbol2);
		
		$j=1;
		$j1=1;
		$j2=1;
		$patient_analgesics_numofrows=$_REQUEST['patient_analgesics_numofrows'];
		for ($i=1;$i<=$patient_analgesics_numofrows;$i++)
		{
			$patient_analgesics_hidden=$_REQUEST['patient_analgesics_hidden_'.$i];
			if($patient_analgesics_hidden=='-1')
			{
				$patient_analgesics_id=$_REQUEST['patient_analgesics_id_'.$i];
				if($patient_analgesics_id<>"")
				{
					$patient_analgesics_hdn_parameters[0]=1;
					$patient_analgesics_hdn_names[0]='deleted';
					$patient_analgesics_hdn_edit_name[0]='patient_analgesics_id';
					$patient_analgesics_hdn_edit_id[0]=$patient_analgesics_id;
					$patient_analgesics_hdn_sumbol[0]='=';
					$patient_analgesics_msg=update('patient_analgesics',$patient_analgesics_hdn_names,$patient_analgesics_hdn_parameters,$patient_analgesics_hdn_edit_name,$patient_analgesics_hdn_edit_id,$patient_analgesics_hdn_sumbol);
					if ($patient_cohort_drugs_msg=="ok")
						$j++;
				}
				else
					$j++;
				
			}
			else
			{
				$patient_analgesics_id=$_REQUEST['patient_analgesics_id_'.$i];
					
					
					$patient_analgesics_parameters[0]=$user_id;
					$patient_analgesics_parameters[1]='now()';
					$patient_analgesics_parameters[2]=$_REQUEST['analgesics_id_'.$i];
					$patient_analgesics_parameters[3]=$patient_cohort_id;
					$patient_analgesics_parameters[4]=date_for_postgres($_REQUEST['startdate_analgesics_'.$i]);
					$patient_analgesics_parameters[5]=date_for_postgres($_REQUEST['stopdate_analgesics_'.$i]);
					$patient_analgesics_parameters[6]=$_REQUEST['intake_'.$i];
					$patient_analgesics_parameters[7]=$patient_followup_id;
					$patient_analgesics_parameters[8]=$pat_id;
					$patient_analgesics_parameters[9]=0;
																
					$patient_analgesics_table_names[0]='editor_id';
					$patient_analgesics_table_names[1]='edit_date';
					$patient_analgesics_table_names[2]='analgesics_id';
					$patient_analgesics_table_names[3]='patient_cohort_id';
					$patient_analgesics_table_names[4]='startdate';
					$patient_analgesics_table_names[5]='stopdate';
					$patient_analgesics_table_names[6]='intake';
					$patient_analgesics_table_names[7]='patient_followup_id';
					$patient_analgesics_table_names[8]='pat_id';
					$patient_analgesics_table_names[9]='deleted';
						
					$patient_analgesics_edit_name[0]='patient_analgesics_id';
					$patient_analgesics_edit_id[0]=$patient_analgesics_id;
					$patient_analgesics_sumbol[0]='=';
					
					if($patient_analgesics_id=='')
					{	
						$patient_analgesics_id=insert('patient_analgesics',$patient_analgesics_table_names,$patient_analgesics_parameters,'patient_analgesics_id');
						if($patient_analgesics_id!='')
							$j++;
					}
					else
					{
						$patient_analgesics_msg=update('patient_analgesics',$patient_analgesics_table_names,$patient_analgesics_parameters,$patient_analgesics_edit_name,$patient_analgesics_edit_id,$patient_analgesics_sumbol);
						if ($patient_analgesics_msg=="ok")
							$j++;
					}
					
					//patient_lookup_drugs set weekdosage
					if($_REQUEST['start_date']<>"" and $_REQUEST['startdate_analgesics_'.$i]<>"")
					{
						$date_start=date_create($_REQUEST['start_date']);
						$date_start = date_sub($date_start,date_interval_create_from_date_string("90 days"));
						
						$date_start=date_format($date_start,"Y-m-d");
						$date_start_time=strtotime($date_start);
						
						
						$date_end=date_create($_REQUEST['start_date']);
						$date_end = date_format($date_end,"Y-m-d");
						$date_end_time=strtotime($date_end);
						
						
						$date_period_start=date_create($_REQUEST['startdate_analgesics_'.$i]);
						$date_period_start=date_format($date_period_start,"Y-m-d");
						$date_period_start_time=strtotime($date_period_start);
						
						if($_REQUEST['stopdate_analgesics_'.$i]<>"")
							$date_period_end=date_create($_REQUEST['stopdate_analgesics_'.$i]);
						else
							$date_period_end=date_create($_REQUEST['start_date']);
						
						
						$date_period_end=date_format($date_period_end,"Y-m-d");
						$date_period_end_time=strtotime($date_period_end);
						
						if ($date_start_time > $date_period_start_time)
							$compare_start_date = date_create($date_start);
						else
							$compare_start_date = date_create($date_period_start);
						
						if ($date_end_time < $date_period_end_time)
							$compare_end_date = date_create($date_end);
						else
							$compare_end_date = date_create($date_period_end);
						
						$compare_start_date=date_format($compare_start_date,"Y-m-d");
						$compare_end_date=date_format($compare_end_date,"Y-m-d");
						
						if(strtotime($compare_start_date)<strtotime($compare_end_date))
							//$days_diff=(strtotime($compare_end_date)-strtotime($compare_start_date))/(60*60*24*7);
							$days_diff=(strtotime($compare_end_date)-strtotime($compare_start_date))/(60*60*24);
						else
							$days_diff=0;

						
						$q1 = "select dose,max_dose from analgesics where analgesics_id=".$_REQUEST['analgesics_id_'.$i];
						$q_exec = pg_query($q1);
						$q_reason=pg_fetch_array($q_exec); 
						$dose=$q_reason['dose'];
						$max_dose=$q_reason['max_dose'];
						$factor=(100*$dose)/$max_dose;
						if($_REQUEST['intake_'.$i]==0 or  $_REQUEST['intake_'.$i]==1)
							$n_factor=0;
						else if($_REQUEST['intake_'.$i]==2)
							$n_factor=0.5/7;
						else if($_REQUEST['intake_'.$i]==3)
							$n_factor=2/7;
						else if($_REQUEST['intake_'.$i]==4)
							$n_factor=4/7;
						else if($_REQUEST['intake_'.$i]==5)
							$n_factor=6/7;
						else if($_REQUEST['intake_'.$i]==6)
							$n_factor=1;
						
						$weekdosage = ($factor*$days_diff*$n_factor)/90;
						
						$patient_lookup_drugs_id="";
						$drugs_id=$_REQUEST['analgesics_id_'.$i];
						
						$patient_lookup_drugs_parameters[0]=$pat_id;
						$patient_lookup_drugs_parameters[1]=0;
						$patient_lookup_drugs_parameters[2]=$user_id;
						$patient_lookup_drugs_parameters[3]='now()';
						$patient_lookup_drugs_parameters[4]=$patient_cohort_id;
						$patient_lookup_drugs_parameters[5]=$patient_followup_id;
						$patient_lookup_drugs_parameters[6]=$_REQUEST['fumonthcohort'];
						$patient_lookup_drugs_parameters[7]=$_REQUEST['fumonthinclusion'];
						$patient_lookup_drugs_parameters[8]=$drugs_id;
						$patient_lookup_drugs_parameters[9]=0;
						$patient_lookup_drugs_parameters[10]=0;
						$patient_lookup_drugs_parameters[11]=round($weekdosage, 2);
						$patient_lookup_drugs_parameters[12]=3;
						
						
						$patient_lookup_drugs_table_names[0]='pat_id';
						$patient_lookup_drugs_table_names[1]='deleted';
						$patient_lookup_drugs_table_names[2]='editor_id';
						$patient_lookup_drugs_table_names[3]='edit_date';
						$patient_lookup_drugs_table_names[4]='patient_cohort_id';
						$patient_lookup_drugs_table_names[5]='patient_followup_id';
						$patient_lookup_drugs_table_names[6]='fumonth_cohort';
						$patient_lookup_drugs_table_names[7]='fumonth_inclusion';
						$patient_lookup_drugs_table_names[8]='drugs_id';
						$patient_lookup_drugs_table_names[9]='route';
						$patient_lookup_drugs_table_names[10]='maintherapy';
						$patient_lookup_drugs_table_names[11]='weekdosage';
						$patient_lookup_drugs_table_names[12]='drug_flag';
						
						$patient_lookup_drugs_edit_name[0]='patient_followup_id';
						$patient_lookup_drugs_edit_id[0]=$patient_followup_id;
						$patient_lookup_drugs_sumbol[0]='=';
						$patient_lookup_drugs_edit_name[1]='drugs_id';
						$patient_lookup_drugs_edit_id[1]=$drugs_id;
						$patient_lookup_drugs_sumbol[1]='=';
						
						$q1 = "select patient_lookup_drugs_id from patient_lookup_drugs where deleted=0 and patient_followup_id=$patient_followup_id and drugs_id=$drugs_id and drug_flag=3";
						$q_exec = pg_query($q1);
						$q_reason=pg_fetch_array($q_exec); 
						$patient_lookup_drugs_id=$q_reason['patient_lookup_drugs_id'];
						
						$j1++;
						if($patient_lookup_drugs_id=='')
						{
							$patient_lookup_drugs_id=insert('patient_lookup_drugs',$patient_lookup_drugs_table_names,$patient_lookup_drugs_parameters,'patient_lookup_drugs_id');
							if($patient_lookup_drugs_id!='')
								$j2++;
						}
						else
							$patient_lookup_drugs_msg=update('patient_lookup_drugs',$patient_lookup_drugs_table_names,$patient_lookup_drugs_parameters,$patient_lookup_drugs_edit_name,$patient_lookup_drugs_edit_id,$patient_lookup_drugs_sumbol);
							if($patient_lookup_drugs_msg=="ok")
								$j2++;
						
					}
			}
		}
		
		if ($j==$i)
			$cohort_analgesics_msg="ok";
		else
			$cohort_analgesics_msg="nok";
		
		//analgesics  table-end
		
		//euroqol table-start
		if($_REQUEST['euroqol']==2)
		{	
			$parameters_euroqol[0]=$patient_followup_id;
			$table_names_euroqol[0]='patient_followup_id';
			$parameters_euroqol[1]=$pat_id;
			$table_names_euroqol[1]='pat_id';
			$parameters_euroqol[2]=0;
			$table_names_euroqol[2]='deleted';
			$parameters_euroqol[3]=$_REQUEST['eq1'];
			$table_names_euroqol[3]='eq1';
			$parameters_euroqol[4]=$_REQUEST['eq2'];
			$table_names_euroqol[4]='eq2';
			$parameters_euroqol[5]=$_REQUEST['eq3'];
			$table_names_euroqol[5]='eq3';
			$parameters_euroqol[6]=$_REQUEST['eq4'];
			$table_names_euroqol[6]='eq4';
			$parameters_euroqol[7]=$_REQUEST['eq5'];
			$table_names_euroqol[7]='eq5';
			$parameters_euroqol[8]=$_REQUEST['eq6'];
			$table_names_euroqol[8]='eq6';
			$parameters_euroqol[9]=$_REQUEST['euroqol_score'];
			if ($parameters_euroqol[9]=="")
				$parameters_euroqol[9]=11111;
			$table_names_euroqol[9]='euroqol_score';
			$parameters_euroqol[10]=0;
			$table_names_euroqol[10]='asqol';
			$parameters_euroqol[11]=0;
			$table_names_euroqol[11]='dlqi';
			$parameters_euroqol[12]=$_REQUEST['eq5d'];
			if ($parameters_euroqol[12]=="")
				$parameters_euroqol[12]=0;
			$table_names_euroqol[12]='eq5d';
			
			
			
			$edit_name_euroqol[0]='patient_euroqol_id';
			$edit_id_euroqol[0]=$_REQUEST['patient_euroqol_id'];
			$sumbol_euroqol[0]='=';
			if($_REQUEST['patient_euroqol_id']=='')
			{
				$patient_euroqol_id=insert('patient_euroqol',$table_names_euroqol,$parameters_euroqol,'patient_euroqol_id');
				if($patient_euroqol_id!='')
					$msg_euroqol="ok";
			}
			else
			{
				$msg_euroqol=update('patient_euroqol',$table_names_euroqol,$parameters_euroqol,$edit_name_euroqol,$edit_id_euroqol,$sumbol_euroqol);
			}
		}
		else
		{
			if($_REQUEST['patient_euroqol_id']<>"")
			{
				$parameters_deleted[0]=1;
				$table_names_deleted[0]='deleted';
				$edit_name_deleted[0]='patient_euroqol_id';
				$edit_id_deleted[0]=$_REQUEST['patient_euroqol_id'];
				$sumbol_deleted[0]='=';
				$msg_euroqol=update('patient_euroqol',$table_names_deleted,$parameters_deleted,$edit_name_deleted,$edit_id_deleted,$sumbol_deleted);
			}
			else
				$msg_euroqol="ok";
		}
		
		//euroqol table-end
		
		//patient_wpai table-start
		if($_REQUEST['wpai']==2)
		{
			$parameters_wpai[0]=$patient_followup_id;
			$table_names_wpai[0]='patient_followup_id';
			$parameters_wpai[1]=$pat_id;
			$table_names_wpai[1]='pat_id';
			$parameters_wpai[2]=0;
			$table_names_wpai[2]='deleted';
			$parameters_wpai[3]=$_REQUEST['wpai1'];
			$table_names_wpai[3]='wpai1';
			$parameters_wpai[4]=$_REQUEST['wpai2'];
			$table_names_wpai[4]='wpai2';
			$parameters_wpai[5]=$_REQUEST['wpai3'];
			$table_names_wpai[5]='wpai3';
			$parameters_wpai[6]=$_REQUEST['wpai4'];
			$table_names_wpai[6]='wpai4';
			$parameters_wpai[7]=$_REQUEST['wpai5'];
			$table_names_wpai[7]='wpai5';
			$parameters_wpai[8]=$_REQUEST['wpai6'];
			$table_names_wpai[8]='wpai6';
			
			
			$edit_name_wpai[0]='patient_wpai_id';
			$edit_id_wpai[0]=$_REQUEST['patient_wpai_id'];
			$sumbol_wpai[0]='=';
			if($_REQUEST['patient_wpai_id']=='')
			{
				$patient_wpai_id=insert('patient_wpai',$table_names_wpai,$parameters_wpai,'patient_wpai_id');
				if($patient_wpai_id!='')
					$msg_wpai="ok";
			}
			else
			{
				$msg_wpai=update('patient_wpai',$table_names_wpai,$parameters_wpai,$edit_name_wpai,$edit_id_wpai,$sumbol_wpai);
			}
		}
		else
		{
			if($_REQUEST['patient_wpai_id']<>"")
			{
				$parameters_deleted_wpai[0]=1;
				$table_names_deleted_wpai[0]='deleted';
				$edit_name_deleted_wpai[0]='patient_wpai_id';
				$edit_id_deleted_wpai[0]=$_REQUEST['patient_wpai_id'];
				$sumbol_deleted_wpai[0]='=';
				$msg_wpai=update('patient_wpai',$table_names_deleted_wpai,$parameters_deleted_wpai,$edit_name_deleted_wpai,$edit_id_deleted_wpai,$sumbol_deleted_wpai);
			}
			else
				$msg_wpai="ok";
		}
		
		//patient_wpai table-end
		
		//patient_hads table-start
		if($_REQUEST['hads']==2)
		{
			$parameters_hads[0]=$patient_followup_id;
			$table_names_hads[0]='patient_followup_id';
			$parameters_hads[1]=$pat_id;
			$table_names_hads[1]='pat_id';
			$parameters_hads[2]=0;
			$table_names_hads[2]='deleted';
			$parameters_hads[3]=$_REQUEST['hads1'];
			$table_names_hads[3]='hads1';
			$parameters_hads[4]=$_REQUEST['hads2'];
			$table_names_hads[4]='hads2';
			$parameters_hads[5]=$_REQUEST['hads3'];
			$table_names_hads[5]='hads3';
			$parameters_hads[6]=$_REQUEST['hads4'];
			$table_names_hads[6]='hads4';
			$parameters_hads[7]=$_REQUEST['hads5'];
			$table_names_hads[7]='hads5';
			$parameters_hads[8]=$_REQUEST['hads6'];
			$table_names_hads[8]='hads6';
			$parameters_hads[9]=$_REQUEST['hads7'];
			$table_names_hads[9]='hads7';
			$parameters_hads[10]=$_REQUEST['hads8'];
			$table_names_hads[10]='hads8';
			$parameters_hads[11]=$_REQUEST['hads9'];
			$table_names_hads[11]='hads9';
			$parameters_hads[12]=$_REQUEST['hads10'];
			$table_names_hads[12]='hads10';
			$parameters_hads[13]=$_REQUEST['hads11'];
			$table_names_hads[13]='hads11';
			$parameters_hads[14]=$_REQUEST['hads12'];
			$table_names_hads[14]='hads12';
			$parameters_hads[15]=$_REQUEST['hads13'];
			$table_names_hads[15]='hads13';
			$parameters_hads[16]=$_REQUEST['hads14'];
			$table_names_hads[16]='hads14';
			
			
			$edit_name_hads[0]='patient_hads_id';
			$edit_id_hads[0]=$_REQUEST['patient_hads_id'];
			$sumbol_hads[0]='=';
			if($_REQUEST['patient_hads_id']=='')
			{
				$patient_hads_id=insert('patient_hads',$table_names_hads,$parameters_hads,'patient_hads_id');
				if($patient_hads_id!='')
					$msg_hads="ok";
			}
			else
			{
				$msg_hads=update('patient_hads',$table_names_hads,$parameters_hads,$edit_name_hads,$edit_id_hads,$sumbol_hads);
			}
		}
		else
		{
			if($_REQUEST['patient_hads_id']<>"")
			{
				$parameters_deleted_hads[0]=1;
				$table_names_deleted_hads[0]='deleted';
				$edit_name_deleted_hads[0]='patient_hads_id';
				$edit_id_deleted_hads[0]=$_REQUEST['patient_hads_id'];
				$sumbol_deleted_hads[0]='=';
				$msg_hads=update('patient_hads',$table_names_deleted_hads,$parameters_deleted_hads,$edit_name_deleted_hads,$edit_id_deleted_hads,$sumbol_deleted_hads);
			}
			else
				$msg_hads="ok";
		}
		//patient_hads table-end
		
		//patient_sf36 table-start
		if($_REQUEST['sf36']==2)
		{
			$parameters_sf36[0]=$patient_followup_id;
			$table_names_sf36[0]='patient_followup_id';
			$parameters_sf36[1]=$pat_id;
			$table_names_sf36[1]='pat_id';
			$parameters_sf36[2]=0;
			$table_names_sf36[2]='deleted';
			$parameters_sf36[3]=$_REQUEST['sf361'];
			$table_names_sf36[3]='sf361';
			$parameters_sf36[4]=$_REQUEST['sf362'];
			$table_names_sf36[4]='sf362';
			$parameters_sf36[5]=$_REQUEST['sf363a'];
			$table_names_sf36[5]='sf363a';
			$parameters_sf36[6]=$_REQUEST['sf363b'];
			$table_names_sf36[6]='sf363b';
			$parameters_sf36[7]=$_REQUEST['sf363c'];
			$table_names_sf36[7]='sf363c';
			$parameters_sf36[8]=$_REQUEST['sf363d'];
			$table_names_sf36[8]='sf363d';
			$parameters_sf36[9]=$_REQUEST['sf363e'];
			$table_names_sf36[9]='sf363e';
			$parameters_sf36[10]=$_REQUEST['sf363f'];
			$table_names_sf36[10]='sf363f';
			$parameters_sf36[11]=$_REQUEST['sf363g'];
			$table_names_sf36[11]='sf363g';
			$parameters_sf36[12]=$_REQUEST['sf363h'];
			$table_names_sf36[12]='sf363h';
			$parameters_sf36[13]=$_REQUEST['sf363i'];
			$table_names_sf36[13]='sf363i';
			$parameters_sf36[14]=$_REQUEST['sf363j'];
			$table_names_sf36[14]='sf363j';
			$parameters_sf36[15]=$_REQUEST['sf364a'];
			$table_names_sf36[15]='sf364a';
			$parameters_sf36[16]=$_REQUEST['sf364b'];
			$table_names_sf36[16]='sf364b';
			$parameters_sf36[17]=$_REQUEST['sf364c'];
			$table_names_sf36[17]='sf364c';
			$parameters_sf36[18]=$_REQUEST['sf364d'];
			$table_names_sf36[18]='sf364d';
			$parameters_sf36[19]=$_REQUEST['sf365a'];
			$table_names_sf36[19]='sf365a';
			$parameters_sf36[20]=$_REQUEST['sf365b'];
			$table_names_sf36[20]='sf365b';
			$parameters_sf36[21]=$_REQUEST['sf365c'];
			$table_names_sf36[21]='sf365c';
			$parameters_sf36[22]=$_REQUEST['sf366'];
			$table_names_sf36[22]='sf366';
			$parameters_sf36[23]=$_REQUEST['sf367'];
			$table_names_sf36[23]='sf367';
			$parameters_sf36[24]=$_REQUEST['sf368'];
			$table_names_sf36[24]='sf368';
			$parameters_sf36[25]=$_REQUEST['sf369a'];
			$table_names_sf36[25]='sf369a';
			$parameters_sf36[26]=$_REQUEST['sf369b'];
			$table_names_sf36[26]='sf369b';
			$parameters_sf36[27]=$_REQUEST['sf369c'];
			$table_names_sf36[27]='sf369c';
			$parameters_sf36[28]=$_REQUEST['sf369d'];
			$table_names_sf36[28]='sf369d';
			$parameters_sf36[29]=$_REQUEST['sf369e'];
			$table_names_sf36[29]='sf369e';
			$parameters_sf36[30]=$_REQUEST['sf369f'];
			$table_names_sf36[30]='sf369f';
			$parameters_sf36[31]=$_REQUEST['sf369g'];
			$table_names_sf36[31]='sf369g';
			$parameters_sf36[32]=$_REQUEST['sf369h'];
			$table_names_sf36[32]='sf369h';
			$parameters_sf36[33]=$_REQUEST['sf369i'];
			$table_names_sf36[33]='sf369i';
			$parameters_sf36[34]=$_REQUEST['sf3610'];
			$table_names_sf36[34]='sf3610';
			$parameters_sf36[35]=$_REQUEST['sf3611a'];
			$table_names_sf36[35]='sf3611a';
			$parameters_sf36[36]=$_REQUEST['sf3611b'];
			$table_names_sf36[36]='sf3611b';
			$parameters_sf36[37]=$_REQUEST['sf3611c'];
			$table_names_sf36[37]='sf3611c';
			$parameters_sf36[38]=$_REQUEST['sf3611d'];
			$table_names_sf36[38]='sf3611d';
			
			
			$edit_name_sf36[0]='patient_sf36_id';
			$edit_id_sf36[0]=$_REQUEST['patient_sf36_id'];
			$sumbol_sf36[0]='=';
			if($_REQUEST['patient_sf36_id']=='')
			{
				$patient_sf36_id=insert('patient_sf36',$table_names_sf36,$parameters_sf36,'patient_sf36_id');
				if($patient_sf36_id!='')
					$msg_sf36="ok";
			}
			else
			{
				$msg_sf36=update('patient_sf36',$table_names_sf36,$parameters_sf36,$edit_name_sf36,$edit_id_sf36,$sumbol_sf36);
			}
		}
		else
		{
			if($_REQUEST['patient_sf36_id']<>"")
			{
				$parameters_deleted_sf36[0]=1;
				$table_names_deleted_sf36[0]='deleted';
				$edit_name_deleted_sf36[0]='patient_sf36_id';
				$edit_id_deleted_sf36[0]=$_REQUEST['patient_sf36_id'];
				$sumbol_deleted_sf36[0]='=';
				$msg_sf36=update('patient_sf36',$table_names_deleted_sf36,$parameters_deleted_sf36,$edit_name_deleted_sf36,$edit_id_deleted_sf36,$sumbol_deleted_sf36);
			}
			else
				$msg_sf36="ok";
		}
	
		//patient_sf36 table-end
													
		//common table-start
		if($_REQUEST['common']==2)
		{	
			$parameters_common[0]=$patient_followup_id;
			$table_names_common[0]='patient_followup_id';
			$parameters_common[1]=$pat_id;
			$table_names_common[1]='pat_id';
			$parameters_common[2]=0;
			$table_names_common[2]='deleted';
			$parameters_common[3]=$_REQUEST['vaspain'];
			$table_names_common[3]='vaspain';
			$parameters_common[4]=$_REQUEST['vasglobal'];
			$table_names_common[4]='vasglobal';
			$parameters_common[5]=$_REQUEST['vasfatigue'];
			$table_names_common[5]='vasfatigue';
			$parameters_common[6]=$_REQUEST['vasphysicial'];
			$table_names_common[6]='vasphysicial';
			$parameters_common[7]=$_REQUEST['swollenjc28'];
			$table_names_common[7]='swollenjc28';
			$parameters_common[8]=$_REQUEST['tenderjc28'];
			$table_names_common[8]='tenderjc28';
			$parameters_common[9]=$_REQUEST['swollenjc44'];
			$table_names_common[9]='swollenjc44';
			$parameters_common[10]=$_REQUEST['tenderjc44'];
			$table_names_common[10]='tenderjc44';
			$parameters_common[11]=$_REQUEST['swollenjc66'];
			$table_names_common[11]='swollenjc66';
			$parameters_common[12]=$_REQUEST['tenderjc68'];
			$table_names_common[12]='tenderjc68';
			$parameters_common[13]=$_REQUEST['esr'];
			$table_names_common[13]='esr';
			$parameters_common[14]=$_REQUEST['crp'];
			$table_names_common[14]='crp';
			$parameters_common[15]=$_REQUEST['haq'];
			$table_names_common[15]='haq';
			$parameters_common[16]=$_REQUEST['mhaq'];
			$table_names_common[16]='mhaq';
			$parameters_common[17]=$_REQUEST['das28esr'];
			$table_names_common[17]='das28esr';
			$parameters_common[18]=$_REQUEST['das28esr3v'];
			$table_names_common[18]='das28esr3v';
			$parameters_common[19]=$_REQUEST['das28crp'];
			$table_names_common[19]='das28crp';
			$parameters_common[20]=$_REQUEST['das28crp3v'];
			$table_names_common[20]='das28crp3v';
			$parameters_common[21]=$_REQUEST['sdai'];
			$table_names_common[21]='sdai';
			$parameters_common[22]=$_REQUEST['cdai'];
			$table_names_common[22]='cdai';
			$parameters_common[23]=$_REQUEST['weight'];
			$table_names_common[23]='weight';
			$parameters_common[24]=$_REQUEST['height'];
			$table_names_common[24]='height';
			$parameters_common[25]=$_REQUEST['bmi'];
			$table_names_common[25]='bmi';
			
			
			$edit_name_common[0]='patient_common_id';
			$edit_id_common[0]=$_REQUEST['patient_common_id'];
			$sumbol_common[0]='=';
			if($_REQUEST['patient_common_id']=='')
			{
				
				$patient_common_id=insert('patient_common',$table_names_common,$parameters_common,'patient_common_id');
				if($patient_common_id!='')
					$msg_common="ok";
			}
			else
			{
				$msg_common=update('patient_common',$table_names_common,$parameters_common,$edit_name_common,$edit_id_common,$sumbol_common);
				echo "msg_common:$msg_common ";
			}
		}
		else
		{
			if($_REQUEST['patient_common_id']<>"")
			{
				$parameters_deleted[0]=1;
				$table_names_deleted[0]='deleted';
				$edit_name_deleted[0]='patient_common_id';
				$edit_id_deleted[0]=$_REQUEST['patient_common_id'];
				$sumbol_deleted[0]='=';
				$msg_common=update('patient_common',$table_names_deleted,$parameters_deleted,$edit_name_deleted,$edit_id_deleted,$sumbol_deleted);
				
			}
			else
			{
				$msg_common="ok";
			}
		}
		
		//common table-end
		
		//spa table-start
		 if($_REQUEST['spa']==2)
		{	
			$parameters_spa[0]=$patient_followup_id;
			$table_names_spa[0]='patient_followup_id';
			$parameters_spa[1]=$pat_id;
			$table_names_spa[1]='pat_id';
			$parameters_spa[2]=0;
			$table_names_spa[2]='deleted';
			$parameters_spa[3]=$_REQUEST['basdai'];
			$table_names_spa[3]='basdai';
			$parameters_spa[4]=$_REQUEST['basdai1'];
			$table_names_spa[4]='basdai1';
			$parameters_spa[5]=$_REQUEST['basdai2'];
			$table_names_spa[5]='basdai2';
			$parameters_spa[6]=$_REQUEST['basdai3'];
			$table_names_spa[6]='basdai3';
			$parameters_spa[7]=$_REQUEST['basdai4'];
			$table_names_spa[7]='basdai4';
			$parameters_spa[8]=$_REQUEST['basdai5'];
			$table_names_spa[8]='basdai5';
			$parameters_spa[9]=$_REQUEST['basdai6'];
			$table_names_spa[9]='basdai6';
			$parameters_spa[10]=$_REQUEST['basfi'];
			$table_names_spa[10]='basfi';
			$parameters_spa[11]=$_REQUEST['basfi1'];
			$table_names_spa[11]='basfi1';
			$parameters_spa[12]=$_REQUEST['basfi2'];
			$table_names_spa[12]='basfi2';
			$parameters_spa[13]=$_REQUEST['basfi3'];
			$table_names_spa[13]='basfi3';
			$parameters_spa[14]=$_REQUEST['basfi4'];
			$table_names_spa[14]='basfi4';
			$parameters_spa[15]=$_REQUEST['basfi5'];
			$table_names_spa[15]='basfi5';
			$parameters_spa[16]=$_REQUEST['basfi6'];
			$table_names_spa[16]='basfi6';
			$parameters_spa[17]=$_REQUEST['basfi7'];
			$table_names_spa[17]='basfi7';
			$parameters_spa[18]=$_REQUEST['basfi8'];
			$table_names_spa[18]='basfi8';
			$parameters_spa[19]=$_REQUEST['basfi9'];
			$table_names_spa[19]='basfi9';
			$parameters_spa[20]=$_REQUEST['basfi10'];
			$table_names_spa[20]='basfi10';
			$parameters_spa[21]=$_REQUEST['asdascrp'];
			$table_names_spa[21]='asdascrp';
			$parameters_spa[22]=$_REQUEST['asdasesr'];
			$table_names_spa[22]='asdasesr';
			$parameters_spa[23]=$_REQUEST['sparest1'];
			$table_names_spa[23]='sparest1';
			$parameters_spa[24]=$_REQUEST['sparest2'];
			$table_names_spa[24]='sparest2';
			$parameters_spa[25]=$_REQUEST['sparest3'];
			$table_names_spa[25]='sparest3';
			$parameters_spa[26]=$_REQUEST['sparest4'];
			$table_names_spa[26]='sparest4';
			$parameters_spa[27]=$_REQUEST['sparest5'];
			$table_names_spa[27]='sparest5';
			$parameters_spa[28]=$_REQUEST['sparest6'];
			$table_names_spa[28]='sparest6';
			$parameters_spa[29]=$_REQUEST['sparest7'];
			$table_names_spa[29]='sparest7';
			$parameters_spa[30]=$_REQUEST['sparest8'];
			$table_names_spa[30]='sparest8';
			$parameters_spa[31]=$_REQUEST['sparest9'];
			$table_names_spa[31]='sparest9';
			$parameters_spa[32]=$_REQUEST['sparest10'];
			$table_names_spa[32]='sparest10';
			
			$edit_name_spa[0]='patient_spa_id';
			$edit_id_spa[0]=$_REQUEST['patient_spa_id'];
			$sumbol_spa[0]='=';
			if($_REQUEST['patient_spa_id']=='')
			{
				$patient_spa_id=insert('patient_spa',$table_names_spa,$parameters_spa,'patient_spa_id');
				if($patient_spa_id!='')
					$msg_spa="ok";
			}
			else
			{
				$msg_spa=update('patient_spa',$table_names_spa,$parameters_spa,$edit_name_spa,$edit_id_spa,$sumbol_spa);
			}
		}
		else
		{
			if($_REQUEST['patient_spa_id']<>"")
			{
				$parameters_deleted[0]=1;
				$table_names_deleted[0]='deleted';
				$edit_name_deleted[0]='patient_spa_id';
				$edit_id_deleted[0]=$_REQUEST['patient_spa_id'];
				$sumbol_deleted[0]='=';
				$msg_spa=update('patient_spa',$table_names_deleted,$parameters_deleted,$edit_name_deleted,$edit_id_deleted,$sumbol_deleted);
			}
			else
				$msg_spa="ok";
		}
		
		
		//spa table-end
		
		//sle table-start
		if($_REQUEST['sle']==2)
		{	
			$parameters_sle[0]=$patient_followup_id;
			$table_names_sle[0]='patient_followup_id';
			$parameters_sle[1]=$pat_id;
			$table_names_sle[1]='pat_id';
			$parameters_sle[2]=0;
			$table_names_sle[2]='deleted';
			$parameters_sle[3]=$_REQUEST['pga'];
			$table_names_sle[3]='pga';
			$parameters_sle[4]=$_REQUEST['sledai1'];
			$table_names_sle[4]='sledai1';
			$parameters_sle[5]=$_REQUEST['sledai2'];
			$table_names_sle[5]='sledai2';
			$parameters_sle[6]=$_REQUEST['sledai3'];
			$table_names_sle[6]='sledai3';
			$parameters_sle[7]=$_REQUEST['sledai4'];
			$table_names_sle[7]='sledai4';
			$parameters_sle[8]=$_REQUEST['sledai5'];
			$table_names_sle[8]='sledai5';
			$parameters_sle[9]=$_REQUEST['sledai6'];
			$table_names_sle[9]='sledai6';
			$parameters_sle[10]=$_REQUEST['sledai7'];
			$table_names_sle[10]='sledai7';
			$parameters_sle[11]=$_REQUEST['sledai8'];
			$table_names_sle[11]='sledai8';
			$parameters_sle[12]=$_REQUEST['sledai9'];
			$table_names_sle[12]='sledai9';
			$parameters_sle[13]=$_REQUEST['sledai10'];
			$table_names_sle[13]='sledai10';
			$parameters_sle[14]=$_REQUEST['sledai11'];
			$table_names_sle[14]='sledai11';
			$parameters_sle[15]=$_REQUEST['sledai12'];
			$table_names_sle[15]='sledai12';
			$parameters_sle[16]=$_REQUEST['sledai13'];
			$table_names_sle[16]='sledai13';
			$parameters_sle[17]=$_REQUEST['sledai14'];
			$table_names_sle[17]='sledai14';
			$parameters_sle[18]=$_REQUEST['sledai15'];
			$table_names_sle[18]='sledai15';
			$parameters_sle[19]=$_REQUEST['sledai16'];
			$table_names_sle[19]='sledai16';
			$parameters_sle[20]=$_REQUEST['sledai17'];
			$table_names_sle[20]='sledai17';
			$parameters_sle[21]=$_REQUEST['sledai18'];
			$table_names_sle[21]='sledai18';
			$parameters_sle[22]=$_REQUEST['sledai19'];
			$table_names_sle[22]='sledai19';
			$parameters_sle[23]=$_REQUEST['sledai20'];
			$table_names_sle[23]='sledai20';
			$parameters_sle[24]=$_REQUEST['sledai21'];
			$table_names_sle[24]='sledai21';
			$parameters_sle[25]=$_REQUEST['sledai22'];
			$table_names_sle[25]='sledai22';
			$parameters_sle[26]=$_REQUEST['sledai23'];
			$table_names_sle[26]='sledai23';
			$parameters_sle[27]=$_REQUEST['sledai24'];
			$table_names_sle[27]='sledai24';
			if($_REQUEST['sledai2k']=="")
				$parameters_sle[28]=0;
			else
				$parameters_sle[28]=$_REQUEST['sledai2k'];
			
			$table_names_sle[28]='sledai2k';
			
			$parameters_sle[29]=$_REQUEST['add1'];
			$table_names_sle[29]='add1';
			$parameters_sle[30]=$_REQUEST['add2'];
			$table_names_sle[30]='add2';
			$parameters_sle[31]=$_REQUEST['add3'];
			$table_names_sle[31]='add3';
			$parameters_sle[32]=$_REQUEST['add4'];
			$table_names_sle[32]='add4';
			$parameters_sle[33]=$_REQUEST['add5'];
			$table_names_sle[33]='add5';
			$parameters_sle[34]=$_REQUEST['add6'];
			$table_names_sle[34]='add6';
			$parameters_sle[35]=$_REQUEST['add7'];
			$table_names_sle[35]='add7';
			$parameters_sle[36]=$_REQUEST['add8'];
			$table_names_sle[36]='add8';
			$parameters_sle[37]=$_REQUEST['add9'];
			$table_names_sle[37]='add9';
			$parameters_sle[38]=$_REQUEST['add10'];
			$table_names_sle[38]='add10';
			$parameters_sle[39]=$_REQUEST['add11'];
			$table_names_sle[39]='add11';
			$parameters_sle[40]=$_REQUEST['add12'];
			$table_names_sle[40]='add12';
			$parameters_sle[41]=$_REQUEST['add13'];
			$table_names_sle[41]='add13';
			$parameters_sle[42]=$_REQUEST['add14'];
			$table_names_sle[42]='add14';
			$parameters_sle[43]=$_REQUEST['add15'];
			$table_names_sle[43]='add15';
			$parameters_sle[44]=$_REQUEST['add16'];
			$table_names_sle[44]='add16';
			$parameters_sle[45]=$_REQUEST['add17'];
			$table_names_sle[45]='add17';
			$parameters_sle[46]=$_REQUEST['add18'];
			$table_names_sle[46]='add18';
			$parameters_sle[47]=$_REQUEST['add19'];
			$table_names_sle[47]='add19';
			$parameters_sle[48]=$_REQUEST['add20'];
			$table_names_sle[48]='add20';
			$parameters_sle[49]=$_REQUEST['add21'];
			$table_names_sle[49]='add21';
			$parameters_sle[50]=$_REQUEST['add22'];
			$table_names_sle[50]='add22';
			$parameters_sle[51]=$_REQUEST['add23'];
			$table_names_sle[51]='add23';
			$parameters_sle[52]=$_REQUEST['add24'];
			$table_names_sle[52]='add24';
			$parameters_sle[53]=$_REQUEST['add25'];
			$table_names_sle[53]='add25';
			$parameters_sle[54]=$_REQUEST['add26'];
			$table_names_sle[54]='add26';
			$parameters_sle[55]=$_REQUEST['flare1'];
			$table_names_sle[55]='flare1';
			$parameters_sle[56]=$_REQUEST['flare2'];
			$table_names_sle[56]='flare2';
			$parameters_sle[57]=$_REQUEST['flare3'];
			$table_names_sle[57]='flare3';
			$parameters_sle[58]=$_REQUEST['flare4'];
			$table_names_sle[58]='flare4';
			$parameters_sle[59]=$_REQUEST['flare5'];
			$table_names_sle[59]='flare5';
			$parameters_sle[60]=$_REQUEST['flare6'];
			$table_names_sle[60]='flare6';
			$parameters_sle[61]=$_REQUEST['flare7'];
			$table_names_sle[61]='flare7';
			$parameters_sle[62]=$_REQUEST['flare8'];
			$table_names_sle[62]='flare8';
			$parameters_sle[63]=$_REQUEST['flare9'];
			$table_names_sle[63]='flare9';
			$parameters_sle[64]=$_REQUEST['flare10'];
			$table_names_sle[64]='flare10';
			$parameters_sle[65]=$_REQUEST['flare11'];
			$table_names_sle[65]='flare11';
			$parameters_sle[66]=$_REQUEST['flare12'];
			$table_names_sle[66]='flare12';
			$parameters_sle[67]=$_REQUEST['flare13'];
			$table_names_sle[67]='flare13';
			if($_REQUEST['flare14']=="")
				$parameters_sle[68]=0;
			else
				$parameters_sle[68]=$_REQUEST['flare14'];
			
			$table_names_sle[68]='flare14';
			
			$parameters_sle[69]=$_REQUEST['sledai12b'];
			$table_names_sle[69]='sledai12b';
			$parameters_sle[70]=$_REQUEST['sledai13b'];
			$table_names_sle[70]='sledai13b';
			$parameters_sle[71]=$_REQUEST['sledai14b'];
			$table_names_sle[71]='sledai14b';
			$parameters_sle[72]=$_REQUEST['sledai16b'];
			$table_names_sle[72]='sledai16b';
			if (isset($_REQUEST['sledai15ba']))
				$sledai15ba="1";
			else
				$sledai15ba="0";
			
			if (isset($_REQUEST['sledai15bb']))
				$sledai15bb="1";
			else
				$sledai15bb="0";
			
			if (isset($_REQUEST['sledai15bc']))
				$sledai15bc="1";
			else
				$sledai15bc="0";
			
			if (isset($_REQUEST['sledai15bd']))
				$sledai15bd="1";
			else
				$sledai15bd="0";
			
			if (isset($_REQUEST['sledai15be']))
				$sledai15be="1";
			else
				$sledai15be="0";
			
			if (isset($_REQUEST['sledai15bf']))
				$sledai15bf="1";
			else
				$sledai15bf="0";
			
			if (isset($_REQUEST['sledai15bg']))
				$sledai15bg="1";
			else
				$sledai15bg="0";
			
			if (isset($_REQUEST['sledai15bh']))
				$sledai15bh="1";
			else
				$sledai15bh="0";
			
			if (isset($_REQUEST['sledai24bb']))
				$sledai24bb="1";
			else
				$sledai24bb="0";
					
			$parameters_sle[73]=$sledai15ba;
			$parameters_sle[74]=$sledai15bb;
			$parameters_sle[75]=$sledai15bc;
			$parameters_sle[76]=$sledai15bd;
			$parameters_sle[77]=$sledai15be;
			$parameters_sle[78]=$sledai15bf;
			$parameters_sle[79]=$sledai15bg;
			$parameters_sle[80]=$sledai15bh;
			$parameters_sle[81]=$_REQUEST['sledai23b'];
			$parameters_sle[82]=$_REQUEST['sledai24ba'];
			$parameters_sle[83]=$sledai24bb;
			
			$table_names_sle[73]='sledai15ba';
			$table_names_sle[74]='sledai15bb';
			$table_names_sle[75]='sledai15bc';
			$table_names_sle[76]='sledai15bd';
			$table_names_sle[77]='sledai15be';
			$table_names_sle[78]='sledai15bf';
			$table_names_sle[79]='sledai15bg';
			$table_names_sle[80]='sledai15bh';
			$table_names_sle[81]='sledai23b';
			$table_names_sle[82]='sledai24ba';
			$table_names_sle[83]='sledai24bb';
			$parameters_sle[84]=$_REQUEST['sledai9b'];
			$table_names_sle[84]='sledai9b';
			$parameters_sle[85]=str_replace("'", "''",$_REQUEST['nefritis1']);
			$table_names_sle[85]='nefritis1';
			$parameters_sle[86]=str_replace("'", "''",$_REQUEST['nefritis2']);
			$table_names_sle[86]='nefritis2';
			
			$edit_name_sle[0]='patient_sle_id';
			$edit_id_sle[0]=$_REQUEST['patient_sle_id'];
			$sumbol_sle[0]='=';
			if($_REQUEST['patient_sle_id']=='')
			{
				$patient_sle_id=insert('patient_sle',$table_names_sle,$parameters_sle,'patient_sle_id');
				if($patient_sle_id!='')
					$msg_sle="ok";
			}
			else
			{
				$msg_sle=update('patient_sle',$table_names_sle,$parameters_sle,$edit_name_sle,$edit_id_sle,$sumbol_sle);
			}
		}
		else
		{
			if($_REQUEST['patient_sle_id']<>"")
			{
				$parameters_deleted[0]=1;
				$table_names_deleted[0]='deleted';
				$edit_name_deleted[0]='patient_sle_id';
				$edit_id_deleted[0]=$_REQUEST['patient_sle_id'];
				$sumbol_deleted[0]='=';
				$msg_sle=update('patient_sle',$table_names_deleted,$parameters_deleted,$edit_name_deleted,$edit_id_deleted,$sumbol_deleted);
			}
			else
				$msg_sle="ok";
		}
		
		
		//sle table-end
		
		//severity_sle table-start
		if($_REQUEST['sle_severity']==2)
		{	
			$parameters_sle_severity[0]=$_REQUEST['cns4'];
			$table_names_sle_severity[0]='cns4';
			$parameters_sle_severity[1]=$patient_followup_id;
			$table_names_sle_severity[1]='patient_followup_id';
			$parameters_sle_severity[2]=$pat_id;
			$table_names_sle_severity[2]='pat_id';
			$parameters_sle_severity[3]=0;
			$table_names_sle_severity[3]='deleted';
			$parameters_sle_severity[4]=$_REQUEST['slicc1'];
			$table_names_sle_severity[4]='slicc1';
			$parameters_sle_severity[5]=$_REQUEST['slicc2'];
			$table_names_sle_severity[5]='slicc2';
			$parameters_sle_severity[6]=$_REQUEST['slicc3'];
			$table_names_sle_severity[6]='slicc3';
			$parameters_sle_severity[7]=$_REQUEST['slicc4'];
			$table_names_sle_severity[7]='slicc4';
			$parameters_sle_severity[8]=$_REQUEST['slicc5'];
			$table_names_sle_severity[8]='slicc5';
			$parameters_sle_severity[9]=$_REQUEST['slicc6'];
			$table_names_sle_severity[9]='slicc6';
			$parameters_sle_severity[10]=$_REQUEST['slicc7'];
			$table_names_sle_severity[10]='slicc7';
			$parameters_sle_severity[11]=$_REQUEST['slicc8'];
			$table_names_sle_severity[11]='slicc8';
			$parameters_sle_severity[12]=$_REQUEST['slicc9'];
			$table_names_sle_severity[12]='slicc9';
			$parameters_sle_severity[13]=$_REQUEST['slicc10'];
			$table_names_sle_severity[13]='slicc10';
			$parameters_sle_severity[14]=$_REQUEST['slicc11'];
			$table_names_sle_severity[14]='slicc11';
			$parameters_sle_severity[15]=$_REQUEST['slicc12'];
			$table_names_sle_severity[15]='slicc12';
			$parameters_sle_severity[16]=$_REQUEST['slicc13'];
			$table_names_sle_severity[16]='slicc13';
			$parameters_sle_severity[17]=$_REQUEST['slicc14'];
			$table_names_sle_severity[17]='slicc14';
			$parameters_sle_severity[18]=$_REQUEST['slicc15'];
			$table_names_sle_severity[18]='slicc15';
			$parameters_sle_severity[19]=$_REQUEST['slicc16'];
			$table_names_sle_severity[19]='slicc16';
			$parameters_sle_severity[20]=$_REQUEST['slicc17'];
			$table_names_sle_severity[20]='slicc17';
			$parameters_sle_severity[21]=$_REQUEST['slicc18'];
			$table_names_sle_severity[21]='slicc18';
			$parameters_sle_severity[22]=$_REQUEST['slicc19'];
			$table_names_sle_severity[22]='slicc19';
			$parameters_sle_severity[23]=$_REQUEST['slicc20'];
			$table_names_sle_severity[23]='slicc20';
			$parameters_sle_severity[24]=$_REQUEST['slicc21'];
			$table_names_sle_severity[24]='slicc21';
			$parameters_sle_severity[25]=$_REQUEST['slicc22'];
			$table_names_sle_severity[25]='slicc22';
			$parameters_sle_severity[26]=$_REQUEST['slicc23'];
			$table_names_sle_severity[26]='slicc23';
			$parameters_sle_severity[27]=$_REQUEST['slicc24'];
			$table_names_sle_severity[27]='slicc24';
			$parameters_sle_severity[28]=$_REQUEST['slicc25'];
			$table_names_sle_severity[28]='slicc25';
			$parameters_sle_severity[29]=$_REQUEST['slicc26'];
			$table_names_sle_severity[29]='slicc26';
			$parameters_sle_severity[30]=$_REQUEST['slicc27'];
			$table_names_sle_severity[30]='slicc27';
			$parameters_sle_severity[31]=$_REQUEST['slicc28'];
			$table_names_sle_severity[31]='slicc28';
			$parameters_sle_severity[32]=$_REQUEST['slicc29'];
			$table_names_sle_severity[32]='slicc29';
			$parameters_sle_severity[33]=$_REQUEST['slicc30'];
			$table_names_sle_severity[33]='slicc30';
			$parameters_sle_severity[34]=$_REQUEST['slicc31'];
			$table_names_sle_severity[34]='slicc31';
			$parameters_sle_severity[35]=$_REQUEST['slicc32'];
			$table_names_sle_severity[35]='slicc32';
			$parameters_sle_severity[36]=$_REQUEST['slicc33'];
			$table_names_sle_severity[36]='slicc33';
			$parameters_sle_severity[37]=$_REQUEST['slicc34'];
			$table_names_sle_severity[37]='slicc34';
			$parameters_sle_severity[38]=$_REQUEST['slicc35'];
			$table_names_sle_severity[38]='slicc35';
			$parameters_sle_severity[39]=$_REQUEST['slicc36'];
			$table_names_sle_severity[39]='slicc36';
			$parameters_sle_severity[40]=$_REQUEST['slicc37'];
			$table_names_sle_severity[40]='slicc37';
			$parameters_sle_severity[41]=$_REQUEST['slicc38'];
			$table_names_sle_severity[41]='slicc38';
			$parameters_sle_severity[42]=$_REQUEST['slicc39'];
			$table_names_sle_severity[42]='slicc39';
			$parameters_sle_severity[43]=$_REQUEST['slicc40'];
			$table_names_sle_severity[43]='slicc40';
			$parameters_sle_severity[44]=$_REQUEST['slicc41'];
			$table_names_sle_severity[44]='slicc41';
			$parameters_sle_severity[45]=$_REQUEST['index1'];
			$table_names_sle_severity[45]='index1';
			$parameters_sle_severity[46]=$_REQUEST['index2'];
			$table_names_sle_severity[46]='index2';
			$parameters_sle_severity[47]=$_REQUEST['index3'];
			$table_names_sle_severity[47]='index3';
			$parameters_sle_severity[48]=$_REQUEST['index4'];
			$table_names_sle_severity[48]='index4';
			$parameters_sle_severity[49]=$_REQUEST['index5'];
			$table_names_sle_severity[49]='index5';
			$parameters_sle_severity[50]=$_REQUEST['index6'];
			$table_names_sle_severity[50]='index6';
			$parameters_sle_severity[51]=$_REQUEST['index7'];
			$table_names_sle_severity[51]='index7';
			$parameters_sle_severity[52]=$_REQUEST['index8'];
			$table_names_sle_severity[52]='index8';
			$parameters_sle_severity[53]=$_REQUEST['index9'];
			$table_names_sle_severity[53]='index9';
			$parameters_sle_severity[54]=$_REQUEST['index10'];
			$table_names_sle_severity[54]='index10';
			$parameters_sle_severity[55]=$_REQUEST['index11'];
			$table_names_sle_severity[55]='index11';
			$parameters_sle_severity[56]=$_REQUEST['index12'];
			$table_names_sle_severity[56]='index12';
			$parameters_sle_severity[57]=$_REQUEST['index13'];
			$table_names_sle_severity[57]='index13';
			
			
			$parameters_sle_severity[58]=$_REQUEST['cns3'];
			$table_names_sle_severity[58]='cns3';
			$parameters_sle_severity[59]=date_for_postgres($_REQUEST['slicc1_date']);
			$table_names_sle_severity[59]='slicc1_date';
			$parameters_sle_severity[60]=date_for_postgres($_REQUEST['slicc2_date']);
			$table_names_sle_severity[60]='slicc2_date';
			$parameters_sle_severity[61]=date_for_postgres($_REQUEST['slicc3_date']);
			$table_names_sle_severity[61]='slicc3_date';
			$parameters_sle_severity[62]=date_for_postgres($_REQUEST['slicc4_date']);
			$table_names_sle_severity[62]='slicc4_date';
			$parameters_sle_severity[63]=date_for_postgres($_REQUEST['slicc5_date']);
			$table_names_sle_severity[63]='slicc5_date';
			$parameters_sle_severity[64]=date_for_postgres($_REQUEST['slicc6_date']);
			$table_names_sle_severity[64]='slicc6_date';
			$parameters_sle_severity[65]=date_for_postgres($_REQUEST['slicc7_date']);
			$table_names_sle_severity[65]='slicc7_date';
			$parameters_sle_severity[66]=date_for_postgres($_REQUEST['slicc8_date']);
			$table_names_sle_severity[66]='slicc8_date';
			$parameters_sle_severity[67]=date_for_postgres($_REQUEST['slicc9_date']);
			$table_names_sle_severity[67]='slicc9_date';
			$parameters_sle_severity[68]=date_for_postgres($_REQUEST['slicc10_date']);
			$table_names_sle_severity[68]='slicc10_date';
			$parameters_sle_severity[69]=date_for_postgres($_REQUEST['slicc11_date']);
			$table_names_sle_severity[69]='slicc11_date';
			$parameters_sle_severity[70]=date_for_postgres($_REQUEST['slicc12_date']);
			$table_names_sle_severity[70]='slicc12_date';
			$parameters_sle_severity[71]=date_for_postgres($_REQUEST['slicc13_date']);
			$table_names_sle_severity[71]='slicc13_date';
			$parameters_sle_severity[72]=date_for_postgres($_REQUEST['slicc14_date']);
			$table_names_sle_severity[72]='slicc14_date';
			$parameters_sle_severity[73]=date_for_postgres($_REQUEST['slicc15_date']);
			$table_names_sle_severity[73]='slicc15_date';
			$parameters_sle_severity[74]=date_for_postgres($_REQUEST['slicc16_date']);
			$table_names_sle_severity[74]='slicc16_date';
			$parameters_sle_severity[75]=date_for_postgres($_REQUEST['slicc17_date']);
			$table_names_sle_severity[75]='slicc17_date';
			$parameters_sle_severity[76]=date_for_postgres($_REQUEST['slicc18_date']);
			$table_names_sle_severity[76]='slicc18_date';
			$parameters_sle_severity[77]=date_for_postgres($_REQUEST['slicc19_date']);
			$table_names_sle_severity[77]='slicc19_date';
			$parameters_sle_severity[78]=date_for_postgres($_REQUEST['slicc20_date']);
			$table_names_sle_severity[78]='slicc20_date';
			$parameters_sle_severity[79]=date_for_postgres($_REQUEST['slicc21_date']);
			$table_names_sle_severity[79]='slicc21_date';
			$parameters_sle_severity[80]=date_for_postgres($_REQUEST['slicc22_date']);
			$table_names_sle_severity[80]='slicc22_date';
			$parameters_sle_severity[81]=date_for_postgres($_REQUEST['slicc23_date']);
			$table_names_sle_severity[81]='slicc23_date';
			$parameters_sle_severity[82]=date_for_postgres($_REQUEST['slicc24_date']);
			$table_names_sle_severity[82]='slicc24_date';
			$parameters_sle_severity[83]=date_for_postgres($_REQUEST['slicc25_date']);
			$table_names_sle_severity[83]='slicc25_date';
			$parameters_sle_severity[84]=date_for_postgres($_REQUEST['slicc26_date']);
			$table_names_sle_severity[84]='slicc26_date';
			$parameters_sle_severity[85]=date_for_postgres($_REQUEST['slicc27_date']);
			$table_names_sle_severity[85]='slicc27_date';
			$parameters_sle_severity[86]=date_for_postgres($_REQUEST['slicc28_date']);
			$table_names_sle_severity[86]='slicc28_date';
			$parameters_sle_severity[87]=date_for_postgres($_REQUEST['slicc29_date']);
			$table_names_sle_severity[87]='slicc29_date';
			$parameters_sle_severity[88]=date_for_postgres($_REQUEST['slicc30_date']);
			$table_names_sle_severity[88]='slicc30_date';
			$parameters_sle_severity[89]=date_for_postgres($_REQUEST['slicc31_date']);
			$table_names_sle_severity[89]='slicc31_date';
			$parameters_sle_severity[90]=date_for_postgres($_REQUEST['slicc32_date']);
			$table_names_sle_severity[90]='slicc32_date';
			$parameters_sle_severity[91]=date_for_postgres($_REQUEST['slicc33_date']);
			$table_names_sle_severity[91]='slicc33_date';
			$parameters_sle_severity[92]=date_for_postgres($_REQUEST['slicc34_date']);
			$table_names_sle_severity[92]='slicc34_date';
			$parameters_sle_severity[93]=date_for_postgres($_REQUEST['slicc35_date']);
			$table_names_sle_severity[93]='slicc35_date';
			$parameters_sle_severity[94]=date_for_postgres($_REQUEST['slicc36_date']);
			$table_names_sle_severity[94]='slicc36_date';
			$parameters_sle_severity[95]=date_for_postgres($_REQUEST['slicc37_date']);
			$table_names_sle_severity[95]='slicc37_date';
			$parameters_sle_severity[96]=date_for_postgres($_REQUEST['slicc38_date']);
			$table_names_sle_severity[96]='slicc38_date';
			$parameters_sle_severity[97]=date_for_postgres($_REQUEST['slicc39_date']);
			$table_names_sle_severity[97]='slicc39_date';
			$parameters_sle_severity[98]=date_for_postgres($_REQUEST['slicc40_date']);
			$table_names_sle_severity[98]='slicc40_date';
			$parameters_sle_severity[99]=date_for_postgres($_REQUEST['index1_date']);
			$table_names_sle_severity[99]='index1_date';
			$parameters_sle_severity[100]=date_for_postgres($_REQUEST['index2_date']);
			$table_names_sle_severity[100]='index2_date';
			$parameters_sle_severity[101]=date_for_postgres($_REQUEST['index3_date']);
			$table_names_sle_severity[101]='index3_date';
			$parameters_sle_severity[102]=date_for_postgres($_REQUEST['index4_date']);
			$table_names_sle_severity[102]='index4_date';
			$parameters_sle_severity[103]=date_for_postgres($_REQUEST['index5_date']);
			$table_names_sle_severity[103]='index5_date';
			$parameters_sle_severity[104]=date_for_postgres($_REQUEST['index6_date']);
			$table_names_sle_severity[104]='index6_date';
			$parameters_sle_severity[105]=date_for_postgres($_REQUEST['index7_date']);
			$table_names_sle_severity[105]='index7_date';
			$parameters_sle_severity[106]=date_for_postgres($_REQUEST['index8_date']);
			$table_names_sle_severity[106]='index8_date';
			$parameters_sle_severity[107]=date_for_postgres($_REQUEST['index9_date']);
			$table_names_sle_severity[107]='index9_date';
			$parameters_sle_severity[108]=date_for_postgres($_REQUEST['index10_date']);
			$table_names_sle_severity[108]='index10_date';
			$parameters_sle_severity[109]=date_for_postgres($_REQUEST['index11_date']);
			$table_names_sle_severity[109]='index11_date';
			$parameters_sle_severity[110]=date_for_postgres($_REQUEST['index12_date']);
			$table_names_sle_severity[110]='index12_date';
			$parameters_sle_severity[111]=$_REQUEST['cns1'];
			$table_names_sle_severity[111]='cns1';
			$parameters_sle_severity[112]=$_REQUEST['cns2'];
			$table_names_sle_severity[112]='cns2';
			$parameters_sle_severity[113]=$_REQUEST['slicc42'];
			$table_names_sle_severity[113]='slicc42';
			$parameters_sle_severity[114]=date_for_postgres($_REQUEST['slicc41_date']);
			$table_names_sle_severity[114]='slicc41_date';
			
			
			$edit_name_sle_severity[0]='patient_sle_severity_id';
			$edit_id_sle_severity[0]=$_REQUEST['patient_sle_severity_id'];
			$sumbol_sle_severity[0]='=';
			if($_REQUEST['patient_sle_severity_id']=='')
			{
				$patient_sle_severity_id=insert('patient_sle_severity',$table_names_sle_severity,$parameters_sle_severity,'patient_sle_severity_id');
				if($patient_sle_severity_id!='')
					$msg_sle_severity="ok";
			}
			else
			{
				$msg_sle_severity=update('patient_sle_severity',$table_names_sle_severity,$parameters_sle_severity,$edit_name_sle_severity,$edit_id_sle_severity,$sumbol_sle_severity);
			}
		}
		else
		{
			if($_REQUEST['patient_sle_severity_id']<>"")
			{
				$parameters_deleted[0]=1;
				$table_names_deleted[0]='deleted';
				$edit_name_deleted[0]='patient_sle_severity_id';
				$edit_id_deleted[0]=$_REQUEST['patient_sle_severity_id'];
				$sumbol_deleted[0]='=';
				$msg_sle_severity=update('patient_sle_severity',$table_names_deleted,$parameters_deleted,$edit_name_deleted,$edit_id_deleted,$sumbol_deleted);
			}
			else
				$msg_sle_severity="ok";
		}
		
		
		//severity_sle table-end
		
		//echo "msg_spa:$msg_spa msg_common:$msg_common msg_sf36:$msg_sf36 msg_hads:$msg_hads msg_wpai:$msg_wpai msg_euroqol:$msg_euroqol msg:$msg cohort_drugs_msg:$cohort_drugs_msg cohort_corticosteroids_msg:$cohort_corticosteroids_msg cohort_analgesics_msg:$cohort_analgesics_msg </br>";
		if ( $msg_sle_severity=="ok" and $msg_spa=="ok" and $msg_common=="ok" and $msg_sf36=="ok" and $msg_hads=="ok" and $msg_wpai=="ok" and $msg_euroqol=="ok" and $msg=="ok" and $cohort_drugs_msg=="ok" and $cohort_corticosteroids_msg=="ok" and $cohort_analgesics_msg=="ok")
		{
			$save_chk="ok";
			$_SESSION['patient_followup_id']=$patient_followup_id;
			pg_query("COMMIT") or die("Transaction commit failed\n");
		}
		else
		{
			$save_chk="nok";
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
	}
	catch(exception $e) {
		echo "e:".$e."</br>";
		echo "ROLLBACK";
		pg_query("ROLLBACK") or die("Transaction rollback failed\n");
	}
	
}
?>
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
	   <h1>
          <?php if($patient_followup_id=="") { echo "New Follow up "; } else { echo "Edit Follow up "; } ?>
        </h1>
		<?php
				if($patient_followup_id!="")
				{
					$exec = get_followup($patient_followup_id);
					$result = pg_fetch_array($exec);
					
					
					$followup_start_date=$result['start_date_str'];
					if($followup_start_date=="12-12-1900")
						$followup_start_date="";
					$fumonthcohort=$result['fumonthcohort'];
					$fumonthinclusion=$result['fumonthinclusion'];
					$physician_assigning=$result['physician_assigning'];
					$cohort_name_str=$result['cohort_name_str'];
					$euroqol=$result['euroqol'];
					$asas=$result['asas'];
					$psaid=$result['psaid'];
					$wpai=$result['wpai'];
					$hads=$result['hads'];
					$sf36=$result['sf36'];
					
					$spa=$result['spa'];
					$sle=$result['sle'];
					$sle_severity=$result['sle_severity'];
					/*
					$previous_dmards_nbr=$result['previous_dmards_nbr'];
					$ongoing_dmards_nbr=$result['ongoing_dmards_nbr'];
					$followup_name=$result['followup_name'];
					$followup_name_str=$result['followup_name_str'];
					$treatment_nbr=$result['treatment_nbr'];
					$age_at_cohort=$result['age_at_cohort'];
					$residence=$result['residence'];
					$contraceptive_methods=$result['contraceptive_methods'];
					$pregnancies=$result['pregnancies'];
					$childbirths=$result['childbirths'];
					$pregnancy_loses=$result['pregnancy_loses'];
					$premature_births=$result['premature_births'];
					$eclampsia=$result['eclampsia'];
					$premature_rom=$result['premature_rom'];
					$placental_insufficiency=$result['placental_insufficiency'];
					$premature_births_reason=$result['premature_births_reason'];
					$employment_changed=$result['employment_changed'];
					$employment_at_cohort=$result['employment_at_cohort'];
					$marital_status=$result['marital_status'];
					$marital_status_year=$result['marital_status_year'];
					$alcohol_consumption=$result['alcohol_consumption'];
					$physical_activity=$result['physical_activity'];
					$mediterranean_diet=$result['mediterranean_diet'];*/
					
					$common=$result['common'];
					$tobacco=$result['tobacco'];
					$smokingstarting=$result['smokingstarting'];
					$smokingstopped=$result['smokingstopped'];
					$smokingyear=$result['smokingyear'];
					$decadefirst=$result['decadefirst'];
					$decadesecond=$result['decadesecond'];
					$decadethird=$result['decadethird'];
					$decadeforth=$result['decadeforth'];
					$decadefifth=$result['decadefifth'];
					$decadesixth=$result['decadesixth'];
					$decadeseventh=$result['decadeseventh'];
					$decadeeight=$result['decadeeight'];
					$averagepackperday=$result['averagepackperday'];
					$averagepackperyear=$result['averagepackperyear'];
				}
				else
				{
					//pernei tis times apo to teleytaio follow up
					$exec_prv = get_prv_followup($pat_id);
					$result_prv = pg_fetch_array($exec_prv);
					
					$sle_severity=$result_prv['sle_severity'];
					$common=$result_prv['common'];
					$tobacco=$result_prv['tobacco'];
					$smokingstarting=$result_prv['smokingstarting'];
					$smokingstopped=$result_prv['smokingstopped'];
					$smokingyear=$result_prv['smokingyear'];
					$decadefirst=$result_prv['decadefirst'];
					$decadesecond=$result_prv['decadesecond'];
					$decadethird=$result_prv['decadethird'];
					$decadeforth=$result_prv['decadeforth'];
					$decadefifth=$result_prv['decadefifth'];
					$decadesixth=$result_prv['decadesixth'];
					$decadeseventh=$result_prv['decadeseventh'];
					$decadeeight=$result_prv['decadeeight'];
					$averagepackperday=$result_prv['averagepackperday'];
					$averagepackperyear=$result_prv['averagepackperyear'];
					
				}
				
				$exec = get_patient_demographics($pat_id);
				$result = pg_fetch_array($exec);
				$dateofinclusion=$result['dateofinclusion_str'];
				if($dateofinclusion=="12-12-1900")
					$dateofinclusion="";
				$dateofbirth=$result['dateofbirth_str'];
				if($dateofbirth=="12-12-1900")
					$dateofbirth="";
				$gender=$result['gender'];
				if($gender==1)
					$gender_str="Female";
				else if($gender==2)
					$gender_str="Male";
				$patient_id=$result['patient_id'];
				
				$order = get_cohort($patient_cohort_id);
				$result = pg_fetch_assoc($order);
				$cohort_name_str=$result['cohort_name_str'];
				$coh_start_date=$result['start_date_str'];
				if($coh_start_date=="12-12-1900")
					$coh_start_date="";
				$coh_stop_date=$result['stop_date_str'];
				if($coh_stop_date=="12-12-1900")
					$coh_stop_date="-";
				
				echo "<h1><small>Patient:".$patient_id.",".$gender_str." (".$dateofbirth.") - Inclusion date: ".$dateofinclusion." </small></br>";
				echo "<small>Cohort:".$cohort_name_str.", start date:".$coh_start_date." end date:".$coh_stop_date."</small></h1>"; ?>
       
      </section>

      <!-- Main content -->
      <section class="content">
	  <div class="alert alert_suc alert-success" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Successful save!</strong>
	  </div>
	  <div class="alert alert_wr alert-danger" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Unsuccessful save!</strong>
	  </div>
	  <form id="form" name="form" action="followup_edit.php" method="POST">
	  <input type="hidden" id="save" name="save" value="1">
	  <input type="hidden" id="pat_id" name="pat_id" value="<?php echo $pat_id;?>">
		<input type="hidden" id="patient_cohort_id" name="patient_cohort_id" value="<?php echo $patient_cohort_id;?>">
		<input type="hidden" id="patient_followup_id" name="patient_followup_id" value="<?php echo $patient_followup_id;?>">
       		<?php
				$exec = get_cohort($patient_cohort_id);
				$result = pg_fetch_array($exec);
				$start_date=$result['start_date_str'];
				if($start_date=="12-12-1900")
						$start_date="";
			?>
			<input type="hidden" class="form-control" id="cohort_start_date" name="cohort_start_date" value="<?php echo $start_date; ?>">
			<?php
				$exec = get_patient_demographics($pat_id);
				$result = pg_fetch_array($exec);
				$dateofinclusion_str=$result['dateofinclusion_str'];
				if ($dateofinclusion_str=='12-12-1900')
					$dateofinclusion_str='';
			?>
			<input type="hidden" class="form-control" id="inclusion_start_date" name="inclusion_start_date" value="<?php echo $dateofinclusion_str; ?>">
			<input type="hidden" class="form-control" id="fumonthinclusion_str" name="fumonthinclusion_str" value="<?php echo $fumonthinclusion; ?>">
			<input type="hidden" class="form-control" id="fumonthcohort_str" name="fumonthcohort_str" value="<?php echo $fumonthcohort; ?>">
			<div class="row">
			<div class="form-group col-md-12">
				<input type="submit" class="btn btn-primary" value="Save">&nbsp;&nbsp;&nbsp;
				<a href="adverses.php"><input type="button" class="btn btn-primary" value="Adverse events"></a>&nbsp;&nbsp;&nbsp;
				<a href="adverses_all.php"><input type="button" class="btn btn-primary" value="All Adverse events"></a>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<!-- cohort div start -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Followup</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div  class="form-group col-md-3" id="divstartdate" name="divstartdate">
								<label>*Followup date</label>
								<div class="input-group date">
								  <div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								  </div>
								  <input name="start_date" type="text" class="form-control pull-right" id="start_date" value="<?php echo $followup_start_date; ?>" readonly>
								</div>
								<!-- /.input group -->
							  </div>
							  <div class="form-group col-md-3" id="divphysician" name="divphysician">
								<label>Physician assigning Followup</label>
								  <select class="form-control" name="physician_assigning" id="physician_assigning">
									<option value="0">--</option>
									<?php
										$sql = get_lookup_tbl_values_specific_parent("referringphysician",$_SESSION['user_centre_id']);
										$numrows = pg_num_rows($sql);
										while($result2 = pg_fetch_array($sql))
										{
											$physician_assigning_id=$result2['id'];
											$value =$result2['value'];
									?>
									<option value="<?php echo $physician_assigning_id; ?>" <?php if($physician_assigning_id==$physician_assigning) { echo "selected"; } ?>><?php echo $value; ?></option>
									<?php
										}
									?>
								  </select>
							</div>
							<div class="form-group col-md-3" id="divinclusionmonth" name="divinclusionmonth">
								<label>FU inclusion month</label>
								  <select class="form-control" name="fumonthinclusion" id="fumonthinclusion">
								  </select>
							</div>
							<div class="form-group col-md-3" id="divcohortmonth" name="divcohortmonth">
								<label>FU cohort month</label>
								  <select class="form-control" name="fumonthcohort" id="fumonthcohort">
								  </select>
							</div>
						</div>
					</div>
				</div>
				<!-- cohort div end -->
				<!-- treatments start div -->
				<div class="row">
					<div class="col-md-12">
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Continuous therapy</small></h3>
							</div>
							<div class="box-body">
								<div class="row">
									<div class="form-group col-md-12" id="divdiseases" name="divdiseases">
									<?php
												$sql = get_drugs_ongoing();
												$drugs_counter=0;
												
												while($result2 = pg_fetch_array($sql))
												{	
													$drugs_values .= $result2['code'];
													$drugs_values .= "-";
													$drugs_values .= $result2['substance'];
													$drugs_values .= "( min: ";
													$drugs_values .= $result2['min'];
													$drugs_values .= " - max:";
													$drugs_values .= $result2['max'];
													$drugs_values .= " / route: ";
													$drugs_values .= $result2['route_of_administration_val'];
													$drugs_values .= " )";
													$drugs_values .= " )";
													
													$drugs_values .= "!@#$%^" ;
													
													$drugs_ids .=$result2['drugs_id'];
													$drugs_ids .= "!@#$%^" ;
													$drugs_maxs .=$result2['max'];
													$drugs_maxs .= "!@#$%^";
													
													$drugs_mins .=$result2['min'];
													$drugs_mins .= "!@#$%^";				
													$drugs_counter++;
												}
											?>
												<input type="hidden" value="<?php echo $drugs_counter; ?>" id="drugs_counter" name="drugs_counter"/>
												<input type="hidden" value="<?php echo $drugs_values; ?>" id="drugs_values" name="drugs_values"/>
												<input type="hidden" value="<?php echo $drugs_ids; ?>" id="drugs_ids" name="drugs_ids"/>
												<input type="hidden" value="<?php echo $drugs_maxs; ?>" id="drugs_maxs" name="drugs_maxs"/>
												<input type="hidden" value="<?php echo $drugs_mins; ?>" id="drugs_mins" name="drugs_mins"/>
											
											<?php
												/*$sql = get_lookup_tbl_values('route_of_administration');
												$route_counter=0;				
												while($result2 = pg_fetch_array($sql))
												{	
													$route_values .= $result2['value'];
													$route_values .= "!@#$%^" ;
													$route_ids .=$result2['id'];
													$route_ids .= "!@#$%^" ;
																	
													$route_counter++;
												}*/
											?>
												<!--<input type="hidden" value="<?php echo $route_counter; ?>" id="route_counter" name="route_counter"/>
												<input type="hidden" value="<?php echo $route_values; ?>" id="route_values" name="route_values"/>
												<input type="hidden" value="<?php echo $route_ids; ?>" id="route_ids" name="route_ids"/>
												-->
												<?php
												$sql = get_drugs_frequencies();
												$drugs_frequency_counter=0;
												
												while($result2 = pg_fetch_array($sql))
												{	
													$drugs_frequency_values .= $result2['value'];
													$drugs_frequency_values .= "!@#$%^" ;
													
													$drugs_frequency_ids .=$result2['drugs_frequency_id'];
													$drugs_frequency_ids .= "!@#$%^" ;
																	
													$drugs_frequency_counter++;
												}
											?>
												<input type="hidden" value="<?php echo $drugs_frequency_counter; ?>" id="drugs_frequency_counter" name="drugs_frequency_counter"/>
												<input type="hidden" value="<?php echo $drugs_frequency_values; ?>" id="drugs_frequency_values" name="drugs_frequency_values"/>
												<input type="hidden" value="<?php echo $drugs_frequency_ids; ?>" id="drugs_frequency_ids" name="drugs_frequency_ids"/>
												<?php
												
												$drugs_reason_values .= 'Change of main diagnosis';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="20";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Insufficient response (primary)';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="1";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Secondary failure';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="2";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Failure & adverse event';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="3";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Lost to follow-up';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="4";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Drug intolerance';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="5";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Drug reaction/Allergy';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="6";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Infection';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="7";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Remission';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="8";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Solid tumor/Lymphoma';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="9";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Other adverse event';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="10";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'patient decision';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="11";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Planned therapy';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="12";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Pregnancy';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="13";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Financial';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="14";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Change to BIOsimilar';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="15";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Change practical reasons';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="16";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Change to bio-originator';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="17";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Other';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="18";
												$drugs_reason_ids .= "!@#$%^" ;
												$drugs_reason_values .= 'Unknown';
												$drugs_reason_values .= "!@#$%^" ;
												$drugs_reason_ids .="19";
												$drugs_reason_ids .= "!@#$%^" ;
												
												
											?>
												<input type="hidden" value="19" id="drugs_reason_counter" name="drugs_reason_counter"/>
												<input type="hidden" value="<?php echo $drugs_reason_values; ?>" id="drugs_reason_values" name="drugs_reason_values"/>
												<input type="hidden" value="<?php echo $drugs_reason_ids; ?>" id="drugs_reason_ids" name="drugs_reason_ids"/>
										  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="patient_drugs_tbl" name="patient_drugs_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th colspan="3" class="col-md-6"><b>Drug</b></th>
													<th class="col-md-1"><b>Main</br>treatment</b></th>
													<!--<th class="col-md-2"><b>Route of</br>administration</b></th>-->
													<th class="col-md-2"><b>Start date</b></th>
													<th class="col-md-2"><b>Stop date</b></th>
													<th class="col-md-1">&nbsp;</th>
												</tr>
											 </thead>
											 <tbody>
											<?php
												$i=0;
												$j=0;
													$order = get_patient_cohort_drugs($patient_cohort_id);
													$num_rows = pg_num_rows($order);
													$drugs_row=0;
													while($result = pg_fetch_assoc($order))
													{
														$i++;
														$j++;
														$patient_cohort_drugs_id=$result['patient_cohort_drugs_id'];
														$drugs_id=$result['drugs_id'];
														$patient_cohort_drugs_min=$result['min'];
														$patient_cohort_drugs_max=$result['max'];
														$main=$result['main'];
														$route_of_administration=$result['route_of_administration'];
														$dose=$result['dose'];
														$frequency=$result['frequency'];
														$startdate_str=$result['startdate_str'];
														if ($startdate_str=='12-12-1900')
															$startdate_str='';
														$stopdate_str=$result['stopdate_str'];
														if ($stopdate_str=='12-12-1900')
															$stopdate_str='';
														
														$drugs_row=$i;
														
														$q1 = "select weekdosage,currentdosage from patient_lookup_drugs where deleted=0 and patient_followup_id=$patient_followup_id and drugs_id=$drugs_id and drug_flag=1";
														$q_exec = pg_query($q1);
														$q_reason=pg_fetch_array($q_exec); 
														$weekdosage=$q_reason['weekdosage'];
														$currentdosage=$q_reason['currentdosage'];
											?>
												  <tr id="patient_cohort_drugs_tr_<?php echo $i;?>" style="background-color:#f0f8ff;">
														<td colspan="3" align="center">
															<input type="hidden" id="patient_cohort_drugs_type_<?php echo $i;?>" name="patient_cohort_drugs_type_<?php echo $i;?>" value="cohort"/>
															<input type="hidden" id="patient_cohort_drugs_hidden_<?php echo $i;?>" name="patient_cohort_drugs_hidden_<?php echo $i;?>" value="1"/>
															<input type="hidden" name="patient_cohort_drugs_id_<?php echo $i; ?>" id="patient_cohort_drugs_id_<?php echo $i; ?>" value="<?php echo $patient_cohort_drugs_id;?>">
															<input type="hidden" name="patient_cohort_drugs_row_<?php echo $i; ?>" id="patient_cohort_drugs_row_<?php echo $i; ?>" value="<?php echo $drugs_row;?>">
															<input type="hidden" name="patient_cohort_drugs_min_<?php echo $i; ?>" id="patient_cohort_drugs_min_<?php echo $i; ?>" value="<?php echo $patient_cohort_drugs_min;?>">
															<input type="hidden" name="patient_cohort_drugs_max_<?php echo $i; ?>" id="patient_cohort_drugs_max_<?php echo $i; ?>" value="<?php echo $patient_cohort_drugs_max;?>">
															<select  class="form-control" name="drugs_id_<?php echo $i; ?>" id="drugs_id_<?php echo $i; ?>" onchange="setminmax(<?php echo $i; ?>)">
																<option value="0">--</option>
																<?php
																	$sql = get_drugs_ongoing();
																	$numrows = pg_num_rows($sql);
																	while($result2 = pg_fetch_array($sql))
																	{
																		$drugs_id2=$result2['drugs_id'];
																		$value = $result2['code'];
																		$value .= "-";
																		$value .= $result2['substance'];
																		$value .= "( min: ";
																		$value .= $result2['min'];
																		$value .= " - max:";
																		$value .= $result2['max'];
																		$value .= " )";
																?>
																<option value="<?php echo $drugs_id2; ?>" <?php if($drugs_id==$drugs_id2) { echo "selected"; } ?>><?php echo $value; ?></option>
																<?php
																	}
																?>
															</select>
															<?php echo "3-month mean: $weekdosage - current dosage: $currentdosage"; ?>
														</td>
														<td align="center">
															<input type="checkbox" id="main_<?php echo $i; ?>" name="main_<?php echo $i; ?>" onclick="calc(<?php echo $i; ?>)"  <?php if ($main==1) { echo 'checked'; }; ?>>
														</td>
														<!--<td align="center">
															<select class="form-control" name="route_of_administration_<?php /* echo $i; ?>" id="route_of_administration_<?php echo $i; ?>">
																<option value="0" <?php if($route_of_administration==0) { echo "selected"; } ?>>--</option>
																<?php
																	$sql = get_lookup_tbl_values('route_of_administration');
																	$numrows = pg_num_rows($sql);
																	while($result2 = pg_fetch_array($sql))
																	{
																		$route=$result2['id'];
																		$value=$result2['value'];
																?>
																	<option value="<?php echo $route; ?>" <?php if($route_of_administration==$route) { echo "selected"; } ?>><?php echo $value; ?></option>
																<?php
																	}
																		*/
																?>
															</select>
														</td>-->
														  <td align="center">
															<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="startdate_<?php echo $i; ?>"  name="startdate_<?php echo $i; ?>" value="<?php echo $startdate_str; ?>">
														</td>
														<td align="center">
															<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="stopdate_<?php echo $i; ?>"  name="stopdate_<?php echo $i; ?>" value="<?php echo $stopdate_str; ?>">
														</td>	
													 	<td align="center">
															
															<a <?php if($main==1) { ?> style="DISPLAY:none;" <?php }?> id="a_<?php echo $i; ?>" name="a_<?php echo $i; ?>" title="Delete" href="javascript:void(0);" onclick="javascript:if (confirm('Are you sure;All periods will be deleted!')) { patient_cohort_drugs_removerow(<?php echo $i; ?>)};"><i class="fa fa-trash"></i><span></a>
														</td>
														 </tr>
											<?php
														$i++;
														$j++;
											?>
												<tr id="patient_cohort_drugs_tr_<?php echo $i;?>">
													 <td align="center">
															Periods
														</td>
														 <td align="center">
															<input type="hidden" id="patient_cohort_drugs_hidden_<?php echo $i;?>" name="patient_cohort_drugs_hidden_<?php echo $i;?>" value="1"/>
															<input type="hidden" id="patient_cohort_drugs_type_<?php echo $i;?>" name="patient_cohort_drugs_type_<?php echo $i;?>" value="period_label"/>
															<input type="hidden" name="patient_cohort_drugs_row_<?php echo $i; ?>" id="patient_cohort_drugs_row_<?php echo $i; ?>" value="<?php echo $drugs_row;?>">
															<input type="hidden" name="patient_cohort_drugs_id_<?php echo $i; ?>" id="patient_cohort_drugs_id_<?php echo $i; ?>" value="<?php echo $patient_cohort_drugs_id;?>">
															Reason for </br> treatment change
														</td>
														<td align="center">
															frequency
														</td>
														<td align="center">
															dose
														</td>
														<td align="center">
															start date
														</td>
														<td align="center">
															stop date
														</td>
														<td align="center">
															<a id="a2_add_<?php echo $i; ?>" name="a2_add_<?php echo $i; ?>" title="Add new period" href="javascript:void(0);" onclick="patient_cohort_drugs_newperiod(<?php echo $i; ?>,<?php echo $patient_cohort_drugs_id; ?>);"><i class="fa fa-calendar-plus-o"></i><span></a>
														</td>
												</tr>
											<?php
														
														$order2 = get_patient_cohort_drugs_period($patient_cohort_drugs_id);
														$num_rows2 = pg_num_rows($order2);
														
														while($result2 = pg_fetch_assoc($order2))
														{
															$i++;
															$j++;
															$patient_cohort_drugs_period_id=$result2['patient_cohort_drugs_period_id'];
															$patient_cohort_drugs_id=$result2['patient_cohort_drugs_id'];
															$reason=$result2['reason'];
															$dose=$result2['dose'];
															$frequency=$result2['frequency'];
															$startdate_str=$result2['startdate_str'];
															if ($startdate_str=='12-12-1900')
																$startdate_str='';
															$stopdate_str=$result2['stopdate_str'];
															if ($stopdate_str=='12-12-1900')
																$stopdate_str='';
											?>
												  <tr id="patient_cohort_drugs_tr_<?php echo $i;?>">
														<td align="center">
															<input type="hidden" id="patient_cohort_drugs_type_<?php echo $i;?>" name="patient_cohort_drugs_type_<?php echo $i;?>" value="period"/>
															<input type="hidden" id="patient_cohort_drugs_hidden_<?php echo $i;?>" name="patient_cohort_drugs_hidden_<?php echo $i;?>" value="1"/>
															<input type="hidden" name="patient_cohort_drugs_id_<?php echo $i; ?>" id="patient_cohort_drugs_id_<?php echo $i; ?>" value="<?php echo $patient_cohort_drugs_id;?>">
															<input type="hidden" name="patient_cohort_drugs_period_id_<?php echo $i; ?>" id="patient_cohort_drugs_period_id_<?php echo $i; ?>" value="<?php echo $patient_cohort_drugs_period_id;?>">
															<input type="hidden" name="patient_cohort_drugs_row_<?php echo $i; ?>" id="patient_cohort_drugs_row_<?php echo $i; ?>" value="<?php echo $drugs_row;?>">
															&nbsp;
														</td>
														<td align="center">
															<select class="form-control" name="reason_<?php echo $i; ?>" id="reason_<?php echo $i; ?>">
																<option value="0" <?php if($reason==0) { echo "selected"; } ?>>--</option>
																<option value="20" <?php if($reason==20) { echo "selected"; } ?>>Change of main diagnosis</option>
																<option value="1" <?php if($reason==1) { echo "selected"; } ?>>Insufficient response (primary)</option>
																<option value="2" <?php if($reason==2) { echo "selected"; } ?>>Secondary failure</option>
																<option value="3" <?php if($reason==3) { echo "selected"; } ?>>Failure & adverse event</option>
																<option value="4" <?php if($reason==4) { echo "selected"; } ?>>Lost to follow-up</option>
																<option value="5" <?php if($reason==5) { echo "selected"; } ?>>Drug intolerance</option>
																<option value="6" <?php if($reason==6) { echo "selected"; } ?>>Drug reaction/Allergy</option>
																<option value="7" <?php if($reason==7) { echo "selected"; } ?>>Infection</option>
																<option value="8" <?php if($reason==8) { echo "selected"; } ?>>Remission</option>
																<option value="9" <?php if($reason==9) { echo "selected"; } ?>>Solid tumor/Lymphoma</option>
																<option value="10" <?php if($reason==10) { echo "selected"; } ?>>Other adverse event</option>
																<option value="11" <?php if($reason==11) { echo "selected"; } ?>>Patient decision</option>
																<option value="12" <?php if($reason==12) { echo "selected"; } ?>>Planned therapy</option>
																<option value="13" <?php if($reason==13) { echo "selected"; } ?>>Pregnancy</option>
																<option value="14" <?php if($reason==14) { echo "selected"; } ?>>Financial</option>
																<option value="15" <?php if($reason==15) { echo "selected"; } ?>>Change to BIOsimilar</option>
																<option value="16" <?php if($reason==16) { echo "selected"; } ?>>Change practical reasons</option>
																<option value="17" <?php if($reason==17) { echo "selected"; } ?>>Change to bio-originator</option>
																<option value="18" <?php if($reason==18) { echo "selected"; } ?>>Other</option>
																<option value="19" <?php if($reason==19) { echo "selected"; } ?>>Unknown</option>
																
															</select>
														</td>
														<td align="center">
															<select class="form-control" name="frequency_<?php echo $i; ?>" id="frequency_<?php echo $i; ?>">
																<option value="0" <?php if($frequency==0) { echo "selected"; } ?>>--</option>
																<?php
																	$sql = get_drugs_frequencies();
																	$numrows = pg_num_rows($sql);
																	while($result3 = pg_fetch_array($sql))
																	{
																		$frequency_id=$result3['drugs_frequency_id'];
																		$value = $result3['value'];
																		
																		
																		
																?>
																<option value="<?php echo $frequency_id; ?>" <?php if($frequency==$frequency_id) { echo "selected"; } ?>><?php echo $value; ?></option>
																<?php
																	}
																?>
															</select>
														</td>
														<td align="center">
															<input type="text" class="form-control" id="dose_<?php echo $i; ?>" name="dose_<?php echo $i; ?>" value="<?php echo $dose	; ?>">
														</td>
														<td align="center">
															<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="startdate_<?php echo $i; ?>"  name="startdate_<?php echo $i; ?>" value="<?php echo $startdate_str; ?>">
														</td>
														<td align="center">
															<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="stopdate_<?php echo $i; ?>"  name="stopdate_<?php echo $i; ?>" value="<?php echo $stopdate_str; ?>">
														</td>
														<td align="center">
															<a id="a2_<?php echo $i; ?>" name="a2_<?php echo $i; ?>" title="Delete" href="javascript:void(0);" onclick="patient_cohort_drugs_period_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
														</td>
														 </tr>
											<?php
														}
													}
												
											?>
											</tbody>
										</table>
										<input type="hidden" name="patient_cohort_drugs_numofrows" id="patient_cohort_drugs_numofrows" value="<?php echo $i; ?>">
										<br>
										 <a class="btn btn-primary" href="javascript:void(0);" onclick="patient_cohort_drugs_addrow();"><i class="fa fa-plus"></i> New record</a>
									 </div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- treatments end div -->
				
				<!-- Corticosteroids start div -->
				<div class="row">
					<div class="col-md-12">
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Parenteral Corticosteroids from last follow up</small></h3>
							</div>
							<div class="box-body">
								<div class="row">
									<div class="form-group col-md-12" id="divcorticosteroids" name="divcorticosteroids">
									<?php
												$sql = get_corticosteroids();
												$corticosteroids_counter=0;
												
												while($result2 = pg_fetch_array($sql))
												{	
													$corticosteroids_values .= $result2['code'];
													$corticosteroids_values .= "-";
													$corticosteroids_values .= $result2['substance'];
													$corticosteroids_values .= "!@#$%^" ;
													
													$corticosteroids_ids .=$result2['corticosteroids_id'];
													$corticosteroids_ids .= "!@#$%^" ;
													
													$corticosteroids_counter++;
												}
												
												$q1 = "select weekdosage from patient_lookup_drugs where deleted=0 and patient_followup_id=$patient_followup_id and drugs_id=0 and drug_flag=2";
												$q_exec = pg_query($q1);
												$q_reason=pg_fetch_array($q_exec); 
												$weekdosage=$q_reason['weekdosage'];
												
												$q1 = "select weekdosage from patient_lookup_drugs where deleted=0 and patient_followup_id=$patient_followup_id and drugs_id=1 and drug_flag=2";
												$q_exec = pg_query($q1);
												$q_reason=pg_fetch_array($q_exec); 
												$weekdosage1=$q_reason['weekdosage'];
												
												$q1 = "select weekdosage from patient_lookup_drugs where deleted=0 and patient_followup_id=$patient_followup_id and drugs_id=2 and drug_flag=2";
												$q_exec = pg_query($q1);
												$q_reason=pg_fetch_array($q_exec); 
												$weekdosage2=$q_reason['weekdosage'];
											?>
												<input type="hidden" value="<?php echo $corticosteroids_counter; ?>" id="corticosteroids_counter" name="corticosteroids_counter"/>
												<input type="hidden" value="<?php echo $corticosteroids_values; ?>" id="corticosteroids_values" name="corticosteroids_values"/>
												<input type="hidden" value="<?php echo $corticosteroids_ids; ?>" id="corticosteroids_ids" name="corticosteroids_ids"/>
										  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="patient_corticosteroids_tbl" name="patient_corticosteroids_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-6"><b>Corticosteroids</b></th>
													<th class="col-md-4"><b>Date</b></th>
													<th class="col-md-2">&nbsp;</th>
												</tr>
											 </thead>
											 <tbody>
											<?php
												$i=0;
												$j=0;
												
												
														
													$order = get_patient_corticosteroids($patient_followup_id);
													$num_rows = pg_num_rows($order);
													while($result = pg_fetch_assoc($order))
													{
														$i++;
														$j++;
														$patient_corticosteroids_id=$result['patient_corticosteroids_id'];
														$corticosteroids_id=$result['corticosteroids_id'];
														$date_str=$result['date_str'];
														if ($date_str=='12-12-1900')
															$date_str='';
											?>
												  <tr id="patient_corticosteroids_tr_<?php echo $i;?>">
														<td align="center">
															<input type="hidden" id="patient_corticosteroids_hidden_<?php echo $i;?>" name="patient_corticosteroids_hidden_<?php echo $i;?>" value="1"/>
															<input type="hidden" name="patient_corticosteroids_id_<?php echo $i; ?>" id="patient_corticosteroids_id_<?php echo $i; ?>" value="<?php echo $patient_corticosteroids_id;?>">
															<select  class="form-control" name="corticosteroids_id_<?php echo $i; ?>" id="corticosteroids_id_<?php echo $i; ?>">
																<option value="0">--</option>
																<?php
																	$sql = get_corticosteroids();
																	$numrows = pg_num_rows($sql);
																	while($result2 = pg_fetch_array($sql))
																	{
																		$corticosteroids_id2=$result2['corticosteroids_id'];
																		$value = $result2['code'];
																		$value .= "-";
																		$value .= $result2['substance'];
																?>
																<option value="<?php echo $corticosteroids_id2; ?>" <?php if($corticosteroids_id==$corticosteroids_id2) { echo "selected"; } ?>><?php echo $value; ?></option>
																<?php
																	}
																?>
															</select>
														</td>
														 <td align="center">
															<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="corticosteroids_date_<?php echo $i; ?>" name="corticosteroids_date_<?php echo $i; ?>" value="<?php echo $date_str; ?>">
														</td>
													 	<td align="center">
															<a id="a_corticosteroids_<?php echo $i; ?>" name="a_corticosteroids_<?php echo $i; ?>" title="Delete" href="javascript:void(0);" onclick="patient_corticosteroids_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
														</td>
													</tr>
											<?php
													}
											?>
											</tbody>
										</table>
										 <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="patient_corticosteroids_tbl2" name="patient_corticosteroids_tbl2"  width="100%"  rules="all">
											<thead>
												<tr align="center"> 
													<th><b>J 3 month dosage: <?php echo $weekdosage; ?></b></th>
													<th><b>J1 3 month dosage: <?php echo $weekdosage1; ?></b></th>
													<th><b>J2 3 month dosage: <?php echo $weekdosage2; ?></b></th>
												</tr>
											</thead>
											</table>
										<input type="hidden" name="patient_corticosteroids_numofrows" id="patient_corticosteroids_numofrows" value="<?php echo $i; ?>">
										<br>
										 <a class="btn btn-primary" href="javascript:void(0);" onclick="patient_corticosteroids_addrow();"><i class="fa fa-plus"></i> New record</a>
									 </div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- Corticosteroids end div -->
				
				<!-- NSAIDs_Analgesics start div -->
				<div class="row">
					<div class="col-md-12">
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">NSAIDs/Analgesics (Last 3 months)</small></h3>
							</div>
							<div class="box-body">
								<div class="row">
									<div class="form-group col-md-12" id="divAnalgesics" name="divAnalgesics">
									<?php
												$sql = get_analgesics();
												$analgesics_counter=0;
												
												while($result2 = pg_fetch_array($sql))
												{	
													$analgesics_values .= $result2['code'];
													$analgesics_values .= "-";
													$analgesics_values .= $result2['substance'];
													$analgesics_values .= "!@#$%^" ;
													
													$analgesics_ids .=$result2['analgesics_id'];
													$analgesics_ids .= "!@#$%^" ;
													
													$analgesics_counter++;
												}
											?>
												<input type="hidden" value="<?php echo $analgesics_counter; ?>" id="analgesics_counter" name="analgesics_counter"/>
												<input type="hidden" value="<?php echo $analgesics_values; ?>" id="analgesics_values" name="analgesics_values"/>
												<input type="hidden" value="<?php echo $analgesics_ids; ?>" id="analgesics_ids" name="analgesics_ids"/>
												
												<?php
												
													$intake_values .= "None (N=0)!@#$%^" ;
													$intake_ids .= "1!@#$%^" ;
													$intake_values .= "< 1 day/week (N=0.5/7)!@#$%^" ;
													$intake_ids .= "2!@#$%^" ;
													$intake_values .= "1-3 days/week (N=2/7)!@#$%^" ;
													$intake_ids .= "3!@#$%^" ;
													$intake_values .= "3-5 days/week (N=4/7)!@#$%^" ;
													$intake_ids .= "4!@#$%^" ;
													$intake_values .= "≥ 5 days/week  (N=6/7)!@#$%^" ;
													$intake_ids .= "5!@#$%^" ;
													$intake_values .= "Everyday (N= 7/7)!@#$%^" ;
													$intake_ids .= "6!@#$%^" ;
											?>
												<input type="hidden" value="6" id="intake_counter" name="intake_counter"/>
												<input type="hidden" value="<?php echo $intake_values; ?>" id="intake_values" name="intake_values"/>
												<input type="hidden" value="<?php echo $intake_ids; ?>" id="intake_ids" name="intake_ids"/>
										  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="patient_analgesics_tbl" name="patient_analgesics_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-3"><b>Analgesics</b></th>
													<th class="col-md-3"><b>Days with intake</b></th>
													<th class="col-md-2"><b>Starting Date</b></th>
													<th class="col-md-2"><b>Ending Date</b></th>
													<th class="col-md-2">&nbsp;</th>
												</tr>
											 </thead>
											 <tbody>
											<?php
												$i=0;
												$j=0;
													$order = get_patient_analgesics($patient_followup_id);
													$num_rows = pg_num_rows($order);
													while($result = pg_fetch_assoc($order))
													{
														$i++;
														$j++;
														$patient_analgesics_id=$result['patient_analgesics_id'];
														$analgesics_id=$result['analgesics_id'];
														$intake=$result['intake'];
														$startdate_str=$result['startdate_str'];
														if ($startdate_str=='12-12-1900')
															$startdate_str='';
														$stopdate_str=$result['stopdate_str'];
														if ($stopdate_str=='12-12-1900')
															$stopdate_str='';
														
														$q1 = "select weekdosage from patient_lookup_drugs where deleted=0 and patient_followup_id=$patient_followup_id and drugs_id=$analgesics_id and drug_flag=3";
														$q_exec = pg_query($q1);
														$q_reason=pg_fetch_array($q_exec); 
														$weekdosage=$q_reason['weekdosage'];
											?>
												  <tr id="patient_analgesics_tr_<?php echo $i;?>">
														<td align="center">
															<input type="hidden" id="patient_analgesics_hidden_<?php echo $i;?>" name="patient_analgesics_hidden_<?php echo $i;?>" value="1"/>
															<input type="hidden" name="patient_analgesics_id_<?php echo $i; ?>" id="patient_analgesics_id_<?php echo $i; ?>" value="<?php echo $patient_analgesics_id;?>">
															<select  class="form-control" name="analgesics_id_<?php echo $i; ?>" id="analgesics_id_<?php echo $i; ?>">
																<option value="0">--</option>
																<?php
																	$sql = get_analgesics();
																	$numrows = pg_num_rows($sql);
																	while($result2 = pg_fetch_array($sql))
																	{
																		$analgesics_id2=$result2['analgesics_id'];
																		$value = $result2['code'];
																		$value .= "-";
																		$value .= $result2['substance'];
																?>
																<option value="<?php echo $analgesics_id2; ?>" <?php if($analgesics_id==$analgesics_id2) { echo "selected"; } ?>><?php echo $value; ?></option>
																<?php
																	}
																?>
															</select>
															<?php echo "3 month NSAID index: $weekdosage"; ?>
														</td>
														<td align="center">
															<select  class="form-control" name="intake_<?php echo $i; ?>" id="intake_<?php echo $i; ?>">
																<option value="0">--</option>
																<option value="1" <?php if($intake==1) { echo "selected"; } ?>>None (N=0)</option>
																<option value="2" <?php if($intake==2) { echo "selected"; } ?>>< 1 day/week (N=0.5/7)</option>
																<option value="3" <?php if($intake==3) { echo "selected"; } ?>>1-3 days/week (N=2/7)</option>
																<option value="4" <?php if($intake==4) { echo "selected"; } ?>>3-5 days/week (N=4/7)</option>
																<option value="5" <?php if($intake==5) { echo "selected"; } ?>>≥ 5 days/week  (N=6/7)</option>
																<option value="6" <?php if($intake==6) { echo "selected"; } ?>>Everyday (N= 7/7)</option>
																
															</select>
														</td>
														 <td align="center">
															<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="startdate_analgesics_<?php echo $i; ?>" name="startdate_analgesics_<?php echo $i; ?>" value="<?php echo $startdate_str; ?>">
														</td>
														<td align="center">
															<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="stopdate_analgesics_<?php echo $i; ?>" name="stopdate_analgesics_<?php echo $i; ?>" value="<?php echo $stopdate_str; ?>">
														</td>
													 	<td align="center">
															<a id="a_analgesics_<?php echo $i; ?>" name="a_analgesics_<?php echo $i; ?>" title="Delete" href="javascript:void(0);" onclick="patient_analgesics_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
														</td>
													</tr>
											<?php
													}
											?>
											</tbody>
										</table>
										<input type="hidden" name="patient_analgesics_numofrows" id="patient_analgesics_numofrows" value="<?php echo $i; ?>">
										<br>
										 <a class="btn btn-primary" href="javascript:void(0);" onclick="patient_analgesics_addrow();"><i class="fa fa-plus"></i> New record</a>
									 </div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- NSAIDs_Analgesics end div -->
				
				<!-- Disease activity of life start div -->
				<div class="row">
					<div class="col-md-12">
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Disease activity</small></h3>
							</div>
							<div class="box-body">
								<div class="row">
									<div class="col-md-4">
										<label>Common</label>
											<select class="form-control" name="common" id="common">
												<option value="0" <?php if($common==0) { echo "selected"; } ?>>--</option>
												<option value="1" <?php if($common==1) { echo "selected"; } ?>>No</option>
												<option value="2" <?php if($common==2) { echo "selected"; } ?>>Yes</option>
											</select>
									</div>
									<?php
										if($common==2) {
											$order = get_common($patient_followup_id);
											$result = pg_fetch_array($order);
											$patient_common_id=$result['patient_common_id'];
											$vaspain=$result['vaspain'];
											if ($vaspain=="")
												$vaspain=-1;
											$vasglobal=$result['vasglobal'];
											if ($vasglobal=="")
												$vasglobal=-1;
											$vasfatigue=$result['vasfatigue'];
											if ($vasfatigue=="")
												$vasfatigue=-1;
											$vasphysicial=$result['vasphysicial'];
											if ($vasphysicial=="")
												$vasphysicial=-1;
											$swollenjc28=$result['swollenjc28'];
											if ($swollenjc28=="")
												$swollenjc28=-1;
											$tenderjc28=$result['tenderjc28'];
											if ($tenderjc28 =="")
												$tenderjc28=-1;
											$swollenjc44=$result['swollenjc44'];
											if ($swollenjc44 =="")
												$swollenjc44=-1;
											$tenderjc44=$result['tenderjc44'];
											if ($tenderjc44 =="")
												$tenderjc44=-1;
											$swollenjc66=$result['swollenjc66'];
											if ($swollenjc66 =="")
												$swollenjc66=-1;
											$tenderjc68=$result['tenderjc68'];
											if ($tenderjc68 =="")
												$tenderjc68=-1;
											$esr=$result['esr'];
											$crp=$result['crp'];
											$haq=$result['haq'];
											$mhaq=$result['mhaq'];
											$das28esr=$result['das28esr'];
											$das28esr3v=$result['das28esr3v'];
											
											$das28crp=$result['das28crp'];
											$das28crp3v=$result['das28crp3v'];
											$sdai=$result['sdai'];
											$cdai=$result['cdai'];
											
											if($patient_followup_id=='' or $patient_followup_id=='0')
											{
												$order_prv = get_prv_common($pat_id);
												$result_prv = pg_fetch_array($order_prv);
												$weight=$result_prv['weight'];
												$height=$result_prv['height'];
												$bmi=$result_prv['bmi'];
											}
											else
											{
												$weight=$result['weight'];
												$height=$result['height'];
												$bmi=$result['bmi'];
											}
										}
									?>
									<input type="hidden" id="patient_common_id" name="patient_common_id" value="<?php echo $patient_common_id; ?>">
									<div class="form-group col-md-12" id="divcommon" name="divcommon" <?php if($common==2) { ?> style="DISPLAY:block;" <?php } else { ?>  style="DISPLAY:none;"  <?php } ?>>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divcommon1_tbl" name="divcommon1_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>VAS pain</b></th>
													<th class="col-md-2"><b>VAS global</b></th>
													<th class="col-md-3"><b>VAS fatigue</b></th>
													<th class="col-md-3"><b>VAS physician</b></th>
													<th class="col-md-2"><b>28Swollen JC</b></th>
													
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="vaspain" id="vaspain">
															<option value="-1" <?php if($vaspain==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=100;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($vaspain==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="vasglobal" id="vasglobal">
															<option value="-1" <?php if($vasglobal==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=100;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($vasglobal==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="vasfatigue" id="vasfatigue">
															<option value="-1" <?php if($vasfatigue==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=100;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($vasfatigue==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="vasphysicial" id="vasphysicial">
															<option value="-1" <?php if($vasphysicial==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=100;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($vasphysicial==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="swollenjc28" id="swollenjc28">
															<option value="-1" <?php if($swollenjc28==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=28;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($swollenjc28==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divcommon2_tbl" name="divcommon2_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>28Tender JC</b></th>
													<th class="col-md-2"><b>44Swollen JC</b></th>
													<th class="col-md-3"><b>44Tender JC</b></th>
													<th class="col-md-3"><b>66Swollen JC</b></th>
													<th class="col-md-2"><b>68Tender JC</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="tenderjc28" id="tenderjc28">
														<option value="-1" <?php if($tenderjc28==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=28;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($tenderjc28==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="swollenjc44" id="swollenjc44">
														<option value="-1" <?php if($swollenjc44==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=44;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($swollenjc44==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="tenderjc44" id="tenderjc44">
														<option value="-1" <?php if($tenderjc44==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=44;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($tenderjc44==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="swollenjc66" id="swollenjc66">
														<option value="-1" <?php if($swollenjc66==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=66;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($swollenjc66==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="tenderjc68" id="tenderjc68">
														<option value="-1" <?php if($tenderjc68==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=68;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($tenderjc68==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divcommon3_tbl" name="divcommon3_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>ESR(mm/hr)</b></th>
													<th class="col-md-3"><b>CRP(mg/dl)</b></th>
													<th class="col-md-3"><b>HAQ</b></th>
													<th class="col-md-2"><b>mHAQ</b></th>													
													<th class="col-md-2"><b>&nbsp;</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<!--<select class="form-control" name="esr" id="esr">
															<?php
																/*for($i=1;$i<=180;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($esr==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}*/
															?>
														</select>-->
														<input type="text" class="form-control" name="esr" id="esr" value="<?php if ($esr=="") {  echo ""; } else { echo $esr; } ?>">
													</td>
													<td>
														<!--<select class="form-control" name="crp" id="crp">
															<?php
																/*for($i=0;$i<=60;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($crp==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}*/
															?>
														</select>-->
														<input type="text" class="form-control" name="crp" id="crp" value="<?php if ($crp=="") {  echo ""; } else { echo $crp; } ?>">
													</td>
													<td>
														<!--<select class="form-control" name="haq" id="haq">
															<?php
																/*for($i=0;$i<=3;$i+=0.1)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if (abs(($haq-$i)/$i) < 0.00001) {  echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}*/
															?>
														</select>-->
														<input type="text" class="form-control" name="haq" id="haq" value="<?php if ($haq=="") {  echo ""; } else { echo $haq; } ?>">
													</td>
													<td>
														<!--<select class="form-control" name="mhaq" id="mhaq">
															<?php
																/*for($i=0;$i<=3;$i+=0.1)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if (abs(($mhaq-$i)/$i) < 0.00001) {  echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}*/
															?>
														</select>-->
														<input type="text" class="form-control" name="mhaq" id="mhaq" value="<?php if ($mhaq=="") {  echo ""; } else { echo $mhaq; } ?>">
													</td>
													<td><b>&nbsp;</b></td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divcommon4_tbl" name="divcommon4_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>DAS28-ESR</b></th>
													<th class="col-md-2"><b>DAS28(ESR)-3v</b></th>
													<th class="col-md-2"><b>DAS28-CRP</b></th>
													<th class="col-md-2"><b>DAS28(CRP)-3v</b></th>
													<th class="col-md-2"><b>SDAI</b></th>
													<th class="col-md-2"><b>CDAI</b></th>
													
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<input type="text" readonly id="das28esr" name="das28esr" value="<?php echo $das28esr; ?>">
													</td>
													<td>
														<input type="text" readonly id="das28esr3v" name="das28esr3v" value="<?php echo $das28esr3v; ?>">
													</td>
													<td>
														<input type="text" readonly id="das28crp" name="das28crp" value="<?php echo $das28crp; ?>">
													</td>
													<td>
														<input type="text" readonly id="das28crp3v" name="das28crp3v" value="<?php echo $das28crp3v; ?>">
													</td>
													<td>
														<input type="text" readonly id="sdai" name="sdai" value="<?php echo $sdai; ?>">
													</td>
													<td>
														<input type="text" readonly id="cdai" name="cdai" value="<?php echo $cdai; ?>">
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divcommon4_tbl" name="divcommon4_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>Body Weight (kg)</b></th>
													<th class="col-md-2"><b>Body Height (cm)</b></th>
													<th class="col-md-2"><b>BMI</b></th>
													<th class="col-md-6">&nbsp;</th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="weight" id="weight">
															<option value="0" <?php if($weight==0) { echo "selected"; } ?>>--</option>
															<?php
																for($i=10;$i<=200;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($weight==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="height" id="height">
															<option value="0" <?php if($height==0) { echo "selected"; } ?>>--</option>
															<?php
																for($i=80;$i<=220;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($height==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<input type="text" readonly id="bmi" name="bmi" value="<?php echo $bmi; ?>">
													</td>
												</tr>
											 </tbody>
										</table>
									 </div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label>SpA</label>
											<select class="form-control" name="spa" id="spa">
												<option value="0" <?php if($spa==0) { echo "selected"; } ?>>--</option>
												<option value="1" <?php if($spa==1) { echo "selected"; } ?>>No</option>
												<option value="2" <?php if($spa==2) { echo "selected"; } ?>>Yes</option>
											</select>
									</div>
									<?php
											$order = get_spa($patient_followup_id);
											
											$result = pg_fetch_array($order);
											$patient_spa_id=$result['patient_spa_id'];
											
											$basdai=$result['basdai'];
											$basdai1=$result['basdai1'];
											
											$basdai2=$result['basdai2'];
											$basdai3=$result['basdai3'];
											$basdai4=$result['basdai4'];
											$basdai5=$result['basdai5'];
											$basdai6=$result['basdai6'];
											for($i=1;$i<=6;$i++)
											{
												
												if(${"basdai".$i}=="")
													${"basdai".$i}=-1;
											}
											$basfi=$result['basfi'];
											$basfi1=$result['basfi1'];
											$basfi2=$result['basfi2'];
											$basfi3=$result['basfi3'];
											$basfi4=$result['basfi4'];
											$basfi5=$result['basfi5'];
											$basfi6=$result['basfi6'];
											$basfi7=$result['basfi7'];
											$basfi8=$result['basfi8'];
											$basfi9=$result['basfi9'];
											$basfi10=$result['basfi10'];
											for($i=1;$i<=10;$i++)
											{
												
												if(${"basfi".$i}=="")
													${"basfi".$i}=-1;
											}
											$asdascrp=$result['asdascrp'];
											$asdasesr=$result['asdasesr'];
											$sparest1=$result['sparest1'];
											$sparest2=$result['sparest2'];
											$sparest3=$result['sparest3'];
											$sparest4=$result['sparest4'];
											$sparest5=$result['sparest5'];
											$sparest6=$result['sparest6'];
											$sparest7=$result['sparest7'];
											$sparest8=$result['sparest8'];
											$sparest9=$result['sparest9'];
											$sparest10=$result['sparest10'];
											for($i=1;$i<=10;$i++)
											{
												
												if(${"sparest".$i}=="")
													${"sparest".$i}=-1;
											}
									?>
									<input type="hidden" id="patient_spa_id" name="patient_spa_id" value="<?php echo $patient_spa_id; ?>">
									<div class="form-group col-md-12" id="divspa" name="divspa" <?php if($spa==2) { ?> style="DISPLAY:block;" <?php } else { ?>  style="DISPLAY:none;"  <?php } ?>>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divspa1_tbl" name="divspa1_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>BASDAI score</b></th>
													<th class="col-md-1"><b>BASDAI1</b></th>
													<th class="col-md-1"><b>BASDAI2</b></th>
													<th class="col-md-2"><b>BASDAI3</b></th>
													<th class="col-md-2"><b>BASDAI4</b></th>
													<th class="col-md-2"><b>BASDAI5</b></th>
													<th class="col-md-2"><b>BASDAI6</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<input type="text" readonly id="basdai" name="basdai" value="<?php echo $basdai; ?>">
													</td>
													<td>
														<select class="form-control" name="basdai1" id="basdai1">
														<option value="-1" <?php if($basdai1==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=10;$i+=0.1)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if ((abs(($basdai1-$i)/$i) < 0.00001) and $basdai1<>"-1") {  echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="basdai2" id="basdai2">
														<option value="-1" <?php if($basdai2=="-1") { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=10;$i+=0.1)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if ( (abs(($basdai2-$i)/$i) < 0.00001) and $basdai2<>"-1") {  echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="basdai3" id="basdai3">
														<option value="-1" <?php if($basdai3==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=10;$i+=0.1)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if ((abs(($basdai3-$i)/$i) < 0.00001) and $basdai3<>"-1") {  echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="basdai4" id="basdai4">
														<option value="-1" <?php if($basdai4==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=10;$i+=0.1)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if ((abs(($basdai4-$i)/$i) < 0.00001) and $basdai4<>"-1") {  echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="basdai5" id="basdai5">
														<option value="-1" <?php if($basdai5==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=10;$i+=0.1)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if ((abs(($basdai5-$i)/$i) < 0.00001) and $basdai5<>"-1") {  echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="basdai6" id="basdai6">
														<option value="-1" <?php if($basdai6==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=10;$i+=0.1)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if ((abs(($basdai6-$i)/$i) < 0.00001) and $basdai6<>"-1") {  echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divspa2_tbl" name="divspa2_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>BASFI score</b></th>
													<th class="col-md-1"><b>BASFI1</b></th>
													<th class="col-md-1"><b>BASFI2</b></th>
													<th class="col-md-1"><b>BASFI3</b></th>
													<th class="col-md-1"><b>BASFI4</b></th>
													<th class="col-md-1"><b>BASFI5</b></th>
													<th class="col-md-1"><b>BASFI6</b></th>
													<th class="col-md-1"><b>BASFI7</b></th>
													<th class="col-md-1"><b>BASFI8</b></th>
													<th class="col-md-1"><b>BASFI9</b></th>
													<th class="col-md-1"><b>BASFI10</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<input type="text" readonly id="basfi" name="basfi" value="<?php echo $basfi; ?>">
													</td>
													<td>
														<select class="form-control" name="basfi1" id="basfi1">
														<option value="-1" <?php if($basfi1==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=10;$i+=0.1)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if ((abs(($basfi1-$i)/$i) < 0.00001) and $basfi1<>"-1") {  echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="basfi2" id="basfi2">
														<option value="-1" <?php if($basfi2==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=10;$i+=0.1)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if ((abs(($basfi2-$i)/$i) < 0.00001) and $basfi2<>"-1") {  echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="basfi3" id="basfi3">
														<option value="-1" <?php if($basfi3==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=10;$i+=0.1)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if ((abs(($basfi3-$i)/$i) < 0.00001) and $basfi3<>"-1") {  echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="basfi4" id="basfi4">
														<option value="-1" <?php if($basfi4==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=10;$i+=0.1)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if ((abs(($basfi4-$i)/$i) < 0.00001) and $basfi4<>"-1") {  echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="basfi5" id="basfi5">
														<option value="-1" <?php if($basfi5==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=10;$i+=0.1)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if ((abs(($basfi5-$i)/$i) < 0.00001) and $basfi5<>"-1") {  echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="basfi6" id="basfi6">
														<option value="-1" <?php if($basfi6==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=10;$i+=0.1)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if ((abs(($basfi6-$i)/$i) < 0.00001) and $basfi6<>"-1") {  echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="basfi7" id="basfi7">
														<option value="-1" <?php if($basfi7==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=10;$i+=0.1)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if ((abs(($basfi7-$i)/$i) < 0.00001) and $basfi7<>"-1") {  echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="basfi8" id="basfi8">
														<option value="-1" <?php if($basfi8==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=10;$i+=0.1)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if ((abs(($basfi8-$i)/$i) < 0.00001) and $basfi8<>"-1") {  echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="basfi9" id="basfi9">
														<option value="-1" <?php if($basfi9==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=10;$i+=0.1)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if ((abs(($basfi9-$i)/$i) < 0.00001) and $basfi9<>"-1") {  echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="basfi10" id="basfi10">
														<option value="-1" <?php if($basfi10==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=10;$i+=0.1)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if ((abs(($basfi10-$i)/$i) < 0.00001) and $basfi10<>"-1") {  echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divspa2_tbl" name="divspa2_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>ASDAS-CRP</b></th>
													<th class="col-md-2"><b>ASDAS-ESR</b></th>
													<th class="col-md-8">&nbsp;</th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<input type="text" readonly id="asdascrp" name="asdascrp" value="<?php echo $asdascrp; ?>">
													</td>
													<td>
														<input type="text" readonly id="asdasesr" name="asdasesr" value="<?php echo $asdasesr; ?>">
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divspa3_tbl" name="divspa3_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-1"><b>SpA Patient Global </b></th>
													<th class="col-md-1"><b>Spondylitis (Yes/No)</b></th>
													<th class="col-md-1"><b>Dactylitis</b></th>
													<th class="col-md-1"><b>Enthesitis (Yes/No)</b></th>
													<th class="col-md-1"><b>Enthesitis-Leeds</b></th>
													<th class="col-md-1"><b>Enthesitis MASES</b></th>
													<th class="col-md-1"><b>Nail psoriasis</b></th>
													<th class="col-md-2"><b>Palmoplantar psoriasis (Yes/No)</b></th>
													<th class="col-md-1"><b>Psoriasis area (%)</b></th>
													<th class="col-md-2"><b>PASI index score</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="sparest1" id="sparest1">
														<option value="-1" <?php if($sparest1==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=10;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($sparest1==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="sparest2" id="sparest2">
															<option value="-1" <?php if($sparest2==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($sparest2==0) { echo "selected"; } ?>>No</option>
															<option value="1" <?php if($sparest2==1) { echo "selected"; } ?>>Yes</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sparest3" id="sparest3">
														<option value="-1" <?php if($sparest3==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=20;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($sparest3==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="sparest4" id="sparest4">
															<option value="-1" <?php if($sparest4==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($sparest4==0) { echo "selected"; } ?>>No</option>
															<option value="1" <?php if($sparest4==1) { echo "selected"; } ?>>Yes</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sparest5" id="sparest5">
														<option value="-1" <?php if($sparest5==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=6;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($sparest5==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="sparest6" id="sparest6">
														<option value="-1" <?php if($sparest6==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=13;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($sparest6==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="sparest7" id="sparest7">
														<option value="-1" <?php if($sparest7==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=20;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($sparest7==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="sparest8" id="sparest8">
															<option value="-1" <?php if($sparest8==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($sparest8==0) { echo "selected"; } ?>>No</option>
															<option value="1" <?php if($sparest8==1) { echo "selected"; } ?>>Yes</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sparest9" id="sparest9">
														<option value="-1" <?php if($sparest9==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=100;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($sparest9==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="sparest10" id="sparest10">
														<option value="-1" <?php if($sparest10==-1) { echo "selected"; } ?>>--</option>
															<?php
																for($i=0;$i<=72;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($sparest10==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label>SLE</label>
											<select class="form-control" name="sle" id="sle">
												<option value="0" <?php if($sle==0) { echo "selected"; } ?>>--</option>
												<option value="1" <?php if($sle==1) { echo "selected"; } ?>>No</option>
												<option value="2" <?php if($sle==2) { echo "selected"; } ?>>Yes</option>
											</select>
									</div>
									<?php
											$order = get_sle($patient_followup_id);
											$result = pg_fetch_array($order);
											$patient_sle_id=$result['patient_sle_id'];
											$pga=$result['pga'];
											$sledai1=$result['sledai1'];
											$sledai2=$result['sledai2'];
											$sledai3=$result['sledai3'];
											$sledai4=$result['sledai4'];
											$sledai5=$result['sledai5'];
											$sledai6=$result['sledai6'];
											$sledai7=$result['sledai7'];
											$sledai8=$result['sledai8'];
											$sledai9=$result['sledai9'];
											$sledai9b=$result['sledai9b'];
											$sledai10=$result['sledai10'];
											$sledai11=$result['sledai11'];
											$sledai12=$result['sledai12'];
											$sledai13=$result['sledai13'];
											$sledai14=$result['sledai14'];
											$sledai15=$result['sledai15'];
											$sledai16=$result['sledai16'];
											$sledai17=$result['sledai17'];
											$sledai18=$result['sledai18'];
											$sledai19=$result['sledai19'];
											$sledai20=$result['sledai20'];
											$sledai21=$result['sledai21'];
											$sledai22=$result['sledai22'];
											$sledai23=$result['sledai23'];
											$sledai24=$result['sledai24'];
											$sledai2k=$result['sledai2k'];
											$sledai15ba=$result['sledai15ba'];
											$sledai15bb=$result['sledai15bb'];
											$sledai15bc=$result['sledai15bc'];
											$sledai15bd=$result['sledai15bd'];
											$sledai15be=$result['sledai15be'];
											$sledai15bf=$result['sledai15bf'];
											$sledai15bg=$result['sledai15bg'];
											$sledai15bh=$result['sledai15bh'];
											$sledai23b=$result['sledai23b'];
											$sledai24ba=$result['sledai24ba'];
											$sledai24bb=$result['sledai24bb'];
											
											$add1=$result['add1'];
											$add2=$result['add2'];
											$add3=$result['add3'];
											$add4=$result['add4'];
											$add5=$result['add5'];
											$add6=$result['add6'];
											$add7=$result['add7'];
											$add8=$result['add8'];
											$add9=$result['add9'];
											$add10=$result['add10'];
											$add11=$result['add11'];
											$add12=$result['add12'];
											$add13=$result['add13'];
											$add14=$result['add14'];
											$add15=$result['add15'];
											$add16=$result['add16'];
											$add17=$result['add17'];
											$add18=$result['add18'];
											$add19=$result['add19'];
											$add20=$result['add20'];
											$add21=$result['add21'];
											$add22=$result['add22'];
											$add23=$result['add23'];
											$add24=$result['add24'];
											$add25=$result['add25'];
											$add26=$result['add26'];
											
											$flare1=$result['flare1'];
											$flare2=$result['flare2'];
											$flare3=$result['flare3'];
											$flare4=$result['flare4'];
											$flare5=$result['flare5'];
											$flare6=$result['flare6'];
											$flare7=$result['flare7'];
											$flare8=$result['flare8'];
											$flare9=$result['flare9'];
											$flare10=$result['flare10'];
											$flare11=$result['flare11'];
											$flare12=$result['flare12'];
											$flare13=$result['flare13'];
											$flare14=$result['flare14'];
											
											$nefritis1=$result['nefritis1'];
											$nefritis2=$result['nefritis2'];
											
											
									?>
									<input type="hidden" id="patient_sle_id" name="patient_sle_id" value="<?php echo $patient_sle_id; ?>">
									<div class="form-group col-md-12" id="divsle" name="divsle" <?php if($sle==2) { ?> style="DISPLAY:block;" <?php } else { ?>  style="DISPLAY:none;"  <?php } ?>>
										</br>
										<div class="row">
											<div  class="form-group col-md-4" id="divphysicianglobal" name="divphysicianglobal">
												<label>Physician Global Assessment</label>
												<select class="form-control" name="pga" id="pga">
													<option value="0" <?php if($pga=="0") { echo "selected"; } ?>>0</option>
													<option value="0.5" <?php if($pga=="0.5") { echo "selected"; } ?>>0.5</option>
													<option value="1" <?php if($pga=="1") { echo "selected"; } ?>>1</option>
													<option value="1.5" <?php if($pga=="1.5") { echo "selected"; } ?>>1.5</option>
													<option value="2" <?php if($pga=="2") { echo "selected"; } ?>>2</option>
													<option value="2.5" <?php if($pga=="2.5") { echo "selected"; } ?>>2.5</option>
													<option value="3" <?php if($pga=="3") { echo "selected"; } ?>>3</option>
												</select>
											</div>
										</div>
										</br>
										<label>SLEDAI-2K</label>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsle1_tbl" name="divsle1_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>SLEDAI1 -Seizure</b></th>
													<th class="col-md-2"><b>SLEDAI2 -Psychosis</b></th>
													<th class="col-md-2"><b>SLEDAI3 -Confusion/altered mental status</b></th>
													<th class="col-md-2"><b>SLEDAI4 -Optic neuritis / retinal exudate</b></th>
													<th class="col-md-2"><b>SLEDAI5 -Cranial neuropathy </b></th>
													<th class="col-md-2"><b>SLEDAI6 -Headache</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="sledai1" id="sledai1">
															<option value="0" <?php if($sledai1==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai1==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai1=='' or $sledai1==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sledai2" id="sledai2">
															<option value="0" <?php if($sledai2==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai2==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai2=='' or $sledai2==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sledai3" id="sledai3">
															<option value="0" <?php if($sledai3==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai3==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai3=='' or $sledai3==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sledai4" id="sledai4">
															<option value="0" <?php if($sledai4==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai4==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai4=='' or $sledai4==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sledai5" id="sledai5">
															<option value="0" <?php if($sledai5==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai5==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai5=='' or $sledai5==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sledai6" id="sledai6">
															<option value="0" <?php if($sledai6==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai6==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai6=='' or $sledai6==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsle2_tbl" name="divsle2_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>SLEDAI7 -Cerebrovascular disease</b></th>
													<th class="col-md-2"><b>SLEDAI8 -Vasculitis</b></th>
													<th class="col-md-2"><b>SLEDAI9 -Arthritis</b></th>
													<th class="col-md-2"><b>SLEDAI10 -Myositis</b></th>
													<th class="col-md-2"><b>SLEDAI11 -Urine casts</b></th>
													<th class="col-md-2">&nbsp;</th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="sledai7" id="sledai7">
															<option value="0" <?php if($sledai7==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai7==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai7=='' or $sledai7==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sledai8" id="sledai8">
															<option value="0" <?php if($sledai8==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai8==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai8=='' or $sledai8==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sledai9" id="sledai9">
															<option value="0" <?php if($sledai9==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai9==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai9=='' or $sledai9==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sledai10" id="sledai10">
															<option value="0" <?php if($sledai10==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai10==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai10=='' or $sledai10==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sledai11" id="sledai11">
															<option value="0" <?php if($sledai11==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai11==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai11=='' or $sledai11==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>&nbsp;</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsle2b_tbl" name="divsle2b_tbl"  width="100%"  rules="all">
											<tbody>
												<tr align="center">
													<td class="col-md-2">&nbsp;</td>
													<td class="col-md-2">&nbsp;</td>
													<td class="col-md-2">
														<div  name="tdsledai9b" id="tdsledai9b">
														<select class="form-control" name="sledai9b" id="sledai9b">
															<option value="0" <?php if($sledai9b==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if($sledai9b==1) { echo "selected"; } ?>>1 large joint</option>
															<option value="2" <?php if($sledai9b==2) { echo "selected"; } ?>>2-10 large joints</option>
															<option value="3" <?php if($sledai9b==3) { echo "selected"; } ?>>1-3 small joints (with/without large joints)</option>
															<option value="4" <?php if($sledai9b==4) { echo "selected"; } ?>>4-10 small joints (with/without large joints)</option>
															<option value="5" <?php if($sledai9b==5) { echo "selected"; } ?>>> 10 joints</option>
														</select>
														</div>
													</td>
													<td class="col-md-2">&nbsp;</td>
													<td class="col-md-2">&nbsp;</td>
													<td class="col-md-2">&nbsp;</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsle3_tbl" name="divsle3_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>SLEDAI12 -Hematuria</b></th>
													<th class="col-md-2"><b>SLEDAI13 -Proteinuria</b></th>
													<th class="col-md-2"><b>SLEDAI14 -Pyuria</b></th>
													<th class="col-md-2"><b>SLEDAI15 -Inflammatory skin rash</b></th>
													<th class="col-md-2"><b>SLEDAI16 -Hair loss</b></th>
													<th class="col-md-2"><b>&nbsp;</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr align="center">
													<td>
														<select class="form-control" name="sledai12" id="sledai12">
															<option value="0" <?php if($sledai12==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai12==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai12=='' or $sledai12==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sledai13" id="sledai13">
															<option value="0" <?php if($sledai13==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai13==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai13=='' or $sledai13==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sledai14" id="sledai14">
															<option value="0" <?php if($sledai14==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai14==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai14=='' or $sledai14==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sledai15" id="sledai15">
															<option value="0" <?php if($sledai15==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai15==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai15=='' or $sledai15==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sledai16" id="sledai16">
															<option value="0" <?php if($sledai16==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai16==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai16=='' or $sledai16==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsle4_tbl" name="divsle4_tbl"  width="100%"  rules="all">
											<tbody>
												<tr align="center">
													<td class="col-md-2">
														<div name="tdsledai12b" id="tdsledai12b">
														<select class="form-control" name="sledai12b" id="sledai12b">
															<option value="0" <?php if($sledai12b==0) { echo "selected"; } ?>>5 to 9 RBC/hpf</option>
															<option value="1" <?php if($sledai12b==1) { echo "selected"; } ?>>10 to 49 RBC/hpf</option>
															<option value="2" <?php if($sledai12b==2) { echo "selected"; } ?>>>=50 RBC/hpf</option>
														</select>
														</div>
													</td>
													<td class="col-md-2">
														<div  name="tdsledai13b" id="tdsledai13b">
														<select class="form-control" name="sledai13b" id="sledai13b">
															<option value="0" <?php if($sledai13b==0) { echo "selected"; } ?>>proteinuria 0.5-1 g/24hr</option>
															<option value="1" <?php if($sledai13b==1) { echo "selected"; } ?>>proteinuria 1-3 g/24hr</option>
															<option value="2" <?php if($sledai13b==2) { echo "selected"; } ?>>proteinuria >=3 g/24hr</option>
														</select>
														</div>
													</td>
													<td class="col-md-2">
														<div  name="tdsledai14b" id="tdsledai14b">
														<select class="form-control" name="sledai14b" id="sledai14b">
															<option value="0" <?php if($sledai14b==0) { echo "selected"; } ?>>5 to 9 WBC/hpf</option>
															<option value="1" <?php if($sledai14b==1) { echo "selected"; } ?>>10 to 49 WBC/hpf</option>
															<option value="2" <?php if($sledai14b==2) { echo "selected"; } ?>>>=50 WBC/hpf</option>
														</select>
														</div>
													</td>
													<td class="col-md-2">
														<div  name="tdsledai15b" id="tdsledai15b">
														head (9%)&nbsp;<input <?php if($sledai15ba==1){ echo "checked"; } ?> type="checkbox" name="sledai15ba" id="sledai15ba"></br>
														Right arm (9%)&nbsp;<input <?php if($sledai15bb==1){ echo "checked"; } ?> type="checkbox" name="sledai15bb" id="sledai15bb"></br>
														Left arm (9%)&nbsp;<input <?php if($sledai15bc==1){ echo "checked"; } ?> type="checkbox" name="sledai15bc" id="sledai15bc"></br>
														Back (18%)&nbsp;<input <?php if($sledai15bd==1){ echo "checked"; } ?> type="checkbox" name="sledai15bd" id="sledai15bd"></br>
														Chest (18%)&nbsp;<input <?php if($sledai15be==1){ echo "checked"; } ?> type="checkbox" name="sledai15be" id="sledai15be"></br>
														Perineum (1%)&nbsp;<input <?php if($sledai15bf==1){ echo "checked"; } ?> type="checkbox" name="sledai15bf" id="sledai15bf"></br>
														Left leg (18%)&nbsp;<input <?php if($sledai15bg==1){ echo "checked"; } ?> type="checkbox" name="sledai15bg" id="sledai15bg"></br>
														Right leg (18%)&nbsp;<input <?php if($sledai15bh==1){ echo "checked"; } ?> type="checkbox" name="sledai15bh" id="sledai15bh">
														</div>
													</td>
													<td class="col-md-2">
														<div  name="tdsledai16b" id="tdsledai16b">
														<select class="form-control" name="sledai16b" id="sledai16b">
															<option value="0" <?php if($sledai16b==0) { echo "selected"; } ?>>Mild/modarate hair loss</option>
															<option value="1" <?php if($sledai16b==1) { echo "selected"; } ?>>Severe hair loss or alopecia</option>
														</select>
														</div>
													</td>
													<td class="col-md-2">&nbsp;</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsle5_tbl" name="divsle5_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>SLEDAI17 -Mucosal ulcers</b></th>
													<th class="col-md-2"><b>SLEDAI18 -Pleural effusion</b></th>
													<th class="col-md-2"><b>SLEDAI19 -Pericarditis</b></th>
													<th class="col-md-2"><b>SLEDAI20 -Low serum C3/C4</b></th>
													<th class="col-md-2"><b>SLEDAI21 -High serum anti-dsDNA</b></th>
													<th class="col-md-2"><b>SLEDAI22 -Fever</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="sledai17" id="sledai17">
															<option value="0" <?php if($sledai17==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai17==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai17=='' or $sledai17==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sledai18" id="sledai18">
															<option value="0" <?php if($sledai18==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai18==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai18=='' or $sledai18==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sledai19" id="sledai19">
															<option value="0" <?php if($sledai19==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai19==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai19=='' or $sledai19==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sledai20" id="sledai20">
															<option value="0" <?php if($sledai20==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai20==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai20==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sledai21" id="sledai21">
															<option value="0" <?php if($sledai21==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai21==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai21==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sledai22" id="sledai22">
															<option value="0" <?php if($sledai22==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sleda22==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai22=='' or $sledai22==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsle6_tbl" name="divsle6_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													
													<th class="col-md-3"><b>SLEDAI23 -Thrombocytopenia</b></th>
													<th class="col-md-4"><b>SLEDAI24 -Leucopenia</b></th>
													<th class="col-md-2"><b>SLEDAI-2K</b></th>
													<th class="col-md-3">&nbsp;</th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="sledai23" id="sledai23">
															<option value="0" <?php if($sledai23==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai23==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai23==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sledai24" id="sledai24">
															<option value="0" <?php if($sledai24==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($sledai24==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($sledai24==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<input class="form-control" type="text" readonly="true" id="sledai2k" name="sledai2k" value="<?php echo $sledai2k; ?>">
													</td>
													<td>&nbsp;</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsle7_tbl" name="divsle7_tbl"  width="100%"  rules="all">
											<tbody>
												<tr align="center">
													<td class="col-md-3">
														<div name="tdsledai23b" id="tdsledai23b">
														<select class="form-control" name="sledai23b" id="sledai23b">
															<option value="0" <?php if($sledai23b==0) { echo "selected"; } ?>>PLTs 75000 to 100000/mm3</option>
															<option value="1" <?php if($sledai23b==1) { echo "selected"; } ?>>PLTs 50000 to 75000/mm3</option>
															<option value="2" <?php if($sledai23b==2) { echo "selected"; } ?>>PLTs 25000 to 50000/mm3</option>
															<option value="3" <?php if($sledai23b==3) { echo "selected"; } ?>>PLTs <25000/mm3</option>
														</select>
														</div>
													</td>
													<td class="col-md-4">
														<div  name="tdsledai24b" id="tdsledai24b">
														<select class="form-control" name="sledai24ba" id="sledai24ba">
															<option value="0" <?php if($sledai24ba==0) { echo "selected"; } ?>>Neutrophils 500 to 1000/mm3</option>
															<option value="1" <?php if($sledai24ba==1) { echo "selected"; } ?>>Neutrophils <500/mm3</option>
														</select></br>
														Lymphocytes <500/mm3 &nbsp;<input type="checkbox" <?php if($sledai24bb==1){ echo "checked"; } ?> name="sledai24bb" id="sledai24bb"></br>
														</div>
													</td>
													<td class="col-md-5">&nbsp;</td>
												</tr>
											 </tbody>
										</table>
								</br>		
								<label>Additional items</label>	 
								<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsle8_tbl" name="divsle8_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>Constitutional: Fatigue</b></th>
													<th class="col-md-2"><b>Constitutional: Lymphadenopathy</b></th>
													<th class="col-md-2"><b>Constitutional: Other</b></th>
													<th class="col-md-2"><b>Mucocutaneous: Panniculitis</b></th>
													<th class="col-md-2"><b>Mucocutaneous: Urticaria</b></th>
													<th class="col-md-2"><b>Mucocutaneous: Other</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="add1" id="add1">
															<option value="0" <?php if($add1==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add1==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add1=='' or $add1==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add2" id="add2">
															<option value="0" <?php if($add2==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add2==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add2=='' or $add2==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add3" id="add3">
															<option value="0" <?php if($add3==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add3==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add3=='' or $add3==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add4" id="add4">
															<option value="0" <?php if($add4==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add4==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add4=='' or $add4==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add5" id="add5">
															<option value="0" <?php if($add5==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add5==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add5=='' or $add5==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add6" id="add6">
															<option value="0" <?php if($add6==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add6==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add6=='' or $add6==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>
								<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsle9_tbl" name="divsle9_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>Gastrointestinal: Hepatitis</b></th>
													<th class="col-md-2"><b>Gastrointestinal: Ascites</b></th>
													<th class="col-md-2"><b>Gastrointestinal: Enteropathy</b></th>
													<th class="col-md-2"><b>Gastrointestinal: Mesenteric vasculitis</b></th>
													<th class="col-md-2"><b>Gastrointestinal: Other</b></th>
													<th class="col-md-2"><b>Respiratory: Alveolitis / pneumonitis</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="add7" id="add7">
															<option value="0" <?php if($add7==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add7==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add7=='' or $add7==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add8" id="add8">
															<option value="0" <?php if($add8==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add8==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add8=='' or $add8==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add9" id="add9">
															<option value="0" <?php if($add9==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add9==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add9=='' or $add9==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add10" id="add10">
															<option value="0" <?php if($add10==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add10==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add10=='' or $add10==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add11" id="add11">
															<option value="0" <?php if($add11==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add11==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add11=='' or $add11==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add12" id="add12">
															<option value="0" <?php if($add12==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add12==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add12=='' or $add12==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>
								<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsle10_tbl" name="divsle10_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>Respiratory: Alveolar hemorrhage / lung vasculitis</b></th>
													<th class="col-md-2"><b>Respiratory: Other</b></th>
													<th class="col-md-2"><b>Hematology: Autoimmune hemolytic anemia Hb <=8 g/dL</b></th>
													<th class="col-md-2"><b>Hematology: TTP or TTP-like</b></th>
													<th class="col-md-2"><b>Hematology: Splenomegaly</b></th>
													<th class="col-md-2"><b>Hematology: Other</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="add13" id="add13">
															<option value="0" <?php if($add13==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add13==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add13=='' or $add13==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add14" id="add14">
															<option value="0" <?php if($add14==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add14==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add14=='' or $add14==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add15" id="add15">
															<option value="0" <?php if($add15==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add15==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add15=='' or $add15==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add16" id="add16">
															<option value="0" <?php if($add16==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add16==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add16=='' or $add16==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add17" id="add17">
															<option value="0" <?php if($add17==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add17==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add17=='' or $add17==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add18" id="add18">
															<option value="0" <?php if($add18==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add18==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add18=='' or $add18==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>
								<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsle11_tbl" name="divsle11_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>Eyes: Scleritis / episcleritis</b></th>
													<th class="col-md-2"><b>Eyes: Other</b></th>
													<th class="col-md-2"><b>Cardiovascular: Myocarditis</b></th>
													<th class="col-md-2"><b>Cardiovascular: Coronary vasculitis</b></th>
													<th class="col-md-2"><b>Cardiovascular: Other</b></th>
													<th class="col-md-2"><b>Neurologic: Myelopathy</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="add19" id="add19">
															<option value="0" <?php if($add19==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add19==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add19=='' or $add19==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add20" id="add20">
															<option value="0" <?php if($add20==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add20==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add20=='' or $add20==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add21" id="add21">
															<option value="0" <?php if($add21==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add21==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add21=='' or $add21==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add22" id="add22">
															<option value="0" <?php if($add22==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add22==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add22=='' or $add22==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add23" id="add23">
															<option value="0" <?php if($add23==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add23==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add23=='' or $add23==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add24" id="add24">
															<option value="0" <?php if($add24==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add24==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add24=='' or $add24==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>
								<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsle12_tbl" name="divsle12_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>Neurologic: Peripheral neuropathy</b></th>
													<th class="col-md-2"><b>Neurologic: Aseptic meningitis</b></th>
													<th class="col-md-8"><b>&nbsp;</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="add25" id="add25">
															<option value="0" <?php if($add25==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add25==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add25=='' or $add25==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="add26" id="add26">
															<option value="0" <?php if($add26==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($add26==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($add26=='' or $add26==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
											 </tbody>
										</table>
								</br>
								<label>SELENA-SLEDAI Flare Index</label>	 	
								<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsle13_tbl" name="divsle13_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>[1] ΔSLEDAI >3</b></th>
													<th class="col-md-2"><b>[2 ]ΔPGA≥1.0 but PGA ≤ 2.5 </b></th>
													<th class="col-md-2"><b>[3] New or worse: skin/mucous, serositis, arthritis, fever</b></th>
													<th class="col-md-2"><b>[4] Increase or added prednisone</b></th>
													<th class="col-md-2"><b>[5] Added NSAID or HCQ</b></th>
													<th class="col-md-2"><b>[6] ΔSLEDAI > 12</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="flare1" id="flare1">
															<option value="0" <?php if($flare1==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($flare1==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($flare1==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="flare2" id="flare2">
															<option value="0" <?php if($flare2==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($flare2==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($flare2==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="flare3" id="flare3">
															<option value="0" <?php if($flare3==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($flare3==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($flare3==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="flare4" id="flare4">
															<option value="0" <?php if($flare4==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($flare4==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($flare4==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="flare5" id="flare5">
															<option value="0" <?php if($flare5==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($flare5==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($flare5==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="flare6" id="flare6">
															<option value="0" <?php if($flare6==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($flare6==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($flare6==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>	
								<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsle14_tbl" name="divsle14_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>[7] Increase in PGA to >2.5</b></th>
													<th class="col-md-2"><b>[8] New or worse: CNS-SLE, vasculitis, nephritis, myositis, thrombocytopenia, hemolytic anemia</b></th>
													<th class="col-md-2"><b>[9] Prednisone increase to ≥30mg/day</b></th>
													<th class="col-md-2"><b>[10] Use of IV methylprednisolone</b></th>
													<th class="col-md-2"><b>[11] Added CY, AZA, MTX or MMF</b></th>
													<th class="col-md-2"><b>[12] Added biological drugs</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="flare7" id="flare7">
															<option value="0" <?php if($flare7==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($flare7==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($flare7==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="flare8" id="flare8">
															<option value="0" <?php if($flare8==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($flare8==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($flare8==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="flare9" id="flare9">
															<option value="0" <?php if($flare9==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($flare9==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($flare9==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="flare10" id="flare10">
															<option value="0" <?php if($flare10==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($flare10==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($flare10==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="flare11" id="flare11">
															<option value="0" <?php if($flare11==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($flare11==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($flare11==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="flare12" id="flare12">
															<option value="0" <?php if($flare12==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($flare12==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($flare12==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>
								<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsle15_tbl" name="divsle15_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>[13] Hospitalization</b></th>
													<th class="col-md-2"><b>SELENA-SLEDAI Flare Index</b></th>
													<th class="col-md-8">&nbsp;</th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="flare13" id="flare13">
															<option value="0" <?php if($flare13==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($flare13==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($flare13==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<input type="text" class="form-control" readonly="true" name="flare14" id="flare14" value="<?php echo $flare14; ?>">
													</td>
													<td>&nbsp;</td>
												</tr>
											 </tbody>
										</table>
								</br>
								<label>CNS/Nephritis activity</label>
								<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsle16_tbl" name="divsle16_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>CNS activity</b></th>
													<th class="col-md-2"><b>Proteinuria (g/24h)</b></th>
													<th class="col-md-8">&nbsp;</th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="nefritis1" id="nefritis1">
															<option value="0" <?php if($nefritis1==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if($nefritis1==1) { echo "selected"; } ?>>1-Worsening</option>
															<option value="2" <?php if($nefritis1==2) { echo "selected"; } ?>>2-Stable</option>
															<option value="3" <?php if($nefritis1==3) { echo "selected"; } ?>>3-Partial response</option>
															<option value="4" <?php if($nefritis1==4) { echo "selected"; } ?>>4-Complete response/remission</option>
														</select>
													</td>
													<td>
														<input type="text" class="form-control" name="nefritis2" id="nefritis2" value="<?php if ($nefritis2=="") {  echo ""; } else { echo $nefritis2; } ?>">
													</td>
													<td>&nbsp;</td>
												</tr>
											 </tbody>
										</table>										
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label>Has the patient ever smoked daily for at least one year?</label>
											<select class="form-control" name="tobacco" id="tobacco">
												<option value="0" <?php if($tobacco==0) { echo "selected"; } ?>>Unknown</option>
												<option value="1" <?php if($tobacco==1) { echo "selected"; } ?>>No</option>
												<option value="2" <?php if($tobacco==2) { echo "selected"; } ?>>Yes</option>
											</select>
									</div>
								</div>
								</br>
								<div class="form-group col-md-12" id="divtobacco" name="divtobacco" <?php if($tobacco==2) { ?> style="DISPLAY:block;" <?php } else { ?>  style="DISPLAY:none;"  <?php } ?>>
									<div class="row">
										<div class="form-group col-md-3" id="divsmokingstarting" name="divsmokingstarting">
											<label>Year that the patient started smoking</label>
												<div class="input-group">
												  <input type="text" class="form-control" id="smokingstarting" name="smokingstarting" value="<?php echo $smokingstarting; ?>">
												</div>
												<!-- /.input group -->
										</div>
										<div class="form-group col-md-3" id="divsmokingstopped" name="divsmokingstopped">
										 <label>Did the patient stop smoking?</label>
											<select class="form-control" name="smokingstopped" id="smokingstopped">
												<option value="0" <?php if ($smokingstopped==0) { echo "selected"; } ?>>No</option>
												<option value="1" <?php if ($smokingstopped==1) { echo "selected"; } ?>>Yes</option>
											</select>
										</div>
										<div  id="divsmokingstoppedyear" name="divsmokingstoppedyear">
											<label>Year</label>
											<div class="input-group">
											  <input type="text" class="form-control" id="smokingyear" name="smokingyear" value="<?php echo $smokingyear; ?>">
											</div>
											<!-- /.input group -->
										  </div>
									</div>
									<div class="row">
										<div class="form-group col-md-12" id="divsmokingfirstdecade" name="divsmokingfirstdecade">
										 <div class="row">
											<div class="form-group col-md-12">
												<label>How many packs/day has the patient approximately smoked per decade</label>
											</div>
										</div>
										<div class="row">
											<div class="form-group col-md-2">
												<label>1st</label>
												  <select class="form-control" name="decadefirst" id="decadefirst">
													<option value="00" <?php if ($decadefirst==00) { echo "selected"; } ?>>0</option>
													<option value="01" <?php if ($decadefirst==01) { echo "selected"; } ?>>1/4</option>
													<option value="02" <?php if ($decadefirst==02) { echo "selected"; } ?>>1/2</option>
													<option value="03" <?php if ($decadefirst==03) { echo "selected"; } ?>>3/4</option>
													<option value="10" <?php if ($decadefirst==10) { echo "selected"; } ?>>1</option>
													<option value="11" <?php if ($decadefirst==11) { echo "selected"; } ?>>1 & 1/4</option>
													<option value="12" <?php if ($decadefirst==12) { echo "selected"; } ?>>1 & 1/2</option>
													<option value="13" <?php if ($decadefirst==13) { echo "selected"; } ?>>1 & 3/4</option>
													<option value="20" <?php if ($decadefirst==20) { echo "selected"; } ?>>2</option>
													<option value="21" <?php if ($decadefirst==21) { echo "selected"; } ?>>2 & 1/4</option>
													<option value="22" <?php if ($decadefirst==22) { echo "selected"; } ?>>2 & 1/2</option>
													<option value="23" <?php if ($decadefirst==23) { echo "selected"; } ?>>2 & 3/4</option>
													<option value="30" <?php if ($decadefirst==30) { echo "selected"; } ?>>3</option>
													<option value="31" <?php if ($decadefirst==31) { echo "selected"; } ?>>3 & 1/4</option>
													<option value="32" <?php if ($decadefirst==32) { echo "selected"; } ?>>3 & 1/2</option>
													<option value="33" <?php if ($decadefirst==33) { echo "selected"; } ?>>3 & 3/4</option>
													<option value="40" <?php if ($decadefirst==40) { echo "selected"; } ?>>4</option>
													<option value="41" <?php if ($decadefirst==41) { echo "selected"; } ?>>4 & 1/4</option>
													<option value="42" <?php if ($decadefirst==42) { echo "selected"; } ?>>4 & 1/2</option>
													<option value="43" <?php if ($decadefirst==43) { echo "selected"; } ?>>4 & 3/4</option>
													<option value="50" <?php if ($decadefirst==50) { echo "selected"; } ?>>5</option>
												  </select>
											</div>
											<div class="form-group col-md-2">
												<label>2nd</label>
												  <select class="form-control" name="decadesecond" id="decadesecond">
													<option value="00" <?php if ($decadesecond==00) { echo "selected"; } ?>>0</option>
													<option value="01" <?php if ($decadesecond==01) { echo "selected"; } ?>>1/4</option>
													<option value="02" <?php if ($decadesecond==02) { echo "selected"; } ?>>1/2</option>
													<option value="03" <?php if ($decadesecond==03) { echo "selected"; } ?>>3/4</option>
													<option value="10" <?php if ($decadesecond==10) { echo "selected"; } ?>>1</option>
													<option value="11" <?php if ($decadesecond==11) { echo "selected"; } ?>>1 & 1/4</option>
													<option value="12" <?php if ($decadesecond==12) { echo "selected"; } ?>>1 & 1/2</option>
													<option value="13" <?php if ($decadesecond==13) { echo "selected"; } ?>>1 & 3/4</option>
													<option value="20" <?php if ($decadesecond==20) { echo "selected"; } ?>>2</option>
													<option value="21" <?php if ($decadesecond==21) { echo "selected"; } ?>>2 & 1/4</option>
													<option value="22" <?php if ($decadesecond==22) { echo "selected"; } ?>>2 & 1/2</option>
													<option value="23" <?php if ($decadesecond==23) { echo "selected"; } ?>>2 & 3/4</option>
													<option value="30" <?php if ($decadesecond==30) { echo "selected"; } ?>>3</option>
													<option value="31" <?php if ($decadesecond==31) { echo "selected"; } ?>>3 & 1/4</option>
													<option value="32" <?php if ($decadesecond==32) { echo "selected"; } ?>>3 & 1/2</option>
													<option value="33" <?php if ($decadesecond==33) { echo "selected"; } ?>>3 & 3/4</option>
													<option value="40" <?php if ($decadesecond==40) { echo "selected"; } ?>>4</option>
													<option value="41" <?php if ($decadesecond==41) { echo "selected"; } ?>>4 & 1/4</option>
													<option value="42" <?php if ($decadesecond==42) { echo "selected"; } ?>>4 & 1/2</option>
													<option value="43" <?php if ($decadesecond==43) { echo "selected"; } ?>>4 & 3/4</option>
													<option value="50" <?php if ($decadesecond==50) { echo "selected"; } ?>>5</option>
												  </select>
											</div>
											<div class="form-group col-md-2">
												<label>3rd</label>
												  <select class="form-control" name="decadethird" id="decadethird">
													<option value="00" <?php if ($decadethird==00) { echo "selected"; } ?>>0</option>
													<option value="01" <?php if ($decadethird==01) { echo "selected"; } ?>>1/4</option>
													<option value="02" <?php if ($decadethird==02) { echo "selected"; } ?>>1/2</option>
													<option value="03" <?php if ($decadethird==03) { echo "selected"; } ?>>3/4</option>
													<option value="10" <?php if ($decadethird==10) { echo "selected"; } ?>>1</option>
													<option value="11" <?php if ($decadethird==11) { echo "selected"; } ?>>1 & 1/4</option>
													<option value="12" <?php if ($decadethird==12) { echo "selected"; } ?>>1 & 1/2</option>
													<option value="13" <?php if ($decadethird==13) { echo "selected"; } ?>>1 & 3/4</option>
													<option value="20" <?php if ($decadethird==20) { echo "selected"; } ?>>2</option>
													<option value="21" <?php if ($decadethird==21) { echo "selected"; } ?>>2 & 1/4</option>
													<option value="22" <?php if ($decadethird==22) { echo "selected"; } ?>>2 & 1/2</option>
													<option value="23" <?php if ($decadethird==23) { echo "selected"; } ?>>2 & 3/4</option>
													<option value="30" <?php if ($decadethird==30) { echo "selected"; } ?>>3</option>
													<option value="31" <?php if ($decadethird==31) { echo "selected"; } ?>>3 & 1/4</option>
													<option value="32" <?php if ($decadethird==32) { echo "selected"; } ?>>3 & 1/2</option>
													<option value="33" <?php if ($decadethird==33) { echo "selected"; } ?>>3 & 3/4</option>
													<option value="40" <?php if ($decadethird==40) { echo "selected"; } ?>>4</option>
													<option value="41" <?php if ($decadethird==41) { echo "selected"; } ?>>4 & 1/4</option>
													<option value="42" <?php if ($decadethird==42) { echo "selected"; } ?>>4 & 1/2</option>
													<option value="43" <?php if ($decadethird==43) { echo "selected"; } ?>>4 & 3/4</option>
													<option value="50" <?php if ($decadethird==50) { echo "selected"; } ?>>5</option>
												  </select>
											</div>
											<div class="form-group col-md-2">
												<label>4th</label>
												  <select class="form-control" name="decadeforth" id="decadeforth">
													<option value="00" <?php if ($decadeforth==00) { echo "selected"; } ?>>0</option>
													<option value="01" <?php if ($decadeforth==01) { echo "selected"; } ?>>1/4</option>
													<option value="02" <?php if ($decadeforth==02) { echo "selected"; } ?>>1/2</option>
													<option value="03" <?php if ($decadeforth==03) { echo "selected"; } ?>>3/4</option>
													<option value="10" <?php if ($decadeforth==10) { echo "selected"; } ?>>1</option>
													<option value="11" <?php if ($decadeforth==11) { echo "selected"; } ?>>1 & 1/4</option>
													<option value="12" <?php if ($decadeforth==12) { echo "selected"; } ?>>1 & 1/2</option>
													<option value="13" <?php if ($decadeforth==13) { echo "selected"; } ?>>1 & 3/4</option>
													<option value="20" <?php if ($decadeforth==20) { echo "selected"; } ?>>2</option>
													<option value="21" <?php if ($decadeforth==21) { echo "selected"; } ?>>2 & 1/4</option>
													<option value="22" <?php if ($decadeforth==22) { echo "selected"; } ?>>2 & 1/2</option>
													<option value="23" <?php if ($decadeforth==23) { echo "selected"; } ?>>2 & 3/4</option>
													<option value="30" <?php if ($decadeforth==3) { echo "selected"; } ?>>3</option>
													<option value="31" <?php if ($decadeforth==31) { echo "selected"; } ?>>3 & 1/4</option>
													<option value="32" <?php if ($decadeforth==32) { echo "selected"; } ?>>3 & 1/2</option>
													<option value="33" <?php if ($decadeforth==33) { echo "selected"; } ?>>3 & 3/4</option>
													<option value="40" <?php if ($decadeforth==40) { echo "selected"; } ?>>4</option>
													<option value="41" <?php if ($decadeforth==41) { echo "selected"; } ?>>4 & 1/4</option>
													<option value="42" <?php if ($decadeforth==42) { echo "selected"; } ?>>4 & 1/2</option>
													<option value="43" <?php if ($decadeforth==43) { echo "selected"; } ?>>4 & 3/4</option>
													<option value="50" <?php if ($decadeforth==50) { echo "selected"; } ?>>5</option>
												  </select>
											</div>
											</div>
											<div class="row">
											<div class="form-group col-md-2">
												<label>5th</label>
												  <select class="form-control" name="decadefifth" id="decadefifth">
													<option value="00" <?php if ($decadefifth==00) { echo "selected"; } ?>>0</option>
													<option value="01" <?php if ($decadefifth==01) { echo "selected"; } ?>>1/4</option>
													<option value="02" <?php if ($decadefifth==02) { echo "selected"; } ?>>1/2</option>
													<option value="03" <?php if ($decadefifth==03) { echo "selected"; } ?>>3/4</option>
													<option value="10" <?php if ($decadefifth==10) { echo "selected"; } ?>>1</option>
													<option value="11" <?php if ($decadefifth==11) { echo "selected"; } ?>>1 & 1/4</option>
													<option value="12" <?php if ($decadefifth==12) { echo "selected"; } ?>>1 & 1/2</option>
													<option value="13" <?php if ($decadefifth==13) { echo "selected"; } ?>>1 & 3/4</option>
													<option value="20" <?php if ($decadefifth==20) { echo "selected"; } ?>>2</option>
													<option value="21" <?php if ($decadefifth==21) { echo "selected"; } ?>>2 & 1/4</option>
													<option value="22" <?php if ($decadefifth==22) { echo "selected"; } ?>>2 & 1/2</option>
													<option value="23" <?php if ($decadefifth==23) { echo "selected"; } ?>>2 & 3/4</option>
													<option value="30" <?php if ($decadefifth==30) { echo "selected"; } ?>>3</option>
													<option value="31" <?php if ($decadefifth==31) { echo "selected"; } ?>>3 & 1/4</option>
													<option value="32" <?php if ($decadefifth==32) { echo "selected"; } ?>>3 & 1/2</option>
													<option value="33" <?php if ($decadefifth==33) { echo "selected"; } ?>>3 & 3/4</option>
													<option value="40" <?php if ($decadefifth==40) { echo "selected"; } ?>>4</option>
													<option value="41" <?php if ($decadefifth==41) { echo "selected"; } ?>>4 & 1/4</option>
													<option value="42" <?php if ($decadefifth==42) { echo "selected"; } ?>>4 & 1/2</option>
													<option value="43" <?php if ($decadefifth==43) { echo "selected"; } ?>>4 & 3/4</option>
													<option value="50" <?php if ($decadefifth==50) { echo "selected"; } ?>>5</option>
												  </select>
											</div>
											<div class="form-group col-md-2">
												<label>6th</label>
												  <select class="form-control" name="decadesixth" id="decadesixth">
													<option value="00" <?php if ($decadesixth==00) { echo "selected"; } ?>>0</option>
													<option value="01" <?php if ($decadesixth==01) { echo "selected"; } ?>>1/4</option>
													<option value="02" <?php if ($decadesixth==02) { echo "selected"; } ?>>1/2</option>
													<option value="03" <?php if ($decadesixth==03) { echo "selected"; } ?>>3/4</option>
													<option value="10" <?php if ($decadesixth==10) { echo "selected"; } ?>>1</option>
													<option value="11" <?php if ($decadesixth==11) { echo "selected"; } ?>>1 & 1/4</option>
													<option value="12" <?php if ($decadesixth==12) { echo "selected"; } ?>>1 & 1/2</option>
													<option value="13" <?php if ($decadesixth==13) { echo "selected"; } ?>>1 & 3/4</option>
													<option value="20" <?php if ($decadesixth==20) { echo "selected"; } ?>>2</option>
													<option value="21" <?php if ($decadesixth==21) { echo "selected"; } ?>>2 & 1/4</option>
													<option value="22" <?php if ($decadesixth==22) { echo "selected"; } ?>>2 & 1/2</option>
													<option value="23" <?php if ($decadesixth==23) { echo "selected"; } ?>>2 & 3/4</option>
													<option value="30" <?php if ($decadesixth==30) { echo "selected"; } ?>>3</option>
													<option value="31" <?php if ($decadesixth==31) { echo "selected"; } ?>>3 & 1/4</option>
													<option value="32" <?php if ($decadesixth==32) { echo "selected"; } ?>>3 & 1/2</option>
													<option value="33" <?php if ($decadesixth==33) { echo "selected"; } ?>>3 & 3/4</option>
													<option value="40" <?php if ($decadesixth==40) { echo "selected"; } ?>>4</option>
													<option value="41" <?php if ($decadesixth==41) { echo "selected"; } ?>>4 & 1/4</option>
													<option value="42" <?php if ($decadesixth==42) { echo "selected"; } ?>>4 & 1/2</option>
													<option value="43" <?php if ($decadesixth==43) { echo "selected"; } ?>>4 & 3/4</option>
													<option value="50" <?php if ($decadesixth==50) { echo "selected"; } ?>>5</option>
												  </select>
											</div>
											<div class="form-group col-md-2">
												<label>7th</label>
												  <select class="form-control" name="decadeseventh" id="decadeseventh">
													<option value="00" <?php if ($decadeseventh==00) { echo "selected"; } ?>>0</option>
													<option value="01" <?php if ($decadeseventh==01) { echo "selected"; } ?>>1/4</option>
													<option value="02" <?php if ($decadeseventh==02) { echo "selected"; } ?>>1/2</option>
													<option value="03" <?php if ($decadeseventh==03) { echo "selected"; } ?>>3/4</option>
													<option value="10" <?php if ($decadeseventh==10) { echo "selected"; } ?>>1</option>
													<option value="11" <?php if ($decadeseventh==11) { echo "selected"; } ?>>1 & 1/4</option>
													<option value="12" <?php if ($decadeseventh==12) { echo "selected"; } ?>>1 & 1/2</option>
													<option value="13" <?php if ($decadeseventh==13) { echo "selected"; } ?>>1 & 3/4</option>
													<option value="20" <?php if ($decadeseventh==20) { echo "selected"; } ?>>2</option>
													<option value="21" <?php if ($decadeseventh==21) { echo "selected"; } ?>>2 & 1/4</option>
													<option value="22" <?php if ($decadeseventh==22) { echo "selected"; } ?>>2 & 1/2</option>
													<option value="23" <?php if ($decadeseventh==23) { echo "selected"; } ?>>2 & 3/4</option>
													<option value="30" <?php if ($decadeseventh==30) { echo "selected"; } ?>>3</option>
													<option value="31" <?php if ($decadeseventh==31) { echo "selected"; } ?>>3 & 1/4</option>
													<option value="32" <?php if ($decadeseventh==32) { echo "selected"; } ?>>3 & 1/2</option>
													<option value="33" <?php if ($decadeseventh==33) { echo "selected"; } ?>>3 & 3/4</option>
													<option value="40" <?php if ($decadeseventh==40) { echo "selected"; } ?>>4</option>
													<option value="41" <?php if ($decadeseventh==41) { echo "selected"; } ?>>4 & 1/4</option>
													<option value="42" <?php if ($decadeseventh==42) { echo "selected"; } ?>>4 & 1/2</option>
													<option value="43" <?php if ($decadeseventh==43) { echo "selected"; } ?>>4 & 3/4</option>
													<option value="50" <?php if ($decadeseventh==50) { echo "selected"; } ?>>5</option>
												  </select>
											</div>
											<div class="form-group col-md-2">
												<label>8th</label>
												  <select class="form-control" name="decadeeight" id="decadeeight">
													<option value="00" <?php if ($decadeeight==00) { echo "selected"; } ?>>0</option>
													<option value="01" <?php if ($decadeeight==01) { echo "selected"; } ?>>1/4</option>
													<option value="02" <?php if ($decadeeight==02) { echo "selected"; } ?>>1/2</option>
													<option value="03" <?php if ($decadeeight==03) { echo "selected"; } ?>>3/4</option>
													<option value="10" <?php if ($decadeeight==10) { echo "selected"; } ?>>1</option>
													<option value="11" <?php if ($decadeeight==11) { echo "selected"; } ?>>1 & 1/4</option>
													<option value="12" <?php if ($decadeeight==12) { echo "selected"; } ?>>1 & 1/2</option>
													<option value="13" <?php if ($decadeeight==13) { echo "selected"; } ?>>1 & 3/4</option>
													<option value="20" <?php if ($decadeeight==20) { echo "selected"; } ?>>2</option>
													<option value="21" <?php if ($decadeeight==21) { echo "selected"; } ?>>2 & 1/4</option>
													<option value="22" <?php if ($decadeeight==22) { echo "selected"; } ?>>2 & 1/2</option>
													<option value="23" <?php if ($decadeeight==23) { echo "selected"; } ?>>2 & 3/4</option>
													<option value="30" <?php if ($decadeeight==30) { echo "selected"; } ?>>3</option>
													<option value="31" <?php if ($decadeeight==31) { echo "selected"; } ?>>3 & 1/4</option>
													<option value="32" <?php if ($decadeeight==32) { echo "selected"; } ?>>3 & 1/2</option>
													<option value="33" <?php if ($decadeeight==33) { echo "selected"; } ?>>3 & 3/4</option>
													<option value="40" <?php if ($decadeeight==40) { echo "selected"; } ?>>4</option>
													<option value="41" <?php if ($decadeeight==41) { echo "selected"; } ?>>4 & 1/4</option>
													<option value="42" <?php if ($decadeeight==42) { echo "selected"; } ?>>4 & 1/2</option>
													<option value="43" <?php if ($decadeeight==43) { echo "selected"; } ?>>4 & 3/4</option>
													<option value="50" <?php if ($decadeeight==50) { echo "selected"; } ?>>5</option>
												  </select>
											</div>
										</div>
										</div>
									</div>
									<div class="row">
										<div class="form-group col-md-3" id="divaveragepackperday" name="divaveragepackperday">
											<label>Average packs/day</label>
												<div class="input-group">
												  <input type="text" readonly=true class="form-control" id="averagepackperday" name="averagepackperday" value="<?php echo $averagepackperday; ?>">
												</div>
												<!-- /.input group -->
										</div>
										<div class="form-group col-md-3" id="divaveragepackperyear" name="divaveragepackperyear">
											<label>Pack-year</label>
												<div class="input-group">
												  <input type="text" readonly=true class="form-control" id="averagepackperyear" name="averagepackperyear" value="<?php echo $averagepackperyear; ?>">
												</div>
												<!-- /.input group -->
										</div>
									</div>
								</div>
					</div>
				</div> <!--  Disease activity end div -->
				<!-- Disease activity of life start div -->
				<div class="row">
					<div class="col-md-12">
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Severity</small></h3>
							</div>
							<div class="box-body">
								<div class="row">
									<div class="col-md-4">
										<label>SLE</label>
											<select class="form-control" name="sle_severity" id="sle_severity">
												<option value="0" <?php if($sle_severity==0) { echo "selected"; } ?>>--</option>
												<option value="1" <?php if($sle_severity==1) { echo "selected"; } ?>>No</option>
												<option value="2" <?php if($sle_severity==2) { echo "selected"; } ?>>Yes</option>
											</select>
									</div>
									<?php
											if ($sle_severity==2){
												
												if($patient_followup_id=='' or $patient_followup_id=='0')
												{
													$order = get_prv_sle_severity($pat_id);
													$result = pg_fetch_array($order);
												}
												else
												{
													$order = get_sle_severity($patient_followup_id);
													$result = pg_fetch_array($order);
												}
												$patient_sle_severity_id=$result['patient_sle_severity_id'];
											}
											
											$slicc1=$result['slicc1'];
											$slicc2=$result['slicc2'];
											$slicc3=$result['slicc3'];
											$slicc4=$result['slicc4'];
											$slicc5=$result['slicc5'];
											$slicc6=$result['slicc6'];
											$slicc7=$result['slicc7'];
											$slicc8=$result['slicc8'];
											$slicc9=$result['slicc9'];
											$slicc10=$result['slicc10'];
											$slicc11=$result['slicc11'];
											$slicc12=$result['slicc12'];
											$slicc13=$result['slicc13'];
											$slicc14=$result['slicc14'];
											$slicc15=$result['slicc15'];
											$slicc16=$result['slicc16'];
											$slicc17=$result['slicc17'];
											$slicc18=$result['slicc18'];
											$slicc19=$result['slicc19'];
											$slicc20=$result['slicc20'];
											$slicc21=$result['slicc21'];
											$slicc22=$result['slicc22'];
											$slicc23=$result['slicc23'];
											$slicc24=$result['slicc24'];
											$slicc25=$result['slicc25'];
											$slicc26=$result['slicc26'];
											$slicc27=$result['slicc27'];
											$slicc28=$result['slicc28'];
											$slicc29=$result['slicc29'];
											$slicc30=$result['slicc30'];
											$slicc31=$result['slicc31'];
											$slicc32=$result['slicc32'];
											$slicc33=$result['slicc33'];
											$slicc34=$result['slicc34'];
											$slicc35=$result['slicc35'];
											$slicc36=$result['slicc36'];
											$slicc37=$result['slicc37'];
											$slicc38=$result['slicc38'];
											$slicc39=$result['slicc39'];
											$slicc40=$result['slicc40'];
											$slicc41=$result['slicc41'];
											for($i=1;$i<=41;$i++)
											{
												
												if(${"slicc".$i}=="")
													${"slicc".$i}=0;
											}
											$slicc42=$result['slicc42'];
											
											
											
											$slicc1_date =$result['slicc1_date_str'];
											if ($slicc1_date=="12-12-1900")
												$slicc1_date="";
											$slicc2_date =$result['slicc2_date_str'];
											if ($slicc2_date=="12-12-1900")
												$slicc2_date="";
											$slicc3_date =$result['slicc3_date_str'];
											if ($slicc3_date=="12-12-1900")
												$slicc3_date="";
											$slicc4_date =$result['slicc4_date_str'];
											if ($slicc4_date=="12-12-1900")
												$slicc4_date="";
											$slicc5_date =$result['slicc5_date_str'];
											if ($slicc5_date=="12-12-1900")
												$slicc5_date="";
											$slicc6_date =$result['slicc6_date_str'];
											if ($slicc6_date=="12-12-1900")
												$slicc6_date="";
											$slicc7_date =$result['slicc7_date_str'];
											if ($slicc7_date=="12-12-1900")
												$slicc7_date="";
											$slicc8_date =$result['slicc8_date_str'];
											if ($slicc8_date=="12-12-1900")
												$slicc8_date="";
											$slicc9_date =$result['slicc9_date_str'];
											if ($slicc9_date=="12-12-1900")
												$slicc9_date="";
											$slicc10_date =$result['slicc10_date_str'];
											if ($slicc10_date=="12-12-1900")
												$slicc10_date="";
											$slicc11_date =$result['slicc11_date_str'];
											if ($slicc11_date=="12-12-1900")
												$slicc11_date="";
											$slicc12_date =$result['slicc12_date_str'];
											if ($slicc12_date=="12-12-1900")
												$slicc12_date="";
											$slicc13_date =$result['slicc13_date_str'];
											if ($slicc13_date=="12-12-1900")
												$slicc13_date="";
											$slicc14_date =$result['slicc14_date_str'];
											if ($slicc14_date=="12-12-1900")
												$slicc14_date="";
											$slicc15_date =$result['slicc15_date_str'];
											if ($slicc15_date=="12-12-1900")
												$slicc15_date="";
											$slicc16_date =$result['slicc16_date_str'];
											if ($slicc16_date=="12-12-1900")
												$slicc16_date="";
											$slicc17_date =$result['slicc17_date_str'];
											if ($slicc17_date=="12-12-1900")
												$slicc17_date="";
											$slicc18_date =$result['slicc18_date_str'];
											if ($slicc18_date=="12-12-1900")
												$slicc18_date="";
											$slicc19_date =$result['slicc19_date_str'];
											if ($slicc19_date=="12-12-1900")
												$slicc19_date="";
											$slicc20_date =$result['slicc20_date_str'];
											if ($slicc20_date=="12-12-1900")
												$slicc20_date="";
											$slicc21_date =$result['slicc21_date_str'];
											if ($slicc21_date=="12-12-1900")
												$slicc21_date="";
											$slicc22_date =$result['slicc22_date_str'];
											if ($slicc22_date=="12-12-1900")
												$slicc22_date="";
											$slicc23_date =$result['slicc23_date_str'];
											if ($slicc23_date=="12-12-1900")
												$slicc23_date="";
											$slicc24_date =$result['slicc24_date_str'];
											if ($slicc24_date=="12-12-1900")
												$slicc24_date="";
											$slicc25_date =$result['slicc25_date_str'];
											if ($slicc25_date=="12-12-1900")
												$slicc25_date="";
											$slicc26_date =$result['slicc26_date_str'];
											if ($slicc26_date=="12-12-1900")
												$slicc26_date="";
											$slicc27_date =$result['slicc27_date_str'];
											if ($slicc27_date=="12-12-1900")
												$slicc27_date="";
											$slicc28_date =$result['slicc28_date_str'];
											if ($slicc28_date=="12-12-1900")
												$slicc28_date="";
											$slicc29_date =$result['slicc29_date_str'];
											if ($slicc29_date=="12-12-1900")
												$slicc29_date="";
											$slicc30_date =$result['slicc30_date_str'];
											if ($slicc30_date=="12-12-1900")
												$slicc30_date="";
											$slicc31_date =$result['slicc31_date_str'];
											if ($slicc31_date=="12-12-1900")
												$slicc31_date="";
											$slicc32_date =$result['slicc32_date_str'];
											if ($slicc32_date=="12-12-1900")
												$slicc32_date="";
											$slicc33_date =$result['slicc33_date_str'];
											if ($slicc33_date=="12-12-1900")
												$slicc33_date="";
											$slicc34_date =$result['slicc34_date_str'];
											if ($slicc34_date=="12-12-1900")
												$slicc34_date="";
											$slicc35_date =$result['slicc35_date_str'];
											if ($slicc35_date=="12-12-1900")
												$slicc35_date="";
											$slicc36_date =$result['slicc36_date_str'];
											if ($slicc36_date=="12-12-1900")
												$slicc36_date="";
											$slicc37_date =$result['slicc37_date_str'];
											if ($slicc37_date=="12-12-1900")
												$slicc37_date="";
											$slicc38_date =$result['slicc38_date_str'];
											if ($slicc38_date=="12-12-1900")
												$slicc38_date="";
											$slicc39_date =$result['slicc39_date_str'];
											if ($slicc39_date=="12-12-1900")
												$slicc39_date="";
											$slicc40_date =$result['slicc40_date_str'];
											if ($slicc40_date=="12-12-1900")
												$slicc40_date="";
											$slicc41_date =$result['slicc41_date_str'];
											if ($slicc41_date=="12-12-1900")
												$slicc41_date="";
											
											$index1_date =$result['index1_date_str'];
											if ($index1_date=="12-12-1900")
												$index1_date="";
											$index2_date =$result['index2_date_str'];
											if ($index2_date=="12-12-1900")
												$index2_date="";
											$index3_date =$result['index3_date_str'];
											if ($index3_date=="12-12-1900")
												$index3_date="";
											$index4_date =$result['index4_date_str'];
											if ($index4_date=="12-12-1900")
												$index4_date="";
											$index5_date =$result['index5_date_str'];
											if ($index5_date=="12-12-1900")
												$index5_date="";
											$index6_date =$result['index6_date_str'];
											if ($index6_date=="12-12-1900")
												$index6_date="";
											$index7_date =$result['index7_date_str'];
											if ($index7_date=="12-12-1900")
												$index7_date="";
											$index8_date =$result['index8_date_str'];
											if ($index8_date=="12-12-1900")
												$index8_date="";
											$index9_date =$result['index9_date_str'];
											if ($index9_date=="12-12-1900")
												$index9_date="";
											$index10_date =$result['index10_date_str'];
											if ($index10_date=="12-12-1900")
												$index10_date="";
											$index11_date =$result['index11_date_str'];
											if ($index11_date=="12-12-1900")
												$index11_date="";
											$index12_date =$result['index12_date_str'];
											if ($index12_date=="12-12-1900")
												$index12_date="";
											
											
											$index1=$result['index1'];
											$index2=$result['index2'];
											$index3=$result['index3'];
											$index4=$result['index4'];
											$index5=$result['index5'];
											$index6=$result['index6'];
											$index7=$result['index7'];
											$index8=$result['index8'];
											$index9=$result['index9'];
											$index10=$result['index10'];
											$index11=$result['index11'];
											$index12=$result['index12'];
											$index13=$result['index13'];
											$index13=$result['index13'];
											
											$cns1=$result['cns1'];
											$cns2=$result['cns2'];
											$cns3=$result['cns3'];
											$cns4=$result['cns4'];
											
											
											
									?>
									<input type="hidden" id="patient_sle_severity_id" name="patient_sle_severity_id" value="<?php echo $patient_sle_severity_id; ?>">
									<div class="form-group col-md-12" id="divsleseverity" name="divsleseverity" <?php if($sle_severity==2) { ?> style="DISPLAY:block;" <?php } else { ?>  style="DISPLAY:none;"  <?php } ?>>
										</br>
										<label>SLICC / ACR Damage Index</label>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsleseverity1_tbl" name="divsleseverity1_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>1 Retinal change or optic atrophy</b></th>
													<th class="col-md-2"><b>2 Cognitive impairment/dementia or major psychosis</b></th>
													<th class="col-md-2"><b>3 Seizures requiring therapy for ≥6 months</b></th>
													<th class="col-md-2"><b>4 Cerebrovascular Accident</b></th>
													<th class="col-md-2"><b>5 Cranial or Peripheral Neuropathy (excluding optic)</b></th>
													<th class="col-md-2"><b>6 Transverse Myelitis</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="slicc1" id="slicc1">
															<option value="-1" <?php if($slicc1==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc1==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc1==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc1_date"  name="slicc1_date" value="<?php echo $slicc1_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc2" id="slicc2">
															<option value="-1" <?php if($slicc2==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc2==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc2==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc2_date"  name="slicc2_date" value="<?php echo $slicc2_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc3" id="slicc3">
															<option value="-1" <?php if($slicc3==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc3==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc3==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc3_date"  name="slicc3_date" value="<?php echo $slicc3_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc4" id="slicc4">
															<option value="-1" <?php if($slicc4==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc4==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc4==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($slicc4==2) { echo "selected"; } ?>>2</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc4_date"  name="slicc4_date" value="<?php echo $slicc4_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc5" id="slicc5">
															<option value="-1" <?php if($slicc5==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc5==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc5==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc5_date"  name="slicc5_date" value="<?php echo $slicc5_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc6" id="slicc6">
															<option value="-1" <?php if($slicc6==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc6==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc6==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc6_date"  name="slicc6_date" value="<?php echo $slicc6_date; ?>">
														</div>
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsleseverity2_tbl" name="divsleseverity2_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>7. Estimated or Measured GFR <50%</b></th>
													<th class="col-md-2"><b>8. Proteinuria ≥3.5 g/24 hours</b></th>
													<th class="col-md-2"><b>9. End-stage renal disease (dialysis or transplantation)</b></th>
													<th class="col-md-2"><b>10. Pulmonary Hypertension (right ventricular hypertrophy)</b></th>
													<th class="col-md-2"><b>11. Pulmonary Fibrosis (clinical or radiographic)</b></th>
													<th class="col-md-2"><b>12. Shrinking Lung (radiographic)</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="slicc7" id="slicc7">
															<option value="-1" <?php if($slicc7==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc7==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc7==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc7_date"  name="slicc7_date" value="<?php echo $slicc7_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc8" id="slicc8">
															<option value="-1" <?php if($slicc8==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc8==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc8==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc8_date"  name="slicc8_date" value="<?php echo $slicc8_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc9" id="slicc9">
															<option value="-1" <?php if($slicc9==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc9==0) { echo "selected"; } ?>>0</option>
															<option value="3" <?php if($slicc9==3) { echo "selected"; } ?>>3</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc9_date"  name="slicc9_date" value="<?php echo $slicc9_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc10" id="slicc10">
															<option value="-1" <?php if($slicc10==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc10==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc10==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc10_date"  name="slicc10_date" value="<?php echo $slicc10_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc11" id="slicc11">
															<option value="-1" <?php if($slicc11==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc11==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc11==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc11_date"  name="slicc11_date" value="<?php echo $slicc11_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc12" id="slicc12">
															<option value="-1" <?php if($slicc12==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc12==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc12==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc12_date"  name="slicc12_date" value="<?php echo $slicc12_date; ?>">
														</div>
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsleseverity3_tbl" name="divsleseverity3_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>13. Pleural Fibrosis (radiographic)</b></th>
													<th class="col-md-2"><b>14. Pulmonary Infarction (radiographic)</b></th>
													<th class="col-md-2"><b>15. Angina or Coronary Artery Bypass</b></th>
													<th class="col-md-2"><b>16. Myocardial Infarction</b></th>
													<th class="col-md-2"><b>17. Cardiomyopathy (left ventricular dysfunction)</b></th>
													<th class="col-md-2"><b>18. Valvular disease (systolic or diastolic murmur >3/6)</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="slicc13" id="slicc13">
															<option value="-1" <?php if($slicc13==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc13==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc13==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc13_date"  name="slicc13_date" value="<?php echo $slicc13_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc14" id="slicc14">
															<option value="-1" <?php if($slicc14==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc14==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc14==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc14_date"  name="slicc14_date" value="<?php echo $slicc14_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc15" id="slicc15">
															<option value="-1" <?php if($slicc15==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc15==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc15==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc15_date"  name="slicc15_date" value="<?php echo $slicc15_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc16" id="slicc16">
															<option value="-1" <?php if($slicc16==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc16==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc16==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($slicc16==2) { echo "selected"; } ?>>2</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc16_date"  name="slicc16_date" value="<?php echo $slicc16_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc17" id="slicc17">
															<option value="-1" <?php if($slicc17==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc17==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc17==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc17_date"  name="slicc17_date" value="<?php echo $slicc17_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc18" id="slicc18">
															<option value="-1" <?php if($slicc18==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc18==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc18==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc18_date"  name="slicc18_date" value="<?php echo $slicc18_date; ?>">
														</div>
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsleseverity4_tbl" name="divsleseverity4_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>19. Pericarditis for ≥6 months or pericardiectomy</b></th>
													<th class="col-md-2"><b>20. Claudication  for ≥6 months</b></th>
													<th class="col-md-2"><b>21. Minor Tissue Loss from Peripheral Vascular Disease (e.g. pulp space)</b></th>
													<th class="col-md-2"><b>22. Significant Tissue Loss from Peripheral Vascular Disease (e.g.loss of digit or limb)</b></th>
													<th class="col-md-2"><b>23. Venous Thrombosis with swelling, ulceration or venous stasis</b></th>
													<th class="col-md-2"><b>24. Infarction or Resection of Bowel (below duodenum), Spleen, Liver or Gallbladder, ever, for any cause</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="slicc19" id="slicc19">
															<option value="-1" <?php if($slicc19==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc19==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc19==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc19_date"  name="slicc19_date" value="<?php echo $slicc19_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc20" id="slicc20">
															<option value="-1" <?php if($slicc20==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc20==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc20==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc20_date"  name="slicc20_date" value="<?php echo $slicc20_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc21" id="slicc21">
															<option value="-1" <?php if($slicc21==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc21==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc21==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc21_date"  name="slicc21_date" value="<?php echo $slicc21_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc22" id="slicc22">
															<option value="-1" <?php if($slicc22==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc22==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc22==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($slicc22==2) { echo "selected"; } ?>>2</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc22_date"  name="slicc22_date" value="<?php echo $slicc22_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc23" id="slicc23">
															<option value="-1" <?php if($slicc23==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc23==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc23==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc23_date"  name="slicc23_date" value="<?php echo $slicc23_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc24" id="slicc24">
															<option value="-1" <?php if($slicc24==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc24==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc24==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($slicc24==2) { echo "selected"; } ?>>2</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc24_date"  name="slicc24_date" value="<?php echo $slicc24_date; ?>">
														</div>
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsleseverity5_tbl" name="divsleseverity5_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>25. Mesenteric Insufficiency</b></th>
													<th class="col-md-2"><b>26. Chronic Peritonitis</b></th>
													<th class="col-md-2"><b>27. Stricture or Upper Gastrointestinal Tract Surgery</b></th>
													<th class="col-md-2"><b>28. Chronic Pancreatitis</b></th>
													<th class="col-md-2"><b>29. Muscle Atrophy or Weakness</b></th>
													<th class="col-md-2"><b>30. Deforming or Erosive Arthritis</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="slicc25" id="slicc25">
															<option value="-1" <?php if($slicc25==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc25==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc25==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc25_date"  name="slicc25_date" value="<?php echo $slicc25_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc26" id="slicc26">
															<option value="-1" <?php if($slicc26==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc26==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc26==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc26_date"  name="slicc26_date" value="<?php echo $slicc26_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc27" id="slicc27">
															<option value="-1" <?php if($slicc27==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc27==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc27==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc27_date"  name="slicc27_date" value="<?php echo $slicc27_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc28" id="slicc28">
															<option value="-1" <?php if($slicc28==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc28==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc28==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($slicc28==2) { echo "selected"; } ?>>2</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc28_date"  name="slicc28_date" value="<?php echo $slicc28_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc29" id="slicc29">
															<option value="-1" <?php if($slicc29==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc29==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc29==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc29_date"  name="slicc29_date" value="<?php echo $slicc29_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc30" id="slicc30">
															<option value="-1" <?php if($slicc30==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc30==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc30==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc30_date"  name="slicc30_date" value="<?php echo $slicc30_date; ?>">
														</div>
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsleseverity6_tbl" name="divsleseverity6_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>31. Osteoporosis with Fracture or Vertebral Collapse</b></th>
													<th class="col-md-2"><b>32. Avascular Necrosis</b></th>
													<th class="col-md-2"><b>33. Osteomyelitis</b></th>
													<th class="col-md-2"><b>34. Tendon rupture</b></th>
													<th class="col-md-2"><b>35. Scarring Chronic Alopecia</b></th>
													<th class="col-md-2"><b>36. Extensive scarring of panniculum other than scalp and pulp space</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="slicc31" id="slicc31">
															<option value="-1" <?php if($slicc31==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc31==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc31==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc31_date"  name="slicc31_date" value="<?php echo $slicc31_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc32" id="slicc32">
															<option value="-1" <?php if($slicc32==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc32==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc32==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($slicc32==2) { echo "selected"; } ?>>2</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc32_date"  name="slicc32_date" value="<?php echo $slicc32_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc33" id="slicc33">
															<option value="-1" <?php if($slicc33==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc33==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc33==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc33_date"  name="slicc33_date" value="<?php echo $slicc33_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc34" id="slicc34">
															<option value="-1" <?php if($slicc34==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc34==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc34==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($slicc34==2) { echo "selected"; } ?>>2</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc34_date"  name="slicc34_date" value="<?php echo $slicc34_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc35" id="slicc35">
															<option value="-1" <?php if($slicc35==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc35==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc35==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc35_date"  name="slicc35_date" value="<?php echo $slicc35_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc36" id="slicc36">
															<option value="-1" <?php if($slicc36==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc36==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc36==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc36_date"  name="slicc36_date" value="<?php echo $slicc36_date; ?>">
														</div>
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsleseverity7_tbl" name="divsleseverity7_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>37. Skin ulceration (excluding thrombosis) for ≥6 months</b></th>
													<th class="col-md-2"><b>38. Premature Gonadal Failure</b></th>
													<th class="col-md-2"><b>39. Diabetes (regardless of therapy)</b></th>
													<th class="col-md-2"><b>40. Malignancy (except dysplasia)</b></th>
													<th class="col-md-2"><b>41. Cataract</b></th>
													<th class="col-md-2"><b>SLICC / ACR Damage Index</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="slicc37" id="slicc37">
															<option value="-1" <?php if($slicc37==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc37==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc37==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc37_date"  name="slicc37_date" value="<?php echo $slicc37_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc38" id="slicc38">
															<option value="-1" <?php if($slicc38==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc38==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc38==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc38_date"  name="slicc38_date" value="<?php echo $slicc38_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc39" id="slicc39">
															<option value="-1" <?php if($slicc39==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc39==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($slicc39==1) { echo "selected"; } ?>>1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc39_date"  name="slicc39_date" value="<?php echo $slicc39_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc40" id="slicc40">
															<option value="-1" <?php if($slicc40==-1) { echo "selected"; } ?>>--</option>
															<option value="0" <?php if($slicc40==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if(slicc40==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($slicc40==2) { echo "selected"; } ?>>2</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc40_date"  name="slicc40_date" value="<?php echo $slicc40_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="slicc41" id="slicc41">
															<?php echo "slicc41".$slicc41; ?>
															<option value="-1" <?php if($slicc41==-1) { echo "selected"; } ?> >--</option>
															<option value="0" <?php if($slicc41==0) { echo "selected"; } ?> >0</option>
															<option value="1" <?php if($slicc41==1) { echo "selected"; } ?> >1</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="slicc41_date"  name="slicc41_date" value="<?php echo $slicc41_date; ?>">
														</div>
													</td>
													<td>
														<input class="form-control" type="text" readonly="true" id="slicc42" name="slicc42" value="<?php echo $slicc42; ?>">
													</td>
												</tr>
											 </tbody>
										</table>
										</br>
										<label>SLE Severity Index</label>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsleseverity8_tbl" name="divsleseverity8_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>1. Overall Physician Assessment</b></th>
													<th class="col-md-2"><b>2. Constitutional symptoms</b></th>
													<th class="col-md-2"><b>3. Mucocutaneous</b></th>
													<th class="col-md-2"><b>4. Gastrointestinal</b></th>
													<th class="col-md-2"><b>5. Respiratory</b></th>
													<th class="col-md-2"><b>6. Musculoskeletal</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="index1" id="index1">
															<option value="0" <?php if($index1==0) { echo "selected"; } ?>>--</option>
															<option value="1" <?php if($index1==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($index1==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($index1==3) { echo "selected"; } ?>>3</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="index1_date"  name="index1_date" value="<?php echo $index1_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="index2" id="index2">
															<option value="0" <?php if($index2==0) { echo "selected"; } ?>>--</option>
															<option value="2" <?php if($index2==2) { echo "selected"; } ?>>2</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="index2_date"  name="index2_date" value="<?php echo $index2_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="index3" id="index3">
															<option value="0" <?php if($index3==0) { echo "selected"; } ?>>--</option>
															<option value="2" <?php if($index3==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($index3==3) { echo "selected"; } ?>>3</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="index3_date"  name="index3_date" value="<?php echo $index3_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="index4" id="index4">
															<option value="0" <?php if($index4==0) { echo "selected"; } ?>>--</option>
															<option value="2" <?php if($index4==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($index4==3) { echo "selected"; } ?>>3</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="index4_date"  name="index4_date" value="<?php echo $index4_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="index5" id="index5">
															<option value="0" <?php if($index5==0) { echo "selected"; } ?>>--</option>
															<option value="2" <?php if($index5==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($index5==3) { echo "selected"; } ?>>3</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="index5_date"  name="index5_date" value="<?php echo $index5_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="index6" id="index6">
															<option value="0" <?php if($index6==0) { echo "selected"; } ?>>--</option>
															<option value="2" <?php if($index6==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($index6==3) { echo "selected"; } ?>>3</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="index6_date"  name="index6_date" value="<?php echo $index6_date; ?>">
														</div>
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsleseverity9_tbl" name="divsleseverity9_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>7. Hematologic</b></th>
													<th class="col-md-2"><b>8. Ophthalmologic</b></th>
													<th class="col-md-2"><b>9. Cardiovascular</b></th>
													<th class="col-md-2"><b>10. Neurological</b></th>
													<th class="col-md-2"><b>11. Renal</b></th>
													<th class="col-md-2"><b>12. Use of immunosuppressants (due to active/refractory SLE)</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="index7" id="index7">
															<option value="0" <?php if($index7==0) { echo "selected"; } ?>>--</option>
															<option value="2" <?php if($index7==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($index7==3) { echo "selected"; } ?>>3</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="index7_date"  name="index7_date" value="<?php echo $index7_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="index8" id="index8">
															<option value="0" <?php if($index8==0) { echo "selected"; } ?>>--</option>
															<option value="2" <?php if($index8==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($index8==3) { echo "selected"; } ?>>3</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="index8_date"  name="index8_date" value="<?php echo $index8_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="index9" id="index9">
															<option value="0" <?php if($index9==0) { echo "selected"; } ?>>--</option>
															<option value="2" <?php if($index9==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($index9==3) { echo "selected"; } ?>>3</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="index9_date"  name="index9_date" value="<?php echo $index9_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="index10" id="index10">
															<option value="0" <?php if($index10==0) { echo "selected"; } ?>>--</option>
															<option value="2" <?php if($index10==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($index10==3) { echo "selected"; } ?>>3</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="index10_date"  name="index10_date" value="<?php echo $index10_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="index11" id="index11">
															<option value="0" <?php if($index11==0) { echo "selected"; } ?>>--</option>
															<option value="2" <?php if($index11==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($index11==3) { echo "selected"; } ?>>3</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="index11_date"  name="index11_date" value="<?php echo $index11_date; ?>">
														</div>
													</td>
													<td>
														<select class="form-control" name="index12" id="index12">
															<option value="0" <?php if($index12==0) { echo "selected"; } ?>>--</option>
															<option value="2" <?php if($index12==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($index12==3) { echo "selected"; } ?>>3</option>
														</select>
														<div class="input-group date">
														  <div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														  </div>
														  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="index12_date"  name="index12_date" value="<?php echo $index12_date; ?>">
														</div>
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsleseverity9_tbl" name="divsleseverity9_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-4"><b>SLE Severity Index</b></th>
													<th class="col-md-8"><b>&nbsp;</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<input class="form-control" type="text" readonly="true" id="index13" name="index13" value="<?php echo $index13; ?>">
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
											 </tbody>
										</table>
										</br>
										<label>CNS/Nephritis Outcome</label>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsleseverity8_tbl" name="divsleseverity8_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>Neuropsychiatric Lupus Outcome</b></th>
													<th class="col-md-2"><b>Chronic kidney disease (stage III or higher)</b></th>
													<th class="col-md-2"><b>End-stage kidney disease</b></th>
													<th class="col-md-2"><b>Kidney transplantation</b></th>
													<th class="col-md-4"><b>&nbsp;</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="cns1" id="cns1">
															<option value="0" <?php if($cns1==0) { echo "selected"; } ?>>No deficit</option>
															<option value="1" <?php if($cns1==1) { echo "selected"; } ?>>Mild neurological deficit</option>
															<option value="2" <?php if($cns1==2) { echo "selected"; } ?>>Moderate neurological deficit</option>
															<option value="3" <?php if($cns1==3) { echo "selected"; } ?>>Moderate neurological deficit</option>
															<option value="4" <?php if($cns1==4) { echo "selected"; } ?>>Death</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="cns2" id="cns2">
															<option value="0" <?php if($cns2==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($cns2==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($cns2==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="cns3" id="cns3">
															<option value="0" <?php if($cns3==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($cns3==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($cns3==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="cns4" id="cns4">
															<option value="0" <?php if($cns4==0) { echo "selected"; } ?>>Unknown</option>
															<option value="1" <?php if($cns4==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($cns4==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														&nbsp;
													</td>
												</tr>
											 </tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Quality of life start div -->
				<div class="row">
					<div class="col-md-12">
						<div class="box box-primary">
							<div class="box-header with-border">
								<h3 class="box-title">Quality of Life-Productivity</small></h3>
							</div>
							<div class="box-body">
								<div class="row">
									<div class="row">
										<div class="col-md-12">
											<div class="col-md-4">
											<label>Euroqol</label>
												<select class="form-control" name="euroqol" id="euroqol">
													<option value="0" <?php if($euroqol==0) { echo "selected"; } ?>>--</option>
													<option value="1" <?php if($euroqol==1) { echo "selected"; } ?>>No</option>
													<option value="2" <?php if($euroqol==2) { echo "selected"; } ?>>Yes</option>
												</select>
											</div>
											<div class="col-md-4">
												<label>ASAS-HI</label>
												<select class="form-control" name="asas" id="asas">
													<?php
														for($i=0;$i<=17;$i++)
														{
													?>
													<option value="<?php echo $i; ?>" <?php if($asas==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
													<?php
														}
													?>
												</select>
											</div>
											<div class="col-md-4">
												<label>PSAID</label>
												<select class="form-control" name="psaid" id="psaid">
												<?php
													for($i=0;$i<=10;$i=$i+0.1)
													{
												?>
												<option value="<?php echo $i; ?>" <?php if($psaid==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
												<?php
													}
												?>
												</select>
											</div>
										</div>
									</div>
									<?php
											$order = get_euroqol($patient_followup_id);
											$result = pg_fetch_array($order);
											$patient_euroqol_id=$result['patient_euroqol_id'];
											$eq1=$result['eq1'];
											$eq2=$result['eq2'];
											$eq3=$result['eq3'];
											$eq4=$result['eq4'];
											$eq5=$result['eq5'];
											$eq6=$result['eq6'];
											$euroqol_score=$result['euroqol_score'];
											//$asqol=$result['asqol'];
											//$dlqi=$result['dlqi'];
											$eq5d=$result['eq5d'];
									?>
									<input type="hidden" id="patient_euroqol_id" name="patient_euroqol_id" value="<?php echo $patient_euroqol_id; ?>">
									<div class="form-group col-md-12" id="diveuroqol" name="diveuroqol" <?php if($euroqol==2) { ?> style="DISPLAY:block;" <?php } else { ?>  style="DISPLAY:none;"  <?php } ?>>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="diveuroqol_tbl" name="diveuroqol_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-1"><b>EQ-1</b></th>
													<th class="col-md-1"><b>EQ-2</b></th>
													<th class="col-md-1"><b>EQ-3</b></th>
													<th class="col-md-1"><b>EQ-4</b></th>
													<th class="col-md-1"><b>EQ-5</b></th>
													<th class="col-md-1"><b>EQ-6</b></th>
													<th class="col-md-2"><b>Euroqol total</b></th>
													<th class="col-md-2"><b>EQ 5D score</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="eq1" id="eq1">
															<option value="1" <?php if($eq1==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($eq1==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($eq1==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="eq2" id="eq2">
															<option value="1" <?php if($eq2==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($eq2==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($eq2==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="eq3" id="eq3">
															<option value="1" <?php if($eq3==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($eq3==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($eq3==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="eq4" id="eq4">
															<option value="1" <?php if($eq4==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($eq4==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($eq4==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="eq5" id="eq5">
															<option value="1" <?php if($eq5==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($eq5==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($eq5==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="eq6" id="eq6">
															<option value="1" <?php if($eq6==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($eq6==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($eq6==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<input type="text" readonly id="euroqol_score" name="euroqol_score" value="<?php echo $euroqol_score; ?>">
													</td>
													<td>
														<input type="text" readonly id="eq5d" name="eq5d" value="<?php echo $eq5d; ?>">
													</td>
												</tr>
											 </tbody>
										</table>
									 </div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label>WPAI:RA</label>
											<select class="form-control" name="wpai" id="wpai">
												<option value="0" <?php if($wpai==0) { echo "selected"; } ?>>--</option>
												<option value="1" <?php if($wpai==1) { echo "selected"; } ?>>No</option>
												<option value="2" <?php if($wpai==2) { echo "selected"; } ?>>Yes</option>
											</select>
									</div>
									<?php
											$order = get_wpai($patient_followup_id);
											$result = pg_fetch_array($order);
											$patient_wpai_id=$result['patient_wpai_id'];
											$wpai1=$result['wpai1'];
											$wpai2=$result['wpai2'];
											$wpai3=$result['wpai3'];
											$wpai4=$result['wpai4'];
											$wpai5=$result['wpai5'];
											$wpai6=$result['wpai6'];
									?>
									<input type="hidden" id="patient_wpai_id" name="patient_wpai_id" value="<?php echo $patient_wpai_id; ?>">
									<div class="form-group col-md-12" id="divwpai" name="divwpai" <?php if($wpai==2) { ?> style="DISPLAY:block;" <?php } else { ?>  style="DISPLAY:none;"  <?php } ?>>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divwpai_tbl" name="divwpai_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>WPAI-1</b></th>
													<th class="col-md-2"><b>WPAI-2</b></th>
													<th class="col-md-2"><b>WPAI-3</b></th>
													<th class="col-md-2"><b>WPAI-4</b></th>
													<th class="col-md-2"><b>WPAI-5</b></th>
													<th class="col-md-2"><b>WPAI-6</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="wpai1" id="wpai1">
															<option value="1" <?php if($wpai1==1) { echo "selected"; } ?>>Yes</option>
															<option value="2" <?php if($wpai1==2) { echo "selected"; } ?>>No</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="wpai2" id="wpai2">
															<?php
																for($i=0;$i<=100;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($wpai2==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="wpai3" id="wpai3">
															<?php
																for($i=0;$i<=100;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($wpai3==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="wpai4" id="wpai4">
															<?php
																for($i=0;$i<=100;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($wpai4==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="wpai5" id="wpai5">
															<?php
																for($i=0;$i<=10;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($wpai5==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
													<td>
														<select class="form-control" name="wpai6" id="wpai6">
															<?php
																for($i=0;$i<=10;$i++)
																{
															?>
															<option value="<?php echo $i; ?>" <?php if($wpai6==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
															<?php
																}
															?>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>
									 </div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label>HADS</label>
											<select class="form-control" name="hads" id="hads">
												<option value="0" <?php if($hads==0) { echo "selected"; } ?>>--</option>
												<option value="1" <?php if($hads==1) { echo "selected"; } ?>>No</option>
												<option value="2" <?php if($hads==2) { echo "selected"; } ?>>Yes</option>
											</select>
									</div>
									<?php
											$order = get_hads($patient_followup_id);
											$result = pg_fetch_array($order);
											$patient_hads_id=$result['patient_hads_id'];
											$hads1=$result['hads1'];
											$hads2=$result['hads2'];
											$hads3=$result['hads3'];
											$hads4=$result['hads4'];
											$hads5=$result['hads5'];
											$hads6=$result['hads6'];
											$hads7=$result['hads7'];
											$hads8=$result['hads8'];
											$hads9=$result['hads9'];
											$hads10=$result['hads10'];
											$hads11=$result['hads11'];
											$hads12=$result['hads12'];
											$hads13=$result['hads13'];
											$hads14=$result['hads14'];
									?>
									<input type="hidden" id="patient_hads_id" name="patient_hads_id" value="<?php echo $patient_hads_id; ?>">
									<div class="form-group col-md-12" id="divhads" name="divhads" <?php if($hads==2) { ?> style="DISPLAY:block;" <?php } else { ?>  style="DISPLAY:none;"  <?php } ?>>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divhads_tbl" name="divhads_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>HADS-1</b></th>
													<th class="col-md-2"><b>HADS-2</b></th>
													<th class="col-md-2"><b>HADS-3</b></th>
													<th class="col-md-2"><b>HADS-4</b></th>
													<th class="col-md-2"><b>HADS-5</b></th>
													<th class="col-md-1"><b>HADS-6</b></th>
													<th class="col-md-1"><b>HADS-7</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="hads1" id="hads1">
															<option value="0" <?php if($hads1==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($hads1==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($hads1==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($hads1==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="hads2" id="hads2">
															<option value="0" <?php if($hads2==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($hads2==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($hads2==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($hads2==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="hads3" id="hads3">
															<option value="0" <?php if($hads3==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($hads3==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($hads3==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($hads3==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="hads4" id="hads4">
															<option value="0" <?php if($hads4==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($hads4==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($hads4==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($hads4==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="hads5" id="hads5">
															<option value="0" <?php if($hads5==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($hads5==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($hads5==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($hads5==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="hads6" id="hads6">
															<option value="0" <?php if($hads6==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($hads6==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($hads6==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($hads6==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="hads7" id="hads7">
															<option value="0" <?php if($hads7==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($hads7==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($hads7==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($hads7==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divhads2_tbl" name="divhads2_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-2"><b>HADS-8</b></th>
													<th class="col-md-2"><b>HADS-9</b></th>
													<th class="col-md-2"><b>HADS-10</b></th>
													<th class="col-md-2"><b>HADS-11</b></th>
													<th class="col-md-2"><b>HADS-12</b></th>
													<th class="col-md-1"><b>HADS-13</b></th>
													<th class="col-md-1"><b>HADS-14</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="hads8" id="hads8">
															<option value="0" <?php if($hads8==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($hads8==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($hads8==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($hads8==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="hads9" id="hads9">
															<option value="0" <?php if($hads9==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($hads9==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($hads9==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($hads9==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="hads10" id="hads10">
															<option value="0" <?php if($hads10==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($hads10==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($hads10==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($hads10==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="hads11" id="hads11">
															<option value="0" <?php if($hads11==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($hads11==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($hads11==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($hads11==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="hads12" id="hads12">
															<option value="0" <?php if($hads12==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($hads12==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($hads12==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($hads12==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="hads13" id="hads13">
															<option value="0" <?php if($hads13==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($hads13==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($hads13==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($hads13==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="hads14" id="hads14">
															<option value="0" <?php if($hads14==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($hads14==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($hads14==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($hads14==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>
									 </div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label>SF-36</label>
											<select class="form-control" name="sf36" id="sf36">
												<option value="0" <?php if($sf36==0) { echo "selected"; } ?>>--</option>
												<option value="1" <?php if($sf36==1) { echo "selected"; } ?>>No</option>
												<option value="2" <?php if($sf36==2) { echo "selected"; } ?>>Yes</option>
											</select>
									</div>
									<?php
										
											$order = get_sf36($patient_followup_id);
											$result = pg_fetch_array($order);
											$patient_sf36_id=$result['patient_sf36_id'];
											$sf361=$result['sf361'];
											$sf362=$result['sf362'];
											$sf363a=$result['sf363a'];
											$sf363b=$result['sf363b'];
											$sf363c=$result['sf363c'];
											$sf363d=$result['sf363d'];
											$sf363e=$result['sf363e'];
											$sf363f=$result['sf363f'];
											$sf363g=$result['sf363g'];
											$sf363h=$result['sf363h'];
											$sf363i=$result['sf363i'];
											$sf363j=$result['sf363j'];
											$sf364a=$result['sf364a'];
											$sf364b=$result['sf364b'];
											$sf364c=$result['sf364c'];
											$sf364d=$result['sf364d'];
											$sf365a=$result['sf365a'];
											$sf365b=$result['sf365b'];
											$sf365c=$result['sf365c'];
											$sf366=$result['sf366'];
											$sf367=$result['sf367'];
											$sf368=$result['sf368'];
											$sf369a=$result['sf369a'];
											$sf369b=$result['sf369b'];
											$sf369c=$result['sf369c'];
											$sf369d=$result['sf369d'];
											$sf369e=$result['sf369e'];
											$sf369f=$result['sf369f'];
											$sf369g=$result['sf369g'];
											$sf369h=$result['sf369h'];
											$sf369i=$result['sf369i'];
											$sf3610=$result['sf3610'];
											$sf3611a=$result['sf3611a'];
											$sf3611b=$result['sf3611b'];
											$sf3611c=$result['sf3611c'];
											$sf3611d=$result['sf3611d'];
									?>
									<input type="hidden" id="patient_sf36_id" name="patient_sf36_id" value="<?php echo $patient_sf36_id; ?>">
									<div class="form-group col-md-12" id="divsf36" name="divsf36" <?php if($sf36==2) { ?> style="DISPLAY:block;" <?php } else { ?>  style="DISPLAY:none;"  <?php } ?>>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsf36_tbl" name="divsf36_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-1"><b>SF36-1</b></th>
													<th class="col-md-1"><b>SF36-2</b></th>
													<th class="col-md-1"><b>SF36-3A</b></th>
													<th class="col-md-1"><b>SF36-3B</b></th>
													<th class="col-md-1"><b>SF36-3C</b></th>
													<th class="col-md-1"><b>SF36-3D</b></th>
													<th class="col-md-1"><b>SF36-3E</b></th>
													<th class="col-md-1"><b>SF36-3F</b></th>
													<th class="col-md-1"><b>SF36-3G</b></th>
													<th class="col-md-1"><b>SF36-3H</b></th>
													<th class="col-md-1"><b>SF36-3I</b></th>
													<th class="col-md-1"><b>SF36-3J</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="sf361" id="sf361">
															<option value="0" <?php if($sf361==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf361==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf361==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf361==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf361==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf361==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf362" id="sf362">
															<option value="0" <?php if($sf362==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf362==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf362==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf362==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf362==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf362==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf363a" id="sf363a">
															<option value="0" <?php if($sf363a==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf363a==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf363a==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf363a==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf363b" id="sf363b">
															<option value="0" <?php if($sf363b==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf363b==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf363b==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf363b==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf363c" id="sf363c">
															<option value="0" <?php if($sf363c==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf363c==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf363c==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf363c==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf363d" id="sf363d">
															<option value="0" <?php if($sf363d==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf363d==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf363d==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf363d==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf363e" id="sf363e">
															<option value="0" <?php if($sf363e==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf363e==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf363e==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf363e==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf363f" id="sf363f">
															<option value="0" <?php if($sf363f==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf363f==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf363f==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf363f==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf363g" id="sf363g">
															<option value="0" <?php if($sf363g==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf363g==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf363g==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf363g==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf363h" id="sf363h">
															<option value="0" <?php if($sf363h==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf363h==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf363h==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf363h==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf363i" id="sf363i">
															<option value="0" <?php if($sf363i==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf363i==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf363i==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf363i==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf363j" id="sf363j">
															<option value="0" <?php if($sf363j==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf363j==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf363j==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf363j==3) { echo "selected"; } ?>>3</option>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsf362_tbl" name="divsf362_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-1"><b>SF36-4A</b></th>
													<th class="col-md-1"><b>SF36-4B</b></th>
													<th class="col-md-1"><b>SF36-4C</b></th>
													<th class="col-md-1"><b>SF36-4D</b></th>
													<th class="col-md-1"><b>SF36-5A</b></th>
													<th class="col-md-1"><b>SF36-5B</b></th>
													<th class="col-md-1"><b>SF36-5C</b></th>
													<th class="col-md-1"><b>SF36-6</b></th>
													<th class="col-md-1"><b>SF36-7</b></th>
													<th class="col-md-1"><b>SF36-8</b></th>
													<th class="col-md-1"><b>SF36-9A</b></th>
													<th class="col-md-1"><b>SF36-9B</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="sf364a" id="sf364a">
															<option value="0" <?php if($sf364a==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf364a==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf364a==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf364a==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf364a==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf364a==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf364b" id="sf364b">
															<option value="0" <?php if($sf364b==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf364b==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf364b==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf364b==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf364b==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf364b==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf364c" id="sf364c">
															<option value="0" <?php if($sf364c==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf364c==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf364c==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf364c==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf364c==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf364c==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf364d" id="sf364d">
															<option value="0" <?php if($sf364d==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf364d==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf364d==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf364d==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf364d==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf364d==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf365a" id="sf365a">
															<option value="0" <?php if($sf365a==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf365a==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf365a==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf365a==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf365a==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf365a==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf365b" id="sf365b">
															<option value="0" <?php if($sf365b==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf365b==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf365b==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf365b==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf365b==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf365b==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf365c" id="sf365c">
															<option value="0" <?php if($sf365c==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf365c==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf365c==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf365c==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf365c==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf365c==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf366" id="sf366">
															<option value="0" <?php if($sf366==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf366==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf366==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf366==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf366==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf366==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf367" id="sf367">
															<option value="0" <?php if($sf367==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf367==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf367==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf367==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf367==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf367==5) { echo "selected"; } ?>>5</option>
															<option value="5" <?php if($sf367==6) { echo "selected"; } ?>>6</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf368" id="sf368">
															<option value="0" <?php if($sf368==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf368==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf368==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf368==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf368==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf368==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf369a" id="sf369a">
															<option value="0" <?php if($sf369a==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf369a==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf369a==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf369a==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf369a==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf369a==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf369b" id="sf369b">
															<option value="0" <?php if($sf369b==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf369b==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf369b==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf369b==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf369b==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf369b==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>
										<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="divsf363_tbl" name="divsf363_tbl"  width="100%"  rules="all">
											<thead>
												  <tr align="center"> 
													<th class="col-md-1"><b>SF36-9C</b></th>
													<th class="col-md-1"><b>SF36-9D</b></th>
													<th class="col-md-1"><b>SF36-9E</b></th>
													<th class="col-md-1"><b>SF36-9F</b></th>
													<th class="col-md-1"><b>SF36-9G</b></th>
													<th class="col-md-1"><b>SF36-9H</b></th>
													<th class="col-md-1"><b>SF36-9I</b></th>
													<th class="col-md-1"><b>SF36-10</b></th>
													<th class="col-md-1"><b>SF36-11A</b></th>
													<th class="col-md-1"><b>SF36-11B</b></th>
													<th class="col-md-1"><b>SF36-11C</b></th>
													<th class="col-md-1"><b>SF36-11D</b></th>
												</tr>
											 </thead>
											 <tbody>
												<tr>
													<td>
														<select class="form-control" name="sf369c" id="sf369c">
															<option value="0" <?php if($sf369c==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf369c==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf369c==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf369c==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf369c==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf369c==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf369d" id="sf369d">
															<option value="0" <?php if($sf369d==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf369d==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf369d==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf369d==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf369d==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf369d==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf369e" id="sf369e">
															<option value="0" <?php if($sf369e==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf369e==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf369e==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf369e==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf369e==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf369e==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf369f" id="sf369f">
															<option value="0" <?php if($sf369f==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf369f==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf369f==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf369f==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf369f==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf369f==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf369g" id="sf369g">
															<option value="0" <?php if($sf369g==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf369g==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf369g==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf369g==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf369g==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf369g==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf369h" id="sf369h">
															<option value="0" <?php if($sf369h==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf369h==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf369h==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf369h==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf369h==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf369h==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf369i" id="sf369i">
															<option value="0" <?php if($sf369i==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf369i==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf369i==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf369i==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf369i==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf369i==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf3610" id="sf3610">
															<option value="0" <?php if($sf3610==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf3610==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf3610==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf3610==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf3610==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf3610==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf3611a" id="sf3611a">
															<option value="0" <?php if($sf3611a==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf3611a==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf3611a==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf3611a==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf3611a==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf3611a==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf3611b" id="sf3611b">
															<option value="0" <?php if($sf3611b==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf3611b==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf3611b==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf3611b==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf3611b==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf3611b==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf3611c" id="sf3611c">
															<option value="0" <?php if($sf3611c==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf3611c==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf3611c==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf3611c==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf3611c==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf3611c==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
													<td>
														<select class="form-control" name="sf3611d" id="sf3611d">
															<option value="0" <?php if($sf3611d==0) { echo "selected"; } ?>>0</option>
															<option value="1" <?php if($sf3611d==1) { echo "selected"; } ?>>1</option>
															<option value="2" <?php if($sf3611d==2) { echo "selected"; } ?>>2</option>
															<option value="3" <?php if($sf3611d==3) { echo "selected"; } ?>>3</option>
															<option value="4" <?php if($sf3611d==4) { echo "selected"; } ?>>4</option>
															<option value="5" <?php if($sf3611d==5) { echo "selected"; } ?>>5</option>
														</select>
													</td>
												</tr>
											 </tbody>
										</table>
										</br>
									 </div>
								</div>
							</div>
						</div>
					</div>
				</div><!-- Quality of life end div -->
				
				<div class="row">
					<div class="form-group col-md-12">
						<input type="submit" class="btn btn-primary" value="Save">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						&nbsp;
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						&nbsp;
					</div>
				</div>
			</div>
		</div>
		</form>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
   <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>


$("#slicc1").change(function(){ sliccindex(); });
$("#slicc2").change(function(){ sliccindex(); });
$("#slicc3").change(function(){ sliccindex(); });
$("#slicc4").change(function(){ sliccindex(); });
$("#slicc5").change(function(){ sliccindex(); });
$("#slicc6").change(function(){ sliccindex(); });
$("#slicc7").change(function(){ sliccindex(); });
$("#slicc8").change(function(){ sliccindex(); });
$("#slicc9").change(function(){ sliccindex(); });
$("#slicc10").change(function(){ sliccindex(); });
$("#slicc11").change(function(){ sliccindex(); });
$("#slicc12").change(function(){ sliccindex(); });
$("#slicc13").change(function(){ sliccindex(); });
$("#slicc14").change(function(){ sliccindex(); });
$("#slicc15").change(function(){ sliccindex(); });
$("#slicc16").change(function(){ sliccindex(); });
$("#slicc17").change(function(){ sliccindex(); });
$("#slicc18").change(function(){ sliccindex(); });
$("#slicc19").change(function(){ sliccindex(); });
$("#slicc20").change(function(){ sliccindex(); });
$("#slicc21").change(function(){ sliccindex(); });
$("#slicc22").change(function(){ sliccindex(); });
$("#slicc23").change(function(){ sliccindex(); });
$("#slicc24").change(function(){ sliccindex(); });
$("#slicc25").change(function(){ sliccindex(); });
$("#slicc26").change(function(){ sliccindex(); });
$("#slicc27").change(function(){ sliccindex(); });
$("#slicc28").change(function(){ sliccindex(); });
$("#slicc29").change(function(){ sliccindex(); });
$("#slicc30").change(function(){ sliccindex(); });
$("#slicc31").change(function(){ sliccindex(); });
$("#slicc32").change(function(){ sliccindex(); });
$("#slicc33").change(function(){ sliccindex(); });
$("#slicc34").change(function(){ sliccindex(); });
$("#slicc35").change(function(){ sliccindex(); });
$("#slicc36").change(function(){ sliccindex(); });
$("#slicc37").change(function(){ sliccindex(); });
$("#slicc38").change(function(){ sliccindex(); });
$("#slicc39").change(function(){ sliccindex(); });
$("#slicc40").change(function(){ sliccindex(); });
$("#slicc41").change(function(){ sliccindex(); });
$("#index1").change(function(){ indexindex(); });
$("#index2").change(function(){ indexindex(); });
$("#index3").change(function(){ indexindex(); });
$("#index4").change(function(){ indexindex(); });
$("#index5").change(function(){ indexindex(); });
$("#index6").change(function(){ indexindex(); });
$("#index7").change(function(){ indexindex(); });
$("#index8").change(function(){ indexindex(); });
$("#index9").change(function(){ indexindex(); });
$("#index10").change(function(){ indexindex(); });
$("#index11").change(function(){ indexindex(); });
$("#index12").change(function(){ indexindex(); });

function indexindex()
{
	var sum1 =0;
	var sum2 =0;
	var sum3 =0;
	
	for (i=1;i<=12;i++)
	{
		if ($("#index"+i).val()==1)
			sum1++;
		
		if ($("#index"+i).val()==2)
			sum2++;
		
		if ($("#index"+i).val()==3)
			sum3++;
	}
	
	if(sum1>0)
		$("#index13").val("Mild");
	if(sum2>0)
		$("#index13").val("Moderate");
	if(sum3>0)
		$("#index13").val("Severe");
	if (sum1==0 && sum2==0 && sum3==0)
		$("#index13").val("");
}

function sliccindex()
{
	var sum =0;
	
	for (i=1;i<=41;i++)
	{
		if (i==7 || i==8)
		{
			if($("#slicc9").val()!=3 && $("#slicc"+i).val()!=-1)
				sum += parseInt($("#slicc"+i).val());
		}
		else
		{
			if ($("#slicc"+i).val()!=-1)
				sum += parseInt($("#slicc"+i).val());
		}
	}
	
	$("#slicc42").val(sum);
}


$("#flare1").change(function(){ flareindex(); });
$("#flare2").change(function(){ flareindex(); });
$("#flare3").change(function(){ flareindex(); });
$("#flare4").change(function(){ flareindex(); });
$("#flare5").change(function(){ flareindex(); });
$("#flare6").change(function(){ flareindex(); });
$("#flare7").change(function(){ flareindex(); });
$("#flare8").change(function(){ flareindex(); });
$("#flare9").change(function(){ flareindex(); });
$("#flare10").change(function(){ flareindex(); });
$("#flare11").change(function(){ flareindex(); });
$("#flare12").change(function(){ flareindex(); });
$("#flare13").change(function(){ flareindex(); });

function flareindex()
{
	var sum =0;
	
	if ($("#flare1").val()==1 || $("#flare2").val()==1 || $("#flare3").val()==1 || $("#flare4").val()==1 || $("#flare5").val()==1)
		sum =1;
	
	if ($("#flare6").val()==1 || $("#flare7").val()==1 || $("#flare8").val()==1 || $("#flare9").val()==1 || $("#flare10").val()==1 || $("#flare11").val()==1 || $("#flare12").val()==1 || $("#flare13").val()==1)
		sum =2;
	
	$("#flare14").val(sum);
}
$("#nefritis2").focusout(function() 
{
	if($("#nefritis2").val()=="")
		$("#nefritis2").val("");
	else
		$("#nefritis2").val(parseFloat($("#nefritis2").val()));
	
	common_calc(); 
	
});
function sledai()
{
	if ($("#sledai9").val()==1)
		$("#divsle2b_tbl").show();
	else
		$("#divsle2b_tbl").hide();
	
	if ($("#sledai9").val()==1)
		$("#tdsledai9b").show();
	else
		$("#tdsledai9b").hide();
	
	if ($("#sledai12").val()==1 || $("#sledai13").val()==1 || $("#sledai14").val()==1 || $("#sledai15").val()==1 || $("#sledai16").val()==1)
		$("#divsle4_tbl").show();
	else
		$("#divsle4_tbl").hide();
	
	if ($("#sledai12").val()==1)
		$("#tdsledai12b").show();
	else
		$("#tdsledai12b").hide();
	
	if ($("#sledai13").val()==1)
		$("#tdsledai13b").show();
	else
		$("#tdsledai13b").hide();
	
	if ($("#sledai14").val()==1)
		$("#tdsledai14b").show();
	else
		$("#tdsledai14b").hide();
	
	if ($("#sledai15").val()==1)
		$("#tdsledai15b").show();
	else
		$("#tdsledai15b").hide();
	
	if ($("#sledai16").val()==1)
		$("#tdsledai16b").show();
	else
		$("#tdsledai16b").hide();
	
	if ($("#sledai23").val()==1 || $("#sledai24").val()==1)
		$("#divsle7_tbl").show();
	else
		$("#divsle7_tbl").hide();
	
	if ($("#sledai23").val()==1)
		$("#tdsledai23b").show();
	else
		$("#tdsledai23b").hide();
	
	if ($("#sledai24").val()==1)
		$("#tdsledai24b").show();
	else
		$("#tdsledai24b").hide();
		
}

function sledai2κ()
{
	var sum=0;
	
	if ($("#sledai1").val()==1)
		sum += 8;
	if ($("#sledai2").val()==1)
		sum += 8;
	if ($("#sledai3").val()==1)
		sum += 8;
	if ($("#sledai4").val()==1)
		sum += 8;
	if ($("#sledai5").val()==1)
		sum += 8;
	if ($("#sledai6").val()==1)
		sum += 8;
	if ($("#sledai7").val()==1)
		sum += 8;
	if ($("#sledai8").val()==1)
		sum += 8;
	if ($("#sledai9").val()==1)
		sum += 4;
	if ($("#sledai10").val()==1)
		sum += 4;
	if ($("#sledai11").val()==1)
		sum += 4;
	if ($("#sledai12").val()==1)
		sum += 4;
	if ($("#sledai13").val()==1)
		sum += 4;
	if ($("#sledai14").val()==1)
		sum += 4;
	if ($("#sledai15").val()==1)
		sum += 2;
	if ($("#sledai16").val()==1)
		sum += 2;
	if ($("#sledai17").val()==1)
		sum += 2;
	if ($("#sledai18").val()==1)
		sum += 2;
	if ($("#sledai19").val()==1)
		sum += 2;
	if ($("#sledai20").val()==1)
		sum += 2;
	if ($("#sledai21").val()==1)
		sum += 2;
	if ($("#sledai22").val()==1)
		sum += 1;
	if ($("#sledai23").val()==1)
		sum += 1;
	if ($("#sledai24").val()==1)
		sum += 1;
	
	$("#sledai2k").val(sum);
}

$("#sledai1").change(function(){ sledai2κ(); });
$("#sledai2").change(function(){ sledai2κ(); });
$("#sledai3").change(function(){ sledai2κ(); });
$("#sledai4").change(function(){ sledai2κ(); });
$("#sledai5").change(function(){ sledai2κ(); });
$("#sledai6").change(function(){ sledai2κ(); });
$("#sledai7").change(function(){ sledai2κ(); });
$("#sledai8").change(function(){ sledai2κ(); });
$("#sledai9").change(function()
{
	sledai();
	sledai2κ();	
});

$("#sledai10").change(function(){ sledai2κ(); });
$("#sledai11").change(function(){ sledai2κ(); });
$("#sledai17").change(function(){ sledai2κ(); });
$("#sledai18").change(function(){ sledai2κ(); });
$("#sledai19").change(function(){ sledai2κ(); });
$("#sledai20").change(function(){ sledai2κ(); });
$("#sledai21").change(function(){ sledai2κ(); });
$("#sledai22").change(function(){ sledai2κ(); });
$("#sledai23").change(function()
{
	sledai();
	sledai2κ();	
});
$("#sledai24").change(function(){
	sledai(); 
	sledai2κ();	
});
$("#sledai12").change(function(){
	sledai();
	sledai2κ();	
});
$("#sledai13").change(function(){
	sledai();
	sledai2κ();		
});
$("#sledai14").change(function(){
	sledai();
	sledai2κ();	
});
$("#sledai15").change(function(){
	sledai();
	sledai2κ();	
});
$("#sledai16").change(function(){
	sledai();
	sledai2κ();	
});

$('[id^=dose_]').focusout(function() 
{
	/*if (($(this).val()).charAt(0)=="0")
		$(this).val(($(this).val()).substr(1));
	
	if(!isNaN($(this).val()))
	{
		if ($(this).val()=="")
			$(this).val("0");
	}
	else
	{
		$(this).val(($(this).val()).slice(0,-1));
		if ($(this).val()=="")
			$(this).val("0");
	}*/
	
	if($(this).val()=="")
		$(this).val("0");
	else
		$(this).val(parseFloat($(this).val()));
});	
		
jQuery(document).ready(function() {
	
	sledai();
	
for(i=1;i<=41;i++)
{
	$('#slicc'+i+'_date').datepicker({
		   dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
		})
		$('#slicc'+i+'_date').inputmask();
}

for(i=1;i<=12;i++)
{
	$('#index'+i+'_date').datepicker({
		   dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
		})
		$('#index'+i+'_date').inputmask();
}
		
	$('#smokingyear').datepicker({
		minViewMode: 2,
		changeYear:true,
		dateFormat: 'yy',
		 yearRange: "-100:+100",
		default: "0:0",
		 clearBtn:true,
		 onSelect: function(dateText) {
		averagepackperday();
		 }
		
	});	
	
	$('#smokingstarting').datepicker({
		minViewMode: 2,
		changeYear:true,
		dateFormat: 'yy',
		 yearRange: "-100:+100",
			default: "0:0",
			 clearBtn: true,
		 onSelect: function(dateText) {
		averagepackperday();
		 }
	});
	
	$('#start_date').datepicker({
		  dateFormat: 'dd-mm-yy',
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onSelect: function(dateText) {
				var fu_date = ($("#start_date").val()).split("-");
				var inclusion_date = ($("#inclusion_start_date").val()).split("-");
				var cohort_date = ($("#cohort_start_date").val()).split("-");
				
				d1=new Date(inclusion_date[2], inclusion_date[1], inclusion_date[0]);
				d2=new Date(fu_date[2], fu_date[1], fu_date[0]);
				
				var months_inclusion;
				months_inclusion = (d2.getFullYear() - d1.getFullYear()) * 12;
				months_inclusion -= d1.getMonth() + 1;
				months_inclusion += d2.getMonth();
				
				d3=new Date(cohort_date[2], cohort_date[1], cohort_date[0]);
				
				var months_cohort;
				months_cohort = (d2.getFullYear() - d3.getFullYear()) * 12;
				months_cohort -= d3.getMonth() + 1;
				months_cohort += d2.getMonth();
				
				cohortval1=(Math.floor(months_cohort/3))*3-3;
				cohortval2=(Math.floor(months_cohort/3))*3;
				cohortval3=(Math.floor(months_cohort/3))*3 + 3;
				cohortval4=(Math.floor(months_cohort/3))*3 + 6;
				var remainder = months_cohort % 3;
				
				$("#fumonthcohort").empty();
				
				if(remainder!=0)
				{	
					if(cohortval1>=0)
					{
						var val = new Option(cohortval1,cohortval1);
						$("#fumonthcohort").append(val);
					}
					
					if(cohortval2>=0)
					{
						var val1 = new Option(cohortval2,cohortval2);
						$("#fumonthcohort").append(val1);
					}
				}
				
				//Για να μην έχουμε κανένα με μηδενικά
				if(months_cohort>=0)
				{
					var val2 = new Option(months_cohort,months_cohort,true,true);
					$("#fumonthcohort").append(val2);
				}
				
				var val3 = new Option(cohortval3,cohortval3);
				$("#fumonthcohort").append(val3);
				var val4 = new Option(cohortval4,cohortval4);
				$("#fumonthcohort").append(val4);
				
				inclusionval1=(Math.floor(months_inclusion/3))*3-3;
				inclusionval2=(Math.floor(months_inclusion/3))*3;
				inclusionval3=(Math.floor(months_inclusion/3))*3+3;
				inclusionval4=(Math.floor(months_inclusion/3))*3+6;
				var remainder = months_inclusion % 3;
				
				$("#fumonthinclusion").empty();
				
				if(remainder!=0)
				{
					if(inclusionval1>=0)
					{
						var val1 = new Option(inclusionval1,inclusionval1);
						$("#fumonthinclusion").append(val1);
					}
					
					if(inclusionval2>=0)
					{
						var val1 = new Option(inclusionval2,inclusionval2);
						$("#fumonthinclusion").append(val1);
					}
				}
				
				//Για να μην έχουμε κανένα με μηδενικά
				if(months_inclusion>=0)
				{
					var val1 = new Option(months_inclusion,months_inclusion,true,true);
					$("#fumonthinclusion").append(val1);
				}
				
				var val3 = new Option(inclusionval3,inclusionval3);
				$("#fumonthinclusion").append(val3);
				var val4 = new Option(inclusionval4,inclusionval4);
				$("#fumonthinclusion").append(val4);
		  }
		});

for(i=1;i<=document.getElementById('patient_corticosteroids_numofrows').value;i++)
{
	$('#corticosteroids_date_'+i).datepicker({
		   dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
		})
		$('#corticosteroids_date_'+i).inputmask();
}

for(i=1;i<=document.getElementById('patient_analgesics_numofrows').value;i++)
{
	$('#startdate_analgesics_'+i).datepicker({
		  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
		})
		$('#startdate_analgesics_'+i).inputmask();
		
		$('#stopdate_analgesics_'+i).datepicker({
		 dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
		})
		$('#stopdate_analgesics_'+i).inputmask();
}

for(i=1;i<=document.getElementById('patient_cohort_drugs_numofrows').value;i++)
{
	if(document.getElementById('patient_cohort_drugs_type_'+i).value=="cohort" || document.getElementById('patient_cohort_drugs_type_'+i).value=="period")
	{
		$('#startdate_'+i).datepicker({
		   dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
		})
		$('#startdate_'+i).inputmask();
		
		$('#stopdate_'+i).datepicker({
		  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
		})
		$('#stopdate_'+i).inputmask();
		
	}
	
}

		
})


function averagepackperday()
{
		var first_val = $("#decadefirst").val();
		if (first_val=='01')
			first_val='0.25';
		else if (first_val=='02')
			first_val='0.50';
		else if (first_val=='03')
			first_val='0.75';
		else if (first_val=='11')
			first_val='1.25';
		else if (first_val=='12')
			first_val='1.50';
		else if (first_val=='13')
			first_val='1.75';
		else if (first_val=='21')
			first_val='2.25';
		else if (first_val=='22')
			first_val='2.50';
		else if (first_val=='23')
			first_val='2.75';
		else if (first_val=='31')
			first_val='3.25';
		else if (first_val=='32')
			first_val='3.50';
		else if (first_val=='33')
			first_val='3.75';
		else if (first_val=='41')
			first_val='4.25';
		else if (first_val=='42')
			first_val='4.50';
		else if (first_val=='43')
			first_val='4.75';
		else 
			first_val=first_val.substring(0, 1);
		
		var second_val = $("#decadesecond").val();
		if (second_val=='01')
			second_val='0.25';
		else if (second_val=='02')
			second_val='0.50';
		else if (second_val=='03')
			second_val='0.75';
		else if (second_val=='11')
			second_val='1.25';
		else if (second_val=='12')
			second_val='1.50';
		else if (second_val=='13')
			second_val='1.75';
		else if (second_val=='21')
			second_val='2.25';
		else if (second_val=='22')
			second_val='2.50';
		else if (second_val=='23')
			second_val='2.75';
		else if (second_val=='31')
			second_val='3.25';
		else if (second_val=='32')
			second_val='3.50';
		else if (second_val=='33')
			second_val='3.75';
		else if (second_val=='41')
			second_val='4.25';
		else if (second_val=='42')
			second_val='4.50';
		else if (second_val=='43')
			second_val='4.75';
		else 
			second_val=second_val.substring(0, 1);
		
		var third_val = $("#decadethird").val();
		if (third_val=='01')
			third_val='0.25';
		else if (third_val=='02')
			third_val='0.50';
		else if (third_val=='03')
			third_val='0.75';
		else if (third_val=='11')
			third_val='1.25';
		else if (third_val=='12')
			third_val='1.50';
		else if (third_val=='13')
			third_val='1.75';
		else if (third_val=='21')
			third_val='2.25';
		else if (third_val=='22')
			third_val='2.50';
		else if (third_val=='23')
			third_val='2.75';
		else if (third_val=='31')
			third_val='3.25';
		else if (third_val=='32')
			third_val='3.50';
		else if (third_val=='33')
			third_val='3.75';
		else if (third_val=='41')
			third_val='4.25';
		else if (third_val=='42')
			third_val='4.50';
		else if (third_val=='43')
			third_val='4.75';
		else 
			third_val=third_val.substring(0, 1);
		
		var forth_val = $("#decadeforth").val();
		if (forth_val=='01')
			forth_val='0.25';
		else if (forth_val=='02')
			forth_val='0.50';
		else if (forth_val=='03')
			forth_val='0.75';
		else if (forth_val=='11')
			forth_val='1.25';
		else if (forth_val=='12')
			forth_val='1.50';
		else if (forth_val=='13')
			forth_val='1.75';
		else if (forth_val=='21')
			forth_val='2.25';
		else if (forth_val=='22')
			forth_val='2.50';
		else if (forth_val=='23')
			forth_val='2.75';
		else if (forth_val=='31')
			forth_val='3.25';
		else if (forth_val=='32')
			forth_val='3.50';
		else if (forth_val=='33')
			forth_val='3.75';
		else if (forth_val=='41')
			forth_val='4.25';
		else if (forth_val=='42')
			forth_val='4.50';
		else if (forth_val=='43')
			forth_val='4.75';
		else 
			forth_val=forth_val.substring(0, 1);
		
		var fifth_val = $("#decadefifth").val();
		if (fifth_val=='01')
			fifth_val='0.25';
		else if (fifth_val=='02')
			fifth_val='0.50';
		else if (fifth_val=='03')
			fifth_val='0.75';
		else if (fifth_val=='11')
			fifth_val='1.25';
		else if (fifth_val=='12')
			fifth_val='1.50';
		else if (fifth_val=='13')
			fifth_val='1.75';
		else if (fifth_val=='21')
			fifth_val='2.25';
		else if (fifth_val=='22')
			fifth_val='2.50';
		else if (fifth_val=='23')
			fifth_val='2.75';
		else if (fifth_val=='31')
			fifth_val='3.25';
		else if (fifth_val=='32')
			fifth_val='3.50';
		else if (fifth_val=='33')
			fifth_val='3.75';
		else if (fifth_val=='41')
			fifth_val='4.25';
		else if (fifth_val=='42')
			fifth_val='4.50';
		else if (fifth_val=='43')
			fifth_val='4.75';
		else 
			fifth_val=fifth_val.substring(0, 1);
		
		var sixth_val = $("#decadesixth").val();
		if (sixth_val=='01')
			sixth_val='0.25';
		else if (sixth_val=='02')
			sixth_val='0.50';
		else if (sixth_val=='03')
			sixth_val='0.75';
		else if (sixth_val=='11')
			sixth_val='1.25';
		else if (sixth_val=='12')
			sixth_val='1.50';
		else if (sixth_val=='13')
			sixth_val='1.75';
		else if (sixth_val=='21')
			sixth_val='2.25';
		else if (sixth_val=='22')
			sixth_val='2.50';
		else if (sixth_val=='23')
			sixth_val='2.75';
		else if (sixth_val=='31')
			sixth_val='3.25';
		else if (sixth_val=='32')
			sixth_val='3.50';
		else if (sixth_val=='33')
			sixth_val='3.75';
		else if (sixth_val=='41')
			sixth_val='4.25';
		else if (sixth_val=='42')
			sixth_val='4.50';
		else if (sixth_val=='43')
			sixth_val='4.75';
		else 
			sixth_val=sixth_val.substring(0, 1);
		
		var seventh_val = $("#decadeseventh").val();
		if (seventh_val=='01')
			seventh_val='0.25';
		else if (seventh_val=='02')
			seventh_val='0.50';
		else if (seventh_val=='03')
			seventh_val='0.75';
		else if (seventh_val=='11')
			seventh_val='1.25';
		else if (seventh_val=='12')
			seventh_val='1.50';
		else if (seventh_val=='13')
			seventh_val='1.75';
		else if (seventh_val=='21')
			seventh_val='2.25';
		else if (seventh_val=='22')
			seventh_val='2.50';
		else if (seventh_val=='23')
			seventh_val='2.75';
		else if (seventh_val=='31')
			seventh_val='3.25';
		else if (seventh_val=='32')
			seventh_val='3.50';
		else if (seventh_val=='33')
			seventh_val='3.75';
		else if (seventh_val=='41')
			seventh_val='4.25';
		else if (seventh_val=='42')
			seventh_val='4.50';
		else if (seventh_val=='43')
			seventh_val='4.75';
		else 
			seventh_val=seventh_val.substring(0, 1);
		
		var eight_val = $("#decadeeight").val();
		if (eight_val=='01')
			eight_val='0.25';
		else if (eight_val=='02')
			eight_val='0.50';
		else if (eight_val=='03')
			eight_val='0.75';
		else if (eight_val=='11')
			eight_val='1.25';
		else if (eight_val=='12')
			eight_val='1.50';
		else if (eight_val=='13')
			eight_val='1.75';
		else if (eight_val=='21')
			eight_val='2.25';
		else if (eight_val=='22')
			eight_val='2.50';
		else if (eight_val=='23')
			eight_val='2.75';
		else if (eight_val=='31')
			eight_val='3.25';
		else if (eight_val=='32')
			eight_val='3.50';
		else if (eight_val=='33')
			eight_val='3.75';
		else if (eight_val=='41')
			eight_val='4.25';
		else if (eight_val=='42')
			eight_val='4.50';
		else if (eight_val=='43')
			eight_val='4.75';
		else 
			eight_val=eight_val.substring(0, 1);
		
		var r=0;
		
		if (first_val != 0)
			r++;
		if (second_val != 0)
			r++;
		if (third_val != 0)
			r++;
		if (forth_val != 0)
			r++;
		if (fifth_val != 0)
			r++;
		if (sixth_val != 0)
			r++;
		if (seventh_val != 0)
			r++;
		if (eight_val != 0)
			r++;
			
		if(r>0)
		{
			var sum = (parseFloat(first_val) + parseFloat(second_val) + parseFloat(third_val) + parseFloat(forth_val) + parseFloat(fifth_val) + parseFloat(sixth_val) + parseFloat(seventh_val) + parseFloat(eight_val))/r;
			var packperday=(sum.toFixed(2)).replace('.',',');
			$("#averagepackperday").val(packperday);
			
		}
		else
		{
			sum=0;
			$("#averagepackperday").val(0);
		}
		
		//if($("#smokingyear").val()!="" && $("#smokingstarting").val()!="")
		if( $("#smokingstarting").val()!="")
		{	
			var smokingyear;
			if($("#smokingyear").val()=="")
			{
				var d = new Date();
				smokingyear = d.getFullYear();
			}
			else
				smokingyear=$("#smokingyear").val();
			
			var sum2=(parseFloat(smokingyear)-parseFloat($("#smokingstarting").val()))*sum;
			var packperyear=(sum2.toFixed(2)).replace('.',',');
			$("#averagepackperyear").val(packperyear);
		}
		else
			$("#averagepackperyear").val(0);
}	
$("#decadefirst").change(function() { averagepackperday(); });
$("#decadesecond").change(function() { averagepackperday(); });
$("#decadethird").change(function() { averagepackperday(); });
$("#decadeforth").change(function() { averagepackperday(); });
$("#decadefifth").change(function() { averagepackperday(); });
$("#decadesixth").change(function() { averagepackperday(); });
$("#decadeseventh").change(function() { averagepackperday(); });
$("#decadeeight").change(function() { averagepackperday(); });

$("#tobacco").change(function() 
	{
		var tobacco_val = $(this).val();
		if(tobacco_val==2)
			$("#divtobacco").show();
		else
			$("#divtobacco").hide();	
	});
	
$(window).on('load', function() {
	if($("#start_date").val()!="")
	{
		var fu_date = ($("#start_date").val()).split("-");
		var inclusion_date = ($("#inclusion_start_date").val()).split("-");
		var cohort_date = ($("#cohort_start_date").val()).split("-");
					
		d1=new Date(inclusion_date[2], inclusion_date[1], inclusion_date[0]);
		d2=new Date(fu_date[2], fu_date[1], fu_date[0]);
					
		var months_inclusion;
		months_inclusion = (d2.getFullYear() - d1.getFullYear()) * 12;
		months_inclusion -= d1.getMonth() + 1;
		months_inclusion += d2.getMonth();
					
		d3=new Date(cohort_date[2], cohort_date[1], cohort_date[0]);
					
		var months_cohort;
		months_cohort = (d2.getFullYear() - d3.getFullYear()) * 12;
		months_cohort -= d3.getMonth() + 1;
		months_cohort += d2.getMonth();
					
		cohortval0=(Math.floor(months_cohort/3))*3-3;
		cohortval1=(Math.floor(months_cohort/3))*3;
		cohortval3=(Math.floor(months_cohort/3))*3 + 3;
		cohortval4=(Math.floor(months_cohort/3))*3 + 6;
		var remainder = months_cohort % 3;
					
		$("#fumonthcohort").empty();
		$("#fumonthinclusion").empty();
		var def_cohort=$("#fumonthcohort_str").val();
		var def_inclusion=$("#fumonthinclusion_str").val();
		
		if(remainder!=0)
		{	
			if(cohortval0>=0)
			{
				if(def_cohort==cohortval0)
					var val1 = new Option(cohortval0,cohortval0,true,true);
				else
					var val1 = new Option(cohortval0,cohortval0);
				
				$("#fumonthcohort").append(val1);
			}
			
			if(cohortval1>=0)
			{
				if(def_cohort==cohortval1)
					var val1 = new Option(cohortval1,cohortval1,true,true);
				else
					var val1 = new Option(cohortval1,cohortval1);
				
				$("#fumonthcohort").append(val1);
			}
		}
		
		//Για να μην έχουμε κανένα με μηδενικά
		if(months_cohort>=0)
		{
			if(def_cohort==months_cohort )
				var val2 = new Option(months_cohort,months_cohort,true,true);
			else
				var val2 = new Option(months_cohort,months_cohort)
			$("#fumonthcohort").append(val2);
		}
		
		if(def_cohort==cohortval3)
			var val3 = new Option(cohortval3,cohortval3,true,true);
		else
			var val3 = new Option(cohortval3,cohortval3);
		$("#fumonthcohort").append(val3);
		
		if(def_cohort==cohortval4)
			var val4 = new Option(cohortval4,cohortval4,true,true);
		else
			var val4 = new Option(cohortval4,cohortval4);
		$("#fumonthcohort").append(val4);
					
		inclusionval0=(Math.floor(months_inclusion/3))*3-3;
		inclusionval1=(Math.floor(months_inclusion/3))*3;
		inclusionval3=(Math.floor(months_inclusion/3))*3+3;
		inclusionval4=(Math.floor(months_inclusion/3))*3+6;
		var remainder = months_inclusion % 3;
						
				
		if(remainder!=0)
		{
			if(inclusionval0>=0)
			{
				if(def_inclusion==inclusionval0)
					var val1 = new Option(inclusionval0,inclusionval0,true,true);
				else
					var val1 = new Option(inclusionval0,inclusionval0);
				$("#fumonthinclusion").append(val1);
			}
			
			if(inclusionval1>=0)
			{
				if(def_inclusion==inclusionval1)
					var val1 = new Option(inclusionval1,inclusionval1,true,true);
				else
					var val1 = new Option(inclusionval1,inclusionval1);
				$("#fumonthinclusion").append(val1);
			}
		}
		
		//Για να μην έχουμε κανένα με μηδενικά
		if(months_inclusion>=0)
		{
			if(def_inclusion==months_inclusion)
				var val2 = new Option(months_inclusion,months_inclusion,true,true);
			else
				var val2 = new Option(months_inclusion,months_inclusion);
			
			$("#fumonthinclusion").append(val2);
		}
		
		if(def_inclusion==inclusionval3)
			var val3 = new Option(inclusionval3,inclusionval3,true,true);
		else
			var val3 = new Option(inclusionval3,inclusionval3);
		
		$("#fumonthinclusion").append(val3);
		
		if(def_inclusion==inclusionval4)
			var val4 = new Option(inclusionval4,inclusionval4,true,true);
		else
			var val4 = new Option(inclusionval4,inclusionval4);
		
		$("#fumonthinclusion").append(val4);
	}
})




$("#spa").change(function() 
	{
		var spa_val = $(this).val();
		if(spa_val==2)
		{
			if($("#patient_followup_id").val()=="" || ( $("#patient_spa_id").val()=="" && $("#patient_followup_id").val()!="") )
			{
				$("#basfi1").val("-1");
				$("#basfi2").val("-1");
				$("#basfi3").val("-1");
				$("#basfi4").val("-1");
				$("#basfi5").val("-1");
				$("#basfi6").val("-1");
				$("#basfi7").val("-1");
				$("#basfi8").val("-1");
				$("#basfi9").val("-1");
				$("#basfi10").val("-1");
				$("#basdai1").val("-1");
				$("#basdai2").val("-1");
				$("#basdai3").val("-1");
				$("#basdai4").val("-1");
				$("#basdai5").val("-1");
				$("#basdai6").val("-1");		
				$("#sparest1").val("-1");
				$("#sparest2").val("-1");
				$("#sparest3").val("-1");
				$("#sparest4").val("-1");
				$("#sparest5").val("-1");
				$("#sparest6").val("-1");
				$("#sparest7").val("-1");
				$("#sparest8").val("-1");
				$("#sparest9").val("-1");
				$("#sparest10").val("-1");
			}
			
			basdai_calc();
			basfi_calc();
			$("#divspa").show();
			 
		}
		else
		{
			$("#divspa").hide();	
			$("#basfi1").val("-1");
			$("#basfi2").val("-1");
			$("#basfi3").val("-1");
			$("#basfi4").val("-1");
			$("#basfi5").val("-1");
			$("#basfi6").val("-1");
			$("#basfi7").val("-1");
			$("#basfi8").val("-1");
			$("#basfi9").val("-1");
			$("#basfi10").val("-1");
			$("#basdai1").val("-1");
			$("#basdai2").val("-1");
			$("#basdai3").val("-1");
			$("#basdai4").val("-1");
			$("#basdai5").val("-1");
			$("#basdai6").val("-1");		
			$("#sparest1").val("-1");
			$("#sparest2").val("-1");
			$("#sparest3").val("-1");
			$("#sparest4").val("-1");
			$("#sparest5").val("-1");
			$("#sparest6").val("-1");
			$("#sparest7").val("-1");
			$("#sparest8").val("-1");
			$("#sparest9").val("-1");
			$("#sparest10").val("-1");
		}	
	});

$("#basfi1").change(function() { basfi_calc(); });	
$("#basfi2").change(function() { basfi_calc(); });	
$("#basfi3").change(function() { basfi_calc(); });	
$("#basfi4").change(function() { basfi_calc(); });	
$("#basfi5").change(function() { basfi_calc(); });	
$("#basfi6").change(function() { basfi_calc(); });	
$("#basfi7").change(function() { basfi_calc(); });
$("#basfi8").change(function() { basfi_calc(); });
$("#basfi9").change(function() { basfi_calc(); });
$("#basfi10").change(function() { basfi_calc(); });

function basfi_calc()
{
	if($("#basfi1").val()==-1 || $("#basfi2").val()==-1 || $("#basfi3").val()==-1 || $("#basfi4").val()==-1 || $("#basfi5").val()==-1 || $("#basfi6").val()==-1 || $("#basfi7").val()==-1 || $("#basfi8").val()==-1 || $("#basfi9").val()==-1 || $("#basfi10").val()==-1)
		$("#basfi").val("");
	else
		$("#basfi").val( ( ( parseFloat($("#basfi1").val()) + parseFloat($("#basfi2").val()) + parseFloat($("#basfi3").val()) + parseFloat($("#basfi4").val()) + parseFloat($("#basfi5").val()) + parseFloat($("#basfi6").val()) + parseFloat($("#basfi7").val()) + parseFloat($("#basfi8").val()) + parseFloat($("#basfi9").val()) + parseFloat($("#basfi10").val())  )/10 ).toFixed(2));
}

$("#basdai1").change(function() { basdai_calc(); });	
$("#basdai2").change(function() { basdai_calc(); });	
$("#basdai3").change(function() { basdai_calc(); });	
$("#basdai4").change(function() { basdai_calc(); });	
$("#basdai5").change(function() { basdai_calc(); });	
$("#basdai6").change(function() { basdai_calc(); });	
$("#sparest1").change(function() { basdai_calc(); });

function basdai_calc()
{
	if($("#basdai1").val()==-1 || $("#basdai2").val()==-1 || $("#basdai3").val()==-1 || $("#basdai4").val()==-1 || $("#basdai5").val()==-1 || $("#basdai6").val()==-1)
		$("#basdai").val("");
	else
		$("#basdai").val( ( ( parseFloat($("#basdai1").val())+parseFloat($("#basdai2").val())+parseFloat($("#basdai3").val())+parseFloat($("#basdai4").val()) + ( parseFloat($("#basdai5").val())+parseFloat($("#basdai6").val()) )/2 )/5 ).toFixed(2));
	
	if($("#basdai2").val()==-1 || $("#basdai3").val()==-1 || $("#basdai6").val()==-1 || $("#sparest1").val()==-1 || $("#crp").val()=="")
		$("#asdascrp").val("");
	else
		$("#asdascrp").val(( 0.12*parseFloat($("#basdai2").val()) + 0.06*parseFloat($("#basdai6").val()) + 0.11*parseFloat($("#sparest1").val()) + 0.07*parseFloat($("#basdai3").val()) + 0.58*(Math.log(parseFloat($("#crp").val())*10+1)) ).toFixed(2));
	
	if($("#basdai2").val()==-1 || $("#basdai3").val()==-1 || $("#basdai6").val()==-1 || $("#sparest1").val()==-1 || $("#esr").val()=="")
		$("#asdasesr").val("");
	else
		$("#asdasesr").val(( 0.08*parseFloat($("#basdai2").val()) + 0.07*parseFloat($("#basdai6").val()) + 0.11*parseFloat($("#sparest1").val()) + 0.09*parseFloat($("#basdai3").val()) + 0.29*(Math.sqrt(parseFloat($("#esr").val()))) ).toFixed(2));
	
}

	
$("#vaspain").change(function() { common_calc(); });
$("#vasglobal").change(function() { common_calc(); });
$("#vasfatigue").change(function() { common_calc(); });
$("#vasphysicial").change(function() { common_calc(); });
$("#swollenjc28").change(function() { common_calc(); });
$("#tenderjc28").change(function() { common_calc(); });
$("#swollenjc44").change(function() { common_calc(); });
$("#tenderjc44").change(function() { common_calc(); });
$("#swollenjc66").change(function() { common_calc(); });
$("#tenderjc68").change(function() { common_calc(); });
$("#esr").focusout(function() 
{
	/*if (($("#esr").val()).charAt(0)=="0")
		$("#esr").val(($("#esr").val()).substr(1));
		
	if(!isNaN($("#esr").val()))
	{	
		if ($("#esr").val()=="")
			$("#esr").val("0");
		common_calc(); 
		$("#asdasesr").val(( 0.08*parseFloat($("#basdai2").val()) + 0.07*parseFloat($("#basdai6").val()) + 0.11*parseFloat($("#sparest1").val()) + 0.09*parseFloat($("#basdai3").val()) + 0.29*(Math.sqrt(parseFloat($("#esr").val()))) ).toFixed(2));
	}
	else
	{
		$("#esr").val(($("#esr").val()).slice(0,-1));
		if ($("#esr").val()=="")
			$("#esr").val("0");
	}*/
	
		if($("#esr").val()=="")
			$("#esr").val("");
		else
			$("#esr").val(parseFloat($("#esr").val()));
		
		common_calc(); 
		if($("#basdai2").val()==-1 || $("#basdai3").val()==-1 || $("#basdai6").val()==-1 || $("#sparest1").val()==-1 || $("#esr").val()=="")
			$("#asdasesr").val("");
		else
			$("#asdasesr").val(( 0.08*parseFloat($("#basdai2").val()) + 0.07*parseFloat($("#basdai6").val()) + 0.11*parseFloat($("#sparest1").val()) + 0.09*parseFloat($("#basdai3").val()) + 0.29*(Math.sqrt(parseFloat($("#esr").val()))) ).toFixed(2));
	
});


var crp_last_value;

$("#crp").focusin(function() 
{
	crp_last_value=$("#crp").val();
});

$("#crp").focusout(function() 
{
	/*if (($("#crp").val()).charAt(0)=="0")
		$("#crp").val(($("#crp").val()).substr(1));
	
	if(!isNaN($("#crp").val()))
	{
		if ($("#crp").val()=="")
			$("#crp").val("0");
		common_calc(); 
		$("#asdascrp").val(( 0.12*parseFloat($("#basdai2").val()) + 0.06*parseFloat($("#basdai6").val()) + 0.11*parseFloat($("#sparest1").val()) + 0.07*parseFloat($("#basdai3").val()) + 0.58*(Math.log(parseFloat($("#crp").val())*10+1)) ).toFixed(2));
	}
	else
	{
		$("#crp").val(($("#crp").val()).slice(0,-1));
		if ($("#crp").val()=="")
			$("#crp").val("0");
	}*/
	
	
	if($("#crp").val()=="")
		$("#crp").val("");
	else
	{
		$("#crp").val(parseFloat($("#crp").val()));
	
		if ($("#crp").val()<0.2)
			$("#crp").val(crp_last_value);
	}
	common_calc(); 
	if($("#basdai2").val()==-1 || $("#basdai3").val()==-1 || $("#basdai6").val()==-1 || $("#sparest1").val()==-1 || $("#crp").val()=="")
		$("#asdascrp").val("");
	else
		$("#asdascrp").val(( 0.12*parseFloat($("#basdai2").val()) + 0.06*parseFloat($("#basdai6").val()) + 0.11*parseFloat($("#sparest1").val()) + 0.07*parseFloat($("#basdai3").val()) + 0.58*(Math.log(parseFloat($("#crp").val())*10+1)) ).toFixed(2));
	
	
});

$("#haq").focusout(function() 
{
	if($("#haq").val()=="")
		$("#haq").val("");
	else
		$("#haq").val(parseFloat($("#haq").val()));
	
	common_calc(); 
	
});

$("#mhaq").focusout(function() 
{
	if($("#mhaq").val()=="")
		$("#mhaq").val("");
	else
		$("#mhaq").val(parseFloat($("#mhaq").val()));
	
	common_calc(); 
	
});

//$("#haq").change(function() { common_calc(); });
//$("#mhaq").change(function() { common_calc(); });

$("#weight").change(function() { bmi_calc(); });
$("#height").change(function() { bmi_calc(); });

function common_calc()
{
	
	if ($("#crp").val()=="" || $("#vasglobal").val()=="-1" || $("#tenderjc28").val()=="-1" || $("#swollenjc28").val()=="-1" || $("#vasphysicial").val()=="-1")
		$("#sdai").val("");
	else
		$("#sdai").val(( parseFloat($("#swollenjc28").val()) +   parseFloat($("#tenderjc28").val()) + ( parseFloat($("#vasglobal").val())/10) + ( parseFloat($("#vasphysicial").val())/10) + parseFloat($("#crp").val()) ).toFixed(2));
	
	if ($("#vasglobal").val()=="-1" || $("#tenderjc28").val()=="-1" || $("#swollenjc28").val()=="-1" || $("#vasphysicial").val()=="-1")
		$("#cdai").val("");
	else
		$("#cdai").val(( parseFloat($("#swollenjc28").val()) +   parseFloat($("#tenderjc28").val()) + (parseFloat($("#vasglobal").val())/10) + (parseFloat($("#vasphysicial").val())/10) ).toFixed(2));
	
	
	
	
	//if ($("#crp").val()=="" && $("#tenderjc28").val()=="-1" && $("#swollenjc28").val()=="-1" && $("#vasglobal").val()=="-1")
	if ($("#crp").val()=="" || $("#tenderjc28").val()=="-1" || $("#swollenjc28").val()=="-1" || $("#vasglobal").val()=="-1")
		$("#das28crp").val("");
	else
	{
		var das28crp = 0;
		var das28crp_tender = 0;
		var das28crp_swollen = 0;
		var das28crp_crp = 0;
		var das28crp_vasglobal = 0;
		das28crp = 0.96;
	
		if ($("#tenderjc28").val()!="-1")
			das28crp_tender = 0.56*Math.sqrt(parseFloat($("#tenderjc28").val()));
		
		
		if ($("#swollenjc28").val()!="-1")
			das28crp_swollen = 0.28*Math.sqrt(parseFloat($("#swollenjc28").val()));
		
		if ($("#crp").val()!="")
			das28crp_crp = 0.36*Math.log(parseFloat($("#crp").val())*10+1);
		else if ($("#crp").val()=="")
			das28crp_crp = 0.36*Math.log(1);
		
		
		if ($("#vasglobal").val()!="-1")
			das28crp_vasglobal = 0.014*parseFloat($("#vasglobal").val());
		
		
		$("#das28crp").val( (parseFloat(das28crp) + parseFloat(das28crp_tender) + parseFloat(das28crp_swollen) + parseFloat(das28crp_crp) + parseFloat(das28crp_vasglobal)).toFixed(2));
		//$("#das28crp").val(((0.56*Math.sqrt(parseFloat($("#tenderjc28").val())) + 0.28*Math.sqrt(parseFloat($("#swollenjc28").val())) + 0.36*Math.log(parseFloat($("#crp").val()))*10+1 ) + 0.014*parseFloat($("#vasglobal").val())  ).toFixed(2));
	
	}
	
	
	
	//if ($("#crp").val()=="" && $("#tenderjc28").val()=="-1" && $("#swollenjc28").val()=="-1")
	if ($("#crp").val()=="" || $("#tenderjc28").val()=="-1" || $("#swollenjc28").val()=="-1")
		$("#das28crp3v").val("");
	else
	{
		var das28crp3v = 0;
		var das28crp3v_tender = 0;
		var das28crp3v_swollen = 0;
		var das28crp3v_crp = 0;
		
		das28crp3v = 1.15;
		
		if ($("#tenderjc28").val()!="-1")
			das28crp3v_tender = 0.56*Math.sqrt(parseFloat($("#tenderjc28").val()));
		
		if ($("#swollenjc28").val()!="-1")
			das28crp3v_swollen = 0.28*Math.sqrt(parseFloat($("#swollenjc28").val()));
		
		if ($("#crp").val()!="")
			das28crp3v_crp = 0.36*Math.log(parseFloat($("#crp").val())*10+1);
		
		else if ($("#crp").val()=="")
			das28crp3v_crp = 0.36*Math.log(1);
	
		
		$("#das28crp3v").val(((parseFloat(das28crp3v_tender) + parseFloat(das28crp3v_swollen) + parseFloat(das28crp3v_crp))*1.1 + parseFloat(das28crp3v)).toFixed(2));
	}
	
	//if ($("#esr").val()=="" && $("#tenderjc28").val()=="-1" && $("#swollenjc28").val()=="-1" && $("#vasglobal").val()=="-1")
	if ($("#esr").val()=="" || $("#tenderjc28").val()=="-1" || $("#swollenjc28").val()=="-1" || $("#vasglobal").val()=="-1")
		$("#das28esr").val("");
	else
	{
		var das28esr_tender = 0;
		var das28esr_swollen = 0;
		var das28esr_esr = 0;
		var das28esr_vasglobal = 0;
		
		if ($("#tenderjc28").val()!="-1")
			das28esr_tender=0.56*Math.sqrt(parseFloat($("#tenderjc28").val()));
		
		if ($("#swollenjc28").val()!="-1")
			das28esr_swollen=0.28*Math.sqrt(parseFloat($("#swollenjc28").val()));
		
		if ($("#esr").val()!="" && $("#esr").val()!="0" )
			das28esr_esr=0.70*Math.log(parseFloat($("#esr").val()));
		
		if ($("#vasglobal").val()!="-1")
			das28esr_vasglobal=0.014*parseFloat($("#vasglobal").val());
		
		$("#das28esr").val((das28esr_tender + das28esr_swollen + das28esr_esr + das28esr_vasglobal).toFixed(3));	
	}
	
	//if ($("#esr").val()=="" && $("#tenderjc28").val()=="-1" && $("#swollenjc28").val()=="-1")
	if ($("#esr").val()=="" || $("#tenderjc28").val()=="-1" || $("#swollenjc28").val()=="-1")
		$("#das28esr3v").val("");
	else
	{
		var das28esr3v_tender = 0;
		var das28esr3v_swollen = 0;
		var das28esr3v_esr = 0;
		
		if ($("#tenderjc28").val()!="-1")
			das28esr3v_tender=0.56*Math.sqrt(parseFloat($("#tenderjc28").val()));
		
		if ($("#swollenjc28").val()!="-1")
			das28esr3v_swollen=0.28*Math.sqrt(parseFloat($("#swollenjc28").val()));
		
		if ($("#esr").val()!="" && $("#esr").val()!="0")
			das28esr3v_esr=0.70*Math.log(parseFloat($("#esr").val()));
		
		
		$("#das28esr3v").val((((das28esr3v_tender + das28esr3v_swollen +  das28esr3v_esr) *1.08) + 0.16).toFixed(2));
	}
}


function bmi_calc()
{
	if($("#weight").val()==0 || $("#height").val()==0)
		$("#bmi").val("");
	else
		$("#bmi").val((parseFloat($("#weight").val()) /( (parseFloat($("#height").val())/100) * (parseFloat($("#height").val())/100))).toFixed(2));
}

$("#sle_severity").change(function() 
	{
		var sle_val = $(this).val();
		if(sle_val==2)
			$("#divsleseverity").show();
		else
			$("#divsleseverity").hide();	
	});
	
$("#sle").change(function() 
	{
		var sle_val = $(this).val();
		if(sle_val==2)
			$("#divsle").show();
		else
			$("#divsle").hide();	
	});
	
$("#common").change(function() 
	{
		var common_val = $(this).val();
		if(common_val==2)
		{
			$("#divcommon").show();
			if($("#patient_followup_id").val()!="")
			{
				$("#vaspain").val("-1");
				$("#vasglobal").val("-1");
				$("#vasfatigue").val("-1");
				$("#vasphysicial").val("-1");
				$("#swollenjc28").val("-1");
				$("#tenderjc28").val("-1");
				$("#swollenjc44").val("-1");
				$("#tenderjc44").val("-1");
				$("#swollenjc66").val("-1");
				$("#tenderjc68").val("-1");
				$("#esr").val("");
				$("#crp").val("");
				$("#haq").val("");
				$("#mhaq").val("");
			}
			
			 bmi_calc();
			 common_calc();
		}
		else
		{
			$("#divcommon").hide();	
			$("#vaspain").val("-1");
			$("#vasglobal").val("-1");
			$("#vasfatigue").val("-1");
			$("#vasphysicial").val("-1");
			$("#swollenjc28").val("-1");
			$("#tenderjc28").val("-1");
			$("#swollenjc44").val("-1");
			$("#tenderjc44").val("-1");
			$("#swollenjc66").val("-1");
			$("#tenderjc68").val("-1");
			$("#esr").val("");
				$("#crp").val("");
				$("#haq").val("");
				$("#mhaq").val("");
		}
	});
	
$("#sf36").change(function() 
	{
		var sf36_val = $(this).val();
		if(sf36_val==2)
			$("#divsf36").show();
		else
			$("#divsf36").hide();	
	});
	
$("#hads").change(function() 
	{
		var hads_val = $(this).val();
		if(hads_val==2)
			$("#divhads").show();
		else
			$("#divhads").hide();	
	});
	
$("#wpai").change(function() 
	{
		var wpai_val = $(this).val();
		if(wpai_val==2)
			$("#divwpai").show();
		else
			$("#divwpai").hide();	
	});
	
$("#eq1").change(function() { euroqol_calc(); });
$("#eq2").change(function() { euroqol_calc(); });
$("#eq3").change(function() { euroqol_calc(); });
$("#eq4").change(function() { euroqol_calc(); });
$("#eq5").change(function() { euroqol_calc(); });
$("#eq6").change(function() { euroqol_calc(); });

function euroqol_calc()
{
		$("#euroqol_score").val($("#eq1").val()+$("#eq2").val()+$("#eq3").val()+$("#eq4").val()+$("#eq5").val());
		//$("#asqol").val(parseInt($("#eq1").val())+parseInt($("#eq2").val())+parseInt($("#eq3").val())+parseInt($("#eq4").val())+parseInt($("#eq5").val())+parseInt($("#eq6").val()));
		eq5d();
}
	
$("#euroqol").change(function() 
	{
		var euroqol_val = $(this).val();
		if(euroqol_val==2)
		{
			euroqol_calc();
			$("#diveuroqol").show();
		}
		else
			 $("#diveuroqol").hide();
		
	});
	
$("#axial").change(function() 
	{
		var axial_val = $(this).val();
		if(axial_val==1)
			$("#divaxial_details").show();
		else
			 $("#divaxial_details").hide();
		
	});
	
$("#rheumatoid_arthritis").change(function() 
	{
		var rheumatoid_arthritis_val = $(this).val();
		if(rheumatoid_arthritis_val==1)
			$("#divrheumatoid_arthritis_details").show();
		else
			 $("#divrheumatoid_arthritis_details").hide();
		
	});
	
function patient_analgesics_addrow()
{
  var tbl = document.getElementById('patient_analgesics_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'patient_analgesics_tr_'+iteration;
  document.getElementById('patient_analgesics_numofrows').value = iteration;
	
	var analgesics_ids=document.getElementById('analgesics_ids').value;
	var analgesics_values=document.getElementById('analgesics_values').value;
	var analgesics_counter=document.getElementById('analgesics_counter').value;
	var analgesics_value=analgesics_values.split('!@#$%^');
	var analgesics_id=analgesics_ids.split('!@#$%^');
	
	var intake_ids=document.getElementById('intake_ids').value;
	var intake_values=document.getElementById('intake_values').value;
	var intake_counter=document.getElementById('intake_counter').value;
	var intake_value=intake_values.split('!@#$%^');
	var intake_id=intake_ids.split('!@#$%^');
	
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('select');
  el1.name = 'analgesics_id_' + iteration;
  el1.id = 'analgesics_id_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
   
	var opt=new Option("--",0);
	el1.add(opt,undefined);
	for(i = 0; i < analgesics_counter; i++)
	{
		var opt=new Option(analgesics_value[i],analgesics_id[i]);
		el1.add(opt,undefined);
	}
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'patient_analgesics_hidden_' + iteration;
  el2.id = 'patient_analgesics_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'patient_analgesics_id_' + iteration;
  el3.id = 'patient_analgesics_id_' + iteration;
  el3.value='';
  
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
  var r11 = row.insertCell(1);
  r11.align='center';
  
   var el11 = document.createElement('select');
  el11.name = 'intake_' + iteration;
  el11.id = 'intake_' + iteration;
  el11.style.width='100%';
  el11.className='form-control';
   
	var opt=new Option("--",0);
	el11.add(opt,undefined);
	for(i = 0; i < intake_counter; i++)
	{
		var opt=new Option(intake_value[i],intake_id[i]);
		el11.add(opt,undefined);
	}

	r11.appendChild(el11);
	
  var r2 = row.insertCell(2);
  r2.align='center';
  
	var el6 = document.createElement('input');
	  el6.type = 'text';
	  el6.name = 'startdate_analgesics_' + iteration;
	  el6.id = 'startdate_analgesics_' + iteration;
	  el6.style.width='100%';
	  el6.className='form-control';
	
	r2.appendChild(el6);
  
  var r22 = row.insertCell(3);
  r22.align='center';
  
	var el66 = document.createElement('input');
	  el66.type = 'text';
	  el66.name = 'stopdate_analgesics_' + iteration;
	  el66.id = 'stopdate_analgesics_' + iteration;
	  el66.style.width='100%';
	  el66.className='form-control';
	
	r22.appendChild(el66);
	
	$('#startdate_analgesics_'+iteration).datepicker({
		 dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  $('#startdate_analgesics_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
		
		$('#stopdate_analgesics_'+iteration).datepicker({
		   dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  $('#stopdate_analgesics_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
  
	var r6 = row.insertCell(4);
	r6.align='center';
	
	var el8 = document.createElement('a');
	el8.name = 'a_analgesics_' + iteration;
	el8.id = 'a_analgesics_' + iteration;
	el8.title = 'Delete';
	el8.href="javascript:avoid(0)";
	var icon = document.createElement('i');
	icon.className='fa fa-trash-o';
	el8.appendChild(icon);
	el8.onclick =function(){patient_analgesics_removerow(iteration);};
	
	r6.appendChild(el8);
}

function patient_analgesics_removerow(row)
{
	document.getElementById('patient_analgesics_hidden_'+row).value='-1';
	document.getElementById('patient_analgesics_tr_'+row).style.display='none';
}


function patient_corticosteroids_addrow()
{
  var tbl = document.getElementById('patient_corticosteroids_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'patient_corticosteroids_tr_'+iteration;
  document.getElementById('patient_corticosteroids_numofrows').value = iteration;
	
	var corticosteroids_ids=document.getElementById('corticosteroids_ids').value;
	var corticosteroids_values=document.getElementById('corticosteroids_values').value;
	var corticosteroids_counter=document.getElementById('corticosteroids_counter').value;
	var corticosteroids_value=corticosteroids_values.split('!@#$%^');
	var corticosteroids_id=corticosteroids_ids.split('!@#$%^');
	
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('select');
  el1.name = 'corticosteroids_id_' + iteration;
  el1.id = 'corticosteroids_id_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
   
	var opt=new Option("--",0);
	el1.add(opt,undefined);
	for(i = 0; i < corticosteroids_counter; i++)
	{
		var opt=new Option(corticosteroids_value[i],corticosteroids_id[i]);
		el1.add(opt,undefined);
	}
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'patient_corticosteroids_hidden_' + iteration;
  el2.id = 'patient_corticosteroids_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'patient_corticosteroids_id_' + iteration;
  el3.id = 'patient_corticosteroids_id_' + iteration;
  el3.value='';
  
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
  var r11 = row.insertCell(1);
  r11.align='center';
  
  
	var el6 = document.createElement('input');
	  el6.type = 'text';
	  el6.name = 'corticosteroids_date_' + iteration;
	  el6.id = 'corticosteroids_date_' + iteration;
	  el6.style.width='100%';
	  el6.className='form-control';
	
	r11.appendChild(el6);
  
  $('#corticosteroids_date_'+iteration).datepicker({
		 dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  $('#corticosteroids_date_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
		
	var r6 = row.insertCell(2);
	r6.align='center';
	
	var el8 = document.createElement('a');
	el8.name = 'a_corticosteroids_' + iteration;
	el8.id = 'a_corticosteroids_' + iteration;
	el8.title = 'Delete';
	el8.href="javascript:avoid(0)";
	var icon = document.createElement('i');
	icon.className='fa fa-trash-o';
	el8.appendChild(icon);
	el8.onclick =function(){patient_corticosteroids_removerow(iteration);};
	
	r6.appendChild(el8);
}

function patient_corticosteroids_removerow(row)
{
	document.getElementById('patient_corticosteroids_hidden_'+row).value='-1';
	document.getElementById('patient_corticosteroids_tr_'+row).style.display='none';
}

function setminmax(row)
{
	var drugs_ids=document.getElementById('drugs_ids').value;
	var drugs_id=drugs_ids.split('!@#$%^');
	var drugs_maxs=document.getElementById('drugs_maxs').value;
	var drugs_max=drugs_maxs.split('!@#$%^');
	var drugs_mins=document.getElementById('drugs_mins').value;
	var drugs_min=drugs_mins.split('!@#$%^');
	var drugs_counter=document.getElementById('drugs_counter').value;
	
	var drugs_id_selected=$("#drugs_id_"+row).val();
	
	for(i = 0; i < drugs_counter; i++)
	{
		if(drugs_id_selected==drugs_id[i])
		{
			$("#patient_cohort_drugs_min_"+row).val(drugs_min[i]);
			$("#patient_cohort_drugs_max_"+row).val(drugs_max[i]);
		}
	}
}

function calc(row)
{
	if (document.getElementById('main_'+row).checked===true) 
	{
		$("input[name^='main_']").prop('checked', false);
		$("a[name^='a_']").show();
		$("#main_"+row).prop('checked', true);
		$("#a_"+row).hide();
	  
	}
	else
	{
		$("input[name^='main_']").prop('checked', false);
		$("a[name^='a_']").show();
		$("#main_"+row).prop('checked', false);
		$("#a_"+row).show();
	}  
}

$(function () {
	  
	  $('#form').submit(function(){
		
		if($("#fumonthinclusion").val()!=0)
			var fuincl = $("#fumonthinclusion").val() % 3;
		else
			var fuincl=0;
		
		if($("#fumonthcohort").val()!=0)
			var fucoh = $("#fumonthcohort").val() % 3;
		else
			var fucoh=0;
		
		var msg2="";
		if(fuincl!=0 || fucoh!=0)
			msg2 += "The month (inclusion or cohort) you selected for the Follow up will not be included in the data export.You should choose a quarter(Cancel) or not (OK) !"
		
		if (msg2!="")
			var conf =confirm(msg2);
		else
			var conf=true;
		
		if(conf==true)
		{
			var msg="";
			var cohort_name=$('#followup_name').val();
			var treatment_nbr=$('#treatment_nbr').val();
			var start_date2=$('#start_date').val();
			
			var start_date_unformat=$('#cohort_start_date').val();
			var start_date_split = start_date_unformat.split("-");
			var start_date= new Date(start_date_split[2]+"-"+start_date_split[1]+"-"+start_date_split[0]);
			
			var tbl = document.getElementById('patient_drugs_tbl');
			var lastRow = tbl.rows.length;
			
			var k=0;
			var checkstopdate=0;
			var dose_min=0;
			var dose_max=0;
			var min=0;
			var max=0;
			
			var sum_main=0;
			var sum_main_drugs=0;
			
			for (i=1;i<=lastRow-1;i++)
			{
				if ($('#patient_cohort_drugs_type_'+i).val()=='cohort')
				{
					sum_main_drugs++;
					if($('#main_'+i).prop('checked'))
						sum_main++;
				}
			
				if (document.getElementById('patient_cohort_drugs_type_'+i).value=='cohort')
				{
					var cohort_startdate_unformat=document.getElementById('startdate_'+i).value;
					var cohort_startdate_split = cohort_startdate_unformat.split("-");
					var cohort_startdate= new Date(cohort_startdate_split[2]+"-"+cohort_startdate_split[1]+"-"+cohort_startdate_split[0]);
					
					var cohort_stopdate_unformat=document.getElementById('stopdate_'+i).value;
					var cohort_stopdate_split = cohort_stopdate_unformat.split("-");
					var cohort_stopdate= new Date(cohort_stopdate_split[2]+"-"+cohort_stopdate_split[1]+"-"+cohort_stopdate_split[0]);
					
					if (cohort_stopdate_unformat=="")
						checkstopdate++;
					
					min=$("#patient_cohort_drugs_min_"+i).val();
					max=$("#patient_cohort_drugs_max_"+i).val();
					
					/*if(cohort_startdate<start_date)
					{
						document.getElementById('startdate_'+i).style.backgroundColor = "red";
						document.getElementById('startdate_'+i).style.color  = "white";
						k++;
					}
					else
					{
						document.getElementById('startdate_'+i).style.backgroundColor = "white";
						document.getElementById('startdate_'+i).style.color  = "black";
					}*/
				}
				if ($('#patient_cohort_drugs_type_'+i).val()=='period')
				{
					if(parseInt($('#dose_'+i).val())>max)
					{
						dose_max++;
						document.getElementById('dose_'+i).style.backgroundColor = "red";
						document.getElementById('dose_'+i).style.color  = "white";
					}
					else if(parseInt($('#dose_'+i).val())<min)
					{
						dose_min++;
						document.getElementById('dose_'+i).style.backgroundColor = "red";
						document.getElementById('dose_'+i).style.color  = "white";
					}
					else
					{
						document.getElementById('dose_'+i).style.backgroundColor = "white";
						document.getElementById('dose_'+i).style.color  = "black";
					}
				}			
			}
			
			//if(k>0)
			//	msg +='-Treatments start date must be greater than cohort start date ('+start_date_unformat+')!\n'; 
			
			if(dose_max>0)
				msg +="-There is a dose greater than maximum dose!\n";
			if(dose_min>0)
				msg +="-There is a dose smaller than minimum dose!\n";
			
			 //if($('#description').val()=='')
				//msg +='-Πρέπει να εισάγετε Περιγραφή!\n'; 
			if(start_date2=="")
			{
				document.getElementById('start_date').style.backgroundColor = "red";
				document.getElementById('start_date').style.color  = "white";
				msg +="-Fill followup start date!\n";
			}
			
			else
			{
				document.getElementById('start_date').style.backgroundColor = "white";
				document.getElementById('start_date').style.color  = "black";
			}
				
			if(sum_main==0 && sum_main_drugs>0)
				msg +='-You must have at least one main Treatment drug!\n';
		
			if(msg != '')
			{
				alert(msg);
				return false;
			}
			else
				return true;
		}
		else
			return false;
		
		}); 
	  });	

function patient_cohort_drugs_addrow()
{
  var tbl = document.getElementById('patient_drugs_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'patient_cohort_drugs_tr_'+iteration;
   $('#patient_cohort_drugs_tr_'+iteration).css('background', '#f0f8ff');
  document.getElementById('patient_cohort_drugs_numofrows').value = iteration;
	
	var drugs_ids=document.getElementById('drugs_ids').value;
	var drugs_values=document.getElementById('drugs_values').value;
	var drugs_counter=document.getElementById('drugs_counter').value;
	var drugs_value=drugs_values.split('!@#$%^');
	var drugs_id=drugs_ids.split('!@#$%^');
	
	/*var route_ids=document.getElementById('route_ids').value;
	var route_values=document.getElementById('route_values').value;
	var route_counter=document.getElementById('route_counter').value;
	var route_value=route_values.split('!@#$%^');
	var route_id=route_ids.split('!@#$%^');*/
	
  var r1 = row.insertCell(0);
  r1.align='center';
  r1.colSpan="3";
  
  var el1 = document.createElement('select');
  el1.name = 'drugs_id_' + iteration;
  el1.id = 'drugs_id_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
   el1.onchange =function(){setminmax(iteration)};
   
	var opt=new Option("--",0);
	el1.add(opt,undefined);
	for(i = 0; i < drugs_counter; i++)
	{
		var opt=new Option(drugs_value[i],drugs_id[i]);
		el1.add(opt,undefined);
	}
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'patient_cohort_drugs_hidden_' + iteration;
  el2.id = 'patient_cohort_drugs_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'patient_cohort_drugs_id_' + iteration;
  el3.id = 'patient_cohort_drugs_id_' + iteration;
  el3.value='';
  
  var el33 = document.createElement('input');
  el33.type = 'hidden';
  el33.name = 'patient_cohort_drugs_type_' + iteration;
  el33.id = 'patient_cohort_drugs_type_' + iteration;
  el33.value='cohort';
  
  var el34 = document.createElement('input');
  el34.type = 'hidden';
  el34.name = 'patient_cohort_drugs_row_' + iteration;
  el34.id = 'patient_cohort_drugs_row_' + iteration;
  el34.value=iteration;
  
  var el35 = document.createElement('input');
  el35.type = 'hidden';
  el35.name = 'patient_cohort_drugs_min_' + iteration;
  el35.id = 'patient_cohort_drugs_min_' + iteration;
  el35.value=0;
  
  var el36 = document.createElement('input');
  el36.type = 'hidden';
  el36.name = 'patient_cohort_drugs_max_' + iteration;
  el36.id = 'patient_cohort_drugs_max_' + iteration;
  el36.value=0;
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  r1.appendChild(el33);
  r1.appendChild(el34);
  r1.appendChild(el35);
  r1.appendChild(el36);
  
  var r11 = row.insertCell(1);
  r11.align='center';
  
  var el333 = document.createElement('input');
	el333.type = 'checkbox';
	el333.name = 'main_' + iteration;
	el333.id = 'main_' + iteration;
	el333.onclick =function(){calc(iteration)};
	r11.appendChild(el333);

	/*var r12 = row.insertCell(2);
	r12.align='center';
  
  var el11 = document.createElement('select');
  el11.name = 'route_of_administration_' + iteration;
  el11.id = 'route_of_administration_' + iteration;
  el11.style.width='100%';
  el11.className='form-control';
  
	var opt=new Option("--",0);
	el11.add(opt,undefined);
	for(i = 0; i < route_counter; i++)
	{
		var opt=new Option(route_value[i],route_id[i]);
		el11.add(opt,undefined);
	}
	
	
	  
	r12.appendChild(el11);
  */
  
	var r4 = row.insertCell(2);
	r4.align='center';
  
		var el6 = document.createElement('input');
	  el6.type = 'text';
	  el6.name = 'startdate_' + iteration;
	  el6.id = 'startdate_' + iteration;
	  el6.style.width='100%';
	  el6.className='form-control';
	
	r4.appendChild(el6);
  
		var r5 = row.insertCell(3);
		r5.align='center';
  
		var el7 = document.createElement('input');
	  el7.type = 'text';
	  el7.name = 'stopdate_' + iteration;
	  el7.id = 'stopdate_' + iteration;
	  el7.style.width='100%';
	  el7.className='form-control';
  
	r5.appendChild(el7);
  
 $('#startdate_'+iteration).datepicker({
		  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  $('#startdate_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
		
		$('#stopdate_'+iteration).datepicker({
		  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  $('#stopdate_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
		
	var r6 = row.insertCell(4);
	r6.align='center';
	
	/*var el9 = document.createElement('a');
	el9.name = 'a_add_' + iteration;
	el9.id = 'a_add_' + iteration;
	el9.title = 'add period';
	el9.href="javascript:avoid(0)";
	var icon = document.createElement('i');
	icon.className='fa fa-calendar-plus-o';
	el9.appendChild(icon);
	el9.onclick =function(){patient_cohort_drugs_newperiod(iteration,'')};*/
	
	var el8 = document.createElement('a');
	el8.name = 'a_' + iteration;
	el8.id = 'a_' + iteration;
	el8.title = 'Delete';
	el8.href="javascript:avoid(0)";
	var icon = document.createElement('i');
	icon.className='fa fa-trash-o';
	el8.appendChild(icon);
	el8.onclick =function(){if (confirm("Are you sure;All periods will be deleted!")) { patient_cohort_drugs_removerow(iteration); }};
	
	/*var span = document.createElement("span");
	span.innerHTML = "&nbsp;&nbsp;&nbsp;&nbsp;";*/
	
	/*r6.appendChild(el9);
	r6.appendChild(span);*/
	r6.appendChild(el8);
	
	var iteration2 = lastRow+1;
	var row = tbl.insertRow(lastRow+1);
	row.id= 'patient_cohort_drugs_tr_'+iteration2;
	document.getElementById('patient_cohort_drugs_numofrows').value = iteration2;
	
	var r111 = row.insertCell(0);
	r111.align='center';
	var span1 = document.createElement("span");
	span1.innerHTML = "Periods";
	r111.appendChild(span1);
	
	var r11 = row.insertCell(1);
	r11.align='center';
	
	
	var el11 = document.createElement('input');
	el11.type = 'hidden';
	el11.name = 'patient_cohort_drugs_hidden_' + iteration2;
	el11.id = 'patient_cohort_drugs_hidden_' + iteration2;
	el11.value='1';
	
	var el12 = document.createElement('input');
	el12.type = 'hidden';
	el12.name = 'patient_cohort_drugs_type_' + iteration2;
	el12.id = 'patient_cohort_drugs_type_' + iteration2;
	el12.value='period_label';
  
	var el13 = document.createElement('input');
	el13.type = 'hidden';
	el13.name = 'patient_cohort_drugs_row_' + iteration2;
	el13.id = 'patient_cohort_drugs_row_' + iteration2;
	el13.value=iteration;
	
	var el333 = document.createElement('input');
	el333.type = 'hidden';
	el333.name = 'patient_cohort_drugs_id_' + iteration2;
	el333.id = 'patient_cohort_drugs_id_' + iteration2;
	el333.value='';
  
	var span = document.createElement("span");
	span.innerHTML = "Reason for </br> treatment change";
	
	r11.appendChild(el11);
	r11.appendChild(el12);
	r11.appendChild(el13);
	r11.appendChild(el333);
	r11.appendChild(span);
	
	var r12 = row.insertCell(2);
	r12.align='center';
	
	
	var span2 = document.createElement("span");
	span2.innerHTML = "frequency";
	
	
	r12.appendChild(span2);
	
	var r13 = row.insertCell(3);
	r13.align='center';
	
	var span3 = document.createElement("span");
	span3.innerHTML = "dose";
	
	r13.appendChild(span3);
	
	var r14 = row.insertCell(4);
	r14.align='center';
	
	var span4 = document.createElement("span");
	span4.innerHTML = "start date";
	
	r14.appendChild(span4);
	
	var r15 = row.insertCell(5);
	r15.align='center';
	
	var span5 = document.createElement("span");
	span5.innerHTML = "stop date";
	
	r15.appendChild(span5);
	
	var r16 = row.insertCell(6);
	r16.align='center';
	
	var el9 = document.createElement('a');
	el9.name = 'a2_add_' + iteration2;
	el9.id = 'a2_add_' + iteration2;
	el9.title = 'add period';
	el9.href="javascript:avoid(0)";
	var icon = document.createElement('i');
	icon.className='fa fa-calendar-plus-o';
	el9.appendChild(icon);
	el9.onclick =function(){patient_cohort_drugs_newperiod(iteration2,'')};
	
	r16.appendChild(el9);
	
	
}

function patient_cohort_drugs_newperiod(w_row,patient_cohort_drugs_id)
{
  var tbl = document.getElementById('patient_drugs_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var newrow2=parseInt(w_row)+1;
  document.getElementById('patient_cohort_drugs_numofrows').value = iteration;
  /*alert("iteration:"+iteration);
  alert("lastRow:"+lastRow);
  alert("newrow2:"+newrow2);
  alert("w_row:"+w_row);*/
   var cohort_new_row=0;
  
  for (i=lastRow-1;i>=newrow2;i--)
  {
	  var newrow3=parseInt(i)+1;
	 
		if (document.getElementById('patient_cohort_drugs_type_'+i).value=='cohort')
		{
			cohort_new_row=newrow3;
			var el51=document.getElementById('patient_cohort_drugs_tr_'+i);
			el51.id= 'patient_cohort_drugs_tr_'+newrow3;
			el51.name= 'patient_cohort_drugs_tr_'+newrow3;
			
			var el52=document.getElementById('drugs_id_'+i);
			el52.id = 'drugs_id_' + newrow3;
			el52.name = 'drugs_id_' + newrow3;
			el52.onchange =function(){setminmax(this.id.substr(9))};
			
			var el53=document.getElementById('patient_cohort_drugs_hidden_'+i);
			el53.id = 'patient_cohort_drugs_hidden_' + newrow3;
			el53.name = 'patient_cohort_drugs_hidden_' + newrow3;
			
			var el54=document.getElementById('patient_cohort_drugs_id_'+i);
			el54.id = 'patient_cohort_drugs_id_' + newrow3;
			el54.name = 'patient_cohort_drugs_id_' + newrow3;
			
			var el55=document.getElementById('patient_cohort_drugs_type_'+i);
			el55.id = 'patient_cohort_drugs_type_' + newrow3;
			el55.name = 'patient_cohort_drugs_type_' + newrow3;
			
			/*var el56=document.getElementById('route_of_administration_'+i);
			el56.id = 'route_of_administration_' + newrow3;
			el56.name = 'route_of_administration_' + newrow3;*/
			
			var el510=document.getElementById('patient_cohort_drugs_row_'+i);
			el510.id = 'patient_cohort_drugs_row_' + newrow3;
			el510.name = 'patient_cohort_drugs_row_' + newrow3;
			
			var el511=document.getElementById('patient_cohort_drugs_min_'+i);
			el511.id = 'patient_cohort_drugs_min_' + newrow3;
			el511.name = 'patient_cohort_drugs_min_' + newrow3;
			
			var el512=document.getElementById('patient_cohort_drugs_max_'+i);
			el512.id = 'patient_cohort_drugs_max_' + newrow3;
			el512.name = 'patient_cohort_drugs_max_' + newrow3;
			
			var el57=document.getElementById('main_'+i);
			el57.name = 'main_' + newrow3;
			el57.id = 'main_' + newrow3;
			el57.onclick =function(){calc(this.id.substr(5))};
			
			var el58=document.getElementById('startdate_'+i);
			el58.id = 'startdate_' + newrow3;
			el58.name = 'startdate_' + newrow3;
			
			var el59=document.getElementById('stopdate_'+i);
			el59.id = 'stopdate_' + newrow3;
			el59.name = 'stopdate_' + newrow3;
			
			$('#startdate_'+newrow3).datepicker('destroy'); //detach
			$('#startdate_'+newrow3).datepicker({
			  dateFormat: 'dd-mm-yy',
			  showButtonPanel: true,
			  yearRange: "-100:+100",
			default: "0:0",
			changeMonth: true,
			changeYear: true,
			  onClose2:function(dateText) {}
			})
			$('#startdate_'+newrow3).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
			
			$('#stopdate_'+newrow3).datepicker('destroy'); //detach
			$('#stopdate_'+newrow3).datepicker({
			  dateFormat: 'dd-mm-yy',
			  showButtonPanel: true,
			  yearRange: "-100:+100",
				default: "0:0",
				changeMonth: true,
				changeYear: true,
			  onClose2:function(dateText) {}
			})
			$('#stopdate_'+newrow3).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
			
			/*var el510=document.getElementById('a_add_'+i);
			el510.id = 'a_add_' + newrow3;
			el510.name = 'a_add_' + newrow3;
			el510.onclick =function() {patient_cohort_drugs_newperiod(this.id.substr(6),el54.id);};*/

			if(el57.value!=1)
			{	
				var el511=document.getElementById('a_'+i);
				el511.id = 'a_' + newrow3;
				el511.name = 'a_' + newrow3;
				el511.onclick =function() {if (confirm("Are you sure;All periods will be deleted!")) { patient_cohort_drugs_removerow(this.id.substr(2));}};
			}
		}
		else if (document.getElementById('patient_cohort_drugs_type_'+i).value=='period')
		{
			var el512=document.getElementById('patient_cohort_drugs_tr_'+i);
			el512.id= 'patient_cohort_drugs_tr_'+newrow3;
			el512.name= 'patient_cohort_drugs_tr_'+newrow3;
			
			var el513=document.getElementById('patient_cohort_drugs_period_id_'+i);
			el513.id = 'patient_cohort_drugs_period_id_' + newrow3;
			el513.name = 'patient_cohort_drugs_period_id_' + newrow3;
			
			var el514=document.getElementById('patient_cohort_drugs_hidden_'+i);
			el514.id = 'patient_cohort_drugs_hidden_' + newrow3;
			el514.name = 'patient_cohort_drugs_hidden_' + newrow3;
			
			var el515=document.getElementById('patient_cohort_drugs_id_'+i);
			el515.id = 'patient_cohort_drugs_id_' + newrow3;
			el515.name = 'patient_cohort_drugs_id_' + newrow3;
			
			var el516=document.getElementById('patient_cohort_drugs_type_'+i);
			el516.id = 'patient_cohort_drugs_type_' + newrow3;
			el516.name = 'patient_cohort_drugs_type_' + newrow3;
			
			var el517=document.getElementById('patient_cohort_drugs_row_'+i);
			el517.id = 'patient_cohort_drugs_row_' + newrow3;
			el517.name = 'patient_cohort_drugs_row_' + newrow3;
			//el517.value=cohort_new_row;
			
			var el523=document.getElementById('reason_'+i);
			el523.name = 'reason_' + newrow3;
			el523.id = 'reason_' + newrow3;
	
			var el518=document.getElementById('dose_'+i);
			el518.id = 'dose_' + newrow3;
			el518.name = 'dose_' + newrow3;
			
			var el519=document.getElementById('frequency_'+i);
			el519.id = 'frequency_' + newrow3;
			el519.name = 'frequency_' + newrow3;
			
			var el520=document.getElementById('startdate_'+i);
			el520.id = 'startdate_' + newrow3;
			el520.name = 'startdate_' + newrow3;
			
			var el521=document.getElementById('stopdate_'+i);
			el521.id = 'stopdate_' + newrow3;
			el521.name = 'stopdate_' + newrow3;
			
			$('#startdate_'+newrow3).datepicker('destroy'); //detach
			$('#startdate_'+newrow3).datepicker({
			  autoclose: true,
			  dateFormat: 'dd-mm-yy',
			  yearRange: "-100:+100",
			 default: "0:0",
			 changeMonth: true,
			changeYear: true
			})
			
			$('#startdate_'+newrow3).datepicker('destroy'); //detach
			$('#startdate_'+newrow3).datepicker({
			 dateFormat: 'dd-mm-yy',
			  showButtonPanel: true,
			  yearRange: "-100:+100",
				default: "0:0",
				changeMonth: true,
				changeYear: true,
			  onClose2:function(dateText) {}
			})
			$('#startdate_'+newrow3).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
			
			$('#stopdate_'+newrow3).datepicker('destroy'); //detach
			$('#stopdate_'+newrow3).datepicker({
			  dateFormat: 'dd-mm-yy',
			  showButtonPanel: true,
			  yearRange: "-100:+100",
				default: "0:0",
				changeMonth: true,
				changeYear: true,
			  onClose2:function(dateText) {}
			})
			$('#stopdate_'+newrow3).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
		}
		else if (document.getElementById('patient_cohort_drugs_type_'+i).value=='period_label')
		{
			var el512=document.getElementById('patient_cohort_drugs_tr_'+i);
			el512.id= 'patient_cohort_drugs_tr_'+newrow3;
			el512.name= 'patient_cohort_drugs_tr_'+newrow3;
			
			
			var el514=document.getElementById('patient_cohort_drugs_hidden_'+i);
			el514.id = 'patient_cohort_drugs_hidden_' + newrow3;
			el514.name = 'patient_cohort_drugs_hidden_' + newrow3;
			
			var el516=document.getElementById('patient_cohort_drugs_type_'+i);
			el516.id = 'patient_cohort_drugs_type_' + newrow3;
			el516.name = 'patient_cohort_drugs_type_' + newrow3;
			
			var el517=document.getElementById('patient_cohort_drugs_row_'+i);
			el517.id = 'patient_cohort_drugs_row_' + newrow3;
			el517.name = 'patient_cohort_drugs_row_' + newrow3;
			//el517.value=cohort_new_row;
			
			var el515=document.getElementById('patient_cohort_drugs_id_'+i);
			el515.id = 'patient_cohort_drugs_id_' + newrow3;
			el515.name = 'patient_cohort_drugs_id_' + newrow3;
			
			var el510=document.getElementById('a2_add_'+i);
			el510.id = 'a2_add_' + newrow3;
			el510.name = 'a2_add_' + newrow3;
			el510.onclick =function() {patient_cohort_drugs_newperiod(this.id.substr(7),el515.value);};
		}
		
  }
  
  var row = tbl.insertRow(newrow2);
  row.id= 'patient_cohort_drugs_tr_'+newrow2;
  
	
	var drugs_frequency_ids=document.getElementById('drugs_frequency_ids').value;
	var drugs_frequency_values=document.getElementById('drugs_frequency_values').value;
	var drugs_frequency_counter=document.getElementById('drugs_frequency_counter').value;
	var drugs_frequency_value=drugs_frequency_values.split('!@#$%^');
	var drugs_frequency_id=drugs_frequency_ids.split('!@#$%^');
	
	var drugs_reason_ids=document.getElementById('drugs_reason_ids').value;
	var drugs_reason_values=document.getElementById('drugs_reason_values').value;
	var drugs_reason_counter=document.getElementById('drugs_reason_counter').value;
	var drugs_reason_value=drugs_reason_values.split('!@#$%^');
	var drugs_reason_id=drugs_reason_ids.split('!@#$%^');
	
	var r111 = row.insertCell(0);
	r111.align='center';
	var span1 = document.createElement("span");
	span1.innerHTML = "&nbsp;";
	r111.appendChild(span1);
	
  var r1 = row.insertCell(1);
  r1.align='center';
  
  var el1 = document.createElement('input');
  el1.type = 'hidden';
  el1.name = 'patient_cohort_drugs_period_id_' + newrow2;
  el1.id = 'patient_cohort_drugs_period_id_' + newrow2;
  el1.value='';
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'patient_cohort_drugs_hidden_' + newrow2;
  el2.id = 'patient_cohort_drugs_hidden_' + newrow2;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'patient_cohort_drugs_id_' + newrow2;
  el3.id = 'patient_cohort_drugs_id_' + newrow2;
  el3.value=patient_cohort_drugs_id;
  
  var el33 = document.createElement('input');
  el33.type = 'hidden';
  el33.name = 'patient_cohort_drugs_type_' + newrow2;
  el33.id = 'patient_cohort_drugs_type_' + newrow2;
  el33.value='period';
  
  var el34 = document.createElement('input');
  el34.type = 'hidden';
  el34.name = 'patient_cohort_drugs_row_' + newrow2;
  el34.id = 'patient_cohort_drugs_row_' + newrow2;
  el34.value = $('#patient_cohort_drugs_row_' + w_row).val();
  
  var el35 = document.createElement('select');
	el35.name = 'reason_' + newrow2;
	el35.id = 'reason_' + newrow2;
	el35.className="form-control";
	el35.style.width='100%';
	
	var opt=new Option("--",0);
	el35.add(opt,undefined);
	for(i = 0; i < drugs_reason_counter; i++)
	{
		var opt=new Option(drugs_reason_value[i],drugs_reason_id[i]);
		el35.add(opt,undefined);
	}
	
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  r1.appendChild(el33);
  r1.appendChild(el34);
  r1.appendChild(el35);
  
	
  
	var r44 = row.insertCell(2);
	r44.align='center';
	
	
	var el5 = document.createElement('select');
	el5.name = 'frequency_' + newrow2;
	el5.id = 'frequency_' + newrow2;
	el5.className="form-control";
	el5.style.width='100%';
	
	var opt=new Option("--",0);
	el5.add(opt,undefined);
	for(i = 0; i < drugs_frequency_counter; i++)
	{
		var opt=new Option(drugs_frequency_value[i],drugs_frequency_id[i]);
		el5.add(opt,undefined);
	}	
	
	
	
	r44.appendChild(el5);
	
	var r11 = row.insertCell(3);
	r11.align='center';
  var el4 = document.createElement('input');
	el4.name = 'dose_' + newrow2;
	el4.id = 'dose_' + newrow2;
	el4.style.width='100%';
	el4.className='form-control';	
	
	
	  
	r11.appendChild(el4);
	
	$('#dose_'+newrow2).focusout(function() 
	{
		/*if (($(this).val()).charAt(0)=="0")
			$(this).val(($(this).val()).substr(1));
		
		if(!isNaN($(this).val()))
		{
			if ($(this).val()=="")
				$(this).val("0");
		}
		else
		{
			$(this).val(($(this).val()).slice(0,-1));
			if ($(this).val()=="")
				$(this).val("0");
		}*/
		if($(this).val()=="")
			$(this).val("0");
		else
			$(this).val(parseFloat($(this).val()));
	});
		
		
	var r4 = row.insertCell(4);
	r4.align='center';
  
	var el6 = document.createElement('input');
	  el6.type = 'text';
	  el6.name = 'startdate_' + newrow2;
	  el6.id = 'startdate_' + newrow2;
	  el6.style.width='100%';
	  el6.className='form-control';
	
	r4.appendChild(el6);
	
  var r5 = row.insertCell(5);
	r5.align='center';
	
	var el7 = document.createElement('input');
	  el7.type = 'text';
	  el7.name = 'stopdate_' + newrow2;
	  el7.id = 'stopdate_' + newrow2;
	  el7.style.width='100%';
	  el7.className='form-control';
  
	r5.appendChild(el7);
	 
	 $('#startdate_'+newrow2).datepicker({
		  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  $('#startdate_'+newrow2).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
		
		$('#stopdate_'+newrow2).datepicker({
		 dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  $('#stopdate_'+newrow2).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
		
	var r2 = row.insertCell(6);
	r2.align='center';
	
	var el8 = document.createElement('a');
	el8.name = 'a2_' + newrow2;
	el8.id = 'a2_' + newrow2;
	el8.title = 'Delete';
	el8.href="javascript:avoid(0)";
	var icon = document.createElement('i');
	icon.className='fa fa-trash-o';
	el8.appendChild(icon);
	el8.onclick =function(){patient_cohort_drugs_period_removerow(newrow2)};
  
	r2.appendChild(el8);
}

function patient_cohort_drugs_removerow(row)
{
	document.getElementById('patient_cohort_drugs_hidden_'+row).value='-1';
	document.getElementById('patient_cohort_drugs_tr_'+row).style.display='none';
	
	 var tbl = document.getElementById('patient_drugs_tbl');
	 var lastRow = tbl.rows.length;
	
	
	for (i=row;i<=lastRow-1;i++)
	{
		if (document.getElementById('patient_cohort_drugs_type_'+i).value=='period' || document.getElementById('patient_cohort_drugs_type_'+i).value=='period_label')
		{
			if (document.getElementById('patient_cohort_drugs_row_'+i).value==document.getElementById('patient_cohort_drugs_row_'+row).value)
			{
				document.getElementById('patient_cohort_drugs_hidden_'+i).value='-1';
				document.getElementById('patient_cohort_drugs_tr_'+i).style.display='none';
			}
		}
	}
	
}

function patient_cohort_drugs_period_removerow(row)
{
	document.getElementById('patient_cohort_drugs_hidden_'+row).value='-1';
	document.getElementById('patient_cohort_drugs_tr_'+row).style.display='none';
	
}

function eq5d()
{
		if ($("#euroqol_score").val()=="11111")
			$("#eq5d").val("1");
		if ($("#euroqol_score").val()=="11112")
			$("#eq5d").val("0,85");
		if ($("#euroqol_score").val()=="11113")
			$("#eq5d").val("0,41");
		if ($("#euroqol_score").val()=="11121")
			$("#eq5d").val("0,8");
		if ($("#euroqol_score").val()=="11122")
			$("#eq5d").val("0,73");
		if ($("#euroqol_score").val()=="11123")
			$("#eq5d").val("0,29");
		if ($("#euroqol_score").val()=="11131")
			$("#eq5d").val("0,26");
		if ($("#euroqol_score").val()=="11132")
			$("#eq5d").val("0,19");
		if ($("#euroqol_score").val()=="11133")
			$("#eq5d").val("0,03");
		if ($("#euroqol_score").val()=="11211")
			$("#eq5d").val("0,88");
		if ($("#euroqol_score").val()=="11212")
			$("#eq5d").val("0,81");
		if ($("#euroqol_score").val()=="11213")
			$("#eq5d").val("0,38");
		if ($("#euroqol_score").val()=="11221")
			$("#eq5d").val("0,76");
		if ($("#euroqol_score").val()=="11222")
			$("#eq5d").val("0,69");
		if ($("#euroqol_score").val()=="11223")
			$("#eq5d").val("0,25");
		if ($("#euroqol_score").val()=="11231")
			$("#eq5d").val("0,23");
		if ($("#euroqol_score").val()=="11232")
			$("#eq5d").val("0,16");
		if ($("#euroqol_score").val()=="11233")
			$("#eq5d").val("-0,01");
		if ($("#euroqol_score").val()=="11311")
			$("#eq5d").val("0,56");
		if ($("#euroqol_score").val()=="11312")
			$("#eq5d").val("0,49");
		if ($("#euroqol_score").val()=="11313")
			$("#eq5d").val("0,32");
		if ($("#euroqol_score").val()=="11321")
			$("#eq5d").val("0,43");
		if ($("#euroqol_score").val()=="11322")
			$("#eq5d").val("0,36");
		if ($("#euroqol_score").val()=="11323")
			$("#eq5d").val("0,2");
		if ($("#euroqol_score").val()=="11331")
			$("#eq5d").val("0,17");
		if ($("#euroqol_score").val()=="11332")
			$("#eq5d").val("0,1");
		if ($("#euroqol_score").val()=="11333")
			$("#eq5d").val("-0,07");
		if ($("#euroqol_score").val()=="12111")
			$("#eq5d").val("0,82");
		if ($("#euroqol_score").val()=="12112")
			$("#eq5d").val("0,74");
		if ($("#euroqol_score").val()=="12113")
			$("#eq5d").val("0,31");
		if ($("#euroqol_score").val()=="12121")
			$("#eq5d").val("0,69");
		if ($("#euroqol_score").val()=="12122")
			$("#eq5d").val("0,62");
		if ($("#euroqol_score").val()=="12123")
			$("#eq5d").val("0,19");
		if ($("#euroqol_score").val()=="12131")
			$("#eq5d").val("0,16");
		if ($("#euroqol_score").val()=="12132")
			$("#eq5d").val("0,09");
		if ($("#euroqol_score").val()=="12133")
			$("#eq5d").val("-0,08");
		if ($("#euroqol_score").val()=="12211")
			$("#eq5d").val("0,78");
		if ($("#euroqol_score").val()=="12212")
			$("#eq5d").val("0,71");
		if ($("#euroqol_score").val()=="12213")
			$("#eq5d").val("0,27");
		if ($("#euroqol_score").val()=="12221")
			$("#eq5d").val("0,66");
		if ($("#euroqol_score").val()=="12222")
			$("#eq5d").val("0,59");
		if ($("#euroqol_score").val()=="12223")
			$("#eq5d").val("0,15");
		if ($("#euroqol_score").val()=="12231")
			$("#eq5d").val("0,12");
		if ($("#euroqol_score").val()=="12232")
			$("#eq5d").val("0,05");
		if ($("#euroqol_score").val()=="12233")
			$("#eq5d").val("-0,11");
		if ($("#euroqol_score").val()=="12311")
			$("#eq5d").val("0,45");
		if ($("#euroqol_score").val()=="12312")
			$("#eq5d").val("0,38");
		if ($("#euroqol_score").val()=="12313")
			$("#eq5d").val("0,22");
		if ($("#euroqol_score").val()=="12321")
			$("#eq5d").val("0,33");
		if ($("#euroqol_score").val()=="12322")
			$("#eq5d").val("0,26");
		if ($("#euroqol_score").val()=="12323")
			$("#eq5d").val("0,09");
		if ($("#euroqol_score").val()=="12331")
			$("#eq5d").val("0,07");
		if ($("#euroqol_score").val()=="12332")
			$("#eq5d").val("0");
		if ($("#euroqol_score").val()=="12333")
			$("#eq5d").val("-0,17");
		if ($("#euroqol_score").val()=="13111")
			$("#eq5d").val("0,44");
		if ($("#euroqol_score").val()=="13112")
			$("#eq5d").val("0,37");
		if ($("#euroqol_score").val()=="13113")
			$("#eq5d").val("0,2");
		if ($("#euroqol_score").val()=="13121")
			$("#eq5d").val("0,31");
		if ($("#euroqol_score").val()=="13122")
			$("#eq5d").val("0,24");
		if ($("#euroqol_score").val()=="13123")
			$("#eq5d").val("0,08");
		if ($("#euroqol_score").val()=="13131")
			$("#eq5d").val("0,05");
		if ($("#euroqol_score").val()=="13132")
			$("#eq5d").val("-0,02");
		if ($("#euroqol_score").val()=="13133")
			$("#eq5d").val("-0,19");
		if ($("#euroqol_score").val()=="13211")
			$("#eq5d").val("0,4");
		if ($("#euroqol_score").val()=="13212")
			$("#eq5d").val("0,33");
		if ($("#euroqol_score").val()=="13213")
			$("#eq5d").val("0,16");
		if ($("#euroqol_score").val()=="13221")
			$("#eq5d").val("0,28");
		if ($("#euroqol_score").val()=="13222")
			$("#eq5d").val("0,21");
		if ($("#euroqol_score").val()=="13223")
			$("#eq5d").val("0,04");
		if ($("#euroqol_score").val()=="13231")
			$("#eq5d").val("0,01");
		if ($("#euroqol_score").val()=="13232")
			$("#eq5d").val("-0,06");
		if ($("#euroqol_score").val()=="13233")
			$("#eq5d").val("-0,22");
		if ($("#euroqol_score").val()=="13311")
			$("#eq5d").val("0,34");
		if ($("#euroqol_score").val()=="13312")
			$("#eq5d").val("0,27");
		if ($("#euroqol_score").val()=="13313")
			$("#eq5d").val("0,11");
		if ($("#euroqol_score").val()=="13321")
			$("#eq5d").val("0,22");
		if ($("#euroqol_score").val()=="13322")
			$("#eq5d").val("0,15");
		if ($("#euroqol_score").val()=="13323")
			$("#eq5d").val("-0,02");
		if ($("#euroqol_score").val()=="13331")
			$("#eq5d").val("-0,04");
		if ($("#euroqol_score").val()=="13332")
			$("#eq5d").val("-0,11");
		if ($("#euroqol_score").val()=="13333")
			$("#eq5d").val("-0,28");
		if ($("#euroqol_score").val()=="21111")
			$("#eq5d").val("0,85");
		if ($("#euroqol_score").val()=="21112")
			$("#eq5d").val("0,78");
		if ($("#euroqol_score").val()=="21113")
			$("#eq5d").val("0,35");
		if ($("#euroqol_score").val()=="21121")
			$("#eq5d").val("0,73");
		if ($("#euroqol_score").val()=="21122")
			$("#eq5d").val("0,66");
		if ($("#euroqol_score").val()=="21123")
			$("#eq5d").val("0,22");
		if ($("#euroqol_score").val()=="21131")
			$("#eq5d").val("0,2");
		if ($("#euroqol_score").val()=="21132")
			$("#eq5d").val("0,12");
		if ($("#euroqol_score").val()=="21133")
			$("#eq5d").val("-0,04");
		if ($("#euroqol_score").val()=="21211")
			$("#eq5d").val("0,81");
		if ($("#euroqol_score").val()=="21212")
			$("#eq5d").val("0,74");
		if ($("#euroqol_score").val()=="21213")
			$("#eq5d").val("0,31");
		if ($("#euroqol_score").val()=="21221")
			$("#eq5d").val("0,69");
		if ($("#euroqol_score").val()=="21222")
			$("#eq5d").val("0,62");
		if ($("#euroqol_score").val()=="21223")
			$("#eq5d").val("0,19");
		if ($("#euroqol_score").val()=="21231")
			$("#eq5d").val("0,16");
		if ($("#euroqol_score").val()=="21232")
			$("#eq5d").val("0,09");
		if ($("#euroqol_score").val()=="21233")
			$("#eq5d").val("-0,08");
		if ($("#euroqol_score").val()=="21311")
			$("#eq5d").val("0,49");
		if ($("#euroqol_score").val()=="21312")
			$("#eq5d").val("0,42");
		if ($("#euroqol_score").val()=="21313")
			$("#eq5d").val("0,25");
		if ($("#euroqol_score").val()=="21321")
			$("#eq5d").val("0,36");
		if ($("#euroqol_score").val()=="21322")
			$("#eq5d").val("0,29");
		if ($("#euroqol_score").val()=="21323")
			$("#eq5d").val("0,13");
		if ($("#euroqol_score").val()=="21331")
			$("#eq5d").val("0,1");
		if ($("#euroqol_score").val()=="21332")
			$("#eq5d").val("0,03");
		if ($("#euroqol_score").val()=="21333")
			$("#eq5d").val("-0,13");
		if ($("#euroqol_score").val()=="22111")
			$("#eq5d").val("0,75");
		if ($("#euroqol_score").val()=="22112")
			$("#eq5d").val("0,68");
		if ($("#euroqol_score").val()=="22113")
			$("#eq5d").val("0,24");
		if ($("#euroqol_score").val()=="22121")
			$("#eq5d").val("0,62");
		if ($("#euroqol_score").val()=="22122")
			$("#eq5d").val("0,55");
		if ($("#euroqol_score").val()=="22123")
			$("#eq5d").val("0,12");
		if ($("#euroqol_score").val()=="22131")
			$("#eq5d").val("0,09");
		if ($("#euroqol_score").val()=="22132")
			$("#eq5d").val("0,02");
		if ($("#euroqol_score").val()=="22133")
			$("#eq5d").val("-0,14");
		if ($("#euroqol_score").val()=="22211")
			$("#eq5d").val("0,71");
		if ($("#euroqol_score").val()=="22212")
			$("#eq5d").val("0,64");
		if ($("#euroqol_score").val()=="22213")
			$("#eq5d").val("0,21");
		if ($("#euroqol_score").val()=="22221")
			$("#eq5d").val("0,59");
		if ($("#euroqol_score").val()=="22222")
			$("#eq5d").val("0,52");
		if ($("#euroqol_score").val()=="22223")
			$("#eq5d").val("0,08");
		if ($("#euroqol_score").val()=="22231")
			$("#eq5d").val("0,06");
		if ($("#euroqol_score").val()=="22232")
			$("#eq5d").val("-0,02");
		if ($("#euroqol_score").val()=="22233")
			$("#eq5d").val("-0,18");
		if ($("#euroqol_score").val()=="22311")
			$("#eq5d").val("0,38");
		if ($("#euroqol_score").val()=="22312")
			$("#eq5d").val("0,31");
		if ($("#euroqol_score").val()=="22313")
			$("#eq5d").val("0,15");
		if ($("#euroqol_score").val()=="22321")
			$("#eq5d").val("0,26");
		if ($("#euroqol_score").val()=="22322")
			$("#eq5d").val("0,19");
		if ($("#euroqol_score").val()=="22323")
			$("#eq5d").val("0,02");
		if ($("#euroqol_score").val()=="22331")
			$("#eq5d").val("0");
		if ($("#euroqol_score").val()=="22332")
			$("#eq5d").val("-0,07");
		if ($("#euroqol_score").val()=="22333")
			$("#eq5d").val("-0,24");
		if ($("#euroqol_score").val()=="23111")
			$("#eq5d").val("0,37");
		if ($("#euroqol_score").val()=="23112")
			$("#eq5d").val("0,3");
		if ($("#euroqol_score").val()=="23113")
			$("#eq5d").val("0,13");
		if ($("#euroqol_score").val()=="23121")
			$("#eq5d").val("0,24");
		if ($("#euroqol_score").val()=="23122")
			$("#eq5d").val("0,17");
		if ($("#euroqol_score").val()=="23123")
			$("#eq5d").val("0,01");
		if ($("#euroqol_score").val()=="23131")
			$("#eq5d").val("-0,02");
		if ($("#euroqol_score").val()=="23132")
			$("#eq5d").val("-0,09");
		if ($("#euroqol_score").val()=="23133")
			$("#eq5d").val("-0,25");
		if ($("#euroqol_score").val()=="23211")
			$("#eq5d").val("0,33");
		if ($("#euroqol_score").val()=="23212")
			$("#eq5d").val("0,26");
		if ($("#euroqol_score").val()=="23213")
			$("#eq5d").val("0,1");
		if ($("#euroqol_score").val()=="23221")
			$("#eq5d").val("0,21");
		if ($("#euroqol_score").val()=="23222")
			$("#eq5d").val("0,14");
		if ($("#euroqol_score").val()=="23223")
			$("#eq5d").val("-0,03");
		if ($("#euroqol_score").val()=="23231")
			$("#eq5d").val("-0,05");
		if ($("#euroqol_score").val()=="23232")
			$("#eq5d").val("-0,13");
		if ($("#euroqol_score").val()=="23233")
			$("#eq5d").val("-0,29");
		if ($("#euroqol_score").val()=="23311")
			$("#eq5d").val("0,27");
		if ($("#euroqol_score").val()=="23312")
			$("#eq5d").val("0,2");
		if ($("#euroqol_score").val()=="23313")
			$("#eq5d").val("0,04");
		if ($("#euroqol_score").val()=="23321")
			$("#eq5d").val("0,15");
		if ($("#euroqol_score").val()=="23322")
			$("#eq5d").val("0,08");
		if ($("#euroqol_score").val()=="23323")
			$("#eq5d").val("-0,09");
		if ($("#euroqol_score").val()=="23331")
			$("#eq5d").val("-0,11");
		if ($("#euroqol_score").val()=="23332")
			$("#eq5d").val("-0,18");
		if ($("#euroqol_score").val()=="23333")
			$("#eq5d").val("-0,35");
		if ($("#euroqol_score").val()=="31111")
			$("#eq5d").val("0,34");
		if ($("#euroqol_score").val()=="31112")
			$("#eq5d").val("0,27");
		if ($("#euroqol_score").val()=="31113")
			$("#eq5d").val("0,1");
		if ($("#euroqol_score").val()=="31121")
			$("#eq5d").val("0,21");
		if ($("#euroqol_score").val()=="31122")
			$("#eq5d").val("0,14");
		if ($("#euroqol_score").val()=="31123")
			$("#eq5d").val("-0,02");
		if ($("#euroqol_score").val()=="31131")
			$("#eq5d").val("-0,05");
		if ($("#euroqol_score").val()=="31132")
			$("#eq5d").val("-0,12");
		if ($("#euroqol_score").val()=="31133")
			$("#eq5d").val("-0,29");
		if ($("#euroqol_score").val()=="31211")
			$("#eq5d").val("0,3");
		if ($("#euroqol_score").val()=="31212")
			$("#eq5d").val("0,23");
		if ($("#euroqol_score").val()=="31213")
			$("#eq5d").val("0,06");
		if ($("#euroqol_score").val()=="31221")
			$("#eq5d").val("0,18");
		if ($("#euroqol_score").val()=="31222")
			$("#eq5d").val("0,11");
		if ($("#euroqol_score").val()=="31223")
			$("#eq5d").val("-0,06");
		if ($("#euroqol_score").val()=="31231")
			$("#eq5d").val("-0,09");
		if ($("#euroqol_score").val()=="31232")
			$("#eq5d").val("-0,16");
		if ($("#euroqol_score").val()=="31233")
			$("#eq5d").val("-0,32");
		if ($("#euroqol_score").val()=="31311")
			$("#eq5d").val("0,24");
		if ($("#euroqol_score").val()=="31312")
			$("#eq5d").val("0,17");
		if ($("#euroqol_score").val()=="31313")
			$("#eq5d").val("0,01");
		if ($("#euroqol_score").val()=="31321")
			$("#eq5d").val("0,12");
		if ($("#euroqol_score").val()=="31322")
			$("#eq5d").val("0,05");
		if ($("#euroqol_score").val()=="31323")
			$("#eq5d").val("-0,12");
		if ($("#euroqol_score").val()=="31331")
			$("#eq5d").val("-0,14");
		if ($("#euroqol_score").val()=="31332")
			$("#eq5d").val("-0,21");
		if ($("#euroqol_score").val()=="31333")
			$("#eq5d").val("0,38");
		if ($("#euroqol_score").val()=="32111")
			$("#eq5d").val("0,23");
		if ($("#euroqol_score").val()=="32112")
			$("#eq5d").val("0,16");
		if ($("#euroqol_score").val()=="32113")
			$("#eq5d").val("0");
		if ($("#euroqol_score").val()=="32121")
			$("#eq5d").val("0,11");
		if ($("#euroqol_score").val()=="32122")
			$("#eq5d").val("0,04");
		if ($("#euroqol_score").val()=="32123")
			$("#eq5d").val("-0,13");
		if ($("#euroqol_score").val()=="32131")
			$("#eq5d").val("-0,15");
		if ($("#euroqol_score").val()=="32132")
			$("#eq5d").val("-0,22");
		if ($("#euroqol_score").val()=="32133")
			$("#eq5d").val("-0,39");
		if ($("#euroqol_score").val()=="32211")
			$("#eq5d").val("0,2");
		if ($("#euroqol_score").val()=="32212")
			$("#eq5d").val("0,13");
		if ($("#euroqol_score").val()=="32213")
			$("#eq5d").val("-0,04");
		if ($("#euroqol_score").val()=="32221")
			$("#eq5d").val("0,07");
		if ($("#euroqol_score").val()=="32222")
			$("#eq5d").val("0");
		if ($("#euroqol_score").val()=="32223")
			$("#eq5d").val("-0,016");
		if ($("#euroqol_score").val()=="32231")
			$("#eq5d").val("-0,19");
		if ($("#euroqol_score").val()=="32232")
			$("#eq5d").val("-0,26");
		if ($("#euroqol_score").val()=="32233")
			$("#eq5d").val("-0,43");
		if ($("#euroqol_score").val()=="32311")
			$("#eq5d").val("0,14");
		if ($("#euroqol_score").val()=="32312")
			$("#eq5d").val("0,07");
		if ($("#euroqol_score").val()=="32313")
			$("#eq5d").val("-0,1");
		if ($("#euroqol_score").val()=="32321")
			$("#eq5d").val("0,02");
		if ($("#euroqol_score").val()=="32322")
			$("#eq5d").val("-0,06");
		if ($("#euroqol_score").val()=="32323")
			$("#eq5d").val("-0,22");
		if ($("#euroqol_score").val()=="32331")
			$("#eq5d").val("0,25");
		if ($("#euroqol_score").val()=="32332")
			$("#eq5d").val("-0,32");
		if ($("#euroqol_score").val()=="32333")
			$("#eq5d").val("-0,48");
		if ($("#euroqol_score").val()=="33111")
			$("#eq5d").val("0,12");
		if ($("#euroqol_score").val()=="33112")
			$("#eq5d").val("0,05");
		if ($("#euroqol_score").val()=="33113")
			$("#eq5d").val("-0,11");
		if ($("#euroqol_score").val()=="33121")
			$("#eq5d").val("0");
		if ($("#euroqol_score").val()=="33122")
			$("#eq5d").val("-0,07");
		if ($("#euroqol_score").val()=="33123")
			$("#eq5d").val("-0,24");
		if ($("#euroqol_score").val()=="33131")
			$("#eq5d").val("-0,26");
		if ($("#euroqol_score").val()=="33132")
			$("#eq5d").val("-0,33");
		if ($("#euroqol_score").val()=="33133")
			$("#eq5d").val("-0,5");
		if ($("#euroqol_score").val()=="33211")
			$("#eq5d").val("0,09");
		if ($("#euroqol_score").val()=="33212")
			$("#eq5d").val("0,02");
		if ($("#euroqol_score").val()=="33213")
			$("#eq5d").val("-0,15");
		if ($("#euroqol_score").val()=="33221")
			$("#eq5d").val("-0,04");
		if ($("#euroqol_score").val()=="33222")
			$("#eq5d").val("-0,11");
		if ($("#euroqol_score").val()=="33223")
			$("#eq5d").val("-0,27");
		if ($("#euroqol_score").val()=="33231")
			$("#eq5d").val("-0,3");
		if ($("#euroqol_score").val()=="33232")
			$("#eq5d").val("-0,37");
		if ($("#euroqol_score").val()=="33233")
			$("#eq5d").val("-0,54");
		if ($("#euroqol_score").val()=="33311")
			$("#eq5d").val("0,3");
		if ($("#euroqol_score").val()=="33312")
			$("#eq5d").val("-0,04");
		if ($("#euroqol_score").val()=="33313")
			$("#eq5d").val("-0,21");
		if ($("#euroqol_score").val()=="33321")
			$("#eq5d").val("-0,09");
		if ($("#euroqol_score").val()=="33322")
			$("#eq5d").val("-0,17");
		if ($("#euroqol_score").val()=="33323")
			$("#eq5d").val("-0,38");
		if ($("#euroqol_score").val()=="33331")
			$("#eq5d").val("-0,36");
		if ($("#euroqol_score").val()=="33332")
			$("#eq5d").val("-0,43");
		if ($("#euroqol_score").val()=="33333")
			$("#eq5d").val("-0,59");

}

</script>
<?php
	
if($save==1)
{
	if($save_chk=="ok")
	{
?>
<script>
$(".alert_suc").show();
window.setTimeout(function() {
    $(".alert_suc").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
	else
	{
?>
<script>
$(".alert_wr").show();
window.setTimeout(function() {
    $(".alert_wr").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
}
?>
</body>
</html>
