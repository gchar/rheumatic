<?php
$id = $_REQUEST['id'];

include '../library/config.php';
include '../library/openDB.php';
include '../library/functions.php';
include '../library/JSON.php';

$populate_arr = array();
if($id<>"" and $id<>"0")
{	
	$query="select patient_followup_id,TO_CHAR(start_date, 'DD-MM-YYYY') AS patient_followup_date_str from patient_followup where deleted=0 and patient_cohort_id=$id order by start_date desc";
	
	$exec = pg_query($query);
	//$result = pg_fetch_array($exec);

	while($result = pg_fetch_assoc($exec))
	{
		$id = $result['patient_followup_id'];
		$value = $result['patient_followup_date_str'];
		$populate_arr[] = array("id" => $id, "value" => $value);
		
	}
	
}

$json = new Services_JSON();
//$decoded = $json->decode($jsondata);
echo $json->encode($populate_arr);
include '../library/closeDB.php';
?>
