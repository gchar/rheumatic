		
<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	$save=$_REQUEST['save'];
	$pat_id=$_REQUEST['pat_id'];
	if($pat_id<>"")
		$_SESSION['pat_id']=$pat_id;
	
	$pat_id=$_SESSION['pat_id'];
	
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<?php
if($save==1)
{
	pg_query("BEGIN") or die("Could not start transaction\n");
	try
	{
		//NON-RHEUMATIC DISEASES table-start
		
		$j=1;
		$nonrheumatic_diseases_numofrows=$_REQUEST['nonrheumatic_diseases_numofrows'];
		for ($i=1;$i<=$nonrheumatic_diseases_numofrows;$i++)
		{
			$nonrheumatic_diseases_hidden=$_REQUEST['nonrheumatic_diseases_hidden_'.$i];
			$nonrheumatic_diseases_id=$_REQUEST['nonrheumatic_diseases_id_'.$i];
			if($nonrheumatic_diseases_hidden=='-1')
			{
				if($nonrheumatic_diseases_id<>"")
				{
					$nonrheumatic_diseases_hdn_parameters[0]=1;
					$nonrheumatic_diseases_hdn_names[0]='deleted';
					$nonrheumatic_diseases_hdn_edit_name[0]='nonrheumatic_diseases_id';
					$nonrheumatic_diseases_hdn_edit_id[0]=$nonrheumatic_diseases_id;
					$nonrheumatic_diseases_hdn_sumbol[0]='=';
					$nonrheumatic_diseases_msg=update('nonrheumatic_diseases',$nonrheumatic_diseases_hdn_names,$nonrheumatic_diseases_hdn_parameters,$nonrheumatic_diseases_hdn_edit_name,$nonrheumatic_diseases_hdn_edit_id,$nonrheumatic_diseases_hdn_sumbol);
					if ($nonrheumatic_diseases_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
			{
				if (isset($_REQUEST['onset_'.$i]))
					$onset="1";
				else
					$onset="0";
				
				$nonrheumatic_diseases_parameters[0]=$_REQUEST['diseases_organs_id_'.$i];
				$nonrheumatic_diseases_parameters[1]=$_REQUEST['diseases_comorbidities_id_'.$i];
				$nonrheumatic_diseases_parameters[2]=date_for_postgres($_REQUEST['date_of_diagnosis_'.$i]);
				$nonrheumatic_diseases_parameters[3]=$onset;
				$nonrheumatic_diseases_parameters[4]=str_replace("'", "''",$_REQUEST['comments_'.$i]);
				$nonrheumatic_diseases_parameters[5]=$user_id;
				$nonrheumatic_diseases_parameters[6]='now()';
				$nonrheumatic_diseases_table_names[0]='diseases_organs_id';
				$nonrheumatic_diseases_table_names[1]='diseases_comorbidities_id';
				$nonrheumatic_diseases_table_names[2]='date_of_diagnosis';
				$nonrheumatic_diseases_table_names[3]='onset';
				$nonrheumatic_diseases_table_names[4]='comments';
				$nonrheumatic_diseases_table_names[5]='editor_id';
				$nonrheumatic_diseases_table_names[6]='edit_date';
				$nonrheumatic_diseases_edit_name[0]='nonrheumatic_diseases_id';
				$nonrheumatic_diseases_edit_id[0]=$nonrheumatic_diseases_id;
				$nonrheumatic_diseases_sumbol[0]='=';
				
				if($nonrheumatic_diseases_id=='')
				{
					
					$nonrheumatic_diseases_parameters[7]=$pat_id;
					$nonrheumatic_diseases_parameters[8]=0;
					
					$nonrheumatic_diseases_table_names[7]='pat_id';
					$nonrheumatic_diseases_table_names[8]='deleted';
					
					$id=insert('nonrheumatic_diseases',$nonrheumatic_diseases_table_names,$nonrheumatic_diseases_parameters,'nonrheumatic_diseases_id');
					if($id!='')
						$j++;
				}
				else
				{
					$nonrheumatic_diseases_msg=update('nonrheumatic_diseases',$nonrheumatic_diseases_table_names,$nonrheumatic_diseases_parameters,$nonrheumatic_diseases_edit_name,$nonrheumatic_diseases_edit_id,$nonrheumatic_diseases_sumbol);
					if ($nonrheumatic_diseases_msg=="ok")
						$j++;
				}
			}
		}
		
		if ($j==$i)
			$comorbidities_msg1="ok";
		else
			$comorbidities_msg1="nok";
		//NON-RHEUMATIC DISEASES table-end
		
		//NON-RHEUMATIC DISEASES THERAPY table-start
		
		$j=1;
		$nonrheumatic_diseases_therapy_numofrows=$_REQUEST['nonrheumatic_diseases_therapy_numofrows'];
		for ($i=1;$i<=$nonrheumatic_diseases_therapy_numofrows;$i++)
		{
			$nonrheumatic_diseases_therapy_hidden=$_REQUEST['nonrheumatic_diseases_therapy_hidden_'.$i];
			$nonrheumatic_diseases_therapy_id=$_REQUEST['nonrheumatic_diseases_therapy_id_'.$i];
			if($nonrheumatic_diseases_therapy_hidden=='-1')
			{
				if($nonrheumatic_diseases_therapy_id<>"")
				{
					$nonrheumatic_diseases_therapy_hdn_parameters[0]=1;
					$nonrheumatic_diseases_therapy_hdn_names[0]='deleted';
					$nonrheumatic_diseases_therapy_hdn_edit_name[0]='nonrheumatic_diseases_therapy_id';
					$nonrheumatic_diseases_therapy_hdn_edit_id[0]=$nonrheumatic_diseases_therapy_id;
					$nonrheumatic_diseases_therapy_hdn_sumbol[0]='=';
					$nonrheumatic_diseases_therapy_msg=update('nonrheumatic_diseases_therapy',$nonrheumatic_diseases_therapy_hdn_names,$nonrheumatic_diseases_therapy_hdn_parameters,$nonrheumatic_diseases_therapy_hdn_edit_name,$nonrheumatic_diseases_therapy_hdn_edit_id,$nonrheumatic_diseases_therapy_hdn_sumbol);
					if ($nonrheumatic_diseases_therapy_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
			{
				
				if (isset($_REQUEST['start_before_inclusion_'.$i]))
					$start_before_inclusion="1";
				else
					$start_before_inclusion="0";
				
				$nonrheumatic_diseases_therapy_parameters[0]=$_REQUEST['diseases_therapy_organs_id_'.$i];
				$nonrheumatic_diseases_therapy_parameters[1]=$_REQUEST['diseases_therapy_comorbidities_id_'.$i];
				$nonrheumatic_diseases_therapy_parameters[2]=date_for_postgres($_REQUEST['start_date_'.$i]);
				$nonrheumatic_diseases_therapy_parameters[3]=$start_before_inclusion;
				$nonrheumatic_diseases_therapy_parameters[4]=date_for_postgres($_REQUEST['stop_date_'.$i]);
				$nonrheumatic_diseases_therapy_parameters[5]=str_replace("'", "''",$_REQUEST['comments2_'.$i]);
				$nonrheumatic_diseases_therapy_parameters[6]=$user_id;
				$nonrheumatic_diseases_therapy_parameters[7]='now()';
				$nonrheumatic_diseases_therapy_table_names[0]='diseases_therapy_organs_id';
				$nonrheumatic_diseases_therapy_table_names[1]='diseases_therapy_comorbidities_id';
				$nonrheumatic_diseases_therapy_table_names[2]='start_date';
				$nonrheumatic_diseases_therapy_table_names[3]='start_before_inclusion';
				$nonrheumatic_diseases_therapy_table_names[4]='stop_date';
				$nonrheumatic_diseases_therapy_table_names[5]='comments';
				$nonrheumatic_diseases_therapy_table_names[6]='editor_id';
				$nonrheumatic_diseases_therapy_table_names[7]='edit_date';
				$nonrheumatic_diseases_therapy_edit_name[0]='nonrheumatic_diseases_therapy_id';
				$nonrheumatic_diseases_therapy_edit_id[0]=$nonrheumatic_diseases_therapy_id;
				$nonrheumatic_diseases_therapy_sumbol[0]='=';
				
				if($nonrheumatic_diseases_therapy_id=='')
				{
					
					$nonrheumatic_diseases_therapy_parameters[8]=$pat_id;
					$nonrheumatic_diseases_therapy_parameters[9]=0;
					
					$nonrheumatic_diseases_therapy_table_names[8]='pat_id';
					$nonrheumatic_diseases_therapy_table_names[9]='deleted';
					
					$id=insert('nonrheumatic_diseases_therapy',$nonrheumatic_diseases_therapy_table_names,$nonrheumatic_diseases_therapy_parameters,'nonrheumatic_diseases_therapy_id');
					if($id!='')
						$j++;
				}
				else
				{
					$nonrheumatic_diseases_therapy_msg=update('nonrheumatic_diseases_therapy',$nonrheumatic_diseases_therapy_table_names,$nonrheumatic_diseases_therapy_parameters,$nonrheumatic_diseases_therapy_edit_name,$nonrheumatic_diseases_therapy_edit_id,$nonrheumatic_diseases_therapy_sumbol);
					if ($nonrheumatic_diseases_therapy_msg=="ok")
						$j++;
				}
			}
		}
		
		if ($j==$i)
			$comorbidities_msg2="ok";
		else
			$comorbidities_msg2="nok";
		//NON-RHEUMATIC DISEASES THERAPY table-end
		
		if ($comorbidities_msg1=="ok" and $comorbidities_msg2=="ok")
		{
			$save_chk="ok";
			pg_query("COMMIT") or die("Transaction commit failed\n");
		}
		else
		{
			$save_chk="nok";
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
	}
	catch(exception $e) {
		echo "e:".$e."</br>";
		echo "ROLLBACK";
		pg_query("ROLLBACK") or die("Transaction rollback failed\n");
	}
	
}

$exec = get_patient_demographics($pat_id);
$result = pg_fetch_array($exec);
$dateofinclusion=$result['dateofinclusion_str'];
if($dateofinclusion=="12-12-1900")
	$dateofinclusion="";
$dateofbirth=$result['dateofbirth_str'];
if($dateofbirth=="12-12-1900")
	$dateofbirth="";
$gender=$result['gender'];
if($gender==1)
	$gender_str="Female";
else if($gender==2)
	$gender_str="Male";
$patient_id=$result['patient_id'];
?>
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
	   <h1>
          Comorbidities
		</h1>
		<h1>
		<?php echo "<small>Patient:".$patient_id.",".$gender_str." (".$dateofbirth.") </small>"; ?>
      </h1>
      </section>

      <!-- Main content -->
      <section class="content">
	  <div class="alert alert_suc alert-success" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Successful save!</strong>
	  </div>
	  <div class="alert alert_wr alert-danger" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Unsuccessful save!</strong>
	  </div>
	  <form id="form" action="comorbidities.php" method="POST">
	  <input type="hidden" id="save" name="save" value="1">
		<input type="hidden" id="pat_id" name="pat_id" value="<?php echo $pat_id;?>">
		<div class="row">
			<div class="form-group col-md-12">
				<input type="submit" class="btn btn-primary" value="Save">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Diagnosis</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-12" id="divdiseases" name="divdiseases">
							<?php
										$sql = get_diseases_organs();
										$diseases_organs_counter=0;
										
										while($result2 = pg_fetch_array($sql))
										{	
											$diseases_organs_values .= $result2['value'];
											$diseases_organs_values .= "!@#$%^" ;
											
											$diseases_organs_ids .=$result2['diseases_organs_id'];
											$diseases_organs_ids .= "!@#$%^" ;
															
											$diseases_organs_counter++;
										}
									?>
										<input type="hidden" value="<?php echo $diseases_organs_counter; ?>" id="diseases_organs_counter" name="diseases_organs_counter"/>
										<input type="hidden" value="<?php echo $diseases_organs_values; ?>" id="diseases_organs_values" name="diseases_organs_values"/>
										<input type="hidden" value="<?php echo $diseases_organs_ids; ?>" id="diseases_organs_ids" name="diseases_organs_ids"/>
								  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="nonrheumatic_diseases_tbl" name="nonrheumatic_diseases_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-2"><b>Organ/System</b></th>
											<th class="col-md-3"><b>Comorbidities</b></th>
											<th class="col-md-2"><b>date of diagnosis</b></th>
											<th class="col-md-1"><b>Diagnosis before inclusion</b></th>
											<th class="col-md-3"><b>Comment</b></th>
											<th class="col-md-1">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
											$order = get_nonrheumatic_diseases($pat_id);
											$num_rows = pg_num_rows($order);
											
											while($result = pg_fetch_assoc($order))
											{
												$nonrheumatic_diseases_id=$result['nonrheumatic_diseases_id'];
												$diseases_organs_id=$result['diseases_organs_id'];
												$diseases_comorbidities_id=$result['diseases_comorbidities_id'];
												$date_of_diagnosis=$result['date_of_diagnosis_str'];
												if($date_of_diagnosis=="12-12-1900")
													$date_of_diagnosis="";
												$onset=$result['onset'];
												$comments=$result['comments'];
												
												
											
											
									?>
										  <tr id="nonrheumatic_diseases_tr_<?php echo $i;?>">
												<td align="center">
													<input type="hidden" id="nonrheumatic_diseases_hidden_<?php echo $i;?>" name="nonrheumatic_diseases_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="nonrheumatic_diseases_id_<?php echo $i; ?>" id="nonrheumatic_diseases_id_<?php echo $i; ?>" value="<?php echo $nonrheumatic_diseases_id;?>">
													<select  onchange="setdiseasecomorbidities(<?php echo $i; ?>)" class="form-control" name="diseases_organs_id_<?php echo $i; ?>" id="diseases_organs_id_<?php echo $i; ?>">
														<option value="0">--</option>
														<?php
															$sql = get_diseases_organs();
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$diseases_organs_id2=$result2['diseases_organs_id'];
																$value=$result2['value'];
																
																if($diseases_organs_id==$diseases_organs_id2)
																	$organ_id=$diseases_organs_id2;
																
														?>
														<option value="<?php echo $diseases_organs_id2; ?>" <?php if($diseases_organs_id==$diseases_organs_id2) { echo "selected"; } ?>><?php echo $value; ?></option>
														<?php
															}
														?>
													</select>
												</td>
											   <td align="center">
													<select class="form-control" name="diseases_comorbidities_id_<?php echo $i; ?>" id="diseases_comorbidities_id_<?php echo $i; ?>">
														<?php
														if ($organ_id==0)
														{
														?>
															<option value="0">No Comorbidities</option>
														<?php
														}
														?>
														<?php
															$sql = get_diseasecomorbidities2($organ_id);
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$diseases_comorbidities_id2=$result2['diseases_comorbidities_id'];
																$code=$result2['code'];
																$value=$result2['value'];
																$icd10=$result2['icd10'];
																$diseases_organs_id2=$result2['diseases_organs_id'];
																
																if($diseases_organs_id2==$diseases_organs_id) 
																{
																	
																
																
														?>
														<option value="<?php echo $diseases_comorbidities_id2; ?>" <?php if($diseases_comorbidities_id==$diseases_comorbidities_id2) { echo "selected"; } ?>><?php echo $code." - ".$value."(".$icd10.")"; ?></option>
														<?php
																}
															}
														?>
													</select>
												</td>
												<td align="center">
													<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="date_of_diagnosis_<?php echo $i; ?>" name="date_of_diagnosis_<?php echo $i; ?>" value="<?php echo $date_of_diagnosis; ?>">
												</td>
												<td align="center">
													<input type="checkbox" id="onset_<?php echo $i; ?>" name="onset_<?php echo $i; ?>" <?php if ($onset==1) { echo "checked"; } ?>>
												</td>
												<td align="center">
													<textarea type="text" class="form-control" id="comments_<?php echo $i; ?>" name="comments_<?php echo $i; ?>"><?php echo $comments; ?></textarea>
												</td>
												<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="nonrheumatic_diseases_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
												 </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										
									?>
									</tbody>
								</table>
								<input type="hidden" name="nonrheumatic_diseases_numofrows" id="nonrheumatic_diseases_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								 <a class="btn btn-primary" href="javascript:void(0);" onclick="nonrheumatic_diseases_addrow();"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
					</div>
				</div><!-- diagnoses status div -->
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Treatment</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-12" id="divdiseasestherapy" name="divdiseasestherapy">
							<?php
										$sql = get_diseases_therapy_organs();
										$diseases_therapy_organs_counter=0;
										
										while($result2 = pg_fetch_array($sql))
										{	
											$diseases_therapy_organs_values .= $result2['value'];
											$diseases_therapy_organs_values .= "!@#$%^" ;
											
											$diseases_therapy_organs_ids .=$result2['diseases_therapy_organs_id'];
											$diseases_therapy_organs_ids .= "!@#$%^" ;
															
											$diseases_therapy_organs_counter++;
										}
									?>
										<input type="hidden" value="<?php echo $diseases_therapy_organs_counter; ?>" id="diseases_therapy_organs_counter" name="diseases_therapy_organs_counter"/>
										<input type="hidden" value="<?php echo $diseases_therapy_organs_values; ?>" id="diseases_therapy_organs_values" name="diseases_therapy_organs_values"/>
										<input type="hidden" value="<?php echo $diseases_therapy_organs_ids; ?>" id="diseases_therapy_organs_ids" name="diseases_therapy_organs_ids"/>
								  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="nonrheumatic_diseases_therapy_tbl" name="nonrheumatic_diseases_therapy_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-2"><b>Organ/System</b></th>
											<th class="col-md-2"><b>Drugs</b></th>
											<th class="col-md-2"><b>Start date</b></th>
											<th class="col-md-2"><b>Start before inclusion</b></th>
											<th class="col-md-2"><b>Stop date</b></th>
											<th class="col-md-1"><b>Comment</b></th>
											<th class="col-md-1">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
											$order = get_nonrheumatic_diseases_therapy($pat_id);
											$num_rows = pg_num_rows($order);
											
											while($result = pg_fetch_assoc($order))
											{
												$nonrheumatic_diseases_therapy_id=$result['nonrheumatic_diseases_therapy_id'];
												$diseases_therapy_organs_id=$result['diseases_therapy_organs_id'];
												$diseases_therapy_comorbidities_id=$result['diseases_therapy_comorbidities_id'];
												$start_date=$result['start_date_str'];
												if($start_date=="12-12-1900")
													$start_date="";
												$stop_date=$result['stop_date_str'];
												if($stop_date=="12-12-1900")
													$stop_date="";
												$start_before_inclusion=$result['start_before_inclusion'];
												$comments=$result['comments'];
												
												
											
											
									?>
										  <tr id="nonrheumatic_diseases_therapy_tr_<?php echo $i;?>">
												<td align="center">
													<input type="hidden" id="nonrheumatic_diseases_therapy_hidden_<?php echo $i;?>" name="nonrheumatic_diseases_therapy_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="nonrheumatic_diseases_therapy_id_<?php echo $i; ?>" id="nonrheumatic_diseases_therapy_id_<?php echo $i; ?>" value="<?php echo $nonrheumatic_diseases_therapy_id;?>">
													<select  onchange="setdiseasecomorbiditiestherapy(<?php echo $i; ?>)" class="form-control" name="diseases_therapy_organs_id_<?php echo $i; ?>" id="diseases_therapy_organs_id_<?php echo $i; ?>">
														<option value="0">--</option>
														<?php
															$sql = get_diseases_therapy_organs();
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$diseases_therapy_organs_id2=$result2['diseases_therapy_organs_id'];
																$value=$result2['value'];
																if($diseases_therapy_organs_id==$diseases_therapy_organs_id2)
																	$therapy_organ_id=$diseases_therapy_organs_id2;
																
																
														?>
														<option value="<?php echo $diseases_therapy_organs_id2; ?>" <?php if($diseases_therapy_organs_id==$diseases_therapy_organs_id2) { echo "selected"; } ?>><?php echo $value; ?></option>
														<?php
															}
														?>
													</select>
												</td>
											   <td align="center">
													<select class="form-control" name="diseases_therapy_comorbidities_id_<?php echo $i; ?>" id="diseases_therapy_comorbidities_id_<?php echo $i; ?>">
														<?php
														if ($therapy_organ_id==0)
														{
														?>
														<option value="0">No Treatment</option>
														<?php
														}
															$sql = get_therapycomorbidities2($therapy_organ_id);
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$diseases_therapy_comorbidities_id2=$result2['diseases_therapy_comorbidities_id'];
																$code=$result2['code'];
																$value=$result2['value'];
																$atc_ddd=$result2['atc_ddd'];
																$diseases_therapy_organs_id2=$result2['diseases_therapy_organs_id'];
																
																if($diseases_therapy_organs_id2==$diseases_therapy_organs_id) 
																{
																	
																
																
														?>
														<option value="<?php echo $diseases_therapy_comorbidities_id2; ?>" <?php if($diseases_therapy_comorbidities_id==$diseases_therapy_comorbidities_id2) { echo "selected"; } ?>><?php echo $code." - ".$value."(".$atc_ddd.")"; ?></option>
														<?php
																}
															}
														?>
													</select>
												</td>
												<td align="center">
													<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="start_date_<?php echo $i; ?>" name="start_date_<?php echo $i; ?>" value="<?php echo $start_date; ?>">
												</td>
												<td align="center">
													<input type="checkbox" id="start_before_inclusion_<?php echo $i; ?>" name="start_before_inclusion_<?php echo $i; ?>" <?php if ($start_before_inclusion==1) { echo "checked"; } ?>>
												</td>
												<td align="center">
													<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="stop_date_<?php echo $i; ?>" name="stop_date_<?php echo $i; ?>" value="<?php echo $stop_date; ?>">
												</td>
												<td align="center">
													<textarea type="text" class="form-control" id="comments2_<?php echo $i; ?>" name="comments2_<?php echo $i; ?>"><?php echo $comments; ?></textarea>
												</td>
												<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="nonrheumatic_diseases_therapy_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
												 </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										
									?>
									</tbody>
								</table>
								<input type="hidden" name="nonrheumatic_diseases_therapy_numofrows" id="nonrheumatic_diseases_therapy_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								 <a class="btn btn-primary" href="javascript:void(0);" onclick="nonrheumatic_diseases_therapy_addrow();"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
					</div>
				</div><!-- diagnoses status div -->
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<input type="submit" class="btn btn-primary" value="Save">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				&nbsp;
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				&nbsp;
			</div>
		</div>
		</form>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
   <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>
$(function () {
	  
	   $('#form').submit(function(){
		
		var msg="";
		var sum=0;
		var row=0;
			
		var rows=document.getElementById('nonrheumatic_diseases_numofrows').value;
		for(i = 1; i <= rows; i++)
		{
			if($('#nonrheumatic_diseases_hidden_'+i).val()==1)
			{
				row++;
				if($('#diseases_comorbidities_id_'+i).val()==0)
					sum++;
			}
		}
		
		var sum2=0;
		var row2=0;
		var rows2=document.getElementById('nonrheumatic_diseases_therapy_numofrows').value;
		for(i = 1; i <= rows2; i++)
		{
			if($('#nonrheumatic_diseases_therapy_hidden_'+i).val()==1)
			{
				row2++;
				if($('#diseases_therapy_comorbidities_id_'+i).val()==0)
					sum2++;
			}
		}
	
		if(sum>0 && row>sum)
			msg +='-You have selected that there is "No Comorbidity" and you have selected a particular comorbidity at the same time!\n';
		
		if(sum2>0 && row2>sum2)
			msg +='-You have selected that there is "No Treatment" and you have selected a particular Treatment at the same time!\n';
		
		if(msg != '')
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
		
		
	  });
	  
jQuery(document).ready(function() {

for(i=1;i<=document.getElementById('nonrheumatic_diseases_numofrows').value;i++)
{
	$('#date_of_diagnosis_'+i).datepicker({
	  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	})
	$('#date_of_diagnosis_'+i).inputmask();
}

for(i=1;i<=document.getElementById('nonrheumatic_diseases_therapy_numofrows').value;i++)
{
	
	$('#start_date_'+i).datepicker({
	   dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	})
	$('#start_date_'+i).inputmask();
	
	$('#stop_date_'+i).datepicker({
	   dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	})
	$('#stop_date_'+i).inputmask();
}
})
$(document).ajaxStart(function(){
		$('body').loading();
		
    });
    $(document).ajaxComplete(function(){
       $('body').loading('stop');
    });
	
function setdiseasecomorbiditiestherapy(id)
{
        var diseases_therapy_organs_id = $("#diseases_therapy_organs_id_"+id).val();
		
        $.ajax({
            url: 'populate_comorbidities_therapy.php',
            type: 'post',
            data: {
				id:diseases_therapy_organs_id
				},
            dataType: 'json',
            success:function(response){
			
                var len = response.length;
				
                $("#diseases_therapy_comorbidities_id_"+id).empty();
				if(len==0)
					$("#diseases_therapy_comorbidities_id_"+id).append("<option value='0'>No Treatment</option>");
				
                for( var i = 0; i<len; i++){
                    var id2 = response[i]['id'];
					var code = response[i]['code'];
                    var value = response[i]['value'];
                    var atc_ddd = response[i]['atc_ddd'];
					
                    $("#diseases_therapy_comorbidities_id_"+id).append("<option value='"+id2+"'>"+code+"-"+value+" ("+atc_ddd+")"+"</option>");
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }

function setdiseasecomorbidities(id)
{
        var diseases_organs_id = $("#diseases_organs_id_"+id).val();
		
        $.ajax({
            url: 'populate_comorbidities.php',
            type: 'post',
            data: {
				id:diseases_organs_id
				},
            dataType: 'json',
            success:function(response){
			
                var len = response.length;
				
				$("#diseases_comorbidities_id_"+id).empty();
				if(len==0)
					$("#diseases_comorbidities_id_"+id).append("<option value='0'>No Comorbidities</option>");
				//else
				//	$("#diseases_comorbidities_id_"+id).append("<option value='0'>--</option>");
				
                for( var i = 0; i<len; i++){
                    var id2 = response[i]['id'];
					var code = response[i]['code'];
                    var value = response[i]['value'];
                    var icd10 = response[i]['icd10'];
					
                    $("#diseases_comorbidities_id_"+id).append("<option value='"+id2+"'>"+code+"-"+value+" ("+icd10+")"+"</option>");
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
	
function nonrheumatic_diseases_addrow()
{
  var tbl = document.getElementById('nonrheumatic_diseases_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'nonrheumatic_diseases_tr_'+iteration;
  document.getElementById('nonrheumatic_diseases_numofrows').value = iteration;
	
	var diseases_organs_ids=document.getElementById('diseases_organs_ids').value;
	var diseases_organs_values=document.getElementById('diseases_organs_values').value;
	var diseases_organs_counter=document.getElementById('diseases_organs_counter').value;
	var diseases_organs_value=diseases_organs_values.split('!@#$%^');
	var diseases_organs_id=diseases_organs_ids.split('!@#$%^');
	
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('select');
  el1.name = 'diseases_organs_id_' + iteration;
  el1.id = 'diseases_organs_id_' + iteration;
  el1.onchange =function(){setdiseasecomorbidities(iteration)};
  el1.style.width='100%';
  el1.className='form-control';
  
	var opt=new Option("--",0);
	el1.add(opt,undefined);
	for(i = 0; i < diseases_organs_counter; i++)
	{
		var opt=new Option(diseases_organs_value[i],diseases_organs_id[i]);
		el1.add(opt,undefined);
	}
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'nonrheumatic_diseases_hidden_' + iteration;
  el2.id = 'nonrheumatic_diseases_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'nonrheumatic_diseases_id_' + iteration;
  el3.id = 'nonrheumatic_diseases_id_' + iteration;
  el3.value='';
  
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
	var r2 = row.insertCell(1);
	r2.align='center';
	
	var el4 = document.createElement('select');
	el4.name = 'diseases_comorbidities_id_' + iteration;
	el4.id = 'diseases_comorbidities_id_' + iteration;
	el4.style.width='100%';
	el4.className='form-control';
  
	var opt=new Option("No Comorbidities",0);
	el4.add(opt,undefined);
	r2.appendChild(el4);
  
	var r3 = row.insertCell(2);
	r3.align='center';
	var el5 = document.createElement('input');
	el5.type = 'text';
	el5.name = 'date_of_diagnosis_' + iteration;
	el5.id = 'date_of_diagnosis_' + iteration;
	el5.className="form-control";
	el5.style.width='100%';
  
	r3.appendChild(el5);
  
	var r4 = row.insertCell(3);
	r4.align='center';
  
	var el6 = document.createElement('input');
	el6.type = 'checkbox';
	el6.name = 'onset_' + iteration;
	el6.id = 'onset_' + iteration;
  
	r4.appendChild(el6);
  
	var r5 = row.insertCell(4);
	r5.align='center';
  
	var el7 = document.createElement('textarea');
	el7.name = 'comments_' + iteration;
	el7.id = 'comments_' + iteration;
	el7.className="form-control";
	el7.style.width='100%';
  
	r5.appendChild(el7);
	
	var r6 = row.insertCell(5);
	r6.align='center';
	
	var el8 = document.createElement('a');
	el8.name = 'a_' + iteration;
	el8.id = 'a_' + iteration;
	el8.title = 'Delete';
	el8.href="javascript:avoid(0)";
	var icon = document.createElement('i');
	icon.className='fa fa-trash-o';
	el8.appendChild(icon);
	el8.onclick =function(){nonrheumatic_diseases_removerow(iteration)};
		 
	r6.appendChild(el8);
	
	$('#date_of_diagnosis_'+ iteration).datepicker({
		    dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  $('#date_of_diagnosis_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
}

function nonrheumatic_diseases_removerow(row)
{
	document.getElementById('nonrheumatic_diseases_hidden_'+row).value='-1';
	document.getElementById('nonrheumatic_diseases_tr_'+row).style.display='none';
}


function nonrheumatic_diseases_therapy_addrow()
{
  var tbl = document.getElementById('nonrheumatic_diseases_therapy_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'nonrheumatic_diseases_therapy_tr_'+iteration;
  document.getElementById('nonrheumatic_diseases_therapy_numofrows').value = iteration;
	
	var diseases_therapy_organs_ids=document.getElementById('diseases_therapy_organs_ids').value;
	var diseases_therapy_organs_values=document.getElementById('diseases_therapy_organs_values').value;
	var diseases_therapy_organs_counter=document.getElementById('diseases_therapy_organs_counter').value;
	var diseases_therapy_organs_value=diseases_therapy_organs_values.split('!@#$%^');
	var diseases_therapy_organs_id=diseases_therapy_organs_ids.split('!@#$%^');
	
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('select');
  el1.name = 'diseases_therapy_organs_id_' + iteration;
  el1.id = 'diseases_therapy_organs_id_' + iteration;
  el1.onchange =function(){setdiseasecomorbiditiestherapy(iteration)};
  el1.style.width='100%';
  el1.className='form-control';
  
	var opt=new Option("--",0);
	el1.add(opt,undefined);
	for(i = 0; i < diseases_therapy_organs_counter; i++)
	{
		var opt=new Option(diseases_therapy_organs_value[i],diseases_therapy_organs_id[i]);
		el1.add(opt,undefined);
	}
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'nonrheumatic_diseases_therapy_hidden_' + iteration;
  el2.id = 'nonrheumatic_diseases_therapy_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'nonrheumatic_diseases_therapy_id_' + iteration;
  el3.id = 'nonrheumatic_diseases_therapy_id_' + iteration;
  el3.value='';
  
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
	var r2 = row.insertCell(1);
	r2.align='center';
	
	var el4 = document.createElement('select');
	el4.name = 'diseases_therapy_comorbidities_id_' + iteration;
	el4.id = 'diseases_therapy_comorbidities_id_' + iteration;
	el4.style.width='100%';
	el4.className='form-control';
  
	var opt=new Option("No Treatment",0);
	el4.add(opt,undefined);
	r2.appendChild(el4);
  
	r2.appendChild(el4);
  
	var r3 = row.insertCell(2);
	r3.align='center';
	var el5 = document.createElement('input');
	el5.type = 'text';
	el5.name = 'start_date_' + iteration;
	el5.id = 'start_date_' + iteration;
	el5.className="form-control";
	el5.style.width='100%';
  
	r3.appendChild(el5);
  
	var r4 = row.insertCell(3);
	r4.align='center';
  
	var el6 = document.createElement('input');
	el6.type = 'checkbox';
	el6.name = 'start_before_inclusion_' + iteration;
	el6.id = 'start_before_inclusion_' + iteration;
	
	r4.appendChild(el6);
  
	var r5 = row.insertCell(4);
	r5.align='center';
  
	var el7 = document.createElement('input');
	el7.type = 'text';
	el7.name = 'stop_date_' + iteration;
	el7.id = 'stop_date_' + iteration;
	el7.className="form-control";
	el7.style.width='100%';
  
	r5.appendChild(el7);
	
	var r6 = row.insertCell(5);
	r6.align='center';
  
	var el8 = document.createElement('textarea');
	el8.name = 'comments2_' + iteration;
	el8.id = 'comments2_' + iteration;
	el8.className="form-control";
	el8.style.width='100%';
  
	r6.appendChild(el8);
	
	var r7 = row.insertCell(6);
	r7.align='center';
	
	var el9 = document.createElement('a');
	el9.name = 'a_' + iteration;
	el9.id = 'a_' + iteration;
	el9.title = 'Delete';
	el9.href="javascript:avoid(0)";
	var icon = document.createElement('i');
	icon.className='fa fa-trash-o';
	el9.appendChild(icon);
	el9.onclick =function(){nonrheumatic_diseases_therapy_removerow(iteration)};
		 
	r7.appendChild(el9);
	
	$('#start_date_'+ iteration).datepicker({
		   dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  $('#start_date_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
  $('#stop_date_'+ iteration).datepicker({
		   dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  $('#stop_date_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
}

function nonrheumatic_diseases_therapy_removerow(row)
{
	document.getElementById('nonrheumatic_diseases_therapy_hidden_'+row).value='-1';
	document.getElementById('nonrheumatic_diseases_therapy_tr_'+row).style.display='none';
}

</script>
<?php
	
if($save==1)
{
	if($save_chk=="ok")
	{
?>
<script>
$(".alert_suc").show();
window.setTimeout(function() {
    $(".alert_suc").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
	else
	{
?>
<script>
$(".alert_wr").show();
window.setTimeout(function() {
    $(".alert_wr").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
}
?>
</body>
</html>
