<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	$save=$_REQUEST['save'];
	$pat_id=$_REQUEST['pat_id'];
	if($pat_id<>"")
		$_SESSION['pat_id']=$pat_id;
	
	$pat_id=$_SESSION['pat_id'];
	
	$patient_cohort_id=$_REQUEST['patient_cohort_id'];
	if($patient_cohort_id<>"")
		$_SESSION['patient_cohort_id']=$patient_cohort_id;
	$patient_cohort_id=$_SESSION['patient_cohort_id'];
	
	
	$patient_followup_id=$_REQUEST['patient_followup_id'];
	if($patient_followup_id<>"")
		$_SESSION['patient_followup_id']=$patient_followup_id;
	
	$patient_followup_id=$_SESSION['patient_followup_id'];
	
	$patient_event_id=$_REQUEST['patient_event_id'];
	if($patient_event_id<>"")
		$_SESSION['patient_event_id']=$patient_event_id;
	
	$patient_event_id=$_SESSION['patient_event_id'];
	
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<?php
if($save==1)
{
	pg_query("BEGIN") or die("Could not start transaction\n");
	try
	{
		
		$order = get_medra_val($_REQUEST['description_lvl_1']);
		$result = pg_fetch_assoc($order);
		$description_lvl_1=$result['description'];
		if($_REQUEST['ctcae_lvl_1']==5)
			$grade_lvl_1=$result['grade1'];
		else if($_REQUEST['ctcae_lvl_1']==4)
			$grade_lvl_1=$result['grade2'];
		else if($_REQUEST['ctcae_lvl_1']==3)
			$grade_lvl_1=$result['grade3'];
		else if($_REQUEST['ctcae_lvl_1']==2)
			$grade_lvl_1=$result['grade4'];
		else if($_REQUEST['ctcae_lvl_1']==1)
			$grade_lvl_1=$result['grade5'];
		
		$order = get_medra_val($_REQUEST['description_lvl_2']);
		$result = pg_fetch_assoc($order);
		$description_lvl_2=$result['description'];
		if($_REQUEST['ctcae_lvl_2']==5)
			$grade_lvl_2=$result['grade1'];
		else if($_REQUEST['ctcae_lvl_2']==4)
			$grade_lvl_2=$result['grade2'];
		else if($_REQUEST['ctcae_lvl_2']==3)
			$grade_lvl_2=$result['grade3'];
		else if($_REQUEST['ctcae_lvl_2']==2)
			$grade_lvl_2=$result['grade4'];
		else if($_REQUEST['ctcae_lvl_2']==1)
			$grade_lvl_2=$result['grade5'];
		
		$order = get_medra_val($_REQUEST['description_lvl_3']);
		$result = pg_fetch_assoc($order);
		$description_lvl_3=$result['description'];
		if($_REQUEST['ctcae_lvl_3']==5)
			$grade_lvl_3=$result['grade1'];
		else if($_REQUEST['ctcae_lvl_3']==4)
			$grade_lvl_3=$result['grade2'];
		else if($_REQUEST['ctcae_lvl_3']==3)
			$grade_lvl_3=$result['grade3'];
		else if($_REQUEST['ctcae_lvl_3']==2)
			$grade_lvl_3=$result['grade4'];
		else if($_REQUEST['ctcae_lvl_3']==1)
			$grade_lvl_3=$result['grade5'];
		
		$order = get_medra_val($_REQUEST['description_lvl_4']);
		$result = pg_fetch_assoc($order);
		$description_lvl_4=$result['description'];
		if($_REQUEST['ctcae_lvl_4']==5)
			$grade_lvl_4=$result['grade1'];
		else if($_REQUEST['ctcae_lvl_4']==4)
			$grade_lvl_4=$result['grade2'];
		else if($_REQUEST['ctcae_lvl_4']==3)
			$grade_lvl_4=$result['grade3'];
		else if($_REQUEST['ctcae_lvl_4']==2)
			$grade_lvl_4=$result['grade4'];
		else if($_REQUEST['ctcae_lvl_4']==1)
			$grade_lvl_4=$result['grade5'];
		
		//$description="lvl1: ".$description_lvl_1." (".$grade_lvl_1.") lvl2: ".$description_lvl_2." (".$grade_lvl_2.") lvl3: ".$description_lvl_3." (".$grade_lvl_3.") lvl4: ".$description_lvl_4."  (".$grade_lvl_4.")";
		
		$parameters[0]=$pat_id;
		$parameters[1]=0;
		$parameters[2]=$user_id;
		$parameters[3]='now()';
		$parameters[4]=date_for_postgres($_REQUEST['start_date']);
		$parameters[5]=$patient_cohort_id;
		$parameters[6]=$patient_followup_id;
		$parameters[7]=$_REQUEST['hospitalization'];
		$parameters[8]=str_replace("'", "''",$_REQUEST['hospitalization_duration']);
		$parameters[9]=$_REQUEST['main_organ'];
		$parameters[10]=$_REQUEST['secondary_organ'];
		$parameters[11]=$_REQUEST['description_lvl_1'];
		$parameters[12]=$_REQUEST['description_lvl_2'];
		$parameters[13]=$_REQUEST['description_lvl_3'];
		$parameters[14]=$_REQUEST['description_lvl_4'];
		$parameters[15]=$_REQUEST['ctcae_lvl_1'];
		$parameters[16]=$_REQUEST['ctcae_lvl_2'];
		$parameters[17]=$_REQUEST['ctcae_lvl_3'];
		$parameters[18]=$_REQUEST['ctcae_lvl_4'];
		$parameters[19]=$_REQUEST['relationship'];
		$parameters[20]=$_REQUEST['outcome'];
		$parameters[21]=$_REQUEST['seriousness'];
		$parameters[22]=$_REQUEST['change_therapy'];
		$parameters[23]=$_REQUEST['description'];
		
		$table_names[0]='pat_id';
		$table_names[1]='deleted';
		$table_names[2]='editor_id';
		$table_names[3]='edit_date';
		$table_names[4]='start_date';
		$table_names[5]='patient_cohort_id';
		$table_names[6]='patient_followup_id';
		$table_names[7]='hospitalization';
		$table_names[8]='hospitalization_duration';
		$table_names[9]='main_organ';
		$table_names[10]='secondary_organ';
		$table_names[11]='description_lvl_1';
		$table_names[12]='description_lvl_2';
		$table_names[13]='description_lvl_3';
		$table_names[14]='description_lvl_4';
		$table_names[15]='ctcae_lvl_1';
		$table_names[16]='ctcae_lvl_2';
		$table_names[17]='ctcae_lvl_3';
		$table_names[18]='ctcae_lvl_4';
		$table_names[19]='relationship';
		$table_names[20]='outcome';
		$table_names[21]='seriousness';
		$table_names[22]='change_therapy';
		$table_names[23]='description';
		
		
		
		$edit_name[0]='patient_event_id';
		$edit_id[0]=$patient_event_id;
		$sumbol[0]='=';
		if($patient_event_id=='')
		{
			$patient_event_id=insert('patient_event',$table_names,$parameters,'patient_event_id');
			if($patient_event_id!='')
				$msg="ok";
		}
		else
		{
			$msg=update('patient_event',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
		}
		
		$description_lvl_1=$_REQUEST['description_lvl_1'];
		$description_lvl_2=$_REQUEST['description_lvl_2'];
		$description_lvl_3=$_REQUEST['description_lvl_3'];
		$description_lvl_4=$_REQUEST['description_lvl_4'];
		
		$k=0;
		$j=0;
		
		$order = get_medra_val($description_lvl_1);
		$result = pg_fetch_array($order);
		$diseases_comorbidities_id = $result['diseases_comorbidities_id'];
		if($diseases_comorbidities_id<>0)
		{
			$order = get_nonrheumatic_disease($pat_id,$diseases_comorbidities_id);
			$num_rows = pg_num_rows($order);
			if($num_rows==0)
			{
				$k++;
				$order2 = get_diseasecomorbiditie($diseases_comorbidities_id);
				$result2 = pg_fetch_array($order2);
				$diseases_organs_id = $result2['diseases_organs_id'];
			
				$nonrheumatic_diseases_parameters[0]=$diseases_organs_id;
				$nonrheumatic_diseases_parameters[1]=$diseases_comorbidities_id;
				$nonrheumatic_diseases_parameters[2]=date_for_postgres($_REQUEST['start_date']);
				$nonrheumatic_diseases_parameters[3]=0;
				$nonrheumatic_diseases_parameters[4]="";
				$nonrheumatic_diseases_parameters[5]=$user_id;
				$nonrheumatic_diseases_parameters[6]='now()';
				$nonrheumatic_diseases_parameters[7]=$pat_id;
				$nonrheumatic_diseases_parameters[8]=0;
				$nonrheumatic_diseases_table_names[0]='diseases_organs_id';
				$nonrheumatic_diseases_table_names[1]='diseases_comorbidities_id';
				$nonrheumatic_diseases_table_names[2]='date_of_diagnosis';
				$nonrheumatic_diseases_table_names[3]='onset';
				$nonrheumatic_diseases_table_names[4]='comments';
				$nonrheumatic_diseases_table_names[5]='editor_id';
				$nonrheumatic_diseases_table_names[6]='edit_date';
				$nonrheumatic_diseases_table_names[7]='pat_id';
				$nonrheumatic_diseases_table_names[8]='deleted';
				$id=insert('nonrheumatic_diseases',$nonrheumatic_diseases_table_names,$nonrheumatic_diseases_parameters,'nonrheumatic_diseases_id');
				if($id!='')
					$j++;
			}
		}
		
		$order = get_medra_val($description_lvl_2);
		$result = pg_fetch_array($order);
		$diseases_comorbidities_id2 = $result['diseases_comorbidities_id'];
		if($diseases_comorbidities_id2<>0)
		{
			$order = get_nonrheumatic_disease($pat_id,$diseases_comorbidities_id2);
			$num_rows = pg_num_rows($order);
			if($num_rows==0)
			{
				$k++;
				$order2 = get_diseasecomorbiditie($diseases_comorbidities_id2);
				$result2 = pg_fetch_array($order2);
				$diseases_organs_id = $result2['diseases_organs_id'];
			
				$nonrheumatic_diseases_parameters[0]=$diseases_organs_id;
				$nonrheumatic_diseases_parameters[1]=$diseases_comorbidities_id2;
				$nonrheumatic_diseases_parameters[2]=date_for_postgres($_REQUEST['start_date']);
				$nonrheumatic_diseases_parameters[3]=0;
				$nonrheumatic_diseases_parameters[4]="";
				$nonrheumatic_diseases_parameters[5]=$user_id;
				$nonrheumatic_diseases_parameters[6]='now()';
				$nonrheumatic_diseases_parameters[7]=$pat_id;
				$nonrheumatic_diseases_parameters[8]=0;
				$nonrheumatic_diseases_table_names[0]='diseases_organs_id';
				$nonrheumatic_diseases_table_names[1]='diseases_comorbidities_id';
				$nonrheumatic_diseases_table_names[2]='date_of_diagnosis';
				$nonrheumatic_diseases_table_names[3]='onset';
				$nonrheumatic_diseases_table_names[4]='comments';
				$nonrheumatic_diseases_table_names[5]='editor_id';
				$nonrheumatic_diseases_table_names[6]='edit_date';
				$nonrheumatic_diseases_table_names[7]='pat_id';
				$nonrheumatic_diseases_table_names[8]='deleted';
				$id=insert('nonrheumatic_diseases',$nonrheumatic_diseases_table_names,$nonrheumatic_diseases_parameters,'nonrheumatic_diseases_id');
				if($id!='')
					$j++;
			}
		}
		
		$order = get_medra_val($description_lvl_3);
		$result = pg_fetch_array($order);
		$diseases_comorbidities_id3 = $result['diseases_comorbidities_id'];
		if($diseases_comorbidities_id3<>0)
		{
			$order = get_nonrheumatic_disease($pat_id,$diseases_comorbidities_id3);
			$num_rows = pg_num_rows($order);
			if($num_rows==0)
			{
				$k++;
				$order2 = get_diseasecomorbiditie($diseases_comorbidities_id3);
				$result2 = pg_fetch_array($order2);
				$diseases_organs_id = $result2['diseases_organs_id'];
			
				$nonrheumatic_diseases_parameters[0]=$diseases_organs_id;
				$nonrheumatic_diseases_parameters[1]=$diseases_comorbidities_id3;
				$nonrheumatic_diseases_parameters[2]=date_for_postgres($_REQUEST['start_date']);
				$nonrheumatic_diseases_parameters[3]=0;
				$nonrheumatic_diseases_parameters[4]="";
				$nonrheumatic_diseases_parameters[5]=$user_id;
				$nonrheumatic_diseases_parameters[6]='now()';
				$nonrheumatic_diseases_parameters[7]=$pat_id;
				$nonrheumatic_diseases_parameters[8]=0;
				$nonrheumatic_diseases_table_names[0]='diseases_organs_id';
				$nonrheumatic_diseases_table_names[1]='diseases_comorbidities_id';
				$nonrheumatic_diseases_table_names[2]='date_of_diagnosis';
				$nonrheumatic_diseases_table_names[3]='onset';
				$nonrheumatic_diseases_table_names[4]='comments';
				$nonrheumatic_diseases_table_names[5]='editor_id';
				$nonrheumatic_diseases_table_names[6]='edit_date';
				$nonrheumatic_diseases_table_names[7]='pat_id';
				$nonrheumatic_diseases_table_names[8]='deleted';
				$id=insert('nonrheumatic_diseases',$nonrheumatic_diseases_table_names,$nonrheumatic_diseases_parameters,'nonrheumatic_diseases_id');
				if($id!='')
					$j++;
			}
		}
		
		$order = get_medra_val($description_lvl_4);
		$result = pg_fetch_array($order);
		$diseases_comorbidities_id4 = $result['diseases_comorbidities_id'];
		if($diseases_comorbidities_id4<>0)
		{
			$order = get_nonrheumatic_disease($pat_id,$diseases_comorbidities_id4);
			$num_rows = pg_num_rows($order);
			if($num_rows==0)
			{
				$k++;
				$order2 = get_diseasecomorbiditie($diseases_comorbidities_id4);
				$result2 = pg_fetch_array($order2);
				$diseases_organs_id = $result2['diseases_organs_id'];
			
				$nonrheumatic_diseases_parameters[0]=$diseases_organs_id;
				$nonrheumatic_diseases_parameters[1]=$diseases_comorbidities_id4;
				$nonrheumatic_diseases_parameters[2]=date_for_postgres($_REQUEST['start_date']);
				$nonrheumatic_diseases_parameters[3]=0;
				$nonrheumatic_diseases_parameters[4]="";
				$nonrheumatic_diseases_parameters[5]=$user_id;
				$nonrheumatic_diseases_parameters[6]='now()';
				$nonrheumatic_diseases_parameters[7]=$pat_id;
				$nonrheumatic_diseases_parameters[8]=0;
				$nonrheumatic_diseases_table_names[0]='diseases_organs_id';
				$nonrheumatic_diseases_table_names[1]='diseases_comorbidities_id';
				$nonrheumatic_diseases_table_names[2]='date_of_diagnosis';
				$nonrheumatic_diseases_table_names[3]='onset';
				$nonrheumatic_diseases_table_names[4]='comments';
				$nonrheumatic_diseases_table_names[5]='editor_id';
				$nonrheumatic_diseases_table_names[6]='edit_date';
				$nonrheumatic_diseases_table_names[7]='pat_id';
				$nonrheumatic_diseases_table_names[8]='deleted';
				$id=insert('nonrheumatic_diseases',$nonrheumatic_diseases_table_names,$nonrheumatic_diseases_parameters,'nonrheumatic_diseases_id');
				if($id!='')
					$j++;
			}
		}
		
		if($j==$k)
			$msg2="ok";
		
		//echo "msg2:$msg2</br>";
		if ($msg=="ok" and $msg2=="ok")
		{
			$save_chk="ok";
			$_SESSION['patient_event_id']=$patient_event_id;
			pg_query("COMMIT") or die("Transaction commit failed\n");
		}
		else
		{
			$save_chk="nok";
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
	}
	catch(exception $e) {
		echo "e:".$e."</br>";
		echo "ROLLBACK";
		pg_query("ROLLBACK") or die("Transaction rollback failed\n");
	}
	
}
?>
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <style>

.tooltip2 {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
}

.tooltip2 .tooltiptext {
    visibility: hidden;
    width: 300px;
    background-color: black;
    color: #fff;
    text-align: left;
    border-radius: 6px;
    padding: 5px 0;
	font-weight: normal;

    /* Position the tooltip */
    position: absolute;
    z-index: 1;
}

.tooltip2:hover .tooltiptext {
    visibility: visible;
}
  </style>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
	   <h1>
          <?php if($patient_event_id=="") { echo "New adverse event "; } else { echo "Edit adverse event "; } ?>
        </h1>
		<?php
				if($patient_event_id!="")
				{
					$exec = get_event($patient_event_id);
					$result = pg_fetch_array($exec);
					
					
					$event_start_date=$result['start_date_str'];
					if($event_start_date=="12-12-1900")
						$event_start_date="";
					
					$followupdate_str=$result['followupdate_str'];
					if($followupdate_str=="12-12-1900")
						$followupdate_str="";
					$cohort_name_str=$result['cohort_name_str'];
					
					$hospitalization=$result['hospitalization'];
					$hospitalization_duration=$result['hospitalization_duration'];
					$main_organ=$result['main_organ'];
					$secondary_organ=$result['secondary_organ'];
					$description_lvl_1=$result['description_lvl_1'];
					$description_lvl_2=$result['description_lvl_2'];
					$description_lvl_3=$result['description_lvl_3'];
					$description_lvl_4=$result['description_lvl_4'];
					$ctcae_lvl_1=$result['ctcae_lvl_1'];
					$ctcae_lvl_2=$result['ctcae_lvl_2'];
					$ctcae_lvl_3=$result['ctcae_lvl_3'];
					$ctcae_lvl_4=$result['ctcae_lvl_4'];
					$description=$result['description'];
					$relationship=$result['relationship'];
					$outcome=$result['outcome'];
					$change_therapy=$result['change_therapy'];
					$seriousness=$result['seriousness'];
					
					if($description_lvl_1<>"")
					{
						$order = get_medra_val($description_lvl_1);
						$result = pg_fetch_assoc($order);
						$description_soc_lvl_1=$result['medrasoc_id'];
						$grade1_1 =$result['grade1'];
						$grade1_2 =$result['grade2'];
						$grade1_3 =$result['grade3'];
						$grade1_4 =$result['grade4'];
						$grade1_5 =$result['grade5'];
					}
					if($description_lvl_2<>"")
					{
						$order = get_medra_val($description_lvl_2);
						$result = pg_fetch_assoc($order);
						$description_soc_lvl_2=$result['medrasoc_id'];
						$grade2_1 =$result['grade1'];
						$grade2_2 =$result['grade2'];
						$grade2_3 =$result['grade3'];
						$grade2_4 =$result['grade4'];
						$grade2_5 =$result['grade5'];
					}
					if($description_lvl_3<>"")
					{
						$order = get_medra_val($description_lvl_3);
						$result = pg_fetch_assoc($order);
						$description_soc_lvl_3=$result['medrasoc_id'];
						$grade3_1 =$result['grade1'];
						$grade3_2 =$result['grade2'];
						$grade3_3 =$result['grade3'];
						$grade3_4 =$result['grade4'];
						$grade3_5 =$result['grade5'];
					}
					if($description_lvl_4<>"")
					{
						$order = get_medra_val($description_lvl_4);
						$result = pg_fetch_assoc($order);
						$description_soc_lvl_4=$result['medrasoc_id'];
						$grade4_1 =$result['grade1'];
						$grade4_2 =$result['grade2'];
						$grade4_3 =$result['grade3'];
						$grade4_4 =$result['grade4'];
						$grade4_5 =$result['grade5'];
					}
					
					$exec = get_patient_demographics($pat_id);
				$result = pg_fetch_array($exec);
				$dateofinclusion=$result['dateofinclusion_str'];
				if($dateofinclusion=="12-12-1900")
					$dateofinclusion="";
				$dateofbirth=$result['dateofbirth_str'];
				if($dateofbirth=="12-12-1900")
					$dateofbirth="";
				$gender=$result['gender'];
				if($gender==1)
					$gender_str="Female";
				else if($gender==2)
					$gender_str="Male";

				$order = get_cohort($patient_cohort_id);
				$result = pg_fetch_assoc($order);
				$cohort_name_str=$result['cohort_name_str'];
				$coh_start_date=$result['start_date_str'];
				if($coh_start_date=="12-12-1900")
					$coh_start_date="";
				$coh_stop_date=$result['stop_date_str'];
				if($coh_stop_date=="12-12-1900")
					$coh_stop_date="-";
					
				}
				else
				{
					$exec = get_patient_demographics($pat_id);
				$result = pg_fetch_array($exec);
				$dateofinclusion=$result['dateofinclusion_str'];
				if($dateofinclusion=="12-12-1900")
					$dateofinclusion="";
				$dateofbirth=$result['dateofbirth_str'];
				if($dateofbirth=="12-12-1900")
					$dateofbirth="";
				$gender=$result['gender'];
				if($gender==1)
					$gender_str="Female";
				else if($gender==2)
					$gender_str="Male";
				$patient_id=$result['patient_id'];
				
				$order = get_cohort($patient_cohort_id);
				$result = pg_fetch_assoc($order);
				$cohort_name_str=$result['cohort_name_str'];
				$coh_start_date=$result['start_date_str'];
				if($coh_start_date=="12-12-1900")
					$coh_start_date="";
				$coh_stop_date=$result['stop_date_str'];
				if($coh_stop_date=="12-12-1900")
					$coh_stop_date="-";
				
					$order = get_followup($patient_followup_id);
					$result = pg_fetch_assoc($order);
					$cohort_name_str=$result['cohort_name_str'];
					$followupdate_str=$result['start_date_str'];
				}
				
				echo "<h1><small>Patient:".$patient_id.",".$gender_str." (".$dateofbirth.") </small></br>";
				echo "<small>Cohort:".$cohort_name_str.", start date:".$coh_start_date." end date:".$coh_stop_date."</small></br>";
				echo "<small>Follow up:".$followupdate_str."</small></h1>";
			?>
       
      </section>

      <!-- Main content -->
      <section class="content">
	  <div class="alert alert_suc alert-success" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Successful save!</strong>
	  </div>
	  <div class="alert alert_wr alert-danger" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Unsuccessful save!</strong>
	  </div>
	  <form id="form" name="form" action="adverse_edit.php" method="POST">
	  <input type="hidden" id="save" name="save" value="1">
	  <input type="hidden" id="pat_id" name="pat_id" value="<?php echo $pat_id;?>">
		<input type="hidden" id="patient_cohort_id" name="patient_cohort_id" value="<?php echo $patient_cohort_id;?>">
		<input type="hidden" id="patient_followup_id" name="patient_followup_id" value="<?php echo $patient_followup_id;?>">
		<input type="hidden" id="patient_event_id" name="patient_event_id" value="<?php echo $patient_event_id;?>">
       		<?php
				$exec = get_cohort($patient_cohort_id);
				$result = pg_fetch_array($exec);
				$start_date=$result['start_date_str'];
				if($start_date=="12-12-1900")
						$start_date="";
			?>
			<input type="hidden" class="form-control" id="cohort_start_date" name="cohort_start_date" value="<?php echo $start_date; ?>">
			<input type="hidden" class="form-control" id="followup_start_date" name="followup_start_date" value="<?php echo $followupdate_str; ?>">
			<div class="row">
			<div class="form-group col-md-12">
				<input type="submit" class="btn btn-primary" value="Save">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<!-- event div start -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Adverse event generic</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div  class="form-group col-md-3" id="divstartdate" name="divstartdate">
								<label>*Adverse event date</label>
								<div class="input-group date">
								  <div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								  </div>
								  <!--<input type="text" class="form-control" id="start_date" name="start_date" value="<?php //echo $event_start_date; ?>" readonly>-->
								  <input type="text" class="form-control" id="start_date" name="start_date" value="<?php echo $event_start_date; ?>">
								</div>
								<!-- /.input group -->
							  </div>
							  <div class="form-group col-md-3" id="divhospitalization" name="divhospitalization">
								<label>Health care utilization</label>
								  <select class="form-control" name="hospitalization" id="hospitalization">
									<option value="0">--</option>
									<option value="1" <?php if($hospitalization==1) { echo "selected"; } ?>>None</option>
									<option value="2" <?php if($hospitalization==2) { echo "selected"; } ?>>Visit to physician</option>
									<option value="3" <?php if($hospitalization==3) { echo "selected"; } ?>>Day care</option>
									<option value="4" <?php if($hospitalization==4) { echo "selected"; } ?>>Hospitalization</option>
								  </select>
								  <div id="divhospitalizationdays" name="divhospitalizationdays" <?php if($hospitalization=="4") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
									days:<input type="text" class="form-control" id="hospitalization_duration" name="hospitalization_duration" value="<?php echo $hospitalization_duration; ?>">
								  </div>
							</div>
							  <div class="form-group col-md-3" id="divmain_organ" name="divmain_organ">
								<label>Main Organ/System</label>
								  <select class="form-control" name="main_organ" id="main_organ">
									<option value="0">--</option>
									<?php
										$sql = get_lookup_tbl_values("organ_system");
										$numrows = pg_num_rows($sql);
										while($result2 = pg_fetch_array($sql))
										{
											$main_organ_id=$result2['id'];
											$value =$result2['value'];
									?>
									<option value="<?php echo $main_organ_id; ?>" <?php if($main_organ_id==$main_organ) { echo "selected"; } ?>><?php echo $value; ?></option>
									<?php
										}
									?>
								  </select>
							</div>
							<div class="form-group col-md-3" id="divsecondary_organ" name="divsecondary_organ">
								<label>Secondary Organ/System</label>
								  <select class="form-control" name="secondary_organ" id="secondary_organ">
									<option value="0">--</option>
									<?php
										$sql = get_lookup_tbl_values("organ_system");
										$numrows = pg_num_rows($sql);
										while($result2 = pg_fetch_array($sql))
										{
											$secondary_organ_id=$result2['id'];
											$value =$result2['value'];
									?>
									<option value="<?php echo $secondary_organ_id; ?>" <?php if($secondary_organ_id==$secondary_organ) { echo "selected"; } ?>><?php echo $value; ?></option>
									<?php
										}
									?>
								  </select>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3">
								<div class="form-group">
									<label>Relationship to main therapy</label>
									  <select class="form-control" name="relationship" id="relationship">
										<option value="0">--</option>
										<option value="1" <?php if($relationship==1) { echo "selected"; } ?>>Not judgeable</option>
										<option value="2" <?php if($relationship==2) { echo "selected"; } ?>>Unlikely</option>
										<option value="3" <?php if($relationship==3) { echo "selected"; } ?>>Possible</option>
										<option value="4" <?php if($relationship==4) { echo "selected"; } ?>>Probable</option>
										<option value="5" <?php if($relationship==5) { echo "selected"; } ?>>Definite</option>
									  </select>
								</div>
							</div>
							<div class="form-group col-md-3">
								<div class="form-group">
									<label>Outcome</label>
									  <select class="form-control" name="outcome" id="outcome">
										<option value="0">--</option>
										<option value="1" <?php if($outcome==1) { echo "selected"; } ?>>Unknown</option>
										<option value="2" <?php if($outcome==2) { echo "selected"; } ?>>Healthy without consequences</option>
										<option value="3" <?php if($outcome==3) { echo "selected"; } ?>>Healthy with consequences</option>
										<option value="4" <?php if($outcome==4) { echo "selected"; } ?>>Still unhealthy</option>
										<option value="5" <?php if($outcome==5) { echo "selected"; } ?>>Lethal</option>
									  </select>
								</div>
							</div>
							<div class="form-group col-md-3">
								<div class="form-group">
									<label>Seriousness</label>
									  <select class="form-control" name="seriousness" id="seriousness">
										<option value="0">--</option>
										<option value="1" <?php if($seriousness==1) { echo "selected"; } ?>>Not graded</option>
										<option value="2" <?php if($seriousness==2) { echo "selected"; } ?>>Mild</option>
										<option value="3" <?php if($seriousness==3) { echo "selected"; } ?>>Moderate</option>
										<option value="4" <?php if($seriousness==4) { echo "selected"; } ?>>Serious</option>
										<option value="5" <?php if($seriousness==5) { echo "selected"; } ?>>Life-threatening</option>
										<option value="6" <?php if($seriousness==6) { echo "selected"; } ?>>Lethal</option>
									  </select>
								</div>
							</div>
							<div class="form-group col-md-3">
								<div class="form-group">
									<label>Change of main therapy due to adverse event</label>
									  <select class="form-control" name="change_therapy" id="change_therapy">
										<option value="0">--</option>
										<option value="1" <?php if($change_therapy==1) { echo "selected"; } ?>>None</option>
										<option value="2" <?php if($change_therapy==2) { echo "selected"; } ?>>Dosage adjustment</option>
										<option value="3" <?php if($change_therapy==3) { echo "selected"; } ?>>Temporary stop</option>
										<option value="4" <?php if($change_therapy==4) { echo "selected"; } ?>>Permanent stop</option>
									  </select>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<!-- event div start -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">MedDRA</h3>
					</div>
					<div class="box-body">
						<?php
								$sql = get_medra();
								$medra_counter=0;
												
								while($result2 = pg_fetch_array($sql))
								{	
									$medra_ids .= $result2['medra_id'];
									$medra_ids .= "!@#$%^" ;
													
									$medra_descriptions .=$result2['description'];
									$medra_descriptions .= "!@#$%^" ;
									
									$medra_codes .=$result2['code'];
									$medra_codes .= "!@#$%^";
									
									$medrasoc_ids .=$result2['medrasoc_id'];
									$medrasoc_ids .= "!@#$%^";
													
									$grade1s .=$result2['grade1'];
									$grade1s .= "!@#$%^";				
									$grade2s .=$result2['grade2'];
									$grade2s .= "!@#$%^";				
									$grade3s .=$result2['grade3'];
									$grade3s .= "!@#$%^";				
									$grade4s .=$result2['grade4'];
									$grade4s .= "!@#$%^";				
									$grade5s .=$result2['grade5'];
									$grade5s .= "!@#$%^";				
									
									$medra_counter++;
								}
							?>
												<input type="hidden" value="<?php echo $medra_counter; ?>" id="medra_counter" name="medra_counter"/>
												<input type="hidden" value="<?php echo $medra_ids; ?>" id="medra_ids" name="medra_ids"/>
												<input type="hidden" value="<?php echo $medra_descriptions; ?>" id="medra_descriptions" name="medra_descriptions"/>
												<input type="hidden" value="<?php echo $medra_codes; ?>" id="medra_codes" name="medra_codes"/>
												<input type="hidden" value="<?php echo $medrasoc_ids; ?>" id="medrasoc_ids" name="medrasoc_ids"/>
												<input type="hidden" value="<?php echo $grade1s; ?>" id="grade1s" name="grade1s"/>
												<input type="hidden" value="<?php echo $grade2s; ?>" id="grade2s" name="grade2s"/>
												<input type="hidden" value="<?php echo $grade3s; ?>" id="grade3s" name="grade3s"/>
												<input type="hidden" value="<?php echo $grade4s; ?>" id="grade4s" name="grade4s"/>
												<input type="hidden" value="<?php echo $grade5s; ?>" id="grade5s" name="grade5s"/>
						<div class="row">
							<div class="form-group col-md-12">
								<div class="form-group">
									<label>Description: </label>
									<textarea rows="3" cols="60" id="description" name="description"><?php echo $description; ?></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3">
							 <label>Description of event_Level 1</label>
								<div class="form-group">
									<label>MedDRA SOC</label>
									  <select class="form-control" name="description_soc_lvl_1" id="description_soc_lvl_1">
										<option value="0" <?php if($description_soc_lvl_1=="") { echo "selected"; }?>>--</option>
										<?php
											$sql = get_medrasoc();
											$numrows = pg_num_rows($sql);
											while($result2 = pg_fetch_array($sql))
											{
												$medrasoc_id=$result2['medrasoc_id'];
												$value =$result2['value'];
										?>
										<option value="<?php echo $medrasoc_id; ?>" <?php if($medrasoc_id==$description_soc_lvl_1) { echo "selected"; } ?>><?php echo $value; ?></option>
										<?php
											}
										?>
									  </select>
								</div>
							</div>
							<div class="form-group col-md-3">
							 <label>Description of event_Level 2</label>
								<div class="form-group">
									<label>MedDRA SOC</label>
									  <select class="form-control" name="description_soc_lvl_2" id="description_soc_lvl_2">
										<option value="0" <?php if($description_soc_lvl_2=="") { echo "selected"; }?>>--</option>
										<?php
											$sql = get_medrasoc();
											$numrows = pg_num_rows($sql);
											while($result2 = pg_fetch_array($sql))
											{
												$medrasoc_id=$result2['medrasoc_id'];
												$value =$result2['value'];
										?>
										<option value="<?php echo $medrasoc_id; ?>" <?php if($medrasoc_id==$description_soc_lvl_2) { echo "selected"; } ?>><?php echo $value; ?></option>
										<?php
											}
										?>
									  </select>
								</div>
							</div>
							<div class="form-group col-md-3">
							 <label>Description of event_Level 3</label>
								<div class="form-group">
									<label>MedDRA SOC</label>
									  <select class="form-control" name="description_soc_lvl_3" id="description_soc_lvl_3">
										<option value="0" <?php if($description_soc_lvl_3=="") { echo "selected"; }?>>--</option>
										<?php
											$sql = get_medrasoc();
											$numrows = pg_num_rows($sql);
											while($result2 = pg_fetch_array($sql))
											{
												$medrasoc_id=$result2['medrasoc_id'];
												$value =$result2['value'];
										?>
										<option value="<?php echo $medrasoc_id; ?>" <?php if($medrasoc_id==$description_soc_lvl_3) { echo "selected"; } ?>><?php echo $value; ?></option>
										<?php
											}
										?>
									  </select>
								</div>
							</div>
							<div class="form-group col-md-3">
							 <label>Description of event_Level 4</label>
								<div class="form-group">
									<label>MedDRA SOC</label>
									  <select class="form-control" name="description_soc_lvl_4" id="description_soc_lvl_4">
										<option value="0" <?php if($description_soc_lvl_4=="") { echo "selected"; }?>>--</option>
										<?php
											$sql = get_medrasoc();
											$numrows = pg_num_rows($sql);
											while($result2 = pg_fetch_array($sql))
											{
												$medrasoc_id=$result2['medrasoc_id'];
												$value =$result2['value'];
										?>
										<option value="<?php echo $medrasoc_id; ?>" <?php if($medrasoc_id==$description_soc_lvl_4) { echo "selected"; } ?>><?php echo $value; ?></option>
										<?php
											}
										?>
									  </select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3">
								<div class="form-group">
								<?php
											if($description_lvl_1<>"")
											{
												$sql = get_medra_specific_medrasoc($description_soc_lvl_1);
												$numrows = pg_num_rows($sql);
												while($result2 = pg_fetch_array($sql))
												{
													if($medra_id==$description_lvl_1) 
													{
														$grade1_1 =$result2['grade1'];
														$grade1_2 =$result2['grade2'];
														$grade1_3 =$result2['grade3'];
														$grade1_4 =$result2['grade4'];
														$grade1_5 =$result2['grade5'];
													} 
												}
											}
										?>
									<label>MedDRA Term <div class="tooltip2">(info)<span class="tooltiptext" id="info1" name="info1" ><?php echo '<b>grade1:</b> '.$grade1_1.'</br><b>grade2:</b> '.$grade1_2.'</br><b> grade3:</b> '.$grade1_3.'</br><b> grade4:</b> '.$grade1_4.'</br><b> grade5:</b> '.$grade1_5; ?></span></div></label>
									  <select class="form-control" name="description_lvl_1" id="description_lvl_1">
										<option value="0">--</option>
										<?php
											if($description_lvl_1<>"")
											{
												$sql = get_medra_specific_medrasoc($description_soc_lvl_1);
												$numrows = pg_num_rows($sql);
												while($result2 = pg_fetch_array($sql))
												{
													$medra_id=$result2['medra_id'];
													$description =$result2['description'];
													
											?>
											<option value="<?php echo $medra_id; ?>" <?php if($medra_id==$description_lvl_1) { echo "selected"; } ?>><?php echo $description; ?></option>
											<?php
												}
											}
										?>
									  </select>
								</div>
							</div>
							<div class="form-group col-md-3">
								<div class="form-group">
									<?php
											if($description_lvl_2<>"")
											{
												$sql = get_medra_specific_medrasoc($description_soc_lvl_2);
												$numrows = pg_num_rows($sql);
												while($result2 = pg_fetch_array($sql))
												{
													if($medra_id==$description_lvl_2) 
													{
														$grade2_1 =$result2['grade1'];
														$grade2_2 =$result2['grade2'];
														$grade2_3 =$result2['grade3'];
														$grade2_4 =$result2['grade4'];
														$grade2_5 =$result2['grade5'];
													} 
												}
											}
										?>
									<label>MedDRA Term <div class="tooltip2">(info)<span class="tooltiptext" id="info2" name="info2" ><?php echo '<b>grade1:</b> '.$grade2_1.'</br><b>grade2:</b> '.$grade2_2.'</br><b> grade3:</b> '.$grade2_3.'</br><b> grade4:</b> '.$grade2_4.'</br><b> grade5:</b> '.$grade2_5; ?></span></div></label>
									  <select class="form-control" name="description_lvl_2" id="description_lvl_2">
										<option value="0">--</option>
										<?php
											if($description_lvl_2<>"")
											{
												$sql = get_medra_specific_medrasoc($description_soc_lvl_2);
												$numrows = pg_num_rows($sql);
												while($result2 = pg_fetch_array($sql))
												{
													$medra_id=$result2['medra_id'];
													$description =$result2['description'];
											?>
											<option value="<?php echo $medra_id; ?>" <?php if($medra_id==$description_lvl_2) { echo "selected"; } ?>><?php echo $description; ?></option>
											<?php
												}
											}
										?>
									  </select>
								</div>
							</div>
							<div class="form-group col-md-3">
								<div class="form-group">
									<?php
											if($description_lvl_3<>"")
											{
												$sql = get_medra_specific_medrasoc($description_soc_lvl_3);
												$numrows = pg_num_rows($sql);
												while($result2 = pg_fetch_array($sql))
												{
													if($medra_id==$description_lvl_3) 
													{
														$grade3_1 =$result2['grade1'];
														$grade3_2 =$result2['grade2'];
														$grade3_3 =$result2['grade3'];
														$grade3_4 =$result2['grade4'];
														$grade3_5 =$result2['grade5'];
													} 
												}
											}
										?>
									<label>MedDRA Term <div class="tooltip2">(info)<span class="tooltiptext" id="info3" name="info3" ><?php echo '<b>grade1:</b> '.$grade3_1.'</br><b>grade2:</b> '.$grade3_2.'</br><b> grade3:</b> '.$grade3_3.'</br><b> grade4:</b> '.$grade3_4.'</br><b> grade5:</b> '.$grade3_5; ?></span></div></label>
									  <select class="form-control" name="description_lvl_3" id="description_lvl_3">
										<option value="0">--</option>
										<?php
											if($description_lvl_3<>"")
											{
												$sql = get_medra_specific_medrasoc($description_soc_lvl_3);
												$numrows = pg_num_rows($sql);
												while($result2 = pg_fetch_array($sql))
												{
													$medra_id=$result2['medra_id'];
													$description =$result2['description'];
											?>
											<option value="<?php echo $medra_id; ?>" <?php if($medra_id==$description_lvl_3) { echo "selected"; } ?>><?php echo $description; ?></option>
											<?php
												}
											}
										?>
									  </select>
								</div>
							</div>
							<div class="form-group col-md-3">
								<div class="form-group">
									<?php
											if($description_lvl_4<>"")
											{
												$sql = get_medra_specific_medrasoc($description_soc_lvl_4);
												$numrows = pg_num_rows($sql);
												while($result2 = pg_fetch_array($sql))
												{
													if($medra_id==$description_lvl_4) 
													{
														$grade4_1 =$result2['grade1'];
														$grade4_2 =$result2['grade2'];
														$grade4_3 =$result2['grade3'];
														$grade4_4 =$result2['grade4'];
														$grade4_5 =$result2['grade5'];
													} 
												}
											}
										?>
									<label>MedDRA Term <div class="tooltip2">(info)<span class="tooltiptext" id="info4" name="info4" ><?php echo '<b>grade1:</b> '.$grade4_1.'</br><b>grade2:</b> '.$grade4_2.'</br><b> grade3:</b> '.$grade4_3.'</br><b> grade4:</b> '.$grade4_4.'</br><b> grade5:</b> '.$grade4_5; ?></span></div></label>
									  <select class="form-control" name="description_lvl_4" id="description_lvl_4">
										<option value="0">--</option>
										<?php
											if($description_lvl_4<>"")
											{
												$sql = get_medra_specific_medrasoc($description_soc_lvl_4);
												$numrows = pg_num_rows($sql);
												while($result2 = pg_fetch_array($sql))
												{
													$medra_id=$result2['medra_id'];
													$description =$result2['description'];
											?>
											<option value="<?php echo $medra_id; ?>" <?php if($medra_id==$description_lvl_4) { echo "selected"; } ?>><?php echo $description; ?></option>
											<?php
												}
											}
										?>
									  </select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3">
								<div class="form-group">
									<label>CTCAE Severity Grading</label>
									  <select class="form-control" name="ctcae_lvl_1" id="ctcae_lvl_1">
										<option value="0">--</option>
										<option value="6" <?php if($ctcae_lvl_1==6) { echo "selected"; } ?>>Not graded</option>
										<option value="5" <?php if($ctcae_lvl_1==5) { echo "selected"; } ?>>Grade1</option>
										<option value="4" <?php if($ctcae_lvl_1==4) { echo "selected"; } ?>>Grade2</option>
										<option value="3" <?php if($ctcae_lvl_1==3) { echo "selected"; } ?>>Grade3</option>
										<option value="2" <?php if($ctcae_lvl_1==2) { echo "selected"; } ?>>Grade4</option>
										<option value="1" <?php if($ctcae_lvl_1==1) { echo "selected"; } ?>>Grade5</option>
									  </select>
								</div>
							</div>
							<div class="form-group col-md-3">
								<div class="form-group">
									<label>CTCAE Severity Grading</label>
									  <select class="form-control" name="ctcae_lvl_2" id="ctcae_lvl_2">
										<option value="0">--</option>
										<option value="6" <?php if($ctcae_lvl_2==6) { echo "selected"; } ?>>Not graded</option>
										<option value="5" <?php if($ctcae_lvl_2==5) { echo "selected"; } ?>>Grade1</option>
										<option value="4" <?php if($ctcae_lvl_2==4) { echo "selected"; } ?>>Grade2</option>
										<option value="3" <?php if($ctcae_lvl_2==3) { echo "selected"; } ?>>Grade3</option>
										<option value="2" <?php if($ctcae_lvl_2==2) { echo "selected"; } ?>>Grade4</option>
										<option value="1" <?php if($ctcae_lvl_2==1) { echo "selected"; } ?>>Grade5</option>
									  </select>
								</div>
							</div>
							<div class="form-group col-md-3">
								<div class="form-group">
									<label>CTCAE Severity Grading</label>
									  <select class="form-control" name="ctcae_lvl_3" id="ctcae_lvl_3">
										<option value="0">--</option>
										<option value="6" <?php if($ctcae_lvl_3==6) { echo "selected"; } ?>>Not graded</option>
										<option value="5" <?php if($ctcae_lvl_3==5) { echo "selected"; } ?>>Grade1</option>
										<option value="4" <?php if($ctcae_lvl_3==4) { echo "selected"; } ?>>Grade2</option>
										<option value="3" <?php if($ctcae_lvl_3==3) { echo "selected"; } ?>>Grade3</option>
										<option value="2" <?php if($ctcae_lvl_3==2) { echo "selected"; } ?>>Grade4</option>
										<option value="1" <?php if($ctcae_lvl_3==1) { echo "selected"; } ?>>Grade5</option>
									  </select>
								</div>
							</div>
							<div class="form-group col-md-3">
								<div class="form-group">
									<label>CTCAE Severity Grading</label>
									  <select class="form-control" name="ctcae_lvl_4" id="ctcae_lvl_4">
										<option value="0">--</option>
										<option value="6" <?php if($ctcae_lvl_4==6) { echo "selected"; } ?>>Not graded</option>
										<option value="5" <?php if($ctcae_lvl_4==5) { echo "selected"; } ?>>Grade1</option>
										<option value="4" <?php if($ctcae_lvl_4==4) { echo "selected"; } ?>>Grade2</option>
										<option value="3" <?php if($ctcae_lvl_4==3) { echo "selected"; } ?>>Grade3</option>
										<option value="2" <?php if($ctcae_lvl_4==2) { echo "selected"; } ?>>Grade4</option>
										<option value="1" <?php if($ctcae_lvl_4==1) { echo "selected"; } ?>>Grade5</option>
									  </select>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- event div end -->
				
				
				<div class="row">
					<div class="form-group col-md-12">
						<input type="submit" class="btn btn-primary" value="Save">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						&nbsp;
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						&nbsp;
					</div>
				</div>
			</div>
		</div>
		</form>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
   <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>
/*$('#start_date').datepicker({
	  autoclose: true,
	  dateFormat: 'dd-mm-yy',
	  yearRange: "-100:+100",
	 default: "0:0",
	 changeMonth: true,
	changeYear: true
	})
*/

$('#start_date').inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });

$("#hospitalization").change(function(){
     var hospitalization_val = $(this).val();
	 if(hospitalization_val==4)
		 $("#divhospitalizationdays").show();
	 else
		 $("#divhospitalizationdays").hide();
    });

$("#description_lvl_1").change(function(){
     var description_lvl_1 = $(this).val();
	 
	var medra_ids=document.getElementById('medra_ids').value;
	var medra_id=medra_ids.split('!@#$%^');
	var grade1s=document.getElementById('grade1s').value;
	var grade1=grade1s.split('!@#$%^');
	var grade2s=document.getElementById('grade2s').value;
	var grade2=grade2s.split('!@#$%^');
	var grade3s=document.getElementById('grade3s').value;
	var grade3=grade3s.split('!@#$%^');
	var grade4s=document.getElementById('grade4s').value;
	var grade4=grade4s.split('!@#$%^');
	var grade5s=document.getElementById('grade5s').value;
	var grade5=grade5s.split('!@#$%^');
	var medra_counter=document.getElementById('medra_counter').value;
	
	
	for(i = 0; i < medra_counter; i++)
	{	
		if(description_lvl_1==medra_id[i])
		{
			/*$("#grade1_1").html(grade1[i]);
			$("#grade1_2").html(grade2[i]);
			$("#grade1_3").html(grade3[i]);
			$("#grade1_4").html(grade4[i]);
			$("#grade1_5").html(grade5[i]);*/
			document.getElementById('info1').innerHTML='<b>grade1:</b> '+grade1[i]+' '+'</br><b>grade2:</b> '+grade2[i]+' '+'</br><b>grade3:</b> '+grade3[i]+' '+'</br><b>grade4:</b> '+grade4[i]+' '+'</br><b>grade5:</b> '+grade5[i];
		}
	}
	
	
    });

$("#description_lvl_2").change(function(){
     var description_lvl_2 = $(this).val();
	 
	var medra_ids=document.getElementById('medra_ids').value;
	var medra_id=medra_ids.split('!@#$%^');
	var grade1s=document.getElementById('grade1s').value;
	var grade1=grade1s.split('!@#$%^');
	var grade2s=document.getElementById('grade2s').value;
	var grade2=grade2s.split('!@#$%^');
	var grade3s=document.getElementById('grade3s').value;
	var grade3=grade3s.split('!@#$%^');
	var grade4s=document.getElementById('grade4s').value;
	var grade4=grade4s.split('!@#$%^');
	var grade5s=document.getElementById('grade5s').value;
	var grade5=grade5s.split('!@#$%^');
	var medra_counter=document.getElementById('medra_counter').value;
	
	
	for(i = 0; i < medra_counter; i++)
	{	
		if(description_lvl_2==medra_id[i])
		{
			/*$("#grade2_1").html(grade1[i]);
			$("#grade2_2").html(grade2[i]);
			$("#grade2_3").html(grade3[i]);
			$("#grade2_4").html(grade4[i]);
			$("#grade2_5").html(grade5[i]);*/
			document.getElementById('info2').innerHTML='<b>grade1:</b> '+grade1[i]+' '+'</br><b>grade2:</b> '+grade2[i]+' '+'</br><b>grade3:</b> '+grade3[i]+' '+'</br><b>grade4:</b> '+grade4[i]+' '+'</br><b>grade5:</b> '+grade5[i];
		}
	}
	
	
    });
	
$("#description_lvl_3").change(function(){
     var description_lvl_3 = $(this).val();
	 
	var medra_ids=document.getElementById('medra_ids').value;
	var medra_id=medra_ids.split('!@#$%^');
	var grade1s=document.getElementById('grade1s').value;
	var grade1=grade1s.split('!@#$%^');
	var grade2s=document.getElementById('grade2s').value;
	var grade2=grade2s.split('!@#$%^');
	var grade3s=document.getElementById('grade3s').value;
	var grade3=grade3s.split('!@#$%^');
	var grade4s=document.getElementById('grade4s').value;
	var grade4=grade4s.split('!@#$%^');
	var grade5s=document.getElementById('grade5s').value;
	var grade5=grade5s.split('!@#$%^');
	var medra_counter=document.getElementById('medra_counter').value;
	
	
	for(i = 0; i < medra_counter; i++)
	{	
		if(description_lvl_3==medra_id[i])
		{
			/*$("#grade3_1").html(grade1[i]);
			$("#grade3_2").html(grade2[i]);
			$("#grade3_3").html(grade3[i]);
			$("#grade3_4").html(grade4[i]);
			$("#grade3_5").html(grade5[i]);*/
			document.getElementById('info3').innerHTML='<b>grade1:</b> '+grade1[i]+' '+'</br><b>grade2:</b> '+grade2[i]+' '+'</br><b>grade3:</b> '+grade3[i]+' '+'</br><b>grade4:</b> '+grade4[i]+' '+'</br><b>grade5:</b> '+grade5[i];
		}
	}
	
	
    });
	
$("#description_lvl_4").change(function(){
     var description_lvl_4 = $(this).val();
	 
	var medra_ids=document.getElementById('medra_ids').value;
	var medra_id=medra_ids.split('!@#$%^');
	var grade1s=document.getElementById('grade1s').value;
	var grade1=grade1s.split('!@#$%^');
	var grade2s=document.getElementById('grade2s').value;
	var grade2=grade2s.split('!@#$%^');
	var grade3s=document.getElementById('grade3s').value;
	var grade3=grade3s.split('!@#$%^');
	var grade4s=document.getElementById('grade4s').value;
	var grade4=grade4s.split('!@#$%^');
	var grade5s=document.getElementById('grade5s').value;
	var grade5=grade5s.split('!@#$%^');
	var medra_counter=document.getElementById('medra_counter').value;
	
	
	for(i = 0; i < medra_counter; i++)
	{	
		if(description_lvl_4==medra_id[i])
		{
			/*$("#grade4_1").html(grade1[i]);
			$("#grade4_2").html(grade2[i]);
			$("#grade4_3").html(grade3[i]);
			$("#grade4_4").html(grade4[i]);
			$("#grade4_5").html(grade5[i]);*/
			document.getElementById('info4').innerHTML='<b>grade1:</b> '+grade1[i]+' '+'</br><b>grade2:</b> '+grade2[i]+' '+'</br><b>grade3:</b> '+grade3[i]+' '+'</br><b>grade4:</b> '+grade4[i]+' '+'</br><b>grade5:</b> '+grade5[i];
		}
	}
	
	
    });

$("#description_soc_lvl_1").change(function(){
     var description_soc_lvl_1_val = $(this).val();
	 
	var medra_ids=document.getElementById('medra_ids').value;
	var medra_id=medra_ids.split('!@#$%^');
	var medra_descriptions=document.getElementById('medra_descriptions').value;
	var medra_description=medra_descriptions.split('!@#$%^');
	var medra_codes=document.getElementById('medra_codes').value;
	var medra_code=medra_codes.split('!@#$%^');
	var medrasoc_ids=document.getElementById('medrasoc_ids').value;
	var medrasoc_id=medrasoc_ids.split('!@#$%^');
	var grade1s=document.getElementById('grade1s').value;
	var grade1=grade1s.split('!@#$%^');
	var grade2s=document.getElementById('grade2s').value;
	var grade2=grade2s.split('!@#$%^');
	var grade3s=document.getElementById('grade3s').value;
	var grade3=grade3s.split('!@#$%^');
	var grade4s=document.getElementById('grade4s').value;
	var grade4=grade4s.split('!@#$%^');
	var grade5s=document.getElementById('grade5s').value;
	var grade5=grade5s.split('!@#$%^');
	
	
	var medra_counter=document.getElementById('medra_counter').value;
	$("#description_lvl_1").empty();
	
	var description_lvl_1=document.getElementById('description_lvl_1');
	var opt=new Option('--',0);
	description_lvl_1.add(opt,undefined);
	for(i = 0; i < medra_counter; i++)
	{
		
		if(description_soc_lvl_1_val==medrasoc_id[i])
		{
			var opt=new Option(medra_description[i],medra_id[i]);
			description_lvl_1.add(opt,undefined);
		}
	}
	
	
    });

$("#description_soc_lvl_2").change(function(){
     var description_soc_lvl_2_val = $(this).val();
	 
	var medra_ids=document.getElementById('medra_ids').value;
	var medra_id=medra_ids.split('!@#$%^');
	var medra_descriptions=document.getElementById('medra_descriptions').value;
	var medra_description=medra_descriptions.split('!@#$%^');
	var medra_codes=document.getElementById('medra_codes').value;
	var medra_code=medra_codes.split('!@#$%^');
	var medrasoc_ids=document.getElementById('medrasoc_ids').value;
	var medrasoc_id=medrasoc_ids.split('!@#$%^');
	var grade1s=document.getElementById('grade1s').value;
	var grade1=grade1s.split('!@#$%^');
	var grade2s=document.getElementById('grade2s').value;
	var grade2=grade2s.split('!@#$%^');
	var grade3s=document.getElementById('grade3s').value;
	var grade3=grade3s.split('!@#$%^');
	var grade4s=document.getElementById('grade4s').value;
	var grade4=grade4s.split('!@#$%^');
	var grade5s=document.getElementById('grade5s').value;
	var grade5=grade5s.split('!@#$%^');
	
	
	var medra_counter=document.getElementById('medra_counter').value;
	$("#description_lvl_2").empty();
	
	var description_lvl_2=document.getElementById('description_lvl_2');
	var opt=new Option('--',0);
	description_lvl_2.add(opt,undefined);
	for(i = 0; i < medra_counter; i++)
	{
		
		if(description_soc_lvl_2_val==medrasoc_id[i])
		{
			var opt=new Option(medra_description[i],medra_id[i]);
			description_lvl_2.add(opt,undefined);
		}
	}
	
	
    });

$("#description_soc_lvl_3").change(function(){
     var description_soc_lvl_3 = $(this).val();
	 
	var medra_ids=document.getElementById('medra_ids').value;
	var medra_id=medra_ids.split('!@#$%^');
	var medra_descriptions=document.getElementById('medra_descriptions').value;
	var medra_description=medra_descriptions.split('!@#$%^');
	var medra_codes=document.getElementById('medra_codes').value;
	var medra_code=medra_codes.split('!@#$%^');
	var medrasoc_ids=document.getElementById('medrasoc_ids').value;
	var medrasoc_id=medrasoc_ids.split('!@#$%^');
	var grade1s=document.getElementById('grade1s').value;
	var grade1=grade1s.split('!@#$%^');
	var grade2s=document.getElementById('grade2s').value;
	var grade2=grade2s.split('!@#$%^');
	var grade3s=document.getElementById('grade3s').value;
	var grade3=grade3s.split('!@#$%^');
	var grade4s=document.getElementById('grade4s').value;
	var grade4=grade4s.split('!@#$%^');
	var grade5s=document.getElementById('grade5s').value;
	var grade5=grade5s.split('!@#$%^');
	
	
	var medra_counter=document.getElementById('medra_counter').value;
	$("#description_lvl_3").empty();
	
	var description_lvl_3=document.getElementById('description_lvl_3');
	var opt=new Option('--',0);
	description_lvl_3.add(opt,undefined);
	for(i = 0; i < medra_counter; i++)
	{
		
		if(description_soc_lvl_3==medrasoc_id[i])
		{
			var opt=new Option(medra_description[i],medra_id[i]);
			description_lvl_3.add(opt,undefined);
		}
	}
	
	
    });

$("#description_soc_lvl_4").change(function(){
     var description_soc_lvl_4 = $(this).val();
	 
	var medra_ids=document.getElementById('medra_ids').value;
	var medra_id=medra_ids.split('!@#$%^');
	var medra_descriptions=document.getElementById('medra_descriptions').value;
	var medra_description=medra_descriptions.split('!@#$%^');
	var medra_codes=document.getElementById('medra_codes').value;
	var medra_code=medra_codes.split('!@#$%^');
	var medrasoc_ids=document.getElementById('medrasoc_ids').value;
	var medrasoc_id=medrasoc_ids.split('!@#$%^');
	var grade1s=document.getElementById('grade1s').value;
	var grade1=grade1s.split('!@#$%^');
	var grade2s=document.getElementById('grade2s').value;
	var grade2=grade2s.split('!@#$%^');
	var grade3s=document.getElementById('grade3s').value;
	var grade3=grade3s.split('!@#$%^');
	var grade4s=document.getElementById('grade4s').value;
	var grade4=grade4s.split('!@#$%^');
	var grade5s=document.getElementById('grade5s').value;
	var grade5=grade5s.split('!@#$%^');
	
	
	var medra_counter=document.getElementById('medra_counter').value;
	$("#description_lvl_4").empty();
	
	var description_lvl_4=document.getElementById('description_lvl_4');
	var opt=new Option('--',0);
	description_lvl_4.add(opt,undefined);
	for(i = 0; i < medra_counter; i++)
	{
		
		if(description_soc_lvl_4==medrasoc_id[i])
		{
			var opt=new Option(medra_description[i],medra_id[i]);
			description_lvl_4.add(opt,undefined);
		}
	}
	
	
    });

$('#form').submit(function(){
		
	var msg="";
	var start_date2=$('#start_date').val();
	var start_date_unformat=$('#cohort_start_date').val();
	var start_date_split = start_date_unformat.split("-");
	var start_date= new Date(start_date_split[2]+"-"+start_date_split[1]+"-"+start_date_split[0]);
	
	var followup_startdate_unformat=document.getElementById('followup_start_date').value;
	var followup_startdate_split = followup_startdate_unformat.split("-");
	var followup_startdate= new Date(followup_startdate_split[2]+"-"+followup_startdate_split[1]+"-"+followup_startdate_split[0]);
	
	//if(cohort_startdate<start_date)
			//msg +='-Treatments start date must be greater than cohort start date ('+start_date_unformat+')!\n'; 
	
	if(start_date2=="")
	{
		document.getElementById('start_date').style.backgroundColor = "red";
		document.getElementById('start_date').style.color  = "white";
		msg +="-Fill adverse event date!\n";
	}
	else
	{
		document.getElementById('start_date').style.backgroundColor = "white";
		document.getElementById('start_date').style.color  = "black";
	}
		
	if(msg != '')
	{
		alert(msg);
		return false;
	}
	else
		return true;
	
}); 

</script>
<?php
	
if($save==1)
{
	if($save_chk=="ok")
	{
?>
<script>
$(".alert_suc").show();
window.setTimeout(function() {
    $(".alert_suc").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);

</script>
<?php
	}
	else
	{
?>
<script>
$(".alert_wr").show();
window.setTimeout(function() {
    $(".alert_wr").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
}
?>
</body>
</html>
