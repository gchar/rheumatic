﻿<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	
	$save=$_REQUEST['save'];
	$pat_id=$_REQUEST['pat_id'];
	$row=$_REQUEST['row'];
	$patient_neuropsychiatric_sle_id=$_REQUEST['patient_neuropsychiatric_sle_id'];
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>
</head>
<body class="hold-transition skin-blue layout-top-nav">
<?php
	if($save==1)
	{
		$patient_neuropsychiatric_parameters[0]=date_for_postgres($_REQUEST['patient_neuropsychiatric_sle_date']);
		$patient_neuropsychiatric_parameters[1]=$_REQUEST['patient_neuropsychiatric_sle'];
		$patient_neuropsychiatric_parameters[2]=$_REQUEST['neuroimaging'];
		$patient_neuropsychiatric_parameters[3]=$_REQUEST['white_matter'];
		$patient_neuropsychiatric_parameters[4]=$_REQUEST['grey_mater'];
		$patient_neuropsychiatric_parameters[5]=$_REQUEST['infarcts'];
		$patient_neuropsychiatric_parameters[6]=$_REQUEST['hemorrhagic'];
		$patient_neuropsychiatric_parameters[7]=$_REQUEST['demyelinating'];
		$patient_neuropsychiatric_parameters[8]=$_REQUEST['atrophy'];
		$patient_neuropsychiatric_parameters[9]=$_REQUEST['spine_mri'];
		$patient_neuropsychiatric_parameters[10]=$_REQUEST['csf_analysis'];
		$patient_neuropsychiatric_parameters[11]=$_REQUEST['increase_wbcs'];
		$patient_neuropsychiatric_parameters[12]=$_REQUEST['increase_proteint'];
		$patient_neuropsychiatric_parameters[13]=$_REQUEST['increase_igg_index'];
		$patient_neuropsychiatric_parameters[14]=$_REQUEST['increase_oligo_clonal_bands'];
		$patient_neuropsychiatric_parameters[15]=$_REQUEST['eeg_studies'];
		$patient_neuropsychiatric_parameters[16]=$_REQUEST['nerve_conduction'];
		$patient_neuropsychiatric_parameters[17]=$_REQUEST['risk_factor_1'];
		$patient_neuropsychiatric_parameters[18]=$_REQUEST['risk_factor_2'];
		$patient_neuropsychiatric_parameters[19]=$_REQUEST['risk_factor_3'];
		$patient_neuropsychiatric_parameters[20]=$_REQUEST['risk_factor_4'];
		$patient_neuropsychiatric_parameters[21]=$_REQUEST['risk_factor_5'];
		$patient_neuropsychiatric_parameters[22]=$_REQUEST['risk_factor_6'];
		$patient_neuropsychiatric_parameters[23]=$_REQUEST['risk_factor_7'];
		$patient_neuropsychiatric_parameters[24]=$user_id;
		$patient_neuropsychiatric_parameters[25]='now()';
		$patient_neuropsychiatric_parameters[26]=$_REQUEST['physicians'];
		
		$patient_neuropsychiatric_table_names[0]='patient_neuropsychiatric_sle_date';
		$patient_neuropsychiatric_table_names[1]='patient_neuropsychiatric_sle';
		$patient_neuropsychiatric_table_names[2]='neuroimaging';
		$patient_neuropsychiatric_table_names[3]='white_matter';
		$patient_neuropsychiatric_table_names[4]='grey_mater';
		$patient_neuropsychiatric_table_names[5]='infarcts';
		$patient_neuropsychiatric_table_names[6]='hemorrhagic';
		$patient_neuropsychiatric_table_names[7]='demyelinating';
		$patient_neuropsychiatric_table_names[8]='atrophy';
		$patient_neuropsychiatric_table_names[9]='spine_mri';
		$patient_neuropsychiatric_table_names[10]='csf_analysis';
		$patient_neuropsychiatric_table_names[11]='increase_wbcs';
		$patient_neuropsychiatric_table_names[12]='increase_proteint';
		$patient_neuropsychiatric_table_names[13]='increase_igg_index';
		$patient_neuropsychiatric_table_names[14]='increase_oligo_clonal_bands';
		$patient_neuropsychiatric_table_names[15]='eeg_studies';
		$patient_neuropsychiatric_table_names[16]='nerve_conduction';
		$patient_neuropsychiatric_table_names[17]='risk_factor_1';
		$patient_neuropsychiatric_table_names[18]='risk_factor_2';
		$patient_neuropsychiatric_table_names[19]='risk_factor_3';
		$patient_neuropsychiatric_table_names[20]='risk_factor_4';
		$patient_neuropsychiatric_table_names[21]='risk_factor_5';
		$patient_neuropsychiatric_table_names[22]='risk_factor_6';
		$patient_neuropsychiatric_table_names[23]='risk_factor_7';
		$patient_neuropsychiatric_table_names[24]='editor_id';
		$patient_neuropsychiatric_table_names[25]='edit_date';
		$patient_neuropsychiatric_table_names[26]='physicians';
		
		$patient_lupus_edit_name[0]='patient_neuropsychiatric_sle_id';
		$patient_lupus_code_edit_id[0]=$patient_neuropsychiatric_sle_id;
		$patient_lupus_code_sumbol[0]='=';
		if($patient_neuropsychiatric_sle_id=='')
		{
			$patient_neuropsychiatric_parameters[27]=$pat_id;
			$patient_neuropsychiatric_parameters[28]=0;
			$patient_neuropsychiatric_table_names[27]='pat_id';
			$patient_neuropsychiatric_table_names[28]='deleted';
				
			$patient_neuropsychiatric_sle_id=insert('patient_neuropsychiatric_sle',$patient_neuropsychiatric_table_names,$patient_neuropsychiatric_parameters,'patient_neuropsychiatric_sle_id');
			if($patient_neuropsychiatric_sle_id!='')
				$save_chk="ok";
		}
		else
			$save_chk=update('patient_neuropsychiatric_sle',$patient_neuropsychiatric_table_names,$patient_neuropsychiatric_parameters,$patient_lupus_edit_name,$patient_lupus_code_edit_id,$patient_lupus_code_sumbol);
		
	}
?>
<div class="wrapper">
  <!-- Main Header -->
  <?php
 // include "../portion/header.php";
  ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php
  //include "../portion/sidebar.php";
  ?>

	 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <!-- Content Header (Page header) -->
    <section class="content-header">
       <h4><b><?php if($patient_neuropsychiatric_sle_id=='') { echo 'Add'; } else { echo 'Edit'; } ?> Neuropsychiatric SLE</b></h4>
	  </section>
			<br>
			 <!-- Main content -->
			<section class="content">
			<div class="alert alert_suc alert-success" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Successful save!</strong>
			  </div>
			  <div class="alert alert_wr alert-danger" role="alert" style="DISPLAY:none;">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>Unsuccessful save!</strong>
			  </div>
			<form  id="form" action="neuropsychiatric.php" method="POST"  enctype="multipart/form-data">
			<br>
			<?php
				if($patient_neuropsychiatric_sle_id!="")
				{
					$exec = get_patient_neuropsychiatric_spec($patient_neuropsychiatric_sle_id);
					$result = pg_fetch_array($exec);
					
					$patient_neuropsychiatric_sle_date=$result['patient_neuropsychiatric_sle_date_str'];
					if($patient_neuropsychiatric_sle_date=='12-12-1900')
						$patient_neuropsychiatric_sle_date='';
					$patient_neuropsychiatric_sle=$result['patient_neuropsychiatric_sle'];
					$neuroimaging=$result['neuroimaging'];
					$white_matter=$result['white_matter'];
					$grey_mater=$result['grey_mater'];
					$infarcts=$result['infarcts'];
					$hemorrhagic=$result['hemorrhagic'];
					$demyelinating=$result['demyelinating'];
					$atrophy=$result['atrophy'];
					$spine_mri=$result['spine_mri'];
					$csf_analysis=$result['csf_analysis'];
					$increase_wbcs=$result['increase_wbcs'];
					$increase_proteint=$result['increase_proteint'];
					$increase_igg_index=$result['increase_igg_index'];
					$increase_oligo_clonal_bands=$result['increase_oligo_clonal_bands'];
					$eeg_studies=$result['eeg_studies'];
					$nerve_conduction=$result['nerve_conduction'];
					$risk_factor_1=$result['risk_factor_1'];
					$risk_factor_2=$result['risk_factor_2'];
					$risk_factor_3=$result['risk_factor_3'];
					$risk_factor_4=$result['risk_factor_4'];
					$risk_factor_5=$result['risk_factor_5'];
					$risk_factor_6=$result['risk_factor_6'];
					$risk_factor_7=$result['risk_factor_7'];
					$physicians=$result['physicians'];
				}
				?>
			<input type="hidden" id="save" name="save" value="1">
			<input type="hidden" id="patient_neuropsychiatric_sle_id" name="patient_neuropsychiatric_sle_id" value="<?php echo $patient_neuropsychiatric_sle_id;?>">
			<input type="hidden" id="row" name="row" value="<?php echo $row;?>">
			<input type="hidden" id="pat_id" name="pat_id" value="<?php echo $pat_id;?>">
			<div class="row">
				<div class="form-group col-md-6">
				  <label>*Neuropsychiatric SLE date</label>
					<input type="text" readonly="true" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="patient_neuropsychiatric_sle_date" name="patient_neuropsychiatric_sle_date" value="<?php echo $patient_neuropsychiatric_sle_date; ?>">
				</div>
				<div class="form-group col-md-6">
				  <label>*Neuropsychiatric SLE</label>
					<select class="form-control" name="patient_neuropsychiatric_sle" id="patient_neuropsychiatric_sle">
						<option value="0" <?php if($patient_neuropsychiatric_sle==0) { echo "selected"; } ?>>--</option>
						<option value="1" <?php if($patient_neuropsychiatric_sle==1) { echo "selected"; } ?>>Acute confusional state</option>
						<option value="2" <?php if($patient_neuropsychiatric_sle==2) { echo "selected"; } ?>>Guillain-Barre syndrome</option>
						<option value="3" <?php if($patient_neuropsychiatric_sle==3) { echo "selected"; } ?>>Anxiety disorder</option>
						<option value="4" <?php if($patient_neuropsychiatric_sle==4) { echo "selected"; } ?>>Aseptic meningitis </option>
						<option value="5" <?php if($patient_neuropsychiatric_sle==5) { echo "selected"; } ?>>Autoimmune inner disease</option>
						<option value="6" <?php if($patient_neuropsychiatric_sle==6) { echo "selected"; } ?>>Autonomic neuropathy</option>
						<option value="7" <?php if($patient_neuropsychiatric_sle==7) { echo "selected"; } ?>>Cerebrovascular disease / TIA</option>
						<option value="8" <?php if($patient_neuropsychiatric_sle==8) { echo "selected"; } ?>>Cognitive dysfunction </option>
						<option value="9" <?php if($patient_neuropsychiatric_sle==9) { echo "selected"; } ?>>Cranial neuropathy </option>
						<option value="10" <?php if($patient_neuropsychiatric_sle==10) { echo "selected"; } ?>>Demyelination </option>
						<option value="11" <?php if($patient_neuropsychiatric_sle==11) { echo "selected"; } ?>>Headache</option>
						<option value="12" <?php if($patient_neuropsychiatric_sle==12) { echo "selected"; } ?>>Mononeuropathy</option>
						<option value="13" <?php if($patient_neuropsychiatric_sle==13) { echo "selected"; } ?>>Mood disorder / Depression</option>
						<option value="14" <?php if($patient_neuropsychiatric_sle==14) { echo "selected"; } ?>>Movement disorder / chorea</option>
						<option value="15" <?php if($patient_neuropsychiatric_sle==15) { echo "selected"; } ?>>Myasthenia gravis</option>
						<option value="16" <?php if($patient_neuropsychiatric_sle==16) { echo "selected"; } ?>>Myelopathy</option>
						<option value="17" <?php if($patient_neuropsychiatric_sle==17) { echo "selected"; } ?>>Plexopathy</option>
						<option value="18" <?php if($patient_neuropsychiatric_sle==18) { echo "selected"; } ?>>Polyneuropathy</option>
						<option value="19" <?php if($patient_neuropsychiatric_sle==19) { echo "selected"; } ?>>Psychosis</option>
						<option value="20" <?php if($patient_neuropsychiatric_sle==20) { echo "selected"; } ?>>Seizure disorder</option>
						
					</select>
				</div>
			</div>
				<fieldset>
				<legend>Neuroimaging</legend>
					<div class="row">
						<div class="form-group col-md-6">
						  <label>Brain MRI</label>
							<select class="form-control" name="neuroimaging" id="neuroimaging">
								<option value="0" <?php if($neuroimaging==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if($neuroimaging==1) { echo "selected"; } ?>>Normal</option>
								<option value="2" <?php if($neuroimaging==2) { echo "selected"; } ?>>Abnormal</option>
							</select>
						</div>
						<div class="form-group col-md-6">
							&nbsp;
						</div>
					</div>
					<div id="divneuroimaging" name="divneuroimaging" <?php if($neuroimaging==2) { echo 'style="DISPLAY:block;" '; } else { echo 'style="DISPLAY:none;" '; } ?>>
						<div class="row">
							<div class="form-group col-md-4">
								<label>White-matter hyper-intensities</label>
								<select class="form-control" name="white_matter" id="white_matter">
									<option value="0" <?php if($white_matter==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if($white_matter==1) { echo "selected"; } ?>>Yes</option>
									<option value="2" <?php if($white_matter==2) { echo "selected"; } ?>>No</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label>Grey-matter hyper-intensities</label>
								<select class="form-control" name="grey_mater" id="grey_mater">
									<option value="0" <?php if($grey_mater==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if($grey_mater==1) { echo "selected"; } ?>>Yes</option>
									<option value="2" <?php if($grey_mater==2) { echo "selected"; } ?>>No</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label>Infarcts</label>
								<select class="form-control" name="infarcts" id="infarcts">
									<option value="0" <?php if($infarcts==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if($infarcts==1) { echo "selected"; } ?>>Yes</option>
									<option value="2" <?php if($infarcts==2) { echo "selected"; } ?>>No</option>
								</select>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-4">
								<label>Hemorrhagic lesions</label>
								<select class="form-control" name="hemorrhagic" id="hemorrhagic">
									<option value="0" <?php if($hemorrhagic==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if($hemorrhagic==1) { echo "selected"; } ?>>Yes</option>
									<option value="2" <?php if($hemorrhagic==2) { echo "selected"; } ?>>No</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label>Demyelinating lesions</label>
								<select class="form-control" name="demyelinating" id="demyelinating">
									<option value="0" <?php if($demyelinating==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if($demyelinating==1) { echo "selected"; } ?>>Yes</option>
									<option value="2" <?php if($demyelinating==2) { echo "selected"; } ?>>No</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label>Atrophy</label>
								<select class="form-control" name="atrophy" id="atrophy">
									<option value="0" <?php if($atrophy==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if($atrophy==1) { echo "selected"; } ?>>Yes</option>
									<option value="2" <?php if($atrophy==2) { echo "selected"; } ?>>No</option>
								</select>
							</div>
						</div>
					</div>
				</fieldset>
					<div class="row">
						<div class="form-group col-md-6">
						  <label>Spine MRI</label>
							<select class="form-control" name="spine_mri" id="spine_mri">
								<option value="0" <?php if($spine_mri==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if($spine_mri==1) { echo "selected"; } ?>>Normal</option>
								<option value="2" <?php if($spine_mri==2) { echo "selected"; } ?>>Abnormal / white-matter hyperintensities</option>
							</select>
						</div>
						<div class="form-group col-md-6">
							&nbsp;
						</div>
					</div>
				<fieldset>
				<legend>CSF analysis</legend>
					<div class="row">
						<div class="form-group col-md-6">
						  <label>CSF analysis</label>
							<select class="form-control" name="csf_analysis" id="csf_analysis">
								<option value="0" <?php if($csf_analysis==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if($csf_analysis==1) { echo "selected"; } ?>>Normal</option>
								<option value="2" <?php if($csf_analysis==2) { echo "selected"; } ?>>Abnormal</option>
							</select>
						</div>
						<div class="form-group col-md-6">
							&nbsp;
						</div>
					</div>
					<div id="divcsf_analysis" name="divcsf_analysis" <?php if($csf_analysis==2) { echo 'style="DISPLAY:block;" '; } else { echo 'style="DISPLAY:none;" '; } ?>>
						<div class="row">
							<div class="form-group col-md-3">
								<label>Increased WBCs</label>
								<select class="form-control" name="increase_wbcs" id="increase_wbcs">
									<option value="0" <?php if($increase_wbcs==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if($increase_wbcs==1) { echo "selected"; } ?>>Yes</option>
									<option value="2" <?php if($increase_wbcs==2) { echo "selected"; } ?>>No</option>
								</select>
							</div>
							<div class="form-group col-md-3">
								<label>Increased protein</label>
								<select class="form-control" name="increase_proteint" id="increase_proteint">
									<option value="0" <?php if($increase_proteint==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if($increase_proteint==1) { echo "selected"; } ?>>Yes</option>
									<option value="2" <?php if($increase_proteint==2) { echo "selected"; } ?>>No</option>
								</select>
							</div>
							<div class="form-group col-md-3">
								<label>Increased IgG index</label>
								<select class="form-control" name="increase_igg_index" id="increase_igg_index">
									<option value="0" <?php if($increase_igg_index==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if($increase_igg_index==1) { echo "selected"; } ?>>Yes</option>
									<option value="2" <?php if($increase_igg_index==2) { echo "selected"; } ?>>No</option>
								</select>
							</div>
							<div class="form-group col-md-3">
								<label>Oligo-clonal bands</label>
								<select class="form-control" name="increase_oligo_clonal_bands" id="increase_oligo_clonal_bands">
									<option value="0" <?php if($increase_oligo_clonal_bands==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if($increase_oligo_clonal_bands==1) { echo "selected"; } ?>>Yes</option>
									<option value="2" <?php if($increase_oligo_clonal_bands==2) { echo "selected"; } ?>>No</option>
								</select>
							</div>
						</div>
					</div>
				</fieldset>
				<div class="row">
						<div class="form-group col-md-6">
						  <label>EEG studies</label>
							<select class="form-control" name="eeg_studies" id="eeg_studies">
								<option value="0" <?php if($eeg_studies==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if($eeg_studies==1) { echo "selected"; } ?>>Normal</option>
								<option value="2" <?php if($eeg_studies==2) { echo "selected"; } ?>>Abnormal - non-specific</option>
								<option value="3" <?php if($eeg_studies==3) { echo "selected"; } ?>>Abnormal - epilepti-form</option>
							</select>
						</div>
						<div class="form-group col-md-6">
							<label>Nerve conduction / EMG studies</label>
							<select class="form-control" name="nerve_conduction" id="nerve_conduction">
								<option value="0" <?php if($nerve_conduction==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if($nerve_conduction==1) { echo "selected"; } ?>>Normal</option>
								<option value="2" <?php if($nerve_conduction==2) { echo "selected"; } ?>>Mononeuritis</option>
								<option value="3" <?php if($nerve_conduction==3) { echo "selected"; } ?>>Polyneuropathy</option>
								<option value="4" <?php if($nerve_conduction==4) { echo "selected"; } ?>>Myopathic </option>
							</select>
						</div>
					</div>
				<fieldset>
				<legend>Risk factors for Primary NPSLE</legend>
					<div class="row">
							<div class="form-group col-md-4">
								<label>Event close (within 6 months) to SLE onset/diagnosis</label>
								<select class="form-control" name="risk_factor_1" id="risk_factor_1">
									<option value="0" <?php if($risk_factor_1==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if($risk_factor_1==1) { echo "selected"; } ?>>Yes</option>
									<option value="2" <?php if($risk_factor_1==2) { echo "selected"; } ?>>No</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label>Major NPSLE</label>
								<select class="form-control" name="risk_factor_2" id="risk_factor_2">
									<option value="0" <?php if($risk_factor_2==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if($risk_factor_2==1) { echo "selected"; } ?>>Yes</option>
									<option value="2" <?php if($risk_factor_2==2) { echo "selected"; } ?>>No</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label>Confounding factors present</label>
								<select class="form-control" name="risk_factor_3" id="risk_factor_3">
									<option value="0" <?php if($risk_factor_3==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if($risk_factor_3==1) { echo "selected"; } ?>>Yes</option>
									<option value="2" <?php if($risk_factor_3==2) { echo "selected"; } ?>>No</option>
								</select>
							</div>
							</div>
						<div class="row">
							<div class="form-group col-md-3">
								<label>Favoring factors present</label>
								<select class="form-control" name="risk_factor_4" id="risk_factor_4">
									<option value="0" <?php if($risk_factor_4==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if($risk_factor_4==1) { echo "selected"; } ?>>Yes</option>
									<option value="2" <?php if($risk_factor_4==2) { echo "selected"; } ?>>No</option>
								</select>
							</div>
							<div class="form-group col-md-3">
								<label>Prior history of NPSLE</label>
								<select class="form-control" name="risk_factor_5" id="risk_factor_5">
									<option value="0" <?php if($risk_factor_5==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if($risk_factor_5==1) { echo "selected"; } ?>>Yes</option>
									<option value="2" <?php if($risk_factor_5==2) { echo "selected"; } ?>>No</option>
								</select>
							</div>
							<div class="form-group col-md-3">
								<label>Positive anti-phospholipid Abs</label>
								<select class="form-control" name="risk_factor_6" id="risk_factor_6">
									<option value="0" <?php if($risk_factor_6==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if($risk_factor_6==1) { echo "selected"; } ?>>Yes</option>
									<option value="2" <?php if($risk_factor_6==2) { echo "selected"; } ?>>No</option>
								</select>
							</div>
							<div class="form-group col-md-3">
								<label>Non-neurological SLE activity/flare</label>
								<select class="form-control" name="risk_factor_7" id="risk_factor_7">
									<option value="0" <?php if($risk_factor_7==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if($risk_factor_7==1) { echo "selected"; } ?>>Yes</option>
									<option value="2" <?php if($risk_factor_7==2) { echo "selected"; } ?>>No</option>
								</select>
							</div>
						</div>
				</fieldset>
				<div class="row">
						<div class="form-group col-md-6">
						  <label>Physician's attribution of NPSLE</label>
							<select class="form-control" name="physicians" id="physicians">
								<option value="0" <?php if($physicians==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if($physicians==1) { echo "selected"; } ?>>Related to SLE (primary NPSLE)</option>
								<option value="2" <?php if($physicians==2) { echo "selected"; } ?>>Not related to SLE (secondary NPSLE)</option>
								<option value="3" <?php if($physicians==3) { echo "selected"; } ?>>Uncertain</option>
							</select>
						</div>
						<div class="form-group col-md-6">
							&nbsp;
						</div>
					</div>
			<div class="row">
				<div class="form-group col-md-12">
					<input class="btn btn-l btn-primary" type="submit" value="OK">
					<input class="btn btn-l btn-primary" type="button" value="Cancel" name="close" onclick="javascript:return closeWin();">
				</div>
			</div>
				</form>
			 </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
//include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>
function reload()
{
	//if (confirm('Έχετε αποθηκεύσει τα δεδομένα σας;Το παράθυρο θα κλείσει!'))
	//{
		var row=$("#row").val();
		if($("#row").val()!="")
		{
			var newrow=row;
			window.opener.document.getElementById('patient_neuropsychiatric_sle_'+newrow).value=$("#patient_neuropsychiatric_sle option:selected").text();
			window.opener.document.getElementById('patient_neuropsychiatric_sle_date_'+newrow).value=$("#patient_neuropsychiatric_sle_date").val();
		}
		else
		{
			window.opener.patient_neuropsychiatric_sle_addrow($("#pat_id").val(),$("#patient_neuropsychiatric_sle_id").val());
			$("#row").val(window.opener.document.getElementById('patient_neuropsychiatric_sle_numofrows').value);
			var newrow=$("#row").val();
			window.opener.document.getElementById('patient_neuropsychiatric_sle_'+newrow).value=$("#patient_neuropsychiatric_sle option:selected").text();
			window.opener.document.getElementById('patient_neuropsychiatric_sle_date_'+newrow).value=$("#patient_neuropsychiatric_sle_date").val();
			window.opener.document.getElementById('patient_neuropsychiatric_sle_id_'+newrow).value=$("#patient_neuropsychiatric_sle_id").val();
			
		}
		
	//}
	//else
	//	return false;
}


$( document ).ready(function() {
	
	$('#patient_neuropsychiatric_sle_date').datepicker({
	  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		onClose2:function(dateText) {}
	})
	$('#patient_neuropsychiatric_sle_date').inputmask();
	
	
	$('#neuroimaging').change(function(){ 
		
		if($('#neuroimaging').val()==2)
			$('#divneuroimaging').show();
		else
			$('#divneuroimaging').hide();
			
	});
	
	$('#csf_analysis').change(function(){ 
		
		if($('#csf_analysis').val()==2)
			$('#divcsf_analysis').show();
		else
			$('#divcsf_analysis').hide();
			
	});
	
});

function closeWin()
{
	return window.close();
}	


  $(function () {
	  
	  $('#form').submit(function(){
		
		var msg="";
		 
		 if($('#patient_neuropsychiatric_sle_date').val()=='')
			msg +='-Fill neuropsychiatric SLE date!\n'; 
		
		if($('#patient_neuropsychiatric_sle').val()=='0')
			msg +='-Select Neuropsychiatric SLE!\n'; 
		
		if(msg != '')
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
		
	  });	
</script>
<?php
if($save==1)
{
	if($save_chk=="ok")
	{
?>
<script>
$(".alert_suc").show();
window.setTimeout(function() {
    $(".alert_suc").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);

reload();
window.close();
</script>
<?php
	}
	else
	{
?>
<script>
$(".alert_wr").show();
window.setTimeout(function() {
    $(".alert_wr").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
window.close();
</script>
<?php
	}
}
?>
</body>
</html>