<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	$save=$_REQUEST['save'];
	$pat_id=$_REQUEST['pat_id'];
	if($pat_id<>"")
		$_SESSION['pat_id']=$pat_id;
	
	$pat_id=$_SESSION['pat_id'];
	
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<?php
if($save==1)
{
	pg_query("BEGIN") or die("Could not start transaction\n");
	try
	{
		
		
		if (isset($_REQUEST['pregnancylosessbefore']))
			$pregnancylosessbefore="1";
		else
			$pregnancylosessbefore="0";
		
		if (isset($_REQUEST['pregnancylosessafter']))
			$pregnancylosessafter="1";
		else
			$pregnancylosessafter="0";
		
		if (isset($_REQUEST['prematurebirths']))
			$prematurebirths="1";
		else
			$prematurebirths="0";		
		
		if (isset($_REQUEST['eclampsia']))
			$eclampsia="1";
		else
			$eclampsia="0";
		
		if (isset($_REQUEST['premature']))
			$premature="1";
		else
			$premature="0";
		
		if (isset($_REQUEST['placental']))
			$placental="1";
		else
			$placental="0";
		
		if (isset($_REQUEST['prematureother']))
			$prematureother="1";
		else
			$prematureother="0";
			
		$parameters[0]=str_replace("'", "''",$_REQUEST['patient_id']);
		$parameters[1]=str_replace("'", "''",$_REQUEST['secondary_id']);
		$parameters[2]=$_REQUEST['gender'];
		$parameters[3]=date_for_postgres($_REQUEST['dateofbirth']);
		$parameters[4]=date_for_postgres($_REQUEST['dateofinclusion']);
		$parameters[5]= $_SESSION['user_centre_id']; //$_REQUEST['centre'];
		$parameters[6]=$_REQUEST['referringphysician'];
		$parameters[7]=str_replace("'", "''",$_REQUEST['ageatinclusion']);
		$parameters[8]=$_REQUEST['race'];
		$parameters[9]=$_REQUEST['ethnicity'];
		$parameters[10]=str_replace("'", "''",$_REQUEST['otherethnicity']);
		$parameters[11]=$_REQUEST['yearsofeduc'];
		$parameters[12]=$_REQUEST['familyhistory'];
		$parameters[13]=$_REQUEST['indexreumatic'];
		$parameters[14]=$_REQUEST['indexreumaticfather'];
		$parameters[15]=$_REQUEST['indexreumaticbrother'];
		$parameters[16]=$_REQUEST['indexreumaticsister'];
		$parameters[17]=$_REQUEST['indexreumaticson'];
		$parameters[18]=$_REQUEST['indexreumaticdaughter'];
		$parameters[19]=$_REQUEST['autoimmune'];
		
		if($_REQUEST['gender']==1)
		{
			if ($_REQUEST['obsetrichistory']==1)
			{
				$parameters[20]=$_REQUEST['obsetrichistory'];
				$parameters[21]=$_REQUEST['yearofmenarche'];
				$parameters[22]=$_REQUEST['menopause'];
				$parameters[23]=$_REQUEST['menopauseyear'];
				$parameters[24]=$_REQUEST['contraceptivemethods'];
				$parameters[25]=str_replace("'", "''",$_REQUEST['contraceptivemethodscomm']);
				$parameters[26]=$_REQUEST['intertility'];
				$parameters[27]=$_REQUEST['assistedreproduction'];
				$parameters[28]=$_REQUEST['pregnancies'];
				$parameters[29]=$_REQUEST['childbirths'];
				$parameters[30]=$_REQUEST['childbirthsyear'];
				$parameters[31]=$pregnancylosessbefore;
				$parameters[32]=$_REQUEST['pregnancylosesbeforeno'];
				$parameters[33]=$pregnancylosessafter;
				$parameters[34]=$_REQUEST['pregnancylosesafterno'];
				$parameters[35]=$prematurebirths;
				$parameters[36]=$_REQUEST['prematurebirthsno'];
				$parameters[37]=$eclampsia;
				$parameters[38]=$_REQUEST['eclampsiano'];
				$parameters[39]=$premature;
				$parameters[40]=$_REQUEST['prematureno'];
				$parameters[41]=$placental;
				$parameters[42]=$_REQUEST['placentalno'];
				$parameters[43]=$prematureother;
				$parameters[44]=$_REQUEST['prematureotherno'];
			}
			else
			{
				$parameters[20]=$_REQUEST['obsetrichistory'];
				$parameters[21]="";
				$parameters[22]=0;
				$parameters[23]="";
				$parameters[24]=0;
				$parameters[25]='';
				$parameters[26]=0;
				$parameters[27]=0;
				$parameters[28]=0;
				$parameters[29]=0;
				$parameters[30]="";
				$parameters[31]=0;
				$parameters[32]=0;
				$parameters[33]=0;
				$parameters[34]=0;
				$parameters[35]=0;
				$parameters[36]=0;
				$parameters[37]=0;
				$parameters[38]=0;
				$parameters[39]=0;
				$parameters[40]=0;
				$parameters[41]=0;
				$parameters[42]=0;
				$parameters[43]=0;
				$parameters[44]=0;
			}
		}
		else
		{
			$parameters[20]=0;
			$parameters[21]="";
			$parameters[22]=0;
			$parameters[23]="";
			$parameters[24]=0;
			$parameters[25]='';
			$parameters[26]=0;
			$parameters[27]=0;
			$parameters[28]=0;
			$parameters[29]=0;
			$parameters[30]="";
			$parameters[31]=0;
			$parameters[32]=0;
			$parameters[33]=0;
			$parameters[34]=0;
			$parameters[35]=0;
			$parameters[36]=0;
			$parameters[37]=0;
			$parameters[38]=0;
			$parameters[39]=0;
			$parameters[40]=0;
			$parameters[41]=0;
			$parameters[42]=0;
			$parameters[43]=0;
			$parameters[44]=0;
		}
		
		$parameters[45]=$_REQUEST['thromboticevents'];
		$parameters[46]=$_REQUEST['employment'];
		$parameters[47]=$_REQUEST['employmentstatus'];
		$parameters[48]=$_REQUEST['worktype'];
		$parameters[49]=str_replace("'", "''",$_REQUEST['job']);
		$parameters[50]=$_REQUEST['previousemployment'];
		$parameters[51]=date_for_postgres($_REQUEST['previousemploymentdate']);
		$parameters[52]=$_REQUEST['previousemploymentstatus'];
		$parameters[53]=$_REQUEST['previousworktype'];
		$parameters[54]=str_replace("'", "''",$_REQUEST['previousjob']);
		$parameters[55]=$_REQUEST['followupemployment'];
		$parameters[56]=date_for_postgres($_REQUEST['followupemploymentdate']);
		$parameters[57]=$_REQUEST['followupemploymentstatus'];
		$parameters[58]=$_REQUEST['followupworktype'];
		$parameters[59]=str_replace("'", "''",$_REQUEST['followupjob']);
		$parameters[60]=$_REQUEST['jobchangerelated'];
		/*$parameters[61]=$_REQUEST['tobacco'];
		$parameters[62]=str_replace("'", "''",$_REQUEST['smokingstarting']);
		$parameters[63]=$_REQUEST['smokingstopped'];
		$parameters[64]=str_replace("'", "''",$_REQUEST['smokingstoppedyear']);
		$parameters[65]=$_REQUEST['decadefirst'];
		$parameters[66]=$_REQUEST['decadesecond'];
		$parameters[67]=$_REQUEST['decadethird'];
		$parameters[68]=$_REQUEST['decadeforth'];
		$parameters[69]=$_REQUEST['decadefifth'];
		$parameters[70]=$_REQUEST['decadesixth'];
		$parameters[71]=$_REQUEST['decadeseventh'];
		$parameters[72]=$_REQUEST['decadeeight'];
		$parameters[73]=str_replace("'", "''",$_REQUEST['averagepackperday']);
		$parameters[74]=str_replace("'", "''",$_REQUEST['averagepackperyear']);*/
		$parameters[61]=$user_id;
		$parameters[62]=$_REQUEST['indexreumaticmother'];
		$parameters[63]=$_REQUEST['id'];
		
		$table_names[0]='patient_id';
		$table_names[1]='secondary_id';
		$table_names[2]='gender';
		$table_names[3]='dateofbirth';
		$table_names[4]='dateofinclusion';
		$table_names[5]='centre';
		$table_names[6]='referringphysician';
		$table_names[7]='ageatinclusion';
		$table_names[8]='race';
		$table_names[9]='ethnicity';
		$table_names[10]='otherethnicity';
		$table_names[11]='yearsofeduc';
		$table_names[12]='familyhistory';
		$table_names[13]='indexreumatic';
		$table_names[14]='indexreumaticfather';
		$table_names[15]='indexreumaticbrother';
		$table_names[16]='indexreumaticsister';
		$table_names[17]='indexreumaticson';
		$table_names[18]='indexreumaticdaughter';
		$table_names[19]='autoimmune';
		$table_names[20]='obsetrichistory';
		$table_names[21]='yearofmenarche';
		$table_names[22]='menopause';
		$table_names[23]='menopauseyear';
		$table_names[24]='contraceptivemethods';
		$table_names[25]='contraceptivemethodscomm';
		$table_names[26]='intertility';
		$table_names[27]='assistedreproduction';
		$table_names[28]='pregnancies';
		$table_names[29]='childbirths';
		$table_names[30]='childbirthsyear';
		$table_names[31]='pregnancylosessbefore';
		$table_names[32]='pregnancylosesbeforeno';
		$table_names[33]='pregnancylosessafter';
		$table_names[34]='pregnancylosesafterno';
		$table_names[35]='prematurebirths';
		$table_names[36]='prematurebirthsno';
		$table_names[37]='eclampsia';
		$table_names[38]='eclampsiano';
		$table_names[39]='premature';
		$table_names[40]='prematureno';
		$table_names[41]='placental';
		$table_names[42]='placentalno';
		$table_names[43]='prematureother';
		$table_names[44]='prematureotherno';
		$table_names[45]='thromboticevents';
		$table_names[46]='employment';
		$table_names[47]='employmentstatus';
		$table_names[48]='worktype';
		$table_names[49]='job';
		$table_names[50]='previousemployment';
		$table_names[51]='previousemploymentdate';
		$table_names[52]='previousemploymentstatus';
		$table_names[53]='previousworktype';
		$table_names[54]='previousjob';
		$table_names[55]='followupemployment';
		$table_names[56]='followupemploymentdate';
		$table_names[57]='followupemploymentstatus';
		$table_names[58]='followupworktype';
		$table_names[59]='followupjob';
		$table_names[60]='jobchangerelated';
		/*$table_names[61]='tobacco';
		$table_names[62]='smokingstarting';
		$table_names[63]='smokingstopped';
		$table_names[64]='smokingstoppedyear';
		$table_names[65]='decadefirst';
		$table_names[66]='decadesecond';
		$table_names[67]='decadethird';
		$table_names[68]='decadeforth';
		$table_names[69]='decadefifth';
		$table_names[70]='decadesixth';
		$table_names[71]='decadeseventh';
		$table_names[72]='decadeeight';
		$table_names[73]='averagepackperday';
		$table_names[74]='averagepackperyear';*/
		$table_names[61]='editor_id';
		$table_names[62]='indexreumaticmother';
		$table_names[63]='id';
		$edit_name[0]='pat_id';
		$edit_id[0]=$pat_id;
		$sumbol[0]='=';
		if($pat_id=='')
		{
			$pat_id=insert('patient',$table_names,$parameters,'pat_id');
			if($pat_id!='')
			{
				$_SESSION['pat_id']=$pat_id;
				$msg="ok";
			}
		}
		else
		{
			$msg=update('patient',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
		}
		
		//patient_resindence table-start
		$resindence_numofrows=$_REQUEST['resindence_numofrows'];
		$j=1;
		for ($i=1;$i<=$resindence_numofrows;$i++)
		{
			$resindence_hidden=$_REQUEST['resindence_hidden_'.$i];
			$resindence_id=$_REQUEST['resindence_id_'.$i];
			if($resindence_hidden=='-1')
			{
				if($resindence_id<>"")
				{
					$patient_resindence_hdn_parameters[0]=1;
					$patient_resindence_hdn_names[0]='deleted';
					$patient_resindence_hdn_edit_name[0]='resindence_id';
					$patient_resindence_hdn_edit_id[0]=$resindence_id;
					$patient_resindence_hdn_sumbol[0]='=';
					$patient_resindence_msg=update('patient_resindence',$patient_resindence_hdn_names,$patient_resindence_hdn_parameters,$patient_resindence_hdn_edit_name,$patient_resindence_hdn_edit_id,$patient_resindence_hdn_sumbol);
					if ($patient_resindence_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
			{
				$patient_resindence_parameters[0]=$_REQUEST['resindence_'.$i];
				$patient_resindence_parameters[1]=date_for_postgres($_REQUEST['resindencedate_from_'.$i]);
				$patient_resindence_parameters[2]=date_for_postgres($_REQUEST['resindencedate_to_'.$i]);
				$patient_resindence_table_names[0]='resindence';
				$patient_resindence_table_names[1]='resindencedate_from';
				$patient_resindence_table_names[2]='resindencedate_to';
				$patient_resindence_edit_name[0]='resindence_id';
				$patient_resindence_code_edit_id[0]=$resindence_id;
				$patient_resindence_code_sumbol[0]='=';
				
				if($resindence_id=='')
				{
					$patient_resindence_parameters[3]=$user_id;
					$patient_resindence_parameters[4]=$pat_id;
					$patient_resindence_parameters[5]=0;
					$patient_resindence_table_names[3]='editor_id';
					$patient_resindence_table_names[4]='pat_id';
					$patient_resindence_table_names[5]='deleted';
					
					$id=insert('patient_resindence',$patient_resindence_table_names,$patient_resindence_parameters,'resindence_id');
					if($id!='')
						$j++;
				}
				else
				{
					$patient_resindence_msg=update('patient_resindence',$patient_resindence_table_names,$patient_resindence_parameters,$patient_resindence_edit_name,$patient_resindence_code_edit_id,$patient_resindence_code_sumbol);
					if ($patient_resindence_msg=="ok")
						$j++;
				}
			}
		}
		
		if ($j==$i)
			$patient_resindence_msg="ok";
		else
			$patient_resindence_msg="nok";
		//patient_resindence table-end
		
		//patient_autoimmune table-start
		$autoimmune_numofrows=$_REQUEST['autoimmune_numofrows'];
		$j=1;
		for ($i=1;$i<=$autoimmune_numofrows;$i++)
		{
			$autoimmune_hidden=$_REQUEST['autoimmune_hidden_'.$i];
			$autoimmune_id=$_REQUEST['autoimmune_id_'.$i];
			if($autoimmune_hidden=='-1')
			{
				if($autoimmune_id<>"")
				{
					$patient_autoimmune_hdn_parameters[0]=1;
					$patient_autoimmune_hdn_names[0]='deleted';
					$patient_autoimmune_hdn_edit_name[0]='autoimmune_id';
					$patient_autoimmune_hdn_edit_id[0]=$autoimmune_id;
					$patient_autoimmune_hdn_sumbol[0]='=';
					$patient_autoimmune_msg=update('patient_autoimmune',$patient_autoimmune_hdn_names,$patient_autoimmune_hdn_parameters,$patient_autoimmune_hdn_edit_name,$patient_autoimmune_hdn_edit_id,$patient_autoimmune_hdn_sumbol);
					if ($patient_autoimmune_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
			{
				$patient_autoimmune_parameters[0]=$_REQUEST['autoimmune_relation_'.$i];
				$patient_autoimmune_parameters[1]=str_replace("'", "''",$_REQUEST['autoimmune_diagnosis_'.$i]);
				$patient_autoimmune_table_names[0]='relation';
				$patient_autoimmune_table_names[1]='diagnosis';
				$patient_autoimmune_edit_name[0]='autoimmune_id';
				$patient_autoimmune_code_edit_id[0]=$autoimmune_id;
				$patient_autoimmune_code_sumbol[0]='=';
				
				if($autoimmune_id=='')
				{
					$patient_autoimmune_parameters[2]=$user_id;
					$patient_autoimmune_parameters[3]=$pat_id;
					$patient_autoimmune_parameters[4]=0;
					$patient_autoimmune_table_names[2]='editor_id';
					$patient_autoimmune_table_names[3]='pat_id';
					$patient_autoimmune_table_names[4]='deleted';
					
					$id=insert('patient_autoimmune',$patient_autoimmune_table_names,$patient_autoimmune_parameters,'autoimmune_id');
					if($id!='')
						$j++;
				}
				else
				{
					$patient_autoimmune_msg=update('patient_autoimmune',$patient_autoimmune_table_names,$patient_autoimmune_parameters,$patient_autoimmune_edit_name,$patient_autoimmune_code_edit_id,$patient_autoimmune_code_sumbol);
					if ($patient_autoimmune_msg=="ok")
						$j++;
				}
			}
		}
		
		if ($j==$i)
			$patient_autoimmune_msg="ok";
		else
			$patient_autoimmune_msg="nok";
		//patient_autoimmune table-end
		
		//patient_venous table-start
		$venous_numofrows=$_REQUEST['venous_numofrows'];
		$j=1;
		for ($i=1;$i<=$venous_numofrows;$i++)
		{
			$venous_hidden=$_REQUEST['venous_hidden_'.$i];
			$venous_id=$_REQUEST['venous_id_'.$i];
			if($venous_hidden=='-1')
			{
				if($venous_id<>"")
				{
					$patient_venous_hdn_parameters[0]=1;
					$patient_venous_hdn_names[0]='deleted';
					$patient_venous_hdn_edit_name[0]='venous_id';
					$patient_venous_hdn_edit_id[0]=$venous_id;
					$patient_venous_hdn_sumbol[0]='=';
					$patient_venous_msg=update('patient_venous',$patient_venous_hdn_names,$patient_venous_hdn_parameters,$patient_venous_hdn_edit_name,$patient_venous_hdn_edit_id,$patient_venous_hdn_sumbol);
					if ($patient_venous_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
			{
				$patient_venous_parameters[0]=$_REQUEST['organ_id_'.$i];
				$patient_venous_parameters[1]=$_REQUEST['venous_year_'.$i];
				$patient_venous_table_names[0]='organ_id';
				$patient_venous_table_names[1]='venous_year';
				$patient_venous_edit_name[0]='venous_id';
				$patient_venous_code_edit_id[0]=$venous_id;
				$patient_venous_code_sumbol[0]='=';
				
				if($venous_id=='')
				{
					$patient_venous_parameters[2]=$user_id;
					$patient_venous_parameters[3]=$pat_id;
					$patient_venous_parameters[4]=0;
					$patient_venous_table_names[2]='editor_id';
					$patient_venous_table_names[3]='pat_id';
					$patient_venous_table_names[4]='deleted';
					
					$id=insert('patient_venous',$patient_venous_table_names,$patient_venous_parameters,'venous_id');
					if($id!='')
						$j++;
				}
				else
				{
					$patient_venous_msg=update('patient_venous',$patient_venous_table_names,$patient_venous_parameters,$patient_venous_edit_name,$patient_venous_code_edit_id,$patient_venous_code_sumbol);
					if ($patient_venous_msg=="ok")
						$j++;
				}
			}
		}
		
		if ($j==$i)
			$patient_venous_msg="ok";
		else
			$patient_venous_msg="nok";
		//patient_venous table-end
		
		//patient_arterial table-start
		$arterial_numofrows=$_REQUEST['arterial_numofrows'];
		$j=1;
		for ($i=1;$i<=$arterial_numofrows;$i++)
		{
			$arterial_hidden=$_REQUEST['arterial_hidden_'.$i];
			$arterial_id=$_REQUEST['arterial_id_'.$i];
			if($arterial_hidden=='-1')
			{
				if($arterial_id<>"")
				{
					$patient_arterial_hdn_parameters[0]=1;
					$patient_arterial_hdn_names[0]='deleted';
					$patient_arterial_hdn_edit_name[0]='arterial_id';
					$patient_arterial_hdn_edit_id[0]=$arterial_id;
					$patient_arterial_hdn_sumbol[0]='=';
					$patient_arterial_msg=update('patient_arterial',$patient_arterial_hdn_names,$patient_arterial_hdn_parameters,$patient_arterial_hdn_edit_name,$patient_arterial_hdn_edit_id,$patient_arterial_hdn_sumbol);
					if ($patient_arterial_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
			{
				$patient_arterial_parameters[0]=$_REQUEST['arterial_organ_id_'.$i];
				$patient_arterial_parameters[1]=$_REQUEST['arterial_year_'.$i];
				$patient_arterial_table_names[0]='organ_id';
				$patient_arterial_table_names[1]='arterial_year';
				$patient_arterial_edit_name[0]='arterial_id';
				$patient_arterial_code_edit_id[0]=$arterial_id;
				$patient_arterial_code_sumbol[0]='=';
				
				if($arterial_id=='')
				{
					$patient_arterial_parameters[2]=$user_id;
					$patient_arterial_parameters[3]=$pat_id;
					$patient_arterial_parameters[4]=0;
					$patient_arterial_table_names[2]='editor_id';
					$patient_arterial_table_names[3]='pat_id';
					$patient_arterial_table_names[4]='deleted';
					
					$id=insert('patient_arterial',$patient_arterial_table_names,$patient_arterial_parameters,'arterial_id');
					if($id!='')
						$j++;
				}
				else
				{
					$patient_arterial_msg=update('patient_arterial',$patient_arterial_table_names,$patient_arterial_parameters,$patient_arterial_edit_name,$patient_arterial_code_edit_id,$patient_arterial_code_sumbol);
					if ($patient_arterial_msg=="ok")
						$j++;
				}
			}
		}
		
		if ($j==$i)
			$patient_arterial_msg="ok";
		else
			$patient_arterial_msg="nok";
		//patient_arterial table-end
		
		//patient_marital table-start
		$marital_numofrows=$_REQUEST['marital_numofrows'];
		$j=1;
		for ($i=1;$i<=$marital_numofrows;$i++)
		{
			$marital_hidden=$_REQUEST['marital_hidden_'.$i];
			$marital_id=$_REQUEST['marital_id_'.$i];
			if($marital_hidden=='-1')
			{
				if($marital_id<>"")
				{
					$patient_marital_hdn_parameters[0]=1;
					$patient_marital_hdn_names[0]='deleted';
					$patient_marital_hdn_edit_name[0]='marital_id';
					$patient_marital_hdn_edit_id[0]=$marital_id;
					$patient_marital_hdn_sumbol[0]='=';
					$patient_marital_msg=update('patient_marital',$patient_marital_hdn_names,$patient_marital_hdn_parameters,$patient_marital_hdn_edit_name,$patient_marital_hdn_edit_id,$patient_marital_hdn_sumbol);
					if ($patient_marital_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
			{
				$patient_marital_parameters[0]=$_REQUEST['marital_status_'.$i];
				$patient_marital_parameters[1]=$_REQUEST['marital_year_'.$i];
				$patient_marital_table_names[0]='marital_status';
				$patient_marital_table_names[1]='marital_year';
				$patient_marital_edit_name[0]='marital_id';
				$patient_marital_code_edit_id[0]=$marital_id;
				$patient_marital_code_sumbol[0]='=';
				
				if($marital_id=='')
				{
					$patient_marital_parameters[2]=$user_id;
					$patient_marital_parameters[3]=$pat_id;
					$patient_marital_parameters[4]=0;
					$patient_marital_table_names[2]='editor_id';
					$patient_marital_table_names[3]='pat_id';
					$patient_marital_table_names[4]='deleted';
					
					$id=insert('patient_marital',$patient_marital_table_names,$patient_marital_parameters,'marital_id');
					if($id!='')
						$j++;
				}
				else
				{
					$patient_marital_msg=update('patient_marital',$patient_marital_table_names,$patient_marital_parameters,$patient_marital_edit_name,$patient_marital_code_edit_id,$patient_marital_code_sumbol);
					if ($patient_marital_msg=="ok")
						$j++;
				}
			}
		}
		
		if ($j==$i)
			$patient_marital_msg="ok";
		else
			$patient_marital_msg="nok";
		//patient_marital table-end
		
		//patient_physical table-start
		$physical_numofrows=$_REQUEST['physical_numofrows'];
		$j=1;
		for ($i=1;$i<=$physical_numofrows;$i++)
		{
			$physical_hidden=$_REQUEST['physical_hidden_'.$i];
			$physical_id=$_REQUEST['physical_id_'.$i];
			if($physical_hidden=='-1')
			{
				if($physical_id<>"")
				{
					$patient_physical_hdn_parameters[0]=1;
					$patient_physical_hdn_names[0]='deleted';
					$patient_physical_hdn_edit_name[0]='physical_id';
					$patient_physical_hdn_edit_id[0]=$physical_id;
					$patient_physical_hdn_sumbol[0]='=';
					$patient_physical_msg=update('patient_physical',$patient_physical_hdn_names,$patient_physical_hdn_parameters,$patient_physical_hdn_edit_name,$patient_physical_hdn_edit_id,$patient_physical_hdn_sumbol);
					if ($patient_physical_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
			{
				$patient_physical_parameters[0]=$_REQUEST['physical_status_'.$i];
				$patient_physical_parameters[1]=date_for_postgres($_REQUEST['date_phys_str_'.$i]);
				$patient_physical_table_names[0]='status';
				$patient_physical_table_names[1]='date';
				$patient_physical_edit_name[0]='physical_id';
				$patient_physical_code_edit_id[0]=$physical_id;
				$patient_physical_code_sumbol[0]='=';
				
				if($physical_id=='')
				{
					$patient_physical_parameters[2]=$user_id;
					$patient_physical_parameters[3]=$pat_id;
					$patient_physical_parameters[4]=0;
					$patient_physical_table_names[2]='editor_id';
					$patient_physical_table_names[3]='pat_id';
					$patient_physical_table_names[4]='deleted';
					
					$id=insert('patient_physical',$patient_physical_table_names,$patient_physical_parameters,'physical_id');
					if($id!='')
						$j++;
				}
				else
				{
					$patient_physical_msg=update('patient_physical',$patient_physical_table_names,$patient_physical_parameters,$patient_physical_edit_name,$patient_physical_code_edit_id,$patient_physical_code_sumbol);
					if ($patient_physical_msg=="ok")
						$j++;
				}
			}
		}
		
		if ($j==$i)
			$patient_physical_msg="ok";
		else
			$patient_physical_msg="nok";
		//patient_physical table-end
		
		//patient_mediterranean table-start
		$mediterranean_numofrows=$_REQUEST['mediterranean_numofrows'];
		$j=1;
		for ($i=1;$i<=$mediterranean_numofrows;$i++)
		{
			$mediterranean_hidden=$_REQUEST['mediterranean_hidden_'.$i];
			$mediterranean_id=$_REQUEST['mediterranean_id_'.$i];
			if($mediterranean_hidden=='-1')
			{
				if($mediterranean_id<>"")
				{
					$patient_mediterranean_hdn_parameters[0]=1;
					$patient_mediterranean_hdn_names[0]='deleted';
					$patient_mediterranean_hdn_edit_name[0]='mediterranean_id';
					$patient_mediterranean_hdn_edit_id[0]=$mediterranean_id;
					$patient_mediterranean_hdn_sumbol[0]='=';
					$patient_mediterranean_msg=update('patient_mediterranean',$patient_mediterranean_hdn_names,$patient_mediterranean_hdn_parameters,$patient_mediterranean_hdn_edit_name,$patient_mediterranean_hdn_edit_id,$patient_mediterranean_hdn_sumbol);
					if ($patient_mediterranean_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
			{ 
				$patient_mediterranean_parameters[0]=$_REQUEST['mediterranean_status_'.$i];
				$patient_mediterranean_parameters[1]=date_for_postgres($_REQUEST['date_med_str_'.$i]);;
				$patient_mediterranean_table_names[0]='status';
				$patient_mediterranean_table_names[1]='date';
				$patient_mediterranean_edit_name[0]='mediterranean_id';
				$patient_mediterranean_code_edit_id[0]=$mediterranean_id;
				$patient_mediterranean_code_sumbol[0]='=';
				
				if($mediterranean_id=='')
				{
					$patient_mediterranean_parameters[2]=$user_id;
					$patient_mediterranean_parameters[3]=$pat_id;
					$patient_mediterranean_parameters[4]=0;
					$patient_mediterranean_table_names[2]='editor_id';
					$patient_mediterranean_table_names[3]='pat_id';
					$patient_mediterranean_table_names[4]='deleted';
					
					$id=insert('patient_mediterranean',$patient_mediterranean_table_names,$patient_mediterranean_parameters,'mediterranean_id');
					if($id!='')
						$j++;
				}
				else
				{
					$patient_mediterranean_msg=update('patient_mediterranean',$patient_mediterranean_table_names,$patient_mediterranean_parameters,$patient_mediterranean_edit_name,$patient_mediterranean_code_edit_id,$patient_mediterranean_code_sumbol);
					if ($patient_mediterranean_msg=="ok")
						$j++;
				}
			}
		}
		
		if ($j==$i)
			$patient_mediterranean_msg="ok";
		else
			$patient_mediterranean_msg="nok";
		//patient_mediterranean table-end
		
		//patient_alcohol table-start
		$alcohol_numofrows=$_REQUEST['alcohol_numofrows'];
		$j=1;
		for ($i=1;$i<=$alcohol_numofrows;$i++)
		{
			$alcohol_hidden=$_REQUEST['alcohol_hidden_'.$i];
			$patient_alcohol_id=$_REQUEST['patient_alcohol_id_'.$i];
			if($alcohol_hidden=='-1')
			{
				if($patient_alcohol_id<>"")
				{
					$patient_alcohol_hdn_parameters[0]=1;
					$patient_alcohol_hdn_names[0]='deleted';
					$patient_alcohol_hdn_edit_name[0]='patient_alcohol_id';
					$patient_alcohol_hdn_edit_id[0]=$patient_alcohol_id;
					$patient_alcohol_hdn_sumbol[0]='=';
					$patient_alcohol_msg=update('patient_alcohol',$patient_alcohol_hdn_names,$patient_alcohol_hdn_parameters,$patient_alcohol_hdn_edit_name,$patient_alcohol_hdn_edit_id,$patient_alcohol_hdn_sumbol);
					if ($patient_alcohol_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
			{
				$patient_alcohol_parameters[0]=$_REQUEST['type_'.$i];
				$patient_alcohol_parameters[1]=$_REQUEST['dose_'.$i];
				$patient_alcohol_table_names[0]='type';
				$patient_alcohol_table_names[1]='dose';
				$patient_alcohol_edit_name[0]='patient_alcohol_id';
				$patient_alcohol_code_edit_id[0]=$patient_alcohol_id;
				$patient_alcohol_code_sumbol[0]='=';
				
				if($patient_alcohol_id=='')
				{
					$patient_alcohol_parameters[2]=$user_id;
					$patient_alcohol_parameters[3]=$pat_id;
					$patient_alcohol_parameters[4]=0;
					$patient_alcohol_table_names[2]='editor_id';
					$patient_alcohol_table_names[3]='pat_id';
					$patient_alcohol_table_names[4]='deleted';
					
					$id=insert('patient_alcohol',$patient_alcohol_table_names,$patient_alcohol_parameters,'patient_alcohol_id');
					if($id!='')
						$j++;
				}
				else
				{
					$patient_alcohol_msg=update('patient_alcohol',$patient_alcohol_table_names,$patient_alcohol_parameters,$patient_alcohol_edit_name,$patient_alcohol_code_edit_id,$patient_alcohol_code_sumbol);
					if ($patient_alcohol_msg=="ok")
						$j++;
				}
			}
		}
		
		if ($j==$i)
			$patient_alcohol_msg="ok";
		else
			$patient_alcohol_msg="nok";
		//patient_alcohol table-end
		/*echo "msg: $msg patient_autoimmune_msg:$patient_autoimmune_msg";
		echo "patient_venous_msg: $patient_venous_msg patient_arterial_msg:$patient_arterial_msg";
		echo "patient_marital_msg: $patient_marital_msg patient_mediterranean_msg:$patient_mediterranean_msg";
		echo "patient_physical_msg: $patient_physical_msg patient_resindence_msg:$patient_resindence_msg";*/
		if ($msg=="ok" and $patient_autoimmune_msg=="ok" and $patient_venous_msg=="ok" and $patient_arterial_msg=="ok" and $patient_marital_msg=="ok" and $patient_mediterranean_msg=="ok" and $patient_physical_msg=="ok" and $patient_alcohol_msg=="ok" and $patient_resindence_msg=="ok")
		{
			$save_chk="ok";
			pg_query("COMMIT") or die("Transaction commit failed\n");
		}
		else
		{
			$save_chk="nok";
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
	}
	catch(exception $e) {
		echo "e:".$e."</br>";
		echo "ROLLBACK";
		pg_query("ROLLBACK") or die("Transaction rollback failed\n");
	}
	
}

/*$order = get_cohorts($pat_id);
$checkstopcohorts=0;
while($result = pg_fetch_assoc($order))
{
	if($stop_date=="12-12-1900" or $stop_date=="")
		$checkstopcohorts++;
}

if ($checkstopcohorts>0)
	$_SESSION['checkstopcohorts']=1;
else
	$_SESSION['checkstopcohorts']=0;
*/
?>
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Demographics
        </h1>
      </section>

      <!-- Main content -->
      <section class="content">
	  <div class="alert alert_suc alert-success" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Successful save!</strong>
	  </div>
	  <div class="alert alert_wr alert-danger" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Unsuccessful save!</strong>
	  </div>
	  <form id="form" action="demographics.php" method="POST">
	  <input type="hidden" id="save" name="save" value="1">
		<input type="hidden" id="pat_id" name="pat_id" value="<?php echo $pat_id;?>">
       		<?php
				if($pat_id!="")
				{
					$exec = get_patient_demographics($pat_id);
					
					$result = pg_fetch_array($exec);
					$patient_id=$result['patient_id'];
					$secondary_id=$result['secondary_id'];
					$gender=$result['gender'];
					$dateofbirth_str=$result['dateofbirth_str'];
					if ($dateofbirth_str=='12-12-1900')
						$dateofbirth_str='';
					$dateofinclusion_str=$result['dateofinclusion_str'];
					if ($dateofinclusion_str=='12-12-1900')
						$dateofinclusion_str='';
					//$centre=$result['centre'];
					$referringphysician=$result['referringphysician'];
					$ageatinclusion=$result['ageatinclusion'];
					$race=$result['race'];
					$ethnicity=$result['ethnicity'];
					$otherethnicity=$result['otherethnicity'];
					
					$yearsofeduc=$result['yearsofeduc'];
					$familyhistory=$result['familyhistory'];
					$indexreumatic=$result['indexreumatic'];
					$indexreumaticfather=$result['indexreumaticfather'];
					$indexreumaticbrother=$result['indexreumaticbrother'];
					$indexreumaticsister=$result['indexreumaticsister'];
					$indexreumaticson=$result['indexreumaticson'];
					$indexreumaticdaughter=$result['indexreumaticdaughter'];
					$indexreumaticmother=$result['indexreumaticmother'];
					$autoimmune=$result['autoimmune'];
					$obsetrichistory=$result['obsetrichistory'];
					$yearofmenarche=$result['yearofmenarche'];
					$menopause=$result['menopause'];
					$menopauseyear=$result['menopauseyear'];
					$contraceptivemethods=$result['contraceptivemethods'];
					$contraceptivemethodscomm=$result['contraceptivemethodscomm'];
					$intertility=$result['intertility'];
					$assistedreproduction=$result['assistedreproduction'];
					$pregnancies=$result['pregnancies'];
					$childbirths=$result['childbirths'];
					$childbirthsyear=$result['childbirthsyear'];
					$pregnancylosessbefore=$result['pregnancylosessbefore'];
					$pregnancylosesbeforeno=$result['pregnancylosesbeforeno'];
					$pregnancylosessafter=$result['pregnancylosessafter'];
					$pregnancylosesafterno=$result['pregnancylosesafterno'];
					$prematurebirths=$result['prematurebirths'];
					$prematurebirthsno=$result['prematurebirthsno'];
					$eclampsia=$result['eclampsia'];
					$eclampsiano=$result['eclampsiano'];
					$premature=$result['premature'];
					$prematureno=$result['prematureno'];
					$placental=$result['placental'];
					$placentalno=$result['placentalno'];
					$prematureother=$result['prematureother'];
					$prematureotherno=$result['prematureotherno'];
					$thromboticevents=$result['thromboticevents'];
					$employment=$result['employment'];
					$employmentstatus=$result['employmentstatus'];
					$worktype=$result['worktype'];
					$job=$result['job'];
					$previousemployment=$result['previousemployment'];
					$previousemploymentdate_str=$result['previousemploymentdate_str'];
					if ($previousemploymentdate_str=='12-12-1900')
						$previousemploymentdate_str='';
					$previousemploymentstatus=$result['previousemploymentstatus'];
					$previousworktype=$result['previousworktype'];
					$previousjob=$result['previousjob'];
					$followupemployment=$result['followupemployment'];
					$followupemploymentdate_str=$result['followupemploymentdate_str'];
					if ($followupemploymentdate_str=='12-12-1900')
						$followupemploymentdate_str='';
					$followupemploymentstatus=$result['followupemploymentstatus'];
					$followupworktype=$result['followupworktype'];
					$followupjob=$result['followupjob'];
					$jobchangerelated=$result['jobchangerelated'];
					$tobacco=$result['tobacco'];
					$smokingstarting=$result['smokingstarting'];
					$smokingstopped=$result['smokingstopped'];
					$smokingstoppedyear=$result['smokingstoppedyear'];
					$decadefirst=$result['decadefirst'];
					$decadesecond=$result['decadesecond'];
					$decadethird=$result['decadethird'];
					$decadeforth=$result['decadeforth'];
					$decadefifth=$result['decadefifth'];
					$decadesixth=$result['decadesixth'];
					$decadeseventh=$result['decadeseventh'];
					$decadeeight=$result['decadeeight'];
					$averagepackperday=$result['averagepackperday'];
					$averagepackperyear=$result['averagepackperyear'];
					$id=$result['id'];
				}
				?>
		<div class="row">
			<div class="form-group col-md-12">
				<input type="button" id="submit1" class="btn btn-primary" value="Save">
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Patients ids</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<!--add has-error in class-->
							<div class="form-group col-md-6" id="divpatientid" name="divpatientid">
							  <label>Patient ID</label>
							  <input type="hidden"  value="<?php echo $pat_id; ?>" id="pat_id" name="pat_id">
							  <input type="text" class="form-control" value="<?php echo $patient_id; ?>" id="patient_id" name="patient_id">
							  <label class="control-label" for="inputError" id="Errorpatientid" name="Errorpatientid" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field correctly!</label>
							</div>
							<div class="form-group col-md-6">
							  <label>Secondary ID</label>
							  <input type="text" class="form-control" placeholder="Secondary ID ..." value="<?php echo $secondary_id; ?>" id="secondary_id" name="secondary_id">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3">
							  <label>Last day of follow-up</label>
							  <?php
							  
								$order = get_last_followup($pat_id);
								$result = pg_fetch_assoc($order);
								$last_follow_up=$result['start_date_str'];
								if($last_follow_up=="12-12-1900")
									$last_follow_up="";
							  ?>
							  <input type="text" readOnly="true" class="form-control" value="<?php echo $last_follow_up; ?>">
							</div>
						</div>
				  </div>
				</div>
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Demographics</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-4" id="divgender" name="divgender">
							  <label>*Gender</label>
							  <select class="form-control" name="gender" id="gender">
								<option value="0" <?php if ($gender==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($gender==1) { echo "selected"; } ?>>Female</option>
								<option value="2" <?php if ($gender==2) { echo "selected"; } ?>>Male</option>
							  </select>
							  <label class="control-label" for="inputError" id="Errorgender" name="Errorgender" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field correctly!</label>
							</div>
							  <div  class="form-group col-md-4" id="divdateofbirth" name="divdateofbirth">
								<label>*Date of birth</label>
								<div class="input-group date">
								  <div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								  </div>
								  <input type="text" readOnly="true" class="form-control pull-right" value="<?php echo $dateofbirth_str; ?>" id="dateofbirth" name="dateofbirth">
								  <label class="control-label" for="inputError" id="Errordateofbirth" name="Errordateofbirth" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field correctly!</label>
								</div>
								<!-- /.input group -->
							  </div>
								<div  class="form-group col-md-4" id="divdateofbirth" name="divdateofbirth">
								<label>Id</label>
								<input type="text" class="form-control" value="<?php echo $id; ?>" id="id" name="id">
								  <!-- /.input group -->
							  </div>
						</div>
						<div class="row">
							<div class="form-group col-md-4" id="divdateofinclusion" name="divdateofinclusion">
								<label>*Date of inclusion</label>
								<div class="input-group">
								  <div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								  </div>
								  <input type="text" class="form-control pull-right"  readOnly="true" value="<?php echo $dateofinclusion_str; ?>" id="dateofinclusion" name="dateofinclusion">
								</div>
								<label class="control-label" for="inputError" id="Errordateofinclusion" name="Errordateofinclusion" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field correctly!</label>
								<!-- /.input group -->
							  </div>
							  <div class="form-group col-md-4">
							  <label>Age at inclusion</label>
							  <input type="text" class="form-control" readonly value="<?php echo $ageatinclusion; ?>" id="ageatinclusion" name="ageatinclusion">
							</div>
							<div class="form-group col-md-4" id="divreferringphysician" name="divreferringphysician">
								<label>Referring physician</label>
								  <select class="form-control" name="referringphysician" id="referringphysician">
									<option value="0">--</option>
									<?php
										$sql = get_lookup_tbl_values_specific_parent("referringphysician",$_SESSION['user_centre_id']);
										$numrows = pg_num_rows($sql);
										while($result2 = pg_fetch_array($sql))
										{
											$referringphysician_id=$result2['id'];
											$value =$result2['value'];
									?>
									<option value="<?php echo $referringphysician_id; ?>" <?php if($referringphysician_id==$referringphysician) { echo "selected"; } ?>><?php echo $value; ?></option>
									<?php
										}
									?>
								  </select>
								  <label class="control-label" for="inputError" id="Errorreferringphysician" name="Errorreferringphysician" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field correctly!</label>
							</div>
							<!--<div class="form-group col-md-4" id="divcentre" name="divcentre">
								<label>*Centre</label>
								  <select class="form-control" name="centre" id="centre">
									<option value="0">--</option>
									<?php
										/*$sql = get_lookup_tbl_values("centre");
										$numrows = pg_num_rows($sql);
										while($result2 = pg_fetch_array($sql))
										{
											$centre_id=$result2['id'];
											$value =$result2['value'];
									?>
									<option value="<?php echo $centre_id; ?>" <?php if($centre_id==$centre) { echo "selected"; } ?>><?php echo $value; ?></option>
									<?php
										}*/
									?>
								  </select>
								  <label class="control-label" for="inputError" id="Errorcentre" name="Errorcentre" style="display:none;"><i class="fa fa-times-circle-o"></i> Fill the field correctly!</label>
							</div>-->
						</div>
						<div class="row">
							<div class="form-group col-md-4" id="divrace" name="divrace">
							  <label>Race</label>
							  <select class="form-control" name="race" id="race">
								<option value="0" <?php if ($race==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($race==1) { echo "selected"; } ?>>White/Caucasian</option>
								<option value="2" <?php if ($race==2) { echo "selected"; } ?>>Black/African-American</option>
								<option value="3" <?php if ($race==3) { echo "selected"; } ?>>Asian/Oriental</option>
								<option value="4" <?php if ($race==4) { echo "selected"; } ?>>Hispanic</option>
								<option value="5" <?php if ($race==5) { echo "selected"; } ?>>Other</option>
								<option value="6" <?php if ($race==6) { echo "selected"; } ?>>Don't know</option>
							  </select>
							  <label class="control-label" for="inputError" id="Errorrace" name="Errorrace" style="display:none;"><i class="fa fa-times-circle-o"></i> Select one option!</label>
							</div>
							<div class="form-group col-md-4" id="divethnicity" name="divethnicity">
							  <label>Ethnicity</label>
							  <select class="form-control" name="ethnicity" id="ethnicity">
								<option value="0" <?php if ($ethnicity==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($ethnicity==1) { echo "selected"; } ?>>Greek</option>
								<option value="2" <?php if ($ethnicity==2) { echo "selected"; } ?>>Other</option>
								<option value="3" <?php if ($ethnicity==3) { echo "selected"; } ?>>Don't know</option>
							  </select>
							  <label class="control-label" for="inputError" id="Errorethnicity" name="Errorethnicity" style="display:none;"><i class="fa fa-times-circle-o"></i> Select one option!</label>
							</div>
							<div class="form-group col-md-4" <?php if($ethnicity==2) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?> id="divotherethnicity" name="divotherethnicity">
							  <label>Other ethnicity</label>
							  <input type="text" class="form-control" value="<?php echo $otherethnicity; ?>" id="otherethnicity" name="otherethnicity">
							</div>
						</div>
						
						<div class="row">
							<div class="form-group col-md-4">
							  <label>Residence</label>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-8" id="divresidence" name="divresidence">
								  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="resindence_tbl" name="resindence_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-4"><b>Residence</b></th>
											<th class="col-md-3"><b>From</b></th>
											<th class="col-md-3"><b>To</b></th>
											<th class="col-md-2">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=0;
										if($pat_id<>"")
										{
											$order = get_resindence($pat_id);
											$num_rows = pg_num_rows($order);
											
											while($result = pg_fetch_assoc($order))
											{
												$resindence_id=$result['resindence_id'];
												$resindence=$result['resindence'];
												$resindencedate_from_str=$result['resindencedate_from_str'];
												if ($resindencedate_from_str=='12-12-1900')
													$resindencedate_from_str='';
												$resindencedate_to_str=$result['resindencedate_to_str'];
												if ($resindencedate_to_str=='12-12-1900')
													$resindencedate_to_str='';
											
									?>
										  <tr id="resindence_tr_<?php echo $i;?>"> 
											   <td align="center">
													<input type="hidden" id="resindence_hidden_<?php echo $i;?>" name="resindence_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="resindence_id_<?php echo $i; ?>" id="resindence_id_<?php echo $i; ?>" value="<?php echo $resindence_id;?>">
														<select class="form-control" name="resindence_<?php echo $i; ?>" id="resindence_<?php echo $i; ?>">
															<option value="1" <?php if ($resindence==1) { echo "selected"; } ?>>Urban (>15.000 dwellers)</option>
															<option value="2" <?php if ($resindence==2) { echo "selected"; } ?>>Suburban (10-15.000 dwellers)</option>
															<option value="3" <?php if ($resindence==3) { echo "selected"; } ?>>Rural (<10.000 dwellers)</option>
															<option value="4" <?php if ($resindence==4) { echo "selected"; } ?>>Don't know</option>
														</select>
												</td>
												<td align="center">
													<input type="text" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" id="resindencedate_from_<?php echo $i; ?>" name="resindencedate_from_<?php echo $i; ?>" value="<?php echo $resindencedate_from_str; ?>">
												</td>
												<td align="center">
													<input type="text" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" id="resindencedate_to_<?php echo $i; ?>" name="resindencedate_to_<?php echo $i; ?>" value="<?php echo $resindencedate_to_str; ?>">
												</td>
													<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="resindence_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
												  </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										}
									?>
									
									</tbody>
								</table>
								<input type="hidden" name="resindence_numofrows" id="resindence_numofrows" value="<?php echo $j; ?>">
								<br>
								 <a class="btn btn-primary" href="javascript:void(0);" onclick="resindence_addrow();"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
						<div class="row">
							<div class="form-group col-md-4" id="divyearsofeduc" name="divyearsofeduc">
							  <label>Years of education</label>
							  <select class="form-control" name="yearsofeduc" id="yearsofeduc">
								<option value="0" <?php if ($yearsofeduc==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($yearsofeduc==1) { echo "selected"; } ?>>0</option>
								<option value="2" <?php if ($yearsofeduc==2) { echo "selected"; } ?>>1-6</option>
								<option value="3" <?php if ($yearsofeduc==3) { echo "selected"; } ?>>7-9</option>
								<option value="4" <?php if ($yearsofeduc==4) { echo "selected"; } ?>>10-12</option>
								<option value="5" <?php if ($yearsofeduc==5) { echo "selected"; } ?>>>12</option>
								<option value="6" <?php if ($yearsofeduc==6) { echo "selected"; } ?>>Unknown</option>
							  </select>
							  <label class="control-label" for="inputError" id="Erroryearsofeduc" name="Erroryearsofeduc" style="display:none;"><i class="fa fa-times-circle-o"></i> Select one option!</label>
							</div>
						</div>
					</div>
				</div> <!-- demographic div -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Family history of autoimmunity</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-4" id="divfamilyhistory" name="divfamilyhistory">
							  <label>Family history of autoimmunity</label>
							  <select class="form-control" name="familyhistory" id="familyhistory">
								<option value="0" <?php if ($familyhistory==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($familyhistory==1) { echo "selected"; } ?>>Yes</option>
								<option value="2" <?php if ($familyhistory==2) { echo "selected"; } ?>>No</option>
								<option value="3" <?php if ($familyhistory==3) { echo "selected"; } ?>>Don't know</option>
							  </select>
							  <label class="control-label" for="inputError" id="Errorfamilyhistory" name="Errorfamilyhistory" style="display:none;"><i class="fa fa-times-circle-o"></i> Select one option!</label>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3" id="divindexreumatic" name="divindexreumatic" <?php if($indexreumatic<>"" and $indexreumatic<>"0" ) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>First-degree relative(s) with index rheumatic disease</label>
							  <select class="form-control" name="indexreumatic" id="indexreumatic">
								<option value="0" <?php if ($indexreumatic==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($indexreumatic==1) { echo "selected"; } ?>>Yes</option>
								<option value="2" <?php if ($indexreumatic==2) { echo "selected"; } ?>>No</option>
								<option value="3" <?php if ($indexreumatic==3) { echo "selected"; } ?>>Don't know</option>
							  </select>
							  <label class="control-label" for="inputError" id="Errorindexreumatic" name="Errorindexreumatic" style="display:none;"><i class="fa fa-times-circle-o"></i> Select one option!</label>
							</div>
							<div class="form-group col-md-3" id="divindexreumatic1" name="divindexreumatic1" <?php if($indexreumatic==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
								<label>Father</label>
								<select class="form-control" name="indexreumaticfather" id="indexreumaticfather">
									<option value="0" <?php if ($indexreumaticfather==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if ($indexreumaticfather==1) { echo "selected"; } ?>>No</option>
									<option value="2" <?php if ($indexreumaticfather==2) { echo "selected"; } ?>>Yes</option>
								  </select>
								<label>Mother</label>
									<select class="form-control" name="indexreumaticmother" id="indexreumaticmother">
									<option value="0" <?php if ($indexreumaticmother==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if ($indexreumaticmother==1) { echo "selected"; } ?>>No</option>
									<option value="2" <?php if ($indexreumaticmother==2) { echo "selected"; } ?>>Yes</option>
								  </select>
							</div>
							<div class="form-group col-md-3" id="divindexreumatic2" name="divindexreumatic2" <?php if($indexreumatic==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
								<label>Brother(s)</label>
								<select class="form-control" name="indexreumaticbrother" id="indexreumaticbrother">
									<option value="0" <?php if ($indexreumaticbrother==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if ($indexreumaticbrother==1) { echo "selected"; } ?>>1</option>
									<option value="2" <?php if ($indexreumaticbrother==2) { echo "selected"; } ?>>2</option>
									<option value="3" <?php if ($indexreumaticbrother==3) { echo "selected"; } ?>>3</option>
									<option value="4" <?php if ($indexreumaticbrother==4) { echo "selected"; } ?>>4</option>
									<option value="5" <?php if ($indexreumaticbrother==5) { echo "selected"; } ?>>5</option>
								  </select>
								<label>Sister(s)</label>
									<select class="form-control" name="indexreumaticsister" id="indexreumaticsister">
									<option value="0" <?php if ($indexreumaticsister==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if ($indexreumaticsister==1) { echo "selected"; } ?>>1</option>
									<option value="2" <?php if ($indexreumaticsister==2) { echo "selected"; } ?>>2</option>
									<option value="3" <?php if ($indexreumaticsister==3) { echo "selected"; } ?>>3</option>
									<option value="4" <?php if ($indexreumaticsister==4) { echo "selected"; } ?>>4</option>
									<option value="5" <?php if ($indexreumaticsister==5) { echo "selected"; } ?>>5</option>
								  </select>
							</div>
							<div class="form-group col-md-3" id="divindexreumatic3" name="divindexreumatic3" <?php if($indexreumatic==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
								<label>Son(s)</label>
								<select class="form-control" name="indexreumaticson" id="indexreumaticson">
									<option value="0" <?php if ($indexreumaticson==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if ($indexreumaticson==1) { echo "selected"; } ?>>1</option>
									<option value="2" <?php if ($indexreumaticson==2) { echo "selected"; } ?>>2</option>
									<option value="3" <?php if ($indexreumaticson==3) { echo "selected"; } ?>>3</option>
									<option value="4" <?php if ($indexreumaticson==4) { echo "selected"; } ?>>4</option>
									<option value="5" <?php if ($indexreumaticson==5) { echo "selected"; } ?>>5</option>
								  </select>
								<label>Daughter(s)</label>
									<select class="form-control" name="indexreumaticdaughter" id="indexreumaticdaughter">
									<option value="0" <?php if ($indexreumaticdaughter==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if ($indexreumaticdaughter==1) { echo "selected"; } ?>>1</option>
									<option value="2" <?php if ($indexreumaticdaughter==2) { echo "selected"; } ?>>2</option>
									<option value="3" <?php if ($indexreumaticdaughter==3) { echo "selected"; } ?>>3</option>
									<option value="4" <?php if ($indexreumaticdaughter==4) { echo "selected"; } ?>>4</option>
									<option value="5" <?php if ($indexreumaticdaughter==5) { echo "selected"; } ?>>5</option>
								  </select>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-4" id="divautoimmune" name="divautoimmune" <?php if($autoimmune<>"" and $autoimmune<>"0" ) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>First-degree relative(s) with other autoimmune disease</label>
							  <select class="form-control" name="autoimmune" id="autoimmune">
								<option value="0" <?php if ($autoimmune==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($autoimmune==1) { echo "selected"; } ?>>Yes</option>
								<option value="2" <?php if ($autoimmune==2) { echo "selected"; } ?>>No</option>
								<option value="3" <?php if ($autoimmune==3) { echo "selected"; } ?>>Don't know</option>
							  </select>
							  <label class="control-label" for="inputError" id="Errorautoimmune" name="Errorautoimmune" style="display:none;"><i class="fa fa-times-circle-o"></i> Select one option!</label>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-8" id="divautoimmunetable" name="divautoimmunetable" <?php if($autoimmune=="1" ) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
								  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="autoimmune_tbl" name="autoimmune_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-3"><b>Relation</b></th>
											<th class="col-md-4"><b>Diagnosis</b></th>
											<th class="col-md-1">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=0;
										if($pat_id<>"")
										{
											$order = get_autoimmune($pat_id);
											$num_rows = pg_num_rows($order);
											
											while($result = pg_fetch_assoc($order))
											{
												$autoimmune_id=$result['autoimmune_id'];
												$relation=$result['relation'];
												$diagnosis=$result['diagnosis'];
											
									?>
										  <tr id="autoimmune_tr_<?php echo $i;?>"> 
											   <td align="center">
													<input type="hidden" id="autoimmune_hidden_<?php echo $i;?>" name="autoimmune_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="autoimmune_id_<?php echo $i; ?>" id="autoimmune_id_<?php echo $i; ?>" value="<?php echo $autoimmune_id;?>">
														<select class="form-control" name="autoimmune_relation_<?php echo $i; ?>" id="autoimmune_relation_<?php echo $i; ?>">
															<option value="1" <?php if ($relation==1) { echo "selected"; } ?>>Parent</option>
															<option value="2" <?php if ($relation==2) { echo "selected"; } ?>>Offspring</option>
															<option value="3" <?php if ($relation==3) { echo "selected"; } ?>>Sibling</option>
														</select>
												</td>
												<td align="center">
													<textarea class="form-control" id="autoimmune_diagnosis_<?php echo $i; ?>" name="autoimmune_diagnosis_<?php echo $i; ?>"><?php echo $diagnosis; ?></textarea>
												</td>
													<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="autoimmune_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
												  </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										}
									?>
									</tbody>
								</table>
								<input type="hidden" name="autoimmune_numofrows" id="autoimmune_numofrows" value="<?php echo $j; ?>">
								<br>
								 <a class="btn btn-primary" href="javascript:void(0);" onclick="autoimmune_addrow();"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
					</div>
				</div><!-- Family history of autoimmunity div -->
				<div class="box box-primary" <?php if ($gender<>1) { ?> style="DISPLAY:none;" <?php } ?> id="divobsetric" name="divobsetric">
					<div class="box-header with-border">
						<h3 class="box-title">Obstetric history</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-3" id="divobsetrichistory" name="divobsetrichistory">
							  <label>Obstetric history</label>
							  <select class="form-control" name="obsetrichistory" id="obsetrichistory">
								<option value="0" <?php if ($obsetrichistory==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($obsetrichistory==1) { echo "selected"; } ?>>Reported</option>
								<option value="2" <?php if ($obsetrichistory==2) { echo "selected"; } ?>>Not reported</option>
								<option value="3" <?php if ($obsetrichistory==3) { echo "selected"; } ?>>Does not aply</option>
							  </select>
							  <label class="control-label" for="inputError" id="Errorobsetrichistory" name="Errorobsetrichistory" style="display:none;"><i class="fa fa-times-circle-o"></i> Select one option!</label>
							</div>
							<div class="form-group col-md-3" id="divyearofmenarche" name="divyearofmenarche" <?php if($obsetrichistory=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>Year of menarche</label>
							  <div class="input-group">
								  <div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								  </div>
									<input type="text" class="form-control pull-right" data-inputmask="'mask': '9999'" value="<?php echo $yearofmenarche; ?>" id="yearofmenarche" name="yearofmenarche">
								  </div>
							</div>
							<div class="form-group col-md-3" id="divmenarche" name="divmenarche" <?php if($obsetrichistory=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>Menopause</label>
							  <select class="form-control" name="menopause" id="menopause">
								<option value="0" <?php if ($menopause==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($menopause==1) { echo "selected"; } ?>>No</option>
								<option value="2" <?php if ($menopause==2) { echo "selected"; } ?>>Yes</option>
							  </select>
							</div>
							<div class="form-group col-md-3" id="divmenopauseyear" name="divmenopauseyear" <?php if($menopause=="2") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>Menopause year</label>
							  <div class="input-group">
								  <div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								  </div>
								   <input type="text" class="form-control pull-right" data-inputmask="'mask': '9999'" value="<?php echo $menopauseyear; ?>" id="menopauseyear" name="menopauseyear">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3" id="divcontraceptivemethods" name="divcontraceptivemethods" <?php if($obsetrichistory=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>Use of contraceptive methods</label>
							  <select class="form-control" name="contraceptivemethods" id="contraceptivemethods">
								<option value="0" <?php if ($contraceptivemethods==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($contraceptivemethods==1) { echo "selected"; } ?>>No</option>
								<option value="2" <?php if ($contraceptivemethods==2) { echo "selected"; } ?>>Yes</option>
							  </select>
							</div>
							<div class="form-group col-md-5" id="divcontraceptivemethodscomm" name="divcontraceptivemethodscomm" <?php if($contraceptivemethods=="2") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>Comments</label>
							  <div class="input-group">
								  <textarea class="form-control" id="contraceptivemethodscomm" name="contraceptivemethodscomm"><?php echo $contraceptivemethodscomm; ?></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3" id="divintertility" name="divintertility" <?php if($obsetrichistory=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>Infertility issue</label>
							  <select class="form-control" name="intertility" id="intertility">
								<option value="0" <?php if ($intertility==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($intertility==1) { echo "selected"; } ?>>No</option>
								<option value="2" <?php if ($intertility==2) { echo "selected"; } ?>>Yes</option>
							  </select>
							</div>
							<div class="form-group col-md-3" id="divassistedreproduction" name="divassistedreproduction" <?php if($obsetrichistory=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>Assisted reproduction methods</label>
							  <select class="form-control" name="assistedreproduction" id="assistedreproduction">
								<option value="0" <?php if ($assistedreproduction==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($assistedreproduction==1) { echo "selected"; } ?>>No</option>
								<option value="2" <?php if ($assistedreproduction==2) { echo "selected"; } ?>>Yes</option>
							  </select>
							</div>
							<div class="form-group col-md-2" id="divpregnancies" name="divpregnancies" <?php if($obsetrichistory=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>No.Pregnancies</label>
							  <select class="form-control" name="pregnancies" id="pregnancies">
								<option value="-1" <?php if ($pregnancies==-1 or $pregnancies=="") { echo "selected"; } ?>>--</option>
								<?php 
									for($i=0;$i<=15;$i++)
									{
								?>
								<option value="<?php echo $i; ?>" <?php if ($pregnancies==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
								<?php 
									}
								?>
							  </select>
							</div>
							<div class="form-group col-md-2" id="divchildbirths" name="divchildbirths" <?php if($obsetrichistory=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>No.Childbirths</label>
							  <select class="form-control" name="childbirths" id="childbirths">
								<option value="-1" <?php if ($childbirths==-1 or $childbirths=="") { echo "selected"; } ?>>--</option>
								<?php 
									for($i=0;$i<=15;$i++)
									{
								?>
								<option value="<?php echo $i; ?>" <?php if ($childbirths==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
								<?php 
									}
								?>
							  </select>
							</div>
							<div class="form-group col-md-2" id="divchildbirthsyear" name="divchildbirthsyear" <?php if($obsetrichistory=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>Childbirths year of last</label>
							  <div class="input-group">
								  <div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								  </div>
								  <input type="text" class="form-control pull-right" data-inputmask="'mask': '9999'" value="<?php echo $childbirthsyear; ?>" id="childbirthsyear" name="childbirthsyear">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3" id="divpregnancylosesbefore" name="divpregnancylosesbefore" <?php if($obsetrichistory=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							 <label>No.Pregnancy loses</label>
								<div class="form-group">
									<div class="checkbox">
										<label>
										  <input type="checkbox" id="pregnancylosessbefore" name="pregnancylosessbefore" <?php if($pregnancylosessbefore==1) { echo "checked"; } ?>>
										  before the 10th week
										</label>
									  </div>
									  <div  id="divpregnancylosesbeforeno" name="divpregnancylosesbeforeno" <?php if($pregnancylosessbefore==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
											<label>Number</label>
											<div class="input-group">
												<select class="form-control" name="pregnancylosesbeforeno" id="pregnancylosesbeforeno">
													<option value="0" <?php if ($pregnancylosesbeforeno=="0") { echo "selected"; } ?>>--</option>
													<?php 
														for($i=1;$i<=15;$i++)
														{
													?>
													<option value="<?php echo $i; ?>" <?php if ($pregnancylosesbeforeno==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
													<?php 
														}
													?>
												  </select>
											</div>
											<!-- /.input group -->
										  </div>
								</div>
							</div>
							<div class="form-group col-md-3" id="divpregnancylosesafter" name="divpregnancylosesafter" <?php if($obsetrichistory=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							 	<label>&nbsp;</label>
								<div class="form-group">
									<div class="checkbox">
										<label>
										  <input type="checkbox" id="pregnancylosessafter" name="pregnancylosessafter" <?php if($pregnancylosessafter==1) { echo "checked"; } ?>>
										  after the 10th week
										</label>
									  </div>
									  <div  id="divpregnancylosesafterno" name="divpregnancylosesafterno" <?php if($pregnancylosessafter==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
											<label>Number</label>
											<div class="input-group">
												<select class="form-control" name="pregnancylosesafterno" id="pregnancylosesafterno">
													<option value="0" <?php if ($pregnancylosesafterno=="0") { echo "selected"; } ?>>--</option>
													<?php 
														for($i=1;$i<=15;$i++)
														{
													?>
													<option value="<?php echo $i; ?>" <?php if ($pregnancylosesafterno==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
													<?php 
														}
													?>
												  </select>
											</div>
											<!-- /.input group -->
										  </div>
								</div>
							</div>
							<div class="form-group col-md-3" id="divprematurebirths" name="divprematurebirths" <?php if($obsetrichistory=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							 <label>Premature births</label>
								<div class="form-group">
									<div class="checkbox">
										<label>
										  <input type="checkbox" id="prematurebirths" name="prematurebirths" <?php if($prematurebirths==1) { echo "checked"; } ?>>
										  Premature births
										</label>
									  </div>
									  <div  id="divprematurebirthsno" name="divprematurebirthsno" <?php if($prematurebirths==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
											<label>Number</label>
											<div class="input-group">
												<select class="form-control" name="prematurebirthsno" id="prematurebirthsno">
													<option value="0" <?php if ($prematurebirthsno=="0") { echo "selected"; } ?>>--</option>
													<?php 
														for($i=1;$i<=15;$i++)
														{
													?>
													<option value="<?php echo $i; ?>" <?php if ($prematurebirthsno==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
													<?php 
														}
													?>
												  </select>
											</div>
											<!-- /.input group -->
										  </div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3" id="diveclampsia" name="diveclampsia" <?php if($obsetrichistory=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							 <label>Reason for premature births</label>
								<div class="form-group">
									<div class="checkbox">
										<label>
										  <input type="checkbox" id="eclampsia" name="eclampsia" <?php if($eclampsia==1) { echo "checked"; } ?>>
										  (pre-)eclampsia
										</label>
									  </div>
									  <div  id="diveclampsiano" name="diveclampsiano" <?php if($eclampsia==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
											<label>Number</label>
											<div class="input-group">
												<select class="form-control" name="eclampsiano" id="eclampsiano">
													<option value="0" <?php if ($eclampsiano=="0") { echo "selected"; } ?>>--</option>
													<?php 
														for($i=1;$i<=15;$i++)
														{
													?>
													<option value="<?php echo $i; ?>" <?php if ($eclampsiano==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
													<?php 
														}
													?>
												  </select>
											</div>
											<!-- /.input group -->
										  </div>
								</div>
							</div>
							<div class="form-group col-md-3" id="divpremature" name="divpremature" <?php if($obsetrichistory=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							 	<label>&nbsp;</label>
								<div class="form-group">
									<div class="checkbox">
										<label>
										  <input type="checkbox" id="premature" name="premature" <?php if($premature==1) { echo "checked"; } ?>>
										 premature ROM
										</label>
									  </div>
									  <div  id="divprematureno" name="divprematureno" <?php if($premature==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
											<label>Number</label>
											<div class="input-group">
												<select class="form-control" name="prematureno" id="prematureno">
													<option value="0" <?php if ($prematureno=="0") { echo "selected"; } ?>>--</option>
													<?php 
														for($i=1;$i<=15;$i++)
														{
													?>
													<option value="<?php echo $i; ?>" <?php if ($prematureno==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
													<?php 
														}
													?>
												  </select>
											</div>
											<!-- /.input group -->
										  </div>
								</div>
							</div>
							<div class="form-group col-md-3" id="divplacental" name="divplacental" <?php if($obsetrichistory=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							 <label>&nbsp;</label>
								<div class="form-group">
									<div class="checkbox">
										<label>
										  <input type="checkbox" id="placental" name="placental" <?php if($placental==1) { echo "checked"; } ?>>
										  placental insufficiency
										</label>
									  </div>
									  <div  id="divplacentalno" name="divplacentalno" <?php if($placental==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
											<label>Number</label>
											<div class="input-group">
												<select class="form-control" name="placentalno" id="placentalno">
													<option value="0" <?php if ($placentalno=="0") { echo "selected"; } ?>>--</option>
													<?php 
														for($i=1;$i<=15;$i++)
														{
													?>
													<option value="<?php echo $i; ?>" <?php if ($placentalno==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
													<?php 
														}
													?>
												  </select>
											</div>
											<!-- /.input group -->
										  </div>
								</div>
							</div>
							<div class="form-group col-md-3" id="divprematureother" name="divprematureother" <?php if($obsetrichistory=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							 <label>&nbsp;</label>
								<div class="form-group">
									<div class="checkbox">
										<label>
										  <input type="checkbox" id="prematureother" name="prematureother" <?php if($prematureother==1) { echo "checked"; } ?>>
										  Other
										</label>
									  </div>
									  <div  id="divprematureotherno" name="divprematureotherno" <?php if($prematureother==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
											<label>Number</label>
											<div class="input-group">
												<select class="form-control" name="prematureotherno" id="prematureotherno">
													<option value="0" <?php if ($prematureotherno=="0") { echo "selected"; } ?>>--</option>
													<?php 
														for($i=1;$i<=15;$i++)
														{
													?>
													<option value="<?php echo $i; ?>" <?php if ($prematureotherno==$i) { echo "selected"; } ?>><?php echo $i; ?></option>
													<?php 
														}
													?>
												  </select>
											</div>
											<!-- /.input group -->
										  </div>
								</div>
							</div>
						</div>
				  </div>
				</div><!-- Obstetric history div -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">History of thrombotic events</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-3" id="divthromboticevents" name="divthromboticevents">
							  <label>history of thrombotic events</label>
							  <select class="form-control" name="thromboticevents" id="thromboticevents">
								<option value="0" <?php if ($thromboticevents==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($thromboticevents==1) { echo "selected"; } ?>>Yes</option>
								<option value="2" <?php if ($thromboticevents==2) { echo "selected"; } ?>>No</option>
								<option value="3" <?php if ($thromboticevents==3) { echo "selected"; } ?>>Don't know</option>
							  </select>
							  <label class="control-label" for="inputError" id="Errorthromboticevents" name="Errorthromboticevents" style="display:none;"><i class="fa fa-times-circle-o"></i> Select one option!</label>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-8" id="divvenous" name="divvenous" <?php if($thromboticevents=="1" ) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
								 <label>Venous</label>
								  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="venous_tbl" name="venous_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-4"><b>venous</b></th>
											<th class="col-md-3"><b>year</b></th>
											<th class="col-md-1">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
										if($pat_id<>"")
										{
											$order = get_venous($pat_id);
											$num_rows = pg_num_rows($order);
										
											while($result = pg_fetch_assoc($order))
											{
												$venous_id=$result['venous_id'];
												$organ_id=$result['organ_id'];
												$venous_year=$result['venous_year'];
											
											
									?>
										  <tr id="venous_tr_<?php echo $i;?>"> 
											   <td align="center">
													<input type="hidden" id="venous_hidden_<?php echo $i;?>" name="venous_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="venous_id_<?php echo $i; ?>" id="venous_id_<?php echo $i; ?>" value="<?php echo $venous_id;?>">
													<select class="form-control"  name="organ_id_<?php echo $i; ?>" id="organ_id_<?php echo $i; ?>">
														<option value="1" <?php if ($organ_id==1) { echo "selected"; } ?>>limbs</option>
														<option value="2" <?php if ($organ_id==2) { echo "selected"; } ?>>brain/dural sinus</option>
														<option value="3" <?php if ($organ_id==3) { echo "selected"; } ?>>visceral vein</option>
													</select>
												</td>
												<td align="center">
													<input type="text" class="form-control" id="venous_year_<?php echo $i; ?>" data-inputmask="'mask': '9999'" name="venous_year_<?php echo $i; ?>" value="<?php echo $venous_year; ?>">
												</td>
													<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="venous_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
												  </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										}
									?>
									</tbody>
								</table>
								<input type="hidden" name="venous_numofrows" id="venous_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								 <a class="btn btn-primary" href="javascript:void(0);" onclick="venous_addrow();"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
						<div class="row">
							<div class="form-group col-md-8" id="divarterial" name="divarterial" <?php if($thromboticevents=="1" ) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
								 <label>Arterial</label>
								  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="arterial_tbl" name="arterial_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-4"><b>Arterial</b></th>
											<th class="col-md-3"><b>year</b></th>
											<th class="col-md-1">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
										if($pat_id<>"")
										{
											$order = get_arterial($pat_id);
											$num_rows = pg_num_rows($order);
										
											while($result = pg_fetch_assoc($order))
											{
												$arterial_id=$result['arterial_id'];
												$arterial_organ_id=$result['organ_id'];
												$arterial_year=$result['arterial_year'];
											
											
									?>
										  <tr id="arterial_tr_<?php echo $i;?>"> 
											   <td align="center">
													<input type="hidden" id="arterial_hidden_<?php echo $i;?>" name="arterial_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="arterial_id_<?php echo $i; ?>" id="arterial_id_<?php echo $i; ?>" value="<?php echo $arterial_id;?>">
														<select class="form-control"  name="arterial_organ_id_<?php echo $i; ?>" id="arterial_organ_id_<?php echo $i; ?>">
														<option value="1" <?php if ($arterial_organ_id==1) { echo "selected"; } ?>>Limbs</option>
														<option value="2" <?php if ($arterial_organ_id==2) { echo "selected"; } ?>>Pulmonary embolism</option>
														<option value="3" <?php if ($arterial_organ_id==3) { echo "selected"; } ?>>Stroke/TIA</option>
														<option value="4" <?php if ($arterial_organ_id==4) { echo "selected"; } ?>>Coronary</option>
														<option value="5" <?php if ($arterial_organ_id==5) { echo "selected"; } ?>>Renal</option>
														<option value="6" <?php if ($arterial_organ_id==6) { echo "selected"; } ?>>Visceral</option>
													</select>
												</td>
												<td align="center">
													<input type="text" class="form-control" data-inputmask="'mask': '9999'" id="arterial_year_<?php echo $i; ?>" name="arterial_year_<?php echo $i; ?>" value="<?php echo $arterial_year; ?>">
												</td>
													<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="arterial_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
												  </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										}
									?>
									</tbody>
								</table>
								<input type="hidden" name="arterial_numofrows" id="arterial_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								 <a class="btn btn-primary" href="javascript:void(0);" onclick="arterial_addrow();"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
					</div>
				</div><!-- history of thrombotic events div -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Employment</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-3" id="divemployment" name="divemployment">
							  <label>Employment</label>
							  <select class="form-control" name="employment" id="employment">
								<option value="0" <?php if ($employment==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($employment==1) { echo "selected"; } ?>>Reported</option>
								<option value="2" <?php if ($employment==2) { echo "selected"; } ?>>Not-reported</option>
							  </select>
							  <label class="control-label" for="inputError" id="Erroremployment" name="Erroremployment" style="display:none;"><i class="fa fa-times-circle-o"></i> Select one option!</label>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3" id="divemploymentstatus" name="divemploymentstatus" <?php if($employment=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>Employment status at inclusion</label>
							  <select class="form-control" name="employmentstatus" id="employmentstatus">
								<option value="0" <?php if ($employmentstatus==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($employmentstatus==1) { echo "selected"; } ?>>Unemployed</option>
								<option value="2" <?php if ($employmentstatus==2) { echo "selected"; } ?>>Student</option>
								<option value="3" <?php if ($employmentstatus==3) { echo "selected"; } ?>>Employee</option>
								<option value="4" <?php if ($employmentstatus==4) { echo "selected"; } ?>>Worker</option>
								<option value="5" <?php if ($employmentstatus==5) { echo "selected"; } ?>>Self-employed</option>
								<option value="6" <?php if ($employmentstatus==6) { echo "selected"; } ?>>Retired</option>
								<option value="7" <?php if ($employmentstatus==7) { echo "selected"; } ?>>Disability pension</option>
								<option value="9" <?php if ($employmentstatus==9) { echo "selected"; } ?>>Housewife</option>
								<option value="8" <?php if ($employmentstatus==8) { echo "selected"; } ?>>Unknown</option>
								
							  </select>
							  <label class="control-label" for="inputError" id="Erroremploymentstatus" name="Erroremploymentstatus" style="display:none;"><i class="fa fa-times-circle-o"></i> Select one option!</label>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3" id="divworktype" name="divworktype" <?php if($employmentstatus=="3" or $employmentstatus=="4" or $employmentstatus=="5") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>Work type</label>
							  <select class="form-control" name="worktype" id="worktype">
								<option value="0" <?php if ($worktype==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($worktype==1) { echo "selected"; } ?>>White-colar</option>
								<option value="2" <?php if ($worktype==2) { echo "selected"; } ?>>Pink-colar</option>
								<option value="3" <?php if ($worktype==3) { echo "selected"; } ?>>Blue-colar</option>
								
							  </select>
							  <label class="control-label" for="inputError" id="Errorworktype" name="Errorworktype" style="display:none;"><i class="fa fa-times-circle-o"></i> Select one option!</label>
							</div>
							<div class="form-group col-md-3" id="divjob" name="divjob" <?php if($employmentstatus=="3" or $employmentstatus=="4" or $employmentstatus=="5") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
								<label>Job</label>
								<input type="text" class="form-control" id="job" name="job" value="<?php echo $job; ?>">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-5" id="divpreviousemployment" name="divpreviousemployment" <?php if($employment=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>Has the patient had a previous employment status which was changed due to the rheumatic disease?</label>
							  <select class="form-control" name="previousemployment" id="previousemployment">
								<option value="0" <?php if ($previousemployment==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($previousemployment==1) { echo "selected"; } ?>>Yes</option>
								<option value="2" <?php if ($previousemployment==2) { echo "selected"; } ?>>No </option>
								<option value="3" <?php if ($previousemployment==3) { echo "selected"; } ?>>Unknown</option>
							  </select>
							  <label class="control-label" for="inputError" id="Errorpreviousemployment" name="Errorpreviousemployment" style="display:none;"><i class="fa fa-times-circle-o"></i> Select one option!</label>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3" id="divpreviousemploymentdate" name="divpreviousemploymentdate" <?php if($previousemployment=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
								<label>date</label>
								<div class="input-group">
								  <div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								  </div>
								  <input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" value="<?php echo $previousemploymentdate_str; ?>" id="previousemploymentdate" name="previousemploymentdate">
								
								</div>
							</div>
							<div class="form-group col-md-3" id="divpreviousemploymentstatus" name="divpreviousemploymentstatus" <?php if($previousemployment=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>Previous employment status</label>
							  <select class="form-control" name="previousemploymentstatus" id="previousemploymentstatus">
								<option value="0" <?php if ($previousemploymentstatus==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($previousemploymentstatus==1) { echo "selected"; } ?>>Unemployed</option>
								<option value="2" <?php if ($previousemploymentstatus==2) { echo "selected"; } ?>>Student</option>
								<option value="3" <?php if ($previousemploymentstatus==3) { echo "selected"; } ?>>Employee</option>
								<option value="4" <?php if ($previousemploymentstatus==4) { echo "selected"; } ?>>Worker</option>
								<option value="5" <?php if ($previousemploymentstatus==5) { echo "selected"; } ?>>Self-employed</option>
								<option value="6" <?php if ($previousemploymentstatus==6) { echo "selected"; } ?>>Retired</option>
								<option value="7" <?php if ($previousemploymentstatus==7) { echo "selected"; } ?>>Disability pension</option>
								<option value="9" <?php if ($previousemploymentstatus==9) { echo "selected"; } ?>>Housewife</option>
								<option value="8" <?php if ($previousemploymentstatus==8) { echo "selected"; } ?>>Unknown</option>
							  </select>
							  <label class="control-label" for="inputError" id="Erroremploymentstatus" name="Erroremploymentstatus" style="display:none;"><i class="fa fa-times-circle-o"></i> Select one option!</label>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3" id="divpreviousworktype" name="divpreviousworktype" <?php if($previousemploymentstatus=="3" or $previousemploymentstatus=="4" or $previousemploymentstatus=="5") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>Previous Work type</label>
							  <select class="form-control" name="previousworktype" id="previousworktype">
								<option value="0" <?php if ($previousworktype==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($previousworktype==1) { echo "selected"; } ?>>White-colar</option>
								<option value="2" <?php if ($previousworktype==2) { echo "selected"; } ?>>Pink-colar</option>
								<option value="3" <?php if ($previousworktype==3) { echo "selected"; } ?>>Blue-colar</option>
								
							  </select>
							  <label class="control-label" for="inputError" id="Errorpreviousworktype" name="Errorpreviousworktype" style="display:none;"><i class="fa fa-times-circle-o"></i> Select one option!</label>
							</div>
							<div class="form-group col-md-3" id="divpreviousjob" name="divpreviousjob" <?php if($previousemploymentstatus=="3" or $previousemploymentstatus=="4" or $previousemploymentstatus=="5") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
								<label>Previous Job</label>
								<input type="text" class="form-control" id="previousjob" name="previousjob" value="<?php echo $previousjob; ?>">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-5" id="divfollowupemployment" name="divfollowupemployment" <?php if($employment=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>Has the employment status at inclusion changed during follow up?</label>
							  <select class="form-control" name="followupemployment" id="followupemployment">
								<option value="0" <?php if ($followupemployment==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($followupemployment==1) { echo "selected"; } ?>>Yes</option>
								<option value="2" <?php if ($followupemployment==2) { echo "selected"; } ?>>No </option>
								<option value="3" <?php if ($followupemployment==3) { echo "selected"; } ?>>Unknown</option>
							  </select>
							  <label class="control-label" for="inputError" id="Errorfollowupemployment" name="Errorfollowupemployment" style="display:none;"><i class="fa fa-times-circle-o"></i> Select one option!</label>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3" id="divfollowupemploymentdate" name="divfollowupemploymentdate" <?php if($followupemployment=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
								<label>date</label>
								<div class="input-group">
								  <div class="input-group-addon">
									<i class="fa fa-calendar"></i>
								  </div>
									<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" value="<?php echo $followupemploymentdate_str; ?>" id="followupemploymentdate" name="followupemploymentdate">
								</div>
							</div>
							<div class="form-group col-md-3" id="divfollowupemploymentstatus" name="divfollowupemploymentstatus" <?php if($followupemployment=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>New employment status</label>
							  <select class="form-control" name="followupemploymentstatus" id="followupemploymentstatus">
								<option value="0" <?php if ($followupemploymentstatus==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($followupemploymentstatus==1) { echo "selected"; } ?>>Unemployed</option>
								<option value="2" <?php if ($followupemploymentstatus==2) { echo "selected"; } ?>>Student</option>
								<option value="3" <?php if ($followupemploymentstatus==3) { echo "selected"; } ?>>Employee</option>
								<option value="4" <?php if ($followupemploymentstatus==4) { echo "selected"; } ?>>Worker</option>
								<option value="5" <?php if ($followupemploymentstatus==5) { echo "selected"; } ?>>Self-employed</option>
								<option value="6" <?php if ($followupemploymentstatus==6) { echo "selected"; } ?>>Retired</option>
								<option value="7" <?php if ($followupemploymentstatus==7) { echo "selected"; } ?>>Disability pension</option>
								<option value="9" <?php if ($followupemploymentstatus==9) { echo "selected"; } ?>>Housewife</option>
								<option value="8" <?php if ($followupemploymentstatus==8) { echo "selected"; } ?>>Unknown</option>
							  </select>
							  <label class="control-label" for="inputError" id="Errorfollowupemploymentstatus" name="Errorfollowupemploymentstatus" style="display:none;"><i class="fa fa-times-circle-o"></i> Select one option!</label>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3" id="divfollowupworktype" name="divfollowupworktype" <?php if($followupemploymentstatus=="3" or $followupemploymentstatus=="4" or $followupemploymentstatus=="5") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>New Work type</label>
							  <select class="form-control" name="followupworktype" id="followupworktype">
								<option value="0" <?php if ($followupworktype==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($followupworktype==1) { echo "selected"; } ?>>White-colar</option>
								<option value="2" <?php if ($followupworktype==2) { echo "selected"; } ?>>Pink-colar</option>
								<option value="3" <?php if ($followupworktype==3) { echo "selected"; } ?>>Blue-colar</option>
								
							  </select>
							  <label class="control-label" for="inputError" id="Errorfollowupworktype" name="Errorfollowupworktype" style="display:none;"><i class="fa fa-times-circle-o"></i> Select one option!</label>
							</div>
							<div class="form-group col-md-3" id="divfollowupjob" name="divfollowupjob" <?php if($followupemploymentstatus=="3" or $followupemploymentstatus=="4" or $followupemploymentstatus=="5") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
								<label>New Job</label>
								<input type="text" class="form-control" id="followupjob" name="followupjob" value="<?php echo $followupjob; ?>">
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-5" id="divjobchangerelated" name="divjobchangerelated" <?php if($employment=="1") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							  <label>Was the change related to the rheumatic disease?</label>
							  <select class="form-control" name="jobchangerelated" id="jobchangerelated">
								<option value="0" <?php if ($jobchangerelated==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($jobchangerelated==1) { echo "selected"; } ?>>Yes</option>
								<option value="2" <?php if ($jobchangerelated==2) { echo "selected"; } ?>>No </option>
								<option value="3" <?php if ($jobchangerelated==3) { echo "selected"; } ?>>Unknown</option>
							  </select>
							  <label class="control-label" for="inputError" id="Errorjobchangerelated" name="Errorjobchangerelated" style="display:none;"><i class="fa fa-times-circle-o"></i> Select one option!</label>
							</div>
						</div>
					</div>
				</div><!-- Employment div -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Marital status</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-8" id="divmarital" name="divmarital">
								  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="marital_tbl" name="marital_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-3"><b>Status</b></th>
											<th class="col-md-3"><b>Year</b></th>
											<th class="col-md-2">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
										if($pat_id<>"")
										{
											$order = get_marital($pat_id);
											$num_rows = pg_num_rows($order);
										
											while($result = pg_fetch_assoc($order))
											{
												$marital_id=$result['marital_id'];
												$marital_status=$result['marital_status'];
												$marital_year=$result['marital_year'];
											
											
									?>
										  <tr id="marital_tr_<?php echo $i;?>"> 
											   <td align="center">
													<input type="hidden" id="marital_hidden_<?php echo $i;?>" name="marital_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="marital_id_<?php echo $i; ?>" id="marital_id_<?php echo $i; ?>" value="<?php echo $marital_id;?>">
													<select class="form-control" name="marital_status_<?php echo $i; ?>" id="marital_status_<?php echo $i; ?>">
														<option value="1" <?php if ($marital_status==1) { echo "selected"; } ?>>Not married</option>
														<option value="2" <?php if ($marital_status==2) { echo "selected"; } ?>>Married</option>
														<option value="3" <?php if ($marital_status==3) { echo "selected"; } ?>>Divorced</option>
														<option value="4" <?php if ($marital_status==4) { echo "selected"; } ?>>Widow</option>
														<option value="5" <?php if ($marital_status==5) { echo "selected"; } ?>>Unknown</option>
													</select>
												</td>
												<td align="center">
													<input type="text" class="form-control pull-right" data-inputmask="'mask': '9999'" value="<?php echo $marital_year; ?>" id="marital_year_<?php echo $i; ?>" name="marital_year_<?php echo $i; ?>">
												</td>
													<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="marital_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
												  </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										}
									?>
									</tbody>
								</table>
								<input type="hidden" name="marital_numofrows" id="marital_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								 <a class="btn btn-primary" href="javascript:void(0);" onclick="marital_addrow();"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
					</div>
				</div><!-- marital status div -->
				<!--<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Environmental exposures</h3>
					</div>
					<div class="box-body">	
						<div class="row">
							<div class="form-group col-md-5" id="divtobacco" name="divtobacco">
							  <label>Tobacco use-Has the patient ever smoked daily for at least one year?</label>
							  <select class="form-control" name="tobacco" id="tobacco">
								<option value="0" <?php if ($tobacco==0) { echo "selected"; } ?>>--</option>
								<option value="1" <?php if ($tobacco==1) { echo "selected"; } ?>>Unknown</option>
								<option value="2" <?php if ($tobacco==2) { echo "selected"; } ?>>Yes</option>
								<option value="3" <?php if ($tobacco==3) { echo "selected"; } ?>>No</option>
							  </select>
							  <label class="control-label" for="inputError" id="Errortobacco" name="Errortobacco" style="display:none;"><i class="fa fa-times-circle-o"></i> Select one option!</label>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3" id="divsmokingstarting" name="divsmokingstarting" <?php if($tobacco=="2") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
								<label>Year that the patient started smoking</label>
									<div class="input-group">
									  <input type="text" class="form-control" id="smokingstarting" name="smokingstarting" value="<?php echo $smokingstarting; ?>">
									</div>
							</div>
							<div class="form-group col-md-3" id="divsmokingstopped" name="divsmokingstopped" <?php if($tobacco=="2") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							 <label>Did the patient stop smoking?</label>
								<select class="form-control" name="smokingstopped" id="smokingstopped">
									<option value="0" <?php if ($smokingstopped==0) { echo "selected"; } ?>>--</option>
									<option value="1" <?php if ($smokingstopped==1) { echo "selected"; } ?>>Yes</option>
									<option value="2" <?php if ($smokingstopped==2) { echo "selected"; } ?>>No</option>
								</select>
							</div>
							<div  id="divsmokingstoppedyear" name="divsmokingstoppedyear" <?php if($smokingstopped==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
								<label>Year</label>
								<div class="input-group">
								  <input type="text" class="form-control" id="smokingstoppedyear" name="smokingstoppedyear" value="<?php echo $smokingstoppedyear; ?>">
								</div>
							  </div>
						</div>
						<div class="row">
							<div class="form-group col-md-12" id="divsmokingfirstdecade" name="divsmokingfirstdecade" <?php if($tobacco=="2") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
							 <div class="row">
								<div class="form-group col-md-12">
									<label>How many packs/day has the patient approximately smoked per decade</label>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-md-1">
									<label>1st</label>
									  <select class="form-control" name="decadefirst" id="decadefirst">
										<option value="0" <?php if ($decadefirst==0) { echo "selected"; } ?>>0</option>
										<option value="1" <?php if ($decadefirst==1) { echo "selected"; } ?>>1</option>
										<option value="2" <?php if ($decadefirst==2) { echo "selected"; } ?>>2</option>
										<option value="3" <?php if ($decadefirst==3) { echo "selected"; } ?>>3</option>
										<option value="4" <?php if ($decadefirst==4) { echo "selected"; } ?>>4</option>
										<option value="5" <?php if ($decadefirst==5) { echo "selected"; } ?>>5</option>
									  </select>
								</div>
								<div class="form-group col-md-1">
									<label>2nd</label>
									  <select class="form-control" name="decadesecond" id="decadesecond">
										<option value="0" <?php if ($decadesecond==0) { echo "selected"; } ?>>0</option>
										<option value="1" <?php if ($decadesecond==1) { echo "selected"; } ?>>1</option>
										<option value="2" <?php if ($decadesecond==2) { echo "selected"; } ?>>2</option>
										<option value="3" <?php if ($decadesecond==3) { echo "selected"; } ?>>3</option>
										<option value="4" <?php if ($decadesecond==4) { echo "selected"; } ?>>4</option>
										<option value="5" <?php if ($decadesecond==5) { echo "selected"; } ?>>5</option>
									  </select>
								</div>
								<div class="form-group col-md-1">
									<label>3rd</label>
									  <select class="form-control" name="decadethird" id="decadethird">
										<option value="0" <?php if ($decadethird==0) { echo "selected"; } ?>>0</option>
										<option value="1" <?php if ($decadethird==1) { echo "selected"; } ?>>1</option>
										<option value="2" <?php if ($decadethird==2) { echo "selected"; } ?>>2</option>
										<option value="3" <?php if ($decadethird==3) { echo "selected"; } ?>>3</option>
										<option value="4" <?php if ($decadethird==4) { echo "selected"; } ?>>4</option>
										<option value="5" <?php if ($decadethird==5) { echo "selected"; } ?>>5</option>
									  </select>
								</div>
								<div class="form-group col-md-1">
									<label>4th</label>
									  <select class="form-control" name="decadeforth" id="decadeforth">
										<option value="0" <?php if ($decadeforth==0) { echo "selected"; } ?>>0</option>
										<option value="1" <?php if ($decadeforth==1) { echo "selected"; } ?>>1</option>
										<option value="2" <?php if ($decadeforth==2) { echo "selected"; } ?>>2</option>
										<option value="3" <?php if ($decadeforth==3) { echo "selected"; } ?>>3</option>
										<option value="4" <?php if ($decadeforth==4) { echo "selected"; } ?>>4</option>
										<option value="5" <?php if ($decadeforth==5) { echo "selected"; } ?>>5</option>
									  </select>
								</div>
								<div class="form-group col-md-1">
									<label>5th</label>
									  <select class="form-control" name="decadefifth" id="decadefifth">
										<option value="0" <?php if ($decadefifth==0) { echo "selected"; } ?>>0</option>
										<option value="1" <?php if ($decadefifth==1) { echo "selected"; } ?>>1</option>
										<option value="2" <?php if ($decadefifth==2) { echo "selected"; } ?>>2</option>
										<option value="3" <?php if ($decadefifth==3) { echo "selected"; } ?>>3</option>
										<option value="4" <?php if ($decadefifth==4) { echo "selected"; } ?>>4</option>
										<option value="5" <?php if ($decadefifth==5) { echo "selected"; } ?>>5</option>
									  </select>
								</div>
								<div class="form-group col-md-1">
									<label>6th</label>
									  <select class="form-control" name="decadesixth" id="decadesixth">
										<option value="0" <?php if ($decadesixth==0) { echo "selected"; } ?>>0</option>
										<option value="1" <?php if ($decadesixth==1) { echo "selected"; } ?>>1</option>
										<option value="2" <?php if ($decadesixth==2) { echo "selected"; } ?>>2</option>
										<option value="3" <?php if ($decadesixth==3) { echo "selected"; } ?>>3</option>
										<option value="4" <?php if ($decadesixth==4) { echo "selected"; } ?>>4</option>
										<option value="5" <?php if ($decadesixth==5) { echo "selected"; } ?>>5</option>
									  </select>
								</div>
								<div class="form-group col-md-1">
									<label>7th</label>
									  <select class="form-control" name="decadeseventh" id="decadeseventh">
										<option value="0" <?php if ($decadeseventh==0) { echo "selected"; } ?>>0</option>
										<option value="1" <?php if ($decadeseventh==1) { echo "selected"; } ?>>1</option>
										<option value="2" <?php if ($decadeseventh==2) { echo "selected"; } ?>>2</option>
										<option value="3" <?php if ($decadeseventh==3) { echo "selected"; } ?>>3</option>
										<option value="4" <?php if ($decadeseventh==4) { echo "selected"; } ?>>4</option>
										<option value="5" <?php if ($decadeseventh==5) { echo "selected"; } ?>>5</option>
									  </select>
								</div>
								<div class="form-group col-md-1">
									<label>8th</label>
									  <select class="form-control" name="decadeeight" id="decadeeight">
										<option value="0" <?php if ($decadeeight==0) { echo "selected"; } ?>>0</option>
										<option value="1" <?php if ($decadeeight==1) { echo "selected"; } ?>>1</option>
										<option value="2" <?php if ($decadeeight==2) { echo "selected"; } ?>>2</option>
										<option value="3" <?php if ($decadeeight==3) { echo "selected"; } ?>>3</option>
										<option value="4" <?php if ($decadeeight==4) { echo "selected"; } ?>>4</option>
										<option value="5" <?php if ($decadeeight==5) { echo "selected"; } ?>>5</option>
									  </select>
								</div>
							</div>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-md-3" id="divaveragepackperday" name="divaveragepackperday" <?php if($tobacco=="2") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
								<label>Average packs/day</label>
									<div class="input-group">
									  <input type="text" readonly=true class="form-control" id="averagepackperday" name="averagepackperday" value="<?php echo $averagepackperday; ?>">
									</div>
							</div>
							<div class="form-group col-md-3" id="divaveragepackperyear" name="divaveragepackperyear" <?php if($tobacco=="2") { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
								<label>Average packs/year</label>
									<div class="input-group">
									  <input type="text" readonly=true class="form-control" id="averagepackperyear" name="averagepackperyear" value="<?php echo $averagepackperyear; ?>">
									</div>
							</div>
						</div>
					</div>
				</div>--><!-- Environmental exposures div -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Physical activity</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-10" id="divphysical" name="divphysical">
								 <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="physical_tbl" name="physical_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-6"><b>Activity during the previous year</b></th>
											<th class="col-md-2"><b>Date</b></th>
											<th class="col-md-2">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
										if($pat_id<>"")
										{
											$order = get_physical($pat_id);
											$num_rows = pg_num_rows($order);
											
											while($result = pg_fetch_assoc($order))
											{
												$physical_id=$result['physical_id'];
												$physical_status=$result['status'];
												$date_phys_str=$result['date_str'];
												if ($date_phys_str=='12-12-1900')
													$date_phys_str='';
											
											
									?>
										  <tr id="physical_tr_<?php echo $i;?>"> 
											   <td align="center">
													<input type="hidden" id="physical_hidden_<?php echo $i;?>" name="physical_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="physical_id_<?php echo $i; ?>" id="physical_id_<?php echo $i; ?>" value="<?php echo $physical_id;?>">
													<select class="form-control" name="physical_status_<?php echo $i; ?>" id="physical_status_<?php echo $i; ?>">
														<option value="1" <?php if ($physical_status==1) { echo "selected"; } ?>>Moving only for necesary chores</option>
														<option value="2" <?php if ($physical_status==2) { echo "selected"; } ?>>Walking or other outdoor activities 1-2 times per week</option>
														<option value="3" <?php if ($physical_status==3) { echo "selected"; } ?>>Walking or other outdoor activities several times per week</option>
														<option value="4" <?php if ($physical_status==4) { echo "selected"; } ?>>Exercing 1-2 times per week to the point of perspiring and heavy breathing</option>
														<option value="5" <?php if ($physical_status==5) { echo "selected"; } ?>>Exercing several times per week to the point of perspiring and heavy breathing</option>
														<option value="6" <?php if ($physical_status==6) { echo "selected"; } ?>>Keep-fit heavy exercise ot competitive sport several times per week</option>
													</select>
												</td>
												<td align="center">
													<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" value="<?php echo $date_phys_str; ?>" id="date_phys_str_<?php echo $i;?>" name="date_phys_str_<?php echo $i;?>">
												</td>
													<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="physical_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
												  </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										}
									?>
									</tbody>
								</table>
								<input type="hidden" name="physical_numofrows" id="physical_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								 <a class="btn btn-primary" href="javascript:void(0);" onclick="physical_addrow();"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
					</div>
				</div><!-- Physical activity div -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Mediterranean diet score</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-10" id="divmediterranean" name="divmediterranean">
								 <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="mediterranean_tbl" name="mediterranean_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-6"><b>There is consumption of</b></th>
											<th class="col-md-2"><b>Date</b></th>
											<th class="col-md-2">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
										if($pat_id<>"")
										{
											$order = get_mediterranean($pat_id);
											$num_rows = pg_num_rows($order);
											
											while($result = pg_fetch_assoc($order))
											{
												$mediterranean_id=$result['mediterranean_id'];
												$mediterranean_status=$result['status'];
												$mediterranean_year=$result['year'];
												$date_med_str=$result['date_str'];
												if ($date_med_str=='12-12-1900')
													$date_med_str='';
											
									?>
										  <tr id="mediterranean_tr_<?php echo $i;?>"> 
											   <td align="center">
													<input type="hidden" id="mediterranean_hidden_<?php echo $i;?>" name="mediterranean_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="mediterranean_id_<?php echo $i; ?>" id="mediterranean_id_<?php echo $i; ?>" value="<?php echo $mediterranean_id;?>">
													<select class="form-control" name="mediterranean_status_<?php echo $i; ?>" id="mediterranean_status_<?php echo $i; ?>">
														<option value="1" <?php if ($mediterranean_status==1) { echo "selected"; } ?>>Olive oil (>=1 spoon per day)</option>
														<option value="2" <?php if ($mediterranean_status==2) { echo "selected"; } ?>>Fruit (>=1 serving per day)</option>
														<option value="3" <?php if ($mediterranean_status==3) { echo "selected"; } ?>>Vegetables or salad (>=1 serving per day)</option>
														<option value="4" <?php if ($mediterranean_status==4) { echo "selected"; } ?>>Fruit (>=1 serving/day) AND vegetables (>=1 serving/day)</option>
														<option value="5" <?php if ($mediterranean_status==5) { echo "selected"; } ?>>Legumes (>=2 servings per week)</option>
														<option value="6" <?php if ($mediterranean_status==6) { echo "selected"; } ?>>Fish (>=3 servings per week)</option>
														<option value="7" <?php if ($mediterranean_status==7) { echo "selected"; } ?>>Wine (>=1 glass per day)</option>
														<option value="8" <?php if ($mediterranean_status==8) { echo "selected"; } ?>>Red meat (<1 serving per day)</option>
														<option value="9" <?php if ($mediterranean_status==9) { echo "selected"; } ?>>White bread (<1/day) AND rice (<1/week) OR, whole-grain bread (>5/week)</option>
													</select>
												</td>
												<td align="center">
													<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" value="<?php echo $date_med_str; ?>" id="date_med_str_<?php echo $i;?>" name="date_med_str_<?php echo $i;?>">
												</td>
													<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="mediterranean_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
												  </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										}
									?>
									</tbody>
								</table>
								<input type="hidden" name="mediterranean_numofrows" id="mediterranean_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								 <a class="btn btn-primary" href="javascript:void(0);" onclick="mediterranean_addrow();"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
					</div>
				</div><!-- mediterranean activity div -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Alcohol consumption</h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-10" id="divmalcohol" name="divalcohol">
								 <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="alcohol_tbl" name="alcohol_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-6"><b>Beverage</b></th>
											<th class="col-md-4"><b>Dose</b></th>
											<th class="col-md-2">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
										if($pat_id<>"")
										{
											$order = get_alcohol($pat_id);
											$num_rows = pg_num_rows($order);
											
											while($result = pg_fetch_assoc($order))
											{
												$patient_alcohol_id=$result['patient_alcohol_id'];
												$type=$result['type'];
												$dose=$result['dose'];
											
									?>
										  <tr id="alcohol_tr_<?php echo $i;?>"> 
											   <td align="center">
													<input type="hidden" id="alcohol_hidden_<?php echo $i;?>" name="alcohol_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="patient_alcohol_id_<?php echo $i; ?>" id="patient_alcohol_id_<?php echo $i; ?>" value="<?php echo $patient_alcohol_id;?>">
													<select class="form-control" name="type_<?php echo $i; ?>" id="type_<?php echo $i; ?>">
														<option value="1" <?php if ($type==1) { echo "selected"; } ?>>Beer</option>
														<option value="2" <?php if ($type==2) { echo "selected"; } ?>>Wine</option>
														<option value="3" <?php if ($type==3) { echo "selected"; } ?>>Liquor</option>
													</select>
												</td>
												<td align="center">
													<select class="form-control" name="dose_<?php echo $i; ?>" id="dose_<?php echo $i; ?>">
														<option value="1" <?php if ($dose==1) { echo "selected"; } ?>>None or <1/month</option>
														<option value="2" <?php if ($dose==2) { echo "selected"; } ?>>1 to 3 per month</option>
														<option value="3" <?php if ($dose==3) { echo "selected"; } ?>>1 per week</option>
														<option value="4" <?php if ($dose==4) { echo "selected"; } ?>>2 to 4 per week</option>
														<option value="5" <?php if ($dose==5) { echo "selected"; } ?>>5 to 6 per week</option>
														<option value="6" <?php if ($dose==6) { echo "selected"; } ?>>1 per day</option>
														<option value="7" <?php if ($dose==7) { echo "selected"; } ?>>2 to 3 per day</option>
														<option value="8" <?php if ($dose==8) { echo "selected"; } ?>>4 to 5 per day</option>
														<option value="9" <?php if ($dose==9) { echo "selected"; } ?>>>=6 per day</option>
													</select>
												</td>
													<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="alcohol_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
												  </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										}
									?>
									</tbody>
								</table>
								<input type="hidden" name="alcohol_numofrows" id="alcohol_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								 <a class="btn btn-primary" href="javascript:void(0);" onclick="alcohol_addrow();"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
					</div>
				</div><!-- alcohol activity div -->
				<div class="row">
					<div class="form-group col-md-12">
						<input type="button" id="submit2" class="btn btn-primary" value="Save">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						&nbsp;
					</div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
						&nbsp;
					</div>
				</div>
			</div>
		</div>
		</form>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
   <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>

$(document).ajaxStart(function(){
		$('body').loading();
		
    });
    $(document).ajaxComplete(function(){
       $('body').loading('stop');
    });
	
$(function () {
	  
	  $('#submit2').click(function(){
		
				var msg="";
				var patient_id=$("#patient_id").val();
				var pat_id=$("#pat_id").val();
				var save=1;
				
				$.ajax({
				url: 'patientid_check.php',
				type: 'post',
				data: {
					pat_id:pat_id,
					patient_id:patient_id,
					},
				dataType: 'json',
				success:function(response){
					var len = response.length;
					if(len>0)
					{	
						var gen = response[0]['gender'];
						var dateofbirth = response[0]['dateofbirth'];
						alert("There is another patient with the same patient id (" + gen + " - " + dateofbirth + ")! Use other patient id!");
						return false;
					}
					else
					{
						var gender=$('#gender').val();
						var dateofbirth=$('#dateofbirth').val();
						var dateofinclusion=$('#dateofinclusion').val();
						var centre=$('#centre').val();
						
						if(isNaN($("#patient_id").val()))
							msg +='-Patient id must be integer!\n';
						if (gender==0)
							msg +='-Select patient  gender!\n';
						if (dateofbirth=="")
							msg +='-Fill patient date of birth!\n';
						if (dateofinclusion=="")
							msg +='-Fill patient date of inclusion!\n';
						if (centre==0)
							msg +='-Select center!\n';
					
						if(msg != '')
						{
							alert(msg);
							return false;
						}
						else
							 $('#form').submit();
					}
					
				},
				failure: function (response) {
					alert(response.d);
					return false;
				}
				
			});
			
			
		}); 
		
	  $('#submit1').click(function(){
		
				var msg="";
				var patient_id=$("#patient_id").val();
				var pat_id=$("#pat_id").val();
				var save=1;
				
				$.ajax({
				url: 'patientid_check.php',
				type: 'post',
				data: {
					pat_id:pat_id,
					patient_id:patient_id,
					},
				dataType: 'json',
				success:function(response){
					var len = response.length;
					if(len>0)
					{	
						var gen = response[0]['gender'];
						var dateofbirth = response[0]['dateofbirth'];
						alert("There is another patient with the same patient id (" + gen + " - " + dateofbirth + ")! Use other patient id!");
						return false;
					}
					else
					{
						var gender=$('#gender').val();
						var dateofbirth=$('#dateofbirth').val();
						var dateofinclusion=$('#dateofinclusion').val();
						var centre=$('#centre').val();
						
						if(isNaN($("#patient_id").val()))
							msg +='-Patient id must be integer!\n';
						if (gender==0)
							msg +='-Select patient  gender!\n';
						if (dateofbirth=="")
							msg +='-Fill patient date of birth!\n';
						if (dateofinclusion=="")
							msg +='-Fill patient date of inclusion!\n';
						if (centre==0)
							msg +='-Select center!\n';
					
						if(msg != '')
						{
							alert(msg);
							return false;
						}
						else
							 $('#form').submit();
					}
					
				},
				failure: function (response) {
					alert(response.d);
					return false;
				}
				
			});
			
			
		}); 
	  });	
function alcohol_addrow()
{
  var tbl = document.getElementById('alcohol_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'alcohol_tr_'+iteration;
  document.getElementById('alcohol_numofrows').value = iteration;
  
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('select');
  el1.name = 'type_' + iteration;
  el1.id = 'type_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
  
	var opt=new Option("Beer",1);
	el1.add(opt,undefined);
	var opt=new Option("Wine",2);
	el1.add(opt,undefined);
	var opt=new Option("Liquor",3);
	el1.add(opt,undefined);
  													
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'alcohol_hidden_' + iteration;
  el2.id = 'alcohol_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'patient_alcohol_id_' + iteration;
  el3.id = 'patient_alcohol_id_' + iteration;
  el3.value='';
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
  var r2 = row.insertCell(1);
  r2.align='center';
  
	var el4 = document.createElement('select');
	el4.name = 'dose_' + iteration;
	el4.id = 'dose_' + iteration;
	el4.style.width='100%';
	el4.className='form-control';
  
	var opt=new Option("None or <1/month",1);
	el4.add(opt,undefined);
	var opt=new Option("1 to 3 per month",2);
	el4.add(opt,undefined);
	var opt=new Option("1 per week",3);
	el4.add(opt,undefined);
	var opt=new Option("2 to 4 per week",4);
	el4.add(opt,undefined);
	var opt=new Option("5 to 6 per week",5);
	el4.add(opt,undefined);
	var opt=new Option("1 per day",6);
	el4.add(opt,undefined);
	var opt=new Option("2 to 3 per day",7);
	el4.add(opt,undefined);
	var opt=new Option("4 to 5 per day",8);
	el4.add(opt,undefined);
	var opt=new Option(">=6 per day",9);
	el4.add(opt,undefined);
  
  r2.appendChild(el4);
  
  var r3 = row.insertCell(2);
  r3.align='center';
  
  var el5 = document.createElement('a');
  el5.title = 'Delete';
  el5.href="javascript:avoid(0)";
  var icon = document.createElement('i');
  icon.className='fa fa-trash-o';
  el5.appendChild(icon);
  el5.onclick =function(){alcohol_removerow(iteration)};
		 
  r3.appendChild(el5);
}

function alcohol_removerow(row)
{
	document.getElementById('alcohol_hidden_'+row).value='-1';
	document.getElementById('alcohol_tr_'+row).style.display='none';
}


function resindence_addrow()
{
  var tbl = document.getElementById('resindence_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'resindence_tr_'+iteration;
  document.getElementById('resindence_numofrows').value = iteration;
  
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('select');
  el1.name = 'resindence_' + iteration;
  el1.id = 'resindence_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
  
															
	var opt=new Option("Urban (>15.000 dwellers)",1);
	el1.add(opt,undefined);
	var opt=new Option("Suburban (10-15.000 dwellers)",2);
	el1.add(opt,undefined);
	var opt=new Option("Rural (<10.000 dwellers)",3);
	el1.add(opt,undefined);
	var opt=new Option("Don't know",4);
	el1.add(opt,undefined);
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'resindence_hidden_' + iteration;
  el2.id = 'resindence_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'resindence_id_' + iteration;
  el3.id = 'resindence_id_' + iteration;
  el3.value='';
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
  var r2 = row.insertCell(1);
  r2.align='center';
  var el4 = document.createElement('input');
  el4.type = 'text';
  el4.name = 'resindencedate_from_' + iteration;
  el4.id = 'resindencedate_from_' + iteration;
  el4.style.width='100%';
  el4.className='form-control';
  //el4.readOnly=true;
  
  
  r2.appendChild(el4);
  
  var r3 = row.insertCell(2);
  r3.align='center';
  var el5 = document.createElement('input');
  el5.type = 'text';
  el5.name = 'resindencedate_to_' + iteration;
  el5.id = 'resindencedate_to_' + iteration;
  el5.style.width='100%';
  el5.className='form-control';
  //el5.readOnly=true;
  
  
  r3.appendChild(el5);
  
  var r4 = row.insertCell(3);
  r4.align='center';
  
  var el6 = document.createElement('a');
  el6.name = 'a_' + iteration;
  el6.id = 'a_' + iteration;
  el6.title = 'Delete';
  el6.href="javascript:avoid(0)";
  var icon = document.createElement('i');
  icon.className='fa fa-trash-o';
  el6.appendChild(icon);
  el6.onclick =function(){resindence_removerow(iteration)};
		 
  r4.appendChild(el6);
  
  $('#resindencedate_from_'+ iteration).datepicker({
		  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  $('#resindencedate_from_' + iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
  
  $('#resindencedate_to_'+ iteration).datepicker({
		 dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
  })
  $('#resindencedate_to_' + iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
  
}

function resindence_removerow(row)
{
	document.getElementById('resindence_hidden_'+row).value='-1';
	document.getElementById('resindence_tr_'+row).style.display='none';
}


jQuery(document).ready(function() {

for(i=1;i<=document.getElementById('resindence_numofrows').value;i++)
{
	$('#resindencedate_from_'+i).datepicker({
	  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	});
	$('#resindencedate_from_' + i).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
	
	$('#resindencedate_to_'+i).datepicker({
	  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	});
	$('#resindencedate_to_' + i).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
}
	
for(i=1;i<=document.getElementById('mediterranean_numofrows').value;i++)
{
	$('#date_med_str_'+i).datepicker({
	dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	});
	$('#date_med_str_'+i).inputmask();
}

for(i=1;i<=document.getElementById('physical_numofrows').value;i++)
{
	$('#date_phys_str_'+i).datepicker({
		dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	});
	$('#date_phys_str_'+i).inputmask();
}

$('#menopauseyear').inputmask();
	

	$('#childbirthsyear').inputmask();
	
	$('#yearofmenarche').inputmask();
	
	$('#dateofinclusion').datepicker({
		 showButtonPanel: true,
		  dateFormat: 'dd-mm-yy',
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onSelect: function(dateText) {
				var start_val = $("#dateofbirth").val();
				var end_val = $("#dateofinclusion").val();
				if(end_val!="" && start_val!="")
				{
					var startyear=(start_val.split('-'))[2];
					var endyear=(end_val.split('-'))[2];
					$("#ageatinclusion").val(endyear - startyear);
				}
				else
					$("#ageatinclusion").val("");
		  },
		  onClose2:function(dateText) { $("#ageatinclusion").val(""); }
		});
		
		//$("#dateofinclusion").inputmask();
		
		$('#dateofbirth').datepicker({
		   showButtonPanel: true,
		  dateFormat: 'dd-mm-yy',
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onSelect: function(dateText) {
				var start_val = $("#dateofbirth").val();
				var end_val = $("#dateofinclusion").val();
				if(end_val!="" && start_val!="")
				{
					var startyear=(start_val.split('-'))[2];
					var endyear=(end_val.split('-'))[2];
					$("#ageatinclusion").val(endyear - startyear);
				}
				else
					$("#ageatinclusion").val("");
		  },
		  onClose2:function(dateText) { $("#ageatinclusion").val(""); }
		});
		
		//$("#dateofbirth").inputmask();
		
		$('#previousemploymentdate').datepicker({
		showButtonPanel: true,
		dateFormat: 'dd-mm-yy',
		yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		onClose2:function(dateText) {}
		});
$("#previousemploymentdate").inputmask();

$('#followupemploymentdate').datepicker({
		showButtonPanel: true,
		dateFormat: 'dd-mm-yy',
		yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		onClose2:function(dateText) {}
		});
$("#followupemploymentdate").inputmask();
})


/*
function averagepackperday()
{
		var first_val = $("#decadefirst").val();
		var second_val = $("#decadesecond").val();
		var third_val = $("#decadethird").val();
		var forth_val = $("#decadeforth").val();
		var fifth_val = $("#decadefifth").val();
		var sixth_val = $("#decadesixth").val();
		var seventh_val = $("#decadeseventh").val();
		var eight_val = $("#decadeeight").val();
		
		var sum = (parseInt(first_val) + parseInt(second_val) + parseInt(third_val) + parseInt(forth_val) + parseInt(fifth_val) + parseInt(sixth_val) + parseInt(seventh_val) + parseInt(eight_val))/8;
		var packperday=(sum.toFixed(2)).replace('.',',');
		$("#averagepackperday").val(packperday);
		
		var sum2=(parseInt($("#smokingstoppedyear").val())-parseInt($("#smokingstarting").val()))*sum;
		var packperyear=(sum2.toFixed(2)).replace('.',',');
		$("#averagepackperyear").val(packperyear);
		
}	
$("#decadefirst").change(function() { averagepackperday(); });
$("#decadesecond").change(function() { averagepackperday(); });
$("#decadethird").change(function() { averagepackperday(); });
$("#decadeforth").change(function() { averagepackperday(); });
$("#decadefifth").change(function() { averagepackperday(); });
$("#decadesixth").change(function() { averagepackperday(); });
$("#decadeseventh").change(function() { averagepackperday(); });
$("#decadeeight").change(function() { averagepackperday(); });

$("#smokingstopped").change(function() 
	{
		var smokingstopped_val = $(this).val();
		if(smokingstopped_val==1)
			$("#divsmokingstoppedyear").show();
		else
			 $("#divsmokingstoppedyear").hide();
		
	});
	
$("#tobacco").change(function(){
     var tobacco_val = $(this).val();
	 if(tobacco_val==2)
	 {
		 $("#divsmokingstarting").show();
		 $("#divsmokingstopped").show();
		 $("#divsmokingfirstdecade").show();
		 $("#divaveragepackperday").show();
		 $("#divaveragepackperyear").show();
		 
	 }
	 else
	 {
		 $("#divsmokingstarting").hide();
		 $("#divsmokingstopped").hide();
		 $("#divsmokingfirstdecade").hide();
		 $("#divsmokingstoppedyear").hide();
		 $("#divaveragepackperday").hide();
		 $("#divaveragepackperyear").hide();
	 }
    });
	
*/	
$("#followupemploymentstatus").change(function(){
     var followupemploymentstatus_val = $(this).val();
	 if(followupemploymentstatus_val==3 || followupemploymentstatus_val==4 || followupemploymentstatus_val==5)
	 {
		 $("#divfollowupworktype").show();
		 $("#divfollowupjob").show();
	 }
	 else
	 {
		 $("#divfollowupworktype").hide();
		 $("#divfollowupjob").hide();
	 }
    });
	
$("#followupemployment").change(function(){
     var followupemployment_val = $(this).val();
	 if(followupemployment_val==1)
	 {
		 $("#divfollowupemploymentdate").show();
		 $("#divfollowupemploymentstatus").show();
	 }
	 else
	 {
		 $("#divfollowupemploymentdate").hide();
		 $("#divfollowupemploymentstatus").hide();
	 }
	 });
	 
$("#previousemploymentstatus").change(function(){
     var previousemploymentstatus_val = $(this).val();
	 if(previousemploymentstatus_val==3 || previousemploymentstatus_val==4 || previousemploymentstatus_val==5)
	 {
		 $("#divpreviousworktype").show();
		 $("#divpreviousjob").show();
	 }
	 else
	 {
		 $("#divpreviousworktype").hide();
		 $("#divpreviousjob").hide();
	 }
    });
	
$("#previousemployment").change(function(){
     var previousemployment_val = $(this).val();
	 if(previousemployment_val==1)
	 {
		 $("#divpreviousemploymentdate").show();
		 $("#divpreviousemploymentstatus").show();
	 }
	 else
	 {
		 $("#divpreviousemploymentdate").hide();
		 $("#divpreviousemploymentstatus").hide();
	 }
	 });
	 
$("#employmentstatus").change(function(){
     var employmentstatus_val = $(this).val();
	 if(employmentstatus_val==3 || employmentstatus_val==4 || employmentstatus_val==5)
	 {
		 $("#divworktype").show();
		 $("#divjob").show();
	 }
	 else
	 {
		 $("#divworktype").hide();
		 $("#divjob").hide();
	 }
    });
	
$("#employment").change(function(){
     var employment_val = $(this).val();
	 if(employment_val==1)
	 {
		 $("#divemploymentstatus").show();
		 $("#divpreviousemployment").show();
		 $("#divfollowupemployment").show();
		 $("#divjobchangerelated").show();
	 }
	 else
	 {
		 $("#divemploymentstatus").hide();
		 $("#divpreviousemployment").hide();
		 $("#divfollowupemployment").hide();
		 $("#divjobchangerelated").hide();
	 }
    });
	
$("#thromboticevents").change(function(){
     var thromboticevents_val = $(this).val();
	 if(thromboticevents_val==1)
	 {
		 $("#divvenous").show();
		 $("#divarterial").show();
	 }
	 else
	 {
		 $("#divvenous").hide();
		 $("#divarterial").hide();
	 }
    });
	
$("#prematureother").change(function() 
	{
		if(this.checked) 
			 $("#divprematureotherno").show();
		 else
			 $("#divprematureotherno").hide();
	});


$("#gender").change(function() 
	{
		if($("#gender").val()==1) 
			 $("#divobsetric").show();
		 else
			 $("#divobsetric").hide();
	});
	
$("#placental").change(function() 
	{
		if(this.checked) 
			 $("#divplacentalno").show();
		 else
			 $("#divplacentalno").hide();
	});
	
$("#premature").change(function() 
	{
		if(this.checked) 
			 $("#divprematureno").show();
		 else
			 $("#divprematureno").hide();
	});
	
$("#eclampsia").change(function() 
	{
		if(this.checked) 
			 $("#diveclampsiano").show();
		 else
			 $("#diveclampsiano").hide();
	});
	
$("#prematurebirths").change(function() 
	{
		if(this.checked) 
			 $("#divprematurebirthsno").show();
		 else
			 $("#divprematurebirthsno").hide();
	});
	
$("#pregnancylosessafter").change(function() 
	{
		if(this.checked) 
			 $("#divpregnancylosesafterno").show();
		 else
			 $("#divpregnancylosesafterno").hide();
	});
	
$("#pregnancylosessbefore").change(function() 
	{
		if(this.checked) 
			 $("#divpregnancylosesbeforeno").show();
		 else
			 $("#divpregnancylosesbeforeno").hide();
	});
	
$("#contraceptivemethods").change(function(){
     var contraceptivemethods_val = $(this).val();
	 if(contraceptivemethods_val==2)
		 $("#divcontraceptivemethodscomm").show();
	 else
		 $("#divcontraceptivemethodscomm").hide();
    });
	
$("#menopause").change(function(){
     var menopause_val = $(this).val();
	 if(menopause_val==2)
		 $("#divmenopauseyear").show();
	 else
		 $("#divmenopauseyear").hide();
    });
	
$("#obsetrichistory").change(function(){
     var obsetrichistory_val = $(this).val();
	 if(obsetrichistory_val==1)
	 {
		 $("#divmenopauseyear").show();
		 $("#divyearofmenarche").show();
		 $("#divmenarche").show();
		 $("#divcontraceptivemethods").show();
		 $("#divintertility").show();
		 $("#divassistedreproduction").show();
		 $("#divpregnancies").show();
		 $("#divchildbirths").show();
		 $("#divchildbirthsyear").show();
		 $("#divpregnancylosesbefore").show();
		 $("#divpregnancylosesafter").show();
		 $("#divprematurebirths").show();		 
		 $("#diveclampsia").show();
		 $("#divpremature").show();
		 $("#divplacental").show();
		 $("#divprematureother").show();
	 }
	 else
	 {
		$("#divmenopauseyear").hide();
		 $("#divyearofmenarche").hide();
		 $("#divmenarche").hide();
		 $("#divcontraceptivemethods").hide();
		 $("#divintertility").hide();
		 $("#divassistedreproduction").hide();
		 $("#divpregnancies").hide();
		 $("#divchildbirths").hide();
		 $("#divchildbirthsyear").hide();
		 $("#divpregnancylosesbefore").hide();
		 $("#divpregnancylosesafter").hide();
		 $("#divprematurebirths").hide();
		 $("#diveclampsia").hide();
		 $("#divpremature").hide();
		 $("#divplacental").hide();
		 $("#divprematureother").hide();
	 }
    });

$("#autoimmune").change(function(){
     var indexautoimmune_val = $(this).val();
	 if(indexautoimmune_val==1)
		 $("#divautoimmunetable").show();
	 else
		 $("#divautoimmunetable").hide();
    });
	
$("#ethnicity").change(function(){
     var ethnicity_val = $(this).val();
	 if(ethnicity_val==2)
		 $("#divotherethnicity").show();
	 else
		 $("#divotherethnicity").hide();
    });
	
$("#familyhistory").change(function(){
     var familyhistory_val = $(this).val();
	 if(familyhistory_val==1)
	 {
		 $("#divindexreumatic").show();
		 $("#divautoimmune").show();
	 }
	 else
	 {
		 $("#divindexreumatic").hide();
		 $("#divautoimmune").hide();
	 }
    });
	
$("#indexreumatic").change(function(){
     var indexreumatic_val = $(this).val();
	 if(indexreumatic_val==1)
	 {
		 $("#divindexreumatic1").show();
		 $("#divindexreumatic2").show();
		 $("#divindexreumatic3").show();
	 }
	 else
	 {
		 $("#divindexreumatic1").hide();
		 $("#divindexreumatic2").hide();
		 $("#divindexreumatic3").hide();
	 }
    });
	
$("#urban").change(function() 
	{
		if(this.checked) 
			 $("#divurbandate").show();
		 else
			 $("#divurbandate").hide();
	});
	
$("#suburban").change(function() 
	{
		if(this.checked) 
			 $("#divsuburbandate").show();
		 else
			 $("#divsuburbandate").hide();
	});
	
$("#rular").change(function() 
	{
		if(this.checked) 
			 $("#divrulardate").show();
		 else
			 $("#divrulardate").hide();
	});
	

function arterial_addrow()
{
  var tbl = document.getElementById('arterial_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'arterial_tr_'+iteration;
  document.getElementById('arterial_numofrows').value = iteration;
  
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'arterial_hidden_' + iteration;
  el2.id = 'arterial_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'arterial_id_' + iteration;
  el3.id = 'arterial_id_' + iteration;
  el3.value='';
  
   var el1 = document.createElement('select');
  el1.name = 'arterial_organ_id_' + iteration;
  el1.id = 'arterial_organ_id_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
  				
	var opt=new Option("limbs",1);
	el1.add(opt,undefined);
	var opt=new Option("Pulmonary embolism",2);
	el1.add(opt,undefined);
	var opt=new Option("Stroke/TIA",3);
	el1.add(opt,undefined);
	var opt=new Option("Coronary",4);
	el1.add(opt,undefined);
	var opt=new Option("Renal",5);
	el1.add(opt,undefined);
	var opt=new Option("Visceral",6);
	el1.add(opt,undefined);
	
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
  var r2 = row.insertCell(1);
  r2.align='center';
  var el4 = document.createElement('input');
  el4.type = 'text';
  el4.name = 'arterial_year_' + iteration;
  el4.id = 'arterial_year_' + iteration;
  el4.style.width='100%';
  el4.className='form-control';
  
  r2.appendChild(el4);
  
 
  var r7 = row.insertCell(2);
  r7.align='center';
  
  var el9 = document.createElement('a');
  el9.name = 'a_' + iteration;
  el9.id = 'a_' + iteration;
  el9.title = 'Delete';
  el9.href="javascript:avoid(0)";
  var icon = document.createElement('i');
  icon.className='fa fa-trash-o';
  el9.appendChild(icon);
  el9.onclick =function(){arterial_removerow(iteration)};
		 
  r7.appendChild(el9);
  
  $('#arterial_year_'+ iteration).inputmask('9999');
}

for(i=1;i<=document.getElementById('arterial_numofrows').value;i++)
{
	$('#arterial_year_'+ i).inputmask();
}

function arterial_removerow(row)
{
	document.getElementById('arterial_hidden_'+row).value='-1';
	document.getElementById('arterial_tr_'+row).style.display='none';
}

function venous_addrow()
{
  var tbl = document.getElementById('venous_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'venous_tr_'+iteration;
  document.getElementById('venous_numofrows').value = iteration;
  
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'venous_hidden_' + iteration;
  el2.id = 'venous_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'venous_id_' + iteration;
  el3.id = 'venous_id_' + iteration;
  el3.value='';
  
   var el1 = document.createElement('select');
  el1.name = 'organ_id_' + iteration;
  el1.id = 'organ_id_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
  				
	var opt=new Option("limbs",1);
	el1.add(opt,undefined);
	var opt=new Option("brain/dural sinus",2);
	el1.add(opt,undefined);
	var opt=new Option("visceral vein",3);
	el1.add(opt,undefined);
	
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
  var r2 = row.insertCell(1);
  r2.align='center';
  var el4 = document.createElement('input');
  el4.type = 'text';
  el4.name = 'venous_year_' + iteration;
  el4.id = 'venous_year_' + iteration;
  el4.style.width='100%';
  el4.className='form-control';
  
  
  r2.appendChild(el4);
  
	var r4 = row.insertCell(2);
  r4.align='center';
  
  var el6 = document.createElement('a');
  el6.name = 'a_' + iteration;
  el6.id = 'a_' + iteration;
  el6.title = 'Delete';
  el6.href="javascript:avoid(0)";
  var icon = document.createElement('i');
  icon.className='fa fa-trash-o';
  el6.appendChild(icon);
  el6.onclick =function(){venous_removerow(iteration)};
		 
  r4.appendChild(el6);
  $('#venous_year_'+ iteration).inputmask('9999');
  
}

for(i=1;i<=document.getElementById('venous_numofrows').value;i++)
{
	$('#venous_year_'+ i).inputmask();
}

function venous_removerow(row)
{
	document.getElementById('venous_hidden_'+row).value='-1';
	document.getElementById('venous_tr_'+row).style.display='none';
}

function autoimmune_addrow()
{
  var tbl = document.getElementById('autoimmune_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'autoimmune_tr_'+iteration;
  document.getElementById('autoimmune_numofrows').value = iteration;
  
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('select');
  el1.name = 'autoimmune_relation_' + iteration;
  el1.id = 'autoimmune_relation_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
  
	var opt=new Option("Parent",1);
	el1.add(opt,undefined);
	var opt=new Option("Offspring",2);
	el1.add(opt,undefined);
	var opt=new Option("Sibling",3);
	el1.add(opt,undefined);
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'autoimmune_hidden_' + iteration;
  el2.id = 'autoimmune_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'autoimmune_id_' + iteration;
  el3.id = 'autoimmune_id_' + iteration;
  el3.value='';
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
  var r2 = row.insertCell(1);
  r2.align='center';
  var el4 = document.createElement('textarea');
  el4.name = 'autoimmune_diagnosis_' + iteration;
  el4.id = 'autoimmune_diagnosis_' + iteration;
  el4.className='form-control';
  el4.style.width='100%';
  
  r2.appendChild(el4);
  
  var r3 = row.insertCell(2);
  r3.align='center';
  
  var el5 = document.createElement('a');
  el5.name = 'a_' + iteration;
  el5.id = 'a_' + iteration;
  el5.title = 'Delete';
  el5.href="javascript:avoid(0)";
  var icon = document.createElement('i');
  icon.className='fa fa-trash-o';
  el5.appendChild(icon);
  el5.onclick =function(){autoimmune_removerow(iteration)};
		 
  r3.appendChild(el5);
}

function autoimmune_removerow(row)
{
	document.getElementById('autoimmune_hidden_'+row).value='-1';
	document.getElementById('autoimmune_tr_'+row).style.display='none';
}

function marital_addrow()
{
  var tbl = document.getElementById('marital_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'marital_tr_'+iteration;
  document.getElementById('marital_numofrows').value = iteration;
  
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('select');
  el1.name = 'marital_status_' + iteration;
  el1.id = 'marital_status_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
  
	var opt=new Option("Not married",1);
	el1.add(opt,undefined);
	var opt=new Option("Married",2);
	el1.add(opt,undefined);
	var opt=new Option("Divorced",3);
	el1.add(opt,undefined);
	var opt=new Option("Widow",4);
	el1.add(opt,undefined);
	var opt=new Option("Unknown",5);
	el1.add(opt,undefined);
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'marital_hidden_' + iteration;
  el2.id = 'marital_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'marital_id_' + iteration;
  el3.id = 'marital_id_' + iteration;
  el3.value='';
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
  var r2 = row.insertCell(1);
  r2.align='center';
  
  var el4 = document.createElement('input');
  el4.type = 'text';
  el4.name = 'marital_year_' + iteration;
  el4.id = 'marital_year_' + iteration;
  el4.style.width='100%';
  el4.className='form-control';
  
  r2.appendChild(el4);
  
  var r3 = row.insertCell(2);
  r3.align='center';
  
  var el5 = document.createElement('a');
  el5.name = 'a_' + iteration;
  el5.id = 'a_' + iteration;
  el5.title = 'Delete';
  el5.href="javascript:avoid(0)";
  var icon = document.createElement('i');
  icon.className='fa fa-trash-o';
  el5.appendChild(icon);
  el5.onclick =function(){marital_removerow(iteration)};
		 
  r3.appendChild(el5);
  $('#marital_year_'+ iteration).inputmask('9999');
}

function marital_removerow(row)
{
	document.getElementById('marital_hidden_'+row).value='-1';
	document.getElementById('marital_tr_'+row).style.display='none';
}

for(i=1;i<=document.getElementById('marital_numofrows').value;i++)
{
	$('#marital_year_'+ i).inputmask();
}

function mediterranean_addrow()
{
  var tbl = document.getElementById('mediterranean_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'mediterranean_tr_'+iteration;
  document.getElementById('mediterranean_numofrows').value = iteration;
  
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('select');
  el1.name = 'mediterranean_status_' + iteration;
  el1.id = 'mediterranean_status_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
  
	var opt=new Option("Olive oil (>=1 spoon per day)",1);
	el1.add(opt,undefined);
	var opt=new Option("Fruit (>=1 serving per day)",2);
	el1.add(opt,undefined);
	var opt=new Option("Vegetables or salad (>=1 serving per day)",3);
	el1.add(opt,undefined);
	var opt=new Option("Fruit (>=1 serving/day) AND vegetables (>=1 serving/day)",4);
	el1.add(opt,undefined);
	var opt=new Option("Legumes (>=2 servings per week)",5);
	el1.add(opt,undefined);
	var opt=new Option("Fish (>=3 servings per week)",6);
	el1.add(opt,undefined);
	var opt=new Option("Wine (>=1 glass per day)",7);
	el1.add(opt,undefined);
	var opt=new Option("Red meat (<1 serving per day)",8);
	el1.add(opt,undefined);
	var opt=new Option("White bread (<1/day) AND rice (<1/week) OR, whole-grain bread (>5/week)",9);
	el1.add(opt,undefined);
  													
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'mediterranean_hidden_' + iteration;
  el2.id = 'mediterranean_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'mediterranean_id_' + iteration;
  el3.id = 'mediterranean_id_' + iteration;
  el3.value='';
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
  var r2 = row.insertCell(1);
  r2.align='center';
  var el4 = document.createElement('input');
  el4.name = 'date_med_str_' + iteration;
  el4.id = 'date_med_str_' + iteration;
  el4.className='form-control';
  el4.style.width='100%';
  
 
  
  r2.appendChild(el4);
  
  $('#date_med_str_'+iteration).datepicker({
		dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	});
	$('#date_med_str_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
	
	
  var r3 = row.insertCell(2);
  r3.align='center';
  
  var el5 = document.createElement('a');
  el5.name = 'a_' + iteration;
  el5.id = 'a_' + iteration;
  el5.title = 'Delete';
  el5.href="javascript:avoid(0)";
  var icon = document.createElement('i');
  icon.className='fa fa-trash-o';
  el5.appendChild(icon);
  el5.onclick =function(){mediterranean_removerow(iteration)};
		 
  r3.appendChild(el5);
}

function mediterranean_removerow(row)
{
	document.getElementById('mediterranean_hidden_'+row).value='-1';
	document.getElementById('mediterranean_tr_'+row).style.display='none';
}

function physical_addrow()
{
  var tbl = document.getElementById('physical_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'physical_tr_'+iteration;
  document.getElementById('physical_numofrows').value = iteration;
  
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('select');
  el1.name = 'physical_status_' + iteration;
  el1.id = 'physical_status_' + iteration;
  el1.style.width='100%';
  el1.className='form-control';
  
	var opt=new Option("Moving only for necesary chores",1);
	el1.add(opt,undefined);
	var opt=new Option("Walking or other outdoor activities 1-2 times per week",2);
	el1.add(opt,undefined);
	var opt=new Option("Walking or other outdoor activities several times per week",3);
	el1.add(opt,undefined);
	var opt=new Option("Exercing 1-2 times per week to the point of perspiring and heavy breathing",4);
	el1.add(opt,undefined);
	var opt=new Option("Exercing several times per week to the point of perspiring and heavy breathing",5);
	el1.add(opt,undefined);
	var opt=new Option("Keep-fit heavy exercise ot competitive sport several times per week",6);
	el1.add(opt,undefined);
  													
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'physical_hidden_' + iteration;
  el2.id = 'physical_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'physical_id_' + iteration;
  el3.id = 'physical_id_' + iteration;
  el3.value='';
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
  var r2 = row.insertCell(1);
  r2.align='center';
  var el4 = document.createElement('input');
  el4.name = 'date_phys_str_' + iteration;
  el4.id = 'date_phys_str_' + iteration;
  el4.className='form-control';
  el4.style.width='100%';
  
  r2.appendChild(el4);
  
  $('#date_phys_str_'+iteration).datepicker({
		dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
	});
	
	$('#date_phys_str_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
  
  var r3 = row.insertCell(2);
  r3.align='center';
  
  var el5 = document.createElement('a');
  el5.name = 'a_' + iteration;
  el5.id = 'a_' + iteration;
  el5.title = 'Delete';
  el5.href="javascript:avoid(0)";
  var icon = document.createElement('i');
  icon.className='fa fa-trash-o';
  el5.appendChild(icon);
  el5.onclick =function(){physical_removerow(iteration)};
		 
  r3.appendChild(el5);
}

function physical_removerow(row)
{
	document.getElementById('physical_hidden_'+row).value='-1';
	document.getElementById('physical_tr_'+row).style.display='none';
}

</script>
<?php
	
if($save==1)
{
	if($save_chk=="ok")
	{
?>
<script>
$(".alert_suc").show();
window.setTimeout(function() {
    $(".alert_suc").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
	else
	{
?>
<script>
$(".alert_wr").show();
window.setTimeout(function() {
    $(".alert_wr").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
}
?>
</body>
</html>
