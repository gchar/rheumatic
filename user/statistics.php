<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	/*$save=$_REQUEST['save'];
	$patient_cohort_enddate=$_REQUEST['patient_cohort_enddate'];
	$patient_cohort_startdate=$_REQUEST['patient_cohort_startdate'];
	$patient_event_enddate=$_REQUEST['patient_event_enddate'];
	$patient_event_startdate=$_REQUEST['patient_event_startdate'];
	$type=$_REQUEST['type'];
	$all_centre=$_REQUEST['all_centre'];
	*/
	$_SESSION['pat_id']="";
	$_SESSION['patient_cohort_id']="";
	$_SESSION['patient_followup_id']="";
	$_SESSION['patient_event_id']="";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<?php
include '../portion/head.php';
?>
  

  <?php
  
  /*if($save==1)
  {
	 pg_query("BEGIN") or die("Could not start transaction\n");
	try
	{

		
		if($type==1)
		{
			$query = "(select statistics_lk1.*,statistics_lk2.*,statistics_lk3.* from statistics_lk1 join statistics_lk2 on statistics_lk1.id=statistics_lk2.id join statistics_lk3 on statistics_lk2.id=statistics_lk3.id order by statistics_lk1.id asc limit 1 )";
			$query .= " union (select statistics_lk1.*,statistics_lk2.*,statistics_lk3.* from statistics_lk1 join statistics_lk2 on statistics_lk1.id=statistics_lk2.id join statistics_lk3 on statistics_lk2.id=statistics_lk3.id ";
			
			$query .= " where statistics_lk1.id<>1 ";
			if($user_all_rpts==1)
			{
				if($all_centre==0)
					$query .= " and statistics_lk1.a5='$user_centre_id' ";
			}
			else
				$query .= " and statistics_lk1.a5='$user_centre_id' ";
			
			if($patient_cohort_enddate<>"" or $patient_cohort_startdate<>"")
			$query .= " and ";
		
			if($patient_cohort_startdate<>"")
				$query .= " statistics_lk1.a8 >= '$patient_cohort_startdate' ";

			if($patient_cohort_enddate<>"")
			{
				if($patient_cohort_startdate<>"")
					$query .= " and statistics_lk1.a8 <= '$patient_cohort_enddate' ";
				else
					$query .= " statistics_lk1.a8 <= '$patient_cohort_enddate' ";
			}

			$query .= " order by statistics_lk1.id asc )";
			
			$final_query="COPY (".$query.") TO 'C:/Bitnami/apache2/htdocs/rheumatic/user/statistics/cohort_".date("YmdHi").".csv' DELIMITER ',';";
			//echo $final_query;
			$exec = pg_query($final_query);
			if ($exec)
				$msg1="ok";
			
			$parameters[0]=0;
			$parameters[1]=$user_id;
			$parameters[2]='now()';
			$parameters[3]=date_for_postgres($_REQUEST['patient_cohort_startdate']);
			$parameters[4]=date_for_postgres($_REQUEST['patient_cohort_enddate']);
			$parameters[5]='now()';
			$parameters[6]="cohort_".date("YmdHi");
			$parameters[7]=1;
			$parameters[8]=$all_centre;
			if($user_all_rpts==0)
				$parameters[9]=$user_centre_id;
			else
			{
				if($all_centre==0)
					$parameters[9]=$user_centre_id;
				else
					$parameters[9]=0;
			}
				
			
			$table_names[0]='deleted';
			$table_names[1]='editor_id';
			$table_names[2]='edit_date';
			$table_names[3]='start_date';
			$table_names[4]='end_date';
			$table_names[5]='create_date';
			$table_names[6]='filename';
			$table_names[7]='type';
			$table_names[8]='all_centre';
			$table_names[9]='centre_id';
			
			$statistics_id=insert('statistics',$table_names,$parameters,'statistics_id');
			if($statistics_id!='')
				$msg2="ok";
		}
		else if($type==2)
		{
			$query = "(select * from statistics_lk4 order by id asc limit 1 )";
			$query .= " union (select * from statistics_lk4  ";
			
			$query .= " where id<>1 ";
			if($user_all_rpts==1)
			{
				if($all_centre==0)
					$query .= " and a5='$user_centre_id' ";
			}
			else
				$query .= " and a5='$user_centre_id' ";
			
			if($patient_event_enddate<>"" or $patient_event_startdate<>"")
			$query .= " and ";
			
			if($patient_event_startdate<>"")
				$query .= " a31 >= '$patient_event_startdate' ";

			if($patient_event_enddate<>"")
			{
				if($patient_event_startdate<>"")
					$query .= " and a31 <= '$patient_event_enddate' ";
				else
					$query .= " a31 <= '$patient_event_enddate' ";
			}

			$query .= " order by id asc )  order by id asc";
			$final_query="COPY (".$query.") TO 'C:/Bitnami/apache2/htdocs/rheumatic/user/statistics/event_".date("YmdHi").".csv' DELIMITER ',';";
			//echo $final_query;
			$exec = pg_query($final_query);
			if ($exec)
				$msg1="ok";
			
			$parameters[0]=0;
			$parameters[1]=$user_id;
			$parameters[2]='now()';
			$parameters[3]=date_for_postgres($_REQUEST['patient_event_startdate']);
			$parameters[4]=date_for_postgres($_REQUEST['patient_event_enddate']);
			$parameters[5]='now()';
			$parameters[6]="event_".date("YmdHi");
			$parameters[7]=2;
			$parameters[8]=$all_centre;
			if($user_all_rpts==0)
				$parameters[9]=$user_centre_id;
			else
			{
				if($all_centre==0)
					$parameters[9]=$user_centre_id;
				else
					$parameters[9]=0;
			}
			
			$table_names[0]='deleted';
			$table_names[1]='editor_id';
			$table_names[2]='edit_date';
			$table_names[3]='start_date';
			$table_names[4]='end_date';
			$table_names[5]='create_date';
			$table_names[6]='filename';
			$table_names[7]='type';
			$table_names[8]='all_centre';
			$table_names[9]='centre_id';
			
			$statistics_id=insert('statistics',$table_names,$parameters,'statistics_id');
			if($statistics_id!='')
				$msg2="ok";
		}
	
		if ($msg1=="ok" and $msg2=="ok" )
		{
			$save_chk="ok";
			pg_query("COMMIT") or die("Transaction commit failed\n");
		}
		else
		{
			$save_chk="nok";
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
	}
	catch(exception $e) {
		echo "e:".$e."</br>";
		echo "ROLLBACK";
		pg_query("ROLLBACK") or die("Transaction rollback failed\n");
	}
  }*/
  ?>
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <small>Reports</small>
        </h1>
      </section>

      <!-- Main content -->
      <section class="content">
	  <div class="alert alert_suc alert-success" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Report was successfully created!</strong>
	  </div>
	  <div class="alert alert_wr alert-danger" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>The report was not successfully created!</strong>
	  </div>
	  <div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Export data criteria</h3>
			</div>
            <div class="box-body">
			 <form id="form1" action="#" method="POST">
				<input type="hidden" id="save" name="save" value="1">
				<div class="row">
					<div class="form-group col-md-4">
					  <label>Select export type</label>
					  <select class="form-control" name="type" id="type">
						<option value="0" <?php if ($type==0) { echo "selected"; } ?>>--</option>
						<option value="1" <?php if ($type==1) { echo "selected"; } ?>>Cohort</option>
						<option value="2" <?php if ($type==2) { echo "selected"; } ?>>Event</option>
						<option value="3" <?php if ($type==3) { echo "selected"; } ?>>Biobanking</option>
						<option value="4" <?php if ($type==4) { echo "selected"; } ?>>Inclusion</option>
					 </select>
					 </div>
				</div>
				<div class="row">
					<div class="form-group col-md-12">
					  <a class="btn btn-primary" onclick="javascript:popup2('report.php');" href="javascript:void();" target="_self"><i class="fa fa-plus"></i> New Report</a>
					</div>
				</div>
			</form>
			</div>
		</div>
		<div class="box" id="results" name="results" <?php if ($type==1 or $type==2 or $type==3) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
			<div class="box-header with-border">
				<h3 class="box-title">Results</h3>
			</div>
			<div class="box-body">
				<div id="divtable_jq" name="divtable_jq" <?php if ($type==1) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
					<table id="table_jq" name="table_jq" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Create date</th>
								<th>Creator</th>
								<th>Start date</th>
								<th>End date</th>
								<?php if($user_all_rpts==1)  { ?> <th>Centre</th> <?php } ?>
								<th>Report</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$exec = get_statistics(1,$user_centre_id,$user_all_rpts,$user_id);
						while($result = pg_fetch_array($exec))
						{
							$filename=$result['filename'];
							$create_date_str=$result['create_date_str'];
							if ($create_date_str=='12-12-1900')
								$create_date_str="";
							$start_date_str=$result['start_date_str'];
							if ($start_date_str=='12-12-1900')
								$start_date_str="";
							$end_date_str=$result['end_date_str'];
							if ($end_date_str=='12-12-1900')
								$end_date_str="";
							$all_centre=$result['all_centre'];
							if($all_centre==0)
								$all_centre_val="Yours";
							else
								$all_centre_val="All centres";
							
							$name=$result['name'];
							$surname=$result['surname'];
						?>
							<tr class="gradeA" align="center" >
								<td>&nbsp;<?php echo $create_date_str; ?></td>
								<td>&nbsp;<?php echo $surname." ".$name; ?></td>
								<td>&nbsp;<?php echo $start_date_str; ?></td>
								<td>&nbsp;<?php echo $end_date_str; ?></td>
								<?php if($user_all_rpts==1)  { ?> <td><?php echo $all_centre_val; ?></td> <?php } ?>
								<td><a href="statistics/<?php echo $filename;  ?>.csv" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a></td>
							</tr>
							<?php
							}
							?>
						</tbody>
					</table>
				</div>
				<div id="divtable_jq2" name="divtable_jq2" <?php if ($type==2) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
					<table id="table_jq2" name="table_jq2" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Create date</th>
								<th>Creator</th>
								<th>Start date</th>
								<th>End date</th>
								<?php if($user_all_rpts==1)  { ?> <th>Centre</th> <?php } ?>
								<th>Report</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$exec = get_statistics(2,$user_centre_id,$user_all_rpts,$user_id);
						while($result = pg_fetch_array($exec))
						{
							$filename=$result['filename'];
							$create_date_str=$result['create_date_str'];
							if ($create_date_str=='12-12-1900')
								$create_date_str="";
							$start_date_str=$result['start_date_str'];
							if ($start_date_str=='12-12-1900')
								$start_date_str="";
							$end_date_str=$result['end_date_str'];
							if ($end_date_str=='12-12-1900')
								$end_date_str="";
							$all_centre=$result['all_centre'];
							if($all_centre==0)
								$all_centre_val="Yours";
							else
								$all_centre_val="All centres";
							
							$name=$result['name'];
							$surname=$result['surname'];
						?>
							<tr class="gradeA" align="center" >
								<td>&nbsp;<?php echo $create_date_str; ?></td>
								<td>&nbsp;<?php echo $surname." ".$name; ?></td>
								<td>&nbsp;<?php echo $start_date_str; ?></td>
								<td>&nbsp;<?php echo $end_date_str; ?></td>
								<?php if($user_all_rpts==1)  { ?> <td><?php echo $all_centre_val; ?></td> <?php } ?>
								<td><a href="statistics/<?php echo $filename;  ?>.csv" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a></td>
							</tr>
							<?php
						}
							?>
						</tbody>
					</table>
				</div>
				<div id="divtable_jq3" name="divtable_jq3" <?php if ($type==3) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
					<table id="table_jq3" name="table_jq3" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Create date</th>
								<th>Creator</th>
								<th>Start date</th>
								<th>End date</th>
								<?php if($user_all_rpts==1)  { ?> <th>Centre</th> <?php } ?>
								<th>Report</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$exec = get_statistics(3,$user_centre_id,$user_all_rpts,$user_id);
						while($result = pg_fetch_array($exec))
						{
							$filename=$result['filename'];
							$create_date_str=$result['create_date_str'];
							if ($create_date_str=='12-12-1900')
								$create_date_str="";
							$start_date_str=$result['start_date_str'];
							if ($start_date_str=='12-12-1900')
								$start_date_str="";
							$end_date_str=$result['end_date_str'];
							if ($end_date_str=='12-12-1900')
								$end_date_str="";
							$all_centre=$result['all_centre'];
							if($all_centre==0)
								$all_centre_val="Yours";
							else
								$all_centre_val="All centres";
							
							$name=$result['name'];
							$surname=$result['surname'];
						?>
							<tr class="gradeA" align="center" >
								<td>&nbsp;<?php echo $create_date_str; ?></td>
								<td>&nbsp;<?php echo $surname." ".$name; ?></td>
								<td>&nbsp;<?php echo $start_date_str; ?></td>
								<td>&nbsp;<?php echo $end_date_str; ?></td>
								<?php if($user_all_rpts==1)  { ?> <td><?php echo $all_centre_val; ?></td> <?php } ?>
								<td><a href="statistics/<?php echo $filename;  ?>.csv" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a></td>
							</tr>
							<?php
						}
							?>
						</tbody>
					</table>
				</div>
				<div id="divtable_jq4" name="divtable_jq4" <?php if ($type==4) { ?> style="display:block;" <?php } else { ?> style="display:none;" <?php } ?>>
					<table id="table_jq4" name="table_jq4" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>Create date</th>
								<th>Creator</th>
								<th>Start date</th>
								<th>End date</th>
								<?php if($user_all_rpts==1)  { ?> <th>Centre</th> <?php } ?>
								<th>Report</th>
							</tr>
						</thead>
						<tbody>
						<?php
						$exec = get_statistics(4,$user_centre_id,$user_all_rpts,$user_id);
						while($result = pg_fetch_array($exec))
						{
							$filename=$result['filename'];
							$create_date_str=$result['create_date_str'];
							if ($create_date_str=='12-12-1900')
								$create_date_str="";
							$start_date_str=$result['start_date_str'];
							if ($start_date_str=='12-12-1900')
								$start_date_str="";
							$end_date_str=$result['end_date_str'];
							if ($end_date_str=='12-12-1900')
								$end_date_str="";
							$all_centre=$result['all_centre'];
							if($all_centre==0)
								$all_centre_val="Yours";
							else
								$all_centre_val="All centres";
							
							$name=$result['name'];
							$surname=$result['surname'];
						?>
							<tr class="gradeA" align="center" >
								<td>&nbsp;<?php echo $create_date_str; ?></td>
								<td>&nbsp;<?php echo $surname." ".$name; ?></td>
								<td>&nbsp;<?php echo $start_date_str; ?></td>
								<td>&nbsp;<?php echo $end_date_str; ?></td>
								<?php if($user_all_rpts==1)  { ?> <td><?php echo $all_centre_val; ?></td> <?php } ?>
								<td><a href="statistics/<?php echo $filename;  ?>.csv" target="_blank"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a></td>
							</tr>
							<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
       <!-- /.box -->
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>
$(document).ready(function() {
	
	$('#table_jq').DataTable();
	$('#table_jq2').DataTable();
	$('#table_jq3').DataTable();
	$('#table_jq4').DataTable();
});

function trg()
{
 	if($("#type").val()==0)
	{
		$("#results").hide();
		$("#divtable_jq").hide();
		$("#divtable_jq2").hide();
		$("#divtable_jq3").hide();
		$("#divtable_jq4").hide();
	}
	else if($("#type").val()==1)
	{
		$("#results").show();
		$("#divtable_jq").show();
		$("#divtable_jq2").hide();
		$("#divtable_jq3").hide();
		$("#divtable_jq4").hide();
	}
	else if($("#type").val()==2)
	{
		$("#results").show();
		$("#divtable_jq").hide();
		$("#divtable_jq2").show();
		$("#divtable_jq3").hide();
		$("#divtable_jq4").hide();
	}
	else if($("#type").val()==3)
	{
		$("#results").show();
		$("#divtable_jq").hide();
		$("#divtable_jq2").hide();
		$("#divtable_jq3").show();
		$("#divtable_jq4").hide();
	}
	else if($("#type").val()==4)
	{
		$("#results").show();
		$("#divtable_jq").hide();
		$("#divtable_jq2").hide();
		$("#divtable_jq3").hide();
		$("#divtable_jq4").show();
	}
};

$("#type").change(function() {
  
	if($("#type").val()==0)
	{
		$("#results").hide();
		$("#divtable_jq").hide();
		$("#divtable_jq2").hide();
		$("#divtable_jq3").hide();
		$("#divtable_jq4").hide();
	}
	else if($("#type").val()==1)
	{
		$("#results").show();
		$("#divtable_jq").show();
		$("#divtable_jq2").hide();
		$("#divtable_jq3").hide();
		$("#divtable_jq4").hide();
	}
	else if($("#type").val()==2)
	{
		$("#results").show();
		$("#divtable_jq").hide();
		$("#divtable_jq2").show();
		$("#divtable_jq3").hide();
		$("#divtable_jq4").hide();
	}
	else if($("#type").val()==3)
	{
		$("#results").show();
		$("#divtable_jq").hide();
		$("#divtable_jq2").hide();
		$("#divtable_jq3").show();
		$("#divtable_jq4").hide();
	}
	else if($("#type").val()==4)
	{
		$("#results").show();
		$("#divtable_jq").hide();
		$("#divtable_jq2").hide();
		$("#divtable_jq3").hide();
		$("#divtable_jq4").show();
	}
});

	
</script>
<?php
	
if($save==1)
{
	if($save_chk=="ok")
	{
?>
<script>
$(".alert_suc").show();
window.setTimeout(function() {
    $(".alert_suc").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
	else
	{
?>
<script>
$(".alert_wr").show();
window.setTimeout(function() {
    $(".alert_wr").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
}
?>
</body>
</html>

