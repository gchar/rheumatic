		
<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	$save=$_REQUEST['save'];
	$pat_id=$_REQUEST['pat_id'];
	if($pat_id<>"")
		$_SESSION['pat_id']=$pat_id;
	
	$pat_id=$_SESSION['pat_id'];
	
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<?php

if($save==1)
{
	pg_query("BEGIN") or die("Could not start transaction\n");
	try
	{
		//symptoms not included table-start
		
		$j=1;
		$symptoms_not_included_numofrows=$_REQUEST['symptoms_not_included_numofrows'];
		for ($i=1;$i<=$symptoms_not_included_numofrows;$i++)
		{
			$symptoms_not_included_hidden=$_REQUEST['symptoms_not_included_hidden_'.$i];
			$symptoms_not_included_id=$_REQUEST['symptoms_not_included_id_'.$i];
			if($symptoms_not_included_hidden=='-1')
			{
				if($symptoms_not_included_id<>"")
				{
					$symptoms_not_included_hdn_parameters[0]=1;
					$symptoms_not_included_hdn_names[0]='deleted';
					$symptoms_not_included_hdn_edit_name[0]='symptoms_not_included_id';
					$symptoms_not_included_hdn_edit_id[0]=$symptoms_not_included_id;
					$symptoms_not_included_hdn_sumbol[0]='=';
					$symptoms_not_included_msg=update('symptoms_not_included',$symptoms_not_included_hdn_names,$symptoms_not_included_hdn_parameters,$symptoms_not_included_hdn_edit_name,$symptoms_not_included_hdn_edit_id,$symptoms_not_included_hdn_sumbol);
					if ($symptoms_not_included_msg=="ok")
						$j++;
				}
				else
					$j++;
			}
			else
			{
				
				$symptoms_not_included_parameters[0]=$_REQUEST['symptoms_id_'.$i];
				$symptoms_not_included_parameters[1]=$_REQUEST['symptoms_organs_id_'.$i];
				$symptoms_not_included_parameters[2]=date_for_postgres($_REQUEST['symptoms_date_'.$i]);
				$symptoms_not_included_parameters[3]=$user_id;
				$symptoms_not_included_parameters[4]='now()';
				$symptoms_not_included_table_names[0]='symptoms_id';
				$symptoms_not_included_table_names[1]='symptoms_organs_id';
				$symptoms_not_included_table_names[2]='symptoms_date';
				$symptoms_not_included_table_names[3]='editor_id';
				$symptoms_not_included_table_names[4]='edit_date';
				$symptoms_not_included_edit_name[0]='symptoms_not_included_id';
				$symptoms_not_included_edit_id[0]=$symptoms_not_included_id;
				$symptoms_not_included_sumbol[0]='=';
				
				if($symptoms_not_included_id=='')
				{
					
					$symptoms_not_included_parameters[5]=$pat_id;
					$symptoms_not_included_parameters[6]=0;
					
					$symptoms_not_included_table_names[5]='pat_id';
					$symptoms_not_included_table_names[6]='deleted';
					
					$id=insert('symptoms_not_included',$symptoms_not_included_table_names,$symptoms_not_included_parameters,'symptoms_not_included_id');
					if($id!='')
						$j++;
				}
				else
				{
					$symptoms_not_included_msg=update('symptoms_not_included',$symptoms_not_included_table_names,$symptoms_not_included_parameters,$symptoms_not_included_edit_name,$symptoms_not_included_edit_id,$symptoms_not_included_sumbol);
					if ($symptoms_not_included_msg=="ok")
						$j++;
				}
			}
		}
		
		if ($j==$i)
			$antirheumatictreat_msg="ok";
		else
			$antirheumatictreat_msg="nok";
		//symptoms not included table-end
		
		
		
		if ($antirheumatictreat_msg=="ok")
		{
			$save_chk="ok";
			pg_query("COMMIT") or die("Transaction commit failed\n");
		}
		else
		{
			$save_chk="nok";
			pg_query("ROLLBACK") or die("Transaction rollback failed\n");
		}
		
	}
	catch(exception $e) {
		echo "e:".$e."</br>";
		echo "ROLLBACK";
		pg_query("ROLLBACK") or die("Transaction rollback failed\n");
	}
	
}

$exec = get_patient_demographics($pat_id);
$result = pg_fetch_array($exec);
$dateofinclusion=$result['dateofinclusion_str'];
if($dateofinclusion=="12-12-1900")
	$dateofinclusion="";
$dateofbirth=$result['dateofbirth_str'];
if($dateofbirth=="12-12-1900")
	$dateofbirth="";
$gender=$result['gender'];
if($gender==1)
	$gender_str="Female";
else if($gender==2)
	$gender_str="Male";
$patient_id=$result['patient_id'];
?>
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
	   <h1>
          Symptoms not included in the criteria
		</h1>
		<h1>
		<?php echo "<small>Patient:".$patient_id.",".$gender_str." (".$dateofbirth.") </small>"; ?>
      </h1>
      </section>
	
      <!-- Main content -->
      <section class="content">
	  <div class="alert alert_suc alert-success" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Successful save!</strong>
	  </div>
	  <div class="alert alert_wr alert-danger" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Unsuccessful save!</strong>
	  </div>
	  <form id="form" action="symptoms.php" method="POST">
	  <input type="hidden" id="save" name="save" value="1">
		<input type="hidden" id="pat_id" name="pat_id" value="<?php echo $pat_id;?>">
		<div class="row">
					<div class="form-group col-md-12">
						<input type="submit" class="btn btn-primary" value="Save">
					</div>
				</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Symptoms not included in the criteria</small></h3>
					</div>
					<div class="box-body">
						<div class="row">
							<div class="form-group col-md-12" id="divdiseases" name="divdiseases">
							<?php
										$sql = get_symptoms_organs();
										$symptoms_organs_counter=0;
										
										while($result2 = pg_fetch_array($sql))
										{	
											$symptoms_organs_values .= $result2['value'];
											$symptoms_organs_values .= "!@#$%^" ;
											
											$symptoms_organs_ids .=$result2['symptoms_organs_id'];
											$symptoms_organs_ids .= "!@#$%^" ;
															
											$symptoms_organs_counter++;
										}
									?>
										<input type="hidden" value="<?php echo $symptoms_organs_counter; ?>" id="symptoms_organs_counter" name="symptoms_organs_counter"/>
										<input type="hidden" value="<?php echo $symptoms_organs_values; ?>" id="symptoms_organs_values" name="symptoms_organs_values"/>
										<input type="hidden" value="<?php echo $symptoms_organs_ids; ?>" id="symptoms_organs_ids" name="symptoms_organs_ids"/>
								  <table cellpadding="0" cellspacing="0" border="0" class="table table-bordered dataTable" role="grid" id="symptoms_not_included_tbl" name="symptoms_not_included_tbl"  width="100%"  rules="all">
									<thead>
										  <tr align="center"> 
											<th class="col-md-4"><b>Organ/System</b></th>
											<th class="col-md-4"><b>Symptom</b></th>
											<th class="col-md-3"><b>date of diagnosis</b></th>
											<th class="col-md-1">&nbsp;</th>
										</tr>
									 </thead>
									 <tbody>
									<?php
										$i=1;
										$j=1;
											$order = get_symptoms_not_included($pat_id);
											$num_rows = pg_num_rows($order);
											
											while($result = pg_fetch_assoc($order))
											{
												$symptoms_not_included_id=$result['symptoms_not_included_id'];
												$symptoms_organs_id=$result['symptoms_organs_id'];
												$symptoms_id=$result['symptoms_id'];
												$symptoms_date=$result['symptoms_date_str'];
												if($symptoms_date=="12-12-1900")
													$symptoms_date="";
												
												
											
											
									?>
										  <tr id="symptoms_not_included_tr_<?php echo $i;?>">
												<td align="center">
													<input type="hidden" id="symptoms_not_included_hidden_<?php echo $i;?>" name="symptoms_not_included_hidden_<?php echo $i;?>" value="1"/>
													<input type="hidden" name="symptoms_not_included_id_<?php echo $i; ?>" id="symptoms_not_included_id_<?php echo $i; ?>" value="<?php echo $symptoms_not_included_id;?>">
													<select  onchange="setsymptoms(<?php echo $i; ?>)" class="form-control" name="symptoms_organs_id_<?php echo $i; ?>" id="symptoms_organs_id_<?php echo $i; ?>">
														<option value="0">--</option>
														<?php
															$sql = get_symptoms_organs();
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$symptoms_organs_id2=$result2['symptoms_organs_id'];
																$value=$result2['value'];
																
																
																
														?>
														<option value="<?php echo $symptoms_organs_id2; ?>" <?php if($symptoms_organs_id==$symptoms_organs_id2) { echo "selected"; } ?>><?php echo $value; ?></option>
														<?php
															}
														?>
													</select>
												</td>
											   <td align="center">
													<select class="form-control" name="symptoms_id_<?php echo $i; ?>" id="symptoms_id_<?php echo $i; ?>">
														<?php
														if($symptoms_organs_id==0) 
														{
														?>
														<option value="0">No Symptoms</option>
														<?php
														}

															$sql = get_symptoms();
															$numrows = pg_num_rows($sql);
															while($result2 = pg_fetch_array($sql))
															{
																$symptoms_id2=$result2['symptoms_id'];
																$value=$result2['value'];
																$symptoms_organs_id2=$result2['symptoms_organs_id'];
																
																if($symptoms_organs_id2==$symptoms_organs_id) 
																{
																	
																
																
														?>
														<option value="<?php echo $symptoms_id2; ?>" <?php if($symptoms_id==$symptoms_id2) { echo "selected"; } ?>><?php echo $value; ?></option>
														<?php
																}
															}
														?>
													</select>
												</td>
												<td align="center">
													<input type="text" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="symptoms_date_<?php echo $i; ?>" name="symptoms_date_<?php echo $i; ?>" value="<?php echo $symptoms_date; ?>">
												</td>
												<td align="center">
													<a title="Delete" href="javascript:void(0);" onclick="symptoms_not_included_removerow(<?php echo $i; ?>);"><i class="fa fa-trash"></i><span></a>
												 </td>
												 </tr>
									<?php
											$i++;
											$j++;
											}
										
									?>
									</tbody>
								</table>
								<input type="hidden" name="symptoms_not_included_numofrows" id="symptoms_not_included_numofrows" value="<?php echo $num_rows; ?>">
								<br>
								 <a class="btn btn-primary" href="javascript:void(0);" onclick="symptoms_not_included_addrow();"><i class="fa fa-plus"></i> New record</a>
							 </div>
						</div>
					</div>
				</div><!-- diagnoses status div -->
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				<input type="submit" class="btn btn-primary" value="Save">
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				&nbsp;
			</div>
		</div>
		<div class="row">
			<div class="form-group col-md-12">
				&nbsp;
			</div>
		</div>
		</form>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  </div>
  <!-- /.content-wrapper -->
   <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>
$(function () {
	  
	   $('#form').submit(function(){
		
		var msg="";
		var sum=0;
		var row=0;
			
		var rows=document.getElementById('symptoms_not_included_numofrows').value;
		for(i = 1; i <= rows; i++)
		{
			if($('#symptoms_not_included_hidden_'+i).val()==1)
			{
				row++;
				if($('#symptoms_id_'+i).val()==0)
					sum++;
			}
		}
		
		if(sum>0 && row>sum)
			msg +='-You have selected that there is "No Symptoms" and you have selected a particular symptom at the same time!\n';
		
		
		if(msg != '')
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
		
		
	  });
	  
$(document).ajaxStart(function(){
		$('body').loading();
		
    });
    $(document).ajaxComplete(function(){
       $('body').loading('stop');
    });
	
$(document).ready(function () {    
	for(i=1;i<=document.getElementById('symptoms_not_included_numofrows').value;i++)
	{
		$("#symptoms_date_"+i).datepicker({
				dateFormat: 'dd-mm-yy',
				showButtonPanel: true,
				yearRange: "-100:+100",
				default: "0:0",
				changeMonth: true,
				changeYear: true,
				onClose2:function(dateText) {}
			});
			
			$('#symptoms_date_'+i).inputmask();

	}
});

function setsymptoms(id)
{
        var symptoms_organs_id = $("#symptoms_organs_id_"+id).val();
		
        $.ajax({
            url: 'populate_symptoms.php',
            type: 'post',
            data: {
				id:symptoms_organs_id
				},
            dataType: 'json',
            success:function(response){
			
                var len = response.length;
				
                $("#symptoms_id_"+id).empty();
				if(len==0)
					$("#symptoms_id_"+id).append("<option value='0'>No Symptoms</option>");
				
                for( var i = 0; i<len; i++){
                    var id2 = response[i]['id'];
                    var value = response[i]['value'];
					
                    $("#symptoms_id_"+id).append("<option value='"+id2+"'>"+value+"</option>");
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }
	
function symptoms_not_included_addrow()
{
  var tbl = document.getElementById('symptoms_not_included_tbl');
  var lastRow = tbl.rows.length;
  // if there's no header row in the table, then iteration = lastRow + 1
  var iteration = lastRow;
  var row = tbl.insertRow(lastRow);
   row.id= 'symptoms_not_included_tr_'+iteration;
  document.getElementById('symptoms_not_included_numofrows').value = iteration;
										
	var symptoms_organs_ids=document.getElementById('symptoms_organs_ids').value;
	var symptoms_organs_values=document.getElementById('symptoms_organs_values').value;
	var symptoms_organs_counter=document.getElementById('symptoms_organs_counter').value;
	var symptoms_organs_value=symptoms_organs_values.split('!@#$%^');
	var symptoms_organs_id=symptoms_organs_ids.split('!@#$%^');
	
  var r1 = row.insertCell(0);
  r1.align='center';
  
  var el1 = document.createElement('select');
  el1.name = 'symptoms_organs_id_' + iteration;
  el1.id = 'symptoms_organs_id_' + iteration;
  el1.onchange =function(){setsymptoms(iteration)};
  el1.style.width='100%';
  el1.className='form-control';
  
	var opt=new Option("--",0);
	el1.add(opt,undefined);
	for(i = 0; i < symptoms_organs_counter; i++)
	{
		var opt=new Option(symptoms_organs_value[i],symptoms_organs_id[i]);
		el1.add(opt,undefined);
	}
  
  var el2 = document.createElement('input');
  el2.type = 'hidden';
  el2.name = 'symptoms_not_included_hidden_' + iteration;
  el2.id = 'symptoms_not_included_hidden_' + iteration;
  el2.value='1';
  
  var el3 = document.createElement('input');
  el3.type = 'hidden';
  el3.name = 'symptoms_not_included_id_' + iteration;
  el3.id = 'symptoms_not_included_id_' + iteration;
  el3.value='';
  
  
  r1.appendChild(el1);
  r1.appendChild(el2);
  r1.appendChild(el3);
  
	var r2 = row.insertCell(1);
	r2.align='center';
	
	var el4 = document.createElement('select');
	el4.name = 'symptoms_id_' + iteration;
	el4.id = 'symptoms_id_' + iteration;
	el4.style.width='100%';
	el4.className='form-control';
  
	var opt=new Option("No Symptoms",0);
	el4.add(opt,undefined);
	r2.appendChild(el4);
  
	r2.appendChild(el4);
  
	var r3 = row.insertCell(2);
	r3.align='center';
	var el5 = document.createElement('input');
	el5.type = 'text';
	el5.name = 'symptoms_date_' + iteration;
	el5.id = 'symptoms_date_' + iteration;
	el5.className="form-control";
	el5.style.width='100%';
  
	r3.appendChild(el5);
  
	var r6 = row.insertCell(3);
	r6.align='center';
	
	var el8 = document.createElement('a');
	el8.name = 'a_' + iteration;
	el8.id = 'a_' + iteration;
	el8.title = 'Delete';
	el8.href="javascript:avoid(0)";
	var icon = document.createElement('i');
	icon.className='fa fa-trash-o';
	el8.appendChild(icon);
	el8.onclick =function(){symptoms_not_included_removerow(iteration)};
		 
	r6.appendChild(el8);
	
  $('#symptoms_date_'+iteration).datepicker({
	 dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		  onClose2:function(dateText) {}
		});
		$('#symptoms_date_'+iteration).inputmask('dd-mm-yyyy', { 'placeholder': 'dd-mm-yyyy' });
}

function symptoms_not_included_removerow(row)
{
	document.getElementById('symptoms_not_included_hidden_'+row).value='-1';
	document.getElementById('symptoms_not_included_tr_'+row).style.display='none';
}


</script>
<?php
	
if($save==1)
{
	if($save_chk=="ok")
	{
?>
<script>
$(".alert_suc").show();
window.setTimeout(function() {
    $(".alert_suc").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
	else
	{
?>
<script>
$(".alert_wr").show();
window.setTimeout(function() {
    $(".alert_wr").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
</script>
<?php
	}
}
?>
</body>
</html>
