﻿<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/validateLogin.php';
	
	$save=$_REQUEST['save'];
	$pat_id=$_REQUEST['pat_id'];
	$row=$_REQUEST['row'];
	$patient_pulmonary_id=$_REQUEST['patient_pulmonary_id'];
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>
</head>
<body class="hold-transition skin-blue layout-top-nav">
<?php
	if($save==1)
	{
		
					
		$patient_pulmonary_parameters[0]=date_for_postgres($_REQUEST['pulmonary_date']);
		$patient_pulmonary_parameters[1]=$_REQUEST['group'];
		$patient_pulmonary_parameters[2]=$_REQUEST['content'];
		$patient_pulmonary_parameters[3]=$_REQUEST['specifications'];
		$patient_pulmonary_parameters[4]=str_replace("'", "''",$_REQUEST['fvc']);
		$patient_pulmonary_parameters[5]=str_replace("'", "''",$_REQUEST['fev1']);
		$patient_pulmonary_parameters[6]=str_replace("'", "''",$_REQUEST['tlcosb']);
		$patient_pulmonary_parameters[7]=str_replace("'", "''",$_REQUEST['tlcova']);
		$patient_pulmonary_parameters[8]=str_replace("'", "''",$_REQUEST['tlc']);
		$patient_pulmonary_parameters[9]=$user_id;
		$patient_pulmonary_parameters[10]='now()';
		
		$patient_pulmonary_table_names[0]='pulmonary_date';
		$patient_pulmonary_table_names[1]='group_id';
		$patient_pulmonary_table_names[2]='content';
		$patient_pulmonary_table_names[3]='specifications';
		$patient_pulmonary_table_names[4]='fvc';
		$patient_pulmonary_table_names[5]='fev1';
		$patient_pulmonary_table_names[6]='tlcosb';
		$patient_pulmonary_table_names[7]='tlcova';
		$patient_pulmonary_table_names[8]='tlc';
		$patient_pulmonary_table_names[9]='editor_id';
		$patient_pulmonary_table_names[10]='edit_date';
		
		$patient_pulmonary_edit_name[0]='patient_pulmonary_id';
		$patient_pulmonary_code_edit_id[0]=$patient_pulmonary_id;
		$patient_pulmonary_code_sumbol[0]='=';
		if($patient_pulmonary_id=='')
		{
			$patient_pulmonary_parameters[11]=$pat_id;
			$patient_pulmonary_parameters[12]=0;
			$patient_pulmonary_table_names[11]='pat_id';
			$patient_pulmonary_table_names[12]='deleted';
				
			$patient_pulmonary_id=insert('patient_pulmonary',$patient_pulmonary_table_names,$patient_pulmonary_parameters,'patient_pulmonary_id');
			if($patient_pulmonary_id!='')
				$save_chk="ok";
		}
		else
			$save_chk=update('patient_pulmonary',$patient_pulmonary_table_names,$patient_pulmonary_parameters,$patient_pulmonary_edit_name,$patient_pulmonary_code_edit_id,$patient_pulmonary_code_sumbol);
		
	}
?>
<div class="wrapper">
  <!-- Main Header -->
  <?php
 // include "../portion/header.php";
  ?>
  <!-- Left side column. contains the logo and sidebar -->
  <?php
  //include "../portion/sidebar.php";
  ?>

	 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <!-- Content Header (Page header) -->
    <section class="content-header">
       <h4><b><?php if($patient_pulmonary_id=='') { echo 'Add'; } else { echo 'Edit'; } ?> Pulmonary involvment in autoimmune disease</b></h4>
	  </section>
			<br>
			 <!-- Main content -->
			<section class="content">
			<div class="alert alert_suc alert-success" role="alert" style="DISPLAY:none;">
		    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<strong>Successful save!</strong>
			  </div>
			  <div class="alert alert_wr alert-danger" role="alert" style="DISPLAY:none;">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<strong>Unsuccessful save!</strong>
			  </div>
			<form  id="form" action="pulmonary.php" method="POST"  enctype="multipart/form-data">
			<br>
			<?php
				if($patient_pulmonary_id!="")
				{
					$exec = get_pulmonary_spec($patient_pulmonary_id);
					$result = pg_fetch_array($exec);
					
					$patient_pulmonary_id=$result['patient_pulmonary_id'];
					$group=$result['group_id'];
					$content=$result['content'];
					$specifications=$result['specifications'];
					$pulmonary_date=$result['pulmonary_date_str'];
					if($pulmonary_date=="12-12-1900")
						$pulmonary_date="";
					$fvc=$result['fvc'];
					$fev1=$result['fev1'];
					$tlcosb=$result['tlcosb'];
					$tlcova=$result['tlcova'];
					$tlc=$result['tlc'];
				}
				?>
			<input type="hidden" id="save" name="save" value="1">
			<input type="hidden" id="patient_pulmonary_id" name="patient_pulmonary_id" value="<?php echo $patient_pulmonary_id;?>">
			<input type="hidden" id="row" name="row" value="<?php echo $row;?>">
			<input type="hidden" id="pat_id" name="pat_id" value="<?php echo $pat_id;?>">
			<div class="row">
				<div class="form-group col-md-4">
				  <label>*Pulmonary date</label>
					<input type="text" readonly="true" class="form-control pull-right" data-inputmask="'alias': 'dd-mm-yyyy'" id="pulmonary_date" name="pulmonary_date" value="<?php echo $pulmonary_date; ?>">
				</div>
				<div class="form-group col-md-8">
				  &nbsp;
				</div>
			</div>
				<fieldset>
				<legend></legend>
					<div class="row">
						<div class="form-group col-md-4">
						  <label>Group</label>
							<select onchange="setpulmonarycontent()" class="form-control" name="group" id="group">
								<option value="0">--</option>
									<?php
										$sql = get_lookup_tbl_values("pulmonary_group");
														
										$numrows = pg_num_rows($sql);
										while($result2 = pg_fetch_array($sql))
										{
											$id=$result2['id'];
											$value=$result2['value'];
																
									?>
									<option value="<?php echo $id; ?>" <?php if($id==$group) { echo "selected"; } ?> ><?php echo $value; ?></option>
									<?php
										}
									?>
											
							</select>
						</div>
						<div class="form-group col-md-4">
						  <label>Content/Values</label>
							<select onchange="setpulmonaryspecifications()" class="form-control" name="content" id="content">
									<option value="0">--</option>
									<?php
										if($group<>"" or $group<>0)
										{
											$sql = get_lookup_val($group,'pulmonary_values');
											$numrows = pg_num_rows($sql);
											while($result2 = pg_fetch_array($sql))
											{
												$id=$result2['id'];
												$value=$result2['value'];
											?>
											<option value="<?php echo $id; ?>" <?php if($id==$content) { echo "selected"; } ?> ><?php echo $value; ?></option>
											<?php
											}
										}
									?>
									
									</select>
						</div>
						<div class="form-group col-md-4">
						  <label>Specifications</label>
							<select class="form-control" name="specifications" id="specifications">
								<option value="0">--</option>
								<?php
									if($content<>"" or $content<>0)
									{
										$sql = get_lookup_val($content,'pulmonary_specifications');
										$numrows = pg_num_rows($sql);
										while($result2 = pg_fetch_array($sql))
										{
											$id=$result2['id'];
											$value=$result2['value'];
										?>
											<option value="<?php echo $id; ?>" <?php if($id==$specifications) { echo "selected"; } ?> ><?php echo $value; ?></option>
										<?php
										}
									}
								?>
								</select>
						</div>
					</div>
				</fieldset>
				<fieldset>
				<legend></legend>
					<div class="row">
						<div class="form-group col-md-2">
						  <label>FVC</label>
							<input type="text" class="form-control pull-right" name="fvc" id="fvc" value="<?php echo $fvc; ?>">
						</div>
						<div class="form-group col-md-2">
						  <label>FEV1</label>
							<input type="text" class="form-control pull-right" name="fev1" id="fev1" value="<?php echo $fev1; ?>">
						</div>
						<div class="form-group col-md-2">
						  <label>TLCO/SB</label>
							<input type="text" class="form-control pull-right" name="tlcosb" id="tlcosb" value="<?php echo $tlcosb; ?>">
						</div>
						<div class="form-group col-md-2">
						  <label>TLCO/VA</label>
							<input type="text" class="form-control pull-right" name="tlcova" id="tlcova" value="<?php echo $tlcova; ?>">
						</div>
						<div class="form-group col-md-2">
						  <label>TLC</label>
							<input type="text" class="form-control pull-right" name="tlc" id="tlc" value="<?php echo $tlc; ?>">
						</div>
					</div>
				</fieldset>
			<div class="row">
				<div class="form-group col-md-12">
					<input class="btn btn-l btn-primary" type="submit" value="OK">
					<input class="btn btn-l btn-primary" type="button" value="Cancel" name="close" onclick="javascript:return closeWin();">
				</div>
			</div>
				</form>
			 </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
//include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
<script>
$(document).ready(function() {
	
$('#pulmonary_date').datepicker({
	  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		onClose2:function(dateText) {}
	})
	$('#pulmonary_date').inputmask();
});
$(document).ajaxStart(function(){
		$('body').loading();
		
    });
    $(document).ajaxComplete(function(){
       $('body').loading('stop');
    });
	
function setpulmonarycontent()
{
        var group_id = $("#group").val();
		
        $.ajax({
            url: 'populate_content.php',
            type: 'post',
            data: {
				id:group_id
				},
            dataType: 'json',
            success:function(response){
			
                var len = response.length;
				
                $("#content").empty();
				$("#content").append("<option value='0'>--</option>");
				$("#specifications").empty();
				$("#specifications").append("<option value='0'>--</option>");
				
                for( var i = 0; i<len; i++){
                    var id2 = response[i]['id'];
					var value = response[i]['value'];
					
                    $("#content").append("<option value='"+id2+"'>"+value+"</option>");
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }

function setpulmonaryspecifications()
{
        var content_id = $("#content").val();
		
        $.ajax({
            url: 'populate_specifications.php',
            type: 'post',
            data: {
				id:content_id
				},
            dataType: 'json',
            success:function(response){
			
                var len = response.length;
				
                $("#specifications").empty();
				$("#specifications").append("<option value='0'>--</option>");
				
                for( var i = 0; i<len; i++){
                    var id2 = response[i]['id'];
					var value = response[i]['value'];
					
                    $("#specifications").append("<option value='"+id2+"'>"+value+"</option>");
                }
            },
            failure: function (response) {
                alert(response.d);
            }
        });
    }


function reload()
{
	//if (confirm('Έχετε αποθηκεύσει τα δεδομένα σας;Το παράθυρο θα κλείσει!'))
	//{
		var row=$("#row").val();
		if($("#row").val()!="")
		{
			var newrow=row;
			window.opener.document.getElementById('group_'+newrow).value=$("#group option:selected").text();
			window.opener.document.getElementById('content_'+newrow).value=$("#content option:selected").text();
			window.opener.document.getElementById('specifications_'+newrow).value=$("#specifications option:selected").text();
			window.opener.document.getElementById('pulmonary_date_'+newrow).value=$("#pulmonary_date").val();
		}
		else
		{
			window.opener.patient_pulmonary_addrow($("#pat_id").val(),$("#patient_pulmonary_id").val());
			$("#row").val(window.opener.document.getElementById('patient_pulmonary_numofrows').value);
			var newrow=$("#row").val();
			window.opener.document.getElementById('group_'+newrow).value=$("#group option:selected").text();
			window.opener.document.getElementById('content_'+newrow).value=$("#content option:selected").text();
			window.opener.document.getElementById('specifications_'+newrow).value=$("#specifications option:selected").text();
			window.opener.document.getElementById('pulmonary_date_'+newrow).value=$("#pulmonary_date").val();
			
		}
		
	//}
	//else
	//	return false;
}


$( document ).ready(function() {
	
	$('#patient_pulmonary_sle_date').datepicker({
	  dateFormat: 'dd-mm-yy',
		  showButtonPanel: true,
		  yearRange: "-100:+100",
		default: "0:0",
		changeMonth: true,
        changeYear: true,
		onClose2:function(dateText) {}
	})
	$('#patient_pulmonary_sle_date').inputmask();
	
	
	$('#neuroimaging').change(function(){ 
		
		if($('#neuroimaging').val()==2)
			$('#divneuroimaging').show();
		else
			$('#divneuroimaging').hide();
			
	});
	
	$('#csf_analysis').change(function(){ 
		
		if($('#csf_analysis').val()==2)
			$('#divcsf_analysis').show();
		else
			$('#divcsf_analysis').hide();
			
	});
	
});

function closeWin()
{
	return window.close();
}	


  $(function () {
	  
	  $('#form').submit(function(){
		
		var msg="";
		 
		 if($('#patient_pulmonary_sle_date').val()=='')
			msg +='-Fill pulmonary SLE date!\n'; 
		
		if($('#patient_pulmonary_sle').val()=='0')
			msg +='-Select pulmonary SLE!\n'; 
		
		if(msg != '')
		{
			alert(msg);
			return false;
		}
		else
			return true;
		
		}); 
		
	  });	
</script>
<?php
if($save==1)
{
	if($save_chk=="ok")
	{
?>
<script>
$(".alert_suc").show();
window.setTimeout(function() {
    $(".alert_suc").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);

reload();
window.close();
</script>
<?php
	}
	else
	{
?>
<script>
$(".alert_wr").show();
window.setTimeout(function() {
    $(".alert_wr").fadeTo(300, 0).slideUp(300, function(){
        $(this).hide(); 
    });
}, 2000);
window.close();
</script>
<?php
	}
}
?>
</body>
</html>