<?php
	session_start();
	
	function get_version()
	{
		$tablecol[0]='version';
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="";
		$query=select('db_version',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_cohort_names($pat_id)
	{
		$tablecol[0]=" patient_cohort_id,cohort_name,TO_CHAR(start_date, 'DD-MM-YYYY') AS patient_cohort_date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by start_date desc " ;
		$query=select('patient_cohort ',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_followup_dates($patient_cohort_id)
	{
		$tablecol[0]=" patient_followup_id,TO_CHAR(start_date, 'DD-MM-YYYY') AS patient_followup_date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_cohort_id';
		$editdata[1]=$patient_cohort_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by start_date desc " ;
		$query=select('patient_followup ',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function export_drugs()
	{
		$tablecol[0]="drugs.drugs_id,REPLACE (coalesce(drugs.code,''), ',', ' ') as code, REPLACE (coalesce(drugs.substance,''), ',', ' ')  as substance,REPLACE (coalesce(lookup_tbl_val.value,''), ',', ' ')  as route_of_administration_val";
		$editcol[0]='drugs.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='drugs.prim';
		$editdata[1]=0;
		$editsymbol[1]='=';
		$editcol[2]='drugs.previous';
		$editdata[2]=0;
		$editsymbol[2]='=';
		$ears[2]=1;
		$editcol[3]='drugs.code';
		$editdata[3]=" ('A7','A8','A10','H1','H2','H3','J') ";
		$editsymbol[3]=' not in ';
		$ears[3]=0;
		$group="";
		$order="" ;
		$query=select_psw('drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_activity_rpt($pat_id,$patient_cohort_id,$fumonth_cohort)
	{
		$tablecol[0]="patient_common.swollenjc28,patient_common.tenderjc28,patient_common.crp,patient_common.das28crp,patient_common.esr,patient_common.vasphysicial,patient_common.mhaq,patient_common.vasglobal,patient_common.vaspain,patient_common.weight,patient_common.height";
		$editcol[0]='patient_followup.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_common.deleted';
		$editdata[1]=0;
		$editsymbol[1]='=';
		$ears[1]=1;		
		$editcol[2]='patient_followup.patient_cohort_id';
		$editdata[2]=$patient_cohort_id;
		$editsymbol[2]='=';
		$ears[2]=1;
		$editcol[3]='patient_common.pat_id';
		$editdata[3]=$pat_id;
		$editsymbol[3]='=';
		$ears[3]=1;
		$editcol[4]="patient_followup.fumonthcohort";
		$editdata[4]=$fumonth_cohort;
		$editsymbol[4]='=';
		$ears[4]=1;
		$group="";
		$order="" ;
		$query=select(' patient_common left join patient_followup on patient_common.patient_followup_id=patient_followup.patient_followup_id ',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_spa_rpt($pat_id,$patient_cohort_id,$fumonth_cohort)
	{
		$tablecol[0]="patient_spa.basdai,patient_spa.basdai1,patient_spa.basdai2,patient_spa.basdai3,patient_spa.basdai4,patient_spa.basdai5,patient_spa.basdai6,patient_spa.basfi";
		$editcol[0]='patient_followup.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_spa.deleted';
		$editdata[1]=0;
		$editsymbol[1]='=';
		$ears[1]=1;		
		$editcol[2]='patient_followup.patient_cohort_id';
		$editdata[2]=$patient_cohort_id;
		$editsymbol[2]='=';
		$ears[2]=1;
		$editcol[3]='patient_spa.pat_id';
		$editdata[3]=$pat_id;
		$editsymbol[3]='=';
		$ears[3]=1;
		$editcol[4]="patient_followup.fumonthcohort";
		$editdata[4]=$fumonth_cohort;
		$editsymbol[4]='=';
		$ears[4]=1;
		$group="";
		$order="" ;
		$query=select(' patient_spa left join patient_followup on patient_spa.patient_followup_id=patient_followup.patient_followup_id ',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_euroqol_rpt($pat_id,$patient_cohort_id,$fumonth_cohort)
	{
		$tablecol[0]="patient_euroqol.euroqol_score,patient_euroqol.eq5d";
		$editcol[0]='patient_followup.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_euroqol.deleted';
		$editdata[1]=0;
		$editsymbol[1]='=';
		$ears[1]=1;		
		$editcol[2]='patient_followup.patient_cohort_id';
		$editdata[2]=$patient_cohort_id;
		$editsymbol[2]='=';
		$ears[2]=1;
		$editcol[3]='patient_euroqol.pat_id';
		$editdata[3]=$pat_id;
		$editsymbol[3]='=';
		$ears[3]=1;
		$editcol[4]="patient_followup.fumonthcohort";
		$editdata[4]=$fumonth_cohort;
		$editsymbol[4]='=';
		$ears[4]=1;
		$group="";
		$order="" ;
		$query=select(' patient_euroqol left join patient_followup on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id ',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_drugs_corticosteroids_dosage($pat_id,$drugs_id,$patient_cohort_id,$fumonth_cohort)
	{
		$tablecol[0]="weekdosage";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_cohort_id';
		$editdata[1]=$patient_cohort_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$editcol[2]='pat_id';
		$editdata[2]=$pat_id;
		$editsymbol[2]='=';
		$ears[2]=1;
		$editcol[3]='drugs_id';
		$editdata[3]=$drugs_id;
		$editsymbol[3]='=';
		$ears[3]=1;
		$editcol[4]="fumonth_cohort";
		$editdata[4]=$fumonth_cohort;
		$editsymbol[4]='=';
		$ears[4]=1;
		$editcol[5]='drug_flag';
		$editdata[5]=2;
		$editsymbol[5]='=';
		$ears[5]=1;
		$group="";
		$order=" order by fumonth_cohort asc" ;
		$query=select('patient_lookup_drugs',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_drugs_ongoing_dosage($pat_id,$drugs_id,$patient_cohort_id,$fumonth_cohort)
	{
		$tablecol[0]="weekdosage";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_cohort_id';
		$editdata[1]=$patient_cohort_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$editcol[2]='pat_id';
		$editdata[2]=$pat_id;
		$editsymbol[2]='=';
		$ears[2]=1;
		$editcol[3]='drugs_id';
		$editdata[3]=$drugs_id;
		$editsymbol[3]='=';
		$ears[3]=1;
		$editcol[4]="fumonth_cohort";
		$editdata[4]=$fumonth_cohort;
		$editsymbol[4]='=';
		$ears[4]=1;
		$editcol[5]='drug_flag';
		$editdata[5]=1;
		$editsymbol[5]='=';
		$ears[5]=1;
		$group="";
		$order=" order by fumonth_cohort asc" ;
		$query=select('patient_lookup_drugs',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_spa($patient_followup_id)
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_followup_id';
		$editdata[1]=$patient_followup_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order="" ;
		$query=select('patient_spa',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_prv_sle_severity($pat_id)
	{
		$str="patient_sle_severity.*";
		for ($i=1;$i<=41;$i++)
		{
			$str .= ",TO_CHAR(patient_sle_severity.slicc".$i."_date, 'DD-MM-YYYY') AS slicc".$i."_date_str";
		}
		for ($i=1;$i<=12;$i++)
		{
			$str .= ",TO_CHAR(patient_sle_severity.index".$i."_date, 'DD-MM-YYYY') AS index".$i."_date_str";
		}
		
		$tablecol[0]=$str;
		$editcol[0]='patient_sle_severity.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_sle_severity.pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		//$order="  order by patient_followup_id desc limit 1  " ;
		$order="  order by patient_followup.start_date desc limit 1 " ;
		//$query=select('patient_sle_severity',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		$query=select('patient_sle_severity left join patient_followup on patient_sle_severity.patient_followup_id=patient_followup.patient_followup_id ',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_sle_severity($patient_followup_id)
	{
		$str="*";
		for ($i=1;$i<=41;$i++)
		{
			$str .= ",TO_CHAR(slicc".$i."_date, 'DD-MM-YYYY') AS slicc".$i."_date_str";
		}
		for ($i=1;$i<=12;$i++)
		{
			$str .= ",TO_CHAR(index".$i."_date, 'DD-MM-YYYY') AS index".$i."_date_str";
		}
		
		$tablecol[0]=$str;
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_followup_id';
		$editdata[1]=$patient_followup_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order="" ;
		$query=select('patient_sle_severity',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_sle($patient_followup_id)
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_followup_id';
		$editdata[1]=$patient_followup_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order="" ;
		$query=select('patient_sle',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_prv_common($pat_id)
	{
		$tablecol[0]="patient_common.*";
		$editcol[0]='patient_common.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_common.pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		//$order="  order by patient_followup_id desc limit 1 " ;
		$order="  order by patient_followup.start_date desc limit 1 " ;
		$query=select('patient_common left join patient_followup on patient_common.patient_followup_id=patient_followup.patient_followup_id ',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_common($patient_followup_id)
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_followup_id';
		$editdata[1]=$patient_followup_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order="" ;
		$query=select('patient_common',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_patient_neuropsychiatric($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(patient_neuropsychiatric_sle_date, 'DD-MM-YYYY') AS patient_neuropsychiatric_sle_date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order="" ;
		$query=select('patient_neuropsychiatric_sle',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_patient_neuropsychiatric_spec($patient_neuropsychiatric_sle_id)
	{
		$tablecol[0]="*,TO_CHAR(patient_neuropsychiatric_sle_date, 'DD-MM-YYYY') AS patient_neuropsychiatric_sle_date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_neuropsychiatric_sle_id';
		$editdata[1]=$patient_neuropsychiatric_sle_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order="" ;
		$query=select('patient_neuropsychiatric_sle',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_patient_lupus_nefritis($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(patient_lupus_nefritis_date, 'DD-MM-YYYY') AS patient_lupus_nefritis_date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order="" ;
		$query=select('patient_lupus_nefritis',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_patient_lupus_nefritis_spec($patient_lupus_nefritis_id)
	{
		$tablecol[0]="*,TO_CHAR(patient_lupus_nefritis_date, 'DD-MM-YYYY') AS patient_lupus_nefritis_date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_lupus_nefritis_id';
		$editdata[1]=$patient_lupus_nefritis_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order="" ;
		$query=select('patient_lupus_nefritis',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_patient_cutaneous($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(patient_cutaneous_date, 'DD-MM-YYYY') AS patient_cutaneous_date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order="" ;
		$query=select('patient_cutaneous',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_pulmonary_spec($patient_pulmonary_id)
	{
		$tablecol[0]="*,TO_CHAR(pulmonary_date, 'DD-MM-YYYY') AS pulmonary_date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_pulmonary_id';
		$editdata[1]=$patient_pulmonary_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order="" ;
		$query=select('patient_pulmonary',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_pulmonary($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(pulmonary_date, 'DD-MM-YYYY') AS pulmonary_date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order="" ;
		$query=select('patient_pulmonary',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_sf36($patient_followup_id)
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_followup_id';
		$editdata[1]=$patient_followup_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order="" ;
		$query=select('patient_sf36',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_hads($patient_followup_id)
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_followup_id';
		$editdata[1]=$patient_followup_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order="" ;
		$query=select('patient_hads',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_wpai($patient_followup_id)
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_followup_id';
		$editdata[1]=$patient_followup_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order="" ;
		$query=select('patient_wpai',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_euroqol($patient_followup_id)
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_followup_id';
		$editdata[1]=$patient_followup_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order="" ;
		$query=select('patient_euroqol',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_medra_specific_medrasoc($medrasoc_id)
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='medrasoc_id';
		$editdata[1]=$medrasoc_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order="" ;
		$query=select('medra',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_medrasoc()
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="" ;
		$query=select('medrasoc',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_medra()
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="" ;
		$query=select('medra',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_medra_val($medra_id)
	{
		$tablecol[0]="*";
		$editcol[0]='medra_id';
		$editdata[0]=$medra_id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="" ;
		$query=select('medra',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_patient_age($pat_id)
	{
		$tablecol[0]="TO_CHAR(dateofbirth, 'DD-MM-YYYY') AS dateofbirth_str";
		$editcol[0]='pat_id';
		$editdata[0]=$pat_id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="" ;
		$query=select('patient',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_cohort($patient_cohort_id)
	{
		$tablecol[0]="patient_cohort.*,TO_CHAR(date_phys, 'DD-MM-YYYY') AS date_phys_str,TO_CHAR(axial_date, 'DD-MM-YYYY') AS axial_date_str,TO_CHAR(rheumatoid_arthritis_date, 'DD-MM-YYYY') AS rheumatoid_arthritis_date_str,discontinuation_reason.value as stop_reason_str,drugs.code || ' - ' || drugs.substance as cohort_name_str,TO_CHAR(start_date, 'DD-MM-YYYY') AS start_date_str,TO_CHAR(stop_date, 'DD-MM-YYYY') AS stop_date_str";
		$editcol[0]='patient_cohort.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_cohort_id';
		$editdata[1]=$patient_cohort_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by start_date desc " ;
		$query=select('patient_cohort left outer join drugs on patient_cohort.cohort_name=drugs.drugs_id  left outer join discontinuation_reason on patient_cohort.stop_reason=discontinuation_reason.discontinuation_reason_id ',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_prv_cohort($pat_id)
	{
		$tablecol[0]="patient_cohort.*,TO_CHAR(axial_date, 'DD-MM-YYYY') AS axial_date_str,TO_CHAR(rheumatoid_arthritis_date, 'DD-MM-YYYY') AS rheumatoid_arthritis_date_str ";
		$editcol[0]='patient_cohort.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_cohort_id desc limit 1" ;
		$query=select('patient_cohort ',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_cohorts_exc($pat_id,$patient_cohort_id)
	{
		$tablecol[0]="patient_cohort.*,discontinuation_reason.value as stop_reason_str,drugs.code || ' - ' || drugs.substance as cohort_name_str,TO_CHAR(start_date, 'DD-MM-YYYY') AS start_date_str,TO_CHAR(stop_date, 'DD-MM-YYYY') AS stop_date_str";
		$editcol[0]='patient_cohort.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		if($patient_cohort_id<>"")
		{
			$editcol[2]='patient_cohort_id';
			$editdata[2]=$patient_cohort_id;
			$editsymbol[2]='<>';
			$ears[2]=1;
		}
		$group="";
		$order=" order by start_date desc " ;
		$query=select('patient_cohort left outer join drugs on patient_cohort.cohort_name=drugs.drugs_id  left outer join discontinuation_reason on patient_cohort.stop_reason=discontinuation_reason.discontinuation_reason_id ',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_cohorts($pat_id)
	{
		$tablecol[0]="patient_cohort.*,discontinuation_reason.value as stop_reason_str,drugs.code || ' - ' || drugs.substance as cohort_name_str,TO_CHAR(start_date, 'DD-MM-YYYY') AS start_date_str,TO_CHAR(stop_date, 'DD-MM-YYYY') AS stop_date_str";
		$editcol[0]='patient_cohort.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by start_date desc " ;
		$query=select('patient_cohort left outer join drugs on patient_cohort.cohort_name=drugs.drugs_id  left outer join discontinuation_reason on patient_cohort.stop_reason=discontinuation_reason.discontinuation_reason_id ',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_first_followup($patient_cohort_id)
	{
		$tablecol[0]="patient_followup_id";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_cohort_id';
		$editdata[1]=$patient_cohort_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by start_date asc " ;
		$query=select('patient_followup',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_prv_followup($pat_id)
	{
		$tablecol[0]=" * ";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by start_date desc limit 1 " ;
		$query=select('patient_followup ',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_followup($patient_followup_id)
	{
		$tablecol[0]="patient_followup.*,TO_CHAR(patient_followup.start_date, 'DD-MM-YYYY') AS start_date_str,drugs.code || ' - ' || drugs.substance as cohort_name_str";
		$editcol[0]='patient_followup.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_followup.patient_followup_id';
		$editdata[1]=$patient_followup_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_followup.start_date desc " ;
		$query=select('patient_followup  join patient_cohort on patient_cohort.patient_cohort_id=patient_followup.patient_cohort_id left outer join drugs on patient_cohort.cohort_name=drugs.drugs_id',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_last_followup($pat_id)
	{
		$tablecol[0]="TO_CHAR(patient_followup.start_date, 'DD-MM-YYYY') AS start_date_str";
		$editcol[0]='patient_followup.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_followup.pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_followup.start_date desc " ;
		$query=select('patient_followup',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_followups($patient_cohort_id)
	{
		$tablecol[0]="patient_followup.*,TO_CHAR(patient_followup.start_date, 'DD-MM-YYYY') AS start_date_str,drugs.code || ' - ' || drugs.substance as cohort_name_str";
		$editcol[0]='patient_followup.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_followup.patient_cohort_id';
		$editdata[1]=$patient_cohort_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_followup.start_date desc " ;
		$query=select('patient_followup join patient_cohort on patient_cohort.patient_cohort_id=patient_followup.patient_cohort_id left outer join drugs on patient_cohort.cohort_name=drugs.drugs_id',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_all_events($patient_cohort_id)
	{
		$tablecol[0]="patient_event.*,TO_CHAR(patient_event.start_date, 'DD-MM-YYYY') AS start_date_str,TO_CHAR(patient_followup.start_date, 'DD-MM-YYYY') AS followupdate_str,drugs.code || ' - ' || drugs.substance as cohort_name_str";
		$editcol[0]='patient_event.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_event.patient_cohort_id';
		$editdata[1]=$patient_cohort_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_event.start_date desc " ;
		$query=select('patient_event join patient_followup on patient_event.patient_followup_id=patient_followup.patient_followup_id join patient_cohort on patient_cohort.patient_cohort_id=patient_followup.patient_cohort_id left outer join drugs on patient_cohort.cohort_name=drugs.drugs_id',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_events($patient_followup_id)
	{
		$tablecol[0]="patient_event.*,TO_CHAR(patient_event.start_date, 'DD-MM-YYYY') AS start_date_str,TO_CHAR(patient_followup.start_date, 'DD-MM-YYYY') AS followupdate_str,drugs.code || ' - ' || drugs.substance as cohort_name_str";
		$editcol[0]='patient_event.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_event.patient_followup_id';
		$editdata[1]=$patient_followup_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_event.start_date desc " ;
		$query=select('patient_event join patient_followup on patient_event.patient_followup_id=patient_followup.patient_followup_id join patient_cohort on patient_cohort.patient_cohort_id=patient_followup.patient_cohort_id left outer join drugs on patient_cohort.cohort_name=drugs.drugs_id',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_event($patient_event_id)
	{
		$tablecol[0]="patient_event.*,TO_CHAR(patient_event.start_date, 'DD-MM-YYYY') AS start_date_str,TO_CHAR(patient_followup.start_date, 'DD-MM-YYYY') AS followupdate_str,drugs.code || ' - ' || drugs.substance as cohort_name_str";
		$editcol[0]='patient_event.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_event.patient_event_id';
		$editdata[1]=$patient_event_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_event.start_date desc " ;
		$query=select('patient_event join patient_followup on patient_event.patient_followup_id=patient_followup.patient_followup_id join patient_cohort on patient_cohort.patient_cohort_id=patient_followup.patient_cohort_id left outer join drugs on patient_cohort.cohort_name=drugs.drugs_id',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_spa_extraarticular($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(date, 'DD-MM-YYYY') AS date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_spa_extraarticular_id asc " ;
		$query=select('patient_spa_extraarticular',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_ra_extraarticular($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(date, 'DD-MM-YYYY') AS date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_ra_extraarticular_id asc " ;
		$query=select('patient_ra_extraarticular',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_patient_antibodies($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(patient_antibodies_date, 'DD-MM-YYYY') AS patient_antibodies_date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_autoantibodies_id asc " ;
		$query=select('patient_autoantibodies',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_patient_cohort_drugs_period($patient_cohort_drugs_id)
	{
		$tablecol[0]="*,TO_CHAR(startdate, 'DD-MM-YYYY') AS startdate_str,TO_CHAR(stopdate, 'DD-MM-YYYY') AS stopdate_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_cohort_drugs_id';
		$editdata[1]=$patient_cohort_drugs_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by startdate desc " ;
		$query=select('patient_cohort_drugs_period',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_patient_cohort_drugs($patient_cohort_id)
	{
		$tablecol[0]="patient_cohort_drugs.*,TO_CHAR(startdate, 'DD-MM-YYYY') AS startdate_str,TO_CHAR(stopdate, 'DD-MM-YYYY') AS stopdate_str,drugs.min,drugs.max";
		$editcol[0]='patient_cohort_drugs.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_cohort_id';
		$editdata[1]=$patient_cohort_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_cohort_drugs_id asc " ;
		$query=select('patient_cohort_drugs join drugs on patient_cohort_drugs.drugs_id=drugs.drugs_id',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_patient_followup_drugs_period($patient_followup_drugs_id)
	{
		$tablecol[0]="*,TO_CHAR(startdate, 'DD-MM-YYYY') AS startdate_str,TO_CHAR(stopdate, 'DD-MM-YYYY') AS stopdate_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_followup_drugs_id';
		$editdata[1]=$patient_followup_drugs_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by startdate asc " ;
		$query=select('patient_followup_drugs_period',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_patient_followup_drugs($patient_followup_id)
	{
		$tablecol[0]="*,TO_CHAR(startdate, 'DD-MM-YYYY') AS startdate_str,TO_CHAR(stopdate, 'DD-MM-YYYY') AS stopdate_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_followup_id';
		$editdata[1]=$patient_followup_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by startdate asc " ;
		$query=select('patient_followup_drugs',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_patient_cohort_previous_drugs($patient_cohort_id)
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_cohort_id';
		$editdata[1]=$patient_cohort_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_cohort_previous_drugs_id asc " ;
		$query=select('patient_cohort_previous_drugs',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_patient_previous_drugs($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(start_date, 'DD-MM-YYYY') AS start_date_str,TO_CHAR(end_date, 'DD-MM-YYYY') AS end_date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_previous_drugs_id asc " ;
		$query=select('patient_previous_drugs',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_discontinuation_reason()
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order=" order by value " ;
		$query=select('discontinuation_reason',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_all_patient_analgesics($patient_cohort_id)
	{
		$tablecol[0]="patient_analgesics.*,TO_CHAR(patient_analgesics.startdate, 'DD-MM-YYYY') AS startdate_str,TO_CHAR(patient_analgesics.stopdate, 'DD-MM-YYYY') AS stopdate_str,TO_CHAR(patient_followup.start_date, 'DD-MM-YYYY') AS patient_followup_date_str";
		$editcol[0]='patient_analgesics.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_analgesics.patient_cohort_id';
		$editdata[1]=$patient_cohort_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_followup.start_date desc,patient_analgesics.startdate desc " ;
		$query=select('patient_analgesics join patient_followup on patient_followup.patient_followup_id=patient_analgesics.patient_followup_id',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_patient_analgesics($patient_followup_id)
	{
		$tablecol[0]="*,TO_CHAR(startdate, 'DD-MM-YYYY') AS startdate_str,TO_CHAR(stopdate, 'DD-MM-YYYY') AS stopdate_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_followup_id';
		$editdata[1]=$patient_followup_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by startdate desc " ;
		$query=select('patient_analgesics',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_analgesic($analgesics_id)
	{
		$tablecol[0]="*";
		$editcol[0]='analgesics_id';
		$editdata[0]=$analgesics_id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="" ;
		$query=select('analgesics',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_analgesics()
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="" ;
		$query=select('analgesics',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_all_patient_corticosteroids($patient_cohort_id)
	{
		$tablecol[0]="patient_corticosteroids.*,TO_CHAR(patient_corticosteroids.date, 'DD-MM-YYYY') AS date_str,TO_CHAR(patient_followup.start_date, 'DD-MM-YYYY') AS patient_followup_date_str";
		$editcol[0]='patient_corticosteroids.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_corticosteroids.patient_cohort_id';
		$editdata[1]=$patient_cohort_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_corticosteroids.date desc " ;
		$query=select('patient_corticosteroids join patient_followup on patient_followup.patient_followup_id=patient_corticosteroids.patient_followup_id',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_patient_corticosteroids($patient_followup_id)
	{
		$tablecol[0]="*,TO_CHAR(date, 'DD-MM-YYYY') AS date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='patient_followup_id';
		$editdata[1]=$patient_followup_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by date desc " ;
		$query=select('patient_corticosteroids',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_corticosteroid($corticosteroids_id)
	{
		$tablecol[0]="*";
		$editcol[0]='corticosteroids_id';
		$editdata[0]=$corticosteroids_id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="" ;
		$query=select('corticosteroids',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_corticosteroids()
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="" ;
		$query=select('corticosteroids',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	
	function get_drugs_frequency($drugs_frequency_id)
	{
		$tablecol[0]="*";
		$editcol[0]='drugs_frequency_id';
		$editdata[0]=$drugs_frequency_id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="" ;
		$query=select('drugs_frequency',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_drugs_frequencies()
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order=" order by ordering asc " ;
		$query=select('drugs_frequency',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_drugs_ongoing_rpt()
	{
		$tablecol[0]="drugs.drugs_id";
		$editcol[0]='drugs.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='drugs.prim';
		$editdata[1]=0;
		$editsymbol[1]='=';
		$ears[1]=1;
		$editcol[2]='drugs.previous';
		$editdata[2]=0;
		$editsymbol[2]='=';
		$ears[2]=1;
		$group="";
		$order=" order by drugs_id asc " ;
		$query=select('drugs',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_drugs_ongoing()
	{
		$tablecol[0]="drugs.*,lookup_tbl_val.value as route_of_administration_val";
		$editcol[0]='drugs.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='drugs.prim';
		$editdata[1]=0;
		$editsymbol[1]='=';
		$ears[1]=1;
		$editcol[2]='drugs.previous';
		$editdata[2]=0;
		$editsymbol[2]='=';
		$ears[2]=1;
		$group="";
		$order=" order by drugs_id asc " ;
		$query=select('drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_drugs_primary()
	{
		$tablecol[0]="drugs.*,lookup_tbl_val.value as route_of_administration_val";
		$editcol[0]='drugs.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='drugs.ongoing';
		$editdata[1]=0;
		$editsymbol[1]='=';
		$ears[1]=1;
		$editcol[2]='drugs.previous';
		$editdata[2]=0;
		$editsymbol[2]='=';
		$ears[2]=1;
		$group="";
		$order=" order by drugs_id asc " ;
		$query=select('drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_comorbidities_rpt()
	{
		$tablecol[0]="diseases_comorbidities_id";
		$editcol[0]='diseases_comorbidities_id';
		$editdata[0]="(1,2,3,4,5,6,7,8,9,11,12,14,17,18,19,20,23,24,28,34,35,40,41,42,43,44,50,51,52,53,54,55,56,57,58,59,61,64,65,69,74,75,76,78,80,81,82,83,85,86,87,88,89,90,91,98,100,106,107,108,109,110,111,112,116,117,118,119,120)";
		$editsymbol[0]=' in ';
		$ears[0]=0;
		$group="";
		$order=" order by diseases_comorbidities_id asc " ;
		$query=select_psw('diseases_comorbidities',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_drugs_anti_spec($drugs_id)
	{
		$tablecol[0]="drugs.*,lookup_tbl_val.value as route_of_administration_val";
		$editcol[0]='drugs.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='drugs.ongoing';
		$editdata[1]=0;
		$editsymbol[1]='=';
		$ears[1]=1;
		$editcol[2]='drugs.prim';
		$editdata[2]=0;
		$editsymbol[2]='=';
		$ears[2]=1;
		$editcol[3]='drugs.drugs_id';
		$editdata[3]=$drugs_id;
		$editsymbol[3]='=';
		$ears[3]=1;
		$group="";
		$order="" ;
		$query=select('drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_drugs_anti()
	{
		$tablecol[0]="drugs.*,lookup_tbl_val.value as route_of_administration_val";
		$editcol[0]='drugs.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='drugs.ongoing';
		$editdata[1]=0;
		$editsymbol[1]='=';
		$ears[1]=1;
		$editcol[2]='drugs.prim';
		$editdata[2]=0;
		$editsymbol[2]='=';
		$ears[2]=1;
		$group="";
		$order=" order by NULLIF(regexp_replace(drugs.code, '[^a-zA-Z]', '', 'g'), '')::text asc,NULLIF(regexp_replace(drugs.code, '\D', '', 'g'), '')::int asc " ;
		$query=select('drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_drug($drugs_id)
	{
		$tablecol[0]="drugs.*,lookup_tbl_val.value as route_of_administration_val";
		$editcol[0]='drugs_id';
		$editdata[0]=$drugs_id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order=" order by drugs_id asc " ;
		$query=select('drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_drugs()
	{
		$tablecol[0]="drugs.*,lookup_tbl_val.value as route_of_administration_val";
		$editcol[0]='drugs.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="" ;
		$query=select('drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_biobankings($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(biobanking_date, 'DD-MM-YYYY') AS biobanking_date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_biobanking_id asc " ;
		$query=select('patient_biobanking',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}

	function get_symptoms_organs()
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="" ;
		$query=select('symptoms_organs',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_symptoms_not_included($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(symptoms_date, 'DD-MM-YYYY') AS symptoms_date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by symptoms_not_included_id asc " ;
		$query=select('symptoms_not_included',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_symptom($symptoms_id)
	{
		$tablecol[0]="*";
		$editcol[0]='symptoms_id';
		$editdata[0]=$symptoms_id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="" ;
		$query=select('symptoms',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_symptoms()
	{
		$tablecol[0]="symptoms.*,symptoms_organs.value as symptoms_organs_val";
		$editcol[0]='symptoms.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="" ;
		$query=select('symptoms join symptoms_organs on symptoms.symptoms_organs_id=symptoms_organs.symptoms_organs_id',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_nonrheumatic_diseases_therapy($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(start_date, 'DD-MM-YYYY') AS start_date_str,TO_CHAR(stop_date, 'DD-MM-YYYY') AS stop_date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by nonrheumatic_diseases_therapy_id asc " ;
		$query=select('nonrheumatic_diseases_therapy',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_nonrheumatic_diseases($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(date_of_diagnosis, 'DD-MM-YYYY') AS date_of_diagnosis_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by nonrheumatic_diseases_id asc " ;
		$query=select('nonrheumatic_diseases',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_diseases_therapy_organs()
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="" ;
		$query=select('diseases_therapy_organs',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_nonrheumatic_disease($pat_id,$diseases_comorbidities_id)
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$editcol[2]='diseases_comorbidities_id';
		$editdata[2]=$diseases_comorbidities_id;
		$editsymbol[2]='=';
		$ears[2]=1;
		$group="";
		$order="" ;
		$query=select('nonrheumatic_diseases',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_diseases_organs()
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="" ;
		$query=select('diseases_organs',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_othercriteria($diagnosis_id,$othercriteria_id)
	{
		$tablecol[0]="active";
		$editcol[0]='diagnosis_id';
		$editdata[0]=$diagnosis_id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='othercriteria_id';
		$editdata[1]=$othercriteria_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$editcol[2]='deleted';
		$editdata[2]=0;
		$editsymbol[2]='=';
		$ears[2]=1;
		$group="";
		$order="" ;
		$query=select('othercriteria_per_diagnosis',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_criteria($diagnosis_id,$criteria_id)
	{
		$tablecol[0]="active";
		$editcol[0]='diagnosis_id';
		$editdata[0]=$diagnosis_id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='criteria_id';
		$editdata[1]=$criteria_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$editcol[2]='deleted';
		$editdata[2]=0;
		$editsymbol[2]='=';
		$ears[2]=1;
		$group="";
		$order="" ;
		$query=select('criteriagroup_per_diagnosis',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_criteriasle_res($pat_id)
	{
		$tablecol[0]="sle1997,sle2012,sle2017";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_criteriasle_id asc" ;
		$query=select('patient_criteriasle',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_criteriasle($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(sle2017date, 'DD-MM-YYYY') AS sle2017date_str,TO_CHAR(criteriadatesle20129, 'DD-MM-YYYY') AS criteriadatesle20129_str,TO_CHAR(criteriadatesle201210, 'DD-MM-YYYY') AS criteriadatesle201210_str,TO_CHAR(criteriadatesle201211, 'DD-MM-YYYY') AS criteriadatesle201211_str,TO_CHAR(criteriadatesle2012b1, 'DD-MM-YYYY') AS criteriadatesle2012b1_str,TO_CHAR(criteriadatesle2012b2, 'DD-MM-YYYY') AS criteriadatesle2012b2_str,TO_CHAR(criteriadatesle2012b3, 'DD-MM-YYYY') AS criteriadatesle2012b3_str,TO_CHAR(criteriadatesle2012b4, 'DD-MM-YYYY') AS criteriadatesle2012b4_str,TO_CHAR(criteriadatesle2012b5, 'DD-MM-YYYY') AS criteriadatesle2012b5_str,TO_CHAR(criteriadatesle2012b6, 'DD-MM-YYYY') AS criteriadatesle2012b6_str,TO_CHAR(criteriadatesle2012c1, 'DD-MM-YYYY') AS criteriadatesle2012c1_str,TO_CHAR(sle2012date, 'DD-MM-YYYY') AS sle2012date_str,TO_CHAR(criteriadatesle2017a1, 'DD-MM-YYYY') AS criteriadatesle2017a1_str,TO_CHAR(criteriadatesle2017b1, 'DD-MM-YYYY') AS criteriadatesle2017b1_str,TO_CHAR(criteriadatesle2017b21, 'DD-MM-YYYY') AS criteriadatesle2017b21_str,TO_CHAR(criteriadatesle2017b22, 'DD-MM-YYYY') AS criteriadatesle2017b22_str,TO_CHAR(criteriadatesle2017b23, 'DD-MM-YYYY') AS criteriadatesle2017b23_str,TO_CHAR(criteriadatesle2017b3, 'DD-MM-YYYY') AS criteriadatesle2017b3_str,TO_CHAR(criteriadatesle2017b41, 'DD-MM-YYYY') AS criteriadatesle2017b41_str,TO_CHAR(criteriadatesle2017b42, 'DD-MM-YYYY') AS criteriadatesle2017b42_str,TO_CHAR(criteriadatesle2017b43, 'DD-MM-YYYY') AS criteriadatesle2017b43_str,TO_CHAR(criteriadatesle2017b51, 'DD-MM-YYYY') AS criteriadatesle2017b51_str,TO_CHAR(criteriadatesle2017b52, 'DD-MM-YYYY') AS criteriadatesle2017b52_str,TO_CHAR(criteriadatesle2017b61, 'DD-MM-YYYY') AS criteriadatesle2017b61_str,TO_CHAR(criteriadatesle2017b62, 'DD-MM-YYYY') AS criteriadatesle2017b62_str,TO_CHAR(criteriadatesle2017b63, 'DD-MM-YYYY') AS criteriadatesle2017b63_str,TO_CHAR(criteriadatesle2017b71, 'DD-MM-YYYY') AS criteriadatesle2017b71_str,TO_CHAR(criteriadatesle2017b72, 'DD-MM-YYYY') AS criteriadatesle2017b72_str,TO_CHAR(criteriadatesle2017b73, 'DD-MM-YYYY') AS criteriadatesle2017b73_str,TO_CHAR(criteriadatesle2017b8, 'DD-MM-YYYY') AS criteriadatesle2017b8_str,TO_CHAR(criteriadatesle2017b91, 'DD-MM-YYYY') AS criteriadatesle2017b91_str,TO_CHAR(criteriadatesle2017b92, 'DD-MM-YYYY') AS criteriadatesle2017b92_str,TO_CHAR(criteriadatesle2017b10, 'DD-MM-YYYY') AS criteriadatesle2017b10_str,TO_CHAR(criteriadatesle19971, 'DD-MM-YYYY') AS criteriadatesle19971_str,TO_CHAR(criteriadatesle19972, 'DD-MM-YYYY') AS criteriadatesle19972_str,TO_CHAR(criteriadatesle19973, 'DD-MM-YYYY') AS criteriadatesle19973_str,TO_CHAR(criteriadatesle19974, 'DD-MM-YYYY') AS criteriadatesle19974_str,TO_CHAR(criteriadatesle19975, 'DD-MM-YYYY') AS criteriadatesle19975_str,TO_CHAR(criteriadatesle19976, 'DD-MM-YYYY') AS criteriadatesle19976_str,TO_CHAR(criteriadatesle19977, 'DD-MM-YYYY') AS criteriadatesle19977_str,TO_CHAR(criteriadatesle19978, 'DD-MM-YYYY') AS criteriadatesle19978_str,TO_CHAR(criteriadatesle19979, 'DD-MM-YYYY') AS criteriadatesle19979_str,TO_CHAR(criteriadatesle199710, 'DD-MM-YYYY') AS criteriadatesle199710_str,TO_CHAR(criteriadatesle199711, 'DD-MM-YYYY') AS criteriadatesle199711_str,TO_CHAR(sle1997date, 'DD-MM-YYYY') AS sle1997date_str,TO_CHAR(criteriadatesle20121, 'DD-MM-YYYY') AS criteriadatesle20121_str,TO_CHAR(criteriadatesle20122, 'DD-MM-YYYY') AS criteriadatesle20122_str,TO_CHAR(criteriadatesle20123, 'DD-MM-YYYY') AS criteriadatesle20123_str,TO_CHAR(criteriadatesle20124, 'DD-MM-YYYY') AS criteriadatesle20124_str,TO_CHAR(criteriadatesle20125, 'DD-MM-YYYY') AS criteriadatesle20125_str,TO_CHAR(criteriadatesle20126, 'DD-MM-YYYY') AS criteriadatesle20126_str,TO_CHAR(criteriadatesle20127, 'DD-MM-YYYY') AS criteriadatesle20127_str,TO_CHAR(criteriadatesle20128, 'DD-MM-YYYY') AS criteriadatesle20128_str ";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_criteriasle_id asc" ;
		$query=select('patient_criteriasle',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_axialcriteria_res($pat_id)
	{
		$tablecol[0]="newyork,imaging,clinical,radiographic ";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_axialcriteria_id asc" ;
		$query=select('patient_axialcriteria',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_axialcriteria($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(date1, 'DD-MM-YYYY') AS date1_str,TO_CHAR(date2, 'DD-MM-YYYY') AS date2_str,TO_CHAR(date3, 'DD-MM-YYYY') AS date3_str,TO_CHAR(date4, 'DD-MM-YYYY') AS date4_str,TO_CHAR(date5, 'DD-MM-YYYY') AS date5_str,TO_CHAR(date6, 'DD-MM-YYYY') AS date6_str,TO_CHAR(date7, 'DD-MM-YYYY') AS date7_str,TO_CHAR(date8, 'DD-MM-YYYY') AS date8_str,TO_CHAR(date9, 'DD-MM-YYYY') AS date9_str,TO_CHAR(date10, 'DD-MM-YYY4') AS date10_str,TO_CHAR(date11, 'DD-MM-YYYY') AS date11_str,TO_CHAR(date12, 'DD-MM-YYYY') AS date12_str,TO_CHAR(date13, 'DD-MM-YYYY') AS date13_str,TO_CHAR(date14, 'DD-MM-YYYY') AS date14_str,TO_CHAR(date15, 'DD-MM-YYYY') AS date15_str,TO_CHAR(date16, 'DD-MM-YYYY') AS date16_str,TO_CHAR(newyork_date, 'DD-MM-YYYY') AS newyork_date_str,TO_CHAR(imaging_date, 'DD-MM-YYYY') AS imaging_date_str,TO_CHAR(clinical_date, 'DD-MM-YYYY') AS clinical_date_str,TO_CHAR(radiographic_date, 'DD-MM-YYYY') AS radiographic_date_str ";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_axialcriteria_id asc" ;
		$query=select('patient_axialcriteria',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_peripheralcriteria_res($pat_id)
	{
		$tablecol[0]="set1,set2 ";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_peripheralcriteria_id asc" ;
		$query=select('patient_peripheralcriteria',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_peripheralcriteria($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(date1, 'DD-MM-YYYY') AS date1_str,TO_CHAR(date2, 'DD-MM-YYYY') AS date2_str,TO_CHAR(date3, 'DD-MM-YYYY') AS date3_str,TO_CHAR(date4, 'DD-MM-YYYY') AS date4_str,TO_CHAR(date5, 'DD-MM-YYYY') AS date5_str,TO_CHAR(date6, 'DD-MM-YYYY') AS date6_str,TO_CHAR(date7, 'DD-MM-YYYY') AS date7_str,TO_CHAR(date8, 'DD-MM-YYYY') AS date8_str,TO_CHAR(date9, 'DD-MM-YYYY') AS date9_str,TO_CHAR(date10, 'DD-MM-YYYY') AS date10_str,TO_CHAR(date11, 'DD-MM-YYYY') AS date11_str,TO_CHAR(date12, 'DD-MM-YYYY') AS date12_str,TO_CHAR(date13, 'DD-MM-YYYY') AS date13_str,TO_CHAR(date14, 'DD-MM-YYYY') AS date14_str,TO_CHAR(date15, 'DD-MM-YYYY') AS date15_str,TO_CHAR(date16, 'DD-MM-YYYY') AS date16_str,TO_CHAR(set1_date, 'DD-MM-YYYY') AS set1_date_str,TO_CHAR(set2_date, 'DD-MM-YYYY') AS set2_date_str ";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_peripheralcriteria_id asc" ;
		$query=select('patient_peripheralcriteria',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_casparcriteria_res($pat_id)
	{
		$tablecol[0]="caspar";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_casparcriteria_id asc" ;
		$query=select('patient_casparcriteria',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_casparcriteria($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(date1, 'DD-MM-YYYY') AS date1_str,TO_CHAR(date2, 'DD-MM-YYYY') AS date2_str,TO_CHAR(date3, 'DD-MM-YYYY') AS date3_str,TO_CHAR(date4, 'DD-MM-YYYY') AS date4_str,TO_CHAR(date5, 'DD-MM-YYYY') AS date5_str,TO_CHAR(caspardate, 'DD-MM-YYYY') AS caspardate_str ";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_casparcriteria_id asc" ;
		$query=select('patient_casparcriteria',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_apscriteria_res($pat_id)
	{
		$tablecol[0]="aps";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_apscriteria_id asc" ;
		$query=select('patient_apscriteria',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_apscriteria($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(date1, 'DD-MM-YYYY') AS date1_str,TO_CHAR(date2, 'DD-MM-YYYY') AS date2_str,TO_CHAR(date3, 'DD-MM-YYYY') AS date3_str,TO_CHAR(dateb1, 'DD-MM-YYYY') AS dateb1_str,TO_CHAR(dateb2, 'DD-MM-YYYY') AS dateb2_str,TO_CHAR(dateb3, 'DD-MM-YYYY') AS dateb3_str,TO_CHAR(dateb4, 'DD-MM-YYYY') AS dateb4_str,TO_CHAR(dateb3, 'DD-MM-YYYY') AS dateb3_str,TO_CHAR(dateb5, 'DD-MM-YYYY') AS dateb5_str,TO_CHAR(apsdate, 'DD-MM-YYYY') AS apsdate_str ";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by patient_apscriteria_id asc" ;
		$query=select('patient_apscriteria',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_racriteria_res($pat_id)
	{
		$tablecol[0]="rheumatoid";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by racriteria_id asc" ;
		$query=select('patient_racriteria',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_racriteria($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(date1, 'DD-MM-YYYY') AS date1_str,TO_CHAR(date2, 'DD-MM-YYYY') AS date2_str,TO_CHAR(date3, 'DD-MM-YYYY') AS date3_str,TO_CHAR(dateb1, 'DD-MM-YYYY') AS dateb1_str,TO_CHAR(dateb2, 'DD-MM-YYYY') AS dateb2_str,TO_CHAR(dateb3, 'DD-MM-YYYY') AS dateb3_str,TO_CHAR(dateb4, 'DD-MM-YYYY') AS dateb4_str,TO_CHAR(rheumatoiddate, 'DD-MM-YYYY') AS rheumatoiddate_str ";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by racriteria_id asc" ;
		$query=select('patient_racriteria',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function  get_diseasecomorbidities2($organ_id)
	{
		$tablecol[0]="*";
		$editcol[0]='diseases_organs_id';
		$editdata[0]=$organ_id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='deleted';
		$editdata[1]=0;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order="  order by code asc " ;
		$query=select('diseases_comorbidities',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_therapycomorbidities2($therapy_organ_id)
	{
		$tablecol[0]="*";
		$editcol[0]='diseases_therapy_organs_id';
		$editdata[0]=$therapy_organ_id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='deleted';
		$editdata[1]=0;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by code asc " ;
		$query=select('diseases_therapy_comorbidities',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	//function get_diseasecomorbidities($pat_id)
	function get_diseasecomorbidities()
	{
		$tablecol[0]="diseases_comorbidities.*,diseases_organs.value as diseases_organs_val";
		$editcol[0]='diseases_comorbidities.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		//$editcol[1]='diseases_comorbidities.pat_id';
		//$editdata[1]=$pat_id;
		//$editsymbol[1]='=';
		//$ears[1]=1;
		$group="";
		$order=" order by diseases_comorbidities.diseases_comorbidities_id asc " ;
		$query=select('diseases_comorbidities join diseases_organs on diseases_organs.diseases_organs_id=diseases_comorbidities.diseases_organs_id ',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_diseasecomorbiditie($diseases_comorbidities_id)
	{
		$tablecol[0]="diseases_organs_id";
		$editcol[0]='diseases_comorbidities_id';
		$editdata[0]=$diseases_comorbidities_id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="" ;
		$query=select('diseases_comorbidities',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	//function get_therapycomorbidities($pat_id)
	function get_therapycomorbidities()
	{
		$tablecol[0]="diseases_therapy_comorbidities.*,diseases_therapy_organs.value as diseases_therapy_organs_val";
		$editcol[0]='diseases_therapy_comorbidities.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		//$editcol[1]='diseases_therapy_comorbidities.pat_id';
		//$editdata[1]=$pat_id;
		//$editsymbol[1]='=';
		//$ears[1]=1;
		$group="";
		$order=" order by diseases_therapy_comorbidities.diseases_therapy_comorbidities_id asc " ;
		$query=select('diseases_therapy_comorbidities join diseases_therapy_organs on diseases_therapy_organs.diseases_therapy_organs_id=diseases_therapy_comorbidities.diseases_therapy_organs_id',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_diagnoses($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(symptom_date, 'DD-MM-YYYY') AS symptom_date_str,TO_CHAR(disease_date, 'DD-MM-YYYY') AS disease_date_str,TO_CHAR(inactive_date, 'DD-MM-YYYY') AS inactive_date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by diagnoses_id asc " ;
		$query=select('patient_diagnosis',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_disease_therapy_comorbidities($diseases_therapy_comorbidities_id)
	{
		$tablecol[0]="*";
		$editcol[0]='diseases_therapy_comorbidities_id';
		$editdata[0]=$diseases_therapy_comorbidities_id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="" ;
		$query=select('diseases_therapy_comorbidities',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_disease_comorbidities($diseases_comorbidities_id)
	{
		$tablecol[0]="*";
		$editcol[0]='diseases_comorbidities_id';
		$editdata[0]=$diseases_comorbidities_id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="" ;
		$query=select('diseases_comorbidities',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_diagnosi($diagnosis_id)
	{
		$tablecol[0]="*";
		$editcol[0]='diagnosis_id';
		$editdata[0]=$diagnosis_id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="" ;
		$query=select('diagnosis',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_diagnosis_all()
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order=" order by NULLIF(regexp_replace(code, '[^a-zA-Z]', '', 'g'), '')::text asc,NULLIF(regexp_replace(code, '\D', '', 'g'), '')::int asc " ;
		$query=select('diagnosis',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_diagnosis()
	{
		$tablecol[0]="*";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='invisible';
		$editdata[1]=0;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by NULLIF(regexp_replace(code, '[^a-zA-Z]', '', 'g'), '')::text asc,NULLIF(regexp_replace(code, '\D', '', 'g'), '')::int asc " ;
		$query=select('diagnosis',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_statistics($type,$user_centre_id,$user_all_rpts,$user_id)
	{
		$tablecol[0]="statistics.*,TO_CHAR(statistics.start_date, 'DD-MM-YYYY') AS start_date_str,TO_CHAR(statistics.end_date, 'DD-MM-YYYY') AS end_date_str,TO_CHAR(statistics.create_date, 'DD-MM-YYYY') AS create_date_str,users.surname,users.name";
		$editcol[0]='statistics.deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='statistics.type';
		$editdata[1]=$type;
		$editsymbol[1]='=';
		$ears[1]=1;
		if($user_all_rpts==0)
		{
			$editcol[2]='statistics.centre_id';
			$editdata[2]=$user_centre_id;
			$editsymbol[2]='=';
			$ears[2]=1;
		}
		else
		{
			$editcol[2]=' ( statistics.centre_id='.$user_centre_id.' or statistics.editor_id='.$user_id.' )';
			$editdata[2]='';
			$editsymbol[2]='';
			$ears[2]=0;
		}
		$group="";
		$order=" order by statistics.create_date desc " ;
		$query=select_normal('statistics left join users on statistics.editor_id=users.user_id',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_patients()
	{
		$tablecol[0]="*,TO_CHAR(dateofbirth, 'DD-MM-YYYY') AS dateofbirth_str,TO_CHAR(dateofinclusion, 'DD-MM-YYYY') AS dateofinclusion_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order=" order by dateofinclusion desc " ;
		$query=select('patient',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_patients_table($search_pat_id,$search_secondary_id,$search_id)
	{
		$tablecol[0]="pat_id,patient_id,secondary_id,id,ageatinclusion,TO_CHAR(dateofbirth, 'DD-MM-YYYY') AS dateofbirth_str,TO_CHAR(dateofinclusion, 'DD-MM-YYYY') AS dateofinclusion_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		
		$editcol[1]='centre';
		$editdata[1]=$_SESSION['user_centre_id'];
		$editsymbol[1]='=';
		$ears[1]=1;
		
		$i=1;
		if($search_pat_id<>"")
		{
			$i++;
			$editcol[$i]='patient_id::text';
			$editdata[$i]= "%".$search_pat_id."%";
			$editsymbol[$i]=' ILIKE ';
			$ears[$i]=0;
		}
		
		if($search_secondary_id<>"")
		{
			$i++;
			$editcol[$i]='secondary_id';
			$editdata[$i]= "%".$search_secondary_id."%";
			$editsymbol[$i]=' ILIKE ';
			$ears[$i]=0;
		}
		
		if($search_id<>"")
		{
			$i++;
			$editcol[$i]='id';
			$editdata[$i]= "%".$search_id."%";
			$editsymbol[$i]=' ILIKE ';
			$ears[$i]=0;
		}
		
		$group="";
		$order=" order by dateofinclusion desc " ;
		$query=select('patient',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_patient_demographics($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(followupemploymentdate, 'DD-MM-YYYY') AS followupemploymentdate_str,TO_CHAR(previousemploymentdate, 'DD-MM-YYYY') AS previousemploymentdate_str,TO_CHAR(dateofbirth, 'DD-MM-YYYY') AS dateofbirth_str,TO_CHAR(dateofinclusion, 'DD-MM-YYYY') AS dateofinclusion_str";
		$editcol[0]='pat_id';
		$editdata[0]=$pat_id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="";
		$query=select('patient',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_logging()
	{
		$tablecol[0]="TO_CHAR(logging.login_date, 'DD-MM-YYYY HH24:MI:SS') AS login_date_str,TO_CHAR(logging.logout_date, 'DD-MM-YYYY HH24:MI:SS') AS logout_date_str,users.surname,users.name,applications.title";
		$editcol='';
		$editdata='';
		$editsymbol='';
		$ears='';
		$group="";
		$order=" order by surname asc,name asc, login_date desc";
		$query=select('logging left join users on logging.user_id=users.user_id left join applications on logging.application_id=applications.application_id',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_venous($pat_id)
	{
		$tablecol[0]='*';
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by venous_id desc " ;
		$query=select('patient_venous',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_arterial($pat_id)
	{
		$tablecol[0]='*';
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by arterial_id desc " ;
		$query=select('patient_arterial',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_marital($pat_id)
	{
		$tablecol[0]='*';
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by marital_year desc " ;
		$query=select('patient_marital',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	
	function get_mediterranean($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(date, 'DD-MM-YYYY') AS date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by mediterranean_id desc " ;
		$query=select('patient_mediterranean',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_alcohol($pat_id)
	{
		$tablecol[0]='*';
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order="" ;
		$query=select('patient_alcohol',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_physical($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(date, 'DD-MM-YYYY') AS date_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by date desc " ;
		$query=select('patient_physical',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_resindence($pat_id)
	{
		$tablecol[0]="*,TO_CHAR(resindencedate_from, 'DD-MM-YYYY') AS resindencedate_from_str,TO_CHAR(resindencedate_to, 'DD-MM-YYYY') AS resindencedate_to_str";
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by resindencedate_from desc " ;
		$query=select('patient_resindence',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_autoimmune($pat_id)
	{
		$tablecol[0]='*';
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='pat_id';
		$editdata[1]=$pat_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by autoimmune_id	 desc " ;
		$query=select('patient_autoimmune',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_users()
	{
		$tablecol[0]='users.*,lookup_tbl_val.value as centre_val';
		$group="";
		$order=" order by lookup_tbl_val.value asc,users.surname asc,users.name asc " ;
		$query=select('users left join lookup_tbl_val on lookup_tbl_val.id=users.centre ',$tablecol,"","","","",$group,$order);
		return $query;
	}
	
	
	function get_lookup_table($list_id)
	{
		$tablecol[0]='*';
		$editcol[0]='id';
		$editdata[0]=$list_id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="";
		$query=select('lookup_tbl',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_lookup_table_val2($list_code)
	{
		$tablecol[0]='*';
		$editcol[0]='code';
		$editdata[0]=$list_code;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="";
		$query=select('lookup_tbl',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_lookup_table_val($list_code)
	{
		$tablecol[0]='*';
		$editcol[0]='code';
		$editdata[0]=$list_code;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="";
		$query=select('lookup_tbl_val',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_lookup_tables()
	{
		$tablecol[0]='*';
		$editcol[0]='deleted';
		$editdata[0]=0;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="order by description asc ";
		$query=select('lookup_tbl',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_explanation($id)
	{
		$tablecol[0]='*';
		$editcol[0]='lookup_tbl_val_id';
		$editdata[0]=$id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="";
		$query=select('autoantibodies_explanation',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_explanations()
	{
		$tablecol[0]='*';
		$group="";
		$order="";
		$query=select('autoantibodies_explanation',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	
	function get_lookup_tbl_value($id)
	{
		$tablecol[0]='*';
		$editcol[0]='id';
		$editdata[0]=$id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="";
		$query=select('lookup_tbl_val',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_lookup_tbl_id($list_code)
	{
		$tablecol[0]='*';
		$editcol[0]='code';
		$editdata[0]=$list_code;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='deleted';
		$editdata[1]="0";
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by id asc ";
		$query=select('lookup_tbl_val',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_lookup_tbl_values_specific_parent($list_code,$parent_id)
	{
		$tablecol[0]='*';
		$editcol[0]='code';
		$editdata[0]=$list_code;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='deleted';
		$editdata[1]="0";
		$editsymbol[1]='=';
		$ears[1]=1;
		$editcol[2]='parent_value_id';
		$editdata[2]=$parent_id;
		$editsymbol[2]='=';
		$ears[2]=1;
		$group="";
		$order=" order by value asc ";
		$query=select('lookup_tbl_val',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_lookup_tbl_values($list_code)
	{
		$tablecol[0]='*';
		$editcol[0]='code';
		$editdata[0]=$list_code;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='deleted';
		$editdata[1]="0";
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order=" order by value asc ";
		$query=select('lookup_tbl_val',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
		
	function get_lookup_value($source_category,$code)
	{
		$tablecol[0]='value';
		$editcol[0]='code';
		$editdata[0]=$code;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='id';
		$editdata[1]=$source_category;
		$editsymbol[1]='=';
		$ears[1]=1;
		$group="";
		$order="";
		$query=select('lookup_tbl_val',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_lookup_val($parent_value_id,$code)
	{
		$tablecol[0]='*';
		$editcol[0]='code';
		$editdata[0]=$code;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='deleted';
		$editdata[1]=0;
		$editsymbol[1]='=';
		$ears[1]=1;
		if($parent_value_id<>0)
		{
			$editcol[2]='parent_value_id';
			$editdata[2]=$parent_value_id;
			$editsymbol[2]='=';
			$ears[2]=1;
		}
		$group="";
		$order="order by value asc";
		$query=select('lookup_tbl_val',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_list($list_code)
	{
		$tablecol[0]='*';
		$editcol[0]='code';
		$editdata[0]=$list_code;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="";
		$query=select('lookup_tbl',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_app($user_id2,$app_id)
	{
		$tablecol[0]='*';
		$editcol[0]='user_id';
		$editdata[0]=$user_id2;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='application_id';
		$editdata[1]=$app_id;
		$editsymbol[1]='=';
		$ears[1]=1;
		$editcol[2]='deleted';
		$editdata[2]="0";
		$editsymbol[2]='=';
		$ears[2]=1;
		$group="";
		$order="";
		$query=select('apps_per_user',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function resetusername()
	{
		$email=$_REQUEST['name_rcv_mail'];
		$query="SELECT username FROM users WHERE mail = '$email' and deleted=0";
				
		$sql = pg_query($query);
		$result = pg_fetch_array($sql);
		$numrows = pg_num_rows($sql);
				
		if($numrows==0)
			return "<div class='alert alert-danger' id='msg'>Δεν υπάρχει χρήστης με αυτό το mail!</div>";
		else
		{
			$title='Εφαρμογή Διαχείρισης Κοινωνικών Δομών Δήμου Αλεξάνδρειας-Ανάκτηση ονόματος χρήστη';
			$mailmsg ='<p><h4>Αυτόματο μήνυμα από την Εφαρμογή Διαχείρισης Κοινωνικών Δομών του Δήμου Αλεξάνδρειας</h4></p>';
			$mailmsg .='<br><p>Το όνομα χρήστη που αντιστοιχεί στο mail σας είναι <b>'.$result['username'].'</b> .</p>';
			$mailmsg .='<br><p>Αν δεν κάνατε εσείς το αίτημα για ανάκτηση ονόματος χρήστη, είναι πιθανό κάποιος άλλος χρήστης να πληκτρολόγησε το δικό σας email ενώ επιχειρούσε ανάκτηση δικού του ονόματος χρήστη.</p>'; 
			$mailmsg .='<p>Σε αυτή την περίπτωση δεν χρειάζεται να κάνετε καμία άλλη ενέργεια και μπορείτε άνετα να αγνοήσετε αυτό το μήνυμα.</p>';		
			
			require 'PHPMailerAutoload.php';
			define('GUSER', 'alexandria.koinonikesdomes@gmail.com'); // GMail username
			define('GPWD', 'alexandria!2#4%.'); // GMail password
			global $error;
			require_once('class.phpmailer.php');
			$mail = new PHPMailer();  // create a new object
			//$mail->IsSMTP(); // enable SMTP
			
			$mail->SMTPDebug =1;  // debugging: 1 = errors and messages, 2 = messages only
			$mail->SMTPAuth = true;  // authentication enabled
			$mail->SMTPSecure = true;
			$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
			$mail->Host = 'smtp.gmail.com';
			$mail->Port = 465; 
			$mail->Username = GUSER;  
			$mail->Password = GPWD;           
			$mail->SetFrom('alexandria.koinonikesdomes@gmail.com');
			$mail->Subject = $title;
			$mail->IsHTML(true); // send as HTML
			$mail->Body = $mailmsg;
			$mail->AltBody = $mailmsg->Body;
			$mail->AddAddress($email);
			$mail->CharSet = 'UTF-8';
			if(!$mail->send())
				return "<div class='alert alert-danger' id='msg'>Κάποιο λάθος συνέβει!Παρακαλώ δοκιμάστε αργότερα!</div>";
			else 
				return "<div class='alert alert-success' id='msg'>Το όνομα χρήστη έχει αποσταλλεί στο e-mail σας!</div>";
		}
	}
	
	function resetpassword()
	{
		$email=$_REQUEST['psw_rcv_mail'];
		$username=$_REQUEST['psw_rcv_name'];
		$query="SELECT user_id FROM users WHERE mail = '$email' and username='$username' and deleted=0";
				
		$sql = pg_query($query);
		$result = pg_fetch_array($sql);
		$numrows = pg_num_rows($sql);
				
		if($numrows==0)
			return "<div class='alert alert-danger' id='msg'>Δεν υπάρχει χρήστης με αυτό το mail ή το όνομα χρήστη!</div>";
		else
		{
			
			$recoverkey=generateRandomString();
			$parameters[0]=$recoverkey;
			$table_names[0]='recoverkey';
			$editcol[0]='user_id';
			$editdata[0]=$result['user_id'];
			$editsymbol[0]='=';
			$update_value=update('users',$table_names,$parameters,$editcol,$editdata,$editsymbol);
			
			if($update_value=='ok')
			{
				$title='Εφαρμογή διαχείρισης κοινωνικών δομών Δήμου Αλεξάνδρειας-- Ανάκτηση κωδικού χρήστη';
				$mailmsg ='<p><h4>Αυτόματο μήνυμα από την Εφαρμογή Διαχείρισης Κοινωνικών Δομών του Δήμου Αλεξάνδρειας</h4></p>';
				$mailmsg .='<br><p>Για να ξεκινήσει η διαδικασία επαναφοράς κωδικού πρόσβασης για το λογαριασμό σας με όνομα χρήστη <b>"'.$username.'"</b> κάντε κλικ στον παρακάτω σύνδεσμο: </p>';
				$mailmsg .='<p><a href="http://'.$_SERVER['HTTP_HOST'].'/demo/newpassword.php?username='.$username.'&key='.$recoverkey.'" target="_blank">http://'.$_SERVER['HTTP_HOST'].'/demo/newpassword.php?username='.$username.'&key='.$recoverkey.'</a></p>';
				$mailmsg .='<br><p>Αν δεν κάνατε εσείς το αίτημα για επαναφορά κωδικού πρόσβασης, είναι πιθανό κάποιος άλλος χρήστης να πληκτρολόγησε το δικό σας όνομα ενώ επιχειρούσε επαναφορά του κωδικού του.</p>';
				$mailmsg .='<p>Σε αυτή την περίπτωση δεν χρειάζεται να κάνετε καμία άλλη ενέργεια και μπορείτε άνετα να αγνοήσετε αυτό το μήνυμα.</p>';
				
				
				require 'PHPMailerAutoload.php';
				define('GUSER', 'alexandria.koinonikesdomes@gmail.com'); // GMail username
				define('GPWD', 'alexandria!2#4%.'); // GMail password
				global $error;
				require_once('class.phpmailer.php');
				$mail = new PHPMailer();  // create a new object
				//$mail->IsSMTP(); // enable SMTP
				$mail->SMTPDebug =1;  // debugging: 1 = errors and messages, 2 = messages only
				$mail->SMTPAuth = true;  // authentication enabled
				$mail->SMTPSecure = true;
				$mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
				$mail->Host = 'smtp.gmail.com';
				$mail->Port = 465; 
				$mail->Username = GUSER;  
				$mail->Password = GPWD;           
				$mail->SetFrom('alexandria.koinonikesdomes@gmail.com');
				$mail->Subject = $title;
				$mail->IsHTML(true); // send as HTML
				$mail->Body = $mailmsg;
				$mail->AltBody = $mailmsg->Body;
				$mail->AddAddress($email);
				$mail->CharSet = 'UTF-8';
				if(!$mail->send())
					return "<div class='alert alert-danger' id='msg'>Κάποιο λάθος συνέβει!Παρακαλώ δοκιμάστε αργότερα!</div>";
				else 
					return "<div class='alert alert-success' id='msg'>Σας έχουν αποσταλλεί στο e-mail σας οδηγίες για την επαναφορά του Κωδικού χρήστη!</div>";
				}
				else
					return "<div class='alert alert-success' id='msg'>Κάποιο λάθος συνέβει!Παρακαλώ δοκιμάστε αργότερα!</div>";
			
		}
	}
	
	function generateRandomString($length = 15) 
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!.';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}
	
	function newpassword()
	{
	
		$key=$_REQUEST['key'];
		$username=$_REQUEST['username'];
		$query="SELECT user_id FROM users WHERE username = '$username' and recoverkey='$key' and deleted=0";
				
		$sql = pg_query($query);
		$result = pg_fetch_array($sql);
		$numrows = pg_num_rows($sql);
				
		if($numrows==0)
			return "<div class='alert alert-danger' id='msg'>Η επαναφορά δεν πραγματοποιήθηκε με επιτυχία!Προσπαθήστε ξανά!</div>";
		else
		{
			
			$parameters[0]="QERsfdHTJR!.3#p2";
			$parameters[1]=md5(str_replace("'", "''",$_REQUEST['new_psw']));
			$parameters[2]="0";
			$table_names[0]='recoverkey';
			$table_names[1]='password';
			$table_names[2]='login_attempt';
			$editcol[0]='user_id';
			$editdata[0]=$result['user_id'];
			$editsymbol[0]='=';
			$update_value=update('users',$table_names,$parameters,$editcol,$editdata,$editsymbol);
			
			if($update_value=='ok')
			{
				return "<div class='alert alert-success' id='msg'>Ο κωδικός σας έχει αλλάξει με επιτυχία!Πατήστε <a href='index.php'>εδώ</a> για να μπείτε στο σύστημα με τον νέο κωδικό σας!</div>";
			}
			else
				return "<div class='alert alert-danger' id='msg'>Κάποιο λάθος συνέβει!Παρακαλώ δοκιμάστε αργότερα!</div>";
			
		}
	}
		
	function get_username2($mail,$user_id)
	{
		$tablecol[0]='*';
		$editcol[0]='mail';
		$editdata[0]=$mail;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='deleted';
		$editdata[1]=0;
		$editsymbol[1]='=';
		$ears[1]=1;
		if($user_id<>"")
		{
			$editcol[2]='user_id';
			$editdata[2]=$user_id;
			$editsymbol[2]='<>';
			$ears[2]=1;
		}
		$group="";
		$order="";
		$query=select('users',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_user($user_id2)
	{
		$tablecol[0]='*';
		$editcol[0]='user_id';
		$editdata[0]=$user_id2;
		$editsymbol[0]='=';
		$ears[0]=1;
		$group="";
		$order="";
		$query=select('users',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
		
	
	function start_logging($user_id,$application_id)
	{
		$parameters[0]=$user_id;
		$parameters[1]=$application_id;
		$parameters[2]=0;
		$parameters[3]='now()';
		$table_names[0]='user_id';
		$table_names[1]='application_id';
		$table_names[2]='deleted';
		$table_names[3]='login_date';
								
		$id=insert('logging',$table_names,$parameters,'logging_id');
		return $id;
	}
	
	function stop_logging($logging_id)
	{
		$parameters[0]='now()';
		$table_names[0]='logout_date';
		$editcol[0]='logging_id';
		$editdata[0]=$logging_id;
		$editsymbol[0]='=';
									
		$update_value=update('logging',$table_names,$parameters,$editcol,$editdata,$editsymbol);
		return $update_value;
	}
	
	function reset_login_attempts($user_id)
	{
		$parameters[0]=0;
		$table_names[0]='login_attempt';
		$editcol[0]='user_id';
		$editdata[0]=$user_id;
		$editsymbol[0]='=';
									
		$update_value=update('users',$table_names,$parameters,$editcol,$editdata,$editsymbol);
		return $update_value;
	}
	
	function update_login_attempts($new_login_attempt,$user_id)
	{
		$table_names[0]='login_attempt';
		$parameters[0]=$new_login_attempt;
		$edit_name[0]='user_id';
		$edit_id[0]=$user_id;
		$sumbol[0]='=';
		
		$msg=update('users',$table_names,$parameters,$edit_name,$edit_id,$sumbol);
		return $msg;
	}
	
	function check_application($user_id,$application_id)
	{
		$tablecol[0]='apps_per_user.*,applications.title';
		$editcol[0]='apps_per_user.user_id';
		$editdata[0]=$user_id;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='apps_per_user.deleted';
		$editdata[1]=0;
		$editsymbol[1]='=';
		$ears[1]=1;
		$editcol[2]='apps_per_user.application_id';
		$editdata[2]=$application_id;
		$editsymbol[2]='=';
		$ears[2]=1;
		$group="";
		$order="";
		$query=select('apps_per_user left join applications on apps_per_user.application_id=applications.application_id',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function check_password($username,$password)
	{
		$tablecol[0]='users.*,lookup_tbl_val.value as centre_val';
		$editcol[0]='users.username';
		$editdata[0]=$username;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='users.deleted';
		$editdata[1]=0;
		$editsymbol[1]='=';
		$ears[1]=1;
		$editcol[2]='users.password';
		//$editdata[2]=$password;
		$editdata[2]="md5('".$password."')";
		$editsymbol[2]='=';
		$ears[2]=0;
		$group="";
		$order="";
		$query=select_psw('users left join lookup_tbl_val on lookup_tbl_val.id=users.centre ',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
	
	function get_username($username,$user_id)
	{
		$tablecol[0]='*';
		$editcol[0]='username';
		$editdata[0]=$username;
		$editsymbol[0]='=';
		$ears[0]=1;
		$editcol[1]='deleted';
		$editdata[1]=0;
		$editsymbol[1]='=';
		$ears[1]=1;
		if($user_id<>"")
		{
			$editcol[2]='user_id';
			$editdata[2]=$user_id;
			$editsymbol[2]='<>';
			$ears[2]=1;
		}
		$group="";
		$order="";
		$query=select('users',$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order);
		return $query;
	}
		
	function insert($tablename,$tablecol,$tabledata,$returnid)
	{
		
		$query ="insert INTO $tablename ";
		$query .=" ( ";
		
		$num=count($tablecol);
		
		for($i=0;$i<$num;$i++)
		{
			if ($i==$num-1)
			{
				$query .= $tablecol[$i];
				$query .= ")";
			}
			else
			{
				$query .= $tablecol[$i];
				$query .= ",";
			}
		}
		
		$query .=" values ( ";
		
		for($i=0;$i<$num;$i++)
		{
			$j=$i+1;
			if ($i==$num-1)
			{
				//$query .= "'";
				//$query .= $tabledata[$i];
				//$query .= "')";
				$query .= "$$j";
				$query .= ")";
			}
			else
			{
				//$query .= "'";
				//$query .= $tabledata[$i];
				//$query .= "',";
				$query .= "$$j,";
			}
		}
		//echo $query.'<br>';
		//$result = pg_query($query);
		$array  = array();
		for($i=0;$i<$num;$i++)
		{
			$array[$i] = $tabledata[$i];
			//echo "array:$array[$i] ";
		}
			
			//$result = pg_prepare("select_query", $query);
			//$result = pg_execute("select_query", $array);
			$result = pg_query_params($query, $array);
		
		if($result)
		{
			$query2="select ".$returnid." from ".$tablename." order by ".$returnid." desc limit 1";
			
			$order2 = pg_query($query2);
			$result2 = pg_fetch_array($order2);
			return $result2[$returnid];
		}
	}
	
	function update($tablename,$tablecol,$tabledata,$editcol,$editdata,$editsymbol)
	{
		//$user_id=$_SESSION['user_id'];
		
		$query ="update $tablename set ";
		$num=count($tablecol);
		for($i=0;$i<$num;$i++)
		{
			/*if(!($tablename=="users" or $tablename=="apps_per_user"))
			{
				$query .= $tablecol[$i];
				$query .= "='";
				$query .= $tabledata[$i];
				$query .= "', ";
			}
			else
			{*/
				$j=$i+1;
				if ($i==$num-1)
				{
					$query .= $tablecol[$i];
					//$query .= "='";
					//$query .= $tabledata[$i];
					//$query .= "' ";
					$query .= "=";
					$query .= "$$j";
				}
				else
				{
					$query .= $tablecol[$i];
					//$query .= "='";
					//$query .= $tabledata[$i];
					//$query .= "', ";
					$query .= "=";
					$query .= "$$j";
					$query .= ", ";
				}
			//}
		}
		
		/*if(!($tablename=="users" or $tablename=="apps_per_user"))
		{
			$query .= "user_id=";
			$query .= $user_id;
			
			if(!($tablename=="userstx"))
			{
				$query .= ",entry_date=now() ";
			}
		}*/
		
		$query .=" where ";
		$num2=count($editcol);
		for($i=0;$i<$num2;$i++)
		{
			if ($i==$num2-1)
			{
				$query .= $editcol[$i];
				$query .= $editsymbol[$i];
				$query .= "'";
				$query .= $editdata[$i];
				$query .= "' ";
			}
			else
			{
				$query .= $editcol[$i];
				$query .= $editsymbol[$i];
				$query .= "'";
				$query .= $editdata[$i];
				$query .= "' and ";
			}
		}	
		
		//echo $query.'<br>';
		
		//$result = pg_query($query);
		
		////$array  = array();
		for($i=0;$i<$num;$i++)
		{
			$array[$i] = $tabledata[$i];
			//echo "array:$array[$i] ";
		}
		
		//$result = pg_prepare("select_query", $query);
		//$result = pg_execute("select_query", $array);
		$result = pg_query_params($query, $array);
			
		if($result)
			return "ok";
	}
	
	function select_psw($tablename,$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order)
	{
		$query ="select ";
		
		$num=count($tablecol);
		
		for($i=0;$i<$num;$i++)
		{
			if ($i==$num-1)
			{
				$query .= $tablecol[$i];
			}
			else
			{
				$query .= $tablecol[$i];
				$query .= ",";
			}
		}
		
		$query .=" from  $tablename ";
		if($editcol!='')
		{
			$query .=" where ";
			$num2=count($editcol);
			for($i=0;$i<$num2;$i++)
			{
				$j=$i+1;
				if ($i==$num2-1)
				{
					$query .= $editcol[$i];
					$query .= $editsymbol[$i];
					$query .= $editdata[$i];
					/*if($ears[$i]==1)
						$query .= " '";
					$query .= $editdata[$i];
					if($ears[$i]==1)
						$query .= "' ";*/
				}
				else
				{
					$query .= $editcol[$i];
					$query .= $editsymbol[$i];
					$query .= "$$j";
					$query .= " and ";
					/*if($ears[$i]==1)
						$query .= " '";
					$query .= $editdata[$i];
					if($ears[$i]==1)
						$query .= "' ";*/
				}
			}	
		}
		
		if($group!='')
		{
			$query .=' '.$group.' ';
		}
		if($order!='')
		{
			$query .=' '.$order.' ';
		}
		
		//echo $query.'<br><br>';
		
		$num2=count($editcol);
		$array  = array();
		for($i=0;$i<$num2-1;$i++)
			$array[$i] = $editdata[$i];
		
		//$result = pg_prepare("select_query", $query);
		//$result = pg_execute("select_query", $array);
		$result = pg_query_params($query, $array);

		return $result;
	}
	
	function select($tablename,$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order)
	{
		$query ="select ";
		
		$num=count($tablecol);
		
		for($i=0;$i<$num;$i++)
		{
			if ($i==$num-1)
			{
				$query .= $tablecol[$i];
			}
			else
			{
				$query .= $tablecol[$i];
				$query .= ",";
			}
		}
		
		$query .=" from  $tablename ";
		if($editcol!='')
		{
			$query .=" where ";
			$num2=count($editcol);
			for($i=0;$i<$num2;$i++)
			{
				$j=$i+1;
				if ($i==$num2-1)
				{
					$query .= $editcol[$i];
					$query .= $editsymbol[$i];
					$query .= "$$j";
					/*if($ears[$i]==1)
						$query .= " '";
					$query .= $editdata[$i];
					if($ears[$i]==1)
						$query .= "' ";*/
					
				}
				else
				{
					$query .= $editcol[$i];
					$query .= $editsymbol[$i];
					$query .= "$$j";
					$query .= " and ";
					/*if($ears[$i]==1)
						$query .= " '";
					$query .= $editdata[$i];
					if($ears[$i]==1)
						$query .= "' ";*/
				}
			}	
		}
		
		if($group!='')
		{
			$query .=' '.$group.' ';
		}
		if($order!='')
		{
			$query .=' '.$order.' ';
		}
		
		//echo $query.'<br><br>';
		
		if ($editcol!='')
		{
			$num2=count($editcol);
			$array  = array();
			for($i=0;$i<$num2;$i++)
			//{
				$array[$i] = $editdata[$i];
			
				//echo "$array:$array[$i]<br>";
			//}
			
			//$result = pg_prepare("select_query", $query);
			//$result = pg_execute("select_query", $array);
			$result = pg_query_params($query, $array);
		}
		else
			$result = pg_query($query);
		return $result;
	}
	
	function select_normal($tablename,$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order)
	{
		$query ="select ";
		
		$num=count($tablecol);
		
		for($i=0;$i<$num;$i++)
		{
			if ($i==$num-1)
			{
				$query .= $tablecol[$i];
			}
			else
			{
				$query .= $tablecol[$i];
				$query .= ",";
			}
		}
		
		$query .=" from  $tablename ";
		if($editcol!='')
		{
			$query .=" where ";
			$num2=count($editcol);
			for($i=0;$i<$num2;$i++)
			{
				$j=$i+1;
				if ($i==$num2-1)
				{
					$query .= $editcol[$i];
					$query .= $editsymbol[$i];
					if($ears[$i]==1)
						$query .= " '";
					$query .= $editdata[$i];
					if($ears[$i]==1)
						$query .= "' ";
					
				}
				else
				{
					$query .= $editcol[$i];
					$query .= $editsymbol[$i];
					if($ears[$i]==1)
						$query .= " '";
					$query .= $editdata[$i];
					if($ears[$i]==1)
						$query .= "' ";
					$query .= " and ";
				}
			}	
		}
		
		if($group!='')
		{
			$query .=' '.$group.' ';
		}
		if($order!='')
		{
			$query .=' '.$order.' ';
		}
		
		//echo $query.'<br><br>';
		
		
		$result = pg_query($query);
		return $result;
	}
	
	function select_multiple_rows($tablename,$tablecol,$editcol,$editdata,$editsymbol,$ears,$group,$order) 
	{
		$query ="select ";
		
		$num=count($tablecol);
		
		for($i=0;$i<$num;$i++)
		{
			if ($i==$num-1)
			{
				$query .= $tablecol[$i];
			}
			else
			{
				$query .= $tablecol[$i];
				$query .= ",";
			}
		}
		
		$query .=" from  $tablename where ";
		
		$num2=count($editcol);
		for($i=0;$i<$num2;$i++)
			{
				$j=$i+1;
				if ($i==$num2-1)
				{
					$query .= $editcol[$i];
					$query .= $editsymbol[$i];
					$query .= "$$j";
					/*if($ears[$i]==1)
						$query .= " '";
					$query .= $editdata[$i];
					if($ears[$i]==1)
						$query .= "' ";*/
				}
				else
				{
					$query .= $editcol[$i];
					$query .= $editsymbol[$i];
					$query .= "$$j";
					$query .= " and ";
					/*if($ears[$i]==1)
						$query .= " '";
					$query .= $editdata[$i];
					if($ears[$i]==1)
						$query .= "' ";*/
				}
			}	
		
		if($group!='')
		{
			$query .=' '.$group.' ';
		}
		if($order!='')
		{
			$query .=' '.$order.' ';
		}
		//echo $query.'<br>';
		if ($editcol!='')
		{
			$num2=count($editcol);
			$array  = array();
			for($i=0;$i<$num2;$i++)
				$array[$i] = $editdata[$i];
			//{
			//echo "$array:$array[$i]<br>";
			//}
			
			//$result = pg_prepare("select_query", $query);
			//$result = pg_execute("select_query", $array);
			$result = pg_query_params($query, $array);
		}
		else
			$result = pg_query($query);
		
		//$result = pg_query($query);
		return $result;
	}

	function curPageURL() 
	{
		 $pageURL = 'http';
		 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
		 $pageURL .= "://";
		 if ($_SERVER["SERVER_PORT"] != "80") {
		  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
		 } else {
		  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
		 }
		 return $pageURL;
	}
	
	function curPageName() 
	{
		return substr($_SERVER["SCRIPT_NAME"],strrpos($_SERVER["SCRIPT_NAME"],"/"));
	}

	function date_for_postgres($date)
	{
		if ($date!="")
		{
			$startdate= explode("-", $date);
			if ($startdate[1]<10 and substr($startdate[1],0,1)!='0')
				$startdate[1]='0'.$startdate[1];
			if ($startdate[0]<10 and substr($startdate[0],0,1)!='0')
				$startdate[0]='0'.$startdate[0];
			$date=$startdate[2].'-'.$startdate[1].'-'.$startdate[0];
		}
		else
			$date='1900-12-12';
			
		return $date;
	}
	?>
