update public.db_version set version='2.40beta 2021/05/26',edit_date=now();
INSERT INTO lookup_tbl (code,description,deleted,parent_code) VALUES ('protocol_description','Biobanking Protocol Description',0,'');
INSERT INTO lookup_tbl_val (code,value,deleted,parent_value_id) VALUES ('protocol_description','SLE biobanking',0,0);
INSERT INTO lookup_tbl_val (code,value,deleted,parent_value_id) VALUES ('protocol_description','UCTD biobanking',0,0);
INSERT INTO lookup_tbl_val (code,value,deleted,parent_value_id) VALUES ('protocol_description','SLE response to treatment',0,0);
INSERT INTO lookup_tbl_val (code,value,deleted,parent_value_id) VALUES ('protocol_description','SLE IL-33',0,0);
INSERT INTO lookup_tbl_val (code,value,deleted,parent_value_id) VALUES ('protocol_description','SLE metabolism',0,0);
INSERT INTO lookup_tbl_val (code,value,deleted,parent_value_id) VALUES ('protocol_description','SLE gender bias',0,0);
INSERT INTO lookup_tbl_val (code,value,deleted,parent_value_id) VALUES ('protocol_description','AxSpA start biologic',0,0);
INSERT INTO lookup_tbl_val (code,value,deleted,parent_value_id) VALUES ('protocol_description','Arthritis biobanking',0,0);
INSERT INTO lookup_tbl_val (code,value,deleted,parent_value_id) VALUES ('protocol_description','Peripheral SpA start biologic',0,0);
INSERT INTO lookup_tbl_val (code,value,deleted,parent_value_id) VALUES ('protocol_description','RA biologic sequential',0,0);
INSERT INTO lookup_tbl_val (code,value,deleted,parent_value_id) VALUES ('protocol_description','RA start biologic',0,0);
INSERT INTO lookup_tbl_val (code,value,deleted,parent_value_id) VALUES ('protocol_description','RA SYSCID',0,0);
UPDATE patient_biobanking set protocol_descr = (select id from lookup_tbl_val where value='SLE biobanking') where protocol_descr=1;
UPDATE patient_biobanking set protocol_descr=(select id from lookup_tbl_val where value='UCTD biobanking') where protocol_descr=2;
UPDATE patient_biobanking set protocol_descr=(select id from lookup_tbl_val where value='SLE response to treatment') where protocol_descr=3;
UPDATE patient_biobanking set protocol_descr=(select id from lookup_tbl_val where value='SLE IL-33') where protocol_descr=4;
UPDATE patient_biobanking set protocol_descr=(select id from lookup_tbl_val where value='SLE metabolism') where protocol_descr=5;
UPDATE patient_biobanking set protocol_descr=(select id from lookup_tbl_val where value='SLE gender bias') where protocol_descr=6;
UPDATE patient_biobanking set protocol_descr=(select id from lookup_tbl_val where value='AxSpA start biologic') where protocol_descr=7;
UPDATE patient_biobanking set protocol_descr=(select id from lookup_tbl_val where value='Arthritis biobanking') where protocol_descr=8;
UPDATE patient_biobanking set protocol_descr=(select id from lookup_tbl_val where value='Peripheral SpA start biologic') where protocol_descr=9;
UPDATE patient_biobanking set protocol_descr=(select id from lookup_tbl_val where value='RA biologic sequential') where protocol_descr=10;
UPDATE patient_biobanking set protocol_descr=(select id from lookup_tbl_val where value='RA start biologic') where protocol_descr=11;
UPDATE patient_biobanking set protocol_descr=(select id from lookup_tbl_val where value='RA SYSCID') where protocol_descr=12;

ALTER TABLE patient_biobanking add column patient_cohort_id integer default 0;
ALTER TABLE patient_biobanking add column patient_followup_id integer default 0;
ALTER TABLE patient_biobanking add column pbmcs integer default 0;
ALTER TABLE patient_biobanking add column pmns integer default 0;

exoun perastei mexri edo
