-- FUNCTION: public.create_insert_query_patient_second_partb_val()

-- DROP FUNCTION public.create_insert_query_patient_second_partb_val();

CREATE OR REPLACE FUNCTION public.create_insert_query_patient_second_partb_val(
	)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100000
    VOLATILE 
AS $BODY$DECLARE

 DECLARE

  patient_record RECORD;
  drugs_id3 RECORD;
  i int=1;
  cnt int=0;
  krow2 int = 0;
  qu text; 
  create_qu text;
  mviews RECORD;  

 BEGIN 

	execute 'DROP TABLE IF EXISTS statistics_lk2b_val_patient;';
	create_qu = 'CREATE TABLE statistics_lk2b_val_patient (id integer,drug_id integer,current int,month integer,label varchar(100),value varchar(20));';
	execute create_qu;
	
  --FOR patient_record IN select patient_cohort.patient_cohort_id,patient_cohort.pat_id from patient_cohort
  FOR patient_record IN select pat_id from patient where deleted=0 and dateofinclusion is not null limit 1
    LOOP
	
		
  FOR drugs_id5 IN 0..28 LOOP 
		--IF drugs_id5<=12 THEN
			cnt=drugs_id5*3;
		--ELSE
			--cnt=(drugs_id5-6)*6;
		--END IF;
				
		
		
	  --FOR drugs_id3 IN SELECT drugs_id FROM drugs WHERE deleted=0 and prim=0 and previous=0  order by drugs_id asc
		FOR drugs_id3 IN SELECT drugs.drugs_id,REPLACE (coalesce(drugs.code,''), ',', ' ') as code,
		 REPLACE (coalesce(drugs.substance,''), ',', ' ')  as substance,REPLACE (coalesce(lookup_tbl_val.value,''), ',', ' ')  as route_of_administration_val
		 FROM drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration
		 WHERE drugs.deleted=0 and drugs.prim=0 and drugs.previous=0 
		and drugs.code not in ('A7','A8','A10','H1','H2','H3','J')  order by drugs.drugs_id asc
			loop
			
				krow2=krow2 + 1;
				qu = 'INSERT INTO statistics_lk2b_val_patient VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,' || CONCAT(cnt,',') /* || patient_record.patient_cohort_id || ',' */ || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-' || CONCAT(cnt,'-') || '3month') || ',';
				qu = qu || quote_literal('');
				
				qu = qu || ');' ;
				execute qu;
				
		END LOOP;
	END LOOP;
END LOOP;
 
RETURN  1;

END;   $BODY$;

ALTER FUNCTION public.create_insert_query_patient_second_partb_val()
    OWNER TO postgres;
