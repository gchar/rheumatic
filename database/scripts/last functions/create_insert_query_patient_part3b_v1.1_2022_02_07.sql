-- FUNCTION: public.create_insert_query_patient_third_partb()

-- DROP FUNCTION public.create_insert_query_patient_third_partb();

CREATE OR REPLACE FUNCTION public.create_insert_query_patient_third_partb(
	)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100000
    VOLATILE 
AS $BODY$DECLARE
  krow int;
  patient_record RECORD;
  dflt character='';
  drugs_id1 RECORD;
  drugs_id2 RECORD;
  drugs_id3 RECORD;
  drugs_id4 RECORD;
  drugs_id5 int;
  drugs_id6 int;
  count_rows RECORD;
  i int=1;
  krow1 int = 1;
  krow2 int = 0;
  cnt int = 0;
  qu text; 
  create_qu text;
  create_first_qu text;
 BEGIN 
 execute 'DROP TABLE IF EXISTS statistics_lk3b_patient;';
create_qu = 'CREATE TABLE statistics_lk3b_patient (id integer,pat_id integer,';

        /*FOR drugs_id4 IN 1..27*34 LOOP 
			create_qu = create_qu || 'a' || drugs_id4 ;
			if  drugs_id4=27*34 then
				create_qu = create_qu || ' VARCHAR(250)' ;
			else
				create_qu = create_qu || ' VARCHAR(250),' ;
			end if;
		end loop;*/
		
		--SLE SLEDAI 26*27=702
		FOR drugs_id5 IN 0..28 LOOP 
			--IF drugs_id5<=12 THEN
				cnt=drugs_id5*3;
			--ELSE
			--	cnt=(drugs_id5-6)*6;
			--END IF;
			
			-- allagi 20/08 start
			create_qu = create_qu || CONCAT('spga_', cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			-- allagi 20/08 end
			
			
			FOR drugs_id6 IN 1..24 LOOP 
				create_qu = create_qu || CONCAT('s', drugs_id6) || CONCAT('_', cnt) ;
				create_qu = create_qu || ' VARCHAR(250),' ;
			end loop;
			
			-- allagi 20/08 start
			--create_qu = create_qu ||  CONCAT('sl2k', drugs_id6) || CONCAT('_', cnt) ;
			create_qu = create_qu || CONCAT('sl2k_', cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			-- allagi 20/08 end
			
			--SLE SELENA-CNS prostethikan kai ta epimerous tou SELENA-SLEDAI Flare Index 
			FOR drugs_id6 IN 1..13 LOOP 
				create_qu = create_qu || CONCAT('sel', drugs_id6) || CONCAT('_', cnt) ;
				create_qu = create_qu || ' VARCHAR(250),' ;
			end loop;
			create_qu = create_qu || CONCAT('sel_', cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('cns_', cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('prot_', cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			
			--SEVERITY SLICC/ACR - SLE severity index - CND/Nephritis outcome 6*27=162
			create_qu = create_qu || CONCAT('SLdmg_', cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('slesevind_', cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('Neuro_', cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('ChrKid_', cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('Endstagekid_', cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('KidTransp_', cnt) ;
			
			if  drugs_id5 = 28 then
				create_qu = create_qu || ' VARCHAR(250)' ;
			else
				create_qu = create_qu || ' VARCHAR(250),' ;
			end if;
		end loop;	
		
		
		
create_qu = create_qu || ')';



execute create_qu;

create_first_qu = 'INSERT INTO statistics_lk3b_patient VALUES (1,0,';

--SLE SLEDAI 26*27=702

FOR drugs_id5 IN 0..28 LOOP 
	
	--IF drugs_id5<=12 THEN
		cnt=drugs_id5*3;
	--ELSE
	--	cnt=(drugs_id5-6)*6;
	--END IF;
	
	-- allagi 20/08 start
	create_first_qu = create_first_qu || quote_literal('SPGA-' || cnt) || ',';
	-- allagi 20/08 end
	create_first_qu = create_first_qu || quote_literal('SL1-' || cnt) || ',' || quote_literal('SL2-' || cnt) || ',' || quote_literal('SL3-' || cnt) || ',' || quote_literal('SL4-' || cnt) || ',' || quote_literal('SL5-' || cnt) || ',' || quote_literal('SL6-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('SL7-' || cnt) || ',' || quote_literal('SL8-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('SL9-' || cnt) || ',' || quote_literal('SL10-' || cnt) || ',' || quote_literal('SL11-' || cnt) || ',' || quote_literal('SL12-' || cnt) || ',' || quote_literal('SL13-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('SL14-' || cnt) || ',' || quote_literal('SL15-' || cnt) || ',' || quote_literal('SL16-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('SL17-' || cnt) || ',' || quote_literal('SL18-' || cnt) || ',' || quote_literal('SL19-' || cnt) || ',' || quote_literal('SL20-' || cnt) || ',' || quote_literal('SL21-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('SL22-' || cnt) || ',' || quote_literal('SL23-' || cnt) || ',' || quote_literal('SL24-' || cnt) || ',' || quote_literal('SL2K-' || cnt) || ',';
	
	--SLE SELENA-CNS 3*27=81 kai ta epimerous tou SELENA-SLEDAI Flare Index 
			FOR drugs_id6 IN 1..13 LOOP 
				create_first_qu = create_first_qu || quote_literal('S'|| drugs_id6 || '-' || cnt) || ',';
			end loop;
	create_first_qu = create_first_qu || quote_literal('S-' || cnt) || ',' || quote_literal('CNS-' || cnt) || ',' || quote_literal('Prote-' || cnt) || ',';
	
	--SEVERITY SLICC/ACR - SLE severity index - CND/Nephritis outcome 6*27=162
	if  drugs_id5 = 28 then
		create_first_qu = create_first_qu || quote_literal('SLdmg-' || cnt) || ',' || quote_literal('slesev-' || cnt) || ',' || quote_literal('Neur-' || cnt) || ',' || quote_literal('ChrKidD-' || cnt) || ',' || quote_literal('Endstagek-' || cnt) || ',' || quote_literal('KidTran-' || cnt);
	else
		create_first_qu = create_first_qu || quote_literal('SLdmg-' || cnt) || ',' || quote_literal('slesev-' || cnt) || ',' || quote_literal('Neur-' || cnt) || ',' || quote_literal('ChrKidD-' || cnt) || ',' || quote_literal('Endstagek-' || cnt) || ',' || quote_literal('KidTran-' || cnt) || ',';
	end if;
end loop;	

create_first_qu = create_first_qu || ');' ;
execute create_first_qu;


	FOR count_rows IN select count(pat_id) as count_rows,pat_id from patient where patient.deleted=0 and patient.dateofinclusion is not null group by pat_id order by pat_id desc
 	
 	LOOP
	end loop;
	
 	FOR patient_record IN select pat_id from patient where deleted=0 and dateofinclusion is not null order by pat_id desc
	 --for testing
	limit 50
 	LOOP
	
	krow1 = krow1 + 1;
	
	qu = 'INSERT INTO statistics_lk3b_patient VALUES ( ' || krow1 ||  ',' || patient_record.pat_id || ',';	
	
	
	
		FOR drugs_id5 IN 0..28 LOOP 
	
			--IF drugs_id5<=12 THEN
				cnt=drugs_id5*3;
			--ELSE
			--	cnt=(drugs_id5-6)*6;
			--END IF;
			
			FOR drugs_id4 IN select count(*) as cnt from patient_followup
			where deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			--and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthinclusion=cnt
			LOOP
			end loop;
			
			IF drugs_id4.cnt>0 then
				FOR drugs_id1 IN select coalesce(patient_sle.pga,'') as pga,coalesce(patient_sle.sledai1,0) as sledai1,coalesce(patient_sle.sledai2,0) as sledai2,coalesce(patient_sle.sledai3,0) as sledai3,coalesce(patient_sle.sledai4,0) as sledai4,
				coalesce(patient_sle.sledai5,0) as sledai5,coalesce(patient_sle.sledai6,0) as sledai6,coalesce(patient_sle.sledai7,0) as sledai7,coalesce(patient_sle.sledai8,0) as sledai8,coalesce(patient_sle.sledai9,0) as sledai9,
				coalesce(patient_sle.sledai10,0) as sledai10,coalesce(patient_sle.sledai11,0) as sledai11,coalesce(patient_sle.sledai12,0) as sledai12,coalesce(patient_sle.sledai13,0) as sledai13,coalesce(patient_sle.sledai14,0) as sledai14,
				coalesce(patient_sle.sledai15,0) as sledai15,coalesce(patient_sle.sledai16,0) as sledai16,coalesce(patient_sle.sledai17,0) as sledai17,coalesce(patient_sle.sledai18,0) as sledai18,coalesce(patient_sle.sledai19,0) as sledai19,
				coalesce(patient_sle.sledai20,0) as sledai20,coalesce(patient_sle.sledai21,0) as sledai21,coalesce(patient_sle.sledai22,0) as sledai22,coalesce(patient_sle.sledai23,0) as sledai23,coalesce(patient_sle.sledai24,0) as sledai24,
				coalesce(patient_sle.sledai2k,0) as sledai2k
				,coalesce(patient_sle.flare1,0) as selena1,coalesce(patient_sle.flare2,0) as selena2,coalesce(patient_sle.flare3,0) as selena3,coalesce(patient_sle.flare4,0) as selena4
				,coalesce(patient_sle.flare5,0) as selena5,coalesce(patient_sle.flare6,0) as selena6,coalesce(patient_sle.flare7,0) as selena7,coalesce(patient_sle.flare8,0) as selena8
				,coalesce(patient_sle.flare9,0) as selena9,coalesce(patient_sle.flare10,0) as selena10,coalesce(patient_sle.flare11,0) as selena11,coalesce(patient_sle.flare12,0) as selena12
				,coalesce(patient_sle.flare13,0) as selena13
				,coalesce(patient_sle.flare14,0) as selena,coalesce(patient_sle.nefritis1,0) as cns,coalesce(patient_sle.nefritis2,'') as protein,
				coalesce(patient_sle_severity.slicc42,'') as SLICCdmg,coalesce(patient_sle_severity.index13,'') as slesevind,coalesce(patient_sle_severity.cns1,0) as cns1,coalesce(patient_sle_severity.cns2,0) as cns2,
				coalesce(patient_sle_severity.cns3,0) as cns3,coalesce(patient_sle_severity.cns4,0) as cns4,patient_followup.patient_followup_id as patient_followup_id
				from patient_followup
				left join patient_sle on patient_sle.patient_followup_id=patient_followup.patient_followup_id 
				left join patient_sle_severity on patient_sle_severity.patient_followup_id=patient_followup.patient_followup_id 
				where patient_followup.deleted=0
				and patient_followup.pat_id=patient_record.pat_id
				--and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
				and patient_followup.fumonthinclusion=cnt  order by patient_followup.patient_followup_id desc  limit 1
				LOOP
				
					FOR drugs_id4 IN select count(*) as cnt from patient_sle
					where deleted=0 and patient_followup_id=drugs_id1.patient_followup_id
					LOOP
					end loop;
					
					IF drugs_id4.cnt>0 then
						-- allagi 20/08 start
						qu = qu || quote_literal(drugs_id1.pga) || ',';
						-- allagi 20/08 end
						qu = qu || quote_literal(drugs_id1.sledai1) || ',' || quote_literal(drugs_id1.sledai2) || ',' || quote_literal(drugs_id1.sledai3) || ',' || quote_literal(drugs_id1.sledai4) || ',' || quote_literal(drugs_id1.sledai5) || ',';
						qu = qu || quote_literal(drugs_id1.sledai6) || ',' || quote_literal(drugs_id1.sledai7) || ',' || quote_literal(drugs_id1.sledai8) || ',' || quote_literal(drugs_id1.sledai9) || ',' || quote_literal(drugs_id1.sledai10) || ',';
						qu = qu || quote_literal(drugs_id1.sledai11) || ',' || quote_literal(drugs_id1.sledai12) || ',' || quote_literal(drugs_id1.sledai13) || ',' || quote_literal(drugs_id1.sledai14) || ',' || quote_literal(drugs_id1.sledai15) || ',';
						qu = qu || quote_literal(drugs_id1.sledai16) || ',' || quote_literal(drugs_id1.sledai17) || ',' || quote_literal(drugs_id1.sledai18) || ',' || quote_literal(drugs_id1.sledai19) || ',' || quote_literal(drugs_id1.sledai20) || ',';
						qu = qu || quote_literal(drugs_id1.sledai21) || ',' || quote_literal(drugs_id1.sledai22) || ',' || quote_literal(drugs_id1.sledai23) || ',' || quote_literal(drugs_id1.sledai24) || ',' || quote_literal(drugs_id1.sledai2k) || ',';
						--kai ta epimerous tou SELENA-SLEDAI Flare Index 
						qu = qu || quote_literal(drugs_id1.selena1) || ',' || quote_literal(drugs_id1.selena2) || ',' || quote_literal(drugs_id1.selena3) || ',' || quote_literal(drugs_id1.selena4) || ',';
						qu = qu || quote_literal(drugs_id1.selena5) || ',' || quote_literal(drugs_id1.selena6) || ',' || quote_literal(drugs_id1.selena7) || ',' || quote_literal(drugs_id1.selena8) || ',';
						qu = qu || quote_literal(drugs_id1.selena9) || ',' || quote_literal(drugs_id1.selena10) || ',' || quote_literal(drugs_id1.selena11) || ',' || quote_literal(drugs_id1.selena12) || ',';
						qu = qu || quote_literal(drugs_id1.selena13) || ',';
						qu = qu || quote_literal(drugs_id1.selena) || ',' || quote_literal(drugs_id1.cns) || ',' || quote_literal(drugs_id1.protein) || ',';
					else
						-- allagi 20/08 start
						qu = qu || quote_literal(dflt) || ',';
						-- allagi 20/08 end
						qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
						qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
						qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
						--kai ta epimerous tou SELENA-SLEDAI Flare Index 
						qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
						qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
						qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
					end if;
					
					FOR drugs_id4 IN select count(*) as cnt from patient_sle_severity
					where deleted=0 and patient_followup_id=drugs_id1.patient_followup_id
					LOOP
					end loop;
					
					IF drugs_id4.cnt>0 then
						qu = qu || quote_literal(drugs_id1.SLICCdmg) || ',' || quote_literal(drugs_id1.slesevind) || ',' || quote_literal(drugs_id1.cns1) || ',' || quote_literal(drugs_id1.cns2) || ',' || quote_literal(drugs_id1.cns3) || ',';
					
						if cnt=84 then
							qu = qu || quote_literal(drugs_id1.cns4);
						else
							qu = qu || quote_literal(drugs_id1.cns4) || ',';
						end if;
					else
						if cnt=84 then
							qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt);
						else
							qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
						end if;
					end if;
					
					
				end loop;
			else
					-- allagi 20/08 start
					qu = qu || quote_literal(dflt) || ',';
					-- allagi 20/08 end
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
					--kai ta epimerous tou SELENA-SLEDAI Flare Index 
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt);
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) ',' || quote_literal(dflt);
					
					if cnt=84 then
						qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt);
					else
						qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
					end if;
			end if;
			
		end loop;

		
	qu = qu || ')';
	execute qu;
	end loop;
RETURN  1;
END; $BODY$;

ALTER FUNCTION public.create_insert_query_patient_third_partb()
    OWNER TO postgres;
