-- FUNCTION: public.create_insert_query_event()

-- DROP FUNCTION public.create_insert_query_event();

CREATE OR REPLACE FUNCTION public.create_insert_query_event(
	)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100000
    VOLATILE 
AS $BODY$DECLARE
krow int=0;
krow2 int=1;
count_rows RECORD;
drugs_id1 RECORD;
drugs_id3 RECORD;
drugs_id4 RECORD;
patient_record RECORD;
drugs_id5 int;
qu text; 
create_qu text;
create_first_qu text;
dflt character='';
mviews RECORD;  
ra_record RECORD;
spa_record RECORD;
pulmonary_record RECORD;
sda_asas_record RECORD;
axspa_cnt RECORD;
axspa RECORD;
ra_cnt RECORD;
ra1 RECORD;
ra2 RECORD;
ra3 RECORD;
spa_cnt RECORD;
spa1 RECORD;
spa2 RECORD;
spa3 RECORD;
dis_act RECORD;
ethnicity text;
yearsofeduc text;
residence text;
employment text;
marital_status text;
physical_status text;
mediterranean_diet text;
alcohol_consumption text;



BEGIN 
execute 'DROP TABLE if exists statistics_lk4;';

create_qu = 'CREATE TABLE statistics_lk4 (id integer,';	
/*create_qu = create_qu || 'a1 VARCHAR,a2 VARCHAR,a3 VARCHAR,a4 VARCHAR,a5 VARCHAR,a6 VARCHAR,a7 VARCHAR,a8 date,a9 date,a10 VARCHAR,';
 create_qu = create_qu || 'a11 VARCHAR,a12 VARCHAR,a13 VARCHAR,a14 VARCHAR,a15 VARCHAR,a16 VARCHAR,a17 VARCHAR,a18 VARCHAR,';
 create_qu = create_qu || 'a19 VARCHAR,a20 VARCHAR,a21 VARCHAR,a22 VARCHAR,a23 VARCHAR,';
 create_qu = create_qu || 'a24 VARCHAR,a25 VARCHAR,a26 VARCHAR,a27 VARCHAR,a28 VARCHAR,a29 VARCHAR,a30 VARCHAR,';
 create_qu = create_qu || 'a31 date,a32 VARCHAR,a33 VARCHAR,a34 VARCHAR,a35 VARCHAR,a36 VARCHAR,a37 VARCHAR,a38 VARCHAR,a39 VARCHAR,a40 VARCHAR,a41 VARCHAR,';
 
 create_qu = create_qu || 'a39 VARCHAR,a40 VARCHAR,a41 VARCHAR,a42 VARCHAR,a43 VARCHAR,a44 VARCHAR,a45 VARCHAR,';
 create_qu = create_qu || 'a46 VARCHAR,a47 VARCHAR,a48 VARCHAR,a49 VARCHAR,a50 VARCHAR,a51 VARCHAR,';
 create_qu = create_qu || 'a52 VARCHAR,a53 VARCHAR,a54 VARCHAR,a55 VARCHAR,a56 VARCHAR,a57 VARCHAR,a58 VARCHAR,a59 VARCHAR,';
 create_qu = create_qu || 'a60 VARCHAR,a61 VARCHAR,a62 VARCHAR,a63 VARCHAR,a64 VARCHAR,a65 VARCHAR,';
 create_qu = create_qu || 'a66 VARCHAR,a67 VARCHAR,a68 VARCHAR,a69 VARCHAR,a70 VARCHAR';*/
 
 FOR select_tbl_cnt IN 1..86 LOOP 
	create_qu = create_qu || 'a' || select_tbl_cnt || ' varchar,';
end loop;



FOR count_rows IN SELECT count(drugs_id) as count_rows FROM drugs WHERE deleted=0 and prim=0 and previous=0
LOOP
end loop;

	FOR drugs_id5 IN 87..count_rows.count_rows+89 LOOP 
		krow = krow + 1;
		create_qu = create_qu || 'a' || drugs_id5 ;

		--if count_rows.count_rows+3=krow then
		--	create_qu = create_qu || ' VARCHAR' ;
		--else
			create_qu = create_qu || ' VARCHAR,' ;
		--end if;
	end loop;

FOR select_tbl_cnt IN 1..69 LOOP 
	create_qu = create_qu || 'comor' || select_tbl_cnt || ' varchar,';
	if select_tbl_cnt=69 then
		create_qu = create_qu || 'comor_date' || select_tbl_cnt || ' varchar';
	else
		create_qu = create_qu || 'comor_date' || select_tbl_cnt || ' varchar,';
	end if;
end loop;

create_qu = create_qu || ');' ;
execute create_qu;

-- vasika stoixeia 14 pedia
create_first_qu = 'INSERT INTO statistics_lk4 VALUES ( 1,' || quote_literal('cohort_id') || ',' || quote_literal('pat_id') || ',' || quote_literal('drug_id') || ',';
create_first_qu = create_first_qu || quote_literal('substance') || ',' || quote_literal('centre_id') || ',' || quote_literal('centre_name') || ',';
create_first_qu = create_first_qu || quote_literal('treatment_nbr') || ',' || quote_literal('01-01-2019') || ',' ;
create_first_qu = create_first_qu || quote_literal('01-01-2019') || ',' || quote_literal('stop_reason') || ',' || quote_literal('stop_cause_comments') || ',';
create_first_qu = create_first_qu || quote_literal('gender') || ',' || quote_literal('date_of_inclusion') || ',' || quote_literal('date_of_birth') ;
--diagnoseis 27 pedia
create_first_qu = create_first_qu || ',' || quote_literal('main_diagnosis_date') || ',' || quote_literal('icd10a') || ',' || quote_literal('main_diagnosis_value') ;
create_first_qu = create_first_qu || ',' || quote_literal('main_symptom_date') || ',' || quote_literal('first_secondary_diagnosis_date') || ',';
create_first_qu = create_first_qu || quote_literal('first_secondary_icd10b') || ',' || quote_literal('first_secondary_diagnosis_value') || ',' ;
create_first_qu = create_first_qu || quote_literal('first_secondary_symptom_date') || ',' || quote_literal('second_secondary_diagnosis_date') || ',';
create_first_qu = create_first_qu || quote_literal('second_secondary_icd10b') || ',' || quote_literal('second_secondary_diagnosis_value') || ',';
create_first_qu = create_first_qu || quote_literal('second_secondary_symptom_date') || ',' || quote_literal('third_secondary_diagnosis_date') || ',';
create_first_qu = create_first_qu || quote_literal('third_secondary_icd10b') || ',' || quote_literal('third_secondary_diagnosis_value') || ',' ;
create_first_qu = create_first_qu || quote_literal('third_secondary_symptom_date') || ',' || quote_literal('01-01-2019') || ',';
create_first_qu = create_first_qu || quote_literal('health_care_utilazation') || ',' || quote_literal('main_organ/system') || ',';
create_first_qu = create_first_qu || quote_literal('secondary_organ/system') || ',' || quote_literal('relationship_to_main_therapy') || ',';
create_first_qu = create_first_qu || quote_literal('outcome') || ',' || quote_literal('seriosness') || ',';
create_first_qu = create_first_qu || quote_literal('change_of_therapy') || ',' || quote_literal('fumonthcohort') || ',' || quote_literal('patient_event_id') || ',' || quote_literal('event_description') || ',';

-- Demographics 8 fields
create_first_qu = create_first_qu || quote_literal('Ethnicity') || ',' || quote_literal('Year_of_education') || ',' || quote_literal('Residence') || ',' || quote_literal('Employment') || ',' ;
create_first_qu = create_first_qu || quote_literal('Marital_status') || ',' || quote_literal('Physical_activity') || ',' || quote_literal('Medit_diet_score') || ',' || quote_literal('Alcohol_cons') || ',' ;
-- Extraarticular RA 6 fields
create_first_qu = create_first_qu || quote_literal('ra_val_1') || ',' || quote_literal('ra_date_1') || ',' || quote_literal('ra_val_2') || ',' || quote_literal('ra_date_2') || ',' ;
create_first_qu = create_first_qu || quote_literal('ra_val_3') || ',' || quote_literal('ra_date_3') || ',' ;
-- Extraarticular SpA 6 fields
create_first_qu = create_first_qu || quote_literal('spa_val_1') || ',' || quote_literal('spa_date_1') || ',' || quote_literal('spa_val_2') || ',' || quote_literal('spa_date_2') || ',' ;
create_first_qu = create_first_qu || quote_literal('spa_val_3') || ',' || quote_literal('spa_date_3') || ',' ;
-- Pulmonary involvement 9 fields
create_first_qu = create_first_qu || quote_literal('pul_inv_group_1') || ',' || quote_literal('pul_inv_val_1') || ',' || quote_literal('pul_inv_date_1') || ',' ;
create_first_qu = create_first_qu || quote_literal('pul_inv_group_2') || ',' || quote_literal('pul_inv_val_2') || ',' || quote_literal('pul_inv_date_2') || ',' ;
create_first_qu = create_first_qu || quote_literal('pul_inv_group_3') || ',' || quote_literal('pul_inv_val_3') || ',' || quote_literal('pul_inv_date_3') || ',' ;
-- Add disease activity 16 field
create_first_qu = create_first_qu || quote_literal('DAS38-ESR_FU0') || ',' || quote_literal('SDAI_FU0') || ',' || quote_literal('CDAI_FU0') || ',' || quote_literal('MHAQ_FU0') || ',' || quote_literal('ASDAS-CRP_FU0') || ',' ;
create_first_qu = create_first_qu || quote_literal('BSDAI_FU0') || ',' || quote_literal('SLEDAI-2K_FU0') || ',' || quote_literal('SLICC_ACR_damage_index_FU0') || ',';
create_first_qu = create_first_qu || quote_literal('DAS38-ESR_cur_fu') || ',' || quote_literal('SDAI_cur_fu') || ',' || quote_literal('CDAI_cur_fu') || ',' || quote_literal('MHAQ_cur_fu') || ',' || quote_literal('ASDAS-CRP_cur_fu') || ',' ;
create_first_qu = create_first_qu || quote_literal('BSDAI_cur_fu') || ',' || quote_literal('SLEDAI-2K_cur_fu') || ',' || quote_literal('SLICC_ACR_damage_index_cur_fu') || ',';

 FOR drugs_id4 IN SELECT REPLACE (coalesce(drugs.substance,''), ',', ' ') as substance,
 REPLACE (coalesce(lookup_tbl_val.value,''), ',', ' ') as route_of_administration_val 
 FROM drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration
 WHERE drugs.deleted=0 and drugs.previous=0 and drugs.prim=0  order by drugs.drugs_id asc

    LOOP

		create_first_qu = create_first_qu || quote_literal(drugs_id4.substance || '-' || drugs_id4.route_of_administration_val) ;
		create_first_qu = create_first_qu || ',';

	end loop;


	create_first_qu = create_first_qu || quote_literal('J') || ',' || quote_literal('J1') || ',' || quote_literal('J2') || ',';
	
-- comorbidities diagnoses 138 fields

create_first_qu = create_first_qu || quote_literal('Aortic_Aneurysm') || ',' || quote_literal('Aortic_Aneurysm date') || ',' || quote_literal('Cardiac_arrhythmia') || ',' || quote_literal('Cardiac_arrhythmia date') || ',';
create_first_qu = create_first_qu || quote_literal('Congestive_Heart_Failure') || ',' || quote_literal('Congestive_Heart_Failure date') || ',' || quote_literal('Coronary_Heart_Disease') || ',' || quote_literal('Coronary_Heart_Disease date') ;
create_first_qu = create_first_qu || ',' || quote_literal('Acute_myocardial_infarction') || ',' || quote_literal('Acute_myocardial_infarction date') || ',' || quote_literal('Angina_pectoris') || ',' || quote_literal('Angina_pectoris date') || ',' || quote_literal('Atrial_fibrillation') || ',' || quote_literal('Atrial_fibrillation date') ;
create_first_qu = create_first_qu || ',' || quote_literal('Pericardial_disease_Acute_Pericarditis') || ',' || quote_literal('Pericardial_disease_Acute_Pericarditis date') || ',' || quote_literal('Hypertension') || ',' || quote_literal('Hypertension date') || ',';
create_first_qu = create_first_qu || quote_literal('Cardiomyopathies') || ',' || quote_literal('Cardiomyopathies date') || ',' || quote_literal('Peripheral_vascular_disease') || ',' || quote_literal('Peripheral_vascular_disease date') || ',' ||  quote_literal('Valvular_Heart_Disease') || ',' ||  quote_literal('Valvular_Heart_Disease date') ;
create_first_qu = create_first_qu || ',' || quote_literal('Ischemic_strokeTIA') || ',' || quote_literal('Ischemic_strokeTIA date') || ',' || quote_literal('Sequelae_of_ischemic_stroke') || ',' || quote_literal('Sequelae_of_ischemic_stroke date') || ',' || quote_literal('Hemorrhagic_stroke') || ',' || quote_literal('Hemorrhagic_stroke date') ;
create_first_qu = create_first_qu || ',' || quote_literal('Sequelae_of_hemorrhagic_stroke') || ',' || quote_literal('Sequelae_of_hemorrhagic_stroke date') || ',';
create_first_qu = create_first_qu || quote_literal('Anxiety_disorders') || ',' || quote_literal('Anxiety_disorders date') || ',' || quote_literal('Bipolar_disorder') || ',' || quote_literal('Bipolar_disorder date') || ',' || quote_literal('Dementia_unspecified') || ','  || quote_literal('Dementia_unspecified date') || ',' ;
create_first_qu = create_first_qu || quote_literal('Major_depression_without_psychotic_symptoms') || ',' || quote_literal('Major_depression_without_psychotic_symptoms date') || ',' || quote_literal('Other_depressive_episodes') || ',' || quote_literal('Other_depressive_episodes_date') || ',' || quote_literal('Multiple_Sclerosis_Demyelinating_disease') || ',' || quote_literal('Multiple_Sclerosis_Demyelinating_disease date') || ',';
create_first_qu = create_first_qu || quote_literal('Parkinsons_disease') || ',' || quote_literal('Parkinsons_disease date') || ',' || quote_literal('Peripheral_Neuropathy') || ',' || quote_literal('Peripheral_Neuropathy date') || ',' || quote_literal('Psychosis') || ','  || quote_literal('Psychosis date') || ',';
create_first_qu = create_first_qu || quote_literal('Schizophrenia') || ',' || quote_literal('Schizophrenia date') || ',' || quote_literal('Asthma') || ',' || quote_literal('Asthma date') || ',' || quote_literal('Bronchiectasis') || ',' || quote_literal('Bronchiectasis_date') || ',' || quote_literal('Chronic_obstructive_pulmonary_disease') || ',' || quote_literal('Chronic_obstructive_pulmonary_disease_date') || ',' || quote_literal('Chronic_sinusitis') || ',' || quote_literal('Chronic_sinusitis date') || ',';
create_first_qu = create_first_qu || quote_literal('Interstitial_Lung_Disease') || ',' || quote_literal('Interstitial_Lung_Disease date') || ',' || quote_literal('Pulmonary_Hypertension') || ',' || quote_literal('Pulmonary_Hypertension date') || ',' || quote_literal('Autoimmune_hepatitis') || ',' || quote_literal('Autoimmune_hepatitis date') ;
create_first_qu = create_first_qu || ',' || quote_literal('Alcoholic_liver_disease') || ',' || quote_literal('Alcoholic_liver_disease date') || ',' || quote_literal('Alcoholic_liver_cirrhosis') || ',' || quote_literal('Alcoholic_liver_cirrhosis date') || ',' ||  quote_literal('Nonalcoholic_fatty_liver_disease_NAFLD') || ',' ||  quote_literal('Nonalcoholic_fatty_liver_disease_NAFLD date') || ',';
create_first_qu = create_first_qu || quote_literal('Crohn’s_disease') || ',' || quote_literal('Crohn’s_disease date') || ',';
create_first_qu = create_first_qu || quote_literal('Diverticulosis') || ',' || quote_literal('Diverticulosis date') || ',' || quote_literal('EsophagitisGERD') || ',' || quote_literal('EsophagitisGERD date') || ',' || quote_literal('Peptic_ulcer_disease_biopsy_proven') || ','  || quote_literal('Peptic_ulcer_disease_biopsy_proven date') || ',';
create_first_qu = create_first_qu || quote_literal('Diabetes_mellitus') || ',' || quote_literal('Diabetes_mellitus date') || ',' || quote_literal('Dyslipidaemia') || ',' || quote_literal('Dyslipidaemia date') || ',' || quote_literal('Hypothyroidism') || ',' || quote_literal('Hypothyroidism_date') || ',' || quote_literal('Hyperthyroidism') || ',' || quote_literal('Hyperthyroidism date') ;
create_first_qu = create_first_qu || ',' || quote_literal('Obesity') || ',' || quote_literal('Obesity date') || ',' || quote_literal('Chronic_kidney_disease_stage_III_GFR60') || ',' || quote_literal('Chronic_kidney_disease_stage_III_GFR60 date') || ',';
create_first_qu = create_first_qu || quote_literal('Chronic_kidney_disease_stage_IIIIV_(GFR_15-59)') || ',' || quote_literal('Chronic_kidney_disease_stage_IIIIV_(GFR_15-59) date') || ',' || quote_literal('End-stage_renal_disease_GFR_15') || ',' || quote_literal('End-stage_renal_disease_GFR_15 date') || ',' || quote_literal('Hodgkin_Lymphoma')  || ',' || quote_literal('Hodgkin_Lymphoma date');
create_first_qu = create_first_qu || ',' || quote_literal('Non-Hodgkin_Lymphoma_unspecified') || ',' || quote_literal('Non-Hodgkin_Lymphoma_unspecified date') || ',' || quote_literal('Leukemia') || ',' || quote_literal('Leukemia date') || ',';
create_first_qu = create_first_qu || quote_literal('Solid_tumor') || ',' || quote_literal('Solid_tumor date') || ',' || quote_literal('Solid_tumor_without_metastasi') || ',' || quote_literal('Solid_tumor_without_metastasi date') || ',' || quote_literal('Metastatic_solid_tumor') || ','  || quote_literal('Metastatic_solid_tumor date') || ',' ;
create_first_qu = create_first_qu || quote_literal('Metastatic_cancer_without_known_primary_tumor_site') || ',' || quote_literal('Metastatic_cancer_without_known_primary_tumor_site date') || ',' || quote_literal('Ischemic_Optic_Neuropathy')  || ',' || quote_literal('Ischemic_Optic_Neuropathy date') || ',' || quote_literal('Optic_Neuritis')  || ',' || quote_literal('Optic_Neuritis date') ;
create_first_qu = create_first_qu || ',' || quote_literal('Chronic_osteomyelitis') || ','  || quote_literal('Chronic_osteomyelitis date') || ',' ;
create_first_qu = create_first_qu || quote_literal('Latent_TB_infection') || ',' || quote_literal('Latent_TB_infection date') || ',' || quote_literal('Viral_hepatitis') || ',' || quote_literal('Viral_hepatitis date') || ',' || quote_literal('Chronic_viral_infection_other') || ','  || quote_literal('Chronic_viral_infection_other date') || ',';
create_first_qu = create_first_qu || quote_literal('Past_hospitalization_for_serious_infection') || ',' || quote_literal('Past_hospitalization_for_serious_infection date') || ',' || quote_literal('Recurrent_past_hospitalizations_for_serious_infections') || ',' || quote_literal('Recurrent_past_hospitalizations_for_serious_infections date') || ',' || quote_literal('History_of_opportunistic_infection') || ',' || quote_literal('History_of_opportunistic_infection date') || ',';
create_first_qu = create_first_qu || quote_literal('Prosthetic_left_hip_joint') || ',' || quote_literal('Prosthetic_left_hip_joint date') || ',' || quote_literal('Prosthetic_right_hip_joint') || ',' || quote_literal('Prosthetic_right_hip_joint date') || ',' || quote_literal('Prosthetic_left_knee_joint')  || ',' || quote_literal('Prosthetic_left_knee_joint date') ;
create_first_qu = create_first_qu || ',' || quote_literal('Prosthetic_right_knee_joint') || ',' || quote_literal('Prosthetic_right_knee_joint date') || ',' || quote_literal('Prosthetic_joint_other') || ',' || quote_literal('Prosthetic_joint_other date');


	create_first_qu = create_first_qu || ');' ;

execute create_first_qu;

  FOR patient_record IN select coalesce(patient_event.patient_event_id,0) as patient_event_id,coalesce(patient_cohort.patient_cohort_id,0) as patient_cohort_id
	,coalesce(patient_followup.fumonthcohort,0) as fumonthcohort
  	,coalesce(patient.patient_id,0) as patient_id,coalesce(patient_cohort.pat_id,0) as pat_id,coalesce(patient_cohort.cohort_name,0) as cohort_name,
	REPLACE (coalesce(drugs.substance,''), ',', ' ')  as substance
	,coalesce(patient.centre,0) as centre,REPLACE (coalesce(lookup_tbl_val.value,''), ',', ' ') as centre_name,
	coalesce(patient_cohort.treatment_nbr,'') as treatment_nbr,coalesce(TO_CHAR(patient_cohort.start_date, 'DD-MM-YYYY'),'12-12-1900') AS start_date_str,
	coalesce(TO_CHAR(patient_cohort.stop_date, 'DD-MM-YYYY'), '12-12-1900') AS stop_date_str
	,coalesce(patient_cohort.stop_reason,0) as stop_reason,REPLACE (coalesce(patient_cohort.stop_ae_reason,''), ',', ' ') as stop_ae_reason,coalesce(patient.gender,0) as gender,
	coalesce(TO_CHAR(patient.dateofinclusion, 'DD-MM-YYYY'),'') AS dateofinclusion_str,
	coalesce(TO_CHAR(patient.dateofbirth, 'DD-MM-YYYY'),'') AS dateofbirth_str,
	coalesce(TO_CHAR(patient_event.start_date, 'DD-MM-YYYY'),'12-12-1900') AS adversedate_str,
	coalesce(patient_event.hospitalization,0) as hospitalization,
	coalesce(patient_event.main_organ,0) as main_organ,
	coalesce(patient_event.secondary_organ,0) as secondary_organ,
	coalesce(patient_event.relationship,0) as relationship,
	coalesce(patient_event.outcome,0) as outcome,
	coalesce(patient_event.seriousness,0) as seriousness,
	coalesce(patient_event.change_therapy,0) as change_therapy
	,REPLACE (coalesce(patient_event.description,''), ',', ' ')  as event_description,
	-- demographics patient
	coalesce(patient.ethnicity,0) as ethnicity,coalesce(patient.yearsofeduc,0) as yearsofeduc,
	-- demographics patient
	coalesce(patient_cohort.residence,0) as residence,coalesce(patient_cohort.employment_at_cohort,0) as employment,
	coalesce(patient_cohort.physical_status,0) as physical_status,coalesce(patient_cohort.marital_status,0) as marital_status,
	coalesce(patient_cohort.mediterranean_diet,'') as mediterranean_diet,coalesce(patient_cohort.alcohol_consumption,'') as alcohol_consumption
	 from patient_event 
	left join patient_followup on patient_event.patient_followup_id=patient_followup.patient_followup_id 
	left join patient_cohort on patient_event.patient_cohort_id=patient_cohort.patient_cohort_id 
	left join patient on patient.pat_id=patient_cohort.pat_id 
	left join drugs on patient_cohort.cohort_name=drugs.drugs_id 
	left join lookup_tbl_val on lookup_tbl_val.id=patient.centre 
	where patient.deleted=0 and patient_cohort.deleted=0 and patient_event.deleted=0
	--and patient_cohort.patient_cohort_id=4150

    LOOP

	krow2=krow2 + 1;

	  qu = 'INSERT INTO statistics_lk4 VALUES ( ' || krow2 || ',' ;
	  --vasika pedia 14 pedia
	  qu = qu || quote_literal(patient_record.patient_cohort_id) || ',' || quote_literal(patient_record.patient_id) || ',' ;
	  qu = qu || quote_literal(patient_record.cohort_name) || ',' || quote_literal(patient_record.substance) || ',' ;
	  qu = qu || quote_literal(patient_record.centre) || ',' || quote_literal(patient_record.centre_name) || ',' ;
	  qu = qu || quote_literal(patient_record.treatment_nbr) || ',' || quote_literal(patient_record.start_date_str) || ',' ;
	  qu = qu || quote_literal(patient_record.stop_date_str) || ',' || quote_literal(patient_record.stop_reason) || ',' ;
	  qu = qu || quote_literal(patient_record.stop_ae_reason) || ',' || quote_literal(patient_record.gender) || ',' ;
	  qu = qu || quote_literal(patient_record.dateofinclusion_str) || ',' || quote_literal(patient_record.dateofbirth_str) || ',' ;

	
		  
	  FOR drugs_id4 IN select count(diagnosis_id) as cnt from patient_diagnosis 
		where deleted=0 and pat_id=patient_record.pat_id and patient_diagnosis.main=1 and inactive=0
		LOOP
		end loop;

		IF drugs_id4.cnt>0 then

			FOR drugs_id3 IN select coalesce(patient_diagnosis.diagnosis_id,0) as diagnosis_id
		  ,coalesce(TO_CHAR(patient_diagnosis.symptom_date, 'DD-MM-YYYY'),'') 
		  AS symptom_date_str,coalesce(TO_CHAR(patient_diagnosis.disease_date, 'DD-MM-YYYY'),'') AS disease_date_str,
		  REPLACE (coalesce(diagnosis.icd10,''), ',', ' ') as icd10,
		  REPLACE (coalesce(diagnosis.value,''), ',', ' ') as value from patient_diagnosis left join diagnosis on 
		  diagnosis.diagnosis_id=patient_diagnosis.diagnosis_id where patient_diagnosis.deleted=0 and 
		  patient_diagnosis.pat_id=patient_record.pat_id and patient_diagnosis.main=1 and inactive=0

			LOOP

			qu = qu || quote_literal(drugs_id3.disease_date_str) || ',' || quote_literal(drugs_id3.icd10) || ',' ;
			qu = qu || quote_literal(drugs_id3.value) || ',' || quote_literal(drugs_id3.symptom_date_str) || ',' ;

			end loop;

		ELSE
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		END IF;


	 FOR drugs_id4 IN select count(diagnosis_id) as cnt from patient_diagnosis 
		where deleted=0 and pat_id=patient_record.pat_id and patient_diagnosis.main=0  and inactive=0
		LOOP
		END LOOP;

	IF drugs_id4.cnt=0 then

		qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;

	END IF;	

	if drugs_id4.cnt=1 then

		FOR drugs_id3 IN select coalesce(patient_diagnosis.diagnosis_id,0) as diagnosis_id
		  ,coalesce(TO_CHAR(patient_diagnosis.symptom_date, 'DD-MM-YYYY'),'') 
		  AS symptom_date_str,coalesce(TO_CHAR(patient_diagnosis.disease_date, 'DD-MM-YYYY'),'') AS disease_date_str,
		  REPLACE (coalesce(diagnosis.icd10,''), ',', ' ') as icd10,
		  REPLACE (coalesce(diagnosis.value,''), ',', ' ') as value from patient_diagnosis left join diagnosis on 
		  diagnosis.diagnosis_id=patient_diagnosis.diagnosis_id where patient_diagnosis.deleted=0 and 
		  patient_diagnosis.pat_id=patient_record.pat_id and patient_diagnosis.main=0  and inactive=0 limit 1

		  LOOP

			qu = qu || quote_literal(drugs_id3.disease_date_str) || ',' || quote_literal(drugs_id3.icd10) || ',' ;
			qu = qu || quote_literal(drugs_id3.value) || ',' || quote_literal(drugs_id3.symptom_date_str) || ',' ; 

		  END LOOP;

			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
	end if;		

	if drugs_id4.cnt=2 then

		FOR drugs_id3 IN select coalesce(patient_diagnosis.diagnosis_id,0) as diagnosis_id
		  ,coalesce(TO_CHAR(patient_diagnosis.symptom_date, 'DD-MM-YYYY'),'') 
		  AS symptom_date_str,coalesce(TO_CHAR(patient_diagnosis.disease_date, 'DD-MM-YYYY'),'') AS disease_date_str,
		  REPLACE (coalesce(diagnosis.icd10,''), ',', ' ') as icd10,
		  REPLACE (coalesce(diagnosis.value,''), ',', ' ') as value from patient_diagnosis left join diagnosis on 
		  diagnosis.diagnosis_id=patient_diagnosis.diagnosis_id where patient_diagnosis.deleted=0 and 
		  patient_diagnosis.pat_id=patient_record.pat_id and patient_diagnosis.main=0  and inactive=0 limit 2

		  LOOP

			qu = qu || quote_literal(drugs_id3.disease_date_str) || ',' || quote_literal(drugs_id3.icd10) || ',' ;
			qu = qu || quote_literal(drugs_id3.value) || ',' || quote_literal(drugs_id3.symptom_date_str) || ',' ; 

		  END LOOP;

			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;

	end if;	  

	if drugs_id4.cnt>=3 then

		FOR drugs_id3 IN select coalesce(patient_diagnosis.diagnosis_id,0) as diagnosis_id
		  ,coalesce(TO_CHAR(patient_diagnosis.symptom_date, 'DD-MM-YYYY'),'') 
		  AS symptom_date_str,coalesce(TO_CHAR(patient_diagnosis.disease_date, 'DD-MM-YYYY'),'') AS disease_date_str,
		  REPLACE (coalesce(diagnosis.icd10,''), ',', ' ') as icd10,
		  REPLACE (coalesce(diagnosis.value,''), ',', ' ') as value from patient_diagnosis left join diagnosis on 
		  diagnosis.diagnosis_id=patient_diagnosis.diagnosis_id where patient_diagnosis.deleted=0 and 
		  patient_diagnosis.pat_id=patient_record.pat_id and patient_diagnosis.main=0  and inactive=0 limit 3

		  LOOP

			qu = qu || quote_literal(drugs_id3.disease_date_str) || ',' || quote_literal(drugs_id3.icd10) || ',' ;
			qu = qu || quote_literal(drugs_id3.value) || ',' || quote_literal(drugs_id3.symptom_date_str) || ',' ; 
		  END LOOP;

	end if;

	   qu = qu || quote_literal(patient_record.adversedate_str) || ',' || quote_literal(patient_record.hospitalization) || ',' ;
	   qu = qu || quote_literal(patient_record.main_organ) || ',' || quote_literal(patient_record.secondary_organ) || ',' ;
	   qu = qu || quote_literal(patient_record.relationship) || ',' || quote_literal(patient_record.outcome) || ',' ;
	   qu = qu || quote_literal(patient_record.seriousness) || ',' || quote_literal(patient_record.change_therapy) || ',';
	   qu = qu || quote_literal(patient_record.fumonthcohort) || ',' || quote_literal(patient_record.patient_event_id) || ',' || quote_literal(patient_record.event_description) || ',' ;

	-- Demographics 8 fields
	
		IF patient_record.ethnicity=0 then
			ethnicity='';
		ELSEIF patient_record.ethnicity=1 then
			ethnicity='Greek';
		ELSEIF patient_record.ethnicity=2 then
			ethnicity='Other';
		ELSEIF patient_record.ethnicity=3 then
			ethnicity='Dont know';
		ELSE
			ethnicity=patient_record.ethnicity;
		END IF;
		
		IF patient_record.yearsofeduc=0 then
			yearsofeduc='';
		ELSEIF patient_record.yearsofeduc=1 then
			yearsofeduc='0';
		ELSEIF patient_record.yearsofeduc=2 then
			yearsofeduc='1-6';
		ELSEIF patient_record.yearsofeduc=3 then
			yearsofeduc='7-9';
		ELSEIF patient_record.yearsofeduc=4 then
			yearsofeduc='10-12';
		ELSEIF patient_record.yearsofeduc=5 then
			yearsofeduc='12';
		ELSEIF patient_record.yearsofeduc=6 then
			yearsofeduc='Unknown';
		ELSE
			yearsofeduc=patient_record.yearsofeduc;
		END IF;
		
		IF patient_record.residence=0 then
			residence='';
		ELSEIF patient_record.residence=1 then
			residence='Urban (>15.000 dwellers)';
		ELSEIF patient_record.residence=2 then
			residence='Suburban (10-15.000 dwellers)';
		ELSEIF patient_record.residence=3 then
			residence='Rular (<10.000 dwellers)';
		ELSEIF patient_record.residence=4 then
			residence='Dont know';
		ELSE
			residence=patient_record.residence;
		END IF;
		
		IF patient_record.employment=0 then
			employment='';
		ELSEIF patient_record.employment=1 then
			employment='Unemployed';
		ELSEIF patient_record.employment=2 then
			employment='Student';
		ELSEIF patient_record.employment=3 then
			employment='Employee';
		ELSEIF patient_record.employment=4 then
			employment='Worker';
		ELSEIF patient_record.employment=5 then
			employment='Self-employed';
		ELSEIF patient_record.employment=6 then
			employment='Retired';
		ELSEIF patient_record.employment=7 then
			employment='Disability pension';
		ELSEIF patient_record.employment=8 then
			employment='Unknown';
		ELSEIF patient_record.employment=9 then
			employment='Housewife';
		ELSE
			employment=patient_record.employment;
		END IF;
		
		IF patient_record.marital_status=0 then
			marital_status='';
		ELSEIF patient_record.marital_status=1 then
			marital_status='Not married';
		ELSEIF patient_record.marital_status=2 then
			marital_status='Married';
		ELSEIF patient_record.marital_status=3 then
			marital_status='Divorced';
		ELSEIF patient_record.marital_status=4 then
			marital_status='Widow';
		ELSEIF patient_record.marital_status=5 then
			marital_status='Unknown';
		ELSE
			marital_status=patient_record.marital_status;
		END IF;
		
		IF patient_record.physical_status=0 then
			physical_status='';
		ELSEIF patient_record.physical_status=1 then
			physical_status='Moving only for necesary chores';
		ELSEIF patient_record.physical_status=2 then
			physical_status='Walking or other outdoor activities 1-2 times per week';
		ELSEIF patient_record.physical_status=3 then
			physical_status='Walking or other outdoor activities several times per week';
		ELSEIF patient_record.physical_status=4 then
			physical_status='Exercing 1-2 times per week to the point of perspiring and heavy breathing';
		ELSEIF patient_record.physical_status=5 then
			physical_status='Exercing several times per week to the point of perspiring and heavy breathing';
		ELSEIF patient_record.physical_status=6 then
			physical_status='Keep-fit heavy exercise ot competitive sport several times per week';
		ELSE
			physical_status=patient_record.physical_status;
		END IF;
		
		qu = qu || quote_literal(ethnicity) || ',' || quote_literal(yearsofeduc) || ',' || quote_literal(residence) || ',' || quote_literal(employment) || ',' ;
		qu = qu || quote_literal(marital_status) || ',' || quote_literal(physical_status) || ',' || quote_literal(patient_record.mediterranean_diet) || ',' || quote_literal(patient_record.alcohol_consumption) || ',' ;
		
		-- Extraarticular RA 6 fields
		FOR ra_cnt IN select count(patient_ra_extraarticular_id) as cnt from patient_ra_extraarticular 
		where deleted=0 and pat_id=patient_record.pat_id 
		LOOP
		END LOOP;
		
			IF ra_cnt.cnt=0 then
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			END IF;	
			
			if ra_cnt.cnt=1 then
				FOR ra1 IN select coalesce(lookup_tbl_val.value,'') as ra_value,coalesce(TO_CHAR(patient_ra_extraarticular.date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_ra_extraarticular left join lookup_tbl_val on patient_ra_extraarticular.ra_extraarticular_id=lookup_tbl_val.id 
				  where patient_ra_extraarticular.deleted=0 and pat_id=patient_record.pat_id order by patient_ra_extraarticular.patient_ra_extraarticular_id desc limit 1
				  LOOP
					qu = qu || quote_literal(ra1.ra_value) || ',' || quote_literal(ra1.date_str) || ',' ;
				  END LOOP;
				  
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			end if;	
			
			if ra_cnt.cnt=2 then
				FOR ra2 IN select coalesce(lookup_tbl_val.value,'') as ra_value,coalesce(TO_CHAR(patient_ra_extraarticular.date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_ra_extraarticular left join lookup_tbl_val on patient_ra_extraarticular.ra_extraarticular_id=lookup_tbl_val.id 
				  where patient_ra_extraarticular.deleted=0 and pat_id=patient_record.pat_id order by patient_ra_extraarticular.patient_ra_extraarticular_id desc limit 2
				  LOOP
					qu = qu || quote_literal(ra2.ra_value) || ',' || quote_literal(ra2.date_str) || ',' ;
				  end loop;
				  
				  qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			end if;	  
			
			if ra_cnt.cnt>=3 then
				FOR ra3 IN select coalesce(lookup_tbl_val.value,'') as ra_value,coalesce(TO_CHAR(patient_ra_extraarticular.date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_ra_extraarticular left join lookup_tbl_val on patient_ra_extraarticular.ra_extraarticular_id=lookup_tbl_val.id 
				  where patient_ra_extraarticular.deleted=0 and pat_id=patient_record.pat_id order by patient_ra_extraarticular.patient_ra_extraarticular_id desc limit 3
				  LOOP
					qu = qu || quote_literal(ra3.ra_value) || ',' || quote_literal(ra3.date_str) || ',' ;
				  END LOOP;
			end if;
			
		
		-- Extraarticular SpA 6 fields
		FOR spa_cnt IN select count(patient_spa_extraarticular_id) as cnt from patient_spa_extraarticular 
		where deleted=0 and pat_id=patient_record.pat_id 
		LOOP
		END LOOP;
		
			IF spa_cnt.cnt=0 then
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			END IF;	
			
			if spa_cnt.cnt=1 then
				FOR spa1 IN select coalesce(lookup_tbl_val.value,'') as spa_value,coalesce(TO_CHAR(patient_spa_extraarticular.date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_spa_extraarticular left join lookup_tbl_val on patient_spa_extraarticular.spa_extraarticular_id=lookup_tbl_val.id 
				  where patient_spa_extraarticular.deleted=0 and pat_id=patient_record.pat_id order by patient_spa_extraarticular.patient_spa_extraarticular_id desc limit 1
				  LOOP
					qu = qu || quote_literal(spa1.spa_value) || ',' || quote_literal(spa1.date_str) || ',' ;
				  END LOOP;
				  
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			end if;	
			
			if spa_cnt.cnt=2 then
				FOR spa2 IN select coalesce(lookup_tbl_val.value,'') as spa_value,coalesce(TO_CHAR(patient_spa_extraarticular.date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_spa_extraarticular left join lookup_tbl_val on patient_spa_extraarticular.spa_extraarticular_id=lookup_tbl_val.id 
				  where patient_spa_extraarticular.deleted=0 and pat_id=patient_record.pat_id order by patient_spa_extraarticular.patient_spa_extraarticular_id desc limit 2
				  LOOP
					qu = qu || quote_literal(spa2.spa_value) || ',' || quote_literal(spa2.date_str) || ',' ;
				  end loop;
				  
				  qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			end if;	  
			
			if spa_cnt.cnt>=3 then
				FOR spa3 IN select coalesce(lookup_tbl_val.value,'') as spa_value,coalesce(TO_CHAR(patient_spa_extraarticular.date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_spa_extraarticular left join lookup_tbl_val on patient_spa_extraarticular.spa_extraarticular_id=lookup_tbl_val.id 
				  where patient_spa_extraarticular.deleted=0 and pat_id=patient_record.pat_id order by patient_spa_extraarticular.patient_spa_extraarticular_id desc limit 3
				  LOOP
					qu = qu || quote_literal(spa3.spa_value) || ',' || quote_literal(spa3.date_str) || ',' ;
				  END LOOP;
			end if;
		
		-- Pulmonary involvement 9 fields
		FOR spa_cnt IN select count(patient_pulmonary_id) as cnt from patient_pulmonary 
		where deleted=0 and pat_id=patient_record.pat_id 
		LOOP
		END LOOP;
		
			IF spa_cnt.cnt=0 then
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			END IF;	
			
			if spa_cnt.cnt=1 then
				FOR spa1 IN select coalesce(group_val.value,'') as group_value,coalesce(content_val.value,'') as content_value,coalesce(TO_CHAR(patient_pulmonary.pulmonary_date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_pulmonary 
				  left join lookup_tbl_val as group_val on patient_pulmonary.group_id=group_val.id
				  left join lookup_tbl_val as content_val on patient_pulmonary.content=content_val.id 
				  where patient_pulmonary.deleted=0 and pat_id=patient_record.pat_id order by patient_pulmonary.patient_pulmonary_id desc limit 1
				  LOOP
					qu = qu || quote_literal(spa1.group_value) || ',' || quote_literal(spa1.content_value) || ','  || quote_literal(spa1.date_str) || ',' ;
				  END LOOP;
				  
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			end if;	
			
			if spa_cnt.cnt=2 then
				FOR spa2 IN  select coalesce(group_val.value,'') as group_value,coalesce(content_val.value,'') as content_value,coalesce(TO_CHAR(patient_pulmonary.pulmonary_date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_pulmonary 
				  left join lookup_tbl_val as group_val on patient_pulmonary.group_id=group_val.id
				  left join lookup_tbl_val as content_val on patient_pulmonary.content=content_val.id 
				  where patient_pulmonary.deleted=0 and pat_id=patient_record.pat_id order by patient_pulmonary.patient_pulmonary_id desc limit 2
				  LOOP
					qu = qu || quote_literal(spa2.group_value) || ',' || quote_literal(spa2.content_value) || ','  || quote_literal(spa2.date_str) || ',' ;
				  end loop;
				  
				  qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			end if;	  
			
			if spa_cnt.cnt>=3 then
				FOR spa3 IN select coalesce(group_val.value,'') as group_value,coalesce(content_val.value,'') as content_value,coalesce(TO_CHAR(patient_pulmonary.pulmonary_date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_pulmonary 
				  left join lookup_tbl_val as group_val on patient_pulmonary.group_id=group_val.id
				  left join lookup_tbl_val as content_val on patient_pulmonary.content=content_val.id 
				  where patient_pulmonary.deleted=0 and pat_id=patient_record.pat_id order by patient_pulmonary.patient_pulmonary_id desc limit 3
				  LOOP
					qu = qu || quote_literal(spa3.group_value) || ',' || quote_literal(spa3.content_value) || ','  || quote_literal(spa3.date_str) || ',' ;
				  END LOOP;
			end if;
		
		-- Disease activity  16 field
		  
		  FOR dis_act IN select coalesce(patient_common.das28esr,'') as das28esr,coalesce(patient_common.sdai,'') as sdai,coalesce(patient_common.cdai,'') as cdai,
		  coalesce(patient_common.mhaq,'') as mhaq,coalesce(patient_spa.asdascrp,'') as asdascrp,coalesce(patient_spa.basdai,'') as basdai
		  ,coalesce(patient_sle.sledai2k,0) as sledai2k,coalesce(patient_sle_severity.slicc42,'') as sliccacr
		  from patient_cohort
		  left join patient_followup on patient_cohort.patient_cohort_id=patient_followup.patient_cohort_id
		  left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id
		  left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id
		  left join patient_sle on patient_sle.patient_followup_id=patient_followup.patient_followup_id
		  left join patient_sle_severity on patient_sle_severity.patient_followup_id=patient_followup.patient_followup_id
		  where patient_followup.deleted=0 and patient_cohort.pat_id=patient_record.pat_id and patient_cohort.patient_cohort_id=patient_record.patient_cohort_id 
		  and patient_followup.fumonthcohort=0 limit 1
		  LOOP
			qu = qu || quote_literal(dis_act.das28esr) || ',' || quote_literal(dis_act.sdai) || ','  || quote_literal(dis_act.cdai) || ',' ;
			qu = qu || quote_literal(dis_act.mhaq) || ',' || quote_literal(dis_act.asdascrp) || ','  || quote_literal(dis_act.basdai) || ',' ;
			qu = qu || quote_literal(dis_act.sledai2k) || ',' || quote_literal(dis_act.sliccacr) || ',' ;
		  END LOOP;
		
		FOR dis_act IN select coalesce(patient_common.das28esr,'') as das28esr,coalesce(patient_common.sdai,'') as sdai,coalesce(patient_common.cdai,'') as cdai,
		  coalesce(patient_common.mhaq,'') as mhaq,coalesce(patient_spa.asdascrp,'') as asdascrp,coalesce(patient_spa.basdai,'') as basdai
		  ,coalesce(patient_sle.sledai2k,0) as sledai2k,coalesce(patient_sle_severity.slicc42,'') as sliccacr
		  from patient_cohort
		  left join patient_followup on patient_cohort.patient_cohort_id=patient_followup.patient_cohort_id
		  left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id
		  left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id
		  left join patient_sle on patient_sle.patient_followup_id=patient_followup.patient_followup_id
		  left join patient_sle_severity on patient_sle_severity.patient_followup_id=patient_followup.patient_followup_id
		  where patient_followup.deleted=0 and patient_cohort.pat_id=patient_record.pat_id and patient_cohort.patient_cohort_id=patient_record.patient_cohort_id 
		  and patient_followup.fumonthcohort=patient_record.fumonthcohort limit 1
		  LOOP
			qu = qu || quote_literal(dis_act.das28esr) || ',' || quote_literal(dis_act.sdai) || ','  || quote_literal(dis_act.cdai) || ',' ;
			qu = qu || quote_literal(dis_act.mhaq) || ',' || quote_literal(dis_act.asdascrp) || ','  || quote_literal(dis_act.basdai) || ',' ;
			qu = qu || quote_literal(dis_act.sledai2k) || ',' || quote_literal(dis_act.sliccacr) || ',' ;
		  END LOOP;		

	 FOR drugs_id3 IN SELECT drugs_id FROM drugs WHERE deleted=0 and prim=0 and previous=0  order by drugs_id asc
    LOOP
		 for drugs_id4 in select count(*) as cnt
		from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
		and pat_id= patient_record.pat_id and drug_flag= 1
		and deleted= '0' and fumonth_cohort=patient_record.fumonthcohort and drugs_id= drugs_id3.drugs_id limit 1 
		  LOOP
		  END LOOP;

		if drugs_id4.cnt>0 then

			for mviews in select weekdosage::text as weekdosage0
			from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			and pat_id= patient_record.pat_id and drug_flag= 1
			and deleted= '0' and fumonth_cohort=patient_record.fumonthcohort and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 
			  LOOP

			if mviews.weekdosage0 is null then
				qu = qu || quote_literal('') || ',';
			else
				qu = qu || quote_literal(mviews.weekdosage0) || ',';
			end if;

		END LOOP;

		else
			qu = qu || quote_literal('') || ',';
		end if;
	END LOOP;

	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort=patient_record.fumonthcohort and drug_flag= 2  and drugs_id=0 
		LOOP
		END LOOP;

		IF drugs_id4.cnt>0 then

			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort=patient_record.fumonthcohort and drug_flag= 2  and drugs_id=0 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			END LOOP;
		ELSE
			qu = qu || quote_literal(dflt) || ',';
		END IF;

		FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort=patient_record.fumonthcohort and drug_flag= 2  and drugs_id=1 
		LOOP
		END LOOP;

		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort=patient_record.fumonthcohort and drug_flag= 2  and drugs_id=1 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			END LOOP;
		ELSE
			qu = qu || quote_literal(dflt) || ',';
		END IF;

	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort=patient_record.fumonthcohort and drug_flag= 2  and drugs_id=2 
		LOOP
		END LOOP;

		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort=patient_record.fumonthcohort and drug_flag= 2  and drugs_id=2 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			END LOOP;
		ELSE
			qu = qu || quote_literal(dflt) || ',';
		END IF;
		
		
		-- comorbidities diagnoses
			FOR drugs_id3 IN select diseases_comorbidities_id from diseases_comorbidities 
		  where diseases_comorbidities_id in (1,2,3,4,5,6,7,8,9,11,12,14,17,18,19,20,23,24,28,34,35,40,41,42,43,44,50
		  ,51,52,53,54,55,56,57,58,59,61,64,65,69,74,75,76,78,80,81,82,83,85,86,87,88,89,90,91,98,100,106,107,108,109
		  ,110,111,112,116,117,118,119,120)
		  order by diseases_comorbidities_id asc
		  LOOP
			  FOR mviews IN select count(*) as cnt from nonrheumatic_diseases where pat_id=patient_record.pat_id 
				  and deleted=0 and diseases_comorbidities_id=drugs_id3.diseases_comorbidities_id
				  LOOP
					IF mviews.cnt > 0 THEN
						 FOR drugs_id4 IN select coalesce(TO_CHAR(date_of_diagnosis, 'DD-MM-YYYY'),'') AS date_of_diagnosis_str from nonrheumatic_diseases where pat_id=patient_record.pat_id 
				  and deleted=0 and diseases_comorbidities_id=drugs_id3.diseases_comorbidities_id
							LOOP
							END LOOP;
						IF drugs_id3.diseases_comorbidities_id =120 THEN
							qu = qu || 'TRUE,' || quote_literal(drugs_id4.date_of_diagnosis_str);
						ELSE
							qu = qu || 'TRUE,' || quote_literal(drugs_id4.date_of_diagnosis_str) || ',' ;
						END IF;
					ELSE
						IF drugs_id3.diseases_comorbidities_id =120 THEN
							qu = qu || 'FALSE,' || quote_literal(dflt) ;
						ELSE
							qu = qu || 'FALSE,' || quote_literal(dflt) || ',' ;
						END IF;
					END IF;
			  end loop;
		  end loop;
		
	qu = qu || ')';	

	execute qu;

END LOOP;

RETURN  1;


END; $BODY$;

ALTER FUNCTION public.create_insert_query_event()
    OWNER TO postgres;
