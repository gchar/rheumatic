-- FUNCTION: public.create_insert_query_biobanking()

-- DROP FUNCTION public.create_insert_query_biobanking();

CREATE OR REPLACE FUNCTION public.create_insert_query_biobanking(
	)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100000
    VOLATILE 

AS $BODY$DECLARE

krow int=0;
krow2 int=1;
count_rows RECORD;
drugs_id1 RECORD;
drugs_id3 RECORD;
drugs_id4 RECORD;
patient_record RECORD;
drugs_id5 int;
qu text; 
create_qu text;
create_first_qu text;
dflt character='';
mviews RECORD;  

BEGIN 

execute 'DROP TABLE if exists statistics_lk5;';

create_qu = 'CREATE TABLE statistics_lk5 (id integer,';
-- pedia gia to biobanking	11 pedia
create_qu = create_qu || 'a1 VARCHAR,a2 VARCHAR,a3 date,a4 VARCHAR,a5 VARCHAR,a6 VARCHAR,a7 VARCHAR,a8 VARCHAR,a9 VARCHAR,a10 VARCHAR,a11 VARCHAR,';

-- pat_id-date of birth-gender-main diagnosis symptom_date-main diagnosis disease_date-main diagnosis icd10-main diagnosis value-disease duration-RF-AntiCCP-ANA-cohort-followup month 13 pedia
create_qu = create_qu || 'a12 VARCHAR,a13 VARCHAR,a14 VARCHAR, a15 VARCHAR,a16 VARCHAR,a17 VARCHAR,a18 VARCHAR,a19 VARCHAR,a20 VARCHAR,a21 VARCHAR,a22 VARCHAR,a23 VARCHAR,a24 VARCHAR,';

-- DAS28-HAQ-BASDAI-ASDAS-BASFI-SLEDAI 2K-SLICC-SLEDAI21 High serum antidsDNA 9 pedia
create_qu = create_qu || 'a25 VARCHAR,a26 VARCHAR,a27 VARCHAR,a28 VARCHAR,a29 VARCHAR,a30 VARCHAR,a31 VARCHAR,a32 VARCHAR,a33 VARCHAR,a34 VARCHAR,';


FOR count_rows IN SELECT count(drugs.drugs_id) as count_rows FROM drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration
	WHERE drugs.deleted=0 and drugs.prim=0 and drugs.previous=0 
	and drugs.code not in ('A7','A8','A10','H1','H2','H3','J')
LOOP
END LOOP;

FOR drugs_id3 IN SELECT drugs.drugs_id FROM drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration 
WHERE drugs.deleted=0 and drugs.prim=0 and drugs.previous=0 
and drugs.code not in ('A7','A8','A10','H1','H2','H3','J')  order by drugs.drugs_id asc
loop
	krow=krow+1;

	if count_rows.count_rows=krow then
		create_qu = create_qu || 'c' || drugs_id3.drugs_id || ' varchar(100),';
		create_qu = create_qu || 'm' || drugs_id3.drugs_id || ' varchar(100)';
	else
		create_qu = create_qu || 'c' || drugs_id3.drugs_id || ' varchar(100),';
		create_qu = create_qu || 'm' || drugs_id3.drugs_id || ' varchar(100),'; 
	end if;
end loop;


create_qu = create_qu || ');' ;
execute create_qu;


-- pedia gia to biobanking	11 pedia
create_first_qu = 'INSERT INTO statistics_lk5 VALUES ( 1,' || quote_literal('patient_biobanking_id') || ',' || quote_literal('biobanking_sample_id') || ',' || quote_literal('01-01-2019') || ',';
create_first_qu = create_first_qu || quote_literal('user_centre_id') || ',' || quote_literal('box') || ',' || quote_literal('availability') || ',' || quote_literal('protocol_descr') || ',';
create_first_qu = create_first_qu || quote_literal('codenbr') || ',' || quote_literal('patient_cohort_id') || ',' ;
create_first_qu = create_first_qu || quote_literal('patient_followup_id') || ',' || quote_literal('pbmcs') || ',' || quote_literal('pmns') || ',';

-- pat_id - date of birth-gender-main diagnosis-main diagnosis date-disease duration-RF-AntiCCP-ANA-cohort-followup month 13 pedia

create_first_qu = create_first_qu || quote_literal('pat id') || ',' || quote_literal('date of birth') || ',' || quote_literal('gender');
create_first_qu = create_first_qu || ',' || quote_literal('main diagnosis symptom_date') ;
create_first_qu = create_first_qu || ',' || quote_literal('main diagnosis disease_date') ;
create_first_qu = create_first_qu || ',' || quote_literal('main diagnosis icd10') ;
create_first_qu = create_first_qu || ',' || quote_literal('main diagnosis value') || ',' || quote_literal('disease duration') || ',' || quote_literal('RF') ;
create_first_qu = create_first_qu || ',' || quote_literal('AntiCCP') || ',' || quote_literal('ANA');
create_first_qu = create_first_qu || ',' || quote_literal('cohort') || ',' || quote_literal('followup');

-- DAS28-HAQ-BASDAI-ASDAS-BASFI-SLEDAI 2K-SLICC-SLEDAI21 High serum antidsDNA 9 pedia

create_first_qu = create_first_qu || ',' || quote_literal('DAS28') || ',' || quote_literal('HAQ') || ',' || quote_literal('BASDAI') ;
create_first_qu = create_first_qu || ',' || quote_literal('ASDAS-CRP') || ',' || quote_literal('ASDAS-ESR') || ',' || quote_literal('BASFI') || ',' || quote_literal('SLEDAI 2K') ;
create_first_qu = create_first_qu || ',' || quote_literal('SLICC') || ',' || quote_literal('SLEDAI21') || ',' ;

krow=0;

FOR count_rows IN SELECT count(drugs.drugs_id) as count_rows FROM drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration
	WHERE drugs.deleted=0 and drugs.prim=0 and drugs.previous=0 
	and drugs.code not in ('A7','A8','A10','H1','H2','H3','J')
LOOP
END LOOP;

FOR drugs_id4 IN SELECT REPLACE (coalesce(drugs.substance,''), ',', ' ') as substance,
 REPLACE (coalesce(lookup_tbl_val.value,''), ',', ' ') as route_of_administration_val 
 FROM drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration
 WHERE drugs.deleted=0 and drugs.prim=0 and drugs.previous=0 
and drugs.code not in ('A7','A8','A10','H1','H2','H3','J')  order by drugs.drugs_id asc
 LOOP
 
	krow = krow + 1;
	
	if count_rows.count_rows=krow then
		create_first_qu = create_first_qu ||  quote_literal('c' || '-' || drugs_id4.substance || '-' || drugs_id4.route_of_administration_val) ;
		create_first_qu = create_first_qu || ',';
		create_first_qu = create_first_qu ||  quote_literal('m' || '-' || drugs_id4.substance || '-' || drugs_id4.route_of_administration_val) ;
	else
		create_first_qu = create_first_qu ||  quote_literal('c' || '-' || drugs_id4.substance || '-' || drugs_id4.route_of_administration_val) ;
		create_first_qu = create_first_qu || ',';
		create_first_qu = create_first_qu ||  quote_literal('m' || '-' || drugs_id4.substance || '-' || drugs_id4.route_of_administration_val) ;
		create_first_qu = create_first_qu || ',';
	end if;
	
	
	
 end loop;

create_first_qu = create_first_qu || ');' ;
execute create_first_qu;

FOR patient_record IN select 
coalesce(TO_CHAR(patient.dateofbirth, 'DD-MM-YYYY'), '12-12-1900') AS dateofbirth,
coalesce(patient.gender,0) as gender,
coalesce(patient.centre,0) as user_centre_id,
coalesce(patient.patient_id,0) as patient_id,
coalesce(patient.pat_id,0) as pat_id,
patient_biobanking.patient_biobanking_id,
coalesce(val2.value,'') as biobanking_sample_id,
coalesce(TO_CHAR(patient_biobanking.biobanking_date, 'DD-MM-YYYY'), '12-12-1900') AS biobanking_date,
coalesce(patient_biobanking.box,'') as box,coalesce(patient_biobanking.availability,0) as availability,
coalesce(val1.value,'') as protocol_descr,
REPLACE(coalesce(patient_biobanking.codenbr,''), '/', '-' ) as codenbr,patient_biobanking.patient_cohort_id,patient_biobanking.patient_followup_id,
coalesce(patient_biobanking.pbmcs,0) as pbmcs,coalesce(patient_biobanking.pmns,0) as pmns,
coalesce(TO_CHAR(patient_diagnosis.symptom_date, 'DD-MM-YYYY'),'12-12-1900') AS symptom_date,
coalesce(TO_CHAR(patient_diagnosis.disease_date, 'DD-MM-YYYY'),'12-12-1900') AS disease_date,
REPLACE (coalesce(diagnosis.icd10,''), ',', ' ') as icd10,
REPLACE (coalesce(diagnosis.value,''), ',', ' ') as value,
coalesce(patient_diagnosis.disdurincl,'') as disdurincl,	
coalesce(rf.patient_antibodies_options,0) as rf_val,
coalesce(anticcp.patient_antibodies_options,0) as anticcp_val,
coalesce(ana.patient_antibodies_options,0) as ana_val,
coalesce(patient_followup.fumonthcohort,0) as fumonthcohort,
(coalesce(pat_coh_drugs.code,'') || ' - ' || coalesce(pat_coh_drugs.substance,'') || ' (' || coalesce(val3.value,'') ||')' ) as cohort,
coalesce(patient_common.das28esr,'') as das28,
coalesce(patient_common.haq,'') as haq,
coalesce(patient_spa.basdai1,'') as basdai,
coalesce(patient_spa.asdascrp,'') as asdascrp,
coalesce(patient_spa.asdasesr,'') as asdasesr,
coalesce(patient_spa.basfi,'') as basfi,
coalesce(patient_sle.sledai2k,0) as sledai2k4,
coalesce(patient_sle_severity.slicc42,'') as slicc,
coalesce(patient_sle.sledai21,0) as sledai21
from patient_biobanking 
left join patient on patient.pat_id=patient_biobanking.pat_id
left join lookup_tbl_val as val1 on patient_biobanking.protocol_descr=val1.id
left join lookup_tbl_val as val2 on patient_biobanking.biobanking_sample_id=val2.id
left join patient_diagnosis on patient_diagnosis.pat_id=patient_biobanking.pat_id
and patient_diagnosis.main=1 and patient_diagnosis.inactive=0 and patient_diagnosis.deleted=0
left join diagnosis on diagnosis.diagnosis_id=patient_diagnosis.diagnosis_id
left join patient_autoantibodies as rf on rf.pat_id=patient_biobanking.pat_id and rf.deleted=0 and rf.antibodies_id=65
left join patient_autoantibodies as anticcp on anticcp.pat_id=patient_biobanking.pat_id and anticcp.deleted=0 and anticcp.antibodies_id=66
left join patient_autoantibodies as ana on ana.pat_id=patient_biobanking.pat_id and ana.deleted=0 and ana.antibodies_id=67
left join patient_cohort on patient_cohort.patient_cohort_id=patient_biobanking.patient_cohort_id 
left join drugs as pat_coh_drugs on pat_coh_drugs.drugs_id=patient_cohort.cohort_name
left join lookup_tbl_val as val3 on pat_coh_drugs.route_of_administration=val3.id
left join patient_followup on patient_followup.patient_followup_id=patient_biobanking.patient_followup_id
left join patient_common on patient_common.patient_followup_id=patient_biobanking.patient_followup_id
left join patient_spa on patient_spa.patient_followup_id=patient_biobanking.patient_followup_id
left join patient_sle on patient_sle.patient_followup_id=patient_biobanking.patient_followup_id
left join patient_sle_severity on patient_sle_severity.patient_followup_id=patient_biobanking.patient_followup_id
where patient_biobanking.deleted=0

LOOP

krow2=krow2 + 1;

-- pedia gia to biobanking	11 pedia
qu = 'INSERT INTO statistics_lk5 VALUES ( ' || krow2 || ',' ;
qu = qu || quote_literal(patient_record.patient_biobanking_id) || ',' || quote_literal(patient_record.biobanking_sample_id) || ',' ;
qu = qu || quote_literal(patient_record.biobanking_date) || ',' || quote_literal(patient_record.user_centre_id) || ',' || quote_literal(patient_record.box) || ',' ;
qu = qu || quote_literal(patient_record.availability) || ',' || quote_literal(patient_record.protocol_descr) || ',' ;
qu = qu || quote_literal(patient_record.codenbr) || ',' || quote_literal(patient_record.patient_cohort_id) || ',' ;
qu = qu || quote_literal(patient_record.patient_followup_id) || ',' || quote_literal(patient_record.pbmcs) || ',' ;
qu = qu || quote_literal(patient_record.pmns) ||  ',' ;

-- pat_id - date of birth-gender-main diagnosis-main diagnosis date-disease duration-RF-AntiCCP-ANA-cohort-followup month 13 pedia
qu = qu || quote_literal(patient_record.patient_id) || ',' || quote_literal(patient_record.dateofbirth) || ',' ;
qu = qu || quote_literal(patient_record.gender) || ',' || quote_literal(patient_record.symptom_date) || ',' ;
qu = qu || quote_literal(patient_record.disease_date) || ',' || quote_literal(patient_record.icd10) || ',' ;
qu = qu || quote_literal(patient_record.value) || ',' || quote_literal(patient_record.disdurincl) || ',' ;
qu = qu || quote_literal(patient_record.rf_val) || ',' || quote_literal(patient_record.anticcp_val) || ',' ;
qu = qu || quote_literal(patient_record.ana_val) || ',' || quote_literal(patient_record.cohort) || ',' || quote_literal(patient_record.fumonthcohort) || ',' ;

-- DAS28-HAQ-BASDAI-ASDAS-BASFI-SLEDAI 2K-SLICC-SLEDAI21 High serum antidsDNA 9 pedia
qu = qu || quote_literal(patient_record.DAS28) || ',' || quote_literal(patient_record.HAQ) || ',' ;
qu = qu || quote_literal(patient_record.BASDAI) || ',' || quote_literal(patient_record.asdascrp) || ',' ;
qu = qu || quote_literal(patient_record.asdasesr) || ',' || quote_literal(patient_record.basfi) || ',' ;
qu = qu || quote_literal(patient_record.sledai2k4) || ',' || quote_literal(patient_record.slicc) || ',' ;
qu = qu || quote_literal(patient_record.sledai21) || ',' ;


	FOR count_rows IN SELECT count(drugs.drugs_id) as count_rows FROM drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration
	WHERE drugs.deleted=0 and drugs.prim=0 and drugs.previous=0 
	and drugs.code not in ('A7','A8','A10','H1','H2','H3','J')
	LOOP
	END LOOP;
	
	krow=0;
	
	FOR drugs_id3 IN SELECT drugs_id FROM drugs WHERE deleted=0 and prim=0 and previous=0 and code not in ('A7','A8','A10','H1','H2','H3','J')  order by drugs_id asc
	LOOP
	
	krow = krow + 1;
	
	for drugs_id4 in select count(*) as cnt	from patient_lookup_drugs where deleted= 0 and patient_cohort_id=patient_record.patient_cohort_id 
	and pat_id= patient_record.pat_id and drug_flag=1 and fumonth_cohort=patient_record.fumonthcohort and drugs_id= drugs_id3.drugs_id limit 1 
	LOOP
	END LOOP;
	
	
	
	if drugs_id4.cnt>0 then
		for mviews in select weekdosage::text as weekdosage,currentdosage::text as currentdosage from patient_lookup_drugs where deleted= 0 and patient_cohort_id=patient_record.patient_cohort_id 
		and pat_id= patient_record.pat_id and drug_flag=1 and fumonth_cohort=patient_record.fumonthcohort and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 
		LOOP
		
		
		if count_rows.count_rows=krow then
			if mviews.currentdosage is null then
				qu = qu || quote_literal('') || ',';
			else
				qu = qu || quote_literal(mviews.currentdosage) || ',';
			end if;
			
			if mviews.weekdosage is null then
				qu = qu || quote_literal('') ;
			else
				qu = qu || quote_literal(mviews.weekdosage) ;
			end if;
		else
			if mviews.currentdosage is null then
				qu = qu || quote_literal('') || ',';
			else
				qu = qu || quote_literal(mviews.currentdosage) || ',';
			end if;
			
			if mviews.weekdosage is null then
				qu = qu || quote_literal('') || ',' ;
			else
				qu = qu || quote_literal(mviews.weekdosage) || ',' ;
			end if;
		end if;
			
		END LOOP;
			
		else
			if count_rows.count_rows=krow then
				qu = qu || quote_literal('') || ',';
				qu = qu || quote_literal('');
			else
				qu = qu || quote_literal('') || ',';
				qu = qu || quote_literal('') || ',';
			end if;
		end if;

	END LOOP;

	

		

	

	qu = qu || ')';	

	execute qu;

END LOOP;

RETURN  1;

END; $BODY$;

ALTER FUNCTION public.create_insert_query_biobanking()
    OWNER TO postgres;
