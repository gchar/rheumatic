-- FUNCTION: public.select_detail_cohort(integer[], integer[], character varying[], character varying[], character varying[],character varying[], integer, integer, integer, date, date)

-- DROP FUNCTION public.select_detail_cohort(integer[], integer[], character varying[], character varying[], character varying[],character varying[], integer, integer, integer, date, date);

CREATE OR REPLACE FUNCTION public.select_detail_cohort(
	drugsin integer[],
	monthsin integer[],
	selectcommons character varying[],
	selectsles character varying[],
	selectcomor character varying[],
	selectrest character varying[],
	user_all_rpts integer,
	all_centre integer,
	user_centre_id integer,
	patient_cohort_startdate date,
	patient_cohort_enddate date)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100000
    VOLATILE 
AS $BODY$DECLARE



DECLARE

  drugnum RECORD;

  monthnum RECORD;

  commonnum RECORD;

  slenum RECORD;

  currentnum RECORD;
  comornumcomor1 RECORD;
  comornumcomor2 RECORD;
  comornumcomor3 RECORD;
  comornumcomor4 RECORD;
  comornumcomor5 RECORD;
  comornumcomor6 RECORD;
  drugs_count RECORD;
  sum_count int;
  
  krow int = 0;

  qu text; 



 BEGIN 

FOR drugs_count IN SELECT count(drugs_id) as count_rows FROM drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration
  WHERE drugs.deleted=0 and drugs.ongoing=0 and drugs.prim=0
    LOOP
	END LOOP;
	
	sum_count=drugs_count.count_rows+199;
	
qu = '(select statistics_lk1.id ';

FOR comornumcomor1 IN 1..39
LOOP
	qu = qu || ' ,statistics_lk1.a' || comornumcomor1;
END LOOP;

--FOR comornumcomor2 IN 178..248
--FOR comornumcomor2 IN 178..255
FOR comornumcomor2 IN 178..sum_count
LOOP
	qu = qu || ' ,statistics_lk1.a' || comornumcomor2;
END LOOP;

FOR comornumcomor5 IN 249..285 -- 1..37
LOOP
	qu = qu || ' ,statistics_lk_cohort_rest.a' || comornumcomor5;
END LOOP;


IF array_length(selectcomor, 1)>0 THEN
	FOR comornum IN 1..array_length(selectcomor, 1)
	LOOP
		qu = qu || ' ,statistics_lk1.a' || selectcomor[comornum];
		qu = qu || ' ,statistics_lk1.a' || (selectcomor[comornum]::INTEGER +1);
		
	END LOOP;
else

end if;

IF array_length(drugsin, 1)>0 OR array_length(selectcommons, 1)>0 OR array_length(selectsles, 1)>0 OR array_length(selectrest, 1)>0 THEN

	qu = qu || ',';

END IF;

	
	IF array_length(drugsin, 1)>0 THEN

		FOR currentnum IN 0..1

		LOOP

			FOR drugnum IN 1..array_length(drugsin, 1)

			LOOP

				FOR monthnum IN 1..array_length(monthsin, 1)

				LOOP

					krow=krow + 1;

						IF currentnum=0 THEN

							qu = qu || '(select distinct label from statistics_lk2b_val where ';

						ELSE

							qu = qu || '(select distinct label from statistics_lk2a_val where ';

						END IF;						

						qu = qu || ' drug_id = ' || drugsin[drugnum];

						qu = qu || ' and month = ' || monthsin[monthnum];

						qu = qu || ' and current = ' || currentnum;

						IF (currentnum=1 and monthnum = array_length(monthsin, 1) AND drugnum = array_length(drugsin, 1) AND  array_length(selectcommons, 1) is null AND array_length(selectsles, 1) is null  AND  array_length(selectrest, 1) is null) THEN

							qu = qu || ' limit 1) as lbl' || krow ;

						ELSE

							qu = qu || ' limit 1) as lbl' || krow || ',';

						END IF;

				END LOOP;

			END LOOP;

		END LOOP;

	END IF;
	
	IF array_length(selectcommons, 1)>0 THEN

		FOR commonnum IN 1..array_length(selectcommons, 1)

			LOOP

				FOR monthnum IN 1..array_length(monthsin, 1)

				LOOP

					krow=krow + 1;

						qu = qu || '(select  '  || selectcommons[commonnum] || '_' || monthsin[monthnum] || ' from statistics_lk3a' ;

						IF (monthnum = array_length(monthsin, 1) AND commonnum = array_length(selectcommons, 1) AND array_length(selectsles, 1) is null  AND  array_length(selectrest, 1) is null) THEN

							qu = qu || ' order by id asc limit 1) as lbl' || krow;

						ELSE

							qu = qu || ' order by id asc  limit 1) as lbl' || krow || ',';

						END IF;

				END LOOP;

			END LOOP;

	END IF;

	IF array_length(selectsles, 1)>0 THEN

		FOR slenum IN 1..array_length(selectsles, 1)

			LOOP

				FOR monthnum IN 1..array_length(monthsin, 1)

				LOOP

					krow=krow + 1;

						qu = qu || '(select ' || selectsles[slenum] || '_' || monthsin[monthnum] || ' from statistics_lk3b'  ;

						IF (monthnum = array_length(monthsin, 1) AND slenum = array_length(selectsles, 1)  AND  array_length(selectrest, 1) is null) THEN

							qu = qu || ' order by id asc limit 1) as lbl' || krow;

						ELSE

							qu = qu || ' order by id asc limit 1) as lbl' || krow || ',';

						END IF;

				END LOOP;

			END LOOP;

	END IF;

	IF array_length(selectrest, 1)>0 THEN

		FOR slenum IN 1..array_length(selectrest, 1)

			LOOP

				FOR monthnum IN 1..array_length(monthsin, 1)

				LOOP

					krow=krow + 1;

						qu = qu || '(select ' || selectrest[slenum] || '_' || monthsin[monthnum] || ' from statistics_lk_cohort_rest'  ;

						IF (monthnum = array_length(monthsin, 1) AND slenum = array_length(selectrest, 1)) THEN

							qu = qu || ' order by id asc limit 1) as lbl' || krow;

						ELSE

							qu = qu || ' order by id asc limit 1) as lbl' || krow || ',';

						END IF;

				END LOOP;

			END LOOP;

	END IF;
	
	qu = qu || ' from statistics_lk1 join statistics_lk_cohort_rest on statistics_lk1.id=statistics_lk_cohort_rest.id order by statistics_lk1.id asc limit 1  )';

	
qu = qu || ' union ';

qu = qu || '(select statistics_lk1.id ';

FOR comornumcomor3 IN 1..39
LOOP
	qu = qu || ' ,statistics_lk1.a' || comornumcomor3;
END LOOP;

--FOR comornumcomor4 IN 178..248
--FOR comornumcomor4 IN 178..255
FOR comornumcomor4 IN 178..sum_count
LOOP
	qu = qu || ' ,statistics_lk1.a' || comornumcomor4;
END LOOP;

FOR comornumcomor6 IN 249..285 -- 1..37
LOOP
	qu = qu || ' ,statistics_lk_cohort_rest.a' || comornumcomor6;
END LOOP;

IF array_length(selectcomor, 1)>0 THEN
	FOR comornum IN 1..array_length(selectcomor, 1)
	LOOP
		qu = qu || ' ,statistics_lk1.a' || selectcomor[comornum];
		qu = qu || ' ,statistics_lk1.a' || (selectcomor[comornum]::INTEGER +1);
	END LOOP;
else

end if;


--IF array_length(drugsin, 1)>0 OR array_length(selectcommons, 1)>0 OR array_length(selectsles, 1)>0 OR array_length(selectrest, 1)>0 THEN

--	qu = qu || ',';

--END IF;

	IF array_length(drugsin, 1)>0 THEN

		FOR currentnum IN 0..1

		LOOP

			FOR drugnum IN 1..array_length(drugsin, 1)

			LOOP

				FOR monthnum IN 1..array_length(monthsin, 1)

				LOOP

					krow=krow + 1;

						

						IF currentnum=0 THEN 

							qu = qu || ',statistics_lk2b.' || 'm';

						ELSE

							qu = qu || ',statistics_lk2a.' || 'c';

						END IF;

						qu = qu ||  drugsin[drugnum];

						qu = qu || '_' || monthsin[monthnum];

				END LOOP;

			END LOOP;

		END LOOP;

	END IF;

	IF array_length(selectcommons, 1)>0 THEN

		FOR commonnum IN 1..array_length(selectcommons, 1)

			LOOP

				FOR monthnum IN 1..array_length(monthsin, 1)

				LOOP

					krow=krow + 1;

					qu = qu || ',statistics_lk3a.'  || selectcommons[commonnum] || '_' || monthsin[monthnum] ;

				END LOOP;

			END LOOP;

	END IF;
	
	IF array_length(selectsles, 1)>0 THEN

		FOR slenum IN 1..array_length(selectsles, 1)

			LOOP

				FOR monthnum IN 1..array_length(monthsin, 1)

				LOOP

					krow=krow + 1;

					qu = qu || ',statistics_lk3b.'  || selectsles[slenum] || '_' || monthsin[monthnum] ;

				END LOOP;

			END LOOP;

	END IF;

	IF array_length(selectrest, 1)>0 THEN

		FOR slenum IN 1..array_length(selectrest, 1)

			LOOP

				FOR monthnum IN 1..array_length(monthsin, 1)

				LOOP

					krow=krow + 1;

					qu = qu || ',statistics_lk_cohort_rest.'  || selectrest[slenum] || '_' || monthsin[monthnum] ;

				END LOOP;

			END LOOP;

	END IF;

  
  
	qu = qu || ' from statistics_lk1 join statistics_lk2a on statistics_lk1.a1::int=statistics_lk2a.cohort_id join statistics_lk2b on statistics_lk1.a1::int=statistics_lk2b.cohort_id ';

	qu = qu || ' join statistics_lk3a on statistics_lk1.a1::int=statistics_lk3a.patient_cohort_id ';

	qu = qu || ' join statistics_lk3b on statistics_lk1.a1::int=statistics_lk3b.patient_cohort_id ';
	
	qu = qu || ' join statistics_lk_cohort_rest on statistics_lk1.a1::int=statistics_lk_cohort_rest.cohort_id  ';

	qu = qu || ' where statistics_lk1.id<>1';

	

	IF user_all_rpts=1 THEN

		IF all_centre=0 THEN

			qu = qu || ' and statistics_lk1.a7::int=' || user_centre_id ;

		END IF;

	ELSE

		qu = qu || ' and statistics_lk1.a7::int=' || user_centre_id;

	END IF;

				

	IF patient_cohort_enddate IS NOT NULL or patient_cohort_startdate IS NOT NULL THEN

		qu = qu || ' and ';

	END IF;		

	

	IF patient_cohort_startdate IS NOT NULL THEN

		qu = qu || ' statistics_lk1.a10 >= ' || quote_literal(patient_cohort_startdate);

	END IF;

	

	IF patient_cohort_enddate IS NOT NULL THEN

		IF patient_cohort_startdate IS NOT NULL THEN

			qu = qu || ' and statistics_lk1.a10 <= ' || quote_literal(patient_cohort_enddate);

		ELSE

			qu = qu || ' statistics_lk1.a10 <= ' || quote_literal(patient_cohort_enddate);

		END IF;

	END IF;

	

	qu = qu || ' order by statistics_lk1.id asc)  order by id asc';



RETURN  qu AS selectquery;



END; 



$BODY$;

ALTER FUNCTION public.select_detail_cohort(integer[], integer[], character varying[], character varying[], character varying[],character varying[], integer, integer, integer, date, date)
    OWNER TO postgres;
