-- FUNCTION: public.create_insert_query_event_rest()

-- DROP FUNCTION public.create_insert_query_event_rest();

CREATE OR REPLACE FUNCTION public.create_insert_query_event_rest(
	)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100000
    VOLATILE 
AS $BODY$DECLARE
  
  select_tbl_cnt int;
  insert_tbl_cnt int;
  cnt int = 0;
  patient_record RECORD;
  ra_record RECORD;
  spa_record RECORD;
  pulmonary_record RECORD;
  sda_asas_record RECORD;
  axspa_cnt RECORD;
  axspa RECORD;
  dflt character='';
  ra_cnt RECORD;
  ra1 RECORD;
  ra2 RECORD;
  ra3 RECORD;
  spa_cnt RECORD;
  spa1 RECORD;
  spa2 RECORD;
  spa3 RECORD;
  drugs_id3 RECORD;
  drugs_id4 RECORD;
  mviews RECORD;
  dis_act RECORD;
  create_qu text;
  qu text; 
  create_first_qu text;
  
  

 BEGIN 

 execute 'DROP TABLE IF EXISTS statistics_lk_event_rest;';
 
 
 -- CREATE TABLE statistics_lk_event_rest
 
create_qu = 'CREATE TABLE statistics_lk_event_rest (id integer,cohort_id varchar,event_id varchar';
FOR select_tbl_cnt IN 1..45 LOOP 
	create_qu = create_qu || ',a' || select_tbl_cnt || ' varchar';
end loop;

FOR select_tbl_cnt IN 1..69 LOOP 
	create_qu = create_qu || ',comor' || select_tbl_cnt || ' varchar';
	create_qu = create_qu || ',comor_date' || select_tbl_cnt || ' varchar';
end loop;

create_qu = create_qu || ');' ;

execute create_qu;

 -- Insert the first record with labels
  
create_first_qu = 'INSERT INTO statistics_lk_event_rest VALUES ( 1,' || quote_literal('cohort_id') || ',' || quote_literal('event_id') || ',';
-- Demographics 8 fields
create_first_qu = create_first_qu || quote_literal('Ethnicity') || ',' || quote_literal('Year_of_education') || ',' || quote_literal('Residence') || ',' || quote_literal('Employment') || ',' ;
create_first_qu = create_first_qu || quote_literal('Marital_status') || ',' || quote_literal('Physical_activity') || ',' || quote_literal('Medit_diet_score') || ',' || quote_literal('Alcohol_cons') || ',' ;
-- Extraarticular RA 6 fields
create_first_qu = create_first_qu || quote_literal('ra_val_1') || ',' || quote_literal('ra_date_1') || ',' || quote_literal('ra_val_2') || ',' || quote_literal('ra_date_2') || ',' ;
create_first_qu = create_first_qu || quote_literal('ra_val_3') || ',' || quote_literal('ra_date_3') || ',' ;
-- Extraarticular SpA 6 fields
create_first_qu = create_first_qu || quote_literal('spa_val_1') || ',' || quote_literal('spa_date_1') || ',' || quote_literal('spa_val_2') || ',' || quote_literal('spa_date_2') || ',' ;
create_first_qu = create_first_qu || quote_literal('spa_val_3') || ',' || quote_literal('spa_date_3') || ',' ;
-- Pulmonary involvement 9 fields
create_first_qu = create_first_qu || quote_literal('pul_inv_group_1') || ',' || quote_literal('pul_inv_val_1') || ',' || quote_literal('pul_inv_date_1') || ',' ;
create_first_qu = create_first_qu || quote_literal('pul_inv_group_2') || ',' || quote_literal('pul_inv_val_2') || ',' || quote_literal('pul_inv_date_2') || ',' ;
create_first_qu = create_first_qu || quote_literal('pul_inv_group_3') || ',' || quote_literal('pul_inv_val_3') || ',' || quote_literal('pul_inv_date_3') || ',' ;
-- Add disease activity 16 field
create_first_qu = create_first_qu || quote_literal('DAS38-ESR_FU0') || ',' || quote_literal('SDAI_FU0') || ',' || quote_literal('CDAI_FU0') || ',' || quote_literal('MHAQ_FU0') || ',' || quote_literal('ASDAS-CRP_FU0') || ',' ;
create_first_qu = create_first_qu || quote_literal('BSDAI_FU0') || ',' || quote_literal('SLEDAI-2K_FU0') || ',' || quote_literal('SLICC_ACR_damage_index_FU0') || ',';
create_first_qu = create_first_qu || quote_literal('DAS38-ESR_cur_fu') || ',' || quote_literal('SDAI_cur_fu') || ',' || quote_literal('CDAI_cur_fu') || ',' || quote_literal('MHAQ_cur_fu') || ',' || quote_literal('ASDAS-CRP_cur_fu') || ',' ;
create_first_qu = create_first_qu || quote_literal('BSDAI_cur_fu') || ',' || quote_literal('SLEDAI-2K_cur_fu') || ',' || quote_literal('SLICC_ACR_damage_index_cur_fu') || ',';

-- comorbidities diagnoses 138 fields

create_first_qu = create_first_qu || quote_literal('Aortic_Aneurysm') || ',' || quote_literal('Aortic_Aneurysm date') || ',' || quote_literal('Cardiac_arrhythmia') || ',' || quote_literal('Cardiac_arrhythmia date') || ',';
create_first_qu = create_first_qu || quote_literal('Congestive_Heart_Failure') || ',' || quote_literal('Congestive_Heart_Failure date') || ',' || quote_literal('Coronary_Heart_Disease') || ',' || quote_literal('Coronary_Heart_Disease date') ;
create_first_qu = create_first_qu || ',' || quote_literal('Acute_myocardial_infarction') || ',' || quote_literal('Acute_myocardial_infarction date') || ',' || quote_literal('Angina_pectoris') || ',' || quote_literal('Angina_pectoris date') || ',' || quote_literal('Atrial_fibrillation') || ',' || quote_literal('Atrial_fibrillation date') ;
create_first_qu = create_first_qu || ',' || quote_literal('Pericardial_disease_Acute_Pericarditis') || ',' || quote_literal('Pericardial_disease_Acute_Pericarditis date') || ',' || quote_literal('Hypertension') || ',' || quote_literal('Hypertension date') || ',';
create_first_qu = create_first_qu || quote_literal('Cardiomyopathies') || ',' || quote_literal('Cardiomyopathies date') || ',' || quote_literal('Peripheral_vascular_disease') || ',' || quote_literal('Peripheral_vascular_disease date') || ',' ||  quote_literal('Valvular_Heart_Disease') || ',' ||  quote_literal('Valvular_Heart_Disease date') ;
create_first_qu = create_first_qu || ',' || quote_literal('Ischemic_strokeTIA') || ',' || quote_literal('Ischemic_strokeTIA date') || ',' || quote_literal('Sequelae_of_ischemic_stroke') || ',' || quote_literal('Sequelae_of_ischemic_stroke date') || ',' || quote_literal('Hemorrhagic_stroke') || ',' || quote_literal('Hemorrhagic_stroke date') ;
create_first_qu = create_first_qu || ',' || quote_literal('Sequelae_of_hemorrhagic_stroke') || ',' || quote_literal('Sequelae_of_hemorrhagic_stroke date') || ',';
create_first_qu = create_first_qu || quote_literal('Anxiety_disorders') || ',' || quote_literal('Anxiety_disorders date') || ',' || quote_literal('Bipolar_disorder') || ',' || quote_literal('Bipolar_disorder date') || ',' || quote_literal('Dementia_unspecified') || ','  || quote_literal('Dementia_unspecified date') || ',' ;
create_first_qu = create_first_qu || quote_literal('Major_depression_without_psychotic_symptoms') || ',' || quote_literal('Major_depression_without_psychotic_symptoms date') || ',' || quote_literal('Other_depressive_episodes') || ',' || quote_literal('Other_depressive_episodes_date') || ',' || quote_literal('Multiple_Sclerosis_Demyelinating_disease') || ',' || quote_literal('Multiple_Sclerosis_Demyelinating_disease date') || ',';
create_first_qu = create_first_qu || quote_literal('Parkinsons_disease') || ',' || quote_literal('Parkinsons_disease date') || ',' || quote_literal('Peripheral_Neuropathy') || ',' || quote_literal('Peripheral_Neuropathy date') || ',' || quote_literal('Psychosis') || ','  || quote_literal('Psychosis date') || ',';
create_first_qu = create_first_qu || quote_literal('Schizophrenia') || ',' || quote_literal('Schizophrenia date') || ',' || quote_literal('Asthma') || ',' || quote_literal('Asthma date') || ',' || quote_literal('Bronchiectasis') || ',' || quote_literal('Bronchiectasis_date') || ',' || quote_literal('Chronic_obstructive_pulmonary_disease') || ',' || quote_literal('Chronic_obstructive_pulmonary_disease_date') || ',' || quote_literal('Chronic_sinusitis') || ',' || quote_literal('Chronic_sinusitis date') || ',';
create_first_qu = create_first_qu || quote_literal('Interstitial_Lung_Disease') || ',' || quote_literal('Interstitial_Lung_Disease date') || ',' || quote_literal('Pulmonary_Hypertension') || ',' || quote_literal('Pulmonary_Hypertension date') || ',' || quote_literal('Autoimmune_hepatitis') || ',' || quote_literal('Autoimmune_hepatitis date') ;
create_first_qu = create_first_qu || ',' || quote_literal('Alcoholic_liver_disease') || ',' || quote_literal('Alcoholic_liver_disease date') || ',' || quote_literal('Alcoholic_liver_cirrhosis') || ',' || quote_literal('Alcoholic_liver_cirrhosis date') || ',' ||  quote_literal('Nonalcoholic_fatty_liver_disease_NAFLD') || ',' ||  quote_literal('Nonalcoholic_fatty_liver_disease_NAFLD date') || ',';
create_first_qu = create_first_qu || quote_literal('Crohn’s_disease') || ',' || quote_literal('Crohn’s_disease date') || ',';
create_first_qu = create_first_qu || quote_literal('Diverticulosis') || ',' || quote_literal('Diverticulosis date') || ',' || quote_literal('EsophagitisGERD') || ',' || quote_literal('EsophagitisGERD date') || ',' || quote_literal('Peptic_ulcer_disease_biopsy_proven') || ','  || quote_literal('Peptic_ulcer_disease_biopsy_proven date') || ',';
create_first_qu = create_first_qu || quote_literal('Diabetes_mellitus') || ',' || quote_literal('Diabetes_mellitus date') || ',' || quote_literal('Dyslipidaemia') || ',' || quote_literal('Dyslipidaemia date') || ',' || quote_literal('Hypothyroidism') || ',' || quote_literal('Hypothyroidism_date') || ',' || quote_literal('Hyperthyroidism') || ',' || quote_literal('Hyperthyroidism date') ;
create_first_qu = create_first_qu || ',' || quote_literal('Obesity') || ',' || quote_literal('Obesity date') || ',' || quote_literal('Chronic_kidney_disease_stage_III_GFR60') || ',' || quote_literal('Chronic_kidney_disease_stage_III_GFR60 date') || ',';
create_first_qu = create_first_qu || quote_literal('Chronic_kidney_disease_stage_IIIIV_(GFR_15-59)') || ',' || quote_literal('Chronic_kidney_disease_stage_IIIIV_(GFR_15-59) date') || ',' || quote_literal('End-stage_renal_disease_GFR_15') || ',' || quote_literal('End-stage_renal_disease_GFR_15 date') || ',' || quote_literal('Hodgkin_Lymphoma')  || ',' || quote_literal('Hodgkin_Lymphoma date');
create_first_qu = create_first_qu || ',' || quote_literal('Non-Hodgkin_Lymphoma_unspecified') || ',' || quote_literal('Non-Hodgkin_Lymphoma_unspecified date') || ',' || quote_literal('Leukemia') || ',' || quote_literal('Leukemia date') || ',';
create_first_qu = create_first_qu || quote_literal('Solid_tumor') || ',' || quote_literal('Solid_tumor date') || ',' || quote_literal('Solid_tumor_without_metastasi') || ',' || quote_literal('Solid_tumor_without_metastasi date') || ',' || quote_literal('Metastatic_solid_tumor') || ','  || quote_literal('Metastatic_solid_tumor date') || ',' ;
create_first_qu = create_first_qu || quote_literal('Metastatic_cancer_without_known_primary_tumor_site') || ',' || quote_literal('Metastatic_cancer_without_known_primary_tumor_site date') || ',' || quote_literal('Ischemic_Optic_Neuropathy')  || ',' || quote_literal('Ischemic_Optic_Neuropathy date') || ',' || quote_literal('Optic_Neuritis')  || ',' || quote_literal('Optic_Neuritis date') ;
create_first_qu = create_first_qu || ',' || quote_literal('Chronic_osteomyelitis') || ','  || quote_literal('Chronic_osteomyelitis date') || ',' ;
create_first_qu = create_first_qu || quote_literal('Latent_TB_infection') || ',' || quote_literal('Latent_TB_infection date') || ',' || quote_literal('Viral_hepatitis') || ',' || quote_literal('Viral_hepatitis date') || ',' || quote_literal('Chronic_viral_infection_other') || ','  || quote_literal('Chronic_viral_infection_other date') || ',';
create_first_qu = create_first_qu || quote_literal('Past_hospitalization_for_serious_infection') || ',' || quote_literal('Past_hospitalization_for_serious_infection date') || ',' || quote_literal('Recurrent_past_hospitalizations_for_serious_infections') || ',' || quote_literal('Recurrent_past_hospitalizations_for_serious_infections date') || ',' || quote_literal('History_of_opportunistic_infection') || ',' || quote_literal('History_of_opportunistic_infection date') || ',';
create_first_qu = create_first_qu || quote_literal('Prosthetic_left_hip_joint') || ',' || quote_literal('Prosthetic_left_hip_joint date') || ',' || quote_literal('Prosthetic_right_hip_joint') || ',' || quote_literal('Prosthetic_right_hip_joint date') || ',' || quote_literal('Prosthetic_left_knee_joint')  || ',' || quote_literal('Prosthetic_left_knee_joint date') ;
create_first_qu = create_first_qu || ',' || quote_literal('Prosthetic_right_knee_joint') || ',' || quote_literal('Prosthetic_right_knee_joint date') || ',' || quote_literal('Prosthetic_joint_other') || ',' || quote_literal('Prosthetic_joint_other date');

	create_first_qu = create_first_qu || ');' ;
	execute create_first_qu;
 
-- Insert the records
 
 FOR patient_record IN select distinct coalesce(patient.patient_id,0) as patient_id,coalesce(patient_cohort.pat_id,0) as pat_id,coalesce(patient_followup.fumonthcohort,0) as fumonthcohort,
	coalesce(patient_cohort.patient_cohort_id,0) as patient_cohort_id, coalesce(patient_event.patient_event_id,0) as patient_event_id, coalesce(patient_event.patient_followup_id,0) as patient_followup_id,
	-- demographics patient
	coalesce(patient.ethnicity,0) as ethnicity,coalesce(patient.yearsofeduc,0) as yearsofeduc,
	-- demographics patient
	coalesce(patient_cohort.residence,0) as residence,coalesce(patient_cohort.employment_at_cohort,0) as employment,
	coalesce(patient_cohort.physical_status,0) as physical_status,coalesce(patient_cohort.marital_status,0) as marital_status,
	coalesce(patient_cohort.mediterranean_diet,'') as mediterranean_diet,coalesce(patient_cohort.alcohol_consumption,'') as alcohol_consumption
	from patient_event 
	left join patient_followup on patient_event.patient_followup_id=patient_followup.patient_followup_id 
	left join patient_cohort on patient_event.patient_cohort_id=patient_cohort.patient_cohort_id 
	left join patient on patient.pat_id=patient_cohort.pat_id 
	where patient.deleted=0 and patient_cohort.deleted=0
	--limit 50
	--and patient_cohort.patient_cohort_id=4150
	
	loop
		
		qu = 'INSERT INTO statistics_lk_event_rest VALUES ( 1,' || quote_literal(patient_record.patient_cohort_id) || ',' || quote_literal(patient_record.patient_event_id) || ',';
		-- Demographics 8 fields
		qu = qu || quote_literal(patient_record.ethnicity) || ',' || quote_literal(patient_record.yearsofeduc) || ',' || quote_literal(patient_record.residence) || ',' || quote_literal(patient_record.employment) || ',' ;
		qu = qu || quote_literal(patient_record.marital_status) || ',' || quote_literal(patient_record.physical_status) || ',' || quote_literal(patient_record.mediterranean_diet) || ',' || quote_literal(patient_record.alcohol_consumption) || ',' ;
		-- Extraarticular RA 6 fields
		FOR ra_cnt IN select count(patient_ra_extraarticular_id) as cnt from patient_ra_extraarticular 
		where deleted=0 and pat_id=patient_record.pat_id 
		LOOP
		END LOOP;
		
			IF ra_cnt.cnt=0 then
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			END IF;	
			
			if ra_cnt.cnt=1 then
				FOR ra1 IN select coalesce(lookup_tbl_val.value,'') as ra_value,coalesce(TO_CHAR(patient_ra_extraarticular.date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_ra_extraarticular left join lookup_tbl_val on patient_ra_extraarticular.ra_extraarticular_id=lookup_tbl_val.id 
				  where patient_ra_extraarticular.deleted=0 and pat_id=patient_record.pat_id order by patient_ra_extraarticular.patient_ra_extraarticular_id desc limit 1
				  LOOP
					qu = qu || quote_literal(ra1.ra_value) || ',' || quote_literal(ra1.date_str) || ',' ;
				  END LOOP;
				  
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			end if;	
			
			if ra_cnt.cnt=2 then
				FOR ra2 IN select coalesce(lookup_tbl_val.value,'') as ra_value,coalesce(TO_CHAR(patient_ra_extraarticular.date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_ra_extraarticular left join lookup_tbl_val on patient_ra_extraarticular.ra_extraarticular_id=lookup_tbl_val.id 
				  where patient_ra_extraarticular.deleted=0 and pat_id=patient_record.pat_id order by patient_ra_extraarticular.patient_ra_extraarticular_id desc limit 2
				  LOOP
					qu = qu || quote_literal(ra2.ra_value) || ',' || quote_literal(ra2.date_str) || ',' ;
				  end loop;
				  
				  qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			end if;	  
			
			if ra_cnt.cnt>=3 then
				FOR ra3 IN select coalesce(lookup_tbl_val.value,'') as ra_value,coalesce(TO_CHAR(patient_ra_extraarticular.date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_ra_extraarticular left join lookup_tbl_val on patient_ra_extraarticular.ra_extraarticular_id=lookup_tbl_val.id 
				  where patient_ra_extraarticular.deleted=0 and pat_id=patient_record.pat_id order by patient_ra_extraarticular.patient_ra_extraarticular_id desc limit 3
				  LOOP
					qu = qu || quote_literal(ra3.ra_value) || ',' || quote_literal(ra3.date_str) || ',' ;
				  END LOOP;
			end if;
			
		
		-- Extraarticular SpA 6 fields
		FOR spa_cnt IN select count(patient_spa_extraarticular_id) as cnt from patient_spa_extraarticular 
		where deleted=0 and pat_id=patient_record.pat_id 
		LOOP
		END LOOP;
		
			IF spa_cnt.cnt=0 then
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			END IF;	
			
			if spa_cnt.cnt=1 then
				FOR spa1 IN select replace(coalesce(lookup_tbl_val.value,''), ',', '') as spa_value,coalesce(TO_CHAR(patient_spa_extraarticular.date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_spa_extraarticular left join lookup_tbl_val on patient_spa_extraarticular.spa_extraarticular_id=lookup_tbl_val.id 
				  where patient_spa_extraarticular.deleted=0 and pat_id=patient_record.pat_id order by patient_spa_extraarticular.patient_spa_extraarticular_id desc limit 1
				  LOOP
					qu = qu || quote_literal(spa1.spa_value) || ',' || quote_literal(spa1.date_str) || ',' ;
				  END LOOP;
				  
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			end if;	
			
			if spa_cnt.cnt=2 then
				FOR spa2 IN select replace(coalesce(lookup_tbl_val.value,''), ',', '') as spa_value,coalesce(TO_CHAR(patient_spa_extraarticular.date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_spa_extraarticular left join lookup_tbl_val on patient_spa_extraarticular.spa_extraarticular_id=lookup_tbl_val.id 
				  where patient_spa_extraarticular.deleted=0 and pat_id=patient_record.pat_id order by patient_spa_extraarticular.patient_spa_extraarticular_id desc limit 2
				  LOOP
					qu = qu || quote_literal(spa2.spa_value) || ',' || quote_literal(spa2.date_str) || ',' ;
				  end loop;
				  
				  qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			end if;	  
			
			if spa_cnt.cnt>=3 then
				FOR spa3 IN select replace(coalesce(lookup_tbl_val.value,''), ',', '') as spa_value,coalesce(TO_CHAR(patient_spa_extraarticular.date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_spa_extraarticular left join lookup_tbl_val on patient_spa_extraarticular.spa_extraarticular_id=lookup_tbl_val.id 
				  where patient_spa_extraarticular.deleted=0 and pat_id=patient_record.pat_id order by patient_spa_extraarticular.patient_spa_extraarticular_id desc limit 3
				  LOOP
					qu = qu || quote_literal(spa3.spa_value) || ',' || quote_literal(spa3.date_str) || ',' ;
				  END LOOP;
			end if;
		
		-- Pulmonary involvement 9 fields
		FOR spa_cnt IN select count(patient_pulmonary_id) as cnt from patient_pulmonary 
		where deleted=0 and pat_id=patient_record.pat_id 
		LOOP
		END LOOP;
		
			IF spa_cnt.cnt=0 then
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			END IF;	
			
			if spa_cnt.cnt=1 then
				FOR spa1 IN select coalesce(group_val.value,'') as group_value,coalesce(content_val.value,'') as content_value,coalesce(TO_CHAR(patient_pulmonary.pulmonary_date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_pulmonary 
				  left join lookup_tbl_val as group_val on patient_pulmonary.group_id=group_val.id
				  left join lookup_tbl_val as content_val on patient_pulmonary.content=content_val.id 
				  where patient_pulmonary.deleted=0 and pat_id=patient_record.pat_id order by patient_pulmonary.patient_pulmonary_id desc limit 1
				  LOOP
					qu = qu || quote_literal(spa1.group_value) || ',' || quote_literal(spa1.content_value) || ','  || quote_literal(spa1.date_str) || ',' ;
				  END LOOP;
				  
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			end if;	
			
			if spa_cnt.cnt=2 then
				FOR spa2 IN  select coalesce(group_val.value,'') as group_value,coalesce(content_val.value,'') as content_value,coalesce(TO_CHAR(patient_pulmonary.pulmonary_date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_pulmonary 
				  left join lookup_tbl_val as group_val on patient_pulmonary.group_id=group_val.id
				  left join lookup_tbl_val as content_val on patient_pulmonary.content=content_val.id 
				  where patient_pulmonary.deleted=0 and pat_id=patient_record.pat_id order by patient_pulmonary.patient_pulmonary_id desc limit 2
				  LOOP
					qu = qu || quote_literal(spa2.group_value) || ',' || quote_literal(spa2.content_value) || ','  || quote_literal(spa2.date_str) || ',' ;
				  end loop;
				  
				  qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			end if;	  
			
			if spa_cnt.cnt>=3 then
				FOR spa3 IN select coalesce(group_val.value,'') as group_value,coalesce(content_val.value,'') as content_value,coalesce(TO_CHAR(patient_pulmonary.pulmonary_date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_pulmonary 
				  left join lookup_tbl_val as group_val on patient_pulmonary.group_id=group_val.id
				  left join lookup_tbl_val as content_val on patient_pulmonary.content=content_val.id 
				  where patient_pulmonary.deleted=0 and pat_id=patient_record.pat_id order by patient_pulmonary.patient_pulmonary_id desc limit 3
				  LOOP
					qu = qu || quote_literal(spa3.group_value) || ',' || quote_literal(spa3.content_value) || ','  || quote_literal(spa3.date_str) || ',' ;
				  END LOOP;
			end if;
			
		
		-- Disease activity  16 field
		  
		  FOR dis_act IN select coalesce(patient_common.das28esr,'') as das28esr,coalesce(patient_common.sdai,'') as sdai,coalesce(patient_common.cdai,'') as cdai,
		  coalesce(patient_common.mhaq,'') as mhaq,coalesce(patient_spa.asdascrp,'') as asdascrp,coalesce(patient_spa.basdai,'') as basdai
		  ,coalesce(patient_sle.sledai2k,0) as sledai2k,coalesce(patient_sle_severity.slicc42,'') as sliccacr
		  from patient_cohort
		  left join patient_followup on patient_cohort.patient_cohort_id=patient_followup.patient_cohort_id
		  left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id
		  left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id
		  left join patient_sle on patient_sle.patient_followup_id=patient_followup.patient_followup_id
		  left join patient_sle_severity on patient_sle_severity.patient_followup_id=patient_followup.patient_followup_id
		  where patient_followup.deleted=0 and patient_cohort.pat_id=patient_record.pat_id and patient_cohort.patient_cohort_id=patient_record.patient_cohort_id 
		  and patient_followup.fumonthcohort=0 limit 1
		  LOOP
			qu = qu || quote_literal(dis_act.das28esr) || ',' || quote_literal(dis_act.sdai) || ','  || quote_literal(dis_act.cdai) || ',' ;
			qu = qu || quote_literal(dis_act.mhaq) || ',' || quote_literal(dis_act.asdascrp) || ','  || quote_literal(dis_act.basdai) || ',' ;
			qu = qu || quote_literal(dis_act.sledai2k) || ',' || quote_literal(dis_act.sliccacr) || ',' ;
		  END LOOP;
		
		FOR dis_act IN select coalesce(patient_common.das28esr,'') as das28esr,coalesce(patient_common.sdai,'') as sdai,coalesce(patient_common.cdai,'') as cdai,
		  coalesce(patient_common.mhaq,'') as mhaq,coalesce(patient_spa.asdascrp,'') as asdascrp,coalesce(patient_spa.basdai,'') as basdai
		  ,coalesce(patient_sle.sledai2k,0) as sledai2k,coalesce(patient_sle_severity.slicc42,'') as sliccacr
		  from patient_cohort
		  left join patient_followup on patient_cohort.patient_cohort_id=patient_followup.patient_cohort_id
		  left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id
		  left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id
		  left join patient_sle on patient_sle.patient_followup_id=patient_followup.patient_followup_id
		  left join patient_sle_severity on patient_sle_severity.patient_followup_id=patient_followup.patient_followup_id
		  where patient_followup.deleted=0 and patient_cohort.pat_id=patient_record.pat_id and patient_cohort.patient_cohort_id=patient_record.patient_cohort_id 
		  and patient_followup.fumonthcohort=patient_record.fumonthcohort limit 1
		  LOOP
			qu = qu || quote_literal(dis_act.das28esr) || ',' || quote_literal(dis_act.sdai) || ','  || quote_literal(dis_act.cdai) || ',' ;
			qu = qu || quote_literal(dis_act.mhaq) || ',' || quote_literal(dis_act.asdascrp) || ','  || quote_literal(dis_act.basdai) || ',' ;
			qu = qu || quote_literal(dis_act.sledai2k) || ',' || quote_literal(dis_act.sliccacr) || ',' ;
		  END LOOP;
		
		-- comorbidities diagnoses
			FOR drugs_id3 IN select diseases_comorbidities_id from diseases_comorbidities 
		  where diseases_comorbidities_id in (1,2,3,4,5,6,7,8,9,11,12,14,17,18,19,20,23,24,28,34,35,40,41,42,43,44,50
		  ,51,52,53,54,55,56,57,58,59,61,64,65,69,74,75,76,78,80,81,82,83,85,86,87,88,89,90,91,98,100,106,107,108,109
		  ,110,111,112,116,117,118,119,120)
		  order by diseases_comorbidities_id asc
		  LOOP
			  FOR mviews IN select count(*) as cnt from nonrheumatic_diseases where pat_id=patient_record.pat_id 
				  and deleted=0 and diseases_comorbidities_id=drugs_id3.diseases_comorbidities_id
				  LOOP
					IF mviews.cnt > 0 THEN
						 FOR drugs_id4 IN select coalesce(TO_CHAR(date_of_diagnosis, 'DD-MM-YYYY'),'') AS date_of_diagnosis_str from nonrheumatic_diseases where pat_id=patient_record.pat_id 
				  and deleted=0 and diseases_comorbidities_id=drugs_id3.diseases_comorbidities_id
							LOOP
							END LOOP;
						IF drugs_id3.diseases_comorbidities_id =120 THEN
							qu = qu || 'TRUE,' || quote_literal(drugs_id4.date_of_diagnosis_str);
						ELSE
							qu = qu || 'TRUE,' || quote_literal(drugs_id4.date_of_diagnosis_str) || ',' ;
						END IF;
					ELSE
						IF drugs_id3.diseases_comorbidities_id =120 THEN
							qu = qu || 'FALSE,' || quote_literal(dflt) ;
						ELSE
							qu = qu || 'FALSE,' || quote_literal(dflt) || ',' ;
						END IF;
					END IF;
			  end loop;
		  end loop;

	qu = qu || ');' ;
	execute qu;
end loop;

RETURN  1;

END; $BODY$;

ALTER FUNCTION public.create_insert_query_event_rest()
    OWNER TO postgres;
