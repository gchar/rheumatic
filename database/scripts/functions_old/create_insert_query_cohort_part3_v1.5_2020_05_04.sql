-- FUNCTION: public.create_insert_query_third_part()

-- DROP FUNCTION public.create_insert_query_third_part();

CREATE OR REPLACE FUNCTION public.create_insert_query_third_part(
	)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100000
    VOLATILE 
AS $BODY$DECLARE
  krow int;
  patient_record RECORD;
  dflt character='';
  drugs_id1 RECORD;
  drugs_id2 RECORD;
  drugs_id3 RECORD;
  drugs_id4 RECORD;
  drugs_id5 int;
  count_rows RECORD;
  i int=1;
  krow1 int = 1;
  krow2 int = 0;
  cnt int = 0;
  qu text; 
  create_qu text;
  create_first_qu text;
 BEGIN 
 execute 'DROP TABLE IF EXISTS statistics_lk3;';
create_qu = 'CREATE TABLE statistics_lk3 (id integer,';
        FOR drugs_id4 IN 1..27*70 LOOP 
			create_qu = create_qu || 'a' || drugs_id4 ;
			if  drugs_id4=27*70 then
				create_qu = create_qu || ' VARCHAR(250)' ;
			else
				create_qu = create_qu || ' VARCHAR(250),' ;
			end if;
		end loop;
create_qu = create_qu || ')';
execute create_qu;

create_first_qu = 'INSERT INTO statistics_lk3 VALUES (1,';
-- J J1 J2 3*27=81
FOR drugs_id4 IN 0..26 LOOP 
	
	IF drugs_id4<=12 THEN
		cnt=drug_id4*3;
	ELSE
		cnt=(drug_id4-6)*6;
	END IF;
	
	create_first_qu = create_first_qu || quote_literal('J-' || cnt) || ',' || quote_literal('J1-' || cnt) || ',' || quote_literal('J2-' || cnt) || ',';
	
end loop;

--REST 33*27=891

FOR drugs_id4 IN 0..26 LOOP 
	
	IF drugs_id4<=12 THEN
		cnt=drug_id4*3;
	ELSE
		cnt=(drug_id4-6)*6;
	END IF;
	create_first_qu = create_first_qu || quote_literal('28swollen_JC-' || cnt) || ',' || quote_literal('28tender_JC-' || cnt) || ',' || quote_literal('BASDAI-' || cnt) || ',' || quote_literal('BASDAI1-' || cnt) || ',' || quote_literal('BASDAI2-' || cnt) || ',';	
	create_first_qu = create_first_qu || quote_literal('BASDAI3-' || cnt) || ',' || quote_literal('BASDAI4-' || cnt) || ',' || quote_literal('BASDAI5-' || cnt) || ',' || quote_literal('BASDAI6-' || cnt) || ',' || quote_literal('BASFI-' || cnt) || ',' || quote_literal('CRP-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('DAS28ESR-' || cnt) || ',' || quote_literal('DAS28CRP-' || cnt) || ',' || quote_literal('ESR-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('VAS_physisian-' || cnt) || ',' || quote_literal('Euroqol_total-' || cnt) || ',' || quote_literal('EQ_5D_score-' || cnt) || ',' || quote_literal('mHAQ-' || cnt) || ',' || quote_literal('VASglobal-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('VASpain-' || cnt) || ',' || quote_literal('Weight-' || cnt) || ',' || quote_literal('Height-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('44SJC-' || cnt) || ',' || quote_literal('44SJC-' || cnt) || ',' || quote_literal('Dactylitis-' || cnt) || ',' || quote_literal('enthesitis-' || cnt) || ',' || quote_literal('psoriasisarea-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('wpai1-' || cnt) || ',' || quote_literal('wpai2-' || cnt) || ',' || quote_literal('wpai3-' || cnt) || ',' || quote_literal('wpai4-' || cnt) || ',' || quote_literal('wpai5-' || cnt) || ',' || quote_literal('wpai6-' || cnt) || ',';

end loop;

--SLE SLEDAI 25*27=675

FOR drugs_id4 IN 0..26 LOOP 
	
	IF drugs_id4<=12 THEN
		cnt=drug_id4*3;
	ELSE
		cnt=(drug_id4-6)*6;
	END IF;
	
	create_first_qu = create_first_qu || quote_literal('SLEDAI1-' || cnt) || ',' || quote_literal('SLEDAI2-' || cnt) || ',' || quote_literal('SLEDAI3-' || cnt) || ',' || quote_literal('SLEDAI4-' || cnt) || ',' || quote_literal('SLEDAI5-' || cnt) || ',' || quote_literal('SLEDAI6-' || cnt) || ',';
	create_first_qu = create_first_qu ||  || quote_literal('SLEDAI7-' || cnt) || ',' || quote_literal('SLEDAI8-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('SLEDAI9-' || cnt) || ',' || quote_literal('SLEDAI10-' || cnt) || ',' || quote_literal('SLEDAI11-' || cnt) || ',' || quote_literal('SLEDAI12-' || cnt) || ',' || quote_literal('SLEDAI13-' || cnt) || ',';
	create_first_qu = create_first_qu ||  || quote_literal('SLEDAI14-' || cnt) || ',' || quote_literal('SLEDAI15-' || cnt) || ',' || quote_literal('SLEDAI16-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('SLEDAI17-' || cnt) || ',' || quote_literal('SLEDAI18-' || cnt) || ',' || quote_literal('SLEDAI19-' || cnt) || ',' || quote_literal('SLEDAI20-' || cnt) || ',' || quote_literal('SLEDAI21-' || cnt) || ',';
	create_first_qu = create_first_qu ||  || quote_literal('SLEDAI22-' || cnt) || ',' || quote_literal('SLEDAI23-' || cnt) || ',' || quote_literal('SLEDAI24-' || cnt) || ',' || quote_literal('SLEDA_2K-' || cnt) || ',';

end loop;	
		


--SLE SELENA-CNS 3*27=81
FOR drugs_id4 IN 0..26 LOOP 
	
	IF drugs_id4<=12 THEN
		cnt=drug_id4*3;
	ELSE
		cnt=(drug_id4-6)*6;
	END IF;
	create_first_qu = create_first_qu || quote_literal('SELENA_Flare_Index-' || cnt) || ',' || quote_literal('CNS_Activity-' || cnt) || ',' || quote_literal('Proteinuria -' || cnt) || ',';
end loop;

--SEVERITY SLICC/ACR - SLE severity index - CND/Nephritis outcome 6*27=162
FOR drugs_id4 IN 0..26 LOOP 
	
	IF drugs_id4<=12 THEN
		cnt=drug_id4*3;
	ELSE
		cnt=(drug_id4-6)*6;
	END IF;
	
	if  drugs_id4 = 26 then
		create_first_qu = create_first_qu || quote_literal('SLICC_dmg_index-' || cnt) || ',' || quote_literal('sle_severity_index-' || cnt) || ',' || quote_literal('Neuropsychiatric_Lupus_Outcome-' || cnt) || ',' || quote_literal('Chronic_kidney_disease-' || cnt) || ',' || quote_literal('End_stage_kidney_disease-' || cnt) || ',' || quote_literal('Kidney_transplantation-' || cnt);
	else
		create_first_qu = create_first_qu || quote_literal('SLICC_dmg_index-' || cnt) || ',' || quote_literal('sle_severity_index-' || cnt) || ',' || quote_literal('Neuropsychiatric_Lupus_Outcome-' || cnt) || ',' || quote_literal('Chronic_kidney_disease-' || cnt) || ',' || quote_literal('End_stage_kidney_disease-' || cnt) || ',' || quote_literal('Kidney_transplantation-' || cnt) || ',';
	end if;
	
end loop;


create_first_qu = create_first_qu || ');' ;
execute create_first_qu;
RETURN  1;

	FOR count_rows IN select count(patient_cohort.pat_id) as count_rows from patient_cohort left join patient on patient.pat_id=patient_cohort.pat_id where patient.deleted=0 and patient_cohort.deleted=0
 	LOOP
	end loop;
 	FOR patient_record IN select patient_cohort.patient_cohort_id,patient_cohort.pat_id from patient_cohort left join patient on patient.pat_id=patient_cohort.pat_id 
	where patient.deleted=0 and patient_cohort.deleted=0
 	LOOP
	krow1 = krow1 + 1;
	qu = 'INSERT INTO statistics_lk3 VALUES ( ' || krow1 ||  ',' ;	
		FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 0 and drug_flag= 2  and drugs_id=0 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 0 and drug_flag= 2  and drugs_id=0 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
		FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 0 and drug_flag= 2  and drugs_id=1 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 0 and drug_flag= 2  and drugs_id=1 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 0 and drug_flag= 2  and drugs_id=2 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 0 and drug_flag= 2  and drugs_id=2 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
		FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 3 and drug_flag= 2  and drugs_id=0 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 3 and drug_flag= 2  and drugs_id=0 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 3 and drug_flag= 2  and drugs_id=1 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 3 and drug_flag= 2  and drugs_id=1 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
		FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 3 and drug_flag= 2  and drugs_id=2 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 3 and drug_flag= 2  and drugs_id=2 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
		FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 6 and drug_flag= 2  and drugs_id=0 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 6 and drug_flag= 2  and drugs_id=0 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 6 and drug_flag= 2  and drugs_id=1 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 6 and drug_flag= 2  and drugs_id=1 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
		FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 6 and drug_flag= 2  and drugs_id=2 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 6 and drug_flag= 2  and drugs_id=2 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 12 and drug_flag= 2  and drugs_id=0
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 12 and drug_flag= 2  and drugs_id=0 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 12 and drug_flag= 2  and drugs_id=1
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 12 and drug_flag= 2  and drugs_id=1 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
		FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 12 and drug_flag= 2  and drugs_id=2
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 12 and drug_flag= 2  and drugs_id=2 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 18 and drug_flag= 2  and drugs_id=0
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 18 and drug_flag= 2  and drugs_id=0 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 18 and drug_flag= 2  and drugs_id=1
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 18 and drug_flag= 2  and drugs_id=1 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
		FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 18 and drug_flag= 2  and drugs_id=2
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 18 and drug_flag= 2  and drugs_id=2 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 24 and drug_flag= 2  and drugs_id=0
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 24 and drug_flag= 2  and drugs_id=0 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 24 and drug_flag= 2  and drugs_id=1
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 24 and drug_flag= 2  and drugs_id=1 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
		FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 24 and drug_flag= 2  and drugs_id=2
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 24 and drug_flag= 2  and drugs_id=2 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 36 and drug_flag= 2  and drugs_id=0 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 36 and drug_flag= 2  and drugs_id=0 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 36 and drug_flag= 2  and drugs_id=1 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 36 and drug_flag= 2  and drugs_id=1 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
		FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 36 and drug_flag= 2  and drugs_id=2 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 36 and drug_flag= 2  and drugs_id=2 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 48 and drug_flag= 2  and drugs_id=0 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 48 and drug_flag= 2  and drugs_id=0 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 48 and drug_flag= 2  and drugs_id=1 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 48 and drug_flag= 2  and drugs_id=1 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
		FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 48 and drug_flag= 2  and drugs_id=2 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 48 and drug_flag= 2  and drugs_id=2 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 60 and drug_flag= 2  and drugs_id=0 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 60 and drug_flag= 2  and drugs_id=0 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 60 and drug_flag= 2  and drugs_id=1 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 60 and drug_flag= 2  and drugs_id=1 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort= 60 and drug_flag= 2  and drugs_id=2 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort= 60 and drug_flag= 2  and drugs_id=2 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			end loop;
		else
			qu = qu || quote_literal(dflt) || ',';
		end if;
		
		FOR drugs_id4 IN select count(patient_followup.*) as cnt from patient_followup
		left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
		left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
		left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
		where patient_followup.deleted=0
		and patient_common.deleted=0
		and patient_followup.pat_id=patient_record.pat_id
		and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
		and patient_followup.fumonthcohort=0 
		LOOP
		end loop;
		
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(patient_common.swollenjc28,'') as swollenjc28 ,coalesce(patient_common.tenderjc28,'') as tenderjc28 ,coalesce(patient_common.crp,'') as crp ,
			coalesce(patient_common.das28crp,'') as das28crp , coalesce(patient_common.das28esr,'') as das28esr ,coalesce(patient_common.esr,'') as esr ,coalesce(patient_common.vasphysicial,'') as vasphysicial ,
			coalesce(patient_common.mhaq,'') as mhaq ,coalesce(patient_common.vasglobal,'') as vasglobal ,coalesce(patient_common.vaspain,'') as vaspain ,coalesce(patient_common.weight,'') as weight ,
			coalesce(patient_common.height,'') as height ,coalesce(patient_spa.basdai,'') as basdai ,coalesce(patient_spa.basdai1,'') as basdai1 ,coalesce(patient_spa.basdai2,'') as basdai2 ,
			coalesce(patient_spa.basdai3,'') as basdai3 ,coalesce(patient_spa.basdai4,'') as basdai4 ,coalesce(patient_spa.basdai5,'') as basdai5 ,coalesce(patient_spa.basdai6,'') as basdai6 ,
			coalesce(patient_spa.basfi,'') as basfi ,coalesce(patient_euroqol.euroqol_score,0) as euroqol_score ,REPLACE (coalesce(patient_euroqol.eq5d,''), ',', '.') as eq5d
			from patient_followup
			left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
			left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
			left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
			where patient_followup.deleted=0
			and patient_common.deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=0  limit 1
			LOOP
				qu = qu || quote_literal(drugs_id1.swollenjc28) || ',' || quote_literal(drugs_id1.tenderjc28) || ',' || quote_literal(drugs_id1.basdai) || ',' || quote_literal(drugs_id1.basdai1) || ',' || quote_literal(drugs_id1.basdai2) || ',' || quote_literal(drugs_id1.basdai3) || ',' || quote_literal(drugs_id1.basdai4) || ',';
				qu = qu || quote_literal(drugs_id1.basdai5) || ',' || quote_literal(drugs_id1.basdai6) || ',' || quote_literal(drugs_id1.basfi) || ',' || quote_literal(drugs_id1.crp) || ',' || quote_literal(drugs_id1.das28esr) || ',' || quote_literal(drugs_id1.das28crp) || ',' || quote_literal(drugs_id1.esr) || ',' || quote_literal(drugs_id1.vasphysicial) || ',';
				qu = qu || quote_literal(drugs_id1.euroqol_score) || ',' || quote_literal(drugs_id1.eq5d) || ',' || quote_literal(drugs_id1.mhaq) || ',' || quote_literal(drugs_id1.vasglobal) || ',' || quote_literal(drugs_id1.vaspain) || ',' || quote_literal(drugs_id1.weight) || ',' || quote_literal(drugs_id1.height) || ',';
			end loop;
		else
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
		end if;
		
		FOR drugs_id4 IN select count(patient_followup.*) as cnt from patient_followup
		left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
		left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
		left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
		where patient_followup.deleted=0
		and patient_common.deleted=0
		and patient_followup.pat_id=patient_record.pat_id
		and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
		and patient_followup.fumonthcohort=3 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(patient_common.swollenjc28,'') as swollenjc28 ,coalesce(patient_common.tenderjc28,'') as tenderjc28 ,coalesce(patient_common.crp,'') as crp ,
			coalesce(patient_common.das28crp,'') as das28crp , coalesce(patient_common.das28esr,'') as das28esr ,coalesce(patient_common.esr,'') as esr ,coalesce(patient_common.vasphysicial,'') as vasphysicial ,
			coalesce(patient_common.mhaq,'') as mhaq ,coalesce(patient_common.vasglobal,'') as vasglobal ,coalesce(patient_common.vaspain,'') as vaspain ,coalesce(patient_common.weight,'') as weight ,
			coalesce(patient_common.height,'') as height ,coalesce(patient_spa.basdai,'') as basdai ,coalesce(patient_spa.basdai1,'') as basdai1 ,coalesce(patient_spa.basdai2,'') as basdai2 ,
			coalesce(patient_spa.basdai3,'') as basdai3 ,coalesce(patient_spa.basdai4,'') as basdai4 ,coalesce(patient_spa.basdai5,'') as basdai5 ,coalesce(patient_spa.basdai6,'') as basdai6 ,
			coalesce(patient_spa.basfi,'') as basfi ,coalesce(patient_euroqol.euroqol_score,0) as euroqol_score ,REPLACE (coalesce(patient_euroqol.eq5d,''), ',', '.') as eq5d
			from patient_followup
			left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
			left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
			left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
			where patient_followup.deleted=0
			and patient_common.deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=3  limit 1
			LOOP
				qu = qu || quote_literal(drugs_id1.swollenjc28) || ',' || quote_literal(drugs_id1.tenderjc28) || ',' || quote_literal(drugs_id1.basdai) || ',' || quote_literal(drugs_id1.basdai1) || ',' || quote_literal(drugs_id1.basdai2) || ',' || quote_literal(drugs_id1.basdai3) || ',' || quote_literal(drugs_id1.basdai4) || ',';
				qu = qu || quote_literal(drugs_id1.basdai5) || ',' || quote_literal(drugs_id1.basdai6) || ',' || quote_literal(drugs_id1.basfi) || ',' || quote_literal(drugs_id1.crp) || ',' || quote_literal(drugs_id1.das28esr) || ',' || quote_literal(drugs_id1.das28crp) || ',' || quote_literal(drugs_id1.esr) || ',' || quote_literal(drugs_id1.vasphysicial) || ',';
				qu = qu || quote_literal(drugs_id1.euroqol_score) || ',' || quote_literal(drugs_id1.eq5d) || ',' || quote_literal(drugs_id1.mhaq) || ',' || quote_literal(drugs_id1.vasglobal) || ',' || quote_literal(drugs_id1.vaspain) || ',' || quote_literal(drugs_id1.weight) || ',' || quote_literal(drugs_id1.height) || ',';
			end loop;
		else
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
		end if;
		FOR drugs_id4 IN select count(patient_followup.*) as cnt from patient_followup
		left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
		left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
		left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
		where patient_followup.deleted=0
		and patient_common.deleted=0
		and patient_followup.pat_id=patient_record.pat_id
		and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
		and patient_followup.fumonthcohort=6
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(patient_common.swollenjc28,'') as swollenjc28 ,coalesce(patient_common.tenderjc28,'') as tenderjc28 ,coalesce(patient_common.crp,'') as crp ,
			coalesce(patient_common.das28crp,'') as das28crp , coalesce(patient_common.das28esr,'') as das28esr ,coalesce(patient_common.esr,'') as esr ,coalesce(patient_common.vasphysicial,'') as vasphysicial ,
			coalesce(patient_common.mhaq,'') as mhaq ,coalesce(patient_common.vasglobal,'') as vasglobal ,coalesce(patient_common.vaspain,'') as vaspain ,coalesce(patient_common.weight,'') as weight ,
			coalesce(patient_common.height,'') as height ,coalesce(patient_spa.basdai,'') as basdai ,coalesce(patient_spa.basdai1,'') as basdai1 ,coalesce(patient_spa.basdai2,'') as basdai2 ,
			coalesce(patient_spa.basdai3,'') as basdai3 ,coalesce(patient_spa.basdai4,'') as basdai4 ,coalesce(patient_spa.basdai5,'') as basdai5 ,coalesce(patient_spa.basdai6,'') as basdai6 ,
			coalesce(patient_spa.basfi,'') as basfi ,coalesce(patient_euroqol.euroqol_score,0) as euroqol_score ,REPLACE (coalesce(patient_euroqol.eq5d,''), ',', '.') as eq5d
			from patient_followup
			left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
			left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
			left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
			where patient_followup.deleted=0
			and patient_common.deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=6  limit 1
			LOOP
				qu = qu || quote_literal(drugs_id1.swollenjc28) || ',' || quote_literal(drugs_id1.tenderjc28) || ',' || quote_literal(drugs_id1.basdai) || ',' || quote_literal(drugs_id1.basdai1) || ',' || quote_literal(drugs_id1.basdai2) || ',' || quote_literal(drugs_id1.basdai3) || ',' || quote_literal(drugs_id1.basdai4) || ',';
				qu = qu || quote_literal(drugs_id1.basdai5) || ',' || quote_literal(drugs_id1.basdai6) || ',' || quote_literal(drugs_id1.basfi) || ',' || quote_literal(drugs_id1.crp) || ',' || quote_literal(drugs_id1.das28esr) || ',' || quote_literal(drugs_id1.das28crp) || ',' || quote_literal(drugs_id1.esr) || ',' || quote_literal(drugs_id1.vasphysicial) || ',';
				qu = qu || quote_literal(drugs_id1.euroqol_score) || ',' || quote_literal(drugs_id1.eq5d) || ',' || quote_literal(drugs_id1.mhaq) || ',' || quote_literal(drugs_id1.vasglobal) || ',' || quote_literal(drugs_id1.vaspain) || ',' || quote_literal(drugs_id1.weight) || ',' || quote_literal(drugs_id1.height) || ',';
			end loop;
		else
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
		end if;
		FOR drugs_id4 IN select count(patient_followup.*) as cnt from patient_followup
		left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
		left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
		left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
		where patient_followup.deleted=0
		and patient_common.deleted=0
		and patient_followup.pat_id=patient_record.pat_id
		and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
		and patient_followup.fumonthcohort=12 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(patient_common.swollenjc28,'') as swollenjc28 ,coalesce(patient_common.tenderjc28,'') as tenderjc28 ,coalesce(patient_common.crp,'') as crp ,
			coalesce(patient_common.das28crp,'') as das28crp , coalesce(patient_common.das28esr,'') as das28esr ,coalesce(patient_common.esr,'') as esr ,coalesce(patient_common.vasphysicial,'') as vasphysicial ,
			coalesce(patient_common.mhaq,'') as mhaq ,coalesce(patient_common.vasglobal,'') as vasglobal ,coalesce(patient_common.vaspain,'') as vaspain ,coalesce(patient_common.weight,'') as weight ,
			coalesce(patient_common.height,'') as height ,coalesce(patient_spa.basdai,'') as basdai ,coalesce(patient_spa.basdai1,'') as basdai1 ,coalesce(patient_spa.basdai2,'') as basdai2 ,
			coalesce(patient_spa.basdai3,'') as basdai3 ,coalesce(patient_spa.basdai4,'') as basdai4 ,coalesce(patient_spa.basdai5,'') as basdai5 ,coalesce(patient_spa.basdai6,'') as basdai6 ,
			coalesce(patient_spa.basfi,'') as basfi ,coalesce(patient_euroqol.euroqol_score,0) as euroqol_score ,REPLACE (coalesce(patient_euroqol.eq5d,''), ',', '.') as eq5d
			from patient_followup
			left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
			left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
			left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
			where patient_followup.deleted=0
			and patient_common.deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=12  limit 1
			LOOP
				qu = qu || quote_literal(drugs_id1.swollenjc28) || ',' || quote_literal(drugs_id1.tenderjc28) || ',' || quote_literal(drugs_id1.basdai) || ',' || quote_literal(drugs_id1.basdai1) || ',' || quote_literal(drugs_id1.basdai2) || ',' || quote_literal(drugs_id1.basdai3) || ',' || quote_literal(drugs_id1.basdai4) || ',';
				qu = qu || quote_literal(drugs_id1.basdai5) || ',' || quote_literal(drugs_id1.basdai6) || ',' || quote_literal(drugs_id1.basfi) || ',' || quote_literal(drugs_id1.crp) || ',' || quote_literal(drugs_id1.das28esr) || ',' || quote_literal(drugs_id1.das28crp) || ',' || quote_literal(drugs_id1.esr) || ',' || quote_literal(drugs_id1.vasphysicial) || ',';
				qu = qu || quote_literal(drugs_id1.euroqol_score) || ',' || quote_literal(drugs_id1.eq5d) || ',' || quote_literal(drugs_id1.mhaq) || ',' || quote_literal(drugs_id1.vasglobal) || ',' || quote_literal(drugs_id1.vaspain) || ',' || quote_literal(drugs_id1.weight) || ',' || quote_literal(drugs_id1.height) || ',';
			end loop;
		else
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
		end if;
		FOR drugs_id4 IN select count(patient_followup.*) as cnt from patient_followup
		left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
		left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
		left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
		where patient_followup.deleted=0
		and patient_common.deleted=0
		and patient_followup.pat_id=patient_record.pat_id
		and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
		and patient_followup.fumonthcohort=18 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(patient_common.swollenjc28,'') as swollenjc28 ,coalesce(patient_common.tenderjc28,'') as tenderjc28 ,coalesce(patient_common.crp,'') as crp ,
			coalesce(patient_common.das28crp,'') as das28crp , coalesce(patient_common.das28esr,'') as das28esr ,coalesce(patient_common.esr,'') as esr ,coalesce(patient_common.vasphysicial,'') as vasphysicial ,
			coalesce(patient_common.mhaq,'') as mhaq ,coalesce(patient_common.vasglobal,'') as vasglobal ,coalesce(patient_common.vaspain,'') as vaspain ,coalesce(patient_common.weight,'') as weight ,
			coalesce(patient_common.height,'') as height ,coalesce(patient_spa.basdai,'') as basdai ,coalesce(patient_spa.basdai1,'') as basdai1 ,coalesce(patient_spa.basdai2,'') as basdai2 ,
			coalesce(patient_spa.basdai3,'') as basdai3 ,coalesce(patient_spa.basdai4,'') as basdai4 ,coalesce(patient_spa.basdai5,'') as basdai5 ,coalesce(patient_spa.basdai6,'') as basdai6 ,
			coalesce(patient_spa.basfi,'') as basfi ,coalesce(patient_euroqol.euroqol_score,0) as euroqol_score ,REPLACE (coalesce(patient_euroqol.eq5d,''), ',', '.') as eq5d
			from patient_followup
			left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
			left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
			left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
			where patient_followup.deleted=0
			and patient_common.deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=18  limit 1
			LOOP
				qu = qu || quote_literal(drugs_id1.swollenjc28) || ',' || quote_literal(drugs_id1.tenderjc28) || ',' || quote_literal(drugs_id1.basdai) || ',' || quote_literal(drugs_id1.basdai1) || ',' || quote_literal(drugs_id1.basdai2) || ',' || quote_literal(drugs_id1.basdai3) || ',' || quote_literal(drugs_id1.basdai4) || ',';
				qu = qu || quote_literal(drugs_id1.basdai5) || ',' || quote_literal(drugs_id1.basdai6) || ',' || quote_literal(drugs_id1.basfi) || ',' || quote_literal(drugs_id1.crp) || ',' || quote_literal(drugs_id1.das28esr) || ',' || quote_literal(drugs_id1.das28crp) || ',' || quote_literal(drugs_id1.esr) || ',' || quote_literal(drugs_id1.vasphysicial) || ',';
				qu = qu || quote_literal(drugs_id1.euroqol_score) || ',' || quote_literal(drugs_id1.eq5d) || ',' || quote_literal(drugs_id1.mhaq) || ',' || quote_literal(drugs_id1.vasglobal) || ',' || quote_literal(drugs_id1.vaspain) || ',' || quote_literal(drugs_id1.weight) || ',' || quote_literal(drugs_id1.height) || ',';
			end loop;
		else
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
		end if;
		FOR drugs_id4 IN select count(patient_followup.*) as cnt from patient_followup
		left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
		left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
		left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
		where patient_followup.deleted=0
		and patient_common.deleted=0
		and patient_followup.pat_id=patient_record.pat_id
		and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
		and patient_followup.fumonthcohort=24 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(patient_common.swollenjc28,'') as swollenjc28 ,coalesce(patient_common.tenderjc28,'') as tenderjc28 ,coalesce(patient_common.crp,'') as crp ,
			coalesce(patient_common.das28crp,'') as das28crp , coalesce(patient_common.das28esr,'') as das28esr ,coalesce(patient_common.esr,'') as esr ,coalesce(patient_common.vasphysicial,'') as vasphysicial ,
			coalesce(patient_common.mhaq,'') as mhaq ,coalesce(patient_common.vasglobal,'') as vasglobal ,coalesce(patient_common.vaspain,'') as vaspain ,coalesce(patient_common.weight,'') as weight ,
			coalesce(patient_common.height,'') as height ,coalesce(patient_spa.basdai,'') as basdai ,coalesce(patient_spa.basdai1,'') as basdai1 ,coalesce(patient_spa.basdai2,'') as basdai2 ,
			coalesce(patient_spa.basdai3,'') as basdai3 ,coalesce(patient_spa.basdai4,'') as basdai4 ,coalesce(patient_spa.basdai5,'') as basdai5 ,coalesce(patient_spa.basdai6,'') as basdai6 ,
			coalesce(patient_spa.basfi,'') as basfi ,coalesce(patient_euroqol.euroqol_score,0) as euroqol_score ,REPLACE (coalesce(patient_euroqol.eq5d,''), ',', '.') as eq5d
			from patient_followup
			left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
			left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
			left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
			where patient_followup.deleted=0
			and patient_common.deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=24  limit 1
			LOOP
				qu = qu || quote_literal(drugs_id1.swollenjc28) || ',' || quote_literal(drugs_id1.tenderjc28) || ',' || quote_literal(drugs_id1.basdai) || ',' || quote_literal(drugs_id1.basdai1) || ',' || quote_literal(drugs_id1.basdai2) || ',' || quote_literal(drugs_id1.basdai3) || ',' || quote_literal(drugs_id1.basdai4) || ',';
				qu = qu || quote_literal(drugs_id1.basdai5) || ',' || quote_literal(drugs_id1.basdai6) || ',' || quote_literal(drugs_id1.basfi) || ',' || quote_literal(drugs_id1.crp) || ',' || quote_literal(drugs_id1.das28esr) || ',' || quote_literal(drugs_id1.das28crp) || ',' || quote_literal(drugs_id1.esr) || ',' || quote_literal(drugs_id1.vasphysicial) || ',';
				qu = qu || quote_literal(drugs_id1.euroqol_score) || ',' || quote_literal(drugs_id1.eq5d) || ',' || quote_literal(drugs_id1.mhaq) || ',' || quote_literal(drugs_id1.vasglobal) || ',' || quote_literal(drugs_id1.vaspain) || ',' || quote_literal(drugs_id1.weight) || ',' || quote_literal(drugs_id1.height) || ',';
			end loop;
		else
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
		end if;
		FOR drugs_id4 IN select count(patient_followup.*) as cnt from patient_followup
		left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
		left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
		left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
		where patient_followup.deleted=0
		and patient_common.deleted=0
		and patient_followup.pat_id=patient_record.pat_id
		and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
		and patient_followup.fumonthcohort=36 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(patient_common.swollenjc28,'') as swollenjc28 ,coalesce(patient_common.tenderjc28,'') as tenderjc28 ,coalesce(patient_common.crp,'') as crp ,
			coalesce(patient_common.das28crp,'') as das28crp , coalesce(patient_common.das28esr,'') as das28esr ,coalesce(patient_common.esr,'') as esr ,coalesce(patient_common.vasphysicial,'') as vasphysicial ,
			coalesce(patient_common.mhaq,'') as mhaq ,coalesce(patient_common.vasglobal,'') as vasglobal ,coalesce(patient_common.vaspain,'') as vaspain ,coalesce(patient_common.weight,'') as weight ,
			coalesce(patient_common.height,'') as height ,coalesce(patient_spa.basdai,'') as basdai ,coalesce(patient_spa.basdai1,'') as basdai1 ,coalesce(patient_spa.basdai2,'') as basdai2 ,
			coalesce(patient_spa.basdai3,'') as basdai3 ,coalesce(patient_spa.basdai4,'') as basdai4 ,coalesce(patient_spa.basdai5,'') as basdai5 ,coalesce(patient_spa.basdai6,'') as basdai6 ,
			coalesce(patient_spa.basfi,'') as basfi ,coalesce(patient_euroqol.euroqol_score,0) as euroqol_score ,REPLACE (coalesce(patient_euroqol.eq5d,''), ',', '.') as eq5d
			from patient_followup
			left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
			left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
			left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
			where patient_followup.deleted=0
			and patient_common.deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=36  limit 1
			LOOP
				qu = qu || quote_literal(drugs_id1.swollenjc28) || ',' || quote_literal(drugs_id1.tenderjc28) || ',' || quote_literal(drugs_id1.basdai) || ',' || quote_literal(drugs_id1.basdai1) || ',' || quote_literal(drugs_id1.basdai2) || ',' || quote_literal(drugs_id1.basdai3) || ',' || quote_literal(drugs_id1.basdai4) || ',';
				qu = qu || quote_literal(drugs_id1.basdai5) || ',' || quote_literal(drugs_id1.basdai6) || ',' || quote_literal(drugs_id1.basfi) || ',' || quote_literal(drugs_id1.crp) || ',' || quote_literal(drugs_id1.das28esr) || ',' || quote_literal(drugs_id1.das28crp) || ',' || quote_literal(drugs_id1.esr) || ',' || quote_literal(drugs_id1.vasphysicial) || ',';
				qu = qu || quote_literal(drugs_id1.euroqol_score) || ',' || quote_literal(drugs_id1.eq5d) || ',' || quote_literal(drugs_id1.mhaq) || ',' || quote_literal(drugs_id1.vasglobal) || ',' || quote_literal(drugs_id1.vaspain) || ',' || quote_literal(drugs_id1.weight) || ',' || quote_literal(drugs_id1.height) || ',';
			end loop;
		else
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
		end if;
		FOR drugs_id4 IN select count(patient_followup.*) as cnt from patient_followup
		left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
		left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
		left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
		where patient_followup.deleted=0
		and patient_common.deleted=0
		and patient_followup.pat_id=patient_record.pat_id
		and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
		and patient_followup.fumonthcohort=48 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(patient_common.swollenjc28,'') as swollenjc28 ,coalesce(patient_common.tenderjc28,'') as tenderjc28 ,coalesce(patient_common.crp,'') as crp ,
			coalesce(patient_common.das28crp,'') as das28crp , coalesce(patient_common.das28esr,'') as das28esr ,coalesce(patient_common.esr,'') as esr ,coalesce(patient_common.vasphysicial,'') as vasphysicial ,
			coalesce(patient_common.mhaq,'') as mhaq ,coalesce(patient_common.vasglobal,'') as vasglobal ,coalesce(patient_common.vaspain,'') as vaspain ,coalesce(patient_common.weight,'') as weight ,
			coalesce(patient_common.height,'') as height ,coalesce(patient_spa.basdai,'') as basdai ,coalesce(patient_spa.basdai1,'') as basdai1 ,coalesce(patient_spa.basdai2,'') as basdai2 ,
			coalesce(patient_spa.basdai3,'') as basdai3 ,coalesce(patient_spa.basdai4,'') as basdai4 ,coalesce(patient_spa.basdai5,'') as basdai5 ,coalesce(patient_spa.basdai6,'') as basdai6 ,
			coalesce(patient_spa.basfi,'') as basfi ,coalesce(patient_euroqol.euroqol_score,0) as euroqol_score ,REPLACE (coalesce(patient_euroqol.eq5d,''), ',', '.') as eq5d
			from patient_followup
			left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
			left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
			left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
			where patient_followup.deleted=0
			and patient_common.deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=48  limit 1 
			LOOP
				qu = qu || quote_literal(drugs_id1.swollenjc28) || ',' || quote_literal(drugs_id1.tenderjc28) || ',' || quote_literal(drugs_id1.basdai) || ',' || quote_literal(drugs_id1.basdai1) || ',' || quote_literal(drugs_id1.basdai2) || ',' || quote_literal(drugs_id1.basdai3) || ',' || quote_literal(drugs_id1.basdai4) || ',';
				qu = qu || quote_literal(drugs_id1.basdai5) || ',' || quote_literal(drugs_id1.basdai6) || ',' || quote_literal(drugs_id1.basfi) || ',' || quote_literal(drugs_id1.crp) || ',' || quote_literal(drugs_id1.das28esr) || ',' || quote_literal(drugs_id1.das28crp) || ',' || quote_literal(drugs_id1.esr) || ',' || quote_literal(drugs_id1.vasphysicial) || ',';
				qu = qu || quote_literal(drugs_id1.euroqol_score) || ',' || quote_literal(drugs_id1.eq5d) || ',' || quote_literal(drugs_id1.mhaq) || ',' || quote_literal(drugs_id1.vasglobal) || ',' || quote_literal(drugs_id1.vaspain) || ',' || quote_literal(drugs_id1.weight) || ',' || quote_literal(drugs_id1.height) || ',';
			end loop;
		else
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
		end if;
		FOR drugs_id4 IN select count(patient_followup.*) as cnt from patient_followup
		left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
		left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
		left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
		where patient_followup.deleted=0
		and patient_common.deleted=0
		and patient_followup.pat_id=patient_record.pat_id
		and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
		and patient_followup.fumonthcohort=60 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(patient_common.swollenjc28,'') as swollenjc28 ,coalesce(patient_common.tenderjc28,'') as tenderjc28 ,coalesce(patient_common.crp,'') as crp ,
			coalesce(patient_common.das28crp,'') as das28crp , coalesce(patient_common.das28esr,'') as das28esr ,coalesce(patient_common.esr,'') as esr ,coalesce(patient_common.vasphysicial,'') as vasphysicial ,
			coalesce(patient_common.mhaq,'') as mhaq ,coalesce(patient_common.vasglobal,'') as vasglobal ,coalesce(patient_common.vaspain,'') as vaspain ,coalesce(patient_common.weight,'') as weight ,
			coalesce(patient_common.height,'') as height ,coalesce(patient_spa.basdai,'') as basdai ,coalesce(patient_spa.basdai1,'') as basdai1 ,coalesce(patient_spa.basdai2,'') as basdai2 ,
			coalesce(patient_spa.basdai3,'') as basdai3 ,coalesce(patient_spa.basdai4,'') as basdai4 ,coalesce(patient_spa.basdai5,'') as basdai5 ,coalesce(patient_spa.basdai6,'') as basdai6 ,
			coalesce(patient_spa.basfi,'') as basfi ,coalesce(patient_euroqol.euroqol_score,0) as euroqol_score ,REPLACE (coalesce(patient_euroqol.eq5d,''), ',', '.') as eq5d
			from patient_followup
			left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
			left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
			left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
			where patient_followup.deleted=0
			and patient_common.deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=60 limit 1
			LOOP
				qu = qu || quote_literal(drugs_id1.swollenjc28) || ',' || quote_literal(drugs_id1.tenderjc28) || ',' || quote_literal(drugs_id1.basdai) || ',' || quote_literal(drugs_id1.basdai1) || ',' || quote_literal(drugs_id1.basdai2) || ',' || quote_literal(drugs_id1.basdai3) || ',' || quote_literal(drugs_id1.basdai4) || ',';
				qu = qu || quote_literal(drugs_id1.basdai5) || ',' || quote_literal(drugs_id1.basdai6) || ',' || quote_literal(drugs_id1.basfi) || ',' || quote_literal(drugs_id1.crp) || ',' || quote_literal(drugs_id1.das28esr) || ',' || quote_literal(drugs_id1.das28crp) || ',' || quote_literal(drugs_id1.esr) || ',' || quote_literal(drugs_id1.vasphysicial) || ',';
				qu = qu || quote_literal(drugs_id1.euroqol_score) || ',' || quote_literal(drugs_id1.eq5d) || ',' || quote_literal(drugs_id1.mhaq) || ',' || quote_literal(drugs_id1.vasglobal) || ',' || quote_literal(drugs_id1.vaspain) || ',' || quote_literal(drugs_id1.weight) || ',' || quote_literal(drugs_id1.height);
			end loop;
		else
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt);		
		end if;
	qu = qu || ')';
	execute qu;
	end loop;
RETURN  1;
END; $BODY$;

ALTER FUNCTION public.create_insert_query_third_part()
    OWNER TO postgres;
