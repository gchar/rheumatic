-- FUNCTION: public.create_insert_query_second_parta()

-- DROP FUNCTION public.create_insert_query_second_parta();

CREATE OR REPLACE FUNCTION public.create_insert_query_second_parta(
	)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100000
    VOLATILE 
AS $BODY$DECLARE

  count_rows RECORD;
  count_rows2 RECORD;
  mviews RECORD;  
  krow int=0;
  krow2 int=0;
  krow3 int=0;
  krow4 int=0;
  drugs_id3 RECORD;
  create_qu text;
  patient_record RECORD;
  qu text; 
  create_first_qu text;
  

 BEGIN 

 execute 'DROP TABLE IF EXISTS statistics_lk2a;';

 FOR count_rows IN SELECT count(drugs.drugs_id) as count_rows FROM drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration
		 WHERE drugs.deleted=0 and drugs.prim=0 and drugs.previous=0 
		and drugs.code not in ('A7','A8','A10','H1','H2','H3','J')
    LOOP
  END LOOP;

create_qu = 'CREATE TABLE statistics_lk2a (id integer,cohort_id integer,';

FOR drugs_id3 IN SELECT drugs.drugs_id,REPLACE (coalesce(drugs.code,''), ',', ' ') as code,
		 REPLACE (coalesce(drugs.substance,''), ',', ' ')  as substance,REPLACE (coalesce(lookup_tbl_val.value,''), ',', ' ')  as route_of_administration_val
		 FROM drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration
		 WHERE drugs.deleted=0 and drugs.prim=0 and drugs.previous=0 
		and drugs.code not in ('A7','A8','A10','H1','H2','H3','J')  order by drugs.drugs_id asc
			loop
			
			krow=krow+1;
			
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_0' || ' varchar(8),'; 
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_3' || ' varchar(8),';
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_6' || ' varchar(8),';
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_9' || ' varchar(8),';
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_12' || ' varchar(8),'; 
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_15' || ' varchar(8),';
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_18' || ' varchar(8),';
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_21' || ' varchar(8),';
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_24' || ' varchar(8),'; 
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_27' || ' varchar(8),';
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_30' || ' varchar(8),'; 
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_33' || ' varchar(8),';
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_36' || ' varchar(8),'; 
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_42' || ' varchar(8),'; 
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_48' || ' varchar(8),'; 
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_54' || ' varchar(8),'; 
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_60' || ' varchar(8),'; 
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_66' || ' varchar(8),'; 
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_72' || ' varchar(8),'; 
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_78' || ' varchar(8),'; 
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_84' || ' varchar(8),'; 
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_90' || ' varchar(8),'; 
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_96' || ' varchar(8),'; 
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_102' || ' varchar(8),'; 
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_108' || ' varchar(8),'; 
			create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_114' || ' varchar(8),'; 
			
		
			
			if count_rows.count_rows=krow then
				create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_120' || ' varchar(8));';
			else
				create_qu = create_qu || 'c' || drugs_id3.drugs_id  || '_120' || ' varchar(8),'; 
			end if;
end loop;

execute create_qu;

FOR count_rows2 IN SELECT count(patient_cohort.pat_id) as count_rows from patient_cohort
  left join patient on patient.pat_id=patient_cohort.pat_id where patient.deleted=0 and patient_cohort.deleted=0
    LOOP
  END LOOP;
 
 
 
 
 FOR patient_record IN select patient_cohort.patient_cohort_id,patient_cohort.pat_id from patient_cohort
  left join patient on patient.pat_id=patient_cohort.pat_id where patient.deleted=0 and patient_cohort.deleted=0
  
    LOOP
	
		krow2=krow2 + 1;
		qu = 'INSERT INTO statistics_lk2a VALUES ( ' || krow2 || ',' || patient_record.patient_cohort_id || ',';
		
  		krow3=0;
		
	  --FOR drugs_id3 IN SELECT drugs_id FROM drugs WHERE deleted=0 and prim=0 and previous=0  order by drugs_id asc
		FOR drugs_id3 IN SELECT drugs.drugs_id,REPLACE (coalesce(drugs.code,''), ',', ' ') as code,
		 REPLACE (coalesce(drugs.substance,''), ',', ' ')  as substance,REPLACE (coalesce(lookup_tbl_val.value,''), ',', ' ')  as route_of_administration_val
		 FROM drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration
		 WHERE drugs.deleted=0 and drugs.prim=0 and drugs.previous=0 
		and drugs.code not in ('A7','A8','A10','H1','H2','H3','J')  order by drugs.drugs_id asc
			loop
			
			krow3=krow3+1;
				
			for mviews in select (select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=0 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage0
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=3 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage3
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=6 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage6
			  ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=9 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage9
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=12 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage12
			  ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=15 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage15
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=18 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage18
			  ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=21 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage21
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=24 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage24
			  ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=27 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage27
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=30 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage30
			  ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=33 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage33
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=36 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage36
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=42 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage42
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=48 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage48
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=54 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage54
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=60 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage60
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=66 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage66
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=72 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage72
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=78 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage78
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=84 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage84
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=90 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage90
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=96 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage96
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=102 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage102
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=108 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage108
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=114 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage114
			 ,(select currentdosage::text
			 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			 and pat_id= patient_record.pat_id and drug_flag= 1
			 and deleted= '0' and fumonth_cohort=120 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as currentdosage120
			 
			  loop
				END LOOP;
				
				
				
				if mviews.currentdosage0 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage0) || ',';
				 end if;
				
				if mviews.currentdosage3 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage3) || ',';
				 end if;
				
				if mviews.currentdosage6 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage6) || ',';
				 end if;
				
				if mviews.currentdosage9 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage9) || ',';
				 end if;
				 
				if mviews.currentdosage12 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage12) || ',';
				 end if;
				
				if mviews.currentdosage15 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage15) || ',';
				 end if;
				 
				if mviews.currentdosage18 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage18) || ',';
				 end if;
				
				if mviews.currentdosage21 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage21) || ',';
				 end if;
				 
				if mviews.currentdosage24 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage24) || ',';
				 end if;
				
				if mviews.currentdosage27 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage27) || ',';
				 end if;
				 
				if mviews.currentdosage30 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage30) || ',';
				 end if;
				
				if mviews.currentdosage33 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage33) || ',';
				 end if;
				 
				if mviews.currentdosage36 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage36) || ',';
				 end if;
				
				if mviews.currentdosage42 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage42) || ',';
				 end if;
				
				if mviews.currentdosage48 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage48) || ',';
				 end if;
				
				if mviews.currentdosage54 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage54) || ',';
				 end if;
				
				if mviews.currentdosage60 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage60) || ',';
				 end if;
							
				if mviews.currentdosage66 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage66) || ',';
				 end if;
				
				if mviews.currentdosage72 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage72) || ',';
				 end if;
				
			
				if mviews.currentdosage78 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage78) || ',';
				 end if;
				
				if mviews.currentdosage84 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage84) || ',';
				 end if;
				
				if mviews.currentdosage90 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage90) || ',';
				 end if;
				
				if mviews.currentdosage96 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage96) || ',';
				 end if;
				
				if mviews.currentdosage102 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage102) || ',';
				 end if;
				
				if mviews.currentdosage108 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage108) || ',';
				 end if;
				
				if mviews.currentdosage114 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage114) || ',';
				 end if;
				
			--if count_rows.count_rows=krow3 and count_rows2.count_rows=krow2 then
			if count_rows.count_rows=krow3 then
				if mviews.currentdosage120 is null then
					qu = qu || quote_literal('');
				 else
					qu = qu || quote_literal(mviews.currentdosage120);
				 end if;
			else
				if mviews.currentdosage120 is null then
					qu = qu || quote_literal('') || ',';
				 else
					qu = qu || quote_literal(mviews.currentdosage120) || ',';
				 end if;
			end if;
			
				
				
		END LOOP;
		
		qu = qu || ');' ;
		execute qu;
	END LOOP;
RETURN  1;

END; $BODY$;

ALTER FUNCTION public.create_insert_query_second_parta()
    OWNER TO postgres;
