-- FUNCTION: public.create_insert_query_second_partb()

-- DROP FUNCTION public.create_insert_query_second_partb();

CREATE OR REPLACE FUNCTION public.create_insert_query_second_partb(
	)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100000
    VOLATILE 
AS $BODY$DECLARE

  count_rows RECORD;
  count_rows2 RECORD;
  mviews RECORD;  
  krow int=0;
  krow2 int=0;
  krow3 int=0;
  drugs_id3 RECORD;
  drugs_id5 int;
  create_qu text;
  patient_record RECORD;
  qu text; 
  create_first_qu text;
  cnt int = 0;
  before_drug_id int = 0;
  

 BEGIN 

 execute 'DROP TABLE IF EXISTS statistics_lk2b;';

 FOR count_rows IN SELECT count(drugs.drugs_id) as count_rows FROM drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration
		 WHERE drugs.deleted=0 and drugs.prim=0 and drugs.previous=0 
		and drugs.code not in ('A7','A8','A10','H1','H2','H3','J')
    LOOP
  END LOOP;

create_qu = 'CREATE TABLE statistics_lk2b (id integer,cohort_id integer,';
	FOR drugs_id5 IN 0..26 LOOP 
		IF drugs_id5<=12 THEN
			cnt=drugs_id5*3;
		ELSE
			cnt=(drugs_id5-6)*6;
		END IF;
				
		krow=0;
		
		FOR drugs_id3 IN SELECT drugs.drugs_id FROM drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration
		 WHERE drugs.deleted=0 and drugs.prim=0 and drugs.previous=0 
		and drugs.code not in ('A7','A8','A10','H1','H2','H3','J')  order by drugs.drugs_id asc
		loop
				krow=krow+1;
			
				if count_rows.count_rows=krow and drugs_id5=26 then
					create_qu = create_qu || 'm' || drugs_id3.drugs_id  || CONCAT('_', cnt) || ' varchar(8));';
				else
					create_qu = create_qu || 'm' || drugs_id3.drugs_id  || CONCAT('_', cnt) || ' varchar(8),'; 
				end if;
		
			
			
			
				
				
			end loop;
			
end loop;

execute create_qu;

--FOR count_rows2 IN SELECT count(patient_cohort.pat_id) as count_rows from patient_cohort
--  left join patient on patient.pat_id=patient_cohort.pat_id where patient.deleted=0 and patient_cohort.deleted=0
 --   LOOP
--  END LOOP;
 
 
 
 
 FOR patient_record IN select patient_cohort.patient_cohort_id,patient_cohort.pat_id from patient_cohort
  left join patient on patient.pat_id=patient_cohort.pat_id where patient.deleted=0 and patient_cohort.deleted=0 
  -- for testing
 -- and patient_cohort.pat_id=10001
    LOOP
	
		krow2=krow2 + 1;
		qu = 'INSERT INTO statistics_lk2b VALUES ( ' || krow2 || ',' || patient_record.patient_cohort_id || ',';
		
  		
		
		FOR drugs_id5 IN 0..26 LOOP 
			
			IF drugs_id5<=12 THEN
				cnt=drugs_id5*3;
			ELSE
				cnt=(drugs_id5-6)*6;
			END IF;
				
				krow3=0;
				
			--FOR drugs_id3 IN SELECT drugs_id FROM drugs WHERE deleted=0 and prim=0 and previous=0  order by drugs_id asc
			FOR mviews IN SELECT drugs.drugs_id,coalesce(patient_lookup_drugs.weekdosage::text,'') as weekdosage FROM drugs 
			left join patient_lookup_drugs on patient_lookup_drugs.drugs_id=drugs.drugs_id
			and patient_lookup_drugs.patient_cohort_id= patient_record.patient_cohort_id
			and patient_lookup_drugs.pat_id= patient_record.pat_id and patient_lookup_drugs.drug_flag= 1
			and patient_lookup_drugs.fumonth_cohort=cnt
			and patient_lookup_drugs.deleted=0
			 WHERE drugs.deleted=0 and drugs.prim=0 and drugs.previous=0 
			and drugs.code not in ('A7','A8','A10','H1','H2','H3','J')
			order by drugs.drugs_id asc
			loop
			
				
				
				if before_drug_id<>mviews.drugs_id then
					
					krow3=krow3+1;
					--if count_rows.count_rows=krow3 and count_rows2.count_rows=krow2 then
					if count_rows.count_rows=krow3 and cnt=120 then
						if mviews.weekdosage is null then
							qu = qu || quote_literal('');
						 else
							qu = qu || quote_literal(mviews.weekdosage);
						 end if;
					else
						if mviews.weekdosage is null then
							qu = qu || quote_literal('') || ',';
						 else
							qu = qu || quote_literal(mviews.weekdosage) || ',';
						 end if;
					end if;
					
				end if;
				
				before_drug_id=mviews.drugs_id;
				
				--for mviews in select weekdosage::text as weekdosage
				 --from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
				 --and pat_id= patient_record.pat_id and drug_flag= 1
				 --and deleted= '0' and fumonth_cohort=cnt and drugs_id=drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1
				--loop
				--END LOOP;
				
				
			
			end loop;
			
		end loop;
		
		qu = qu || ');' ;
		execute qu;
	END LOOP;
RETURN  1;

END; $BODY$;

ALTER FUNCTION public.create_insert_query_second_partb()
    OWNER TO postgres;
