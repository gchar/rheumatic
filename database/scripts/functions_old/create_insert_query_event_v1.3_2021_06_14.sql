-- FUNCTION: public.create_insert_query_event()

-- DROP FUNCTION public.create_insert_query_event();

CREATE OR REPLACE FUNCTION public.create_insert_query_event(
	)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100000
    VOLATILE 
AS $BODY$DECLARE
krow int=0;
krow2 int=1;
count_rows RECORD;
drugs_id1 RECORD;
drugs_id3 RECORD;
drugs_id4 RECORD;
patient_record RECORD;
drugs_id5 int;
qu text; 
create_qu text;
create_first_qu text;
dflt character='';
mviews RECORD;  

BEGIN 
execute 'DROP TABLE if exists statistics_lk4;';

create_qu = 'CREATE TABLE statistics_lk4 (id integer,';	
create_qu = create_qu || 'a1 VARCHAR,a2 VARCHAR,a3 VARCHAR,a4 VARCHAR,a5 VARCHAR,a6 VARCHAR,a7 VARCHAR,a8 date,a9 date,a10 VARCHAR,';
 create_qu = create_qu || 'a11 VARCHAR,a12 VARCHAR,a13 VARCHAR,a14 VARCHAR,a15 VARCHAR,a16 VARCHAR,a17 VARCHAR,a18 VARCHAR,';
 create_qu = create_qu || 'a19 VARCHAR,a20 VARCHAR,a21 VARCHAR,a22 VARCHAR,a23 VARCHAR,';
 create_qu = create_qu || 'a24 VARCHAR,a25 VARCHAR,a26 VARCHAR,a27 VARCHAR,a28 VARCHAR,a29 VARCHAR,a30 VARCHAR,';
 create_qu = create_qu || 'a31 date,a32 VARCHAR,a33 VARCHAR,a34 VARCHAR,a35 VARCHAR,a36 VARCHAR,a37 VARCHAR,a38 VARCHAR,a39 VARCHAR,a40 VARCHAR,a41 VARCHAR,';
 /*create_qu = create_qu || 'a39 VARCHAR,a40 VARCHAR,a41 VARCHAR,a42 VARCHAR,a43 VARCHAR,a44 VARCHAR,a45 VARCHAR,';
 create_qu = create_qu || 'a46 VARCHAR,a47 VARCHAR,a48 VARCHAR,a49 VARCHAR,a50 VARCHAR,a51 VARCHAR,';
 create_qu = create_qu || 'a52 VARCHAR,a53 VARCHAR,a54 VARCHAR,a55 VARCHAR,a56 VARCHAR,a57 VARCHAR,a58 VARCHAR,a59 VARCHAR,';
 create_qu = create_qu || 'a60 VARCHAR,a61 VARCHAR,a62 VARCHAR,a63 VARCHAR,a64 VARCHAR,a65 VARCHAR,';
 create_qu = create_qu || 'a66 VARCHAR,a67 VARCHAR,a68 VARCHAR,a69 VARCHAR,a70 VARCHAR';*/

FOR count_rows IN SELECT count(drugs_id) as count_rows FROM drugs WHERE deleted=0 and prim=0 and previous=0
LOOP
end loop;

	FOR drugs_id5 IN 42..count_rows.count_rows+44 LOOP 
		krow = krow + 1;
		create_qu = create_qu || 'a' || drugs_id5 ;

		if count_rows.count_rows+3=krow then
			create_qu = create_qu || ' VARCHAR' ;
		else
			create_qu = create_qu || ' VARCHAR,' ;
		end if;
	end loop;

create_qu = create_qu || ');' ;
execute create_qu;

create_first_qu = 'INSERT INTO statistics_lk4 VALUES ( 1,' || quote_literal('cohort_id') || ',' || quote_literal('pat_id') || ',' || quote_literal('drug_id') || ',';
create_first_qu = create_first_qu || quote_literal('substance') || ',' || quote_literal('centre_id') || ',' || quote_literal('centre_name') || ',';
create_first_qu = create_first_qu || quote_literal('treatment_nbr') || ',' || quote_literal('01-01-2019') || ',' ;
create_first_qu = create_first_qu || quote_literal('01-01-2019') || ',' || quote_literal('stop_reason') || ',' || quote_literal('stop_cause_comments') || ',';
create_first_qu = create_first_qu || quote_literal('gender') || ',' || quote_literal('date_of_inclusion') || ',' || quote_literal('date_of_birth') ;
create_first_qu = create_first_qu || ',' || quote_literal('main_diagnosis_date') || ',' || quote_literal('icd10a') || ',' || quote_literal('main_diagnosis_value') ;
create_first_qu = create_first_qu || ',' || quote_literal('main_symptom_date') || ',' || quote_literal('first_secondary_diagnosis_date') || ',';
create_first_qu = create_first_qu || quote_literal('first_secondary_icd10b') || ',' || quote_literal('first_secondary_diagnosis_value') || ',' ;
create_first_qu = create_first_qu || quote_literal('first_secondary_symptom_date') || ',' || quote_literal('second_secondary_diagnosis_date') || ',';
create_first_qu = create_first_qu || quote_literal('second_secondary_icd10b') || ',' || quote_literal('second_secondary_diagnosis_value') || ',';
create_first_qu = create_first_qu || quote_literal('second_secondary_symptom_date') || ',' || quote_literal('third_secondary_diagnosis_date') || ',';
create_first_qu = create_first_qu || quote_literal('third_secondary_icd10b') || ',' || quote_literal('third_secondary_diagnosis_value') || ',' ;
create_first_qu = create_first_qu || quote_literal('third_secondary_symptom_date') || ',' || quote_literal('01-01-2019') || ',';
create_first_qu = create_first_qu || quote_literal('health_care_utilazation') || ',' || quote_literal('main_organ/system') || ',';
create_first_qu = create_first_qu || quote_literal('secondary_organ/system') || ',' || quote_literal('relationship_to_main_therapy') || ',';
create_first_qu = create_first_qu || quote_literal('outcome') || ',' || quote_literal('seriosness') || ',';
create_first_qu = create_first_qu || quote_literal('change_of_therapy') || ',' || quote_literal('fumonthcohort') || ',' || quote_literal('patient_event_id') || ',' || quote_literal('event_description') || ',';

 FOR drugs_id4 IN SELECT REPLACE (coalesce(drugs.substance,''), ',', ' ') as substance,
 REPLACE (coalesce(lookup_tbl_val.value,''), ',', ' ') as route_of_administration_val 
 FROM drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration
 WHERE drugs.deleted=0 and drugs.previous=0 and drugs.prim=0  order by drugs.drugs_id asc

    LOOP

		create_first_qu = create_first_qu || quote_literal(drugs_id4.substance || '-' || drugs_id4.route_of_administration_val) ;
		create_first_qu = create_first_qu || ',';

	end loop;


	create_first_qu = create_first_qu || quote_literal('J') || ',' || quote_literal('J1') || ',' || quote_literal('J2');
	create_first_qu = create_first_qu || ');' ;

execute create_first_qu;

  FOR patient_record IN select coalesce(patient_event.patient_event_id,0) as patient_event_id,coalesce(patient_cohort.patient_cohort_id,0) as patient_cohort_id
	,coalesce(patient_followup.fumonthcohort,0) as fumonthcohort
  	,coalesce(patient.patient_id,0) as patient_id,coalesce(patient_cohort.pat_id,0) as pat_id,coalesce(patient_cohort.cohort_name,0) as cohort_name,
	REPLACE (coalesce(drugs.substance,''), ',', ' ')  as substance
	,coalesce(patient.centre,0) as centre,REPLACE (coalesce(lookup_tbl_val.value,''), ',', ' ') as centre_name,
	coalesce(patient_cohort.treatment_nbr,'') as treatment_nbr,coalesce(TO_CHAR(patient_cohort.start_date, 'DD-MM-YYYY'),'12-12-1900') AS start_date_str,
	coalesce(TO_CHAR(patient_cohort.stop_date, 'DD-MM-YYYY'), '12-12-1900') AS stop_date_str
	,coalesce(patient_cohort.stop_reason,0) as stop_reason,REPLACE (coalesce(patient_cohort.stop_ae_reason,''), ',', ' ') as stop_ae_reason,coalesce(patient.gender,0) as gender,
	coalesce(TO_CHAR(patient.dateofinclusion, 'DD-MM-YYYY'),'') AS dateofinclusion_str,
	coalesce(TO_CHAR(patient.dateofbirth, 'DD-MM-YYYY'),'') AS dateofbirth_str,
	coalesce(TO_CHAR(patient_event.start_date, 'DD-MM-YYYY'),'12-12-1900') AS adversedate_str,
	coalesce(patient_event.hospitalization,0) as hospitalization,
	coalesce(patient_event.main_organ,0) as main_organ,
	coalesce(patient_event.secondary_organ,0) as secondary_organ,
	coalesce(patient_event.relationship,0) as relationship,
	coalesce(patient_event.outcome,0) as outcome,
	coalesce(patient_event.seriousness,0) as seriousness,
	coalesce(patient_event.change_therapy,0) as change_therapy
	,REPLACE (coalesce(patient_event.description,''), ',', ' ')  as event_description
	 from patient_event 
	left join patient_followup on patient_event.patient_followup_id=patient_followup.patient_followup_id 
	left join patient_cohort on patient_event.patient_cohort_id=patient_cohort.patient_cohort_id 
	left join patient on patient.pat_id=patient_cohort.pat_id 
	left join drugs on patient_cohort.cohort_name=drugs.drugs_id 
	left join lookup_tbl_val on lookup_tbl_val.id=patient.centre 
	where patient.deleted=0 and patient_cohort.deleted=0 and patient_event.deleted=0


    LOOP

	krow2=krow2 + 1;

	  qu = 'INSERT INTO statistics_lk4 VALUES ( ' || krow2 || ',' ;
	  qu = qu || quote_literal(patient_record.patient_cohort_id) || ',' || quote_literal(patient_record.patient_id) || ',' ;
	  qu = qu || quote_literal(patient_record.cohort_name) || ',' || quote_literal(patient_record.substance) || ',' ;
	  qu = qu || quote_literal(patient_record.centre) || ',' || quote_literal(patient_record.centre_name) || ',' ;
	  qu = qu || quote_literal(patient_record.treatment_nbr) || ',' || quote_literal(patient_record.start_date_str) || ',' ;
	  qu = qu || quote_literal(patient_record.stop_date_str) || ',' || quote_literal(patient_record.stop_reason) || ',' ;
	  qu = qu || quote_literal(patient_record.stop_ae_reason) || ',' || quote_literal(patient_record.gender) || ',' ;
	  qu = qu || quote_literal(patient_record.dateofinclusion_str) || ',' || quote_literal(patient_record.dateofbirth_str) || ',' ;

	  FOR drugs_id4 IN select count(diagnosis_id) as cnt from patient_diagnosis 
		where deleted=0 and pat_id=patient_record.pat_id and patient_diagnosis.main=1 and inactive=0
		LOOP
		end loop;

		IF drugs_id4.cnt>0 then

			FOR drugs_id3 IN select coalesce(patient_diagnosis.diagnosis_id,0) as diagnosis_id
		  ,coalesce(TO_CHAR(patient_diagnosis.symptom_date, 'DD-MM-YYYY'),'') 
		  AS symptom_date_str,coalesce(TO_CHAR(patient_diagnosis.disease_date, 'DD-MM-YYYY'),'') AS disease_date_str,
		  REPLACE (coalesce(diagnosis.icd10,''), ',', ' ') as icd10,
		  REPLACE (coalesce(diagnosis.value,''), ',', ' ') as value from patient_diagnosis left join diagnosis on 
		  diagnosis.diagnosis_id=patient_diagnosis.diagnosis_id where patient_diagnosis.deleted=0 and 
		  patient_diagnosis.pat_id=patient_record.pat_id and patient_diagnosis.main=1 and inactive=0

			LOOP

			qu = qu || quote_literal(drugs_id3.disease_date_str) || ',' || quote_literal(drugs_id3.icd10) || ',' ;
			qu = qu || quote_literal(drugs_id3.value) || ',' || quote_literal(drugs_id3.symptom_date_str) || ',' ;

			end loop;

		ELSE
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		END IF;


	 FOR drugs_id4 IN select count(diagnosis_id) as cnt from patient_diagnosis 
		where deleted=0 and pat_id=patient_record.pat_id and patient_diagnosis.main=0  and inactive=0
		LOOP
		END LOOP;

	IF drugs_id4.cnt=0 then

		qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;

	END IF;	

	if drugs_id4.cnt=1 then

		FOR drugs_id3 IN select coalesce(patient_diagnosis.diagnosis_id,0) as diagnosis_id
		  ,coalesce(TO_CHAR(patient_diagnosis.symptom_date, 'DD-MM-YYYY'),'') 
		  AS symptom_date_str,coalesce(TO_CHAR(patient_diagnosis.disease_date, 'DD-MM-YYYY'),'') AS disease_date_str,
		  REPLACE (coalesce(diagnosis.icd10,''), ',', ' ') as icd10,
		  REPLACE (coalesce(diagnosis.value,''), ',', ' ') as value from patient_diagnosis left join diagnosis on 
		  diagnosis.diagnosis_id=patient_diagnosis.diagnosis_id where patient_diagnosis.deleted=0 and 
		  patient_diagnosis.pat_id=patient_record.pat_id and patient_diagnosis.main=0  and inactive=0 limit 1

		  LOOP

			qu = qu || quote_literal(drugs_id3.disease_date_str) || ',' || quote_literal(drugs_id3.icd10) || ',' ;
			qu = qu || quote_literal(drugs_id3.value) || ',' || quote_literal(drugs_id3.symptom_date_str) || ',' ; 

		  END LOOP;

			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
	end if;		

	if drugs_id4.cnt=2 then

		FOR drugs_id3 IN select coalesce(patient_diagnosis.diagnosis_id,0) as diagnosis_id
		  ,coalesce(TO_CHAR(patient_diagnosis.symptom_date, 'DD-MM-YYYY'),'') 
		  AS symptom_date_str,coalesce(TO_CHAR(patient_diagnosis.disease_date, 'DD-MM-YYYY'),'') AS disease_date_str,
		  REPLACE (coalesce(diagnosis.icd10,''), ',', ' ') as icd10,
		  REPLACE (coalesce(diagnosis.value,''), ',', ' ') as value from patient_diagnosis left join diagnosis on 
		  diagnosis.diagnosis_id=patient_diagnosis.diagnosis_id where patient_diagnosis.deleted=0 and 
		  patient_diagnosis.pat_id=patient_record.pat_id and patient_diagnosis.main=0  and inactive=0 limit 2

		  LOOP

			qu = qu || quote_literal(drugs_id3.disease_date_str) || ',' || quote_literal(drugs_id3.icd10) || ',' ;
			qu = qu || quote_literal(drugs_id3.value) || ',' || quote_literal(drugs_id3.symptom_date_str) || ',' ; 

		  END LOOP;

			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;

	end if;	  

	if drugs_id4.cnt>=3 then

		FOR drugs_id3 IN select coalesce(patient_diagnosis.diagnosis_id,0) as diagnosis_id
		  ,coalesce(TO_CHAR(patient_diagnosis.symptom_date, 'DD-MM-YYYY'),'') 
		  AS symptom_date_str,coalesce(TO_CHAR(patient_diagnosis.disease_date, 'DD-MM-YYYY'),'') AS disease_date_str,
		  REPLACE (coalesce(diagnosis.icd10,''), ',', ' ') as icd10,
		  REPLACE (coalesce(diagnosis.value,''), ',', ' ') as value from patient_diagnosis left join diagnosis on 
		  diagnosis.diagnosis_id=patient_diagnosis.diagnosis_id where patient_diagnosis.deleted=0 and 
		  patient_diagnosis.pat_id=patient_record.pat_id and patient_diagnosis.main=0  and inactive=0 limit 3

		  LOOP

			qu = qu || quote_literal(drugs_id3.disease_date_str) || ',' || quote_literal(drugs_id3.icd10) || ',' ;
			qu = qu || quote_literal(drugs_id3.value) || ',' || quote_literal(drugs_id3.symptom_date_str) || ',' ; 
		  END LOOP;

	end if;

	   qu = qu || quote_literal(patient_record.adversedate_str) || ',' || quote_literal(patient_record.hospitalization) || ',' ;
	   qu = qu || quote_literal(patient_record.main_organ) || ',' || quote_literal(patient_record.secondary_organ) || ',' ;
	   qu = qu || quote_literal(patient_record.relationship) || ',' || quote_literal(patient_record.outcome) || ',' ;
	   qu = qu || quote_literal(patient_record.seriousness) || ',' || quote_literal(patient_record.change_therapy) || ',';
	   qu = qu || quote_literal(patient_record.fumonthcohort) || ',' || quote_literal(patient_record.patient_event_id) || ',' || quote_literal(patient_record.event_description) || ',' ;


	 FOR drugs_id3 IN SELECT drugs_id FROM drugs WHERE deleted=0 and prim=0 and previous=0  order by drugs_id asc
    LOOP
		 for drugs_id4 in select count(*) as cnt
		from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
		and pat_id= patient_record.pat_id and drug_flag= 1
		and deleted= '0' and fumonth_cohort=patient_record.fumonthcohort and drugs_id= drugs_id3.drugs_id limit 1 
		  LOOP
		  END LOOP;

		if drugs_id4.cnt>0 then

			for mviews in select weekdosage::text as weekdosage0
			from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
			and pat_id= patient_record.pat_id and drug_flag= 1
			and deleted= '0' and fumonth_cohort=patient_record.fumonthcohort and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 
			  LOOP

			if mviews.weekdosage0 is null then
				qu = qu || quote_literal('') || ',';
			else
				qu = qu || quote_literal(mviews.weekdosage0) || ',';
			end if;

		END LOOP;

		else
			qu = qu || quote_literal('') || ',';
		end if;
	END LOOP;

	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort=patient_record.fumonthcohort and drug_flag= 2  and drugs_id=0 
		LOOP
		END LOOP;

		IF drugs_id4.cnt>0 then

			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort=patient_record.fumonthcohort and drug_flag= 2  and drugs_id=0 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			END LOOP;
		ELSE
			qu = qu || quote_literal(dflt) || ',';
		END IF;

		FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort=patient_record.fumonthcohort and drug_flag= 2  and drugs_id=1 
		LOOP
		END LOOP;

		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort=patient_record.fumonthcohort and drug_flag= 2  and drugs_id=1 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage || ',';
			END LOOP;
		ELSE
			qu = qu || quote_literal(dflt) || ',';
		END IF;

	FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
		and patient_cohort_id = patient_record.patient_cohort_id
		and pat_id= patient_record.pat_id and fumonth_cohort=patient_record.fumonthcohort and drug_flag= 2  and drugs_id=2 
		LOOP
		END LOOP;

		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
			and patient_cohort_id = patient_record.patient_cohort_id
			and pat_id= patient_record.pat_id and fumonth_cohort=patient_record.fumonthcohort and drug_flag= 2  and drugs_id=2 order by drugs_id asc limit 1
			LOOP
				qu = qu || drugs_id1.weekdosage;
			END LOOP;
		ELSE
			qu = qu || quote_literal(dflt);
		END IF;
	qu = qu || ')';	

	execute qu;

END LOOP;

RETURN  1;


END; $BODY$;

ALTER FUNCTION public.create_insert_query_event()
    OWNER TO postgres;
