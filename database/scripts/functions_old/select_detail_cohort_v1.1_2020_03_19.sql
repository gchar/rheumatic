-- FUNCTION: public.select_detail_cohort(integer[], integer[], integer, integer, integer, date, date)

-- DROP FUNCTION public.select_detail_cohort(integer[], integer[], integer, integer, integer, date, date);

CREATE OR REPLACE FUNCTION public.select_detail_cohort(
	drugsin integer[],
	monthsin integer[],
	user_all_rpts integer,
	all_centre integer,
	user_centre_id integer,
	patient_cohort_startdate date,
	patient_cohort_enddate date)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100000
    VOLATILE 
AS $BODY$DECLARE

DECLARE
  drugnum RECORD;
  monthnum RECORD;
  currentnum RECORD;
  krow int = 0;
  qu text; 

 BEGIN 

qu = '(select statistics_lk1.*,';
	

	FOR drugnum IN 1..array_length(drugsin, 1)
		LOOP
		  FOR currentnum IN 0..1
			LOOP
				FOR monthnum IN 1..array_length(monthsin, 1)
				LOOP
					krow=krow + 1;

						qu = qu || '(select distinct label from statistics_lk2_val where ';
						qu = qu || ' drug_id = ' || drugsin[drugnum];
						qu = qu || ' and month = ' || monthsin[monthnum];
						qu = qu || ' and current = ' || currentnum;
						qu = qu || ' limit 1) as lbl' || krow || ',';
				END LOOP;
			END LOOP;
		END LOOP;
	qu = qu || 'statistics_lk3.* from statistics_lk1 join statistics_lk3 on statistics_lk1.id=statistics_lk3.id order by statistics_lk1.id asc limit 1  )';
	
qu = qu || ' union (select distinct statistics_lk1.*';

	FOR drugnum IN 1..array_length(drugsin, 1)
		LOOP
		  FOR currentnum IN 0..1
			LOOP
				FOR monthnum IN 1..array_length(monthsin, 1)
				LOOP
					krow=krow + 1;

						qu = qu || ',statistics_lk2.';
						IF currentnum=0 THEN 
							qu = qu || 'm';
						ELSE
							qu = qu || 'c';
						END IF;
						
						qu = qu ||  drugsin[drugnum];
						qu = qu || '_' || monthsin[monthnum];
				END LOOP;
			END LOOP;
		END LOOP;
	qu = qu || ',statistics_lk3.* from statistics_lk1 join statistics_lk2 on statistics_lk1.a1::int=statistics_lk2.cohort_id ';
	qu = qu || 'join statistics_lk3 on statistics_lk1.id=statistics_lk3.id where statistics_lk1.id<>1';
	
	IF user_all_rpts=1 THEN
		IF all_centre=0 THEN
			qu = qu || ' and statistics_lk1.a6::int=' || user_centre_id ;
		END IF;
	ELSE
		qu = qu || ' and statistics_lk1.a6::int=' || user_centre_id;
	END IF;
				
	IF patient_cohort_enddate IS NOT NULL or patient_cohort_startdate IS NOT NULL THEN
		qu = qu || ' and ';
	END IF;		
	
	IF patient_cohort_startdate IS NOT NULL THEN
		qu = qu || ' statistics_lk1.a9 >= ' || quote_literal(patient_cohort_startdate);
	END IF;
	
	IF patient_cohort_enddate IS NOT NULL THEN
		IF patient_cohort_startdate IS NOT NULL THEN
			qu = qu || ' and statistics_lk1.a9 <= ' || quote_literal(patient_cohort_enddate);
		ELSE
			qu = qu || ' statistics_lk1.a9 <= ' || quote_literal(patient_cohort_enddate);
		END IF;
	END IF;
	
	qu = qu || ' order by statistics_lk1.id asc)';

RETURN  qu AS selectquery;

END; 

$BODY$;

ALTER FUNCTION public.select_detail_cohort(integer[], integer[], integer, integer, integer, date, date)
    OWNER TO postgres;
