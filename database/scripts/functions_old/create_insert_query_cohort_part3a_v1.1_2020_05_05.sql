-- FUNCTION: public.create_insert_query_third_parta()

-- DROP FUNCTION public.create_insert_query_third_parta();

CREATE OR REPLACE FUNCTION public.create_insert_query_third_parta(
	)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100000
    VOLATILE 
AS $BODY$DECLARE
  krow int;
  patient_record RECORD;
  dflt character='';
  drugs_id1 RECORD;
  drugs_id2 RECORD;
  drugs_id3 RECORD;
  drugs_id4 RECORD;
  drugs_id5 int;
  drugs_id6 int;
  count_rows RECORD;
  i int=1;
  krow1 int = 1;
  krow2 int = 0;
  cnt int = 0;
  qu text; 
  create_qu text;
  create_first_qu text;
 BEGIN 
 execute 'DROP TABLE IF EXISTS statistics_lk3a;';
create_qu = 'CREATE TABLE statistics_lk3a (id integer,patient_cohort_id integer,';
        /*FOR drugs_id4 IN 1..27*36 LOOP 
			create_qu = create_qu || 'a' || drugs_id4 ;
			if  drugs_id4=27*36 then
				create_qu = create_qu || ' VARCHAR(250)' ;
			else
				create_qu = create_qu || ' VARCHAR(250),' ;
			end if;
		end loop;*/
		
		-- J J1 J2 3*27=81
		FOR drugs_id4 IN 0..26 LOOP 
			IF drugs_id4<=12 THEN
				cnt=drugs_id4*3;
			ELSE
				cnt=(drugs_id4-6)*6;
			END IF;
			
			create_qu = create_qu || CONCAT('J_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('J1_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('J2_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			
		end loop;
		
		--REST 33*27=891
		FOR drugs_id4 IN 0..26 LOOP 
			IF drugs_id4<=12 THEN
				cnt=drugs_id4*3;
			ELSE
				cnt=(drugs_id4-6)*6;
			END IF;
			
			create_qu = create_qu || CONCAT('SJC28_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('TJC28_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('BSD_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			FOR drugs_id6 IN 1..6 LOOP 
				create_qu = create_qu || CONCAT(CONCAT(CONCAT('BSD',drugs_id6),'_'),cnt) ;
				create_qu = create_qu || ' VARCHAR(250),' ;
			end loop;
			create_qu = create_qu || CONCAT('BSF_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('CRP_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('DAS28ESR_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('DAS28CRP_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('ESR_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('VASph_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('Euroqol_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('EQ5D_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('mHAQ_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('VASglb_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('VASpain_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('Wght_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('Hght_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('SJC44_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('TJC44_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('Dactyl_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('Enthesitis_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('psorarea_',cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			
			FOR drugs_id6 IN 1..6 LOOP 
				create_qu = create_qu || CONCAT(CONCAT(CONCAT('w',drugs_id6),'_'),cnt) ;
				IF drugs_id6=6 and drugs_id4=26 THEN
					create_qu = create_qu || ' VARCHAR(250)' ;
				ELSE	
					create_qu = create_qu || ' VARCHAR(250),' ;
				END IF;
			end loop;
			
		end loop;
create_qu = create_qu || ')';
execute create_qu;

create_first_qu = 'INSERT INTO statistics_lk3a VALUES (1,0,';
-- J J1 J2 3*27=81
FOR drugs_id5 IN 0..26 LOOP 
	
	IF drugs_id5<=12 THEN
		cnt = drugs_id5*3;
	ELSE
		cnt=(drugs_id5-6)*6;
	END IF;
	
	create_first_qu = create_first_qu || quote_literal('J-' || cnt) || ',' || quote_literal('J1-' || cnt) || ',' || quote_literal('J2-' || cnt) || ',';
	
end loop;

--REST 33*27=891

FOR drugs_id5 IN 0..26 LOOP 
	
	IF drugs_id5<=12 THEN
		cnt=drugs_id5*3;
	ELSE
		cnt=(drugs_id5-6)*6;
	END IF;
	create_first_qu = create_first_qu || quote_literal('28SJC-' || cnt) || ',' || quote_literal('28TJC-' || cnt) || ',' || quote_literal('BSD-' || cnt) || ',' || quote_literal('BSD1-' || cnt) || ',' || quote_literal('BSD2-' || cnt) || ',';	
	create_first_qu = create_first_qu || quote_literal('BSD3-' || cnt) || ',' || quote_literal('BSD4-' || cnt) || ',' || quote_literal('BSD5-' || cnt) || ',' || quote_literal('BSD6-' || cnt) || ',' || quote_literal('BSF-' || cnt) || ',' || quote_literal('CRP-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('DAS28ESR-' || cnt) || ',' || quote_literal('DAS28CRP-' || cnt) || ',' || quote_literal('ESR-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('VASph-' || cnt) || ',' || quote_literal('Euroqol-' || cnt) || ',' || quote_literal('EQ5D-' || cnt) || ',' || quote_literal('mHAQ-' || cnt) || ',' || quote_literal('VASglb-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('VASpain-' || cnt) || ',' || quote_literal('Wght-' || cnt) || ',' || quote_literal('Hght-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('44SJC-' || cnt) || ',' || quote_literal('44TJC-' || cnt) || ',' || quote_literal('Dactyl-' || cnt) || ',' || quote_literal('Enthesitis-' || cnt) || ',' || quote_literal('psorarea-' || cnt) || ',';
	

	if  drugs_id5 = 26 then
		create_first_qu = create_first_qu || quote_literal('w1-' || cnt) || ',' || quote_literal('w2-' || cnt) || ',' || quote_literal('w3-' || cnt) || ',' || quote_literal('w4-' || cnt) || ',' || quote_literal('w5-' || cnt) || ',' || quote_literal('w6-' || cnt);
	else
		create_first_qu = create_first_qu || quote_literal('w1-' || cnt) || ',' || quote_literal('w2-' || cnt) || ',' || quote_literal('w3-' || cnt) || ',' || quote_literal('W4-' || cnt) || ',' || quote_literal('w5-' || cnt) || ',' || quote_literal('w6-' || cnt) || ',';
	end if;
	
end loop;


create_first_qu = create_first_qu || ');' ;
execute create_first_qu;

	FOR count_rows IN select count(patient_cohort.pat_id) as count_rows from patient_cohort left join patient on patient.pat_id=patient_cohort.pat_id where patient.deleted=0 and patient_cohort.deleted=0
 	LOOP
	end loop;
 	FOR patient_record IN select patient_cohort.patient_cohort_id,patient_cohort.pat_id from patient_cohort left join patient on patient.pat_id=patient_cohort.pat_id 
	where patient.deleted=0 and patient_cohort.deleted=0
	-- for testing
	--and patient_cohort.pat_id=10001
	
 	LOOP
	
	krow1 = krow1 + 1;
	qu = 'INSERT INTO statistics_lk3a VALUES ( ' || krow1 ||  ',' || patient_record.patient_cohort_id || ',';	
	
	-- J J1 J2 3*27=81
		FOR drugs_id5 IN 0..26 LOOP 
			
			IF drugs_id5<=12 THEN
				cnt = drugs_id5*3;
			ELSE
				cnt=(drugs_id5-6)*6;
			END IF;
			
			FOR drugs_id6 IN 0..2 LOOP 
				FOR drugs_id4 IN select count(weekdosage) as cnt from patient_lookup_drugs where deleted= 0 
				and patient_cohort_id = patient_record.patient_cohort_id
				and pat_id= patient_record.pat_id and fumonth_cohort= cnt and drug_flag= 2  and drugs_id=drugs_id6 
				LOOP
				
				end loop;
				
				IF drugs_id4.cnt>0 then
					FOR drugs_id1 IN select coalesce(weekdosage,'') as weekdosage from patient_lookup_drugs where deleted= 0 
					and patient_cohort_id = patient_record.patient_cohort_id
					and pat_id= patient_record.pat_id and fumonth_cohort= cnt and drug_flag= 2  and drugs_id=drugs_id6 order by drugs_id asc limit 1
					LOOP
						qu = qu || drugs_id1.weekdosage || ',';
					end loop;
				else
					qu = qu || quote_literal(dflt) || ',';
				end if;
			end loop;
			
		end loop;
	

		--REST 33*27=891
		FOR drugs_id5 IN 0..26 LOOP 
			IF drugs_id5<=12 THEN
				cnt=drugs_id5*3;
			ELSE
				cnt=(drugs_id5-6)*6;
			END IF;
			
			
			
			FOR drugs_id4 IN select count(*) as cnt from patient_followup
			where deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=cnt
			LOOP
			end loop;
			
			IF drugs_id4.cnt>0 then
				FOR drugs_id1 IN select coalesce(patient_common.swollenjc28,'') as swollenjc28 ,coalesce(patient_common.tenderjc28,'') as tenderjc28 ,coalesce(patient_common.crp,'') as crp ,
				coalesce(patient_common.das28crp,'') as das28crp , coalesce(patient_common.das28esr,'') as das28esr ,coalesce(patient_common.esr,'') as esr ,coalesce(patient_common.vasphysicial,'') as vasphysicial ,
				coalesce(patient_common.mhaq,'') as mhaq ,coalesce(patient_common.vasglobal,'') as vasglobal ,coalesce(patient_common.vaspain,'') as vaspain ,coalesce(patient_common.weight,'') as weight ,
				coalesce(patient_common.height,'') as height ,coalesce(patient_spa.basdai,'') as basdai ,coalesce(patient_spa.basdai1,'') as basdai1 ,coalesce(patient_spa.basdai2,'') as basdai2 ,
				coalesce(patient_spa.basdai3,'') as basdai3 ,coalesce(patient_spa.basdai4,'') as basdai4 ,coalesce(patient_spa.basdai5,'') as basdai5 ,coalesce(patient_spa.basdai6,'') as basdai6 ,
				coalesce(patient_spa.basfi,'') as basfi ,coalesce(patient_euroqol.euroqol_score,0) as euroqol_score ,REPLACE (coalesce(patient_euroqol.eq5d,''), ',', '.') as eq5d,
				coalesce(patient_common.swollenjc44,'') as swollenjc44 , coalesce(patient_common.tenderjc44,'') as tenderjc44 ,coalesce(patient_spa.sparest3,'') as dactylitis ,coalesce(patient_spa.sparest4,'') as enthesitis,
				coalesce(patient_spa.sparest9,'') as psorarea,coalesce(patient_wpai.wpai1,0) as wpai1,coalesce(patient_wpai.wpai2,0) as wpai2,coalesce(patient_wpai.wpai3,0) as wpai3,
				coalesce(patient_wpai.wpai4,0) as wpai4,coalesce(patient_wpai.wpai5,0) as wpai5,coalesce(patient_wpai.wpai6,0) as wpai6
				from patient_followup
				left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
				left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id 
				left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id 
				left join patient_wpai on patient_wpai.patient_followup_id=patient_followup.patient_followup_id
				where patient_followup.deleted=0
				and patient_followup.pat_id=patient_record.pat_id
				and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
				and patient_followup.fumonthcohort=cnt  limit 1
				LOOP
					qu = qu || quote_literal(drugs_id1.swollenjc28) || ',' || quote_literal(drugs_id1.tenderjc28) || ',' || quote_literal(drugs_id1.basdai) || ',' || quote_literal(drugs_id1.basdai1) || ',' || quote_literal(drugs_id1.basdai2) || ',' || quote_literal(drugs_id1.basdai3) || ',' || quote_literal(drugs_id1.basdai4) || ',';
					qu = qu || quote_literal(drugs_id1.basdai5) || ',' || quote_literal(drugs_id1.basdai6) || ',' || quote_literal(drugs_id1.basfi) || ',' || quote_literal(drugs_id1.crp) || ',' || quote_literal(drugs_id1.das28esr) || ',' || quote_literal(drugs_id1.das28crp) || ',' || quote_literal(drugs_id1.esr) || ',' || quote_literal(drugs_id1.vasphysicial) || ',';
					qu = qu || quote_literal(drugs_id1.euroqol_score) || ',' || quote_literal(drugs_id1.eq5d) || ',' || quote_literal(drugs_id1.mhaq) || ',' || quote_literal(drugs_id1.vasglobal) || ',' || quote_literal(drugs_id1.vaspain) || ',' || quote_literal(drugs_id1.weight) || ',' || quote_literal(drugs_id1.height) || ',';
					qu = qu || quote_literal(drugs_id1.swollenjc44) || ',' || quote_literal(drugs_id1.tenderjc44) || ',' || quote_literal(drugs_id1.dactylitis) || ',' || quote_literal(drugs_id1.enthesitis) || ',' || quote_literal(drugs_id1.psorarea) || ',' || quote_literal(drugs_id1.wpai1) || ',' || quote_literal(drugs_id1.wpai2) || ',';
					
					if cnt=120 then
						qu = qu || quote_literal(drugs_id1.wpai3) || ',' || quote_literal(drugs_id1.wpai4) || ',' || quote_literal(drugs_id1.wpai5) || ',' || quote_literal(drugs_id1.wpai6);
					else
						qu = qu || quote_literal(drugs_id1.wpai3) || ',' || quote_literal(drugs_id1.wpai4) || ',' || quote_literal(drugs_id1.wpai5) || ',' || quote_literal(drugs_id1.wpai6) || ',';
					end if;
					
				end loop;
			else
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
					
					if cnt=120 then
						qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt);
					else
						qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
					end if;
			end if;
			
		end loop;
	qu = qu || ')';
	execute qu;
	end loop;
RETURN  1;
END; $BODY$;

ALTER FUNCTION public.create_insert_query_third_parta()
    OWNER TO postgres;
