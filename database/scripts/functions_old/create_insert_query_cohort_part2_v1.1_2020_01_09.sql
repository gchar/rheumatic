DECLARE

  krow int;
  patient_record RECORD;
  drugs_id3 RECORD;
  drugs_id4 int;
  drugs_id5 int;
  drugs_id6 record;
  i int=1;
  krow2 int = 1;
  krow3 int = 0;
  qu text; 
  create_qu text;
  create_first_qu text;
  mviews RECORD;  
  count_rows RECORD;

 BEGIN 

 execute 'DROP TABLE IF EXISTS statistics_lk2;';

 FOR count_rows IN SELECT count(drugs_id) as count_rows FROM drugs WHERE deleted=0 and prim=0 and previous=0
    LOOP
  END LOOP;

 create_qu = 'CREATE TABLE statistics_lk2 (id integer,';
 
  FOR drugs_id4 IN SELECT drugs_id FROM drugs WHERE deleted=0 and prim=0 and previous=0  order by drugs_id asc
    LOOP
	krow3 = krow3 + 1;
	FOR drugs_id5 IN i..i+8 LOOP 
		create_qu = create_qu || 'a' || drugs_id5 ;
		if count_rows.count_rows=krow3 and drugs_id5=i+8 then
			create_qu = create_qu || ' VARCHAR(250)' ;
		else
			create_qu = create_qu || ' VARCHAR(250),' ;
		end if;
	end loop;
	i=i+9;
	END LOOP;
	
 	create_qu = create_qu || ');' ;
	execute create_qu;
	
 krow3 = 0;
 create_first_qu='INSERT INTO statistics_lk2 VALUES (1,';
 FOR drugs_id6 IN SELECT REPLACE (coalesce(drugs.code,''), ',', ' ') as code,
 REPLACE (coalesce(drugs.substance,''), ',', ' ')  as substance,REPLACE (coalesce(lookup_tbl_val.value,''), ',', ' ')  as route_of_administration_val
 FROM drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration
 WHERE drugs.deleted=0 and drugs.prim=0 and drugs.previous=0  order by drugs.drugs_id asc
    LOOP
	krow3 = krow3 + 1;
	create_first_qu = create_first_qu || quote_literal(drugs_id6.code || '-' || drugs_id6.substance || '-0') ;
	create_first_qu = create_first_qu || ',';
	create_first_qu = create_first_qu || quote_literal(drugs_id6.code || '-' || drugs_id6.substance || '-3') ;
	create_first_qu = create_first_qu || ',';
	create_first_qu = create_first_qu || quote_literal(drugs_id6.code || '-' || drugs_id6.substance || '-6') ;
	create_first_qu = create_first_qu || ',';
	create_first_qu = create_first_qu || quote_literal(drugs_id6.code || '-' || drugs_id6.substance || '-12') ;
	create_first_qu = create_first_qu || ',';
	create_first_qu = create_first_qu || quote_literal(drugs_id6.code || '-' || drugs_id6.substance || '-18') ;
	create_first_qu = create_first_qu || ',';
	create_first_qu = create_first_qu || quote_literal(drugs_id6.code || '-' || drugs_id6.substance || '-24') ;
	create_first_qu = create_first_qu || ',';
	create_first_qu = create_first_qu || quote_literal(drugs_id6.code || '-' || drugs_id6.substance || '-36') ;
	create_first_qu = create_first_qu || ',';
	create_first_qu = create_first_qu || quote_literal(drugs_id6.code || '-' || drugs_id6.substance || '-48') ;
	create_first_qu = create_first_qu || ',';
	
	IF count_rows.count_rows=krow3 then
		create_first_qu = create_first_qu || quote_literal(drugs_id6.code || '-' || drugs_id6.substance || '-60') ;
	ELSE
		create_first_qu = create_first_qu || quote_literal(drugs_id6.code || '-' || drugs_id6.substance || '-60') ;
	create_first_qu = create_first_qu || ',';
	END IF;
	
	END LOOP;
	
 	create_first_qu = create_first_qu || ');' ;
	execute create_first_qu;
	
  FOR patient_record IN select patient_cohort.patient_cohort_id,patient_cohort.pat_id from patient_cohort
  left join patient on patient.pat_id=patient_cohort.pat_id where patient.deleted=0 and patient_cohort.deleted=0 
    LOOP
	krow = 1;
	krow2=krow2 + 1;
  qu = 'INSERT INTO statistics_lk2 VALUES ( ' || krow2 || ',';
  
  FOR drugs_id3 IN SELECT drugs_id FROM drugs WHERE deleted=0 and prim=0 and previous=0  order by drugs_id asc
    LOOP
	
	 for mviews in select (select weekdosage::text
 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
 and pat_id= patient_record.pat_id and drug_flag= 1
 and deleted= '0' and fumonth_cohort=0 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as weekdosage0
 ,(select weekdosage::text
 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
 and pat_id= patient_record.pat_id and drug_flag= 1
 and deleted= '0' and fumonth_cohort=3 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as weekdosage3
 ,(select weekdosage::text
 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
 and pat_id= patient_record.pat_id and drug_flag= 1
 and deleted= '0' and fumonth_cohort=6 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as weekdosage6
 ,(select weekdosage::text
 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
 and pat_id= patient_record.pat_id and drug_flag= 1
 and deleted= '0' and fumonth_cohort=12 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as weekdosage12
 ,(select weekdosage::text
 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
 and pat_id= patient_record.pat_id and drug_flag= 1
 and deleted= '0' and fumonth_cohort=18 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as weekdosage18
 ,(select weekdosage::text
 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
 and pat_id= patient_record.pat_id and drug_flag= 1
 and deleted= '0' and fumonth_cohort=24 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as weekdosage24
 ,(select weekdosage::text
 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
 and pat_id= patient_record.pat_id and drug_flag= 1
 and deleted= '0' and fumonth_cohort=36 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as weekdosage36
 ,(select weekdosage::text
 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
 and pat_id= patient_record.pat_id and drug_flag= 1
 and deleted= '0' and fumonth_cohort=48 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as weekdosage48
 ,(select weekdosage::text
 from patient_lookup_drugs where deleted= 0 and patient_cohort_id= patient_record.patient_cohort_id 
 and pat_id= patient_record.pat_id and drug_flag= 1
 and deleted= '0' and fumonth_cohort=60 and drugs_id= drugs_id3.drugs_id order by patient_lookup_drugs desc limit 1 ) as weekdosage60
 loop
 if mviews.weekdosage0 is null then
 	qu = qu || quote_literal('') || ',';
 else
 	qu = qu || quote_literal(mviews.weekdosage0) || ',';
 end if;
 if mviews.weekdosage3 is null then
 	qu = qu || quote_literal('') || ',';
 else
 	qu = qu || quote_literal(mviews.weekdosage3) || ',';
 end if;
 if mviews.weekdosage6 is null then
 	qu = qu || quote_literal('') || ',';
 else
 	qu = qu || quote_literal(mviews.weekdosage6) || ',';
 end if;
 if mviews.weekdosage12 is null then
 	qu = qu || quote_literal('') || ',';
 else
 	qu = qu || quote_literal(mviews.weekdosage12) || ',';
 end if;
 if mviews.weekdosage18 is null then
 	qu = qu || quote_literal('') || ',';
 else
 	qu = qu || quote_literal(mviews.weekdosage18) || ',';
 end if;
 if mviews.weekdosage24 is null then
 	qu = qu || quote_literal('') || ',';
 else
 	qu = qu || quote_literal(mviews.weekdosage24) || ',';
 end if;
 if mviews.weekdosage36 is null then
 	qu = qu || quote_literal('') || ',';
 else
 	qu = qu || quote_literal(mviews.weekdosage36) || ',';
 end if;
 if mviews.weekdosage48 is null then
 	qu = qu || quote_literal('') || ',';
 else
 	qu = qu || quote_literal(mviews.weekdosage48) || ',';
 end if;
 if krow = count_rows.count_rows then
	 if mviews.weekdosage60 is null then
		qu = qu || quote_literal('');
	 else
		qu = qu || quote_literal(mviews.weekdosage60);
	 end if;
 else
 	if mviews.weekdosage60 is null then
		qu = qu || quote_literal('') || ',';
	 else
		qu = qu || quote_literal(mviews.weekdosage60) || ',';
	 end if;
 end if;
 
 END LOOP;
 
 krow = krow + 1;
	 END LOOP;
	 
qu = qu || ')';	
execute qu;

END LOOP;

RETURN  1;

END; 