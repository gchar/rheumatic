-- FUNCTION: public.create_insert_query_third_partb()

-- DROP FUNCTION public.create_insert_query_third_partb();

CREATE OR REPLACE FUNCTION public.create_insert_query_third_partb(
	)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100000
    VOLATILE 
AS $BODY$DECLARE
  krow int;
  patient_record RECORD;
  dflt character='';
  drugs_id1 RECORD;
  drugs_id2 RECORD;
  drugs_id3 RECORD;
  drugs_id4 RECORD;
  drugs_id5 int;
  drugs_id6 int;
  count_rows RECORD;
  i int=1;
  krow1 int = 1;
  krow2 int = 0;
  cnt int = 0;
  qu text; 
  create_qu text;
  create_first_qu text;
 BEGIN 
 execute 'DROP TABLE IF EXISTS statistics_lk3b;';
create_qu = 'CREATE TABLE statistics_lk3b (id integer,patient_cohort_id integer,';

        /*FOR drugs_id4 IN 1..27*34 LOOP 
			create_qu = create_qu || 'a' || drugs_id4 ;
			if  drugs_id4=27*34 then
				create_qu = create_qu || ' VARCHAR(250)' ;
			else
				create_qu = create_qu || ' VARCHAR(250),' ;
			end if;
		end loop;*/
		
		--SLE SLEDAI 25*27=675
		FOR drugs_id5 IN 0..26 LOOP 
			IF drugs_id5<=12 THEN
				cnt=drugs_id5*3;
			ELSE
				cnt=(drugs_id5-6)*6;
			END IF;
				
			FOR drugs_id6 IN 1..24 LOOP 
				create_qu = create_qu || CONCAT('s', drugs_id6) || CONCAT('_', cnt) ;
				create_qu = create_qu || ' VARCHAR(250),' ;
			end loop;
			
			create_qu = create_qu ||  CONCAT('sl2k', drugs_id6) || CONCAT('_', cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			
		end loop;	
		
		--SLE SELENA-CNS 3*27=81
		FOR drugs_id5 IN 0..26 LOOP 
			IF drugs_id5<=12 THEN
				cnt=drugs_id5*3;
			ELSE
				cnt=(drugs_id5-6)*6;
			END IF;
			create_qu = create_qu || CONCAT('sel_', cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('cns_', cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('prot_', cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			
		end loop;
		
		--SEVERITY SLICC/ACR - SLE severity index - CND/Nephritis outcome 6*27=162
		FOR drugs_id5 IN 0..26 LOOP 
			IF drugs_id5<=12 THEN
				cnt=drugs_id5*3;
			ELSE
				cnt=(drugs_id5-6)*6;
			END IF;
			create_qu = create_qu || CONCAT('SLICCdmg_', cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('slesevind_', cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('NeuroLup_', cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('ChrKidDis_', cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('EndstagekidDis_', cnt) ;
			create_qu = create_qu || ' VARCHAR(250),' ;
			create_qu = create_qu || CONCAT('KidTransp_', cnt) ;
			
			if  drugs_id5 = 26 then
				create_qu = create_qu || ' VARCHAR(250)' ;
			else
				create_qu = create_qu || ' VARCHAR(250),' ;
			end if;
			
		end loop;
		
create_qu = create_qu || ')';



execute create_qu;

create_first_qu = 'INSERT INTO statistics_lk3b VALUES (1,0,';

--SLE SLEDAI 25*27=675

FOR drugs_id5 IN 0..26 LOOP 
	
	IF drugs_id5<=12 THEN
		cnt=drugs_id5*3;
	ELSE
		cnt=(drugs_id5-6)*6;
	END IF;
	
	create_first_qu = create_first_qu || quote_literal('SL1-' || cnt) || ',' || quote_literal('SL2-' || cnt) || ',' || quote_literal('SL3-' || cnt) || ',' || quote_literal('SL4-' || cnt) || ',' || quote_literal('SL5-' || cnt) || ',' || quote_literal('SL6-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('SL7-' || cnt) || ',' || quote_literal('SL8-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('SL9-' || cnt) || ',' || quote_literal('SL10-' || cnt) || ',' || quote_literal('SL11-' || cnt) || ',' || quote_literal('SL12-' || cnt) || ',' || quote_literal('SL13-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('SL14-' || cnt) || ',' || quote_literal('SL15-' || cnt) || ',' || quote_literal('SL16-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('SL17-' || cnt) || ',' || quote_literal('SL18-' || cnt) || ',' || quote_literal('SL19-' || cnt) || ',' || quote_literal('SL20-' || cnt) || ',' || quote_literal('SL21-' || cnt) || ',';
	create_first_qu = create_first_qu || quote_literal('SL22-' || cnt) || ',' || quote_literal('SL23-' || cnt) || ',' || quote_literal('SL24-' || cnt) || ',' || quote_literal('SL2K-' || cnt) || ',';

end loop;	
		


--SLE SELENA-CNS 3*27=81
FOR drugs_id5 IN 0..26 LOOP 
	
	IF drugs_id5<=12 THEN
		cnt=drugs_id5*3;
	ELSE
		cnt=(drugs_id5-6)*6;
	END IF;
	create_first_qu = create_first_qu || quote_literal('SELENA-' || cnt) || ',' || quote_literal('CNS-' || cnt) || ',' || quote_literal('Protein-' || cnt) || ',';
end loop;

--SEVERITY SLICC/ACR - SLE severity index - CND/Nephritis outcome 6*27=162
FOR drugs_id5 IN 0..26 LOOP 
	
	IF drugs_id5<=12 THEN
		cnt=drugs_id5*3;
	ELSE
		cnt=(drugs_id5-6)*6;
	END IF;
	
	if  drugs_id5 = 26 then
		create_first_qu = create_first_qu || quote_literal('SLICCdmg-' || cnt) || ',' || quote_literal('slesevind-' || cnt) || ',' || quote_literal('NeuroLup-' || cnt) || ',' || quote_literal('ChrKidDis-' || cnt) || ',' || quote_literal('EndstagekidDis-' || cnt) || ',' || quote_literal('KidTransp-' || cnt);
	else
		create_first_qu = create_first_qu || quote_literal('SLICCdmg-' || cnt) || ',' || quote_literal('slesevind-' || cnt) || ',' || quote_literal('NeuroLup-' || cnt) || ',' || quote_literal('ChrKidDis-' || cnt) || ',' || quote_literal('EndstagekidDis-' || cnt) || ',' || quote_literal('KidTransp-' || cnt) || ',';
	end if;
	
end loop;


create_first_qu = create_first_qu || ');' ;
execute create_first_qu;


	FOR count_rows IN select count(patient_cohort.pat_id) as count_rows from patient_cohort left join patient on patient.pat_id=patient_cohort.pat_id where patient.deleted=0 and patient_cohort.deleted=0
 	LOOP
	end loop;
 	FOR patient_record IN select patient_cohort.patient_cohort_id,patient_cohort.pat_id from patient_cohort left join patient on patient.pat_id=patient_cohort.pat_id 
	where patient.deleted=0 and patient_cohort.deleted=0
	-- for testing
	--and patient_cohort.pat_id=10001
 	LOOP
	
	krow1 = krow1 + 1;
	
	qu = 'INSERT INTO statistics_lk3b VALUES ( ' || krow1 ||  ',' || patient_record.patient_cohort_id || ',';	
	
	
	
		FOR drugs_id5 IN 0..26 LOOP 
	
			IF drugs_id5<=12 THEN
				cnt=drugs_id5*3;
			ELSE
				cnt=(drugs_id5-6)*6;
			END IF;
			
			FOR drugs_id4 IN select count(*) as cnt from patient_followup
			where deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=cnt
			LOOP
			end loop;
			
			IF drugs_id4.cnt>0 then
				FOR drugs_id1 IN select coalesce(patient_sle.sledai1,0) as sledai1,coalesce(patient_sle.sledai2,0) as sledai2,coalesce(patient_sle.sledai3,0) as sledai3,coalesce(patient_sle.sledai4,0) as sledai4,
				coalesce(patient_sle.sledai5,0) as sledai5,coalesce(patient_sle.sledai6,0) as sledai6,coalesce(patient_sle.sledai7,0) as sledai7,coalesce(patient_sle.sledai8,0) as sledai8,coalesce(patient_sle.sledai9,0) as sledai9,
				coalesce(patient_sle.sledai10,0) as sledai10,coalesce(patient_sle.sledai11,0) as sledai11,coalesce(patient_sle.sledai12,0) as sledai12,coalesce(patient_sle.sledai13,0) as sledai13,coalesce(patient_sle.sledai14,0) as sledai14,
				coalesce(patient_sle.sledai15,0) as sledai15,coalesce(patient_sle.sledai16,0) as sledai16,coalesce(patient_sle.sledai17,0) as sledai17,coalesce(patient_sle.sledai18,0) as sledai18,coalesce(patient_sle.sledai19,0) as sledai19,
				coalesce(patient_sle.sledai20,0) as sledai20,coalesce(patient_sle.sledai21,0) as sledai21,coalesce(patient_sle.sledai22,0) as sledai22,coalesce(patient_sle.sledai23,0) as sledai23,coalesce(patient_sle.sledai24,0) as sledai24,
				coalesce(patient_sle.sledai2k,0) as sledai2k,coalesce(patient_sle.flare14,0) as selena,coalesce(patient_sle.nefritis1,0) as cns,coalesce(patient_sle.nefritis2,'') as protein,
				coalesce(patient_sle_severity.slicc41,'') as SLICCdmg,coalesce(patient_sle_severity.index13,'') as slesevind,coalesce(patient_sle_severity.cns1,0) as cns1,coalesce(patient_sle_severity.cns2,0) as cns2,
				coalesce(patient_sle_severity.cns3,0) as cns3,coalesce(patient_sle_severity.cns4,0) as cns4
				from patient_followup
				left join patient_sle on patient_sle.patient_followup_id=patient_followup.patient_followup_id 
				left join patient_sle_severity on patient_sle_severity.patient_followup_id=patient_followup.patient_followup_id 
				where patient_followup.deleted=0
				and patient_followup.pat_id=patient_record.pat_id
				and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
				and patient_followup.fumonthcohort=cnt  limit 1
				LOOP
					qu = qu || quote_literal(drugs_id1.sledai1) || ',' || quote_literal(drugs_id1.sledai2) || ',' || quote_literal(drugs_id1.sledai3) || ',' || quote_literal(drugs_id1.sledai4) || ',' || quote_literal(drugs_id1.sledai5) || ',';
					qu = qu || quote_literal(drugs_id1.sledai6) || ',' || quote_literal(drugs_id1.sledai7) || ',' || quote_literal(drugs_id1.sledai8) || ',' || quote_literal(drugs_id1.sledai9) || ',' || quote_literal(drugs_id1.sledai10) || ',';
					qu = qu || quote_literal(drugs_id1.sledai11) || ',' || quote_literal(drugs_id1.sledai12) || ',' || quote_literal(drugs_id1.sledai13) || ',' || quote_literal(drugs_id1.sledai14) || ',' || quote_literal(drugs_id1.sledai15) || ',';
					qu = qu || quote_literal(drugs_id1.sledai16) || ',' || quote_literal(drugs_id1.sledai17) || ',' || quote_literal(drugs_id1.sledai18) || ',' || quote_literal(drugs_id1.sledai19) || ',' || quote_literal(drugs_id1.sledai20) || ',';
					qu = qu || quote_literal(drugs_id1.sledai21) || ',' || quote_literal(drugs_id1.sledai22) || ',' || quote_literal(drugs_id1.sledai23) || ',' || quote_literal(drugs_id1.sledai24) || ',' || quote_literal(drugs_id1.sledai2k) || ',';
					qu = qu || quote_literal(drugs_id1.selena) || ',' || quote_literal(drugs_id1.cns) || ',' || quote_literal(drugs_id1.protein) || ',';
					qu = qu || quote_literal(drugs_id1.SLICCdmg) || ',' || quote_literal(drugs_id1.slesevind) || ',' || quote_literal(drugs_id1.cns1) || ',' || quote_literal(drugs_id1.cns2) || ',' || quote_literal(drugs_id1.cns3) || ',';
					
					if cnt=120 then
						qu = qu || quote_literal(drugs_id1.cns4);
					else
						qu = qu || quote_literal(drugs_id1.cns4) || ',';
					end if;
					
				end loop;
			else
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
					
					if cnt=120 then
						qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt);
					else
						qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
					end if;
			end if;
			
		end loop;	
		
		
	qu = qu || ')';
	execute qu;
	end loop;
RETURN  1;
END; $BODY$;

ALTER FUNCTION public.create_insert_query_third_partb()
    OWNER TO postgres;
