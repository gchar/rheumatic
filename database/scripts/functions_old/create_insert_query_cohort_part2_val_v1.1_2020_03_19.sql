-- FUNCTION: public.create_insert_query_second_part_val()

-- DROP FUNCTION public.create_insert_query_second_part_val();

CREATE OR REPLACE FUNCTION public.create_insert_query_second_part_val(
	)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100000
    VOLATILE 
AS $BODY$DECLARE

 DECLARE

  patient_record RECORD;
  drugs_id3 RECORD;
  i int=1;
  krow2 int = 0;
  qu text; 
  create_qu text;
  mviews RECORD;  

 BEGIN 

	execute 'DROP TABLE IF EXISTS statistics_lk2_val;';
	create_qu = 'CREATE TABLE statistics_lk2_val (id integer,drug_id integer,current int,month integer,cohort_id integer,label varchar(100),value varchar(20));';
	execute create_qu;
	
  FOR patient_record IN select patient_cohort.patient_cohort_id,patient_cohort.pat_id from patient_cohort
  left join patient on patient.pat_id=patient_cohort.pat_id where patient.deleted=0 and patient_cohort.deleted=0 limit 1
    LOOP
	
		
  
	  --FOR drugs_id3 IN SELECT drugs_id FROM drugs WHERE deleted=0 and prim=0 and previous=0  order by drugs_id asc
		FOR drugs_id3 IN SELECT drugs.drugs_id,REPLACE (coalesce(drugs.code,''), ',', ' ') as code,
		 REPLACE (coalesce(drugs.substance,''), ',', ' ')  as substance,REPLACE (coalesce(lookup_tbl_val.value,''), ',', ' ')  as route_of_administration_val
		 FROM drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration
		 WHERE drugs.deleted=0 and drugs.prim=0 and drugs.previous=0 
		and drugs.code not in ('A9','A10','A11','B13','B14','E1','E2','E3','F1','J')  order by drugs.drugs_id asc
			loop
			
			
				
				krow2=krow2 + 1;
				qu = 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,0,' || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-0-current') || ',';
				qu = qu || quote_literal('');
				
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu ||  'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,3,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-3-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,6,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-6-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,12,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-12-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,18,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-18-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,24,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-24-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,30,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-30-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,36,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-36-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,42,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-42-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,48,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-48-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,54,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-54-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,60,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-60-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,66,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-66-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,72,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-72-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,78,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-78-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,84,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-84-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,90,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-90-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,96,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-96-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,102,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-102-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,108,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-108-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,114,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-114-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',1,120,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-120-current') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
				krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,0,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-0-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,3,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-3-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,6,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-6-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,12,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-12-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,18,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-18-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,24,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-24-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,30,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-30-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,36,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-36-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,42,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-42-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,48,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-48-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,54,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-54-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,60,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-60-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,66,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-66-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,72,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-72-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,78,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-78-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,84,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-84-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,90,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-90-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,96,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-96-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,102,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-102-3month') || ',' ;
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,108,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-108-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,114,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-114-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				--execute qu;
				
			krow2=krow2 + 1;
				qu = qu || 'INSERT INTO statistics_lk2_val VALUES ( ' || krow2 || ',' || drugs_id3.drugs_id || ',0,120,'  || patient_record.patient_cohort_id || ',' || quote_literal(drugs_id3.code || '-' || drugs_id3.substance || '-120-3month') || ',';
				qu = qu || quote_literal('');
				qu = qu || ');' ;
				execute qu;
		END LOOP;
	END LOOP;
 
RETURN  1;

END;   $BODY$;

ALTER FUNCTION public.create_insert_query_second_part_val()
    OWNER TO postgres;
