DECLARE
  krow int;
  patient_record RECORD;
  dflt character='';
  drugs_id1 RECORD;
  drugs_id2 RECORD;
  drugs_id3 RECORD;
  drugs_id4 RECORD;
  drugs_id5 int;
  count_rows RECORD;
  i int=1;
  krow1 int = 1;
  krow2 int = 0;
  qu text; 
  create_qu text;
  create_first_qu text;
 BEGIN 
 execute 'DROP TABLE IF EXISTS statistics_lk3;';
create_qu = 'CREATE TABLE statistics_lk3 (id integer,';
        FOR drugs_id4 IN 1..198 LOOP 
			create_qu = create_qu || 'a' || drugs_id4 ;
			if  drugs_id4=198 then
				create_qu = create_qu || ' VARCHAR(250)' ;
			else
				create_qu = create_qu || ' VARCHAR(250),' ;
			end if;
		end loop;
create_qu = create_qu || ')';
execute create_qu;

create_first_qu = 'INSERT INTO statistics_lk3 VALUES (1,';
create_first_qu = create_first_qu || quote_literal('28swollen_JC-0') || ',' || quote_literal('28tender_JC-0') || ',' || quote_literal('BASDAI-0') || ',' || quote_literal('BASDAI1-0') || ',' || quote_literal('BASDAI2-0') || ',' || quote_literal('BASDAI3-0') || ',' || quote_literal('BASDAI4-0') || ',' || quote_literal('BASDAI5-0') || ',' || quote_literal('BASDAI6-0') || ',' || quote_literal('BASFI-0') || ',' || quote_literal('CRP-0') || ',' || quote_literal('DAS28ESR-0') || ',' || quote_literal('DAS28CRP-0') || ',' || quote_literal('ESR-0') || ',';
create_first_qu = create_first_qu || quote_literal('VAS_physisian-0') || ',' || quote_literal('Euroqol_total-0') || ',' || quote_literal('EQ_5D_score-0') || ',' || quote_literal('mHAQ-0') || ',' || quote_literal('VASglobal-0') || ',' || quote_literal('VASpain-0') || ',' || quote_literal('Weight-0') || ',' || quote_literal('Height-0') || ',';
create_first_qu = create_first_qu || quote_literal('28swollen_JC-3') || ',' || quote_literal('28tender_JC-3') || ',' || quote_literal('BASDAI-3') || ',' || quote_literal('BASDAI1-3') || ',' || quote_literal('BASDAI2-3') || ',' || quote_literal('BASDAI3-3') || ',' || quote_literal('BASDAI4-3') || ',' || quote_literal('BASDAI5-3') || ',' || quote_literal('BASDAI6-3') || ',' || quote_literal('BASFI-3') || ',' || quote_literal('CRP-3') || ',' || quote_literal('DAS28ESR-3') ||  ',' || quote_literal('DAS28CRP-3') || ',' || quote_literal('ESR-3') || ',';
create_first_qu = create_first_qu || quote_literal('VAS_physisian-3') || ',' || quote_literal('Euroqol_total-3') || ',' || quote_literal('EQ_5D_score-3') || ',' || quote_literal('mHAQ-3') || ',' || quote_literal('VASglobal-3') || ',' || quote_literal('VASpain-3') || ',' || quote_literal('Weight-3') || ',' || quote_literal('Height-3') || ',';
create_first_qu = create_first_qu || quote_literal('28swollen_JC-6') || ',' || quote_literal('28tender_JC-6') || ',' || quote_literal('BASDAI-6') || ',' || quote_literal('BASDAI1-6') || ',' || quote_literal('BASDAI2-6') || ',' || quote_literal('BASDAI3-6') || ',' || quote_literal('BASDAI4-6') || ',' || quote_literal('BASDAI5-6') || ',' || quote_literal('BASDAI6-6') || ',' || quote_literal('BASFI-6') || ',' || quote_literal('CRP-6') || ',' || quote_literal('DAS28ESR-6') ||  ',' || quote_literal('DAS28CRP-6') || ',' || quote_literal('ESR-6') || ',';
create_first_qu = create_first_qu || quote_literal('VAS_physisian-6') || ',' || quote_literal('Euroqol_total-6') || ',' || quote_literal('EQ_5D_score-6') || ',' || quote_literal('mHAQ-6') || ',' || quote_literal('VASglobal-6') || ',' || quote_literal('VASpain-6') || ',' || quote_literal('Weight-6') || ',' || quote_literal('Height-6') || ',';
create_first_qu = create_first_qu || quote_literal('28swollen_JC-12') || ',' || quote_literal('28tender_JC-12') || ',' || quote_literal('BASDAI-12') || ',' || quote_literal('BASDAI1-12') || ',' || quote_literal('BASDAI2-12') || ',' || quote_literal('BASDAI3-12') || ',' || quote_literal('BASDAI4-12') || ',' || quote_literal('BASDAI5-12') || ',' || quote_literal('BASDAI6-12') || ',' || quote_literal('BASFI-12') || ',' || quote_literal('CRP-12') || ',' || quote_literal('DAS28ESR-12')  || ',' || quote_literal('DAS28CRP-12') || ',' || quote_literal('ESR-12') || ',';
create_first_qu = create_first_qu || quote_literal('VAS_physisian-12') || ',' || quote_literal('Euroqol_total-12') || ',' || quote_literal('EQ_5D_score-12') || ',' || quote_literal('mHAQ-12') || ',' || quote_literal('VASglobal-12') || ',' || quote_literal('VASpain-12') || ',' || quote_literal('Weight-12') || ',' || quote_literal('Height-12') || ',';
create_first_qu = create_first_qu || quote_literal('28swollen JC-18') || ',' || quote_literal('28tender JC-18') || ',' || quote_literal('BASDAI-18') || ',' || quote_literal('BASDAI1-18') || ',' || quote_literal('BASDAI2-18') || ',' || quote_literal('BASDAI3-18') || ',' || quote_literal('BASDAI4-18') || ',' || quote_literal('BASDAI5-18') || ',' || quote_literal('BASDAI6-18') || ',' || quote_literal('BASFI-18') || ',' || quote_literal('CRP-18') || ',' || quote_literal('DAS28ESR-18')  || ',' || quote_literal('DAS28CRP-18') || ',' || quote_literal('ESR-18') || ',';
create_first_qu = create_first_qu || quote_literal('VAS physisian-18') || ',' || quote_literal('Euroqol total-18') || ',' || quote_literal('EQ 5D score-18') || ',' || quote_literal('mHAQ-18') || ',' || quote_literal('VASglobal-18') || ',' || quote_literal('VASpain-18') || ',' || quote_literal('Weight-18') || ',' || quote_literal('Height-18') || ',';
create_first_qu = create_first_qu || quote_literal('28swollen JC-24') || ',' || quote_literal('28tender JC-24') || ',' || quote_literal('BASDAI-24') || ',' || quote_literal('BASDAI1-24') || ',' || quote_literal('BASDAI2-24') || ',' || quote_literal('BASDAI3-24') || ',' || quote_literal('BASDAI4-24') || ',' || quote_literal('BASDAI5-24') || ',' || quote_literal('BASDAI6-24') || ',' || quote_literal('BASFI-24') || ',' || quote_literal('CRP-24') || ',' || quote_literal('DAS28ESR-24')  || ',' || quote_literal('DAS28CRP-24') || ',' || quote_literal('ESR-24') || ',';
create_first_qu = create_first_qu || quote_literal('VAS physisian-24') || ',' || quote_literal('Euroqol total-24') || ',' || quote_literal('EQ 5D score-24') || ',' || quote_literal('mHAQ-24') || ',' || quote_literal('VASglobal-24') || ',' || quote_literal('VASpain-24') || ',' || quote_literal('Weight-24') || ',' || quote_literal('Height-24') || ',';
create_first_qu = create_first_qu || quote_literal('28swollen JC-36') || ',' || quote_literal('28tender JC-36') || ',' || quote_literal('BASDAI-36') || ',' || quote_literal('BASDAI1-36') || ',' || quote_literal('BASDAI2-36') || ',' || quote_literal('BASDAI3-36') || ',' || quote_literal('BASDAI4-36') || ',' || quote_literal('BASDAI5-36') || ',' || quote_literal('BASDAI6-36') || ',' || quote_literal('BASFI-36') || ',' || quote_literal('CRP-36') || ',' || quote_literal('DAS28ESR-36')  || ',' || quote_literal('DAS28CRP-36') || ',' || quote_literal('ESR-36') || ',';
create_first_qu = create_first_qu || quote_literal('VAS physisian-36') || ',' || quote_literal('Euroqol total-36') || ',' || quote_literal('EQ 5D score-36') || ',' || quote_literal('mHAQ-36') || ',' || quote_literal('VASglobal-36') || ',' || quote_literal('VASpain-36') || ',' || quote_literal('Weight-36') || ',' || quote_literal('Height-36') || ',';
create_first_qu = create_first_qu || quote_literal('28swollen JC-48') || ',' || quote_literal('28tender JC-48') || ',' || quote_literal('BASDAI-48') || ',' || quote_literal('BASDAI1-48') || ',' || quote_literal('BASDAI2-48') || ',' || quote_literal('BASDAI3-48') || ',' || quote_literal('BASDAI4-48') || ',' || quote_literal('BASDAI5-48') || ',' || quote_literal('BASDAI6-48') || ',' || quote_literal('BASFI-48') || ',' || quote_literal('CRP-48') || ',' || quote_literal('DAS28ESR-48')  || ',' || quote_literal('DAS28CRP-48') || ',' || quote_literal('ESR-48') || ',';
create_first_qu = create_first_qu || quote_literal('VAS physisian-48') || ',' || quote_literal('Euroqol total-48') || ',' || quote_literal('EQ 5D score-48') || ',' || quote_literal('mHAQ-48') || ',' || quote_literal('VASglobal-48') || ',' || quote_literal('VASpain-48') || ',' || quote_literal('Weight-48') || ',' || quote_literal('Height-48') || ',';
create_first_qu = create_first_qu || quote_literal('28swollen JC-60') || ',' || quote_literal('28tender JC-60') || ',' || quote_literal('BASDAI-60') || ',' || quote_literal('BASDAI1-60') || ',' || quote_literal('BASDAI2-60') || ',' || quote_literal('BASDAI3-60') || ',' || quote_literal('BASDAI4-60') || ',' || quote_literal('BASDAI5-60') || ',' || quote_literal('BASDAI6-60') || ',' || quote_literal('BASFI-60') || ',' || quote_literal('CRP-60') || ',' || quote_literal('DAS28ESR-60')  || ',' || quote_literal('DAS28CRP-60') || ',' || quote_literal('ESR-60') || ',';
create_first_qu = create_first_qu || quote_literal('VAS physisian-60') || ',' || quote_literal('Euroqol total-60') || ',' || quote_literal('EQ 5D score-60') || ',' || quote_literal('mHAQ-60') || ',' || quote_literal('VASglobal-60') || ',' || quote_literal('VASpain-60') || ',' || quote_literal('Weight-60') || ',' || quote_literal('Height-60');
create_first_qu = create_first_qu || ');' ;
execute create_first_qu;
	FOR count_rows IN select count(patient_cohort.pat_id) as count_rows from patient_cohort left join patient on patient.pat_id=patient_cohort.pat_id where patient.deleted=0 and patient_cohort.deleted=0  limit 3
 	LOOP
	end loop;
 	FOR patient_record IN select patient_cohort.patient_cohort_id,patient_cohort.pat_id from patient_cohort left join patient on patient.pat_id=patient_cohort.pat_id  limit 3
	where patient.deleted=0 and patient_cohort.deleted=0
 	LOOP
	krow1 = krow1 + 1;
	qu = 'INSERT INTO statistics_lk3 VALUES ( ' || krow1 ||  ',' ;	
		
		FOR drugs_id4 IN select count(patient_followup.*) as cnt from patient_followup
		left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
		left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
		left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
		where patient_followup.deleted=0
		and patient_common.deleted=0
		and patient_followup.pat_id=patient_record.pat_id
		and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
		and patient_followup.fumonthcohort=0 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(patient_common.swollenjc28,'') as swollenjc28 ,coalesce(patient_common.tenderjc28,'') as tenderjc28 ,coalesce(patient_common.crp,'') as crp ,
			coalesce(patient_common.das28crp,'') as das28crp , coalesce(patient_common.das28esr,'') as das28esr ,coalesce(patient_common.esr,'') as esr ,coalesce(patient_common.vasphysicial,'') as vasphysicial ,
			coalesce(patient_common.mhaq,'') as mhaq ,coalesce(patient_common.vasglobal,'') as vasglobal ,coalesce(patient_common.vaspain,'') as vaspain ,coalesce(patient_common.weight,'') as weight ,
			coalesce(patient_common.height,'') as height ,coalesce(patient_spa.basdai,'') as basdai ,coalesce(patient_spa.basdai1,'') as basdai1 ,coalesce(patient_spa.basdai2,'') as basdai2 ,
			coalesce(patient_spa.basdai3,'') as basdai3 ,coalesce(patient_spa.basdai4,'') as basdai4 ,coalesce(patient_spa.basdai5,'') as basdai5 ,coalesce(patient_spa.basdai6,'') as basdai6 ,
			coalesce(patient_spa.basfi,'') as basfi ,coalesce(patient_euroqol.euroqol_score,0) as euroqol_score ,REPLACE (coalesce(patient_euroqol.eq5d,''), ',', '.') as eq5d
			from patient_followup
			left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
			left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
			left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
			where patient_followup.deleted=0
			and patient_common.deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=0  limit 1
			LOOP
				qu = qu || quote_literal(drugs_id1.swollenjc28) || ',' || quote_literal(drugs_id1.tenderjc28) || ',' || quote_literal(drugs_id1.basdai) || ',' || quote_literal(drugs_id1.basdai1) || ',' || quote_literal(drugs_id1.basdai2) || ',' || quote_literal(drugs_id1.basdai3) || ',' || quote_literal(drugs_id1.basdai4) || ',';
				qu = qu || quote_literal(drugs_id1.basdai5) || ',' || quote_literal(drugs_id1.basdai6) || ',' || quote_literal(drugs_id1.basfi) || ',' || quote_literal(drugs_id1.crp) || ',' || quote_literal(drugs_id1.das28esr) || ',' || quote_literal(drugs_id1.das28crp) || ',' || quote_literal(drugs_id1.esr) || ',' || quote_literal(drugs_id1.vasphysicial) || ',';
				qu = qu || quote_literal(drugs_id1.euroqol_score) || ',' || quote_literal(drugs_id1.eq5d) || ',' || quote_literal(drugs_id1.mhaq) || ',' || quote_literal(drugs_id1.vasglobal) || ',' || quote_literal(drugs_id1.vaspain) || ',' || quote_literal(drugs_id1.weight) || ',' || quote_literal(drugs_id1.height) || ',';
			end loop;
		else
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
		end if;
		FOR drugs_id4 IN select count(patient_followup.*) as cnt from patient_followup
		left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
		left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
		left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
		where patient_followup.deleted=0
		and patient_common.deleted=0
		and patient_followup.pat_id=patient_record.pat_id
		and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
		and patient_followup.fumonthcohort=3 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(patient_common.swollenjc28,'') as swollenjc28 ,coalesce(patient_common.tenderjc28,'') as tenderjc28 ,coalesce(patient_common.crp,'') as crp ,
			coalesce(patient_common.das28crp,'') as das28crp , coalesce(patient_common.das28esr,'') as das28esr ,coalesce(patient_common.esr,'') as esr ,coalesce(patient_common.vasphysicial,'') as vasphysicial ,
			coalesce(patient_common.mhaq,'') as mhaq ,coalesce(patient_common.vasglobal,'') as vasglobal ,coalesce(patient_common.vaspain,'') as vaspain ,coalesce(patient_common.weight,'') as weight ,
			coalesce(patient_common.height,'') as height ,coalesce(patient_spa.basdai,'') as basdai ,coalesce(patient_spa.basdai1,'') as basdai1 ,coalesce(patient_spa.basdai2,'') as basdai2 ,
			coalesce(patient_spa.basdai3,'') as basdai3 ,coalesce(patient_spa.basdai4,'') as basdai4 ,coalesce(patient_spa.basdai5,'') as basdai5 ,coalesce(patient_spa.basdai6,'') as basdai6 ,
			coalesce(patient_spa.basfi,'') as basfi ,coalesce(patient_euroqol.euroqol_score,0) as euroqol_score ,REPLACE (coalesce(patient_euroqol.eq5d,''), ',', '.') as eq5d
			from patient_followup
			left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
			left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
			left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
			where patient_followup.deleted=0
			and patient_common.deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=3  limit 1
			LOOP
				qu = qu || quote_literal(drugs_id1.swollenjc28) || ',' || quote_literal(drugs_id1.tenderjc28) || ',' || quote_literal(drugs_id1.basdai) || ',' || quote_literal(drugs_id1.basdai1) || ',' || quote_literal(drugs_id1.basdai2) || ',' || quote_literal(drugs_id1.basdai3) || ',' || quote_literal(drugs_id1.basdai4) || ',';
				qu = qu || quote_literal(drugs_id1.basdai5) || ',' || quote_literal(drugs_id1.basdai6) || ',' || quote_literal(drugs_id1.basfi) || ',' || quote_literal(drugs_id1.crp) || ',' || quote_literal(drugs_id1.das28esr) || ',' || quote_literal(drugs_id1.das28crp) || ',' || quote_literal(drugs_id1.esr) || ',' || quote_literal(drugs_id1.vasphysicial) || ',';
				qu = qu || quote_literal(drugs_id1.euroqol_score) || ',' || quote_literal(drugs_id1.eq5d) || ',' || quote_literal(drugs_id1.mhaq) || ',' || quote_literal(drugs_id1.vasglobal) || ',' || quote_literal(drugs_id1.vaspain) || ',' || quote_literal(drugs_id1.weight) || ',' || quote_literal(drugs_id1.height) || ',';
			end loop;
		else
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
		end if;
		FOR drugs_id4 IN select count(patient_followup.*) as cnt from patient_followup
		left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
		left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
		left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
		where patient_followup.deleted=0
		and patient_common.deleted=0
		and patient_followup.pat_id=patient_record.pat_id
		and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
		and patient_followup.fumonthcohort=6
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(patient_common.swollenjc28,'') as swollenjc28 ,coalesce(patient_common.tenderjc28,'') as tenderjc28 ,coalesce(patient_common.crp,'') as crp ,
			coalesce(patient_common.das28crp,'') as das28crp , coalesce(patient_common.das28esr,'') as das28esr ,coalesce(patient_common.esr,'') as esr ,coalesce(patient_common.vasphysicial,'') as vasphysicial ,
			coalesce(patient_common.mhaq,'') as mhaq ,coalesce(patient_common.vasglobal,'') as vasglobal ,coalesce(patient_common.vaspain,'') as vaspain ,coalesce(patient_common.weight,'') as weight ,
			coalesce(patient_common.height,'') as height ,coalesce(patient_spa.basdai,'') as basdai ,coalesce(patient_spa.basdai1,'') as basdai1 ,coalesce(patient_spa.basdai2,'') as basdai2 ,
			coalesce(patient_spa.basdai3,'') as basdai3 ,coalesce(patient_spa.basdai4,'') as basdai4 ,coalesce(patient_spa.basdai5,'') as basdai5 ,coalesce(patient_spa.basdai6,'') as basdai6 ,
			coalesce(patient_spa.basfi,'') as basfi ,coalesce(patient_euroqol.euroqol_score,0) as euroqol_score ,REPLACE (coalesce(patient_euroqol.eq5d,''), ',', '.') as eq5d
			from patient_followup
			left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
			left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
			left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
			where patient_followup.deleted=0
			and patient_common.deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=6  limit 1
			LOOP
				qu = qu || quote_literal(drugs_id1.swollenjc28) || ',' || quote_literal(drugs_id1.tenderjc28) || ',' || quote_literal(drugs_id1.basdai) || ',' || quote_literal(drugs_id1.basdai1) || ',' || quote_literal(drugs_id1.basdai2) || ',' || quote_literal(drugs_id1.basdai3) || ',' || quote_literal(drugs_id1.basdai4) || ',';
				qu = qu || quote_literal(drugs_id1.basdai5) || ',' || quote_literal(drugs_id1.basdai6) || ',' || quote_literal(drugs_id1.basfi) || ',' || quote_literal(drugs_id1.crp) || ',' || quote_literal(drugs_id1.das28esr) || ',' || quote_literal(drugs_id1.das28crp) || ',' || quote_literal(drugs_id1.esr) || ',' || quote_literal(drugs_id1.vasphysicial) || ',';
				qu = qu || quote_literal(drugs_id1.euroqol_score) || ',' || quote_literal(drugs_id1.eq5d) || ',' || quote_literal(drugs_id1.mhaq) || ',' || quote_literal(drugs_id1.vasglobal) || ',' || quote_literal(drugs_id1.vaspain) || ',' || quote_literal(drugs_id1.weight) || ',' || quote_literal(drugs_id1.height) || ',';
			end loop;
		else
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
		end if;
		FOR drugs_id4 IN select count(patient_followup.*) as cnt from patient_followup
		left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
		left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
		left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
		where patient_followup.deleted=0
		and patient_common.deleted=0
		and patient_followup.pat_id=patient_record.pat_id
		and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
		and patient_followup.fumonthcohort=12 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(patient_common.swollenjc28,'') as swollenjc28 ,coalesce(patient_common.tenderjc28,'') as tenderjc28 ,coalesce(patient_common.crp,'') as crp ,
			coalesce(patient_common.das28crp,'') as das28crp , coalesce(patient_common.das28esr,'') as das28esr ,coalesce(patient_common.esr,'') as esr ,coalesce(patient_common.vasphysicial,'') as vasphysicial ,
			coalesce(patient_common.mhaq,'') as mhaq ,coalesce(patient_common.vasglobal,'') as vasglobal ,coalesce(patient_common.vaspain,'') as vaspain ,coalesce(patient_common.weight,'') as weight ,
			coalesce(patient_common.height,'') as height ,coalesce(patient_spa.basdai,'') as basdai ,coalesce(patient_spa.basdai1,'') as basdai1 ,coalesce(patient_spa.basdai2,'') as basdai2 ,
			coalesce(patient_spa.basdai3,'') as basdai3 ,coalesce(patient_spa.basdai4,'') as basdai4 ,coalesce(patient_spa.basdai5,'') as basdai5 ,coalesce(patient_spa.basdai6,'') as basdai6 ,
			coalesce(patient_spa.basfi,'') as basfi ,coalesce(patient_euroqol.euroqol_score,0) as euroqol_score ,REPLACE (coalesce(patient_euroqol.eq5d,''), ',', '.') as eq5d
			from patient_followup
			left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
			left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
			left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
			where patient_followup.deleted=0
			and patient_common.deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=12  limit 1
			LOOP
				qu = qu || quote_literal(drugs_id1.swollenjc28) || ',' || quote_literal(drugs_id1.tenderjc28) || ',' || quote_literal(drugs_id1.basdai) || ',' || quote_literal(drugs_id1.basdai1) || ',' || quote_literal(drugs_id1.basdai2) || ',' || quote_literal(drugs_id1.basdai3) || ',' || quote_literal(drugs_id1.basdai4) || ',';
				qu = qu || quote_literal(drugs_id1.basdai5) || ',' || quote_literal(drugs_id1.basdai6) || ',' || quote_literal(drugs_id1.basfi) || ',' || quote_literal(drugs_id1.crp) || ',' || quote_literal(drugs_id1.das28esr) || ',' || quote_literal(drugs_id1.das28crp) || ',' || quote_literal(drugs_id1.esr) || ',' || quote_literal(drugs_id1.vasphysicial) || ',';
				qu = qu || quote_literal(drugs_id1.euroqol_score) || ',' || quote_literal(drugs_id1.eq5d) || ',' || quote_literal(drugs_id1.mhaq) || ',' || quote_literal(drugs_id1.vasglobal) || ',' || quote_literal(drugs_id1.vaspain) || ',' || quote_literal(drugs_id1.weight) || ',' || quote_literal(drugs_id1.height) || ',';
			end loop;
		else
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
		end if;
		FOR drugs_id4 IN select count(patient_followup.*) as cnt from patient_followup
		left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
		left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
		left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
		where patient_followup.deleted=0
		and patient_common.deleted=0
		and patient_followup.pat_id=patient_record.pat_id
		and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
		and patient_followup.fumonthcohort=18 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(patient_common.swollenjc28,'') as swollenjc28 ,coalesce(patient_common.tenderjc28,'') as tenderjc28 ,coalesce(patient_common.crp,'') as crp ,
			coalesce(patient_common.das28crp,'') as das28crp , coalesce(patient_common.das28esr,'') as das28esr ,coalesce(patient_common.esr,'') as esr ,coalesce(patient_common.vasphysicial,'') as vasphysicial ,
			coalesce(patient_common.mhaq,'') as mhaq ,coalesce(patient_common.vasglobal,'') as vasglobal ,coalesce(patient_common.vaspain,'') as vaspain ,coalesce(patient_common.weight,'') as weight ,
			coalesce(patient_common.height,'') as height ,coalesce(patient_spa.basdai,'') as basdai ,coalesce(patient_spa.basdai1,'') as basdai1 ,coalesce(patient_spa.basdai2,'') as basdai2 ,
			coalesce(patient_spa.basdai3,'') as basdai3 ,coalesce(patient_spa.basdai4,'') as basdai4 ,coalesce(patient_spa.basdai5,'') as basdai5 ,coalesce(patient_spa.basdai6,'') as basdai6 ,
			coalesce(patient_spa.basfi,'') as basfi ,coalesce(patient_euroqol.euroqol_score,0) as euroqol_score ,REPLACE (coalesce(patient_euroqol.eq5d,''), ',', '.') as eq5d
			from patient_followup
			left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
			left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
			left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
			where patient_followup.deleted=0
			and patient_common.deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=18  limit 1
			LOOP
				qu = qu || quote_literal(drugs_id1.swollenjc28) || ',' || quote_literal(drugs_id1.tenderjc28) || ',' || quote_literal(drugs_id1.basdai) || ',' || quote_literal(drugs_id1.basdai1) || ',' || quote_literal(drugs_id1.basdai2) || ',' || quote_literal(drugs_id1.basdai3) || ',' || quote_literal(drugs_id1.basdai4) || ',';
				qu = qu || quote_literal(drugs_id1.basdai5) || ',' || quote_literal(drugs_id1.basdai6) || ',' || quote_literal(drugs_id1.basfi) || ',' || quote_literal(drugs_id1.crp) || ',' || quote_literal(drugs_id1.das28esr) || ',' || quote_literal(drugs_id1.das28crp) || ',' || quote_literal(drugs_id1.esr) || ',' || quote_literal(drugs_id1.vasphysicial) || ',';
				qu = qu || quote_literal(drugs_id1.euroqol_score) || ',' || quote_literal(drugs_id1.eq5d) || ',' || quote_literal(drugs_id1.mhaq) || ',' || quote_literal(drugs_id1.vasglobal) || ',' || quote_literal(drugs_id1.vaspain) || ',' || quote_literal(drugs_id1.weight) || ',' || quote_literal(drugs_id1.height) || ',';
			end loop;
		else
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
		end if;
		FOR drugs_id4 IN select count(patient_followup.*) as cnt from patient_followup
		left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
		left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
		left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
		where patient_followup.deleted=0
		and patient_common.deleted=0
		and patient_followup.pat_id=patient_record.pat_id
		and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
		and patient_followup.fumonthcohort=24 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(patient_common.swollenjc28,'') as swollenjc28 ,coalesce(patient_common.tenderjc28,'') as tenderjc28 ,coalesce(patient_common.crp,'') as crp ,
			coalesce(patient_common.das28crp,'') as das28crp , coalesce(patient_common.das28esr,'') as das28esr ,coalesce(patient_common.esr,'') as esr ,coalesce(patient_common.vasphysicial,'') as vasphysicial ,
			coalesce(patient_common.mhaq,'') as mhaq ,coalesce(patient_common.vasglobal,'') as vasglobal ,coalesce(patient_common.vaspain,'') as vaspain ,coalesce(patient_common.weight,'') as weight ,
			coalesce(patient_common.height,'') as height ,coalesce(patient_spa.basdai,'') as basdai ,coalesce(patient_spa.basdai1,'') as basdai1 ,coalesce(patient_spa.basdai2,'') as basdai2 ,
			coalesce(patient_spa.basdai3,'') as basdai3 ,coalesce(patient_spa.basdai4,'') as basdai4 ,coalesce(patient_spa.basdai5,'') as basdai5 ,coalesce(patient_spa.basdai6,'') as basdai6 ,
			coalesce(patient_spa.basfi,'') as basfi ,coalesce(patient_euroqol.euroqol_score,0) as euroqol_score ,REPLACE (coalesce(patient_euroqol.eq5d,''), ',', '.') as eq5d
			from patient_followup
			left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
			left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
			left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
			where patient_followup.deleted=0
			and patient_common.deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=24  limit 1
			LOOP
				qu = qu || quote_literal(drugs_id1.swollenjc28) || ',' || quote_literal(drugs_id1.tenderjc28) || ',' || quote_literal(drugs_id1.basdai) || ',' || quote_literal(drugs_id1.basdai1) || ',' || quote_literal(drugs_id1.basdai2) || ',' || quote_literal(drugs_id1.basdai3) || ',' || quote_literal(drugs_id1.basdai4) || ',';
				qu = qu || quote_literal(drugs_id1.basdai5) || ',' || quote_literal(drugs_id1.basdai6) || ',' || quote_literal(drugs_id1.basfi) || ',' || quote_literal(drugs_id1.crp) || ',' || quote_literal(drugs_id1.das28esr) || ',' || quote_literal(drugs_id1.das28crp) || ',' || quote_literal(drugs_id1.esr) || ',' || quote_literal(drugs_id1.vasphysicial) || ',';
				qu = qu || quote_literal(drugs_id1.euroqol_score) || ',' || quote_literal(drugs_id1.eq5d) || ',' || quote_literal(drugs_id1.mhaq) || ',' || quote_literal(drugs_id1.vasglobal) || ',' || quote_literal(drugs_id1.vaspain) || ',' || quote_literal(drugs_id1.weight) || ',' || quote_literal(drugs_id1.height) || ',';
			end loop;
		else
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
		end if;
		FOR drugs_id4 IN select count(patient_followup.*) as cnt from patient_followup
		left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
		left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
		left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
		where patient_followup.deleted=0
		and patient_common.deleted=0
		and patient_followup.pat_id=patient_record.pat_id
		and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
		and patient_followup.fumonthcohort=36 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(patient_common.swollenjc28,'') as swollenjc28 ,coalesce(patient_common.tenderjc28,'') as tenderjc28 ,coalesce(patient_common.crp,'') as crp ,
			coalesce(patient_common.das28crp,'') as das28crp , coalesce(patient_common.das28esr,'') as das28esr ,coalesce(patient_common.esr,'') as esr ,coalesce(patient_common.vasphysicial,'') as vasphysicial ,
			coalesce(patient_common.mhaq,'') as mhaq ,coalesce(patient_common.vasglobal,'') as vasglobal ,coalesce(patient_common.vaspain,'') as vaspain ,coalesce(patient_common.weight,'') as weight ,
			coalesce(patient_common.height,'') as height ,coalesce(patient_spa.basdai,'') as basdai ,coalesce(patient_spa.basdai1,'') as basdai1 ,coalesce(patient_spa.basdai2,'') as basdai2 ,
			coalesce(patient_spa.basdai3,'') as basdai3 ,coalesce(patient_spa.basdai4,'') as basdai4 ,coalesce(patient_spa.basdai5,'') as basdai5 ,coalesce(patient_spa.basdai6,'') as basdai6 ,
			coalesce(patient_spa.basfi,'') as basfi ,coalesce(patient_euroqol.euroqol_score,0) as euroqol_score ,REPLACE (coalesce(patient_euroqol.eq5d,''), ',', '.') as eq5d
			from patient_followup
			left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
			left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
			left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
			where patient_followup.deleted=0
			and patient_common.deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=36  limit 1
			LOOP
				qu = qu || quote_literal(drugs_id1.swollenjc28) || ',' || quote_literal(drugs_id1.tenderjc28) || ',' || quote_literal(drugs_id1.basdai) || ',' || quote_literal(drugs_id1.basdai1) || ',' || quote_literal(drugs_id1.basdai2) || ',' || quote_literal(drugs_id1.basdai3) || ',' || quote_literal(drugs_id1.basdai4) || ',';
				qu = qu || quote_literal(drugs_id1.basdai5) || ',' || quote_literal(drugs_id1.basdai6) || ',' || quote_literal(drugs_id1.basfi) || ',' || quote_literal(drugs_id1.crp) || ',' || quote_literal(drugs_id1.das28esr) || ',' || quote_literal(drugs_id1.das28crp) || ',' || quote_literal(drugs_id1.esr) || ',' || quote_literal(drugs_id1.vasphysicial) || ',';
				qu = qu || quote_literal(drugs_id1.euroqol_score) || ',' || quote_literal(drugs_id1.eq5d) || ',' || quote_literal(drugs_id1.mhaq) || ',' || quote_literal(drugs_id1.vasglobal) || ',' || quote_literal(drugs_id1.vaspain) || ',' || quote_literal(drugs_id1.weight) || ',' || quote_literal(drugs_id1.height) || ',';
			end loop;
		else
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
		end if;
		FOR drugs_id4 IN select count(patient_followup.*) as cnt from patient_followup
		left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
		left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
		left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
		where patient_followup.deleted=0
		and patient_common.deleted=0
		and patient_followup.pat_id=patient_record.pat_id
		and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
		and patient_followup.fumonthcohort=48 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(patient_common.swollenjc28,'') as swollenjc28 ,coalesce(patient_common.tenderjc28,'') as tenderjc28 ,coalesce(patient_common.crp,'') as crp ,
			coalesce(patient_common.das28crp,'') as das28crp , coalesce(patient_common.das28esr,'') as das28esr ,coalesce(patient_common.esr,'') as esr ,coalesce(patient_common.vasphysicial,'') as vasphysicial ,
			coalesce(patient_common.mhaq,'') as mhaq ,coalesce(patient_common.vasglobal,'') as vasglobal ,coalesce(patient_common.vaspain,'') as vaspain ,coalesce(patient_common.weight,'') as weight ,
			coalesce(patient_common.height,'') as height ,coalesce(patient_spa.basdai,'') as basdai ,coalesce(patient_spa.basdai1,'') as basdai1 ,coalesce(patient_spa.basdai2,'') as basdai2 ,
			coalesce(patient_spa.basdai3,'') as basdai3 ,coalesce(patient_spa.basdai4,'') as basdai4 ,coalesce(patient_spa.basdai5,'') as basdai5 ,coalesce(patient_spa.basdai6,'') as basdai6 ,
			coalesce(patient_spa.basfi,'') as basfi ,coalesce(patient_euroqol.euroqol_score,0) as euroqol_score ,REPLACE (coalesce(patient_euroqol.eq5d,''), ',', '.') as eq5d
			from patient_followup
			left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
			left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
			left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
			where patient_followup.deleted=0
			and patient_common.deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=48  limit 1 
			LOOP
				qu = qu || quote_literal(drugs_id1.swollenjc28) || ',' || quote_literal(drugs_id1.tenderjc28) || ',' || quote_literal(drugs_id1.basdai) || ',' || quote_literal(drugs_id1.basdai1) || ',' || quote_literal(drugs_id1.basdai2) || ',' || quote_literal(drugs_id1.basdai3) || ',' || quote_literal(drugs_id1.basdai4) || ',';
				qu = qu || quote_literal(drugs_id1.basdai5) || ',' || quote_literal(drugs_id1.basdai6) || ',' || quote_literal(drugs_id1.basfi) || ',' || quote_literal(drugs_id1.crp) || ',' || quote_literal(drugs_id1.das28esr) || ',' || quote_literal(drugs_id1.das28crp) || ',' || quote_literal(drugs_id1.esr) || ',' || quote_literal(drugs_id1.vasphysicial) || ',';
				qu = qu || quote_literal(drugs_id1.euroqol_score) || ',' || quote_literal(drugs_id1.eq5d) || ',' || quote_literal(drugs_id1.mhaq) || ',' || quote_literal(drugs_id1.vasglobal) || ',' || quote_literal(drugs_id1.vaspain) || ',' || quote_literal(drugs_id1.weight) || ',' || quote_literal(drugs_id1.height) || ',';
			end loop;
		else
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';		
		end if;
		FOR drugs_id4 IN select count(patient_followup.*) as cnt from patient_followup
		left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
		left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
		left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
		where patient_followup.deleted=0
		and patient_common.deleted=0
		and patient_followup.pat_id=patient_record.pat_id
		and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
		and patient_followup.fumonthcohort=60 
		LOOP
		end loop;
		IF drugs_id4.cnt>0 then
			FOR drugs_id1 IN select coalesce(patient_common.swollenjc28,'') as swollenjc28 ,coalesce(patient_common.tenderjc28,'') as tenderjc28 ,coalesce(patient_common.crp,'') as crp ,
			coalesce(patient_common.das28crp,'') as das28crp , coalesce(patient_common.das28esr,'') as das28esr ,coalesce(patient_common.esr,'') as esr ,coalesce(patient_common.vasphysicial,'') as vasphysicial ,
			coalesce(patient_common.mhaq,'') as mhaq ,coalesce(patient_common.vasglobal,'') as vasglobal ,coalesce(patient_common.vaspain,'') as vaspain ,coalesce(patient_common.weight,'') as weight ,
			coalesce(patient_common.height,'') as height ,coalesce(patient_spa.basdai,'') as basdai ,coalesce(patient_spa.basdai1,'') as basdai1 ,coalesce(patient_spa.basdai2,'') as basdai2 ,
			coalesce(patient_spa.basdai3,'') as basdai3 ,coalesce(patient_spa.basdai4,'') as basdai4 ,coalesce(patient_spa.basdai5,'') as basdai5 ,coalesce(patient_spa.basdai6,'') as basdai6 ,
			coalesce(patient_spa.basfi,'') as basfi ,coalesce(patient_euroqol.euroqol_score,0) as euroqol_score ,REPLACE (coalesce(patient_euroqol.eq5d,''), ',', '.') as eq5d
			from patient_followup
			left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id 
			left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id and patient_spa.deleted=0
			left join patient_euroqol on patient_euroqol.patient_followup_id=patient_followup.patient_followup_id and patient_euroqol.deleted=0
			where patient_followup.deleted=0
			and patient_common.deleted=0
			and patient_followup.pat_id=patient_record.pat_id
			and patient_followup.patient_cohort_id= patient_record.patient_cohort_id
			and patient_followup.fumonthcohort=60 limit 1
			LOOP
				qu = qu || quote_literal(drugs_id1.swollenjc28) || ',' || quote_literal(drugs_id1.tenderjc28) || ',' || quote_literal(drugs_id1.basdai) || ',' || quote_literal(drugs_id1.basdai1) || ',' || quote_literal(drugs_id1.basdai2) || ',' || quote_literal(drugs_id1.basdai3) || ',' || quote_literal(drugs_id1.basdai4) || ',';
				qu = qu || quote_literal(drugs_id1.basdai5) || ',' || quote_literal(drugs_id1.basdai6) || ',' || quote_literal(drugs_id1.basfi) || ',' || quote_literal(drugs_id1.crp) || ',' || quote_literal(drugs_id1.das28esr) || ',' || quote_literal(drugs_id1.das28crp) || ',' || quote_literal(drugs_id1.esr) || ',' || quote_literal(drugs_id1.vasphysicial) || ',';
				qu = qu || quote_literal(drugs_id1.euroqol_score) || ',' || quote_literal(drugs_id1.eq5d) || ',' || quote_literal(drugs_id1.mhaq) || ',' || quote_literal(drugs_id1.vasglobal) || ',' || quote_literal(drugs_id1.vaspain) || ',' || quote_literal(drugs_id1.weight) || ',' || quote_literal(drugs_id1.height);
			end loop;
		else
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',';
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt);		
		end if;
	qu = qu || ')';
	execute qu;
	end loop;
RETURN  1;
END; 