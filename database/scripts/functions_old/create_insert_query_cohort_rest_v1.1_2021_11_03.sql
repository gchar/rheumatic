-- FUNCTION: public.create_insert_query_cohort_rest()

-- DROP FUNCTION public.create_insert_query_cohort_rest();

CREATE OR REPLACE FUNCTION public.create_insert_query_cohort_rest(
	)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100000
    VOLATILE 
AS $BODY$DECLARE
  
  select_tbl_cnt int;
  insert_tbl_cnt int;
  cnt int = 0;
  rows_cnt int = 1;
  patient_record RECORD;
  ra_record RECORD;
  spa_record RECORD;
  pulmonary_record RECORD;
  sda_asas_record RECORD;
  sda_asas_cnt RECORD;
  axspa_cnt RECORD;
  axspa RECORD;
  dflt character='';
  ra_cnt RECORD;
  ra1 RECORD;
  ra2 RECORD;
  ra3 RECORD;
  spa_cnt RECORD;
  spa1 RECORD;
  spa2 RECORD;
  spa3 RECORD;
  ethnicity text;
  yearsofeduc text;
  residence text;
  employment text;
  marital_status text;
  physical_status text;
  mediterranean_diet text;
  alcohol_consumption text;
  asas text;
  psaid text;
  
  create_qu text;
  qu text; 
  create_first_qu text;
  
  

 BEGIN 

 execute 'DROP TABLE IF EXISTS statistics_lk_cohort_rest;';
 
 
 -- CREATE TABLE statistics_lk_cohort_rest
 
create_qu = 'CREATE TABLE statistics_lk_cohort_rest (id integer,cohort_id integer';
FOR select_tbl_cnt IN 249..285 LOOP-- 1..37 LOOP 
	create_qu = create_qu || ',a' || select_tbl_cnt || ' varchar';
end loop;

FOR select_tbl_cnt IN 0..26 LOOP 

		IF select_tbl_cnt<=12 THEN
			cnt=select_tbl_cnt*3;
		ELSE
			cnt=(select_tbl_cnt-6)*6;
		END IF;
	create_qu = create_qu || ',SDAI_' || cnt || ' varchar';
	create_qu = create_qu || ',CDAI_' || cnt || ' varchar';
	create_qu = create_qu || ',CRP_' || cnt || ' varchar';
	create_qu = create_qu || ',ESR_' || cnt || ' varchar';
	create_qu = create_qu || ',ASASHI_' || cnt || ' varchar';
	create_qu = create_qu || ',PSAID_' || cnt || ' varchar';
end loop;

create_qu = create_qu || ');' ;

execute create_qu;

 -- Insert the first record with labels
  
create_first_qu = 'INSERT INTO statistics_lk_cohort_rest VALUES ( 1,1,';
-- Demographics 8 fields
create_first_qu = create_first_qu || quote_literal('Ethnicity') || ',' || quote_literal('Year_of_education') || ',' || quote_literal('Residence') || ',' || quote_literal('Employment') || ',' ;
create_first_qu = create_first_qu || quote_literal('Marital_status') || ',' || quote_literal('Physical_activity') || ',' || quote_literal('Medit_diet_score') || ',' || quote_literal('Alcohol_cons') || ',' ;
-- Extraarticular RA 6 fields
create_first_qu = create_first_qu || quote_literal('ra_val_1') || ',' || quote_literal('ra_date_1') || ',' || quote_literal('ra_val_2') || ',' || quote_literal('ra_date_2') || ',' ;
create_first_qu = create_first_qu || quote_literal('ra_val_3') || ',' || quote_literal('ra_date_3') || ',' ;
-- Extraarticular SpA 6 fields
create_first_qu = create_first_qu || quote_literal('spa_val_1') || ',' || quote_literal('spa_date_1') || ',' || quote_literal('spa_val_2') || ',' || quote_literal('spa_date_2') || ',' ;
create_first_qu = create_first_qu || quote_literal('spa_val_3') || ',' || quote_literal('spa_date_3') || ',' ;
-- Pulmonary involvement 9 fields
create_first_qu = create_first_qu || quote_literal('pul_inv_group_1') || ',' || quote_literal('pul_inv_val_1') || ',' || quote_literal('pul_inv_date_1') || ',' ;
create_first_qu = create_first_qu || quote_literal('pul_inv_group_2') || ',' || quote_literal('pul_inv_val_2') || ',' || quote_literal('pul_inv_date_2') || ',' ;
create_first_qu = create_first_qu || quote_literal('pul_inv_group_3') || ',' || quote_literal('pul_inv_val_3') || ',' || quote_literal('pul_inv_date_3') || ',' ;
-- AxSpA individual classification criteria 8 field
create_first_qu = create_first_qu || quote_literal('axspa_1') || ',' || quote_literal('axspa_4') || ',' || quote_literal('axspa_5') || ',' || quote_literal('axspa_6') || ',' ;
create_first_qu = create_first_qu || quote_literal('axspa_9') || ',' || quote_literal('axspa_11') || ',' || quote_literal('axspa_15') || ',' || quote_literal('axspa_16') || ',' ;
-- SDAI & CDAI& ASDAS-CRP & ASDAS - ESR for every FU
-- ASAS-HI & PSAID for every FU

FOR insert_tbl_cnt IN 0..26 LOOP 
		IF insert_tbl_cnt<=12 THEN
			cnt=insert_tbl_cnt*3;
		ELSE
			cnt=(insert_tbl_cnt-6)*6;
		END IF;
		
		create_first_qu = create_first_qu || quote_literal('sdai' || CONCAT('_', cnt)) || ',';
		create_first_qu = create_first_qu || quote_literal('cdai' || CONCAT('_', cnt)) || ',';
		create_first_qu = create_first_qu || quote_literal('asdas_crp' || CONCAT('_', cnt)) || ',';
		create_first_qu = create_first_qu || quote_literal('asdas_esr' || CONCAT('_', cnt)) || ',';
		create_first_qu = create_first_qu || quote_literal('asas' || CONCAT('_', cnt)) || ',';
		
		if insert_tbl_cnt=26 then
			create_first_qu = create_first_qu || quote_literal('psaid' ||  CONCAT('_', cnt));
		else
			create_first_qu = create_first_qu || quote_literal('psaid' ||  CONCAT('_', cnt)) || ','; 
		end if;
		
end loop;

	create_first_qu = create_first_qu || ');' ;
	execute create_first_qu;

-- Insert the records
 
FOR patient_record IN select distinct coalesce(patient.patient_id,0) as patient_id,coalesce(patient_cohort.pat_id,0) as pat_id,coalesce(patient_cohort.patient_cohort_id,0) as patient_cohort_id,
	-- demographics patient
	coalesce(patient.ethnicity,0) as ethnicity,coalesce(patient.yearsofeduc,0) as yearsofeduc,
	-- demographics patient
	coalesce(patient_cohort.residence,0) as residence,coalesce(patient_cohort.employment_at_cohort,0) as employment,
	coalesce(patient_cohort.physical_status,0) as physical_status,coalesce(patient_cohort.marital_status,0) as marital_status,
	coalesce(patient_cohort.mediterranean_diet,'') as mediterranean_diet,coalesce(patient_cohort.alcohol_consumption,'') as alcohol_consumption
	from patient_cohort 
	left join patient on patient.pat_id=patient_cohort.pat_id 
	where patient.deleted=0 and patient_cohort.deleted=0
	--limit 50
	--and patient_cohort.pat_id=10001
	
	loop
		
		rows_cnt = rows_cnt +1;
		
		qu = 'INSERT INTO statistics_lk_cohort_rest VALUES ( ' || rows_cnt ||',' || quote_literal(patient_record.patient_cohort_id) || ',';
		-- Demographics 8 fields
		
		IF patient_record.ethnicity=0 then
			ethnicity='';
		ELSEIF patient_record.ethnicity=1 then
			ethnicity='Greek';
		ELSEIF patient_record.ethnicity=2 then
			ethnicity='Other';
		ELSEIF patient_record.ethnicity=3 then
			ethnicity='Dont know';
		ELSE
			ethnicity=patient_record.ethnicity;
		END IF;
		
		IF patient_record.yearsofeduc=0 then
			yearsofeduc='';
		ELSEIF patient_record.yearsofeduc=1 then
			yearsofeduc='0';
		ELSEIF patient_record.yearsofeduc=2 then
			yearsofeduc='1-6';
		ELSEIF patient_record.yearsofeduc=3 then
			yearsofeduc='7-9';
		ELSEIF patient_record.yearsofeduc=4 then
			yearsofeduc='10-12';
		ELSEIF patient_record.yearsofeduc=5 then
			yearsofeduc='12';
		ELSEIF patient_record.yearsofeduc=6 then
			yearsofeduc='Unknown';
		ELSE
			yearsofeduc=patient_record.yearsofeduc;
		END IF;
		
		IF patient_record.residence=0 then
			residence='';
		ELSEIF patient_record.residence=1 then
			residence='Urban (>15.000 dwellers)';
		ELSEIF patient_record.residence=2 then
			residence='Suburban (10-15.000 dwellers)';
		ELSEIF patient_record.residence=3 then
			residence='Rular (<10.000 dwellers)';
		ELSEIF patient_record.residence=4 then
			residence='Dont know';
		ELSE
			residence=patient_record.residence;
		END IF;
		
		IF patient_record.employment=0 then
			employment='';
		ELSEIF patient_record.employment=1 then
			employment='Unemployed';
		ELSEIF patient_record.employment=2 then
			employment='Student';
		ELSEIF patient_record.employment=3 then
			employment='Employee';
		ELSEIF patient_record.employment=4 then
			employment='Worker';
		ELSEIF patient_record.employment=5 then
			employment='Self-employed';
		ELSEIF patient_record.employment=6 then
			employment='Retired';
		ELSEIF patient_record.employment=7 then
			employment='Disability pension';
		ELSEIF patient_record.employment=8 then
			employment='Unknown';
		ELSEIF patient_record.employment=9 then
			employment='Housewife';
		ELSE
			employment=patient_record.employment;
		END IF;
		
		IF patient_record.marital_status=0 then
			marital_status='';
		ELSEIF patient_record.marital_status=1 then
			marital_status='Not married';
		ELSEIF patient_record.marital_status=2 then
			marital_status='Married';
		ELSEIF patient_record.marital_status=3 then
			marital_status='Divorced';
		ELSEIF patient_record.marital_status=4 then
			marital_status='Widow';
		ELSEIF patient_record.marital_status=5 then
			marital_status='Unknown';
		ELSE
			marital_status=patient_record.marital_status;
		END IF;
		
		IF patient_record.physical_status=0 then
			physical_status='';
		ELSEIF patient_record.physical_status=1 then
			physical_status='Moving only for necesary chores';
		ELSEIF patient_record.physical_status=2 then
			physical_status='Walking or other outdoor activities 1-2 times per week';
		ELSEIF patient_record.physical_status=3 then
			physical_status='Walking or other outdoor activities several times per week';
		ELSEIF patient_record.physical_status=4 then
			physical_status='Exercing 1-2 times per week to the point of perspiring and heavy breathing';
		ELSEIF patient_record.physical_status=5 then
			physical_status='Exercing several times per week to the point of perspiring and heavy breathing';
		ELSEIF patient_record.physical_status=6 then
			physical_status='Keep-fit heavy exercise ot competitive sport several times per week';
		ELSE
			physical_status=patient_record.physical_status;
		END IF;
		
		qu = qu || quote_literal(ethnicity) || ',' || quote_literal(yearsofeduc) || ',' || quote_literal(residence) || ',' || quote_literal(employment) || ',' ;
		qu = qu || quote_literal(marital_status) || ',' || quote_literal(physical_status) || ',' || quote_literal(patient_record.mediterranean_diet) || ',' || quote_literal(patient_record.alcohol_consumption) || ',' ;
		-- Extraarticular RA 6 fields
		FOR ra_cnt IN select count(patient_ra_extraarticular_id) as cnt from patient_ra_extraarticular 
		where deleted=0 and pat_id=patient_record.pat_id 
		LOOP
		END LOOP;
		
			IF ra_cnt.cnt=0 then
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			END IF;	
			
			if ra_cnt.cnt=1 then
				FOR ra1 IN select coalesce(lookup_tbl_val.value,'') as ra_value,coalesce(TO_CHAR(patient_ra_extraarticular.date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_ra_extraarticular left join lookup_tbl_val on patient_ra_extraarticular.ra_extraarticular_id=lookup_tbl_val.id 
				  where patient_ra_extraarticular.deleted=0 and pat_id=patient_record.pat_id order by patient_ra_extraarticular.patient_ra_extraarticular_id desc limit 1
				  LOOP
					qu = qu || quote_literal(ra1.ra_value) || ',' || quote_literal(ra1.date_str) || ',' ;
				  END LOOP;
				  
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			end if;	
			
			if ra_cnt.cnt=2 then
				FOR ra2 IN select coalesce(lookup_tbl_val.value,'') as ra_value,coalesce(TO_CHAR(patient_ra_extraarticular.date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_ra_extraarticular left join lookup_tbl_val on patient_ra_extraarticular.ra_extraarticular_id=lookup_tbl_val.id 
				  where patient_ra_extraarticular.deleted=0 and pat_id=patient_record.pat_id order by patient_ra_extraarticular.patient_ra_extraarticular_id desc limit 2
				  LOOP
					qu = qu || quote_literal(ra2.ra_value) || ',' || quote_literal(ra2.date_str) || ',' ;
				  end loop;
				  
				  qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			end if;	  
			
			if ra_cnt.cnt>=3 then
				FOR ra3 IN select coalesce(lookup_tbl_val.value,'') as ra_value,coalesce(TO_CHAR(patient_ra_extraarticular.date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_ra_extraarticular left join lookup_tbl_val on patient_ra_extraarticular.ra_extraarticular_id=lookup_tbl_val.id 
				  where patient_ra_extraarticular.deleted=0 and pat_id=patient_record.pat_id order by patient_ra_extraarticular.patient_ra_extraarticular_id desc limit 3
				  LOOP
					qu = qu || quote_literal(ra3.ra_value) || ',' || quote_literal(ra3.date_str) || ',' ;
				  END LOOP;
			end if;
			
		
		-- Extraarticular SpA 6 fields
		FOR spa_cnt IN select count(patient_spa_extraarticular_id) as cnt from patient_spa_extraarticular 
		where deleted=0 and pat_id=patient_record.pat_id 
		LOOP
		END LOOP;
		
			IF spa_cnt.cnt=0 then
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			END IF;	
			
			if spa_cnt.cnt=1 then
				FOR spa1 IN select coalesce(lookup_tbl_val.value,'') as spa_value,coalesce(TO_CHAR(patient_spa_extraarticular.date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_spa_extraarticular left join lookup_tbl_val on patient_spa_extraarticular.spa_extraarticular_id=lookup_tbl_val.id 
				  where patient_spa_extraarticular.deleted=0 and pat_id=patient_record.pat_id order by patient_spa_extraarticular.patient_spa_extraarticular_id desc limit 1
				  LOOP
					qu = qu || quote_literal(spa1.spa_value) || ',' || quote_literal(spa1.date_str) || ',' ;
				  END LOOP;
				  
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			end if;	
			
			if spa_cnt.cnt=2 then
				FOR spa2 IN select coalesce(lookup_tbl_val.value,'') as spa_value,coalesce(TO_CHAR(patient_spa_extraarticular.date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_spa_extraarticular left join lookup_tbl_val on patient_spa_extraarticular.spa_extraarticular_id=lookup_tbl_val.id 
				  where patient_spa_extraarticular.deleted=0 and pat_id=patient_record.pat_id order by patient_spa_extraarticular.patient_spa_extraarticular_id desc limit 2
				  LOOP
					qu = qu || quote_literal(spa2.spa_value) || ',' || quote_literal(spa2.date_str) || ',' ;
				  end loop;
				  
				  qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			end if;	  
			
			if spa_cnt.cnt>=3 then
				FOR spa3 IN select coalesce(lookup_tbl_val.value,'') as spa_value,coalesce(TO_CHAR(patient_spa_extraarticular.date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_spa_extraarticular left join lookup_tbl_val on patient_spa_extraarticular.spa_extraarticular_id=lookup_tbl_val.id 
				  where patient_spa_extraarticular.deleted=0 and pat_id=patient_record.pat_id order by patient_spa_extraarticular.patient_spa_extraarticular_id desc limit 3
				  LOOP
					qu = qu || quote_literal(spa3.spa_value) || ',' || quote_literal(spa3.date_str) || ',' ;
				  END LOOP;
			end if;
		
		-- Pulmonary involvement 9 fields
		FOR spa_cnt IN select count(patient_pulmonary_id) as cnt from patient_pulmonary 
		where deleted=0 and pat_id=patient_record.pat_id 
		LOOP
		END LOOP;
		
			IF spa_cnt.cnt=0 then
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
				qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			END IF;	
			
			if spa_cnt.cnt=1 then
				FOR spa1 IN select coalesce(group_val.value,'') as group_value,coalesce(content_val.value,'') as content_value,coalesce(TO_CHAR(patient_pulmonary.pulmonary_date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_pulmonary 
				  left join lookup_tbl_val as group_val on patient_pulmonary.group_id=group_val.id
				  left join lookup_tbl_val as content_val on patient_pulmonary.content=content_val.id 
				  where patient_pulmonary.deleted=0 and pat_id=patient_record.pat_id order by patient_pulmonary.patient_pulmonary_id desc limit 1
				  LOOP
					qu = qu || quote_literal(spa1.group_value) || ',' || quote_literal(spa1.content_value) || ','  || quote_literal(spa1.date_str) || ',' ;
				  END LOOP;
				  
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
					qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			end if;	
			
			if spa_cnt.cnt=2 then
				FOR spa2 IN  select coalesce(group_val.value,'') as group_value,coalesce(content_val.value,'') as content_value,coalesce(TO_CHAR(patient_pulmonary.pulmonary_date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_pulmonary 
				  left join lookup_tbl_val as group_val on patient_pulmonary.group_id=group_val.id
				  left join lookup_tbl_val as content_val on patient_pulmonary.content=content_val.id 
				  where patient_pulmonary.deleted=0 and pat_id=patient_record.pat_id order by patient_pulmonary.patient_pulmonary_id desc limit 2
				  LOOP
					qu = qu || quote_literal(spa2.group_value) || ',' || quote_literal(spa2.content_value) || ','  || quote_literal(spa2.date_str) || ',' ;
				  end loop;
				  
				  qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			end if;	  
			
			if spa_cnt.cnt>=3 then
				FOR spa3 IN select coalesce(group_val.value,'') as group_value,coalesce(content_val.value,'') as content_value,coalesce(TO_CHAR(patient_pulmonary.pulmonary_date, 'DD-MM-YYYY'),'') AS date_str
				  from patient_pulmonary 
				  left join lookup_tbl_val as group_val on patient_pulmonary.group_id=group_val.id
				  left join lookup_tbl_val as content_val on patient_pulmonary.content=content_val.id 
				  where patient_pulmonary.deleted=0 and pat_id=patient_record.pat_id order by patient_pulmonary.patient_pulmonary_id desc limit 3
				  LOOP
					qu = qu || quote_literal(spa3.group_value) || ',' || quote_literal(spa3.content_value) || ','  || quote_literal(spa3.date_str) || ',' ;
				  END LOOP;
			end if;
			
		
		-- AxSpA individual classification criteria 8 field
		-- τσεκαρισμα αν έχει axSpA εγγραφή
		  FOR axspa_cnt IN select count(patient_axialcriteria_id) as cnt from patient_axialcriteria where deleted=0 and pat_id=patient_record.pat_id
			LOOP
			END LOOP;
		
		-- Αν έχει axSpA εγγραφή
		IF axspa_cnt.cnt>0 then
		
			FOR axspa IN select coalesce(criteria1,0) as criteria1,coalesce(criteria4,0) as criteria4,coalesce(criteria5,0) as criteria5,coalesce(criteria6,0) as criteria6
			,coalesce(criteria9,0) as criteria9,coalesce(criteria11,0) as criteria11,coalesce(criteria15,0) as criteria15,coalesce(criteria16,0) as criteria16
			  from patient_axialcriteria where deleted=0 and pat_id=patient_record.pat_id limit 1
			  LOOP
				qu = qu || quote_literal(axspa.criteria1) || ',' || quote_literal(axspa.criteria4) || ',' ;
				qu = qu || quote_literal(axspa.criteria5) || ',' || quote_literal(axspa.criteria6) || ',' ;
				qu = qu || quote_literal(axspa.criteria9) || ',' || quote_literal(axspa.criteria11) || ',' ;
				qu = qu || quote_literal(axspa.criteria15) || ',' || quote_literal(axspa.criteria16) || ',' ;
				end loop;
		-- Αν δεν έχει axSpA εγγραφή
		ELSE
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		END IF;
		


		FOR insert_tbl_cnt IN 0..26 
		LOOP 
			IF insert_tbl_cnt<=12 THEN
				cnt=insert_tbl_cnt*3;
			ELSE
				cnt=(insert_tbl_cnt-6)*6;
			END IF;
			
				-- SDAI & CDAI& ASDAS-CRP & ASDAS - ESR for every FU
				-- τσεκαρισμα αν έχει εγγραφή
				  FOR sda_asas_cnt IN SELECT count(patient_cohort.patient_cohort_id) as cnt
					FROM patient_cohort 
					left join patient_followup on patient_followup.patient_cohort_id=patient_cohort.patient_cohort_id
					left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id
					left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id
					WHERE patient_followup.deleted=0 and patient_followup.fumonthcohort=cnt and patient_followup.patient_cohort_id=patient_record.patient_cohort_id limit 1
					LOOP
					END LOOP;
				-- Αν έχει SDAI & CDAI& ASDAS-CRP & ASDAS - ESR εγγραφή
					IF sda_asas_cnt.cnt>0 then	
					
						FOR sda_asas_record IN SELECT 
						-- SDAI & CDAI& ASDAS-CRP & ASDAS - ESR for every FU
						coalesce(patient_common.sdai,'') as sdai,coalesce(patient_common.cdai,'') as cdai,coalesce(patient_spa.asdascrp,'') as asdas_crp,coalesce(patient_spa.asdasesr,'') as asdas_esr,
						-- patient followup ASAS-HI & PSAID
						coalesce(patient_followup.asas,-1) as asas,coalesce(patient_followup.psaid,'-1') as psaid
						FROM patient_cohort 
						left join patient_followup on patient_followup.patient_cohort_id=patient_cohort.patient_cohort_id
						left join patient_common on patient_common.patient_followup_id=patient_followup.patient_followup_id
						left join patient_spa on patient_spa.patient_followup_id=patient_followup.patient_followup_id
						WHERE patient_followup.deleted=0 and patient_followup.fumonthcohort=cnt and patient_followup.patient_cohort_id=patient_record.patient_cohort_id limit 1
						loop
						end loop;
						
						
						qu = qu || quote_literal(sda_asas_record.sdai) || ',';
						qu = qu || quote_literal(sda_asas_record.cdai) || ',';
						qu = qu || quote_literal(sda_asas_record.asdas_crp) || ',';
						qu = qu || quote_literal(sda_asas_record.asdas_esr) || ',';
						
						IF sda_asas_record.asas=-1 then
							asas='UNKNOWN';
						ELSE
							asas=sda_asas_record.asas;
						END IF;
						qu = qu || quote_literal(asas) || ',';
						
						IF sda_asas_record.psaid='-1' then
							psaid='UNKNOWN';
						ELSE
							psaid=sda_asas_record.psaid;
						END IF;
						
						if cnt=120 then
							qu = qu || quote_literal(psaid);
						else
							qu = qu || quote_literal(psaid) || ',';
						end if;
					
				-- Αν δεν έχει εγγραφή
					ELSE
						qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
						qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
						qu = qu || quote_literal(dflt) || ',' ;
						
						if cnt=120 then
							qu = qu || quote_literal(dflt);
						else
							qu = qu || quote_literal(dflt) || ',';
						end if;
					END IF;
			
				
				
				
			
			
		end loop;
  	
	qu = qu || ');' ;
	execute qu;
end loop;

RETURN  1;

END; $BODY$;

ALTER FUNCTION public.create_insert_query_cohort_rest()
    OWNER TO postgres;
