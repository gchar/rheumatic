DECLARE
  krow int;
  patient_record RECORD;
  drugs_id3 RECORD;
  drugs_id4 record;
  drugs_id5 int;
	i int=1;
  krow2 int = 1;
  krow3 int = 0;
  qu text; 
  create_qu text;
  create_first_qu text;
  mviews RECORD;  
  mviews2 RECORD;
  mviews3 RECORD;
  mviews4 RECORD;
  mviews5 RECORD;
  count_rows RECORD;
  dflt character='';
 BEGIN 
 
 execute 'DROP TABLE IF EXISTS statistics_lk1;';
 
 FOR count_rows IN SELECT count(drugs_id) as count_rows FROM drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration
  WHERE drugs.deleted=0 and drugs.ongoing=0 and drugs.prim=0
    LOOP
	END LOOP;
 
create_qu = 'CREATE TABLE statistics_lk1 (id integer,';	
create_qu = create_qu || 'a1 VARCHAR,a2 VARCHAR,a3 VARCHAR,a4 VARCHAR,a5 VARCHAR,a6 VARCHAR,a7 VARCHAR,a8 date,a9 date,a10 VARCHAR,';
create_qu = create_qu || 'a11 VARCHAR,a12 VARCHAR,a13 VARCHAR,a14 VARCHAR,a15 VARCHAR,a16 VARCHAR,a17 VARCHAR,a18 VARCHAR,';
create_qu = create_qu || 'a19 VARCHAR,a20 VARCHAR,a21 VARCHAR,a22 VARCHAR,a23 VARCHAR,';
create_qu = create_qu || 'a24 VARCHAR,a25 VARCHAR,a26 VARCHAR,a27 VARCHAR,a28 VARCHAR,';
create_qu = create_qu || 'a29 VARCHAR,a30 VARCHAR,';
create_qu = create_qu || 'a31 VARCHAR,a32 VARCHAR,a33 VARCHAR,a34 VARCHAR,a35 VARCHAR,a36 VARCHAR,a37 VARCHAR,a38 VARCHAR,';
create_qu = create_qu || 'a39 VARCHAR,a40 VARCHAR,a41 VARCHAR,a42 VARCHAR,a43 VARCHAR,a44 VARCHAR,a45 VARCHAR,';
create_qu = create_qu || 'a46 VARCHAR,a47 VARCHAR,a48 VARCHAR,a49 VARCHAR,a50 VARCHAR,a51 VARCHAR,';
create_qu = create_qu || 'a52 VARCHAR,a53 VARCHAR,a54 VARCHAR,a55 VARCHAR,a56 VARCHAR,a57 VARCHAR,a58 VARCHAR,a59 VARCHAR,';
create_qu = create_qu || 'a60 VARCHAR,a61 VARCHAR,a62 VARCHAR,a63 VARCHAR,a64 VARCHAR,a65 VARCHAR,';
create_qu = create_qu || 'a66 VARCHAR,';
create_qu = create_qu || 'a67 VARCHAR,a68 VARCHAR,a69 VARCHAR,a70 VARCHAR,a71 VARCHAR,a72 VARCHAR,a73 VARCHAR,a74 VARCHAR,a75 VARCHAR,';
create_qu = create_qu || 'a76 VARCHAR,a77 VARCHAR,a78 VARCHAR,a79 VARCHAR,a80 VARCHAR,';
create_qu = create_qu || 'a81 VARCHAR,a82 VARCHAR,a83 VARCHAR,a84 VARCHAR,a85 VARCHAR,a86 VARCHAR,a87 VARCHAR,';
create_qu = create_qu || 'a88 VARCHAR,a89 VARCHAR,a90 VARCHAR,a91 VARCHAR,a92 VARCHAR,a93 VARCHAR,';
create_qu = create_qu || 'a94 VARCHAR,a95 VARCHAR,a96 VARCHAR,a97 VARCHAR,a98 VARCHAR,a99 VARCHAR,';

FOR drugs_id5 IN 100..count_rows.count_rows+107 LOOP 
	create_qu = create_qu || 'a' || drugs_id5 ;
	if drugs_id5=count_rows.count_rows+107 then
		create_qu = create_qu || ' VARCHAR' ;
	else
		create_qu = create_qu || ' VARCHAR,' ;
	end if;
END LOOP;

create_qu = create_qu || ');' ;

execute create_qu;

create_first_qu = 'INSERT INTO statistics_lk1 VALUES ( 1,' || quote_literal('cohort_id') || ',' || quote_literal('pat_id') || ',' || quote_literal('drug_id') || ',' || quote_literal('substance') || ',' ;
create_first_qu = create_first_qu || quote_literal('centre_id') || ',' || quote_literal('centre_name') || ',' || quote_literal('treatment_nbr') || ',' || quote_literal('01-01-2019') || ',' ;
create_first_qu = create_first_qu || quote_literal('01-01-2019') || ',' || quote_literal('stop_reason') || ',' || quote_literal('stop_cause_comments') || ',';
create_first_qu = create_first_qu || quote_literal('gender') || ',' || quote_literal('date_of_inclusion') || ',' || quote_literal('date_of_birth') ;
create_first_qu = create_first_qu || ',' || quote_literal('main_diagnosis_date') || ',' || quote_literal('icd10a') || ',' || quote_literal('main_diagnosis_value') ;
create_first_qu = create_first_qu || ',' || quote_literal('main_symptom_date') || ',' || quote_literal('first_secondary_diagnosis_date') || ',';
create_first_qu = create_first_qu || quote_literal('first_secondary_icd10b') || ',' || quote_literal('first_secondary_diagnosis_value') || ',' ;
create_first_qu = create_first_qu || quote_literal('first_secondary_symptom_date') || ',' || quote_literal('second_secondary_diagnosis_date') || ',' || quote_literal('second_secondary_icd10b') || ',';
create_first_qu = create_first_qu || quote_literal('second_secondary_diagnosis_value') || ',' || quote_literal('second_secondary_symptom_date') || ',' ;
create_first_qu = create_first_qu || quote_literal('third_secondary_diagnosis_date') || ',' || quote_literal('third_secondary_icd10b') || ',' || quote_literal('third_secondary_diagnosis_value') || ',' ;
create_first_qu = create_first_qu || quote_literal('third_secondary_symptom_date') || ',' || quote_literal('Aortic_Aneurysm') || ',' ;
create_first_qu = create_first_qu || quote_literal('Cardiac_arrhythmia') || ',' || quote_literal('Congestive_Heart_Failure') || ',' || quote_literal('Coronary_Heart_Disease') ;
create_first_qu = create_first_qu || ',' || quote_literal('Acute_myocardial_infarction') || ',' || quote_literal('Angina_pectoris') || ',' || quote_literal('Atrial_fibrillation') ;
create_first_qu = create_first_qu || ',' || quote_literal('Pericardial_disease_Acute_Pericarditis') || ',' || quote_literal('Hypertension') || ',';
create_first_qu = create_first_qu || quote_literal('Cardiomyopathies') || ',' || quote_literal('Peripheral_vascular_disease') || ',' || quote_literal('Valvular_Heart_Disease') ;
create_first_qu = create_first_qu || ',' || quote_literal('Ischemic_strokeTIA') || ',' || quote_literal('Sequelae_of_ischemic_stroke') || ',' || quote_literal('Hemorrhagic_stroke') ;
create_first_qu = create_first_qu || ',' || quote_literal('Sequelae_of_hemorrhagic_stroke') || ',';
create_first_qu = create_first_qu || quote_literal('Anxiety_disorders') || ',' || quote_literal('Bipolar_disorder') || ',' || quote_literal('Dementia_unspecified') || ',' ;
create_first_qu = create_first_qu || quote_literal('Major_depression_without_psychotic_symptoms') || ',' || quote_literal('Other_depressive_episodes') || ',' || quote_literal('Multiple_Sclerosis_Demyelinating_disease') || ',';
create_first_qu = create_first_qu || quote_literal('Parkinsons_disease') || ',' || quote_literal('Peripheral_Neuropathy') || ',' || quote_literal('Psychosis') || ',' ;
create_first_qu = create_first_qu || quote_literal('Schizophrenia') || ',' || quote_literal('Asthma') || ',' || quote_literal('Bronchiectasis') || ',' || quote_literal('Chronic_obstructive_pulmonary_disease') || ',' || quote_literal('Chronic_sinusitis') || ',';
create_first_qu = create_first_qu || quote_literal('Interstitial_Lung_Disease') || ',' || quote_literal('Pulmonary_Hypertension') || ',' || quote_literal('Autoimmune_hepatitis') ;
create_first_qu = create_first_qu || ',' || quote_literal('Alcoholic_liver_disease') || ',' || quote_literal('Alcoholic_liver_cirrhosis') || ',' || quote_literal('Nonalcoholic_fatty_liver_disease_NAFLD') || ',';
create_first_qu = create_first_qu || quote_literal('Crohn’s_disease') || ',';
create_first_qu = create_first_qu || quote_literal('Diverticulosis') || ',' || quote_literal('EsophagitisGERD') || ',' || quote_literal('Peptic_ulcer_disease_biopsy_proven') || ',' ;
create_first_qu = create_first_qu || quote_literal('Diabetes_mellitus') || ',' || quote_literal('Dyslipidaemia') || ',' || quote_literal('Hypothyroidism') || ',' || quote_literal('Hyperthyroidism') ;
create_first_qu = create_first_qu || ',' || quote_literal('Obesity') || ',' || quote_literal('Chronic_kidney_disease_stage_III_GFR60') || ',';
create_first_qu = create_first_qu || quote_literal('Chronic_kidney_disease_stage_IIIIV_(GFR_15-59)') || ',' || quote_literal('End-stage_renal_disease_GFR_15') || ',' || quote_literal('Hodgkin_Lymphoma') ;
create_first_qu = create_first_qu || ',' || quote_literal('Non-Hodgkin_Lymphoma_unspecified') || ',' || quote_literal('Leukemia') || ',';
create_first_qu = create_first_qu || quote_literal('Solid_tumor') || ',' || quote_literal('Solid_tumor_without_metastasi') || ',' || quote_literal('Metastatic_solid_tumor') || ',' ;
create_first_qu = create_first_qu || quote_literal('Metastatic_cancer_without_known_primary_tumor_site') || ',' || quote_literal('Ischemic_Optic_Neuropathy') || ',' || quote_literal('Optic_Neuritis') ;
create_first_qu = create_first_qu || ',' || quote_literal('Chronic_osteomyelitis') || ',' ;
create_first_qu = create_first_qu || quote_literal('Latent_TB_infection') || ',' || quote_literal('Viral_hepatitis') || ',' || quote_literal('Chronic_viral_infection_other') || ',' ;
create_first_qu = create_first_qu || quote_literal('Past_hospitalization_for_serious_infection') || ',' || quote_literal('Recurrent_past_hospitalizations_for_serious_infections') || ',' || quote_literal('History_of_opportunistic_infection') || ',';
create_first_qu = create_first_qu || quote_literal('Prosthetic_left_hip_joint') || ',' || quote_literal('Prosthetic_right_hip_joint') || ',' || quote_literal('Prosthetic_left_knee_joint') ;
create_first_qu = create_first_qu || ',' || quote_literal('Prosthetic_right_knee_joint') || ',' || quote_literal('Prosthetic_joint_other') || ',';
krow3 = 0;

 FOR drugs_id4 IN SELECT REPLACE (coalesce(drugs.substance,''), ',', ' ') as substance,REPLACE (coalesce(lookup_tbl_val.value,''), ',', ' ') as route_of_administration_val FROM drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration
  WHERE drugs.deleted=0 and drugs.ongoing=0 and drugs.prim=0  order by drugs_id asc
    LOOP
	krow3 = krow3 + 1;
	create_first_qu = create_first_qu || quote_literal(drugs_id4.substance || '-' || drugs_id4.route_of_administration_val) ;
	create_first_qu = create_first_qu || ',';
	end loop;
	create_first_qu = create_first_qu || quote_literal('Anti-CCP') || ',' || quote_literal('Rheumatoid Factor') || ',' || quote_literal('erosions') || ',' || quote_literal('RA') || ',' || quote_literal('SpA') || ',' ;
	create_first_qu = create_first_qu || quote_literal('CASPAR') || ',' || quote_literal('SLE') || ',' || quote_literal('APS');
 	create_first_qu = create_first_qu || ');' ;
	execute create_first_qu;
	
  FOR patient_record IN select coalesce(patient_cohort.erosions,0) as erosions,coalesce(patient_cohort.patient_cohort_id,0) as patient_cohort_id
  	,coalesce(patient_cohort.pat_id,0) as pat_id,coalesce(patient_cohort.cohort_name,0) as cohort_name,REPLACE (coalesce(drugs.substance,''), ',', ' ')  as substance
	,coalesce(patient.centre,0) as centre,REPLACE (coalesce(lookup_tbl_val.value,''), ',', ' ')  as centre_name,
	coalesce(patient_cohort.treatment_nbr,'') as treatment_nbr,coalesce(TO_CHAR(patient_cohort.start_date, 'DD-MM-YYYY'),'12-12-1900') AS start_date_str,
	coalesce(TO_CHAR(patient_cohort.stop_date, 'DD-MM-YYYY'), '12-12-1900') AS stop_date_str
	,coalesce(patient_cohort.stop_reason,0) as stop_reason,REPLACE (coalesce(patient_cohort.stop_ae_reason,''), ',', ' ') as stop_ae_reason,coalesce(patient.gender,0) as gender,
	coalesce(TO_CHAR(patient.dateofinclusion, 'DD-MM-YYYY'),'') AS dateofinclusion_str,
	coalesce(TO_CHAR(patient.dateofbirth, 'DD-MM-YYYY'),'') AS dateofbirth_str from patient_cohort left join patient 
	on patient.pat_id=patient_cohort.pat_id left join drugs on patient_cohort.cohort_name=drugs.drugs_id 
	left join lookup_tbl_val on lookup_tbl_val.id=patient.centre where patient.deleted=0 and patient_cohort.deleted=0
    LOOP
	
	krow2=krow2 + 1;
	  qu = 'INSERT INTO statistics_lk1 VALUES ( ' || krow2 || ',' ;
	  qu = qu || quote_literal(patient_record.patient_cohort_id) || ',' || quote_literal(patient_record.pat_id) || ',' ;
	  qu = qu || quote_literal(patient_record.cohort_name) || ',' || quote_literal(patient_record.substance) || ',' ;
	  qu = qu || quote_literal(patient_record.centre) || ',' || quote_literal(patient_record.centre_name) || ',' ;
	  qu = qu || quote_literal(patient_record.treatment_nbr) || ',' || quote_literal(patient_record.start_date_str) || ',' ;
	  qu = qu || quote_literal(patient_record.stop_date_str) || ',' || quote_literal(patient_record.stop_reason) || ',' ;
	  qu = qu || quote_literal(patient_record.stop_ae_reason) || ',' || quote_literal(patient_record.gender) || ',' ;
	  qu = qu || quote_literal(patient_record.dateofinclusion_str) || ',' || quote_literal(patient_record.dateofbirth_str) || ',' ;
	  FOR drugs_id4 IN select count(diagnosis_id) as cnt from patient_diagnosis 
		where deleted=0 and pat_id=patient_record.pat_id and patient_diagnosis.main=1
		LOOP
		END LOOP;
		
		IF drugs_id4.cnt>0 then
			FOR drugs_id3 IN select coalesce(patient_diagnosis.diagnosis_id,0) as diagnosis_id
		  ,coalesce(TO_CHAR(patient_diagnosis.symptom_date, 'DD-MM-YYYY'),'') 
		  AS symptom_date_str,coalesce(TO_CHAR(patient_diagnosis.disease_date, 'DD-MM-YYYY'),'') AS disease_date_str,
		  REPLACE (coalesce(diagnosis.icd10,''), ',', ' ') as icd10,REPLACE (coalesce(diagnosis.value,''), ',', ' ') as value from patient_diagnosis left join diagnosis on 
		  diagnosis.diagnosis_id=patient_diagnosis.diagnosis_id where patient_diagnosis.deleted=0 and 
		  patient_diagnosis.pat_id=patient_record.pat_id and patient_diagnosis.main=1
		  LOOP
			qu = qu || quote_literal(drugs_id3.disease_date_str) || ',' || quote_literal(drugs_id3.icd10) || ',' ;
			qu = qu || quote_literal(drugs_id3.value) || ',' || quote_literal(drugs_id3.symptom_date_str) || ',' ;
			end loop;
		ELSE
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		END IF;
		
	 FOR drugs_id4 IN select count(diagnosis_id) as cnt from patient_diagnosis 
		where deleted=0 and pat_id=patient_record.pat_id and patient_diagnosis.main=0
		LOOP
		END LOOP;
		
	IF drugs_id4.cnt=0 then
		qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
	END IF;	
	
	if drugs_id4.cnt=1 then
		FOR drugs_id3 IN select coalesce(patient_diagnosis.diagnosis_id,0) as diagnosis_id
		  ,coalesce(TO_CHAR(patient_diagnosis.symptom_date, 'DD-MM-YYYY'),'') 
		  AS symptom_date_str,coalesce(TO_CHAR(patient_diagnosis.disease_date, 'DD-MM-YYYY'),'') AS disease_date_str,
		  REPLACE (coalesce(diagnosis.icd10,''), ',', ' ') as icd10,REPLACE (coalesce(diagnosis.value,''), ',', ' ') as value from patient_diagnosis left join diagnosis on 
		  diagnosis.diagnosis_id=patient_diagnosis.diagnosis_id where patient_diagnosis.deleted=0 and 
		  patient_diagnosis.pat_id=patient_record.pat_id and patient_diagnosis.main=0 limit 1
		  LOOP
			qu = qu || quote_literal(drugs_id3.disease_date_str) || ',' || quote_literal(drugs_id3.icd10) || ',' ;
			qu = qu || quote_literal(drugs_id3.value) || ',' || quote_literal(drugs_id3.symptom_date_str) || ',' ; 
		  END LOOP;
		  
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
			qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
	end if;	
	
	if drugs_id4.cnt=2 then
		FOR drugs_id3 IN select coalesce(patient_diagnosis.diagnosis_id,0) as diagnosis_id
		  ,coalesce(TO_CHAR(patient_diagnosis.symptom_date, 'DD-MM-YYYY'),'') 
		  AS symptom_date_str,coalesce(TO_CHAR(patient_diagnosis.disease_date, 'DD-MM-YYYY'),'') AS disease_date_str,
		  REPLACE (coalesce(diagnosis.icd10,''), ',', ' ') as icd10,REPLACE (coalesce(diagnosis.value,''), ',', ' ') as value from patient_diagnosis left join diagnosis on 
		  diagnosis.diagnosis_id=patient_diagnosis.diagnosis_id where patient_diagnosis.deleted=0 and 
		  patient_diagnosis.pat_id=patient_record.pat_id and patient_diagnosis.main=0 limit 2
		  LOOP
			qu = qu || quote_literal(drugs_id3.disease_date_str) || ',' || quote_literal(drugs_id3.icd10) || ',' ;
			qu = qu || quote_literal(drugs_id3.value) || ',' || quote_literal(drugs_id3.symptom_date_str) || ',' ; 
		  end loop;
		  qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
		qu = qu || quote_literal(dflt) || ',' || quote_literal(dflt) || ',' ;
	end if;	  
	
	if drugs_id4.cnt>=3 then
		FOR drugs_id3 IN select coalesce(patient_diagnosis.diagnosis_id,0) as diagnosis_id
		  ,coalesce(TO_CHAR(patient_diagnosis.symptom_date, 'DD-MM-YYYY'),'') 
		  AS symptom_date_str,coalesce(TO_CHAR(patient_diagnosis.disease_date, 'DD-MM-YYYY'),'') AS disease_date_str,
		  REPLACE (coalesce(diagnosis.icd10,''), ',', ' ') as icd10,REPLACE (coalesce(diagnosis.value,''), ',', ' ') as value from patient_diagnosis left join diagnosis on 
		  diagnosis.diagnosis_id=patient_diagnosis.diagnosis_id where patient_diagnosis.deleted=0 and 
		  patient_diagnosis.pat_id=patient_record.pat_id and patient_diagnosis.main=0 limit 3
		  LOOP
			qu = qu || quote_literal(drugs_id3.disease_date_str) || ',' || quote_literal(drugs_id3.icd10) || ',' ;
			qu = qu || quote_literal(drugs_id3.value) || ',' || quote_literal(drugs_id3.symptom_date_str) || ',' ; 
		  END LOOP;
	end if;
	
	  FOR drugs_id3 IN select diseases_comorbidities_id from diseases_comorbidities 
	  where diseases_comorbidities_id in (1,2,3,4,5,6,7,8,9,11,12,14,17,18,19,20,23,24,28,34,35,40,41,42,43,44,50
	  ,51,52,53,54,55,56,57,58,59,61,64,65,69,74,75,76,78,80,81,82,83,85,86,87,88,89,90,91,98,100,106,107,108,109
	  ,110,111,112,116,117,118,119,120)
	  order by diseases_comorbidities_id asc
	  LOOP
		  FOR mviews IN select count(*) as cnt from nonrheumatic_diseases where pat_id=patient_record.pat_id 
			  and deleted=0 and diseases_comorbidities_id=drugs_id3.diseases_comorbidities_id
			  LOOP
				IF mviews.cnt > 0 THEN
					qu = qu || 'TRUE,' ;
				ELSE
					qu = qu || 'FALSE,' ;
				END IF;
		  end loop;
	  end loop;
	  
	  FOR drugs_id3 IN SELECT drugs.drugs_id FROM drugs join lookup_tbl_val on lookup_tbl_val.id=drugs.route_of_administration
	  WHERE drugs.deleted=0 and drugs.ongoing=0 and drugs.prim=0  order by drugs_id asc
	  LOOP
		  FOR mviews IN select count(*) as cnt from patient_previous_drugs where pat_id=patient_record.pat_id 
		  and deleted=0 and drugs_id=drugs_id3.drugs_id
		  LOOP
			IF mviews.cnt > 0 THEN
				qu = qu || 'TRUE,' ;
			ELSE
				qu = qu || 'FALSE,' ;
			END IF;
		  end loop;
	  end loop;
	  
	   FOR mviews IN select count(*) as cnt from patient_autoantibodies where pat_id=patient_record.pat_id 
	   and deleted=0 and antibodies_id=66
	  LOOP
		IF mviews.cnt > 0 THEN
				qu = qu || 'TRUE,' ;
			ELSE
				qu = qu || 'FALSE,' ;
			END IF;
	  end loop;
	  
	  FOR mviews IN select count(*) as cnt from patient_autoantibodies where pat_id=patient_record.pat_id 
	   and deleted=0 and antibodies_id=65
	  LOOP
			IF mviews.cnt > 0 THEN
				qu = qu || 'TRUE,' ;
			ELSE
				qu = qu || 'FALSE,' ;
			END IF;
	  end loop;
	  
	  qu = qu || quote_literal(patient_record.erosions) || ',';
  	  FOR mviews4 IN select COUNT(rheumatoid) AS cnt from patient_racriteria where pat_id=patient_record.pat_id and deleted=0
	  LOOP
	  END LOOP;
	  
	  IF mviews4.cnt>0 THEN
	  FOR mviews2 IN select rheumatoid from patient_racriteria where pat_id=patient_record.pat_id and deleted=0
		  LOOP
			IF mviews2.rheumatoid = 'Yes' THEN
					qu = qu || 'TRUE,' ;
				ELSE
					qu = qu || 'FALSE,' ;
				END IF;
		  end loop;
	  ELSE
	  	qu = qu || 'FALSE,' ;
	 END IF;
	 
	  FOR mviews4 IN select COUNT(*) AS cnt1 from patient_peripheralcriteria where pat_id=patient_record.pat_id 
	   and deleted=0
	  LOOP
	  END LOOP;
	  
	  FOR mviews5 IN select COUNT(*) AS cnt1 from patient_axialcriteria 
	  where pat_id=patient_record.pat_id and deleted=0
	  LOOP
	  END LOOP;
	  
	  FOR mviews2 IN select set1,set2 from patient_peripheralcriteria where pat_id=patient_record.pat_id 
	   and deleted=0 order by patient_peripheralcriteria_id asc
	  LOOP
	  END LOOP;
	  
  	  FOR mviews3 IN select newyork,imaging,clinical,radiographic from patient_axialcriteria 
	  where pat_id=patient_record.pat_id and deleted=0 order by patient_axialcriteria_id asc
	  LOOP
	  END LOOP;
	  
		IF mviews4.cnt1>0 or mviews5.cnt1>0 then
		   IF mviews2.set1 = 'Yes' or mviews2.set2 = 'Yes' or mviews3.newyork = 'Yes' or mviews3.imaging = 'Yes' or mviews3.clinical = 'Yes' or mviews3.radiographic = 'Yes' THEN
				qu = qu || 'TRUE,' ;
			ELSE
				qu = qu || 'FALSE,' ;
			END IF;
		ELSE
			qu = qu || 'FALSE,' ;
		END IF;
	FOR mviews4 IN select COUNT(*) AS cnt1 from patient_casparcriteria where pat_id=patient_record.pat_id 
	 and deleted=0
	  LOOP
	  END LOOP;
	  
	  IF mviews4.cnt1>0 then 
		FOR mviews IN select caspar from patient_casparcriteria where pat_id=patient_record.pat_id 
	   and deleted=0 order by patient_casparcriteria_id asc
		  LOOP
			IF mviews.caspar = 'Yes' THEN
				qu = qu || 'TRUE,' ;
			ELSE
				qu = qu || 'FALSE,' ;
			END IF;
		  end loop;
  		else
			qu = qu || 'FALSE,' ;
		end if;
		
		FOR mviews4 IN select COUNT(*) AS cnt1 from patient_criteriasle where pat_id=patient_record.pat_id 
		   and deleted=0
		  LOOP
		END LOOP;
		
		  IF mviews4.cnt1>0 then 
		  FOR mviews IN select sle1997,sle2012,sle2017 from patient_criteriasle where pat_id=patient_record.pat_id 
		   and deleted=0 order by patient_criteriasle_id asc
		  LOOP
				IF  mviews.sle1997 = 'Yes' or mviews.sle2012 = 'Yes' or mviews.sle2017 = 'Yes' THEN
					qu = qu || 'TRUE,' ;
				ELSE
					qu = qu || 'FALSE,' ;
				END IF;
		  END LOOP;
		else
			qu = qu || 'FALSE,' ;
		end if;
		
		FOR mviews4 IN select COUNT(*) AS cnt1 from patient_apscriteria where pat_id=patient_record.pat_id 
		   and deleted=0
	  LOOP
	  END LOOP;
	  
	  IF mviews4.cnt1>0 then 
 		  FOR mviews IN select aps from patient_apscriteria where pat_id=patient_record.pat_id 
		   and deleted=0 order by patient_apscriteria_id asc
		  LOOP
				IF mviews.aps = 'Yes' THEN
					qu = qu || 'TRUE' ;
				ELSE
					qu = qu || 'FALSE' ;
				END IF;
		  end loop;
 	 	else
			qu = qu || 'FALSE' ;
		END IF;
	qu = qu || ')';	
	
	execute qu;
END LOOP;

RETURN  1;

END; 