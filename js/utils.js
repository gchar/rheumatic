﻿

function toggleFormElements(bDisabled)
{
	
	var inputs= $("input");
	for (var i=0;i<inputs.length;i++)
	{
		if ( inputs[i].type == 'image'  && (inputs[i].getAttribute('src')=="../images/save.jpg" || inputs[i].getAttribute('src')=="../images/newrecord4.jpg" || inputs[i].getAttribute('src') == "../images/newrecord3.jpg" || inputs[i].getAttribute('src') == "../images/newrecord2.jpg" || inputs[i].getAttribute('src') == "../images/newrecord.jpg" || inputs[i].getAttribute('src') == "../images/delete.jpg"))
		{
			inputs[i].style.display = 'none';
		}
		else// if (inputs[i].className != 'likelink' && inputs[i].className != 'likelink2' && inputs[i].type != 'hidden')
		{
			inputs[i].disabled=bDisabled;
		}
	}
	
	var selects= $("select");
	for (var i=0;i<selects.length;i++)
	{
		selects[i].disabled=bDisabled;
	}
	
	
	
	var textareas= $("textarea");
	for (var i=0;i<textareas.length;i++)
	{
		textareas[i].disabled=bDisabled;
	}
	
	var buttons= $("button");
	for (var i=0;i<buttons.length;i++)
	{
		if(bDisabled==true)
		{
			buttons[i].style.display = 'none';
		}
		else
		{
			buttons[i].style.display = 'block';
		}
		
	}
	
	var imgs= $("img");
	for (var i=0;i<imgs.length;i++)
	{
		if(bDisabled==true && (imgs[i].getAttribute('src')=="../images/save.jpg" || imgs[i].getAttribute('src')=="../images/newrecord4.jpg" || imgs[i].getAttribute('src') == "../images/newrecord3.jpg" || imgs[i].getAttribute('src') == "../images/newrecord2.jpg" || imgs[i].getAttribute('src') == "../images/newrecord.jpg" || imgs[i].getAttribute('src') == "../images/delete.jpg"))
		{
			//imgs[i].onclick = function(){return false;}
			imgs[i].style.display = 'none';
			//imgs[i].disabled = bDisabled;
			
		}
		else
		{
			//imgs[i].onclick = function(){return true;}
		}
		
	}
	
	var alink= $(document.getElementById("form").getElementsByTagName("a")).not(".nodisabled");
	for (var i=0;i<alink.length;i++)
	{
		
		if(bDisabled==true)
		{
			alink[i].style.display = 'none';
		}
		else
		{
			alink[i].style.display = 'block';
		}
	}
}

function setdeleteid(table,id_name,id)
{
	if(confirm("Η εγγραφή θα διαγραφεί οριστικά!"))
	{
		window.open("../library/delete.php?table="+table+"&id_name="+id_name+"&id="+id,"_blank","toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=1,height=1,left = 0,top = 0");
	}
}

function setdeleteid2(table,id_name,id)
{
	if(confirm("Η εγγραφή θα διαγραφεί οριστικά!"))
	{
		window.open("../library/delete2.php?table="+table+"&id_name="+id_name+"&id="+id,"_blank","toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=1,height=1,left = 0,top = 0");
	}
}

function popup(url)
{
	window.open(url,"_blank","toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=1,width=700,height=700,left = 200,top = 50");
}

function popup2(url)
{
	window.open(url,"_blank","toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=1000,height=850,left = 200,top = 50");
}

function popup3(url)
{
	window.open(url,"_blank","toolbar=0,scrollbars=1,location=0,statusbar=0,menubar=0,resizable=1,width=1300,height=700,left = 50,top = 50");
}

function closeWin()
{
	window.opener.location.reload();
	window.close();
}

function comma6(pedio)
{
	var budget;
	budget = document.getElementById(pedio).value;
	budget = budget.replace (/\,/g, '.');
	if (isNaN(budget))
	{
		budget=budget.substring(0, budget.length-1);
	}
	else
	{
		var len = "";
		var exist = budget.indexOf(".");
		
		if (exist!=-1)
		{
			len = budget.split('.')[1];
		}
		if(len.length>2)
		{
			budget=parseFloat(budget).toFixed(2);
		}
		budget = budget.replace (/\./g, ',');
	}
	document.getElementById(pedio).value=budget;
	
}

function comma7(pedio,id)
{
	var budget;
	budget = document.getElementById(pedio+id).value;
	budget = budget.replace (/\,/g, '.');
	if (isNaN(budget))
	{
		budget=budget.substring(0, budget.length-1);
	}
	else
	{
		var len = "";
		var exist = budget.indexOf(".");
		
		if (exist!=-1)
		{
			len = budget.split('.')[1];
		}
		if(len.length>2)
		{
			budget=parseFloat(budget).toFixed(2);
		}
		budget = budget.replace (/\./g, ',');
	}
	document.getElementById(pedio+id).value=budget;
}

function comma8(pedio,id,id2)
{
	var budget;
	var id3=pedio+id+"!"+id2;
	budget = document.getElementById(id3).value;
	budget = budget.replace (/\,/g, '.');
	if (isNaN(budget))
	{
		budget=budget.substring(0, budget.length-1);
	}
	else
	{
		var len = "";
		var exist = budget.indexOf(".");
		
		if (exist!=-1)
		{
			len = budget.split('.')[1];
		}
		if(len.length>2)
		{
			budget=parseFloat(budget).toFixed(2);
		}
		budget = budget.replace (/\./g, ',');
	}
	document.getElementById(id3).value=budget;
}

function message()
{
	if (confirm('Εχετε αποθηκεύσει τα δεδομένα σας;'))
	
		return window.location.href='support.php'; 
	
	else 
		
		return false;
}
function printContent(id)
{
str=document.getElementById(id).innerHTML
//str=str.replace('<INPUT class="textfield" id="nomikos" name="nomikos" type="text" style="WIDTH:90%;" maxLength="100">',document.getElementById('111').value)
newwin=window.open('','printwin','left=10,top=10,width=900,height=600')
newwin.document.write('<HTML>\n<HEAD>\n')
newwin.document.write('<TITLE>Print Page</TITLE>\n')
newwin.document.write('<script>\n')
newwin.document.write('function chkstate(){\n')
newwin.document.write('if(document.readyState=="complete"){\n')
newwin.document.write('window.close()\n')
newwin.document.write('}\n')
newwin.document.write('else{\n')
newwin.document.write('setTimeout("chkstate()",2000)\n')
newwin.document.write('}\n')
newwin.document.write('}\n')
newwin.document.write('function print_win(){\n')
newwin.document.write('window.print();\n')
newwin.document.write('chkstate();\n')
newwin.document.write('}\n')
newwin.document.write('<\/script>\n')
newwin.document.write('</HEAD>\n')
newwin.document.write('<BODY onload="print_win()">\n')
newwin.document.write(str)
newwin.document.write('</BODY>\n')
newwin.document.write('</HTML>\n')
newwin.document.close()
}

function comma(pedio)
{
	var budget;
	var budget2;
	////positioning///
	var position=getCaret(pedio);
	var length=document.getElementById(pedio).value.length;
	////positioning///
	budget = document.getElementById(pedio).value;
	budget2 = document.getElementById(pedio).value;
	budget = budget.replace (/\./g, '');
	budget = budget.replace (/\,/g, '.');
	
	if (isNaN(budget))
	{
		var first=budget2.indexOf(",")+1;
		var last=budget2.lastIndexOf(",")+1;
		var strLength=budget2.length;
		var diff=strLength-last;
		
		if (first==last)
		{
			for (var i=0;i<=strLength-1;i++)
			{
				if (budget2.charAt(i)!="0" && budget2.charAt(i)!="1" && budget2.charAt(i)!="2" && budget2.charAt(i)!="3" && budget2.charAt(i)!="4" && budget2.charAt(i)!="5" && budget2.charAt(i)!="6" && budget2.charAt(i)!="7" && budget2.charAt(i)!="8" && budget2.charAt(i)!="9" && budget2.charAt(i)!="." && budget2.charAt(i)!=",")
				{
					budget=(budget2.substring(0, i)).concat(budget2.substring(i+1, budget2.length));
				}
			}
		}
		else
		{
			if(diff>=2)
			{
				budget=(budget2.substring(0, first-1)).concat(budget2.substring(first, budget2.length));
			}
			else
			{
				budget=(budget2.substring(0, last-1)).concat(budget2.substring(last, budget2.length));
			}
		}
	}
	else
	{
		var len = "";
		var exist = budget.indexOf(".");
		
		if (exist!=-1)
		{
			len = budget.split('.')[1];
		}
		if(len.length>2)
		{
			budget=parseFloat(budget).toFixed(2);
		}
		budget = budget.replace (/\./g, ',');
		budget=dirtyCommas(budget);
		
	}
	
	document.getElementById(pedio).value=budget;
	/////positioning////////
	var length2=document.getElementById(pedio).value.length;
	if (length==length2)
		setCaretTo(document.getElementById(pedio),position);
	else
		setCaretTo(document.getElementById(pedio),position+1);
	/////positioning////////
	
}

function comma2(pedio,id)
{
	var budget;
	var budget2;
	////positioning///
	var position=getCaret(pedio+id);
	var length=document.getElementById(pedio+id).value.length;
	////positioning///
	budget = document.getElementById(pedio+id).value;
	budget2 = document.getElementById(pedio+id).value;
	budget = budget.replace (/\./g, '');
	budget = budget.replace (/\,/g, '.');
	
	if (isNaN(budget))
	{
		var first=budget2.indexOf(",")+1;
		var last=budget2.lastIndexOf(",")+1;
		var strLength=budget2.length;
		var diff=strLength-last;
		
		if (first==last)
		{
			for (var i=0;i<=strLength-1;i++)
			{
				if (budget2.charAt(i)!="0" && budget2.charAt(i)!="1" && budget2.charAt(i)!="2" && budget2.charAt(i)!="3" && budget2.charAt(i)!="4" && budget2.charAt(i)!="5" && budget2.charAt(i)!="6" && budget2.charAt(i)!="7" && budget2.charAt(i)!="8" && budget2.charAt(i)!="9" && budget2.charAt(i)!="." && budget2.charAt(i)!=",")
				{
					budget=(budget2.substring(0, i)).concat(budget2.substring(i+1, budget2.length));
				}
			}
		}
		else
		{
			if(diff>=2)
			{
				budget=(budget2.substring(0, first-1)).concat(budget2.substring(first, budget2.length));
			}
			else
			{
				budget=(budget2.substring(0, last-1)).concat(budget2.substring(last, budget2.length));
			}
		}
	}
	else
	{
		var len = "";
		var exist = budget.indexOf(".");
		
		if (exist!=-1)
		{
			len = budget.split('.')[1];
		}
		if(len.length>2)
		{
			budget=parseFloat(budget).toFixed(2);
		}
		budget = budget.replace (/\./g, ',');
		budget=dirtyCommas(budget);
		
	}
	
	document.getElementById(pedio+id).value=budget;
	/////positioning////////
	var length2=document.getElementById(pedio+id).value.length;
	if (length==length2)
		setCaretTo(document.getElementById(pedio+id),position);
	else
		setCaretTo(document.getElementById(pedio+id),position+1);
	/////positioning////////
}

function comma3(pedio,id,id2)
{
	var budget;
	var budget2;
	var id3=pedio+id+"!"+id2;
	////positioning///
	var position=getCaret(id3);
	var length=document.getElementById(id3).value.length;
	////positioning///
	budget =document.getElementById(id3).value;
	budget2 = document.getElementById(id3).value;;
	budget = budget.replace (/\./g, '');
	budget = budget.replace (/\,/g, '.');
	
	if (isNaN(budget))
	{
		var first=budget2.indexOf(",")+1;
		var last=budget2.lastIndexOf(",")+1;
		var strLength=budget2.length;
		var diff=strLength-last;
		
		if (first==last)
		{
			for (var i=0;i<=strLength-1;i++)
			{
				if (budget2.charAt(i)!="0" && budget2.charAt(i)!="1" && budget2.charAt(i)!="2" && budget2.charAt(i)!="3" && budget2.charAt(i)!="4" && budget2.charAt(i)!="5" && budget2.charAt(i)!="6" && budget2.charAt(i)!="7" && budget2.charAt(i)!="8" && budget2.charAt(i)!="9" && budget2.charAt(i)!="." && budget2.charAt(i)!=",")
				{
					budget=(budget2.substring(0, i)).concat(budget2.substring(i+1, budget2.length));
				}
			}
		}
		else
		{
			if(diff>=2)
			{
				budget=(budget2.substring(0, first-1)).concat(budget2.substring(first, budget2.length));
			}
			else
			{
				budget=(budget2.substring(0, last-1)).concat(budget2.substring(last, budget2.length));
			}
		}
	}
	else
	{
		var len = "";
		var exist = budget.indexOf(".");
		
		if (exist!=-1)
		{
			len = budget.split('.')[1];
		}
		if(len.length>2)
		{
			budget=parseFloat(budget).toFixed(2);
		}
		budget = budget.replace (/\./g, ',');
		budget=dirtyCommas(budget);
		
	}
	
	document.getElementById(id3).value=budget;
	/////positioning////////
	var length2=document.getElementById(id3).value.length;
	if (length==length2)
		setCaretTo(document.getElementById(id3),position);
	else
		setCaretTo(document.getElementById(id3),position+1);
	/////positioning////////
}

function formatNumber1(number) 
{
   var comma = ',',
       string = Math.max(0, number).toFixed(0),
       length = string.length,
       end = /^\d{4,}$/.test(string) ? length % 3 : 0;
   return (end ? string.slice(0, end) + comma : '') + string.slice(end).replace(/(\d{3})(?=\d)/g, '$1' + comma);
 }
  
function formatNumber2(number)
{
   return Math.max(0, number).toFixed(0).replace(/(?=(?:\d{3})+$)(?!^)/g, ',');
}

function dirtyCommas(num)
{
	return String(num).replace(/^\d+(?=,|$)/, function (int) { return int.replace(/(?=(?:\d{3})+$)(?!^)/g, "."); });
}

function reversecomma(pedio)
{
	var budget;
	budget = document.getElementById(pedio).value;
	budget2 = document.getElementById(pedio).value;
	budget = budget.replace (/\./g, '');
	budget = budget.replace (/\,/g, '.');
	
	if (isNaN(budget))
	{
		var first=budget2.indexOf(",")+1;
		var last=budget2.lastIndexOf(",")+1;
		var strLength=budget2.length;
		var diff=strLength-last;
		
		if (first==last)
		{
			for (var i=0;i<=strLength-1;i++)
			{
				if (budget2.charAt(i)!="0" && budget2.charAt(i)!="1" && budget2.charAt(i)!="2" && budget2.charAt(i)!="3" && budget2.charAt(i)!="4" && budget2.charAt(i)!="5" && budget2.charAt(i)!="6" && budget2.charAt(i)!="7" && budget2.charAt(i)!="8" && budget2.charAt(i)!="9" && budget2.charAt(i)!="." && budget2.charAt(i)!=",")
				{
					budget=(budget2.substring(0, i)).concat(budget2.substring(i+1, budget2.length));
				}
			}
		}
		else
		{
			if(diff>=2)
			{
				budget=(budget2.substring(0, first-1)).concat(budget2.substring(first, budget2.length));
			}
			else
			{
				budget=(budget2.substring(0, last-1)).concat(budget2.substring(last, budget2.length));
			}
		}
	}
	else
	{
		var len = "";
		var exist = budget.indexOf(".");
		
		if (exist!=-1)
		{
			len = budget.split('.')[1];
		}
		if(len.length>2)
		{
			budget=parseFloat(budget).toFixed(2);
		}
		budget = budget.replace (/\./g, ',');
		budget=dirtyCommas(budget);
		
	}
	
	document.getElementById(pedio).value=budget;
	
	budget = budget.replace (/\./g, '');
	budget = budget.replace (/\,/g, '.');
	
	return budget;
	
}
	
function setCaretTo(obj, pos) 
{
	if(obj.createTextRange)
	{
		var range = obj.createTextRange();
		range.move('character', pos);
		range.select();
	} 
	else if(obj.selectionStart) 
	{
		obj.focus();
		obj.setSelectionRange(pos, pos);
	}
}

function getCaret(el2) 
{
  el=document.getElementById(el2);
  if (el.selectionStart)
  {
	 return el.selectionStart;
  } 
  else if (document.selection) 
  {
    el.focus();

    var r = document.selection.createRange();
    if (r == null) 
	{
      return 0;
    }

    var re = el.createTextRange(),
    rc = re.duplicate();
    re.moveToBookmark(r.getBookmark());
    rc.setEndPoint('EndToStart', re);
	
	 return rc.text.length;
  } 
  return 0;
}

Number.prototype.formatMoney = function(c, d, t){
var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
 };
 
 
//######################################################################################
// Author: ricocheting.com
// Version: v2.0
// Date: 2011-03-31
// Description: displays the amount of time until the "dateFuture" entered below.

// NOTE: the month entered must be one less than current month. ie; 0=January, 11=December
// NOTE: the hour is in 24 hour format. 0=12am, 15=3pm etc
// format: dateFuture1 = new Date(year,month-1,day,hour,min,sec)
// example: dateFuture1 = new Date(2003,03,26,14,15,00) = April 26, 2003 - 2:15:00 pm

//dateFuture1 = new Date(2011,11,9,3,20,51);
var date1=new Date();
var date2 = date1.getTime(); // this is the milliseconds
//date2 += 1200000;
date2 += 1800000;
// note 60 sec x 60 min x 24 hr x 60 day x milli
date1.setTime(date2);
dateFuture1 = new Date(date1);

// TESTING: comment out the line below to print out the "dateFuture" for testing purposes
//document.write(dateFuture +"<br />");


//###################################
//nothing beyond this point
function GetCount(ddate,iid){

	dateNow = new Date();	//grab current date
	amount = ddate.getTime() - dateNow.getTime();	//calc milliseconds between dates
	delete dateNow;

	// if time is already past
	if(amount < 0){
		//document.getElementById(iid).innerHTML="Ο χρόνος σας έχει λήξει λόγω αδράνειας πρέπει να ξανασυνδεθείτε στο σύστημα!";
		alert("Your time has expired because of inactivity you need to reconnect to the system!");
		window.location.href='../index.php';
	}
	// else date is still good
	else{
		mins=0;secs=0;out="";

		amount = Math.floor(amount/1000);//kill the "milliseconds" so just secs

		mins=Math.floor(amount/60);//minutes
		amount=amount%60;

		secs=Math.floor(amount);//seconds
		if (mins<='9')
			out += "0"+mins +" "+((mins==1)?"minute":"minutes")+":";
		else
			out += mins +" "+((mins==1)?"minute":"minutes")+":";
		
		if (secs<='9')
			out += "0"+secs+" "+((secs==1)?"Second":"Seconds")+", ";
		else
			out += secs+" "+((secs==1)?"Second":"Seconds")+", ";
		
		out = out.substr(0,out.length-2);
		document.getElementById(iid).innerHTML="Remaining Time: " + out;

		setTimeout(function(){GetCount(ddate,iid)}, 1000);
	}
}

window.onload=function(){
	GetCount(dateFuture1, 'countbox1');
	//you can add additional countdowns here (just make sure you create dateFuture2 and countbox2 etc for each)
};