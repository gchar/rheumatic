<!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <li <?php if($cur_url=="index.php" or $cur_url=="user_edit.php") { ?> class="active" <?php } ?> >
				<a href="index.php"><i class="fa fa-users"></i><span> Users</span></a>
			</li>
			<li <?php if($cur_url=="lists.php" or $cur_url=="lists_edit.php" or $cur_url=="list.php" or $cur_url=="list_edit.php") { ?> class="active" <?php } ?> >
				<a href="lists.php"><i class="fa fa-gears"></i><span> Settings-Dynamic lists</span></a>
			</li>
			<li <?php if($cur_url=="diagnosis.php" or $cur_url=="diagnosis_edit.php") { ?> class="active" <?php } ?> >
				<a href="diagnosis.php"><i class="fa fa-gears"></i><span> Diagnosis</span></a>
			</li>
			<li class="dropdown">
              <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gears"></i><span> Drugs <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
               <li <?php if($cur_url=="drugs.php" or $cur_url=="drugs_edit.php") { ?> class="active" <?php } ?> >
				<a href="drugs.php"><i class="fa fa-gears"></i><span>Drugs</span></a>
			</li>
			<li <?php if($cur_url=="drugs_frequency.php" or $cur_url=="drugs_frequency_edit.php") { ?> class="active" <?php } ?> >
				<a href="drugs_frequency.php"><i class="fa fa-gears"></i><span>Drugs frequency</span></a>
			</li>
			<li <?php if($cur_url=="corticosteroids.php" or $cur_url=="corticosteroids_edit.php") { ?> class="active" <?php } ?> >
				<a href="corticosteroids.php"><i class="fa fa-gears"></i><span>Corticosteroids</span></a>
			</li>
			<li <?php if($cur_url=="analgesics.php" or $cur_url=="analgesics_edit.php") { ?> class="active" <?php } ?> >
				<a href="analgesics.php"><i class="fa fa-gears"></i><span>NSAIDS Analgesics</span></a>
			</li>
			 </ul>
            </li>
			<li class="dropdown">
              <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gears"></i><span> Comorbidities <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li <?php if($cur_url=="diseasecomorbidities.php" or $cur_url=="diseasecomorbidities_edit.php") { ?> class="active" <?php } ?> >
					<a href="diseasecomorbidities.php">Disease Comorbidities</span></a>
				</li>
				<li <?php if($cur_url=="therapycomorbidities.php" or $cur_url=="therapycomorbidities_edit.php") { ?> class="active" <?php } ?> >
					<a href="therapycomorbidities.php">Therapy Comorbidities</span></a>
				</li>
			 </ul>
            </li>
			<li <?php if($cur_url=="symptoms.php" or $cur_url=="symptoms_edit.php") { ?> class="active" <?php } ?> >
				<a href="symptoms.php"><i class="fa fa-gears"></i><span> Symptoms</span></a>
			</li>
			<li <?php if($cur_url=="logging.php") { ?> class="active" <?php } ?> >
				<a href="logging.php"><i class="fa fa-laptop"></i><span> Logging</span></a>
			</li>
			<li><a href="../index.php?logout=1"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a></li>
          </ul>
        </div>