 <footer class="main-footer navbar-fixed-bottom">
    <!-- To the right -->
	<?php
	$sql = get_version();
	$result = pg_fetch_array($sql);
	$version=$result['version'];
	?>
    <div class="pull-right hidden-xs">
			<strong>Copyright @ 2020 ICS - FORTH - CeHA - Version:<?php echo $version?></strong>
    </div>
    <!-- Default to the left -->
	<span  style="font-weight:bold;color :#72AFD2;" id="countbox1"></span>
  </footer>