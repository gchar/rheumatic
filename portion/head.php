<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>UCRCR</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="../bower_components/bootstrap/dist/css/bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../dist/css/skins/skin-blue.min.css">  
  <link rel="stylesheet" href="../plugins/datatables/dataTables.bootstrap.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../bower_components/Ionicons/css/ionicons.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="../plugins/select2/select2.css">
  <!-- jquery-ui style -->
  <link rel="stylesheet" href="../bower_components/jquery-ui/themes/base/jquery-ui.css">
  <!-- daterange picker 
  <link rel="stylesheet" href="../bower_components/bootstrap-daterangepicker/daterangepicker.css">-->
  <!-- bootstrap datepicker 
  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <link rel="stylesheet" href="../bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.css">
  -->

  <link rel="stylesheet" href="../css/epoch_styles.css">
  <link rel="stylesheet" href="../css/loading.css">
  <link rel="stylesheet" href="../css/custom.css">
   