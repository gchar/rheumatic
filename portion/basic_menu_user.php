<!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <!--<li><a href="demographics.php"><i class="fa fa-user-plus" aria-hidden="true"></i> New patient</a></li>-->
			<li <?php if($cur_url=="index.php") { ?> class="active" <?php } ?>><a href="index.php" <?php if($_SESSION['pat_id']<>"") { ?> onclick="return confirm('Are you sure you want to exit from patient folder?')" <?php } ?>><i class="fa fa-search" aria-hidden="true"></i> Patient register</a></li>
			<?php 
			if($pat_id<>"")
			{
			?>
            <li class="dropdown">
              <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Patient edit <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li <?php if($cur_url=="demographics.php") { ?> class="active" <?php } ?>><a href="demographics.php">Demographics</a></li>
				<li class="divider"></li>
				<li <?php if($cur_url=="diagnoses.php") { ?> class="active" <?php } ?>><a href="diagnoses.php">Diagnoses</a></li>
				<!--<li <?php if($cur_url=="criteria.php") { ?> class="active" <?php } ?>><a href="criteria.php">Classification criteria</a></li>-->
				<li <?php if($cur_url=="symptoms.php") { ?> class="active" <?php } ?>><a href="symptoms.php">Symptoms not included in the criteria</a></li>
				<li <?php if($cur_url=="otherdisease.php") { ?> class="active" <?php } ?>><a href="otherdisease.php">Other disease characteristics</a></li>
				<li class="divider"></li>
				<li <?php if($cur_url=="antirheumatictreat.php") { ?> class="active" <?php } ?>><a href="antirheumatictreat.php">Previous anti-rheumatic drug treatment</a></li>
				<li class="divider"></li>
				<li <?php if($cur_url=="comorbidities.php") { ?> class="active" <?php } ?>><a href="comorbidities.php">Comorbidities</a></li>
				<li class="divider"></li>
				<li <?php if($cur_url=="biobanking.php") { ?> class="active" <?php } ?>><a href="biobanking.php">Biobanking</a></li>
				<li class="divider"></li>
				<li <?php if($cur_url=="cohorts.php") { ?> class="active" <?php } ?>><a href="cohorts.php">Cohorts</a></li>
				<?php 
					$order = get_cohorts($pat_id);
					$checkstopcohorts=0;
					while($result = pg_fetch_assoc($order))
					{
						if($result['stop_date']=="1900-12-12" or $result['stop_date']=="")
							$checkstopcohorts++;
					}

					//if ($checkstopcohorts>0)
					//	$_SESSION['checkstopcohorts']=1;
					//else
					//	$_SESSION['checkstopcohorts']=0;
					//echo "checkstopcohorts:$checkstopcohorts</br>";
					if ($checkstopcohorts==0)
					{
				?>
				<li><a href="cohort_edit.php?new=1">&nbsp;&nbsp;&nbsp;&nbsp;New cohort</a></li>
				<?php
					}
				$patient_cohort_id=$_SESSION['patient_cohort_id'];
				if($patient_cohort_id<>"")
				{
				?>
				<li <?php if($cur_url=="cohort_edit.php") { ?> class="active" <?php } ?>><a href="cohort_edit.php">&nbsp;&nbsp;&nbsp;&nbsp;Selected cohort</a></li>
				<?php
				}
				?>
			 </ul>
            </li>
			<?php
			$patient_cohort_id=$_SESSION['patient_cohort_id'];
			if($patient_cohort_id<>"")
			{
			?>
			 <li class="dropdown">
              <a href="" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Cohort edit <span class="caret"></span></a>
				 <ul class="dropdown-menu" role="menu">
					<?php
					$patient_cohort_id=$_SESSION['patient_cohort_id'];
					if($patient_cohort_id<>"")
					{
					?>
					<li <?php if($cur_url=="followups.php") { ?> class="active" <?php } ?>><a href="followups.php">Follow-ups</a></li>
					<?php
					}
					?>
					<li><a href="followup_edit.php?new=1">&nbsp;&nbsp;&nbsp;&nbsp;New Follow-up</a></li>
					<?php
					$patient_followup_id=$_SESSION['patient_followup_id'];
					if($patient_followup_id<>"")
					{
					?>
						<li <?php if($cur_url=="followup_edit.php") { ?> class="active" <?php } ?>><a href="followup_edit.php">&nbsp;&nbsp;&nbsp;&nbsp;Selected Follow-up</a></li>
						<li class="divider"></li>
						<li <?php if($cur_url=="adverses.php") { ?> class="active" <?php } ?>><a href="adverses.php">Adverse events</a></li>
					<?php
					}
					$patient_event_id=$_SESSION['patient_event_id'];
					if($patient_event_id<>"")
					{
					?>
					<li <?php if($cur_url=="adverse_edit.php") { ?> class="active" <?php } ?>><a href="adverse_edit.php">&nbsp;&nbsp;&nbsp;&nbsp;Selected Event</a></li>
					<?php
					}
					?>
					<?php
					
					if($patient_cohort_id<>"")
					{
					?>
					<li <?php if($cur_url=="adverses_all.php") { ?> class="active" <?php } ?>><a href="adverses_all.php">All Adverse events</a></li>
					<?php
					}
					?>
				 </ul>
            </li>
			<?php
			}
			}
			?>
			
			<li <?php if($cur_url=="statistics.php") { ?> class="active" <?php } ?>><a href="statistics.php" <?php if($_SESSION['pat_id']<>"") { ?> onclick="return confirm('Are you sure you want to exit from patient folder?')" <?php } ?>>Exports</a></li>
			<li><a href="../index.php?logout=1" onclick="return confirm('Are you sure you want to exit?')" ><i class="fa fa-sign-out" aria-hidden="true" ></i> Logout</a></li>
			<li><a href="#">User: <?php echo $_SESSION['user']; ?> - Centre: <?php echo $_SESSION['user_centre_val']; ?></a></li>
          </ul>
        </div>