<!-- jQuery 3 -->
<script src="../bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap 3.3.7 -->
<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="../dist/js/adminlte.min.js"></script>
<!-- dataTables -->
<script src="../plugins/datatables/jquery.dataTables.min.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap.min.js"></script>
<!--  jQuery UI - v1.11.4 - 2015-03-11 -->
<script src="../plugins/jQueryUI/jquery-ui.js"></script>
<!-- bootstrap datepicker -->
<!--
<script src="../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
<script src="../bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
-->

<!-- Select2 -->
<script src="../plugins/select2/select2.full.min.js"></script>
<!-- InputMask -->
<script src="../plugins/inputmask/dist/jquery.inputmask.bundle.js"></script>

<script src="../js/epoch_classes.js"></script>
<script src="../js/utils.js"></script>
<script src="../js/loading.js"></script>
<script src="../js/moment-with-locales.min.js"></script>
<script src="../js/dateFormat.js"></script>

