<?php
	//set_time_limit(600);
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/functions.php';

	$query = "DROP TABLE statistics_lk1;";
	$result = pg_query($query);
			
	///create table - start 
$query_table = "";
$query_table .= "CREATE TABLE statistics_lk1 (id integer,";	
$query_table .='a1 VARCHAR(250),a2 VARCHAR(250),a3 VARCHAR(250),a4 VARCHAR(250),a5 VARCHAR(250),a6 VARCHAR(250),a7 VARCHAR(250),a8 date,a9 date,a10 VARCHAR(250),';
$query_table .='a11 VARCHAR(250),a12 VARCHAR(250),a13 VARCHAR(250),a14 VARCHAR(250),a15 VARCHAR(250),a16 VARCHAR(250),a17 VARCHAR(250),a18 VARCHAR(250),';
$query_table .='a19 VARCHAR(250),a20 VARCHAR(250),a21 VARCHAR(250),a22 VARCHAR(250),a23 VARCHAR(250),';
$query_table .='a24 VARCHAR(250),a25 VARCHAR(250),a26 VARCHAR(250),a27 VARCHAR(250),a28 VARCHAR(250),';
$query_table .='a29 VARCHAR(250),a30 VARCHAR(250),';
////comorbidities - start////
$query_table .='a31 VARCHAR(250),a32 VARCHAR(250),a33 VARCHAR(250),a34 VARCHAR(250),a35 VARCHAR(250),a36 VARCHAR(250),a37 VARCHAR(250),a38 VARCHAR(250),';
$query_table .='a39 VARCHAR(250),a40 VARCHAR(250),a41 VARCHAR(250),a42 VARCHAR(250),a43 VARCHAR(250),a44 VARCHAR(250),a45 VARCHAR(250),';
$query_table .='a46 VARCHAR(250),a47 VARCHAR(250),a48 VARCHAR(250),a49 VARCHAR(250),a50 VARCHAR(250),a51 VARCHAR(250),';
$query_table .='a52 VARCHAR(250),a53 VARCHAR(250),a54 VARCHAR(250),a55 VARCHAR(250),a56 VARCHAR(250),a57 VARCHAR(250),a58 VARCHAR(250),a59 VARCHAR(250),';
$query_table .='a60 VARCHAR(250),a61 VARCHAR(250),a62 VARCHAR(250),a63 VARCHAR(250),a64 VARCHAR(250),a65 VARCHAR(250),';
//$query_table .='Other (unspecified) liver cirrhosis VARCHAR(250),';
$query_table .='a66 VARCHAR(250),';
//$query_table .='Drug-induced chronic liver disease VARCHAR(250),';
$query_table .='a67 VARCHAR(250),a68 VARCHAR(250),a69 VARCHAR(250),a70 VARCHAR(250),a71 VARCHAR(250),a72 VARCHAR(250),a73 VARCHAR(250),a74 VARCHAR(250),a75 VARCHAR(250),';
$query_table .='a76 VARCHAR(250),a77 VARCHAR(250),a78 VARCHAR(250),a79 VARCHAR(250),a80 VARCHAR(250),';
$query_table .='a81 VARCHAR(250),a82 VARCHAR(250),a83 VARCHAR(250),a84 VARCHAR(250),a85 VARCHAR(250),a86 VARCHAR(250),a87 VARCHAR(250),';
$query_table .='a88 VARCHAR(250),a89 VARCHAR(250),a90 VARCHAR(250),a91 VARCHAR(250),a92 VARCHAR(250),a93 VARCHAR(250),';
$query_table .='a94 VARCHAR(250),a95 VARCHAR(250),a96 VARCHAR(250),a97 VARCHAR(250),a98 VARCHAR(250),a99 VARCHAR(250),';
////comorbidities - end////
$i=100;
////previous anti-reumatic drug treatment_nbr - start////

$exec2 = get_drugs_anti();
while($result2 = pg_fetch_array($exec2))
{	
	$query_table .= "a";
	$query_table .= $i;
	$query_table .= ' VARCHAR(250),';
	$i++;
}


////previous anti-reumatic drug treatment_nbr - end////

////other diseases characteristics - autoantibodies - start////


	for($j=$i;$j<=$i+1;$j++)
	{
		$query_table .= "a";
		$query_table .=$j;
		$query_table .=" VARCHAR(250),";
	}
	$i=$i+2;
////other diseases characteristics - autoantibodies - end////
////severity - start////

	for($j=$i;$j<=$i;$j++)
	{
		$query_table .= "a";
		$query_table .=$j;
		$query_table .=" VARCHAR(250),";
	}
	$i=$i+1;
	
////severity - end////
////B2-Criteria - start////
	for($j=$i;$j<=$i+4;$j++)
	{
		$query_table .= "a";
		$query_table .=$j;
		if($j<>$i+4)
			$query_table .=" VARCHAR(250),";
		else
			$query_table .=" VARCHAR(250)";
	}
	$i=$i+5;
	$query_table .= ");";
	$result = pg_query($query_table);
////B2-Criteria - end////
	
	///insert first row - start 
		
$query_table ="INSERT INTO statistics_lk1 VALUES ( 1,'cohort_id','pat_id','drug_id','substance','centre_id','centre_name','treatment_nbr','01-01-2019','01-01-2019','stop_reason','";
$query_table .="stop_cause_comments','gender','date_of_inclusion','date_of_birth','main_diagnosis_date','icd10a','main_diagnosis_value','main_symptom_date','";
$query_table .="first_secondary_diagnosis_date','first_secondary_icd10b','first_secondary_diagnosis_value','first_secondary_symptom_date','second_secondary_diagnosis_date','";
$query_table .="second_secondary_icd10b','second_secondary_diagnosis_value','second_secondary_symptom_date','third_secondary_diagnosis_date','third_secondary_icd10b','";
$query_table .="third_secondary_diagnosis_value','third_secondary_symptom_date','";
////comorbidities - start////
$query_table .="Aortic_Aneurysm','Cardiac_arrhythmia','Congestive_Heart_Failure','Coronary_Heart_Disease','Acute_myocardial_infarction','Angina_pectoris','Atrial_fibrillation','Pericardial_disease_Acute_Pericarditis','";
$query_table .="Hypertension','Cardiomyopathies','Peripheral_vascular_disease','Valvular_Heart_Disease','Ischemic_strokeTIA','Sequelae_of_ischemic_stroke','Hemorrhagic_stroke','";
$query_table .="Sequelae_of_hemorrhagic_stroke','Anxiety_disorders','Bipolar_disorder','Dementia_unspecified','Major_depression_without_psychotic_symptoms','Other_depressive_episodes','";
$query_table .="Multiple_Sclerosis_Demyelinating_disease','Parkinsons_disease','Peripheral_Neuropathy','Psychosis','Schizophrenia','Asthma','Bronchiectasis','Chronic_obstructive_pulmonary_disease','";
$query_table .="Chronic_sinusitis','Interstitial_Lung_Disease','Pulmonary_Hypertension','Autoimmune_hepatitis','Alcoholic_liver_disease','Alcoholic_liver_cirrhosis','";
//$query_table .="Other (unspecified) liver cirrhosis','";
$query_table .="Nonalcoholic_fatty_liver_disease_NAFLD','";
//$query_table .="Drug-induced chronic liver disease','";
$query_table .="Crohn’s_disease','Diverticulosis','EsophagitisGERD','Peptic_ulcer_disease_biopsy_proven','Diabetes_mellitus','Dyslipidaemia','Hypothyroidism','Hyperthyroidism','Obesity','";
$query_table .="Chronic_kidney_disease_stage_III_GFR60','Chronic_kidney_disease_stage_IIIIV_(GFR_15-59)','End-stage_renal_disease_GFR_15','Hodgkin_Lymphoma','Non-Hodgkin_Lymphoma_unspecified','";
$query_table .="Leukemia','Solid_tumor','Solid_tumor_without_metastasi','Metastatic_solid_tumor','Metastatic_cancer_without_known_primary_tumor_site','Ischemic_Optic_Neuropathy','Optic_Neuritis','";
$query_table .="Chronic_osteomyelitis','Latent_TB_infection','Viral_hepatitis','Chronic_viral_infection_other','Past_hospitalization_for_serious_infection','Recurrent_past_hospitalizations_for_serious_infections','";
$query_table .="History_of_opportunistic_infection','Prosthetic_left_hip_joint','Prosthetic_right_hip_joint','Prosthetic_left_knee_joint','Prosthetic_right_knee_joint','Prosthetic_joint_other',";
////comorbidities - end////

////previous anti-reumatic drug treatment_nbr - start////

$exec2 = get_drugs_anti();
while($result2 = pg_fetch_array($exec2))
{	
	$query_table .= "'";
	$query_table .= str_replace("'","",$result2['substance']);
	$query_table .= "',";
}

////previous anti-reumatic drug treatment_nbr - end////

////other diseases characteristics - autoantibodies - start////

	$query_table .="'Anti-CCP','Rheumatoid Factor',";
////other diseases characteristics - autoantibodies - end////
////severity - start////
	$query_table .="'erosions',";
////severity - end////
////B2-Criteria - start////
	$query_table .="'RA','SpA','CASPAR','SLE','APS'";
////B2-Criteria - end////
$query_table .= ');';

$result_query_table = pg_query($query_table);

///insert data - start
	
$query = "select patient_cohort.erosions,patient_cohort.patient_cohort_id,patient_cohort.pat_id,patient_cohort.cohort_name,drugs.substance,patient.centre,lookup_tbl_val.value as centre_name,patient_cohort.treatment_nbr,TO_CHAR(patient_cohort.start_date, 'DD-MM-YYYY') AS start_date_str,TO_CHAR(patient_cohort.stop_date, 'DD-MM-YYYY') AS stop_date_str";
$query .= ",patient_cohort.stop_reason,patient_cohort.stop_ae_reason,patient.gender,TO_CHAR(patient.dateofinclusion, 'DD-MM-YYYY') AS dateofinclusion_str,TO_CHAR(patient.dateofbirth, 'DD-MM-YYYY') AS dateofbirth_str from patient_cohort left join patient on patient.pat_id=patient_cohort.pat_id left join drugs on patient_cohort.cohort_name=drugs.drugs_id left join lookup_tbl_val on lookup_tbl_val.id=patient.centre where patient.deleted=0 and patient_cohort.deleted=0 limit 100";
$exec = pg_query($query);
$num_rows = pg_num_rows($exec);

$row=1;
while($result = pg_fetch_array($exec))
{
	$row++;
$patient_cohort_id=$result['patient_cohort_id'];
$pat_id=$result['pat_id'];
$cohort_name=str_replace(",","",$result['cohort_name']);
$cohort_name=str_replace("'","",$cohort_name);
$substance=str_replace(",","",$result['substance']);
$substance=str_replace("'","",$substance);
$centre=$result['centre'];
$centre_name=str_replace(",","",$result['centre_name']);
$centre_name=str_replace("'","",$centre_name);
$treatment_nbr=$result['treatment_nbr'];
$start_date=$result['start_date_str'];
if ($start_date=="")
	$start_date="1900-01-01";
$stop_date=$result['stop_date_str'];
if ($stop_date=="")
	$stop_date="1900-01-01";
$stop_reason=$result['stop_reason'];
$stop_ae_reason=str_replace(",","",$result['stop_ae_reason']);
$stop_ae_reason=str_replace("'","",$stop_ae_reason);
$gender=$result['gender'];
$dateofinclusion=$result['dateofinclusion_str'];
$dateofbirth=$result['dateofbirth_str'];
$erosions=str_replace(",","",$result['erosions']);
$erosions=str_replace("'","",$erosions);

$query_table ="INSERT INTO statistics_lk1 VALUES ( $row,'$patient_cohort_id','$pat_id','$cohort_name','$substance','$centre','$centre_name','$treatment_nbr','$start_date','$stop_date','$stop_reason','";
$query_table .="$stop_ae_reason','$gender','$dateofinclusion','$dateofbirth',";


$query2 = "select patient_diagnosis.diagnosis_id,TO_CHAR(patient_diagnosis.symptom_date, 'DD-MM-YYYY') AS symptom_date_str,TO_CHAR(patient_diagnosis.disease_date, 'DD-MM-YYYY') AS disease_date_str,patient_diagnosis.icd10";
$query2 .= ",diagnosis.value from patient_diagnosis left join diagnosis on diagnosis.diagnosis_id=patient_diagnosis.diagnosis_id where patient_diagnosis.deleted=0 and patient_diagnosis.pat_id=$pat_id and patient_diagnosis.main=1";
$exec2 = pg_query($query2);
$result2 = pg_fetch_array($exec2);
$main_diagnosis_date=$result2['disease_date_str'];
$icd10a=str_replace(",","",$result2['icd10']);
$icd10a=str_replace("'","",$icd10a);
$main_diagnosis_value=str_replace(",","",$result2['value']);
$main_diagnosis_value=str_replace("'","",$main_diagnosis_value);
$main_symptom_date=$result2['symptom_date_str'];


$query_table .="'$main_diagnosis_date','$icd10a','$main_diagnosis_value','$main_symptom_date','";

$query2 = "select patient_diagnosis.diagnosis_id,TO_CHAR(patient_diagnosis.symptom_date, 'DD-MM-YYYY') AS symptom_date_str,TO_CHAR(patient_diagnosis.disease_date, 'DD-MM-YYYY') AS disease_date_str,patient_diagnosis.icd10";
$query2 .= ",diagnosis.value from patient_diagnosis left join diagnosis on diagnosis.diagnosis_id=patient_diagnosis.diagnosis_id where patient_diagnosis.deleted=0 and patient_diagnosis.pat_id=$pat_id and patient_diagnosis.main=0 limit 3";

$exec2 = pg_query($query2);

$result2 = pg_fetch_array($exec2);
$secondary_diagnosis_date=$result2['disease_date_str'];
$icd10a=str_replace(",","",$result2['icd10']);
$icd10a=str_replace("'","",$icd10a);
$secondary_diagnosis_value=str_replace(",","",$result2['value']);
$secondary_diagnosis_value=str_replace("'","",$secondary_diagnosis_value);
$secondary_symptom_date=$result2['symptom_date_str'];

$query_table .="$secondary_diagnosis_date','$icd10a','$secondary_diagnosis_value','$secondary_symptom_date','";

$result2 = pg_fetch_array($exec2);
$secondary_diagnosis_date=$result2['disease_date_str'];
$icd10a=str_replace(",","",$result2['icd10']);
$icd10a=str_replace("'","",$icd10a);
$secondary_diagnosis_value=str_replace(",","",$result2['value']);
$secondary_diagnosis_value=str_replace("'","",$secondary_diagnosis_value);
$secondary_symptom_date=$result2['symptom_date_str'];

$query_table .="$secondary_diagnosis_date','$icd10a','$secondary_diagnosis_value','$secondary_symptom_date','";

$result2 = pg_fetch_array($exec2);
$secondary_diagnosis_date=$result2['disease_date_str'];
$icd10a=str_replace(",","",$result2['icd10']);
$icd10a=str_replace("'","",$icd10a);
$secondary_diagnosis_value=str_replace(",","",$result2['value']);
$secondary_diagnosis_value=str_replace("'","",$secondary_diagnosis_value);
$secondary_symptom_date=$result2['symptom_date_str'];

$query_table .="$secondary_diagnosis_date','$icd10a','$secondary_diagnosis_value','$secondary_symptom_date','";

////comorbidities - start////
$exec2 = get_comorbidities_rpt();
while($result2 = pg_fetch_array($exec2))
{	
	$diseases_comorbidities_id = $result2['diseases_comorbidities_id'];
	$query3 = "select count(*) as cnt from patient_diagnosis where pat_id=$pat_id and deleted=0 and diagnosis_id=$diseases_comorbidities_id";
	$exec3 = pg_query($query3);
	$result3 = pg_fetch_array($exec3);
	$cnt=$result3['cnt'];
	
	if($cnt>0)
		$query_table .="TRUE";
	else
		$query_table .="FALSE";
	
	$query_table .="','";
}

////comorbidities - end////

////previous anti-reumatic drug treatment_nbr - start////
$exec2 = get_drugs_anti();
while($result2 = pg_fetch_array($exec2))
{	
	$previous_drugs_id = $result2['drugs_id'];
	$query3 = "select count(*) as cnt from patient_previous_drugs where pat_id=$pat_id and deleted=0 and drugs_id=$previous_drugs_id";
	$exec3 = pg_query($query3);
	$result3 = pg_fetch_array($exec3);
	$cnt=$result3['cnt'];
	
	if($cnt>0)
		$query_table .="TRUE";
	else
		$query_table .="FALSE";
	
	$query_table .="','";

}
////previous anti-reumatic drug treatment_nbr - end////

////other diseases characteristics - autoantibodies - start////
	$query3 = "select count(*) as cnt from patient_autoantibodies where pat_id=$pat_id and deleted=0 and antibodies_id=66";
	$exec3 = pg_query($query3);
	$result3 = pg_fetch_array($exec3);
	$cnt=$result3['cnt'];
	
	if($cnt>0)
		$query_table .="TRUE";
	else
		$query_table .="FALSE";
	
	$query_table .="','";
	
	
	$query3 = "select count(*) as cnt from patient_autoantibodies where pat_id=$pat_id and deleted=0 and antibodies_id=65";
	$exec3 = pg_query($query3);
	$result3 = pg_fetch_array($exec3);
	$cnt=$result3['cnt'];
	
	if($cnt>0)
		$query_table .="TRUE";
	else
		$query_table .="FALSE";
	
	$query_table .="',";
////other diseases characteristics - autoantibodies - end////
////severity - start////
	$query_table .="'";
	$query_table .=$erosions;
	$query_table .="','";
////severity - end////

////B2-Criteria - start////

	$order = get_racriteria_res($pat_id);
	$result2 = pg_fetch_assoc($order);
	$rheumatoid=$result2['rheumatoid'];
	if($rheumatoid=='Yes')
		$query_table .="TRUE";
	else
		$query_table .="FALSE";
	
	$query_table .="','";
	
	$order = get_peripheralcriteria_res($pat_id);
	$result2 = pg_fetch_assoc($order);
	$set1=$result2['set1'];
	$set2=$result2['set2'];
	
	$order = get_axialcriteria_res($pat_id);
	$result2 = pg_fetch_assoc($order);
	$newyork=$result2['newyork'];
	$imaging=$result2['imaging'];
	$clinical=$result2['clinical'];
	$radiographic=$result2['radiographic'];
	
	if($set1=='Yes' or $set2=='Yes' or $newyork=='Yes' or $imaging=='Yes' or $clinical=='Yes' or $radiographic=='Yes')
		$query_table .="TRUE";
	else
		$query_table .="FALSE";
	
	$query_table .="','";
	
	$order = get_casparcriteria_res($pat_id);
	$result2 = pg_fetch_assoc($order);
	$caspar=$result2['caspar'];
	if($caspar=='Yes')
		$query_table .="TRUE";
	else
		$query_table .="FALSE";
	
	$query_table .="','";
	
	$order = get_criteriasle_res($pat_id);
	$result2 = pg_fetch_assoc($order);
	$sle1997=$result2['sle1997'];
	$sle2012=$result2['sle2012'];
	$sle2017=$result2['sle2017'];
	
	if($sle1997=='Yes' or $sle2012=='Yes' or $sle2017=='Yes')
		$query_table .="TRUE";
	else
		$query_table .="FALSE";
	
	$query_table .="','";
	
	$order = get_apscriteria_res($pat_id);
	$result2 = pg_fetch_assoc($order);
	$aps=$result2['aps'];
	if($aps=='Yes')
		$query_table .="TRUE";
	else
		$query_table .="FALSE";
	
	$query_table .="'";
	$query_table .= ');';
	$result_query_table = pg_query($query_table);
	echo "Εγγραφή:".($row-1)." χρόνος:".date("Y-m-d H:i:s")."</br>";
////B2-Criteria - end////
}

if($num_rows==$row-1)
	echo "<b>Η εισαγωγή στοιχείων έγινε κανονικά</b>";
else
{
		echo "<b>Η εισαγωγή στοιχείων δεν έγινε κανονικά.</b>";
		echo "Δεν μπήκαν ".($num_rows-$row-1)." εγγραφές";
}
include '../library/closeDB.php';
?>
