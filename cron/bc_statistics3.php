<?php
	set_time_limit(600);
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/functions.php';
	$query = "DROP TABLE statistics_lk3;";
	$result = pg_query($query);
	
	///create table - start 

////Corticosteroids & NSAID index - start////
$query_table = "";
$query_table .= "CREATE TABLE statistics_lk3 (id integer,";
	for($j=$i;$j<=$i+27;$j++)
	{
		$query_table .= "a";
		$query_table .=$j;
		$query_table .=" VARCHAR(250),";
	}
	$i=$i+28;
	
////Corticosteroids & NSAID index - end////

////Disease activity Common & Spa Euroqol - start////

	for($j=$i;$j<=$i+187;$j++)
	{
		$query_table .= "a";
		$query_table .=$j;
		if($j<>$i+187)
			$query_table .=" VARCHAR(250),";
		else
			$query_table .=" VARCHAR(250)";
	}
	$i=$i+188;
	
	$query_table .= ");";
	$result_query_table = pg_query($query_table);
////Disease activity Common & Spa Euroqol - end////

	
	///create table - end 
	
	///insert first row - start 
	
////Corticosteroids & NSAID index - start////
$query_table ="INSERT INTO statistics_lk3 VALUES (1,";
	$query_table .="'J-0','J1-0','J2-0','J-3','J1-3','J2-3','J-6','J1-6','J2-6','J-12','J1-12','J2-12',";
	$query_table .="'J-18','J1-18','J2-18','J-24','J1-24','J2-24','J-36','J1-36','J2-36','J-48','J1-48','J2-48','J-60','J1-60','J2-60',";
	

////Corticosteroids & NSAID index - end////

////Disease activity Common & Spa Euroqol - start////

	$query_table .="'28swollen_JC-0','28tender_JC-0','BASDAI-0','BASDAI1-0','BASDAI2-0','BASDAI3-0','BASDAI4-0','BASDAI5-0','BASDAI6-0','BASFI-0','CRP-0','DAS28CRP-0','ESR-0',";
	$query_table .="'VAS_physisian-0','Euroqol_total-0','EQ_5D_score-0','mHAQ-0','VASglobal-0','VASpain-0','Weight-0','Height-0',";
	$query_table .="'28swollen_JC-3','28tender_JC-3','BASDAI-3','BASDAI1-3','BASDAI2-3','BASDAI3-3','BASDAI4-3','BASDAI5-3','BASDAI6-3','BASFI-3','CRP-3','DAS28CRP-3','ESR-3',";
	$query_table .="'VAS_physisian-3','Euroqol_total-3','EQ_5D_score-3','mHAQ-3','VASglobal-3','VASpain-3','Weight-3','Height-3',";
	$query_table .="'28swollen_JC-6','28tender_JC-6','BASDAI-6','BASDAI1-6','BASDAI2-6','BASDAI3-6','BASDAI4-6','BASDAI5-6','BASDAI6-6','BASFI-6','CRP-6','DAS28CRP-6','ESR-6',";
	$query_table .="'VAS_physisian-6','Euroqol_total-6','EQ_5D_score-6','mHAQ-6','VASglobal-6','VASpain-6','Weight-6','Height-6',";
	$query_table .="'28swollen_JC-12','28tender_JC-12','BASDAI-12','BASDAI1-12','BASDAI2-12','BASDAI3-12','BASDAI4-12','BASDAI5-12','BASDAI6-12','BASFI-12','CRP-12','DAS28CRP-12','ESR-12',";
	$query_table .="'VAS_physisian-12','Euroqol_total-12','EQ_5D_score-12','mHAQ-12','VASglobal-12','VASpain-12','Weight-12','Height-12',";
	$query_table .="'28swollen JC-18','28tender JC-18','BASDAI-18','BASDAI1-18','BASDAI2-18','BASDAI3-18','BASDAI4-18','BASDAI5-18','BASDAI6-18','BASFI-18','CRP-18','DAS28CRP-18','ESR-18',";
	$query_table .="'VAS physisian-18','Euroqol total-18','EQ 5D score-18','mHAQ-18','VASglobal-18','VASpain-18','Weight-18','Height-18',";
	$query_table .="'28swollen JC-24','28tender JC-24','BASDAI-24','BASDAI1-24','BASDAI2-24','BASDAI3-24','BASDAI4-24','BASDAI5-24','BASDAI6-24','BASFI-24','CRP-24','DAS28CRP-24','ESR-24',";
	$query_table .="'VAS physisian-24','Euroqol total-24','EQ 5D score-24','mHAQ-24','VASglobal-24','VASpain-24','Weight-24','Height-24',";
	$query_table .="'28swollen JC-36','28tender JC-36','BASDAI-36','BASDAI1-36','BASDAI2-36','BASDAI3-36','BASDAI4-36','BASDAI5-36','BASDAI6-36','BASFI-36','CRP-36','DAS28CRP-36','ESR-36',";
	$query_table .="'VAS physisian-36','Euroqol total-36','EQ 5D score-36','mHAQ-36','VASglobal-36','VASpain-36','Weight-36','Height-36',";
	$query_table .="'28swollen JC-48','28tender JC-48','BASDAI-48','BASDAI1-48','BASDAI2-48','BASDAI3-48','BASDAI4-48','BASDAI5-48','BASDAI6-48','BASFI-48','CRP-48','DAS28CRP-48','ESR-48',";
	$query_table .="'VAS physisian-48','Euroqol total-48','EQ 5D score-48','mHAQ-48','VASglobal-48','VASpain-48','Weight-48','Height-48',";
	$query_table .="'28swollen JC-60','28tender JC-60','BASDAI-60','BASDAI1-60','BASDAI2-60','BASDAI3-60','BASDAI4-60','BASDAI5-60','BASDAI6-60','BASFI-60','CRP-60','DAS28CRP-60','ESR-60',";
	$query_table .="'VAS physisian-60','Euroqol total-60','EQ 5D score-60','mHAQ-60','VASglobal-60','VASpain-60','Weight-60','Height-60'";
////Disease activity Common & Spa Euroqol - end////

	$query_table .= ');';
	
	$result_query_table = pg_query($query_table);
	
	///insert first row - end 

	
	///insert data - start
	
$query = "select patient_cohort.patient_cohort_id,patient_cohort.pat_id from patient_cohort left join patient on patient.pat_id=patient_cohort.pat_id where patient.deleted=0 and patient_cohort.deleted=0 limit 1";
$exec = pg_query($query);
$num_rows = pg_num_rows($exec);

$row=1;
while($result = pg_fetch_array($exec))
{
	$row++;
$patient_cohort_id=$result['patient_cohort_id'];
$pat_id=$result['pat_id'];


	$query_table ="INSERT INTO statistics_lk3 VALUES ( $row,";
	////Corticosteroids & NSAID index - start////

	$sql3 = get_drugs_corticosteroids_dosage($pat_id,0,$patient_cohort_id,0);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	////$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,1,$patient_cohort_id,0);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,2,$patient_cohort_id,0);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,0,$patient_cohort_id,3);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,1,$patient_cohort_id,3);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,2,$patient_cohort_id,3);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,0,$patient_cohort_id,6);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,1,$patient_cohort_id,6);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,2,$patient_cohort_id,6);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,0,$patient_cohort_id,12);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,1,$patient_cohort_id,12);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,2,$patient_cohort_id,12);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,0,$patient_cohort_id,18);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,1,$patient_cohort_id,18);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,2,$patient_cohort_id,18);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,0,$patient_cohort_id,24);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,1,$patient_cohort_id,24);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,2,$patient_cohort_id,24);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,0,$patient_cohort_id,36);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,1,$patient_cohort_id,36);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,2,$patient_cohort_id,36);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,0,$patient_cohort_id,48);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,1,$patient_cohort_id,48);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,2,$patient_cohort_id,48);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,0,$patient_cohort_id,60);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,1,$patient_cohort_id,60);
	$result3 = pg_fetch_array($sql3);
	//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,2,$patient_cohort_id,60);
	$result3 = pg_fetch_array($sql3);
//$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=$result3['weekdosage'];
	//$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
////Corticosteroids & NSAID index - end////

////Disease activity Common & Spa Euroqol - start////
	$sql3 = get_activity_rpt($pat_id,$patient_cohort_id,0);
	$result3 = pg_fetch_array($sql3);
	$sql4 = get_spa_rpt($pat_id,$patient_cohort_id,0);
	$result4 = pg_fetch_array($sql4);
	$sql5 = get_euroqol_rpt($pat_id,$patient_cohort_id,0);
	$result5 = pg_fetch_array($sql5);
	/*$swollenjc28=str_replace(",",".",$result3['swollenjc28']);
	$swollenjc28=str_replace("'","",$swollenjc28);
	$tenderjc28=str_replace(",",".",$result3['tenderjc28']);
	$tenderjc28=str_replace("'","",$tenderjc28);
	$basdai=str_replace(",",".",$result4['basdai']);
	$basdai=str_replace("'","",$basdai);
	$basdai1=str_replace(",",".",$result4['basdai1']);
	$basdai1=str_replace("'","",$basdai1);
	$basdai2=str_replace(",",".",$result4['basdai2']);
	$basdai2=str_replace("'","",$basdai2);
	$basdai3=str_replace(",",".",$result4['basdai3']);
	$basdai3=str_replace("'","",$basdai3);
	$basdai4=str_replace(",",".",$result4['basdai4']);
	$basdai4=str_replace("'","",$basdai4);
	$basdai5=str_replace(",",".",$result4['basdai5']);
	$basdai5=str_replace("'","",$basdai5);
	$basdai6=str_replace(",",".",$result4['basdai6']);
	$basdai6=str_replace("'","",$basdai6);
	$basfi=str_replace(",",".",$result4['basfi']);
	$basfi=str_replace("'","",$basfi);
	$crp=str_replace(",",".",$result3['crp']);
	$crp=str_replace("'","",$crp);
	$das28crp=str_replace(",",".",$result3['das28crp']);
	$das28crp=str_replace("'","",$das28crp);
	$esr=str_replace(",",".",$result3['esr']);
	$esr=str_replace("'","",$esr);
	$euroqol_score=str_replace(",",".",$result5['euroqol_score']);
	$euroqol_score=str_replace("'","",$euroqol_score);
	$vasphysicial=str_replace(",",".",$result3['vasphysicial']);
	$vasphysicial=str_replace("'","",$vasphysicial);
	$eq5d=str_replace(",",".",$result5['eq5d']);
	$eq5d=str_replace("'","",$eq5d);
	$mhaq=str_replace("'","",$mhaq);
	$mhaq=str_replace(",",".",$result3['mhaq']);
	$mhaq=str_replace("'","",$mhaq);
	$vasglobal=str_replace(",",".",$result3['vasglobal']);
	$vasglobal=str_replace("'","",$vasglobal);
	$vaspain=str_replace(",",".",$result3['vaspain']);
	$vaspain=str_replace("'","",$vaspain);
	$weight=str_replace(",",".",$result3['weight']);
	$weight=str_replace("'","",$weight);
	$height=str_replace(",",".",$result3['height']);
	$height=str_replace("'","",$height);*/
	$swollenjc28=$result3['swollenjc28'];
	$tenderjc28=$result3['tenderjc28'];
	$basdai=$result4['basdai'];
	$basdai=$result4['basdai1'];
	$basdai=$result4['basdai2'];
	$basdai=$result4['basdai3'];
	$basdai=$result4['basdai4'];
	$basdai=$result4['basdai5'];
	$basdai=$result4['basdai6'];
	$basdai=$result4['basfi'];
	$crp=$result3['crp'];
	$das28crp=$result3['das28crp'];
	$esr=$result3['esr'];
	$euroqol_score=$result5['euroqol_score'];
	$vasphysicial=$result3['vasphysicial'];
	$eq5d=$result5['eq5d'];
	$mhaq=$result3['mhaq'];
	$vasglobal=$result3['vasglobal'];
	$vaspain=$result3['vaspain'];
	$weight=$result3['weight'];
	$height=$result3['height'];
	
	$query_table .="'$swollenjc28','$tenderjc28','$basdai','$basdai1','$basdai2','$basdai3','$basdai4',";
	$query_table .="'$basdai5','$basdai6','$basfi','$crp','$das28crp','$esr','$euroqol_score',";
	$query_table .="'$vasphysicial','$eq5d','$mhaq','$vasglobal','$vaspain','$weight','$height',";
	
	$sql3 = get_activity_rpt($pat_id,$patient_cohort_id,3);
	$result3 = pg_fetch_array($sql3);
	$sql4 = get_spa_rpt($pat_id,$patient_cohort_id,3);
	$result4 = pg_fetch_array($sql4);
	$sql5 = get_euroqol_rpt($pat_id,$patient_cohort_id,3);
	$result5 = pg_fetch_array($sql5);
	$swollenjc28=$result3['swollenjc28'];
	$tenderjc28=$result3['tenderjc28'];
	$basdai=$result4['basdai'];
	$basdai=$result4['basdai1'];
	$basdai=$result4['basdai2'];
	$basdai=$result4['basdai3'];
	$basdai=$result4['basdai4'];
	$basdai=$result4['basdai5'];
	$basdai=$result4['basdai6'];
	$basdai=$result4['basfi'];
	$crp=$result3['crp'];
	$das28crp=$result3['das28crp'];
	$esr=$result3['esr'];
	$euroqol_score=$result5['euroqol_score'];
	$vasphysicial=$result3['vasphysicial'];
	$eq5d=$result5['eq5d'];
	$mhaq=$result3['mhaq'];
	$vasglobal=$result3['vasglobal'];
	$vaspain=$result3['vaspain'];
	$weight=$result3['weight'];
	$height=$result3['height'];
	
	$query_table .="'$swollenjc28','$tenderjc28','$basdai','$basdai1','$basdai2','$basdai3','$basdai4',";
	$query_table .="'$basdai5','$basdai6','$basfi','$crp','$das28crp','$esr','$euroqol_score',";
	$query_table .="'$vasphysicial','$eq5d','$mhaq','$vasglobal','$vaspain','$weight','$height',";
	
	$sql3 = get_activity_rpt($pat_id,$patient_cohort_id,6);
	$result3 = pg_fetch_array($sql3);
	$sql4 = get_spa_rpt($pat_id,$patient_cohort_id,6);
	$result4 = pg_fetch_array($sql4);
	$sql5 = get_euroqol_rpt($pat_id,$patient_cohort_id,6);
	$result5 = pg_fetch_array($sql5);
	$swollenjc28=$result3['swollenjc28'];
	$tenderjc28=$result3['tenderjc28'];
	$basdai=$result4['basdai'];
	$basdai=$result4['basdai1'];
	$basdai=$result4['basdai2'];
	$basdai=$result4['basdai3'];
	$basdai=$result4['basdai4'];
	$basdai=$result4['basdai5'];
	$basdai=$result4['basdai6'];
	$basdai=$result4['basfi'];
	$crp=$result3['crp'];
	$das28crp=$result3['das28crp'];
	$esr=$result3['esr'];
	$euroqol_score=$result5['euroqol_score'];
	$vasphysicial=$result3['vasphysicial'];
	$eq5d=$result5['eq5d'];
	$mhaq=$result3['mhaq'];
	$vasglobal=$result3['vasglobal'];
	$vaspain=$result3['vaspain'];
	$weight=$result3['weight'];
	$height=$result3['height'];
	
	$query_table .="'$swollenjc28','$tenderjc28','$basdai','$basdai1','$basdai2','$basdai3','$basdai4',";
	$query_table .="'$basdai5','$basdai6','$basfi','$crp','$das28crp','$esr','$euroqol_score',";
	$query_table .="'$vasphysicial','$eq5d','$mhaq','$vasglobal','$vaspain','$weight','$height',";
	
	$sql3 = get_activity_rpt($pat_id,$patient_cohort_id,12);
	$result3 = pg_fetch_array($sql3);
	$sql4 = get_spa_rpt($pat_id,$patient_cohort_id,12);
	$result4 = pg_fetch_array($sql4);
	$sql5 = get_euroqol_rpt($pat_id,$patient_cohort_id,12);
	$result5 = pg_fetch_array($sql5);
	$swollenjc28=$result3['swollenjc28'];
	$tenderjc28=$result3['tenderjc28'];
	$basdai=$result4['basdai'];
	$basdai=$result4['basdai1'];
	$basdai=$result4['basdai2'];
	$basdai=$result4['basdai3'];
	$basdai=$result4['basdai4'];
	$basdai=$result4['basdai5'];
	$basdai=$result4['basdai6'];
	$basdai=$result4['basfi'];
	$crp=$result3['crp'];
	$das28crp=$result3['das28crp'];
	$esr=$result3['esr'];
	$euroqol_score=$result5['euroqol_score'];
	$vasphysicial=$result3['vasphysicial'];
	$eq5d=$result5['eq5d'];
	$mhaq=$result3['mhaq'];
	$vasglobal=$result3['vasglobal'];
	$vaspain=$result3['vaspain'];
	$weight=$result3['weight'];
	$height=$result3['height'];
	
	$query_table .="'$swollenjc28','$tenderjc28','$basdai','$basdai1','$basdai2','$basdai3','$basdai4',";
	$query_table .="'$basdai5','$basdai6','$basfi','$crp','$das28crp','$esr','$euroqol_score',";
	$query_table .="'$vasphysicial','$eq5d','$mhaq','$vasglobal','$vaspain','$weight','$height',";
	
	$sql3 = get_activity_rpt($pat_id,$patient_cohort_id,18);
	$result3 = pg_fetch_array($sql3);
	$sql4 = get_spa_rpt($pat_id,$patient_cohort_id,18);
	$result4 = pg_fetch_array($sql4);
	$sql5 = get_euroqol_rpt($pat_id,$patient_cohort_id,18);
	$result5 = pg_fetch_array($sql5);
	$swollenjc28=$result3['swollenjc28'];
	$tenderjc28=$result3['tenderjc28'];
	$basdai=$result4['basdai'];
	$basdai=$result4['basdai1'];
	$basdai=$result4['basdai2'];
	$basdai=$result4['basdai3'];
	$basdai=$result4['basdai4'];
	$basdai=$result4['basdai5'];
	$basdai=$result4['basdai6'];
	$basdai=$result4['basfi'];
	$crp=$result3['crp'];
	$das28crp=$result3['das28crp'];
	$esr=$result3['esr'];
	$euroqol_score=$result5['euroqol_score'];
	$vasphysicial=$result3['vasphysicial'];
	$eq5d=$result5['eq5d'];
	$mhaq=$result3['mhaq'];
	$vasglobal=$result3['vasglobal'];
	$vaspain=$result3['vaspain'];
	$weight=$result3['weight'];
	$height=$result3['height'];
	
	$query_table .="'$swollenjc28','$tenderjc28','$basdai','$basdai1','$basdai2','$basdai3','$basdai4',";
	$query_table .="'$basdai5','$basdai6','$basfi','$crp','$das28crp','$esr','$euroqol_score',";
	$query_table .="'$vasphysicial','$eq5d','$mhaq','$vasglobal','$vaspain','$weight','$height',";
	
	$sql3 = get_activity_rpt($pat_id,$patient_cohort_id,24);
	$result3 = pg_fetch_array($sql3);
	$sql4 = get_spa_rpt($pat_id,$patient_cohort_id,24);
	$result4 = pg_fetch_array($sql4);
	$sql5 = get_euroqol_rpt($pat_id,$patient_cohort_id,24);
	$result5 = pg_fetch_array($sql5);
	$swollenjc28=$result3['swollenjc28'];
	$tenderjc28=$result3['tenderjc28'];
	$basdai=$result4['basdai'];
	$basdai=$result4['basdai1'];
	$basdai=$result4['basdai2'];
	$basdai=$result4['basdai3'];
	$basdai=$result4['basdai4'];
	$basdai=$result4['basdai5'];
	$basdai=$result4['basdai6'];
	$basdai=$result4['basfi'];
	$crp=$result3['crp'];
	$das28crp=$result3['das28crp'];
	$esr=$result3['esr'];
	$euroqol_score=$result5['euroqol_score'];
	$vasphysicial=$result3['vasphysicial'];
	$eq5d=$result5['eq5d'];
	$mhaq=$result3['mhaq'];
	$vasglobal=$result3['vasglobal'];
	$vaspain=$result3['vaspain'];
	$weight=$result3['weight'];
	$height=$result3['height'];
	
	$query_table .="'$swollenjc28','$tenderjc28','$basdai','$basdai1','$basdai2','$basdai3','$basdai4',";
	$query_table .="'$basdai5','$basdai6','$basfi','$crp','$das28crp','$esr','$euroqol_score',";
	$query_table .="'$vasphysicial','$eq5d','$mhaq','$vasglobal','$vaspain','$weight','$height',";
	
	$sql3 = get_activity_rpt($pat_id,$patient_cohort_id,36);
	$result3 = pg_fetch_array($sql3);
	$sql4 = get_spa_rpt($pat_id,$patient_cohort_id,36);
	$result4 = pg_fetch_array($sql4);
	$sql5 = get_euroqol_rpt($pat_id,$patient_cohort_id,36);
	$result5 = pg_fetch_array($sql5);
	$swollenjc28=$result3['swollenjc28'];
	$tenderjc28=$result3['tenderjc28'];
	$basdai=$result4['basdai'];
	$basdai=$result4['basdai1'];
	$basdai=$result4['basdai2'];
	$basdai=$result4['basdai3'];
	$basdai=$result4['basdai4'];
	$basdai=$result4['basdai5'];
	$basdai=$result4['basdai6'];
	$basdai=$result4['basfi'];
	$crp=$result3['crp'];
	$das28crp=$result3['das28crp'];
	$esr=$result3['esr'];
	$euroqol_score=$result5['euroqol_score'];
	$vasphysicial=$result3['vasphysicial'];
	$eq5d=$result5['eq5d'];
	$mhaq=$result3['mhaq'];
	$vasglobal=$result3['vasglobal'];
	$vaspain=$result3['vaspain'];
	$weight=$result3['weight'];
	$height=$result3['height'];
	
	$query_table .="'$swollenjc28','$tenderjc28','$basdai','$basdai1','$basdai2','$basdai3','$basdai4',";
	$query_table .="'$basdai5','$basdai6','$basfi','$crp','$das28crp','$esr','$euroqol_score',";
	$query_table .="'$vasphysicial','$eq5d','$mhaq','$vasglobal','$vaspain','$weight','$height',";
	
	$sql3 = get_activity_rpt($pat_id,$patient_cohort_id,48);
	$result3 = pg_fetch_array($sql3);
	$sql4 = get_spa_rpt($pat_id,$patient_cohort_id,48);
	$result4 = pg_fetch_array($sql4);
	$sql5 = get_euroqol_rpt($pat_id,$patient_cohort_id,48);
	$result5 = pg_fetch_array($sql5);
	$swollenjc28=$result3['swollenjc28'];
	$tenderjc28=$result3['tenderjc28'];
	$basdai=$result4['basdai'];
	$basdai=$result4['basdai1'];
	$basdai=$result4['basdai2'];
	$basdai=$result4['basdai3'];
	$basdai=$result4['basdai4'];
	$basdai=$result4['basdai5'];
	$basdai=$result4['basdai6'];
	$basdai=$result4['basfi'];
	$crp=$result3['crp'];
	$das28crp=$result3['das28crp'];
	$esr=$result3['esr'];
	$euroqol_score=$result5['euroqol_score'];
	$vasphysicial=$result3['vasphysicial'];
	$eq5d=$result5['eq5d'];
	$mhaq=$result3['mhaq'];
	$vasglobal=$result3['vasglobal'];
	$vaspain=$result3['vaspain'];
	$weight=$result3['weight'];
	$height=$result3['height'];
	
	$query_table .="'$swollenjc28','$tenderjc28','$basdai','$basdai1','$basdai2','$basdai3','$basdai4',";
	$query_table .="'$basdai5','$basdai6','$basfi','$crp','$das28crp','$esr','$euroqol_score',";
	$query_table .="'$vasphysicial','$eq5d','$mhaq','$vasglobal','$vaspain','$weight','$height',";
	
	$sql3 = get_activity_rpt($pat_id,$patient_cohort_id,60);
	$result3 = pg_fetch_array($sql3);
	$sql4 = get_spa_rpt($pat_id,$patient_cohort_id,60);
	$result4 = pg_fetch_array($sql4);
	$sql5 = get_euroqol_rpt($pat_id,$patient_cohort_id,60);
	$result5 = pg_fetch_array($sql5);
	$swollenjc28=$result3['swollenjc28'];
	$tenderjc28=$result3['tenderjc28'];
	$basdai=$result4['basdai'];
	$basdai=$result4['basdai1'];
	$basdai=$result4['basdai2'];
	$basdai=$result4['basdai3'];
	$basdai=$result4['basdai4'];
	$basdai=$result4['basdai5'];
	$basdai=$result4['basdai6'];
	$basdai=$result4['basfi'];
	$crp=$result3['crp'];
	$das28crp=$result3['das28crp'];
	$esr=$result3['esr'];
	$euroqol_score=$result5['euroqol_score'];
	$vasphysicial=$result3['vasphysicial'];
	$eq5d=$result5['eq5d'];
	$mhaq=$result3['mhaq'];
	$vasglobal=$result3['vasglobal'];
	$vaspain=$result3['vaspain'];
	$weight=$result3['weight'];
	$height=$result3['height'];
	
	$query_table .="'$swollenjc28','$tenderjc28','$basdai','$basdai1','$basdai2','$basdai3','$basdai4',";
	$query_table .="'$basdai5','$basdai6','$basfi','$crp','$das28crp','$esr','$euroqol_score',";
	$query_table .="'$vasphysicial','$eq5d','$mhaq','$vasglobal','$vaspain','$weight','$height'";
	$query_table .= ");";
	$result_query_table = pg_query($query_table);
	echo "Εγγραφή:".($row-1)." χρόνος:".date("Y-m-d H:i:s")."</br>";
}	
	///insert data - end
if($num_rows==$row-1)
	echo "<b>Η εισαγωγή στοιχείων έγινε κανονικά</b>";
else
{
		echo "<b>Η εισαγωγή στοιχείων δεν έγινε κανονικά.</b>";
		echo "Δεν μπήκαν ".($num_rows-$row-1)." εγγραφές";
}
include '../library/closeDB.php';
?>
