<?php
	session_start();
	
	include '../library/config.php';
	include '../library/openDB.php';
	include '../library/functions.php';
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php
include '../portion/head.php';
?>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav fixed sidebar-mini">
<div class="wrapper">

   <!-- Main Header -->
  <?php
  include "../portion/header.php";
  ?>
  <!-- Full Width Column -->
  <div class="content-wrapper">
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <small>Reports</small>
        </h1>
      </section>

      <!-- Main content -->
      <section class="content">
	 	<?php
			$query = "DROP TABLE statistics_lk1;";
			$result = pg_query($query);
			$query = "DROP TABLE statistics_lk2;";
			$result = pg_query($query);
			$query = "DROP TABLE statistics_lk3;";
			$result = pg_query($query);
			
			?>
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">create data</h3>
					</div>
						 <div class="box-body">
							<table id="table_jq" class="table table-bordered table-striped">
	<?php
	
	///create table - start 
$query_table = "";
$query_table .= "CREATE TABLE statistics_lk1 (id integer,";	
$query_table .='a1 VARCHAR(250),a2 VARCHAR(250),a3 VARCHAR(250),a4 VARCHAR(250),a5 VARCHAR(250),a6 VARCHAR(250),a7 VARCHAR(250),a8 VARCHAR(250),a9 VARCHAR(250),a10 VARCHAR(250),';
$query_table .='a11 VARCHAR(250),a12 VARCHAR(250),a13 VARCHAR(250),a14 VARCHAR(250),a15 VARCHAR(250),a16 VARCHAR(250),a17 VARCHAR(250),a18 VARCHAR(250),';
$query_table .='a19 VARCHAR(250),a20 VARCHAR(250),a21 VARCHAR(250),a22 VARCHAR(250),a23 VARCHAR(250),';
$query_table .='a24 VARCHAR(250),a25 VARCHAR(250),a26 VARCHAR(250),a27 VARCHAR(250),a28 VARCHAR(250),';
$query_table .='a29 VARCHAR(250),a30 VARCHAR(250),';
////comorbidities - start////
$query_table .='a31 VARCHAR(250),a32 VARCHAR(250),a33 VARCHAR(250),a34 VARCHAR(250),a35 VARCHAR(250),a36 VARCHAR(250),a37 VARCHAR(250),a38 VARCHAR(250),';
$query_table .='a39 VARCHAR(250),a40 VARCHAR(250),a41 VARCHAR(250),a42 VARCHAR(250),a43 VARCHAR(250),a44 VARCHAR(250),a45 VARCHAR(250),';
$query_table .='a46 VARCHAR(250),a47 VARCHAR(250),a48 VARCHAR(250),a49 VARCHAR(250),a50 VARCHAR(250),a51 VARCHAR(250),';
$query_table .='a52 VARCHAR(250),a53 VARCHAR(250),a54 VARCHAR(250),a55 VARCHAR(250),a56 VARCHAR(250),a57 VARCHAR(250),a58 VARCHAR(250),a59 VARCHAR(250),';
$query_table .='a60 VARCHAR(250),a61 VARCHAR(250),a62 VARCHAR(250),a63 VARCHAR(250),a64 VARCHAR(250),a65 VARCHAR(250),';
//$query_table .='Other (unspecified) liver cirrhosis VARCHAR(250),';
$query_table .='a66 VARCHAR(250),';
//$query_table .='Drug-induced chronic liver disease VARCHAR(250),';
$query_table .='a67 VARCHAR(250),a68 VARCHAR(250),a69 VARCHAR(250),a70 VARCHAR(250),a71 VARCHAR(250),a72 VARCHAR(250),a73 VARCHAR(250),a74 VARCHAR(250),a75 VARCHAR(250),';
$query_table .='a76 VARCHAR(250),a77 VARCHAR(250),a78 VARCHAR(250),a79 VARCHAR(250),a80 VARCHAR(250),';
$query_table .='a81 VARCHAR(250),a82 VARCHAR(250),a83 VARCHAR(250),a84 VARCHAR(250),a85 VARCHAR(250),a86 VARCHAR(250),a87 VARCHAR(250),';
$query_table .='a88 VARCHAR(250),a89 VARCHAR(250),a90 VARCHAR(250),a91 VARCHAR(250),a92 VARCHAR(250),a93 VARCHAR(250),';
$query_table .='a94 VARCHAR(250),a95 VARCHAR(250),a96 VARCHAR(250),a97 VARCHAR(250),a98 VARCHAR(250),a99 VARCHAR(250),';
////comorbidities - end////
$i=100;
////previous anti-reumatic drug treatment_nbr - start////

$exec2 = get_drugs_anti();
while($result2 = pg_fetch_array($exec2))
{	
	$query_table .= "a";
	$query_table .= $i;
	$query_table .= ' VARCHAR(250),';
	$i++;
}


////previous anti-reumatic drug treatment_nbr - end////

////other diseases characteristics - autoantibodies - start////


	for($j=$i;$j<=$i+1;$j++)
	{
		$query_table .= "a";
		$query_table .=$j;
		$query_table .=" VARCHAR(250),";
	}
	$i=$i+2;
////other diseases characteristics - autoantibodies - end////
////severity - start////

	for($j=$i;$j<=$i;$j++)
	{
		$query_table .= "a";
		$query_table .=$j;
		$query_table .=" VARCHAR(250),";
	}
	$i=$i+1;
	
////severity - end////
////B2-Criteria - start////
	for($j=$i;$j<=$i+4;$j++)
	{
		$query_table .= "a";
		$query_table .=$j;
		if($j<>$i+4)
			$query_table .=" VARCHAR(250),";
		else
			$query_table .=" VARCHAR(250)";
	}
	$i=$i+5;
	$query_table .= ");";
	$result = pg_query($query_table);
////B2-Criteria - end////

////Continuous therapy - start////
$sql = get_drugs_ongoing();
$num_rows = pg_num_rows($sql);
$query_table = "";
$query_table .= "CREATE TABLE statistics_lk2 (id integer,";
$sql = get_drugs_ongoing();
$k=0;
while($result2 = pg_fetch_array($sql))
{
	$k++;
	
	for($j=$i;$j<=$i+8;$j++)
	{
		$query_table .= "a";
		$query_table .=$j;
		if($k==$num_rows and $j==$i+8)
			$query_table .=" VARCHAR(250)";
		else
			$query_table .=" VARCHAR(250),";
	}
	$i=$i+9;
}
$query_table .= ");";


$result = pg_query($query_table);

////Continuous therapy - end////

////Corticosteroids & NSAID index - start////
$query_table = "";
$query_table .= "CREATE TABLE statistics_lk3 (id integer,";
	for($j=$i;$j<=$i+27;$j++)
	{
		$query_table .= "a";
		$query_table .=$j;
		$query_table .=" VARCHAR(250),";
	}
	$i=$i+28;
	
////Corticosteroids & NSAID index - end////

////Disease activity Common & Spa Euroqol - start////

	for($j=$i;$j<=$i+187;$j++)
	{
		$query_table .= "a";
		$query_table .=$j;
		if($j<>$i+187)
			$query_table .=" VARCHAR(250),";
		else
			$query_table .=" VARCHAR(250)";
	}
	$i=$i+188;
	
	$query_table .= ");";
	$result_query_table = pg_query($query_table);
////Disease activity Common & Spa Euroqol - end////

	
	///create table - end 
	
	///insert first row - start 
		
$query_table ="INSERT INTO statistics_lk1 VALUES ( 1,'cohort_id','pat_id','drug_id','substance','centre_id','centre_name','treatment_nbr','cohort_start_date','cohort_stop_date','stop_reason','";
$query_table .="stop_cause_comments','gender','date_of_inclusion','date_of_birth','main_diagnosis_date','icd10a','main_diagnosis_value','main_symptom_date','";
$query_table .="first_secondary_diagnosis_date','first_secondary_icd10b','first_secondary_diagnosis_value','first_secondary_symptom_date','second_secondary_diagnosis_date','";
$query_table .="second_secondary_icd10b','second_secondary_diagnosis_value','second_secondary_symptom_date','third_secondary_diagnosis_date','third_secondary_icd10b','";
$query_table .="third_secondary_diagnosis_value','third_secondary_symptom_date','";
////comorbidities - start////
$query_table .="Aortic_Aneurysm','Cardiac_arrhythmia','Congestive_Heart_Failure','Coronary_Heart_Disease','Acute_myocardial_infarction','Angina_pectoris','Atrial_fibrillation','Pericardial_disease_Acute_Pericarditis','";
$query_table .="Hypertension','Cardiomyopathies','Peripheral_vascular_disease','Valvular_Heart_Disease','Ischemic_strokeTIA','Sequelae_of_ischemic_stroke','Hemorrhagic_stroke','";
$query_table .="Sequelae_of_hemorrhagic_stroke','Anxiety_disorders','Bipolar_disorder','Dementia_unspecified','Major_depression_without_psychotic_symptoms','Other_depressive_episodes','";
$query_table .="Multiple_Sclerosis_Demyelinating_disease','Parkinsons_disease','Peripheral_Neuropathy','Psychosis','Schizophrenia','Asthma','Bronchiectasis','Chronic_obstructive_pulmonary_disease','";
$query_table .="Chronic_sinusitis','Interstitial_Lung_Disease','Pulmonary_Hypertension','Autoimmune_hepatitis','Alcoholic_liver_disease','Alcoholic_liver_cirrhosis','";
//$query_table .="Other (unspecified) liver cirrhosis','";
$query_table .="Nonalcoholic_fatty_liver_disease_NAFLD','";
//$query_table .="Drug-induced chronic liver disease','";
$query_table .="Crohn’s_disease','Diverticulosis','EsophagitisGERD','Peptic_ulcer_disease_biopsy_proven','Diabetes_mellitus','Dyslipidaemia','Hypothyroidism','Hyperthyroidism','Obesity','";
$query_table .="Chronic_kidney_disease_stage_III_GFR60','Chronic_kidney_disease_stage_IIIIV_(GFR_15-59)','End-stage_renal_disease_GFR_15','Hodgkin_Lymphoma','Non-Hodgkin_Lymphoma_unspecified','";
$query_table .="Leukemia','Solid_tumor','Solid_tumor_without_metastasi','Metastatic_solid_tumor','Metastatic_cancer_without_known_primary_tumor_site','Ischemic_Optic_Neuropathy','Optic_Neuritis','";
$query_table .="Chronic_osteomyelitis','Latent_TB_infection','Viral_hepatitis','Chronic_viral_infection_other','Past_hospitalization_for_serious_infection','Recurrent_past_hospitalizations_for_serious_infections','";
$query_table .="History_of_opportunistic_infection','Prosthetic_left_hip_joint','Prosthetic_right_hip_joint','Prosthetic_left_knee_joint','Prosthetic_right_knee_joint','Prosthetic_joint_other',";
////comorbidities - end////

////previous anti-reumatic drug treatment_nbr - start////

$exec2 = get_drugs_anti();
while($result2 = pg_fetch_array($exec2))
{	
	$query_table .= "'";
	$query_table .= str_replace("'","",$result2['substance']);
	$query_table .= "',";
}

////previous anti-reumatic drug treatment_nbr - end////

////other diseases characteristics - autoantibodies - start////

	$query_table .="'Anti-CCP','Rheumatoid Factor',";
////other diseases characteristics - autoantibodies - end////
////severity - start////
	$query_table .="'erosions',";
////severity - end////
////B2-Criteria - start////
	$query_table .="'RA','SpA','CASPAR','SLE','APS'";
////B2-Criteria - end////
$query_table .= ');';

$result_query_table = pg_query($query_table);

////Continuous therapy - start////
$query_table ="INSERT INTO statistics_lk2 VALUES (1,";
$sql = get_drugs_ongoing();
$num_rows = pg_num_rows($sql);
$k=0;
while($result2 = pg_fetch_array($sql))
{
	$k++;
	$query_table .="'".str_replace("'","",$result2["code"])."-".str_replace("'","",$result2["substance"])."-0','".str_replace("'","",$result2["code"])."-".str_replace("'","",$result2["substance"])."-3','".str_replace("'","",$result2["code"])."-".str_replace("'","",$result2["substance"])."-6',";
	$query_table .="'".str_replace("'","",$result2["code"])."-".str_replace("'","",$result2["substance"])."-12','".str_replace("'","",$result2["code"])."-".str_replace("'","",$result2["substance"])."-18','".str_replace("'","",$result2["code"])."-".str_replace("'","",$result2["substance"])."-24',";
	if($k<>$num_rows)
		$query_table .="'".str_replace("'","",$result2["code"])."-".str_replace("'","",$result2["substance"])."-36','".str_replace("'","",$result2["code"])."-".str_replace("'","",$result2["substance"])."-48','".str_replace("'","",$result2["code"])."-".str_replace("'","",$result2["substance"])."-60',";
	else
		$query_table .="'".str_replace("'","",$result2["code"])."-".str_replace("'","",$result2["substance"])."-36','".str_replace("'","",$result2["code"])."-".str_replace("'","",$result2["substance"])."-48','".str_replace("'","",$result2["code"])."-".str_replace("'","",$result2["substance"])."-60'";
	
}
$query_table .= ');';

$result_query_table = pg_query($query_table);

////Continuous therapy - end////

////Corticosteroids & NSAID index - start////
$query_table ="INSERT INTO statistics_lk3 VALUES (1,";
	$query_table .="'J-0','J1-0','J2-0','J-3','J1-3','J2-3','J-6','J1-6','J2-6','J-12','J1-12','J2-12',";
	$query_table .="'J-18','J1-18','J2-18','J-24','J1-24','J2-24','J-36','J1-36','J2-36','J-48','J1-48','J2-48','J-60','J1-60','J2-60',";
	

////Corticosteroids & NSAID index - end////

////Disease activity Common & Spa Euroqol - start////

	$query_table .="'28swollen_JC-0','28tender_JC-0','BASDAI-0','BASDAI1-0','BASDAI2-0','BASDAI3-0','BASDAI4-0','BASDAI5-0','BASDAI6-0','BASFI-0','CRP-0','DAS28CRP-0','ESR-0',";
	$query_table .="'VAS_physisian-0','Euroqol_total-0','EQ_5D_score-0','mHAQ-0','VASglobal-0','VASpain-0','Weight-0','Height-0',";
	$query_table .="'28swollen_JC-3','28tender_JC-3','BASDAI-3','BASDAI1-3','BASDAI2-3','BASDAI3-3','BASDAI4-3','BASDAI5-3','BASDAI6-3','BASFI-3','CRP-3','DAS28CRP-3','ESR-3',";
	$query_table .="'VAS_physisian-3','Euroqol_total-3','EQ_5D_score-3','mHAQ-3','VASglobal-3','VASpain-3','Weight-3','Height-3',";
	$query_table .="'28swollen_JC-6','28tender_JC-6','BASDAI-6','BASDAI1-6','BASDAI2-6','BASDAI3-6','BASDAI4-6','BASDAI5-6','BASDAI6-6','BASFI-6','CRP-6','DAS28CRP-6','ESR-6',";
	$query_table .="'VAS_physisian-6','Euroqol_total-6','EQ_5D_score-6','mHAQ-6','VASglobal-6','VASpain-6','Weight-6','Height-6',";
	$query_table .="'28swollen_JC-12','28tender_JC-12','BASDAI-12','BASDAI1-12','BASDAI2-12','BASDAI3-12','BASDAI4-12','BASDAI5-12','BASDAI6-12','BASFI-12','CRP-12','DAS28CRP-12','ESR-12',";
	$query_table .="'VAS_physisian-12','Euroqol_total-12','EQ_5D_score-12','mHAQ-12','VASglobal-12','VASpain-12','Weight-12','Height-12',";
	$query_table .="'28swollen JC-18','28tender JC-18','BASDAI-18','BASDAI1-18','BASDAI2-18','BASDAI3-18','BASDAI4-18','BASDAI5-18','BASDAI6-18','BASFI-18','CRP-18','DAS28CRP-18','ESR-18',";
	$query_table .="'VAS physisian-18','Euroqol total-18','EQ 5D score-18','mHAQ-18','VASglobal-18','VASpain-18','Weight-18','Height-18',";
	$query_table .="'28swollen JC-24','28tender JC-24','BASDAI-24','BASDAI1-24','BASDAI2-24','BASDAI3-24','BASDAI4-24','BASDAI5-24','BASDAI6-24','BASFI-24','CRP-24','DAS28CRP-24','ESR-24',";
	$query_table .="'VAS physisian-24','Euroqol total-24','EQ 5D score-24','mHAQ-24','VASglobal-24','VASpain-24','Weight-24','Height-24',";
	$query_table .="'28swollen JC-36','28tender JC-36','BASDAI-36','BASDAI1-36','BASDAI2-36','BASDAI3-36','BASDAI4-36','BASDAI5-36','BASDAI6-36','BASFI-36','CRP-36','DAS28CRP-36','ESR-36',";
	$query_table .="'VAS physisian-36','Euroqol total-36','EQ 5D score-36','mHAQ-36','VASglobal-36','VASpain-36','Weight-36','Height-36',";
	$query_table .="'28swollen JC-48','28tender JC-48','BASDAI-48','BASDAI1-48','BASDAI2-48','BASDAI3-48','BASDAI4-48','BASDAI5-48','BASDAI6-48','BASFI-48','CRP-48','DAS28CRP-48','ESR-48',";
	$query_table .="'VAS physisian-48','Euroqol total-48','EQ 5D score-48','mHAQ-48','VASglobal-48','VASpain-48','Weight-48','Height-48',";
	$query_table .="'28swollen JC-60','28tender JC-60','BASDAI-60','BASDAI1-60','BASDAI2-60','BASDAI3-60','BASDAI4-60','BASDAI5-60','BASDAI6-60','BASFI-60','CRP-60','DAS28CRP-60','ESR-60',";
	$query_table .="'VAS physisian-60','Euroqol total-60','EQ 5D score-60','mHAQ-60','VASglobal-60','VASpain-60','Weight-60','Height-60'";
////Disease activity Common & Spa Euroqol - end////

	$query_table .= ');';
	
	$result_query_table = pg_query($query_table);
	
	///insert first row - end 

	
	///insert data - start
	
$query = "select patient_cohort.erosions,patient_cohort.patient_cohort_id,patient_cohort.pat_id,patient_cohort.cohort_name,drugs.substance,patient.centre,lookup_tbl_val.value as centre_name,patient_cohort.treatment_nbr,TO_CHAR(patient_cohort.start_date, 'DD-MM-YYYY') AS start_date_str,TO_CHAR(patient_cohort.stop_date, 'DD-MM-YYYY') AS stop_date_str";
$query .= ",patient_cohort.stop_reason,patient_cohort.stop_ae_reason,patient.gender,TO_CHAR(patient.dateofinclusion, 'DD-MM-YYYY') AS dateofinclusion_str,TO_CHAR(patient.dateofbirth, 'DD-MM-YYYY') AS dateofbirth_str from patient_cohort left join patient on patient.pat_id=patient_cohort.pat_id left join drugs on patient_cohort.cohort_name=drugs.drugs_id left join lookup_tbl_val on lookup_tbl_val.id=patient.centre where patient.deleted=0 and patient_cohort.deleted=0 limit 3";


$exec = pg_query($query);
$row=1;
while($result = pg_fetch_array($exec))
{
	$row++;
$patient_cohort_id=$result['patient_cohort_id'];
$pat_id=$result['pat_id'];
$cohort_name=str_replace(",","",$result['cohort_name']);
$cohort_name=str_replace("'","",$cohort_name);
$substance=str_replace(",","",$result['substance']);
$substance=str_replace("'","",$substance);
$centre=$result['centre'];
$centre_name=str_replace(",","",$result['centre_name']);
$centre_name=str_replace("'","",$centre_name);
$treatment_nbr=$result['treatment_nbr'];
$start_date=$result['start_date_str'];
$stop_date=$result['stop_date_str'];
$stop_reason=$result['stop_reason'];
$stop_ae_reason=str_replace(",","",$result['stop_ae_reason']);
$stop_ae_reason=str_replace("'","",$stop_ae_reason);
$gender=$result['gender'];
$dateofinclusion=$result['dateofinclusion_str'];
$dateofbirth=$result['dateofbirth_str'];
$erosions=str_replace(",","",$result['erosions']);
$erosions=str_replace("'","",$erosions);

$query_table ="INSERT INTO statistics_lk1 VALUES ( $row,'$patient_cohort_id','$pat_id','$cohort_name','$substance','$centre','$centre_name','$treatment_nbr','$start_date','$stop_date','$stop_reason','";
$query_table .="$stop_ae_reason','$gender','$dateofinclusion','$dateofbirth',";


$query2 = "select patient_diagnosis.diagnosis_id,TO_CHAR(patient_diagnosis.symptom_date, 'DD-MM-YYYY') AS symptom_date_str,TO_CHAR(patient_diagnosis.disease_date, 'DD-MM-YYYY') AS disease_date_str,patient_diagnosis.icd10";
$query2 .= ",diagnosis.value from patient_diagnosis left join diagnosis on diagnosis.diagnosis_id=patient_diagnosis.diagnosis_id where patient_diagnosis.deleted=0 and patient_diagnosis.pat_id=$pat_id and patient_diagnosis.main=1";
$exec2 = pg_query($query2);
$result2 = pg_fetch_array($exec2);
$main_diagnosis_date=$result2['disease_date_str'];
$icd10a=str_replace(",","",$result2['icd10']);
$icd10a=str_replace("'","",$icd10a);
$main_diagnosis_value=str_replace(",","",$result2['value']);
$main_diagnosis_value=str_replace("'","",$main_diagnosis_value);
$main_symptom_date=$result2['symptom_date_str'];


$query_table .="'$main_diagnosis_date','$icd10a','$main_diagnosis_value','$main_symptom_date','";

$query2 = "select patient_diagnosis.diagnosis_id,TO_CHAR(patient_diagnosis.symptom_date, 'DD-MM-YYYY') AS symptom_date_str,TO_CHAR(patient_diagnosis.disease_date, 'DD-MM-YYYY') AS disease_date_str,patient_diagnosis.icd10";
$query2 .= ",diagnosis.value from patient_diagnosis left join diagnosis on diagnosis.diagnosis_id=patient_diagnosis.diagnosis_id where patient_diagnosis.deleted=0 and patient_diagnosis.pat_id=$pat_id and patient_diagnosis.main=0 limit 3";

$exec2 = pg_query($query2);

$result2 = pg_fetch_array($exec2);
$secondary_diagnosis_date=$result2['disease_date_str'];
$icd10a=str_replace(",","",$result2['icd10']);
$icd10a=str_replace("'","",$icd10a);
$secondary_diagnosis_value=str_replace(",","",$result2['value']);
$secondary_diagnosis_value=str_replace("'","",$secondary_diagnosis_value);
$secondary_symptom_date=$result2['symptom_date_str'];

$query_table .="$secondary_diagnosis_date','$icd10a','$secondary_diagnosis_value','$secondary_symptom_date','";

$result2 = pg_fetch_array($exec2);
$secondary_diagnosis_date=$result2['disease_date_str'];
$icd10a=str_replace(",","",$result2['icd10']);
$icd10a=str_replace("'","",$icd10a);
$secondary_diagnosis_value=str_replace(",","",$result2['value']);
$secondary_diagnosis_value=str_replace("'","",$secondary_diagnosis_value);
$secondary_symptom_date=$result2['symptom_date_str'];

$query_table .="$secondary_diagnosis_date','$icd10a','$secondary_diagnosis_value','$secondary_symptom_date','";

$result2 = pg_fetch_array($exec2);
$secondary_diagnosis_date=$result2['disease_date_str'];
$icd10a=str_replace(",","",$result2['icd10']);
$icd10a=str_replace("'","",$icd10a);
$secondary_diagnosis_value=str_replace(",","",$result2['value']);
$secondary_diagnosis_value=str_replace("'","",$secondary_diagnosis_value);
$secondary_symptom_date=$result2['symptom_date_str'];

$query_table .="$secondary_diagnosis_date','$icd10a','$secondary_diagnosis_value','$secondary_symptom_date','";

////comorbidities - start////
$exec2 = get_comorbidities_rpt();
while($result2 = pg_fetch_array($exec2))
{	
	$diseases_comorbidities_id = $result2['diseases_comorbidities_id'];
	$query3 = "select count(*) as cnt from patient_diagnosis where pat_id=$pat_id and deleted=0 and diagnosis_id=$diseases_comorbidities_id";
	$exec3 = pg_query($query3);
	$result3 = pg_fetch_array($exec3);
	$cnt=$result3['cnt'];
	
	if($cnt>0)
		$query_table .="TRUE";
	else
		$query_table .="FALSE";
	
	$query_table .="','";
}

////comorbidities - end////

////previous anti-reumatic drug treatment_nbr - start////
$exec2 = get_drugs_anti();
while($result2 = pg_fetch_array($exec2))
{	
	$previous_drugs_id = $result2['drugs_id'];
	$query3 = "select count(*) as cnt from patient_previous_drugs where pat_id=$pat_id and deleted=0 and drugs_id=$previous_drugs_id";
	$exec3 = pg_query($query3);
	$result3 = pg_fetch_array($exec3);
	$cnt=$result3['cnt'];
	
	if($cnt>0)
		$query_table .="TRUE";
	else
		$query_table .="FALSE";
	
	$query_table .="','";

}
////previous anti-reumatic drug treatment_nbr - end////

////other diseases characteristics - autoantibodies - start////
	$query3 = "select count(*) as cnt from patient_autoantibodies where pat_id=$pat_id and deleted=0 and antibodies_id=66";
	$exec3 = pg_query($query3);
	$result3 = pg_fetch_array($exec3);
	$cnt=$result3['cnt'];
	
	if($cnt>0)
		$query_table .="TRUE";
	else
		$query_table .="FALSE";
	
	$query_table .="','";
	
	
	$query3 = "select count(*) as cnt from patient_autoantibodies where pat_id=$pat_id and deleted=0 and antibodies_id=65";
	$exec3 = pg_query($query3);
	$result3 = pg_fetch_array($exec3);
	$cnt=$result3['cnt'];
	
	if($cnt>0)
		$query_table .="TRUE";
	else
		$query_table .="FALSE";
	
	$query_table .="',";
////other diseases characteristics - autoantibodies - end////
////severity - start////
	$query_table .="'";
	$query_table .=$erosions;
	$query_table .="','";
////severity - end////

////B2-Criteria - start////

	$order = get_racriteria_res($pat_id);
	$result2 = pg_fetch_assoc($order);
	$rheumatoid=$result2['rheumatoid'];
	if($rheumatoid=='Yes')
		$query_table .="TRUE";
	else
		$query_table .="FALSE";
	
	$query_table .="','";
	
	$order = get_peripheralcriteria_res($pat_id);
	$result2 = pg_fetch_assoc($order);
	$set1=$result2['set1'];
	$set2=$result2['set2'];
	
	$order = get_axialcriteria_res($pat_id);
	$result2 = pg_fetch_assoc($order);
	$newyork=$result2['newyork'];
	$imaging=$result2['imaging'];
	$clinical=$result2['clinical'];
	$radiographic=$result2['radiographic'];
	
	if($set1=='Yes' or $set2=='Yes' or $newyork=='Yes' or $imaging=='Yes' or $clinical=='Yes' or $radiographic=='Yes')
		$query_table .="TRUE";
	else
		$query_table .="FALSE";
	
	$query_table .="','";
	
	$order = get_casparcriteria_res($pat_id);
	$result2 = pg_fetch_assoc($order);
	$caspar=$result2['caspar'];
	if($caspar=='Yes')
		$query_table .="TRUE";
	else
		$query_table .="FALSE";
	
	$query_table .="','";
	
	$order = get_criteriasle_res($pat_id);
	$result2 = pg_fetch_assoc($order);
	$sle1997=$result2['sle1997'];
	$sle2012=$result2['sle2012'];
	$sle2017=$result2['sle2017'];
	
	if($sle1997=='Yes' or $sle2012=='Yes' or $sle2017=='Yes')
		$query_table .="TRUE";
	else
		$query_table .="FALSE";
	
	$query_table .="','";
	
	$order = get_apscriteria_res($pat_id);
	$result2 = pg_fetch_assoc($order);
	$aps=$result2['aps'];
	if($aps=='Yes')
		$query_table .="TRUE";
	else
		$query_table .="FALSE";
	
	$query_table .="'";
	$query_table .= ');';
	$result_query_table = pg_query($query_table);
	
////B2-Criteria - end////


	
////Disease activity Common & Spa Euroqol - end////

////Continuous therapy - start////
$query_table ="INSERT INTO statistics_lk2 VALUES ( $row,";
$sql = get_drugs_ongoing();
$num_rows = pg_num_rows($sql);
$k=0;
while($result2 = pg_fetch_array($sql))
{
	$k++;
	$drugs_id2=$result2['drugs_id'];
	$sql3 = get_drugs_ongoing_dosage($pat_id,$drugs_id2,$patient_cohort_id,0);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_ongoing_dosage($pat_id,$drugs_id2,$patient_cohort_id,3);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_ongoing_dosage($pat_id,$drugs_id2,$patient_cohort_id,6);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_ongoing_dosage($pat_id,$drugs_id2,$patient_cohort_id,12);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_ongoing_dosage($pat_id,$drugs_id2,$patient_cohort_id,18);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_ongoing_dosage($pat_id,$drugs_id2,$patient_cohort_id,24);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_ongoing_dosage($pat_id,$drugs_id2,$patient_cohort_id,36);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_ongoing_dosage($pat_id,$drugs_id2,$patient_cohort_id,48);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_ongoing_dosage($pat_id,$drugs_id2,$patient_cohort_id,60);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	if($k<>$num_rows)
		$query_table .="'$weekdosage',";
	else
		$query_table .="'$weekdosage'";
}

	$query_table .= ");";
	$result_query_table = pg_query($query_table);
////Continuous therapy - end////

	$query_table ="INSERT INTO statistics_lk3 VALUES ( $row,";
	////Corticosteroids & NSAID index - start////

	$sql3 = get_drugs_corticosteroids_dosage($pat_id,0,$patient_cohort_id,0);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,1,$patient_cohort_id,0);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,2,$patient_cohort_id,0);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,0,$patient_cohort_id,3);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,1,$patient_cohort_id,3);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,2,$patient_cohort_id,3);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,0,$patient_cohort_id,6);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,1,$patient_cohort_id,6);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,2,$patient_cohort_id,6);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,0,$patient_cohort_id,12);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,1,$patient_cohort_id,12);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,2,$patient_cohort_id,12);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,0,$patient_cohort_id,18);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,1,$patient_cohort_id,18);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,2,$patient_cohort_id,18);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,0,$patient_cohort_id,24);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,1,$patient_cohort_id,24);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,2,$patient_cohort_id,24);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,0,$patient_cohort_id,36);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,1,$patient_cohort_id,36);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,2,$patient_cohort_id,36);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,0,$patient_cohort_id,48);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,1,$patient_cohort_id,48);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,2,$patient_cohort_id,48);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,0,$patient_cohort_id,60);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,1,$patient_cohort_id,60);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
	$sql3 = get_drugs_corticosteroids_dosage($pat_id,2,$patient_cohort_id,60);
	$result3 = pg_fetch_array($sql3);
	$weekdosage=str_replace(",",".",$result3['weekdosage']);
	$weekdosage=str_replace("'","",$weekdosage);
	$query_table .="'$weekdosage',";
	
////Corticosteroids & NSAID index - end////

////Disease activity Common & Spa Euroqol - start////
	$sql3 = get_activity_rpt($pat_id,$patient_cohort_id,0);
	$result3 = pg_fetch_array($sql3);
	$sql4 = get_spa_rpt($pat_id,$patient_cohort_id,0);
	$result4 = pg_fetch_array($sql4);
	$sql5 = get_euroqol_rpt($pat_id,$patient_cohort_id,0);
	$result5 = pg_fetch_array($sql4);
	$swollenjc28=str_replace(",",".",$result3['swollenjc28']);
	$swollenjc28=str_replace("'","",$swollenjc28);
	$tenderjc28=str_replace(",",".",$result3['tenderjc28']);
	$tenderjc28=str_replace("'","",$tenderjc28);
	$basdai=str_replace(",",".",$result4['basdai']);
	$basdai=str_replace("'","",$basdai);
	$basdai1=str_replace(",",".",$result4['basdai1']);
	$basdai1=str_replace("'","",$basdai1);
	$basdai2=str_replace(",",".",$result4['basdai2']);
	$basdai2=str_replace("'","",$basdai2);
	$basdai3=str_replace(",",".",$result4['basdai3']);
	$basdai3=str_replace("'","",$basdai3);
	$basdai4=str_replace(",",".",$result4['basdai4']);
	$basdai4=str_replace("'","",$basdai4);
	$basdai5=str_replace(",",".",$result4['basdai5']);
	$basdai5=str_replace("'","",$basdai5);
	$basdai6=str_replace(",",".",$result4['basdai6']);
	$basdai6=str_replace("'","",$basdai6);
	$basfi=str_replace(",",".",$result4['basfi']);
	$basfi=str_replace("'","",$basfi);
	$crp=str_replace(",",".",$result3['crp']);
	$crp=str_replace("'","",$crp);
	$das28crp=str_replace(",",".",$result3['das28crp']);
	$das28crp=str_replace("'","",$das28crp);
	$esr=str_replace(",",".",$result3['esr']);
	$esr=str_replace("'","",$esr);
	$euroqol_score=str_replace(",",".",$result5['euroqol_score']);
	$euroqol_score=str_replace("'","",$euroqol_score);
	$vasphysicial=str_replace(",",".",$result3['vasphysicial']);
	$vasphysicial=str_replace("'","",$vasphysicial);
	$eq5d=str_replace(",",".",$result5['eq5d']);
	$eq5d=str_replace("'","",$eq5d);
	$mhaq=str_replace("'","",$mhaq);
	$mhaq=str_replace(",",".",$result3['mhaq']);
	$mhaq=str_replace("'","",$mhaq);
	$vasglobal=str_replace(",",".",$result3['vasglobal']);
	$vasglobal=str_replace("'","",$vasglobal);
	$vaspain=str_replace(",",".",$result3['vaspain']);
	$vaspain=str_replace("'","",$vaspain);
	$weight=str_replace(",",".",$result3['weight']);
	$weight=str_replace("'","",$weight);
	$height=str_replace(",",".",$result3['height']);
	$height=str_replace("'","",$height);
	
	$query_table .="'$swollenjc28','$tenderjc28','$basdai','$basdai1','$basdai2','$basdai3','$basdai4',";
	$query_table .="'$basdai5','$basdai6','$basfi','$crp','$das28crp','$esr','$euroqol_score',";
	$query_table .="'$vasphysicial','$eq5d','$mhaq','$vasglobal','$vaspain','$weight','$height',";
	
	$sql3 = get_activity_rpt($pat_id,$patient_cohort_id,3);
	$result3 = pg_fetch_array($sql3);
	$sql4 = get_spa_rpt($pat_id,$patient_cohort_id,3);
	$result4 = pg_fetch_array($sql4);
	$sql5 = get_euroqol_rpt($pat_id,$patient_cohort_id,3);
	$result5 = pg_fetch_array($sql4);
	$swollenjc28=str_replace(",",".",$result3['swollenjc28']);
	$swollenjc28=str_replace("'","",$swollenjc28);
	$tenderjc28=str_replace(",",".",$result3['tenderjc28']);
	$tenderjc28=str_replace("'","",$tenderjc28);
	$basdai=str_replace(",",".",$result4['basdai']);
	$basdai=str_replace("'","",$basdai);
	$basdai1=str_replace(",",".",$result4['basdai1']);
	$basdai1=str_replace("'","",$basdai1);
	$basdai2=str_replace(",",".",$result4['basdai2']);
	$basdai2=str_replace("'","",$basdai2);
	$basdai3=str_replace(",",".",$result4['basdai3']);
	$basdai3=str_replace("'","",$basdai3);
	$basdai4=str_replace(",",".",$result4['basdai4']);
	$basdai4=str_replace("'","",$basdai4);
	$basdai5=str_replace(",",".",$result4['basdai5']);
	$basdai5=str_replace("'","",$basdai5);
	$basdai6=str_replace(",",".",$result4['basdai6']);
	$basdai6=str_replace("'","",$basdai6);
	$basfi=str_replace(",",".",$result4['basfi']);
	$basfi=str_replace("'","",$basfi);
	$crp=str_replace(",",".",$result3['crp']);
	$crp=str_replace("'","",$crp);
	$das28crp=str_replace(",",".",$result3['das28crp']);
	$das28crp=str_replace("'","",$das28crp);
	$esr=str_replace(",",".",$result3['esr']);
	$esr=str_replace("'","",$esr);
	$euroqol_score=str_replace(",",".",$result5['euroqol_score']);
	$euroqol_score=str_replace("'","",$euroqol_score);
	$vasphysicial=str_replace(",",".",$result3['vasphysicial']);
	$vasphysicial=str_replace("'","",$vasphysicial);
	$eq5d=str_replace(",",".",$result5['eq5d']);
	$eq5d=str_replace("'","",$eq5d);
	$mhaq=str_replace("'","",$mhaq);
	$mhaq=str_replace(",",".",$result3['mhaq']);
	$mhaq=str_replace("'","",$mhaq);
	$vasglobal=str_replace(",",".",$result3['vasglobal']);
	$vasglobal=str_replace("'","",$vasglobal);
	$vaspain=str_replace(",",".",$result3['vaspain']);
	$vaspain=str_replace("'","",$vaspain);
	$weight=str_replace(",",".",$result3['weight']);
	$weight=str_replace("'","",$weight);
	$height=str_replace(",",".",$result3['height']);
	$height=str_replace("'","",$height);
	
	$query_table .="'$swollenjc28','$tenderjc28','$basdai','$basdai1','$basdai2','$basdai3','$basdai4',";
	$query_table .="'$basdai5','$basdai6','$basfi','$crp','$das28crp','$esr','$euroqol_score',";
	$query_table .="'$vasphysicial','$eq5d','$mhaq','$vasglobal','$vaspain','$weight','$height',";
	
	$sql3 = get_activity_rpt($pat_id,$patient_cohort_id,6);
	$result3 = pg_fetch_array($sql3);
	$sql4 = get_spa_rpt($pat_id,$patient_cohort_id,6);
	$result4 = pg_fetch_array($sql4);
	$sql5 = get_euroqol_rpt($pat_id,$patient_cohort_id,6);
	$result5 = pg_fetch_array($sql4);
	$swollenjc28=str_replace(",",".",$result3['swollenjc28']);
	$swollenjc28=str_replace("'","",$swollenjc28);
	$tenderjc28=str_replace(",",".",$result3['tenderjc28']);
	$tenderjc28=str_replace("'","",$tenderjc28);
	$basdai=str_replace(",",".",$result4['basdai']);
	$basdai=str_replace("'","",$basdai);
	$basdai1=str_replace(",",".",$result4['basdai1']);
	$basdai1=str_replace("'","",$basdai1);
	$basdai2=str_replace(",",".",$result4['basdai2']);
	$basdai2=str_replace("'","",$basdai2);
	$basdai3=str_replace(",",".",$result4['basdai3']);
	$basdai3=str_replace("'","",$basdai3);
	$basdai4=str_replace(",",".",$result4['basdai4']);
	$basdai4=str_replace("'","",$basdai4);
	$basdai5=str_replace(",",".",$result4['basdai5']);
	$basdai5=str_replace("'","",$basdai5);
	$basdai6=str_replace(",",".",$result4['basdai6']);
	$basdai6=str_replace("'","",$basdai6);
	$basfi=str_replace(",",".",$result4['basfi']);
	$basfi=str_replace("'","",$basfi);
	$crp=str_replace(",",".",$result3['crp']);
	$crp=str_replace("'","",$crp);
	$das28crp=str_replace(",",".",$result3['das28crp']);
	$das28crp=str_replace("'","",$das28crp);
	$esr=str_replace(",",".",$result3['esr']);
	$esr=str_replace("'","",$esr);
	$euroqol_score=str_replace(",",".",$result5['euroqol_score']);
	$euroqol_score=str_replace("'","",$euroqol_score);
	$vasphysicial=str_replace(",",".",$result3['vasphysicial']);
	$vasphysicial=str_replace("'","",$vasphysicial);
	$eq5d=str_replace(",",".",$result5['eq5d']);
	$eq5d=str_replace("'","",$eq5d);
	$mhaq=str_replace("'","",$mhaq);
	$mhaq=str_replace(",",".",$result3['mhaq']);
	$mhaq=str_replace("'","",$mhaq);
	$vasglobal=str_replace(",",".",$result3['vasglobal']);
	$vasglobal=str_replace("'","",$vasglobal);
	$vaspain=str_replace(",",".",$result3['vaspain']);
	$vaspain=str_replace("'","",$vaspain);
	$weight=str_replace(",",".",$result3['weight']);
	$weight=str_replace("'","",$weight);
	$height=str_replace(",",".",$result3['height']);
	$height=str_replace("'","",$height);
	
	$query_table .="'$swollenjc28','$tenderjc28','$basdai','$basdai1','$basdai2','$basdai3','$basdai4',";
	$query_table .="'$basdai5','$basdai6','$basfi','$crp','$das28crp','$esr','$euroqol_score',";
	$query_table .="'$vasphysicial','$eq5d','$mhaq','$vasglobal','$vaspain','$weight','$height',";
	
	$sql3 = get_activity_rpt($pat_id,$patient_cohort_id,12);
	$result3 = pg_fetch_array($sql3);
	$sql4 = get_spa_rpt($pat_id,$patient_cohort_id,12);
	$result4 = pg_fetch_array($sql4);
	$sql5 = get_euroqol_rpt($pat_id,$patient_cohort_id,12);
	$result5 = pg_fetch_array($sql4);
	$swollenjc28=str_replace(",",".",$result3['swollenjc28']);
	$swollenjc28=str_replace("'","",$swollenjc28);
	$tenderjc28=str_replace(",",".",$result3['tenderjc28']);
	$tenderjc28=str_replace("'","",$tenderjc28);
	$basdai=str_replace(",",".",$result4['basdai']);
	$basdai=str_replace("'","",$basdai);
	$basdai1=str_replace(",",".",$result4['basdai1']);
	$basdai1=str_replace("'","",$basdai1);
	$basdai2=str_replace(",",".",$result4['basdai2']);
	$basdai2=str_replace("'","",$basdai2);
	$basdai3=str_replace(",",".",$result4['basdai3']);
	$basdai3=str_replace("'","",$basdai3);
	$basdai4=str_replace(",",".",$result4['basdai4']);
	$basdai4=str_replace("'","",$basdai4);
	$basdai5=str_replace(",",".",$result4['basdai5']);
	$basdai5=str_replace("'","",$basdai5);
	$basdai6=str_replace(",",".",$result4['basdai6']);
	$basdai6=str_replace("'","",$basdai6);
	$basfi=str_replace(",",".",$result4['basfi']);
	$basfi=str_replace("'","",$basfi);
	$crp=str_replace(",",".",$result3['crp']);
	$crp=str_replace("'","",$crp);
	$das28crp=str_replace(",",".",$result3['das28crp']);
	$das28crp=str_replace("'","",$das28crp);
	$esr=str_replace(",",".",$result3['esr']);
	$esr=str_replace("'","",$esr);
	$euroqol_score=str_replace(",",".",$result5['euroqol_score']);
	$euroqol_score=str_replace("'","",$euroqol_score);
	$vasphysicial=str_replace(",",".",$result3['vasphysicial']);
	$vasphysicial=str_replace("'","",$vasphysicial);
	$eq5d=str_replace(",",".",$result5['eq5d']);
	$eq5d=str_replace("'","",$eq5d);
	$mhaq=str_replace("'","",$mhaq);
	$mhaq=str_replace(",",".",$result3['mhaq']);
	$mhaq=str_replace("'","",$mhaq);
	$vasglobal=str_replace(",",".",$result3['vasglobal']);
	$vasglobal=str_replace("'","",$vasglobal);
	$vaspain=str_replace(",",".",$result3['vaspain']);
	$vaspain=str_replace("'","",$vaspain);
	$weight=str_replace(",",".",$result3['weight']);
	$weight=str_replace("'","",$weight);
	$height=str_replace(",",".",$result3['height']);
	$height=str_replace("'","",$height);
	
	$query_table .="'$swollenjc28','$tenderjc28','$basdai','$basdai1','$basdai2','$basdai3','$basdai4',";
	$query_table .="'$basdai5','$basdai6','$basfi','$crp','$das28crp','$esr','$euroqol_score',";
	$query_table .="'$vasphysicial','$eq5d','$mhaq','$vasglobal','$vaspain','$weight','$height',";
	
	$sql3 = get_activity_rpt($pat_id,$patient_cohort_id,18);
	$result3 = pg_fetch_array($sql3);
	$sql4 = get_spa_rpt($pat_id,$patient_cohort_id,18);
	$result4 = pg_fetch_array($sql4);
	$sql5 = get_euroqol_rpt($pat_id,$patient_cohort_id,18);
	$result5 = pg_fetch_array($sql4);
	$swollenjc28=str_replace(",",".",$result3['swollenjc28']);
	$swollenjc28=str_replace("'","",$swollenjc28);
	$tenderjc28=str_replace(",",".",$result3['tenderjc28']);
	$tenderjc28=str_replace("'","",$tenderjc28);
	$basdai=str_replace(",",".",$result4['basdai']);
	$basdai=str_replace("'","",$basdai);
	$basdai1=str_replace(",",".",$result4['basdai1']);
	$basdai1=str_replace("'","",$basdai1);
	$basdai2=str_replace(",",".",$result4['basdai2']);
	$basdai2=str_replace("'","",$basdai2);
	$basdai3=str_replace(",",".",$result4['basdai3']);
	$basdai3=str_replace("'","",$basdai3);
	$basdai4=str_replace(",",".",$result4['basdai4']);
	$basdai4=str_replace("'","",$basdai4);
	$basdai5=str_replace(",",".",$result4['basdai5']);
	$basdai5=str_replace("'","",$basdai5);
	$basdai6=str_replace(",",".",$result4['basdai6']);
	$basdai6=str_replace("'","",$basdai6);
	$basfi=str_replace(",",".",$result4['basfi']);
	$basfi=str_replace("'","",$basfi);
	$crp=str_replace(",",".",$result3['crp']);
	$crp=str_replace("'","",$crp);
	$das28crp=str_replace(",",".",$result3['das28crp']);
	$das28crp=str_replace("'","",$das28crp);
	$esr=str_replace(",",".",$result3['esr']);
	$esr=str_replace("'","",$esr);
	$euroqol_score=str_replace(",",".",$result5['euroqol_score']);
	$euroqol_score=str_replace("'","",$euroqol_score);
	$vasphysicial=str_replace(",",".",$result3['vasphysicial']);
	$vasphysicial=str_replace("'","",$vasphysicial);
	$eq5d=str_replace(",",".",$result5['eq5d']);
	$eq5d=str_replace("'","",$eq5d);
	$mhaq=str_replace("'","",$mhaq);
	$mhaq=str_replace(",",".",$result3['mhaq']);
	$mhaq=str_replace("'","",$mhaq);
	$vasglobal=str_replace(",",".",$result3['vasglobal']);
	$vasglobal=str_replace("'","",$vasglobal);
	$vaspain=str_replace(",",".",$result3['vaspain']);
	$vaspain=str_replace("'","",$vaspain);
	$weight=str_replace(",",".",$result3['weight']);
	$weight=str_replace("'","",$weight);
	$height=str_replace(",",".",$result3['height']);
	$height=str_replace("'","",$height);
	
	$query_table .="'$swollenjc28','$tenderjc28','$basdai','$basdai1','$basdai2','$basdai3','$basdai4',";
	$query_table .="'$basdai5','$basdai6','$basfi','$crp','$das28crp','$esr','$euroqol_score',";
	$query_table .="'$vasphysicial','$eq5d','$mhaq','$vasglobal','$vaspain','$weight','$height',";
	
	$sql3 = get_activity_rpt($pat_id,$patient_cohort_id,24);
	$result3 = pg_fetch_array($sql3);
	$sql4 = get_spa_rpt($pat_id,$patient_cohort_id,24);
	$result4 = pg_fetch_array($sql4);
	$sql5 = get_euroqol_rpt($pat_id,$patient_cohort_id,24);
	$result5 = pg_fetch_array($sql4);
	$swollenjc28=str_replace(",",".",$result3['swollenjc28']);
	$swollenjc28=str_replace("'","",$swollenjc28);
	$tenderjc28=str_replace(",",".",$result3['tenderjc28']);
	$tenderjc28=str_replace("'","",$tenderjc28);
	$basdai=str_replace(",",".",$result4['basdai']);
	$basdai=str_replace("'","",$basdai);
	$basdai1=str_replace(",",".",$result4['basdai1']);
	$basdai1=str_replace("'","",$basdai1);
	$basdai2=str_replace(",",".",$result4['basdai2']);
	$basdai2=str_replace("'","",$basdai2);
	$basdai3=str_replace(",",".",$result4['basdai3']);
	$basdai3=str_replace("'","",$basdai3);
	$basdai4=str_replace(",",".",$result4['basdai4']);
	$basdai4=str_replace("'","",$basdai4);
	$basdai5=str_replace(",",".",$result4['basdai5']);
	$basdai5=str_replace("'","",$basdai5);
	$basdai6=str_replace(",",".",$result4['basdai6']);
	$basdai6=str_replace("'","",$basdai6);
	$basfi=str_replace(",",".",$result4['basfi']);
	$basfi=str_replace("'","",$basfi);
	$crp=str_replace(",",".",$result3['crp']);
	$crp=str_replace("'","",$crp);
	$das28crp=str_replace(",",".",$result3['das28crp']);
	$das28crp=str_replace("'","",$das28crp);
	$esr=str_replace(",",".",$result3['esr']);
	$esr=str_replace("'","",$esr);
	$euroqol_score=str_replace(",",".",$result5['euroqol_score']);
	$euroqol_score=str_replace("'","",$euroqol_score);
	$vasphysicial=str_replace(",",".",$result3['vasphysicial']);
	$vasphysicial=str_replace("'","",$vasphysicial);
	$eq5d=str_replace(",",".",$result5['eq5d']);
	$eq5d=str_replace("'","",$eq5d);
	$mhaq=str_replace("'","",$mhaq);
	$mhaq=str_replace(",",".",$result3['mhaq']);
	$mhaq=str_replace("'","",$mhaq);
	$vasglobal=str_replace(",",".",$result3['vasglobal']);
	$vasglobal=str_replace("'","",$vasglobal);
	$vaspain=str_replace(",",".",$result3['vaspain']);
	$vaspain=str_replace("'","",$vaspain);
	$weight=str_replace(",",".",$result3['weight']);
	$weight=str_replace("'","",$weight);
	$height=str_replace(",",".",$result3['height']);
	$height=str_replace("'","",$height);
	
	$query_table .="'$swollenjc28','$tenderjc28','$basdai','$basdai1','$basdai2','$basdai3','$basdai4',";
	$query_table .="'$basdai5','$basdai6','$basfi','$crp','$das28crp','$esr','$euroqol_score',";
	$query_table .="'$vasphysicial','$eq5d','$mhaq','$vasglobal','$vaspain','$weight','$height',";
	
	$sql3 = get_activity_rpt($pat_id,$patient_cohort_id,36);
	$result3 = pg_fetch_array($sql3);
	$sql4 = get_spa_rpt($pat_id,$patient_cohort_id,36);
	$result4 = pg_fetch_array($sql4);
	$sql5 = get_euroqol_rpt($pat_id,$patient_cohort_id,36);
	$result5 = pg_fetch_array($sql4);
	$swollenjc28=str_replace(",",".",$result3['swollenjc28']);
	$swollenjc28=str_replace("'","",$swollenjc28);
	$tenderjc28=str_replace(",",".",$result3['tenderjc28']);
	$tenderjc28=str_replace("'","",$tenderjc28);
	$basdai=str_replace(",",".",$result4['basdai']);
	$basdai=str_replace("'","",$basdai);
	$basdai1=str_replace(",",".",$result4['basdai1']);
	$basdai1=str_replace("'","",$basdai1);
	$basdai2=str_replace(",",".",$result4['basdai2']);
	$basdai2=str_replace("'","",$basdai2);
	$basdai3=str_replace(",",".",$result4['basdai3']);
	$basdai3=str_replace("'","",$basdai3);
	$basdai4=str_replace(",",".",$result4['basdai4']);
	$basdai4=str_replace("'","",$basdai4);
	$basdai5=str_replace(",",".",$result4['basdai5']);
	$basdai5=str_replace("'","",$basdai5);
	$basdai6=str_replace(",",".",$result4['basdai6']);
	$basdai6=str_replace("'","",$basdai6);
	$basfi=str_replace(",",".",$result4['basfi']);
	$basfi=str_replace("'","",$basfi);
	$crp=str_replace(",",".",$result3['crp']);
	$crp=str_replace("'","",$crp);
	$das28crp=str_replace(",",".",$result3['das28crp']);
	$das28crp=str_replace("'","",$das28crp);
	$esr=str_replace(",",".",$result3['esr']);
	$esr=str_replace("'","",$esr);
	$euroqol_score=str_replace(",",".",$result5['euroqol_score']);
	$euroqol_score=str_replace("'","",$euroqol_score);
	$vasphysicial=str_replace(",",".",$result3['vasphysicial']);
	$vasphysicial=str_replace("'","",$vasphysicial);
	$eq5d=str_replace(",",".",$result5['eq5d']);
	$eq5d=str_replace("'","",$eq5d);
	$mhaq=str_replace("'","",$mhaq);
	$mhaq=str_replace(",",".",$result3['mhaq']);
	$mhaq=str_replace("'","",$mhaq);
	$vasglobal=str_replace(",",".",$result3['vasglobal']);
	$vasglobal=str_replace("'","",$vasglobal);
	$vaspain=str_replace(",",".",$result3['vaspain']);
	$vaspain=str_replace("'","",$vaspain);
	$weight=str_replace(",",".",$result3['weight']);
	$weight=str_replace("'","",$weight);
	$height=str_replace(",",".",$result3['height']);
	$height=str_replace("'","",$height);
	
	$query_table .="'$swollenjc28','$tenderjc28','$basdai','$basdai1','$basdai2','$basdai3','$basdai4',";
	$query_table .="'$basdai5','$basdai6','$basfi','$crp','$das28crp','$esr','$euroqol_score',";
	$query_table .="'$vasphysicial','$eq5d','$mhaq','$vasglobal','$vaspain','$weight','$height',";
	
	$sql3 = get_activity_rpt($pat_id,$patient_cohort_id,48);
	$result3 = pg_fetch_array($sql3);
	$sql4 = get_spa_rpt($pat_id,$patient_cohort_id,48);
	$result4 = pg_fetch_array($sql4);
	$sql5 = get_euroqol_rpt($pat_id,$patient_cohort_id,48);
	$result5 = pg_fetch_array($sql4);
	$swollenjc28=str_replace(",",".",$result3['swollenjc28']);
	$swollenjc28=str_replace("'","",$swollenjc28);
	$tenderjc28=str_replace(",",".",$result3['tenderjc28']);
	$tenderjc28=str_replace("'","",$tenderjc28);
	$basdai=str_replace(",",".",$result4['basdai']);
	$basdai=str_replace("'","",$basdai);
	$basdai1=str_replace(",",".",$result4['basdai1']);
	$basdai1=str_replace("'","",$basdai1);
	$basdai2=str_replace(",",".",$result4['basdai2']);
	$basdai2=str_replace("'","",$basdai2);
	$basdai3=str_replace(",",".",$result4['basdai3']);
	$basdai3=str_replace("'","",$basdai3);
	$basdai4=str_replace(",",".",$result4['basdai4']);
	$basdai4=str_replace("'","",$basdai4);
	$basdai5=str_replace(",",".",$result4['basdai5']);
	$basdai5=str_replace("'","",$basdai5);
	$basdai6=str_replace(",",".",$result4['basdai6']);
	$basdai6=str_replace("'","",$basdai6);
	$basfi=str_replace(",",".",$result4['basfi']);
	$basfi=str_replace("'","",$basfi);
	$crp=str_replace(",",".",$result3['crp']);
	$crp=str_replace("'","",$crp);
	$das28crp=str_replace(",",".",$result3['das28crp']);
	$das28crp=str_replace("'","",$das28crp);
	$esr=str_replace(",",".",$result3['esr']);
	$esr=str_replace("'","",$esr);
	$euroqol_score=str_replace(",",".",$result5['euroqol_score']);
	$euroqol_score=str_replace("'","",$euroqol_score);
	$vasphysicial=str_replace(",",".",$result3['vasphysicial']);
	$vasphysicial=str_replace("'","",$vasphysicial);
	$eq5d=str_replace(",",".",$result5['eq5d']);
	$eq5d=str_replace("'","",$eq5d);
	$mhaq=str_replace("'","",$mhaq);
	$mhaq=str_replace(",",".",$result3['mhaq']);
	$mhaq=str_replace("'","",$mhaq);
	$vasglobal=str_replace(",",".",$result3['vasglobal']);
	$vasglobal=str_replace("'","",$vasglobal);
	$vaspain=str_replace(",",".",$result3['vaspain']);
	$vaspain=str_replace("'","",$vaspain);
	$weight=str_replace(",",".",$result3['weight']);
	$weight=str_replace("'","",$weight);
	$height=str_replace(",",".",$result3['height']);
	$height=str_replace("'","",$height);
	
	$query_table .="'$swollenjc28','$tenderjc28','$basdai','$basdai1','$basdai2','$basdai3','$basdai4',";
	$query_table .="'$basdai5','$basdai6','$basfi','$crp','$das28crp','$esr','$euroqol_score',";
	$query_table .="'$vasphysicial','$eq5d','$mhaq','$vasglobal','$vaspain','$weight','$height',";
	
	$sql3 = get_activity_rpt($pat_id,$patient_cohort_id,60);
	$result3 = pg_fetch_array($sql3);
	$sql4 = get_spa_rpt($pat_id,$patient_cohort_id,60);
	$result4 = pg_fetch_array($sql4);
	$sql5 = get_euroqol_rpt($pat_id,$patient_cohort_id,60);
	$result5 = pg_fetch_array($sql4);
	$swollenjc28=str_replace(",",".",$result3['swollenjc28']);
	$swollenjc28=str_replace("'","",$swollenjc28);
	$tenderjc28=str_replace(",",".",$result3['tenderjc28']);
	$tenderjc28=str_replace("'","",$tenderjc28);
	$basdai=str_replace(",",".",$result4['basdai']);
	$basdai=str_replace("'","",$basdai);
	$basdai1=str_replace(",",".",$result4['basdai1']);
	$basdai1=str_replace("'","",$basdai1);
	$basdai2=str_replace(",",".",$result4['basdai2']);
	$basdai2=str_replace("'","",$basdai2);
	$basdai3=str_replace(",",".",$result4['basdai3']);
	$basdai3=str_replace("'","",$basdai3);
	$basdai4=str_replace(",",".",$result4['basdai4']);
	$basdai4=str_replace("'","",$basdai4);
	$basdai5=str_replace(",",".",$result4['basdai5']);
	$basdai5=str_replace("'","",$basdai5);
	$basdai6=str_replace(",",".",$result4['basdai6']);
	$basdai6=str_replace("'","",$basdai6);
	$basfi=str_replace(",",".",$result4['basfi']);
	$basfi=str_replace("'","",$basfi);
	$crp=str_replace(",",".",$result3['crp']);
	$crp=str_replace("'","",$crp);
	$das28crp=str_replace(",",".",$result3['das28crp']);
	$das28crp=str_replace("'","",$das28crp);
	$esr=str_replace(",",".",$result3['esr']);
	$esr=str_replace("'","",$esr);
	$euroqol_score=str_replace(",",".",$result5['euroqol_score']);
	$euroqol_score=str_replace("'","",$euroqol_score);
	$vasphysicial=str_replace(",",".",$result3['vasphysicial']);
	$vasphysicial=str_replace("'","",$vasphysicial);
	$eq5d=str_replace(",",".",$result5['eq5d']);
	$eq5d=str_replace("'","",$eq5d);
	$mhaq=str_replace("'","",$mhaq);
	$mhaq=str_replace(",",".",$result3['mhaq']);
	$mhaq=str_replace("'","",$mhaq);
	$vasglobal=str_replace(",",".",$result3['vasglobal']);
	$vasglobal=str_replace("'","",$vasglobal);
	$vaspain=str_replace(",",".",$result3['vaspain']);
	$vaspain=str_replace("'","",$vaspain);
	$weight=str_replace(",",".",$result3['weight']);
	$weight=str_replace("'","",$weight);
	$height=str_replace(",",".",$result3['height']);
	$height=str_replace("'","",$height);
	
	$query_table .="'$swollenjc28','$tenderjc28','$basdai','$basdai1','$basdai2','$basdai3','$basdai4',";
	$query_table .="'$basdai5','$basdai6','$basfi','$crp','$das28crp','$esr','$euroqol_score',";
	$query_table .="'$vasphysicial','$eq5d','$mhaq','$vasglobal','$vaspain','$weight','$height'";
	$query_table .= ");";
	$result_query_table = pg_query($query_table);
}	
	///insert data - end
?>
			
       </div>
       <!-- /.box -->
		</div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
<?php
include "../portion/footer.php";
?>
</div>
<!-- ./wrapper -->
<?php
include "../portion/js.php";
include '../library/closeDB.php';
?>
</body>
</html>
